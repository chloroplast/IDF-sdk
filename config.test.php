<?php
ini_set("display_errors", "on");

return [
    //cache
    'cache.route.disable' => true,
    'cache.session.ttl' => 300, //用户`session`缓存时间
    'cache.restful.ttl'=> 300, //接口请求缓存时间

    //session
    'session.save_path' => 'memcached-session-1:11211,memcached-session-2:11211',
    
    //memcached
    'memcached.service'=>[['memcached-data-1',11211],['memcached-data-2',11211]],
    
    //jwt
    'jwt.iss' => 'idf',
    'jwt.sub' => 'idf.user',
    'jwt.aud' => 'idf',
    'jwt.key' => 'MIICXAIBAAKBgQC8kGa1pSjbSYZVebtTRBLxBz5H4i2p',

    //cookie
    'cookie.name'       => 'cookieName',
    'cookie.domain'     => '',
    'cookie.path'       => '/',
    'cookie.encrypt.key' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9',
];