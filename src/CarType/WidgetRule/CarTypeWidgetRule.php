<?php
namespace Sdk\CarType\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

class CarTypeWidgetRule
{
    const NAME_MIN_LENGTH = 1;
    const NAME_MAX_LENGTH = 20;
    //验证车辆类型长度：1-20个字符
    public function name($name) : bool
    {
        if (!V::stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(CARTYPE_NAME_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    //验证板数上限为3位以内数字
    public function palletNumber($palletNumber) : bool
    {
        $regex = '/^[1-9]\d{0,1}$/';

        if (!preg_match($regex, $palletNumber)) {
            Core::setLastError(CARTYPE_PALLET_NUMBER_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    //验证载重上限为7位以内数字
    public function weight($weight) : bool
    {
        $regex = '/^[1-9]\d{0,5}$/';

        if (!preg_match($regex, $weight)) {
            Core::setLastError(CARTYPE_WEIGHT_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }
}
