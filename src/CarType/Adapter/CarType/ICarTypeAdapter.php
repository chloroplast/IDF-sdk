<?php
namespace Sdk\CarType\Adapter\CarType;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

interface ICarTypeAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    
}
