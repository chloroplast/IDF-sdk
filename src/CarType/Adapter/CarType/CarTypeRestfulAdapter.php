<?php
namespace Sdk\CarType\Adapter\CarType;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\CarType\Model\NullCarType;
use Sdk\CarType\Translator\CarTypeRestfulTranslator;

class CarTypeRestfulAdapter extends CommonRestfulAdapter implements ICarTypeAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'name' => CARTYPE_NAME_FORMAT_INCORRECT,
            'palletNumber' => CARTYPE_PALLET_NUMBER_FORMAT_INCORRECT,
            'weight' => CARTYPE_WEIGHT_FORMAT_INCORRECT
        ),
        100002 => array(
            'carType' => RESOURCE_CAN_NOT_MODIFY
        ),
        100003 => array(
            'name' => CARTYPE_NAME_EXISTS
        ),
        100004 => array(
            'staff' => USER_IDENTITY_AUTHENTICATION_FAILED
        )
    );

    const SCENARIOS = [
        'CARTYPE_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'CARTYPE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new CarTypeRestfulTranslator(),
            'carTypes',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullCarType::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'name',
            'palletNumber',
            'weight',
            'staff'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'name',
            'palletNumber',
            'weight',
            'staff'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function disableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function deletedTranslatorKeys() : array
    {
        return array();
    }
}
