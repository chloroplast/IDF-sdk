<?php
namespace Sdk\CarType\Adapter\CarType;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\CarType\Model\CarType;

//use Sdk\CarType\Utils\MockObjectGenerate;

class CarTypeMockAdapter implements ICarTypeAdapter
{
    use OperateAbleMockAdapterTrait, FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new CarType($id);
       // return MockObjectGenerate::generateCarType($id);
    }
}
