<?php
namespace Sdk\CarType\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\CarType\Model\CarType;
use Sdk\CarType\Model\NullCarType;

use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class CarTypeTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullCarType::getInstance();
    }

    public function arrayToObject(array $expression, $carType = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($carType == null) {
            $carType = new CarType();
        }
        
        if (isset($expression['id'])) {
            $carType->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['name'])) {
            $carType->setName($expression['name']);
        }
        if (isset($expression['palletNumber'])) {
            $carType->setPalletNumber($expression['palletNumber']);
        }
        if (isset($expression['weight'])) {
            $carType->setWeight($expression['weight']);
        }
        if (isset($expression['status']['id'])) {
            $carType->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $carType->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $carType->setUpdateTime($expression['updateTime']);
        }

        return $carType;
    }

    public function objectToArray($carType, array $keys = array())
    {
        if (!$carType instanceof CarType) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'palletNumber',
                'weight',
                'staff' => ['id', 'name'],
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($carType->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $carType->getName();
        }
        if (in_array('palletNumber', $keys)) {
            $expression['palletNumber'] = $carType->getPalletNumber();
        }
        if (in_array('weight', $keys)) {
            $expression['weight'] = $carType->getWeight();
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $carType->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($carType->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $carType->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $carType->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $carType->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $carType->getUpdateTime());
        }

        return $expression;
    }
}
