<?php
namespace Sdk\CarType\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\CarType\Model\CarType;
use Sdk\CarType\Model\NullCarType;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class CarTypeRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $carType = null)
    {
        if (empty($expression)) {
            return NullCarType::getInstance();
        }

        if ($carType == null) {
            $carType = new CarType();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $carType->setId($data['id']);
        }
        if (isset($attributes['name'])) {
            $carType->setName($attributes['name']);
        }
        if (isset($attributes['palletNumber'])) {
            $carType->setPalletNumber($attributes['palletNumber']);
        }
        if (isset($attributes['weight'])) {
            $carType->setWeight($attributes['weight']);
        }
        if (isset($attributes['status'])) {
            $carType->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $carType->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $carType->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $carType->setUpdateTime($attributes['updateTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }
        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $carType->setStaff($staff);
        }

        return $carType;
    }

    public function objectToArray($carType, array $keys = array())
    {
        if (!$carType instanceof CarType) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'palletNumber',
                'weight',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'carTypes'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $carType->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $carType->getName();
        }
        if (in_array('palletNumber', $keys)) {
            $attributes['palletNumber'] = $carType->getPalletNumber();
        }
        if (in_array('weight', $keys)) {
            $attributes['weight'] = $carType->getWeight();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        
        return $expression;
    }
}
