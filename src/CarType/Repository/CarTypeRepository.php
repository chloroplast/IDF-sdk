<?php
namespace Sdk\CarType\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\CarType\Model\CarType;
use Sdk\CarType\Adapter\CarType\ICarTypeAdapter;
use Sdk\CarType\Adapter\CarType\CarTypeMockAdapter;
use Sdk\CarType\Adapter\CarType\CarTypeRestfulAdapter;

class CarTypeRepository extends CommonRepository implements ICarTypeAdapter
{
    const LIST_MODEL_UN = 'CARTYPE_LIST';
    const FETCH_ONE_MODEL_UN = 'CARTYPE_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new CarTypeRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new CarTypeMockAdapter()
        );
    }
}
