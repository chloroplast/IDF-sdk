<?php
namespace Sdk\CarType\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\User\Staff\Model\Staff;

use Sdk\CarType\Repository\CarTypeRepository;

class CarType implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    private $id;
    /**
     * @var string $name 车辆类型
     */
    private $name;
    /**
     * @var int $palletNumber 板数上限
     */
    private $palletNumber;
    /**
     * @var int $weight 载重上限
     */
    private $weight;
    /**
     * @var Staff $staff 发布人
     */
    private $staff;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->palletNumber = 0;
        $this->weight = 0;
        $this->staff = new Staff();
        $this->status = self::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new CarTypeRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->palletNumber);
        unset($this->weight);
        unset($this->staff);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setPalletNumber(int $palletNumber): void
    {
        $this->palletNumber = $palletNumber;
    }

    public function getPalletNumber(): int
    {
        return $this->palletNumber;
    }

    public function setWeight(int $weight): void
    {
        $this->weight = $weight;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function setStaff(Staff $staff): void
    {
        $this->staff = $staff;
    }

    public function getStaff(): Staff
    {
        return $this->staff;
    }

    protected function getRepository() : CarTypeRepository
    {
        return $this->repository;
    }
}
