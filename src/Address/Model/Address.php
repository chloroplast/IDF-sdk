<?php
namespace Sdk\Address\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\User\Member\Model\Member;
use Sdk\Address\Repository\AddressRepository;

class Address implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    const IS_DEFAULT = array(
        'NO' => 0,
        'YES' => 1
    );
    const IS_DEFAULT_CN = array(
        self::IS_DEFAULT['NO'] => '否',
        self::IS_DEFAULT['YES'] => '是'
    );
    const IS_DEFAULT_TYPE = array(
        self::IS_DEFAULT['NO'] => 'danger',
        self::IS_DEFAULT['YES'] => 'success'
    );

    const CATEGORY = array(
        'SHIPPING' => 0,
        'RECEIVING' => 2
    );
    const CATEGORY_CN = array(
        self::CATEGORY['SHIPPING'] => '发货地址',
        self::CATEGORY['RECEIVING'] => '派送（收货）地址'
    );

    private $id;
    /**
     * @var string $enterpriseName 仓库联系人
     */
    private $enterpriseName;
    /**
     * @var string $firstAddress 地址一
     */
    private $firstAddress;
    /**
     * @var string $secondAddress 地址二
     */
    private $secondAddress;
    /**
     * @var string $phone 联系电话
     */
    private $phone;
    /**
     * @var string $state 州
     */
    private $state;
    /**
     * @var string $city 市
     */
    private $city;
    /**
     * @var string $postalCode 邮编
     */
    private $postalCode;
    /**
     * @var int $isDefault 是否自营
     */
    private $isDefault;
    /**
     * @var int $category 地址类型
     */
    private $category;
    /**
     * @var Member $member
     */
    private $member;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->enterpriseName = '';
        $this->firstAddress = '';
        $this->secondAddress = '';
        $this->phone = '';
        $this->state = '';
        $this->city = '';
        $this->postalCode = '';
        $this->isDefault = self::IS_DEFAULT['NO'];
        $this->category = self::CATEGORY['SHIPPING'];
        $this->member = new Member();
        $this->status = self::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new AddressRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->enterpriseName);
        unset($this->firstAddress);
        unset($this->secondAddress);
        unset($this->phone);
        unset($this->state);
        unset($this->city);
        unset($this->postalCode);
        unset($this->isDefault);
        unset($this->category);
        unset($this->member);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setEnterpriseName(string $enterpriseName): void
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName(): string
    {
        return $this->enterpriseName;
    }

    public function setFirstAddress(string $firstAddress): void
    {
        $this->firstAddress = $firstAddress;
    }

    public function getFirstAddress(): string
    {
        return $this->firstAddress;
    }

    public function setSecondAddress(string $secondAddress): void
    {
        $this->secondAddress = $secondAddress;
    }

    public function getSecondAddress(): string
    {
        return $this->secondAddress;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setState(string $state): void
    {
        $this->state = $state;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setPostalCode(string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setIsDefault(int $isDefault): void
    {
        $this->isDefault = $isDefault;
    }

    public function getIsDefault(): int
    {
        return $this->isDefault;
    }

    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): int
    {
        return $this->category;
    }

    public function setMember(Member $member): void
    {
        $this->member = $member;
    }

    public function getMember(): Member
    {
        return $this->member;
    }

    protected function getRepository() : AddressRepository
    {
        return $this->repository;
    }

    public function default() : bool
    {
        if (!$this->isEnableStatus()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY, array('pointer' => 'isDefault'));
            return false;
        }

        return $this->getRepository()->default($this);
    }
}
