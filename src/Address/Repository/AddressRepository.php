<?php
namespace Sdk\Address\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Address\Model\Address;
use Sdk\Address\Adapter\Address\IAddressAdapter;
use Sdk\Address\Adapter\Address\AddressMockAdapter;
use Sdk\Address\Adapter\Address\AddressRestfulAdapter;

class AddressRepository extends CommonRepository implements IAddressAdapter
{
    const LIST_MODEL_UN = 'ADDRESS_LIST';
    const FETCH_ONE_MODEL_UN = 'ADDRESS_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new AddressRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new AddressMockAdapter()
        );
    }

    public function default(Address $address) : bool
    {
        return $this->getAdapter()->default($address);
    }
}
