<?php
namespace Sdk\Address\Adapter\Address;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Address\Model\Address;

interface IAddressAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function default(Address $address) : bool;
}
