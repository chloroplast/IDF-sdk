<?php
namespace Sdk\Address\Adapter\Address;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Address\Model\Address;
use Sdk\Address\Model\NullAddress;
use Sdk\Address\Translator\AddressRestfulTranslator;

class AddressRestfulAdapter extends CommonRestfulAdapter implements IAddressAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'enterpriseName' => ENTERPRISE_NAME_FORMAT_INCORRECT,
            'firstAddress' => PARAMETER_FORMAT_ERROR,
            'secondAddress' => PARAMETER_FORMAT_ERROR,
            'phone' => PHONE_FORMAT_INCORRECT,
            'state' => STATE_FORMAT_INCORRECT,
            'city' => CITY_FORMAT_INCORRECT,
            'postalCode' => PARAMETER_FORMAT_ERROR,
            'isDefault' => PARAMETER_FORMAT_ERROR,
            'category' => PARAMETER_FORMAT_ERROR
        ),
        100002 => array(
            'address' => RESOURCE_CAN_NOT_MODIFY
        ),
        100004 => array(
            'member' => USER_IDENTITY_AUTHENTICATION_FAILED
        )
    );
    
    const SCENARIOS = [
        'ADDRESS_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'ADDRESS_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new AddressRestfulTranslator(),
            'addresses',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullAddress::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'enterpriseName',
            'firstAddress',
            'secondAddress',
            'phone',
            'state',
            'city',
            'postalCode',
            'isDefault',
            'category',
            'member'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'enterpriseName',
            'firstAddress',
            'secondAddress',
            'phone',
            'state',
            'city',
            'postalCode',
            'isDefault',
            'category',
            'member'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array(
            'member'
        );
    }

    protected function disableTranslatorKeys() : array
    {
        return array(
            'member'
        );
    }

    protected function deletedTranslatorKeys() : array
    {
        return array(
            'member'
        );
    }

    public function default(Address $address) : bool
    {
        $data = $this->getTranslator()->objectToArray($address, array('member'));
        unset($data['data']['attributes']);

        $this->patch(
            $this->getResource().'/'.$address->getId().'/default',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($address);
            return true;
        }

        return false;
    }
}
