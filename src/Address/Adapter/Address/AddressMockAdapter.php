<?php
namespace Sdk\Address\Adapter\Address;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Address\Model\Address;

//use Sdk\Address\Utils\MockObjectGenerate;

class AddressMockAdapter implements IAddressAdapter
{
    use OperateAbleMockAdapterTrait, FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new Address($id);
       // return MockObjectGenerate::generateAddress($id);
    }

    public function default(Address $address) : bool
    {
        unset($address);
        return true;
    }
}
