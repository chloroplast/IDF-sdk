<?php
namespace Sdk\Address\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Address\Model\Address;
use Sdk\Address\Model\NullAddress;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AddressTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullAddress::getInstance();
    }

    public function arrayToObject(array $expression, $address = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($address == null) {
            $address = new Address();
        }

        if (isset($expression['id'])) {
            $address->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['enterpriseName'])) {
            $address->setEnterpriseName($expression['enterpriseName']);
        }
        if (isset($expression['firstAddress'])) {
            $address->setFirstAddress($expression['firstAddress']);
        }
        if (isset($expression['secondAddress'])) {
            $address->setSecondAddress($expression['secondAddress']);
        }
        if (isset($expression['phone'])) {
            $address->setPhone($expression['phone']);
        }
        if (isset($expression['state'])) {
            $address->setState($expression['state']);
        }
        if (isset($expression['city'])) {
            $address->setCity($expression['city']);
        }
        if (isset($expression['postalCode'])) {
            $address->setPostalCode($expression['postalCode']);
        }
        if (isset($expression['isDefault'])) {
            $address->setIsDefault($expression['isDefault']);
        }
        if (isset($expression['category'])) {
            $address->setCategory($expression['category']);
        }
        if (isset($expression['status']['id'])) {
            $address->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $address->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $address->setUpdateTime($expression['updateTime']);
        }

        return $address;
    }

    public function objectToArray($address, array $keys = array())
    {
        if (!$address instanceof Address) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'enterpriseName',
                'firstAddress',
                'secondAddress',
                'phone',
                'state',
                'city',
                'postalCode',
                'isDefault',
                'category',
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($address->getId());
        }
        if (in_array('enterpriseName', $keys)) {
            $expression['enterpriseName'] = $address->getEnterpriseName();
        }
        if (in_array('firstAddress', $keys)) {
            $expression['firstAddress'] = $address->getFirstAddress();
        }
        if (in_array('secondAddress', $keys)) {
            $expression['secondAddress'] = $address->getSecondAddress();
        }
        if (in_array('phone', $keys)) {
            $expression['phone'] = $address->getPhone();
        }
        if (in_array('state', $keys)) {
            $expression['state'] = $address->getState();
        }
        if (in_array('city', $keys)) {
            $expression['city'] = $address->getCity();
        }
        if (in_array('postalCode', $keys)) {
            $expression['postalCode'] = $address->getPostalCode();
        }
        if (in_array('isDefault', $keys)) {
            $expression['isDefault'] = $this->statusFormatConversion(
                $address->getIsDefault(),
                Address::IS_DEFAULT_TYPE,
                Address::IS_DEFAULT_CN
            );
        }
        if (in_array('category', $keys)) {
            $expression['category'] = $this->typeFormatConversion(
                $address->getCategory(),
                Address::CATEGORY_CN
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($address->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $address->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $address->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $address->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $address->getUpdateTime());
        }

        return $expression;
    }
}
