<?php
namespace Sdk\Address\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Address\Model\Address;
use Sdk\Address\Model\NullAddress;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AddressRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $address = null)
    {
        if (empty($expression)) {
            return NullAddress::getInstance();
        }

        if ($address == null) {
            $address = new Address();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        
        if (isset($data['id'])) {
            $address->setId($data['id']);
        }
        if (isset($attributes['enterpriseName'])) {
            $address->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['firstAddress'])) {
            $address->setFirstAddress($attributes['firstAddress']);
        }
        if (isset($attributes['secondAddress'])) {
            $address->setSecondAddress($attributes['secondAddress']);
        }
        if (isset($attributes['phone'])) {
            $address->setPhone($attributes['phone']);
        }
        if (isset($attributes['state'])) {
            $address->setState($attributes['state']);
        }
        if (isset($attributes['city'])) {
            $address->setCity($attributes['city']);
        }
        if (isset($attributes['postalCode'])) {
            $address->setPostalCode($attributes['postalCode']);
        }
        if (isset($attributes['isDefault'])) {
            $address->setIsDefault($attributes['isDefault']);
        }
        if (isset($attributes['category'])) {
            $address->setCategory($attributes['category']);
        }
        if (isset($attributes['status'])) {
            $address->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $address->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $address->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $address->setUpdateTime($attributes['updateTime']);
        }

        return $address;
    }

    public function objectToArray($address, array $keys = array())
    {
        if (!$address instanceof Address) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'enterpriseName',
                'firstAddress',
                'secondAddress',
                'phone',
                'state',
                'city',
                'postalCode',
                'isDefault',
                'category',
                'member'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'addresses'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $address->getId();
        }

        $attributes = array();

        if (in_array('enterpriseName', $keys)) {
            $attributes['enterpriseName'] = $address->getEnterpriseName();
        }
        if (in_array('firstAddress', $keys)) {
            $attributes['firstAddress'] = $address->getFirstAddress();
        }
        if (in_array('secondAddress', $keys)) {
            $attributes['secondAddress'] = $address->getSecondAddress();
        }
        if (in_array('phone', $keys)) {
            $attributes['phone'] = $address->getPhone();
        }
        if (in_array('state', $keys)) {
            $attributes['state'] = $address->getState();
        }
        if (in_array('city', $keys)) {
            $attributes['city'] = $address->getCity();
        }
        if (in_array('postalCode', $keys)) {
            $attributes['postalCode'] = $address->getPostalCode();
        }
        if (in_array('isDefault', $keys)) {
            $attributes['isDefault'] = $address->getIsDefault();
        }
        if (in_array('category', $keys)) {
            $attributes['category'] = $address->getCategory();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('member', $keys)) {
            $memberRelationships = array(
                'type' => 'member',
                'id' => strval($address->getMember()->getId())
            );

            $expression['data']['relationships']['member']['data'] = $memberRelationships;
        }

        return $expression;
    }
}
