<?php
namespace Sdk\Policy\Adapter\OrderLTLPolicy;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

interface IOrderLTLPolicyAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function getTemplate();
}
