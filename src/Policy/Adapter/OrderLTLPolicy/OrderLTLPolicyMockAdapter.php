<?php
namespace Sdk\Policy\Adapter\OrderLTLPolicy;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\OrderLTLPolicy;

class OrderLTLPolicyMockAdapter implements IOrderLTLPolicyAdapter
{
    use OperateAbleMockAdapterTrait, FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new OrderLTLPolicy($id);
    }

    public function getTemplate()
    {
        return new Template();
    }
}
