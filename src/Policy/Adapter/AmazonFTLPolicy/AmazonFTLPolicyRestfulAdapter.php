<?php
namespace Sdk\Policy\Adapter\AmazonFTLPolicy;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Policy\Model\NullTemplate;
use Sdk\Policy\Translator\TemplateRestfulTranslator;

use Sdk\Policy\Model\NullAmazonFTLPolicy;
use Sdk\Policy\Translator\AmazonFTLPolicyRestfulTranslator;

class AmazonFTLPolicyRestfulAdapter extends CommonRestfulAdapter implements IAmazonFTLPolicyAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'name' => POLICY_NAME_FORMAT_INCORRECT,//策略名称格式错误
            'description' => POLICY_DESCRIPTION_FORMAT_INCORRECT,//策略描述格式错误
            'content' => POLICY_CONTENT_FORMAT_INCORRECT,//策略内容格式错误
        ),
        100002 => array(
            'amazonFTLPolicy' => AMAZONFTLPOLICY_CAN_NOT_MODIFY,//亚马逊整板订单价格策略不能操作|403
        ),
        100004 => array(
            'publisher' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在|404
            'strategies' => STRATEGIES_NOT_EXISTS,//策略不存在|404
        )
    );
    
    const SCENARIOS = [
        'AMAZONFTL_POLICY_LIST'=>[
            'fields'=>[],
            'include' => ''
        ],
        'AMAZONFTL_POLICY_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new AmazonFTLPolicyRestfulTranslator(),
            'strategies/amazonFTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullAmazonFTLPolicy::getInstance();
    }

    protected function getNullTemplate() : INull
    {
        return NullTemplate::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getTemplateRestfulTranslator() : TemplateRestfulTranslator
    {
        return new TemplateRestfulTranslator();
    }

    protected function translateToTemplate()
    {
        return $this->getTemplateRestfulTranslator()->arrayToObject($this->getContents());
    }

    public function getTemplate()
    {
        $this->get('templates/price/amazonFTL');

        return $this->isSuccess() ? $this->translateToTemplate() : $this->getNullTemplate();
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'name',
            'description',
            'content',
            'publisher'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'name',
            'description',
            'content',
            'publisher'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function disableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function deletedTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }
}
