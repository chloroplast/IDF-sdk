<?php
namespace Sdk\Policy\Adapter\AmazonFTLPolicy;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\AmazonFTLPolicy;

class AmazonFTLPolicyMockAdapter implements IAmazonFTLPolicyAdapter
{
    use OperateAbleMockAdapterTrait, FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new AmazonFTLPolicy($id);
    }

    public function getTemplate()
    {
        return new Template();
    }
}
