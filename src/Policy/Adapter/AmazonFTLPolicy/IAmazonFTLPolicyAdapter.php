<?php
namespace Sdk\Policy\Adapter\AmazonFTLPolicy;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

interface IAmazonFTLPolicyAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function getTemplate();
}
