<?php
namespace Sdk\Policy\Adapter\OrderFTLPolicy;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

interface IOrderFTLPolicyAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function getTemplate();
}
