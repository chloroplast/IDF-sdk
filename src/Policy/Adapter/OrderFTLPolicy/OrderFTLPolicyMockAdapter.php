<?php
namespace Sdk\Policy\Adapter\OrderFTLPolicy;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\OrderFTLPolicy;

class OrderFTLPolicyMockAdapter implements IOrderFTLPolicyAdapter
{
    use OperateAbleMockAdapterTrait, FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new OrderFTLPolicy($id);
    }

    public function getTemplate()
    {
        return new Template();
    }
}
