<?php
namespace Sdk\Policy\Adapter\OrderFTLPolicy;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Policy\Model\NullTemplate;
use Sdk\Policy\Translator\TemplateRestfulTranslator;

use Sdk\Policy\Model\NullOrderFTLPolicy;
use Sdk\Policy\Translator\OrderFTLPolicyRestfulTranslator;

class OrderFTLPolicyRestfulAdapter extends CommonRestfulAdapter implements IOrderFTLPolicyAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'name' => POLICY_NAME_FORMAT_INCORRECT,//策略名称格式错误
            'description' => POLICY_DESCRIPTION_FORMAT_INCORRECT,//策略描述格式错误
            'content' => POLICY_CONTENT_FORMAT_INCORRECT,//策略内容格式错误
        ),
        100002 => array(
            'orderFTLPolicy' => ORDERFTLPOLICY_CAN_NOT_MODIFY,//非亚马逊整板订单价格策略不能操作|403
        ),
        100004 => array(
            'publisher' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在|404
            'strategies' => STRATEGIES_NOT_EXISTS,//策略不存在|404
        )
    );
    
    const SCENARIOS = [
        'ORDERFTL_POLICY_LIST'=>[
            'fields'=>[],
            'include' => ''
        ],
        'ORDERFTL_POLICY_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new OrderFTLPolicyRestfulTranslator(),
            'strategies/orderFTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullOrderFTLPolicy::getInstance();
    }

    protected function getNullTemplate() : INull
    {
        return NullTemplate::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getTemplateRestfulTranslator() : TemplateRestfulTranslator
    {
        return new TemplateRestfulTranslator();
    }

    protected function translateToTemplate()
    {
        return $this->getTemplateRestfulTranslator()->arrayToObject($this->getContents());
    }

    public function getTemplate()
    {
        $this->get('templates/price/orderFTL');

        return $this->isSuccess() ? $this->translateToTemplate() : $this->getNullTemplate();
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'name',
            'description',
            'content',
            'publisher'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'name',
            'description',
            'content',
            'publisher'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function disableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function deletedTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }
}
