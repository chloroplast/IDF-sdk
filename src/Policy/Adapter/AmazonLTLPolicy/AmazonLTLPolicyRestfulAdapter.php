<?php
namespace Sdk\Policy\Adapter\AmazonLTLPolicy;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Policy\Model\NullTemplate;
use Sdk\Policy\Translator\TemplateRestfulTranslator;

use Sdk\Policy\Model\NullAmazonLTLPolicy;
use Sdk\Policy\Translator\AmazonLTLPolicyRestfulTranslator;

use Sdk\Policy\Model\StrategyPriceReport;
use Sdk\Policy\Translator\StrategyPriceReportRestfulTranslator;

class AmazonLTLPolicyRestfulAdapter extends CommonRestfulAdapter implements IAmazonLTLPolicyAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'name' => POLICY_NAME_FORMAT_INCORRECT,//策略名称格式错误
            'description' => POLICY_DESCRIPTION_FORMAT_INCORRECT,//策略描述格式错误
            'selfOperatedWarehouse' => SELFOPERATED_WAREHOUSE_FORMAT_INCORRECT,//所属自营仓库格式错误
            'content' => POLICY_CONTENT_FORMAT_INCORRECT,//策略内容格式错误
        ),
        100002 => array(
            'amazonLTLPolicy' => AMAZONLTLPOLICY_CAN_NOT_MODIFY,//亚马逊散板订单价格策略不能操作|403
        ),
        100004 => array(
            'publisher' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在|404
            'selfOperatedWarehouse' => SELFOPERATEDWAREHOUSE_NOT_EXISTS,//自营仓库不存在|404
            'strategies' => STRATEGIES_NOT_EXISTS,//策略不存在|404
        ),
        100420 => array(
            'amazonLTLBasePriceStrategy' => AMAZONLTLBASEPRICESTRATEGY_NOT_FOUND,//亚马逊散板基础价格策略未匹配|404
            'dimensionPriceStrategy' => DIMENSIONPRICESTRATEGY_NOT_FOUND,//体积价格策略未匹配|404
            'gradePriceStrategy' => GRADEPRICESTRATEGY_NOT_FOUND,//vip等级价格策略未匹配|404
            'idfPickupPriceStrategy' => IDFPICKUPPRICESTRATEGY_NOT_FOUND,//idf取货价格策略未匹配|404
            'weightPriceStrategy' => WEIGHTPRICESTRATEGY_NOT_FOUND,//重量价格策略未匹配|404
            'maxPriceGroup' => MAXPRICEGROUP_NOT_FOUND,//货物属性(取高)策略未匹配|404
            'minPriceGroup' => MINPRICEGROUP_NOT_FOUND,//用户属性(取低)策略未匹配|404
            'sequenceGroup' => SEQUENCEGROUP_NOT_FOUND,//业务属性策略未匹配|404
        )
    );
    
    const SCENARIOS = [
        'AMAZONLTL_POLICY_LIST'=>[
            'fields'=>[],
            'include' => ''
        ],
        'AMAZONLTL_POLICY_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new AmazonLTLPolicyRestfulTranslator(),
            'strategies/amazonLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullAmazonLTLPolicy::getInstance();
    }

    protected function getNullTemplate() : INull
    {
        return NullTemplate::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function getTemplateRestfulTranslator() : TemplateRestfulTranslator
    {
        return new TemplateRestfulTranslator();
    }

    protected function translateToTemplate()
    {
        return $this->getTemplateRestfulTranslator()->arrayToObject($this->getContents());
    }

    public function getTemplate()
    {
        $this->get('templates/price/amazonLTL');

        return $this->isSuccess() ? $this->translateToTemplate() : $this->getNullTemplate();
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'name',
            'description',
            'content',
            'publisher',
            'selfOperatedWarehouse'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'name',
            'description',
            'content',
            'publisher',
            'selfOperatedWarehouse'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function disableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function deletedTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function getStrategyPriceReportRestfulTranslator() : StrategyPriceReportRestfulTranslator
    {
        return new StrategyPriceReportRestfulTranslator();
    }

    public function calculate(StrategyPriceReport $strategyPriceReport) : bool
    {
        $data = $this->getStrategyPriceReportRestfulTranslator()->objectToArray(
            $strategyPriceReport,
            array(
                "pickupWarehouse",
                "targetWarehouse",
                "palletNumber",
                "weight",
                "width",
                "length",
                "height",
                "idfPickup",
                "grade",
                "content",
                "staff"
            )
        );

        $this->post(
            $this->getResource().'/calculate',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->getStrategyPriceReportRestfulTranslator()->arrayToObject(
                $this->getContents(),
                $strategyPriceReport
            );
            return true;
        }

        return false;
    }
}
