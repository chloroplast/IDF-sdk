<?php
namespace Sdk\Policy\Adapter\AmazonLTLPolicy;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\AmazonLTLPolicy;
use Sdk\Policy\Model\StrategyPriceReport;

//use Sdk\Policy\Utils\MockObjectGenerate;

class AmazonLTLPolicyMockAdapter implements IAmazonLTLPolicyAdapter
{
    use OperateAbleMockAdapterTrait, FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new AmazonLTLPolicy($id);
       // return MockObjectGenerate::generateAmazonLTLPolicy($id);
    }

    public function getTemplate()
    {
        return new Template();
    }

    public function calculate(StrategyPriceReport $strategyPriceReport) : bool
    {
        unset($strategyPriceReport);
        return true;
    }
}
