<?php
namespace Sdk\Policy\Adapter\AmazonLTLPolicy;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Policy\Model\StrategyPriceReport;

interface IAmazonLTLPolicyAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function getTemplate();

    public function calculate(StrategyPriceReport $strategyPriceReport) : bool;
}
