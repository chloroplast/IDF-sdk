<?php
namespace Sdk\Policy\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\NullTemplate;

class TemplateRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $template = null)
    {
        if (empty($expression)) {
            return NullTemplate::getInstance();
        }

        if ($template == null) {
            $template = new Template();
        }

        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['template'])) {
            $template->setContent($attributes['template']);
        }

        return $template;
    }

    public function objectToArray($template, array $keys = array())
    {
        unset($template);
        unset($keys);
        return array();
    }
}
