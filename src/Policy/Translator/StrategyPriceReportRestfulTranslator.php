<?php
namespace Sdk\Policy\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Policy\Model\StrategyPriceReport;
use Sdk\Policy\Model\NullStrategyPriceReport;

class StrategyPriceReportRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $strategyPriceReport = null)
    {
        if (empty($expression)) {
            return NullStrategyPriceReport::getInstance();
        }

        if ($strategyPriceReport == null) {
            $strategyPriceReport = new StrategyPriceReport();
        }

        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($data['id'])) {
            $strategyPriceReport->setId($data['id']);
        }
        if (isset($attributes['price'])) {
            $strategyPriceReport->setPrice($attributes['price']);
        }
        if (isset($attributes['priceRemark'])) {
            $strategyPriceReport->setPriceRemark($attributes['priceRemark']);
        }

        return $strategyPriceReport;
    }

    public function objectToArray($strategyPriceReport, array $keys = array())
    {
        if (!$strategyPriceReport instanceof StrategyPriceReport) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                "pickupWarehouse",
                "targetWarehouse",
                "palletNumber",
                "weight",
                "width",
                "length",
                "height",
                "idfPickup",
                "grade",
                "content",
                "staff"
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'amazonLTLPolicyReport'
            )
        );

        $attributes = array();

        if (in_array('palletNumber', $keys)) {
            $attributes['palletNumber'] = $strategyPriceReport->getPalletNumber();
        }
        if (in_array('weight', $keys)) {
            $attributes['weight'] = $strategyPriceReport->getWeight();
        }
        if (in_array('width', $keys)) {
            $attributes['width'] = $strategyPriceReport->getWidth();
        }
        if (in_array('length', $keys)) {
            $attributes['length'] = $strategyPriceReport->getLength();
        }
        if (in_array('height', $keys)) {
            $attributes['height'] = $strategyPriceReport->getHeight();
        }
        if (in_array('idfPickup', $keys)) {
            $attributes['idfPickup'] = $strategyPriceReport->getIdfPickup();
        }
        if (in_array('grade', $keys)) {
            $attributes['grade'] = $strategyPriceReport->getGrade();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $strategyPriceReport->getContent();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('pickupWarehouse', $keys)) {
            $pickupWarehouseRelationships = array(
                'type' => 'warehouse',
                'id' => strval($strategyPriceReport->getPickupWarehouse()->getId())
            );

            $expression['data']['relationships']['pickupWarehouse']['data'] = $pickupWarehouseRelationships;
        }
        if (in_array('targetWarehouse', $keys)) {
            $targetWarehouseRelationships = array(
                'type' => 'warehouse',
                'id' => strval($strategyPriceReport->getTargetWarehouse()->getId())
            );

            $expression['data']['relationships']['targetWarehouse']['data'] = $targetWarehouseRelationships;
        }
        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        
        return $expression;
    }
}
