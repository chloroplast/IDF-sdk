<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Policy\Model\OrderLTLPolicy;
use Sdk\Policy\Model\NullOrderLTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderLTLPolicyTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullOrderLTLPolicy::getInstance();
    }

    public function objectToArray($orderLTLPolicy, array $keys = array())
    {
        if (!$orderLTLPolicy instanceof OrderLTLPolicy) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'content',
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($orderLTLPolicy->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $orderLTLPolicy->getName();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $orderLTLPolicy->getDescription();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $orderLTLPolicy->getContent();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($orderLTLPolicy->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $orderLTLPolicy->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone(
                'Y-m-d H:i',
                $orderLTLPolicy->getCreateTime()
            );
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $orderLTLPolicy->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone(
                'Y-m-d H:i',
                $orderLTLPolicy->getUpdateTime()
            );
        }

        return $expression;
    }
}
