<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Policy\Model\StrategyPriceReport;
use Sdk\Policy\Model\NullStrategyPriceReport;

class StrategyPriceReportTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullStrategyPriceReport::getInstance();
    }

    public function objectToArray($strategyPriceReport, array $keys = array())
    {
        if (!$strategyPriceReport instanceof StrategyPriceReport) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'price',
                'priceRemark',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($strategyPriceReport->getId());
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $strategyPriceReport->getPrice();
        }
        if (in_array('priceRemark', $keys)) {
            $expression['priceRemark'] = $strategyPriceReport->getPriceRemark();
        }

        return $expression;
    }
}
