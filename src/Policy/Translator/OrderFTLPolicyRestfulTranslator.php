<?php
namespace Sdk\Policy\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Policy\Model\OrderFTLPolicy;
use Sdk\Policy\Model\NullOrderFTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderFTLPolicyRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $orderFTLPolicy = null)
    {
        if (empty($expression)) {
            return NullOrderFTLPolicy::getInstance();
        }

        if ($orderFTLPolicy == null) {
            $orderFTLPolicy = new OrderFTLPolicy();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($data['id'])) {
            $orderFTLPolicy->setId($data['id']);
        }
        if (isset($attributes['name'])) {
            $orderFTLPolicy->setName($attributes['name']);
        }
        if (isset($attributes['description'])) {
            $orderFTLPolicy->setDescription($attributes['description']);
        }
        if (isset($attributes['content'])) {
            $orderFTLPolicy->setContent($attributes['content']);
        }
        if (isset($attributes['status'])) {
            $orderFTLPolicy->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $orderFTLPolicy->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $orderFTLPolicy->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $orderFTLPolicy->setUpdateTime($attributes['updateTime']);
        }

        return $orderFTLPolicy;
    }

    public function objectToArray($orderFTLPolicy, array $keys = array())
    {
        if (!$orderFTLPolicy instanceof OrderFTLPolicy) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'content',
                'staff',
                'publisher'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'orderFTLPolicies'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $orderFTLPolicy->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $orderFTLPolicy->getName();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $orderFTLPolicy->getDescription();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $orderFTLPolicy->getContent();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        if (in_array('publisher', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['publisher']['data'] = $staffRelationships;
        }
        
        return $expression;
    }
}
