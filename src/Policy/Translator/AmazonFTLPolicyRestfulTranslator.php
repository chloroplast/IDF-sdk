<?php
namespace Sdk\Policy\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Policy\Model\AmazonFTLPolicy;
use Sdk\Policy\Model\NullAmazonFTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AmazonFTLPolicyRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $amazonFTLPolicy = null)
    {
        if (empty($expression)) {
            return NullAmazonFTLPolicy::getInstance();
        }

        if ($amazonFTLPolicy == null) {
            $amazonFTLPolicy = new AmazonFTLPolicy();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($data['id'])) {
            $amazonFTLPolicy->setId($data['id']);
        }
        if (isset($attributes['name'])) {
            $amazonFTLPolicy->setName($attributes['name']);
        }
        if (isset($attributes['description'])) {
            $amazonFTLPolicy->setDescription($attributes['description']);
        }
        if (isset($attributes['content'])) {
            $amazonFTLPolicy->setContent($attributes['content']);
        }
        if (isset($attributes['status'])) {
            $amazonFTLPolicy->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $amazonFTLPolicy->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $amazonFTLPolicy->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $amazonFTLPolicy->setUpdateTime($attributes['updateTime']);
        }

        return $amazonFTLPolicy;
    }

    public function objectToArray($amazonFTLPolicy, array $keys = array())
    {
        if (!$amazonFTLPolicy instanceof AmazonFTLPolicy) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'content',
                'staff',
                'publisher'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'amazonFTLPolicies'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $amazonFTLPolicy->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $amazonFTLPolicy->getName();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $amazonFTLPolicy->getDescription();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $amazonFTLPolicy->getContent();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        if (in_array('publisher', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['publisher']['data'] = $staffRelationships;
        }
        
        return $expression;
    }
}
