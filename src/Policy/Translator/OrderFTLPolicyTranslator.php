<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Policy\Model\OrderFTLPolicy;
use Sdk\Policy\Model\NullOrderFTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderFTLPolicyTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullOrderFTLPolicy::getInstance();
    }

    public function objectToArray($orderFTLPolicy, array $keys = array())
    {
        if (!$orderFTLPolicy instanceof OrderFTLPolicy) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'content',
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($orderFTLPolicy->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $orderFTLPolicy->getName();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $orderFTLPolicy->getDescription();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $orderFTLPolicy->getContent();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($orderFTLPolicy->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $orderFTLPolicy->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone(
                'Y-m-d H:i',
                $orderFTLPolicy->getCreateTime()
            );
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $orderFTLPolicy->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone(
                'Y-m-d H:i',
                $orderFTLPolicy->getUpdateTime()
            );
        }

        return $expression;
    }
}
