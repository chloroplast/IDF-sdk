<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Policy\Model\AmazonFTLPolicy;
use Sdk\Policy\Model\NullAmazonFTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AmazonFTLPolicyTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullAmazonFTLPolicy::getInstance();
    }

    public function objectToArray($amazonFTLPolicy, array $keys = array())
    {
        if (!$amazonFTLPolicy instanceof AmazonFTLPolicy) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'content',
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($amazonFTLPolicy->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $amazonFTLPolicy->getName();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $amazonFTLPolicy->getDescription();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $amazonFTLPolicy->getContent();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($amazonFTLPolicy->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $amazonFTLPolicy->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone(
                'Y-m-d H:i',
                $amazonFTLPolicy->getCreateTime()
            );
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $amazonFTLPolicy->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone(
                'Y-m-d H:i',
                $amazonFTLPolicy->getUpdateTime()
            );
        }

        return $expression;
    }
}
