<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Policy\Model\AmazonLTLPolicy;
use Sdk\Policy\Model\NullAmazonLTLPolicy;

use Sdk\Warehouse\Translator\WarehouseTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AmazonLTLPolicyTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullAmazonLTLPolicy::getInstance();
    }
    
    protected function getWarehouseTranslator() : WarehouseTranslator
    {
        return new WarehouseTranslator();
    }

    public function objectToArray($amazonLTLPolicy, array $keys = array())
    {
        if (!$amazonLTLPolicy instanceof AmazonLTLPolicy) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'content',
                'selfOperatedWarehouse' => array('id', 'code'),
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($amazonLTLPolicy->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $amazonLTLPolicy->getName();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $amazonLTLPolicy->getDescription();
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $amazonLTLPolicy->getContent();
        }
        if (isset($keys['selfOperatedWarehouse'])) {
            $expression['selfOperatedWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $amazonLTLPolicy->getSelfOperatedWarehouse(),
                $keys['selfOperatedWarehouse']
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($amazonLTLPolicy->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $amazonLTLPolicy->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone(
                'Y-m-d H:i',
                $amazonLTLPolicy->getCreateTime()
            );
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $amazonLTLPolicy->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone(
                'Y-m-d H:i',
                $amazonLTLPolicy->getUpdateTime()
            );
        }

        return $expression;
    }
}
