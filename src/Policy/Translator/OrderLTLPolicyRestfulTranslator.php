<?php
namespace Sdk\Policy\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Policy\Model\OrderLTLPolicy;
use Sdk\Policy\Model\NullOrderLTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderLTLPolicyRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $orderLTLPolicy = null)
    {
        if (empty($expression)) {
            return NullOrderLTLPolicy::getInstance();
        }

        if ($orderLTLPolicy == null) {
            $orderLTLPolicy = new OrderLTLPolicy();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();

        if (isset($data['id'])) {
            $orderLTLPolicy->setId($data['id']);
        }
        if (isset($attributes['name'])) {
            $orderLTLPolicy->setName($attributes['name']);
        }
        if (isset($attributes['description'])) {
            $orderLTLPolicy->setDescription($attributes['description']);
        }
        if (isset($attributes['content'])) {
            $orderLTLPolicy->setContent($attributes['content']);
        }
        if (isset($attributes['status'])) {
            $orderLTLPolicy->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $orderLTLPolicy->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $orderLTLPolicy->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $orderLTLPolicy->setUpdateTime($attributes['updateTime']);
        }

        return $orderLTLPolicy;
    }

    public function objectToArray($orderLTLPolicy, array $keys = array())
    {
        if (!$orderLTLPolicy instanceof OrderLTLPolicy) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'content',
                'staff',
                'publisher'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'orderLTLPolicies'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $orderLTLPolicy->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $orderLTLPolicy->getName();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $orderLTLPolicy->getDescription();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $orderLTLPolicy->getContent();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        if (in_array('publisher', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['publisher']['data'] = $staffRelationships;
        }
        
        return $expression;
    }
}
