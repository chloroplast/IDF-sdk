<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\NullTemplate;

class TemplateTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullTemplate::getInstance();
    }

    public function objectToArray($template, array $keys = array())
    {
        if (!$template instanceof Template) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'template',
            );
        }

        $expression = array();

        if (in_array('template', $keys)) {
            $expression['template'] = $template->getContent();
        }

        return $expression;
    }
}
