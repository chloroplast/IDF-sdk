<?php
namespace Sdk\Policy\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Policy\Model\AmazonLTLPolicy;
use Sdk\Policy\Model\NullAmazonLTLPolicy;

use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AmazonLTLPolicyRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getWarehouseRestfulTranslator() : WarehouseRestfulTranslator
    {
        return new WarehouseRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $amazonLTLPolicy = null)
    {
        if (empty($expression)) {
            return NullAmazonLTLPolicy::getInstance();
        }

        if ($amazonLTLPolicy == null) {
            $amazonLTLPolicy = new AmazonLTLPolicy();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $amazonLTLPolicy->setId($data['id']);
        }
        if (isset($attributes['name'])) {
            $amazonLTLPolicy->setName($attributes['name']);
        }
        if (isset($attributes['description'])) {
            $amazonLTLPolicy->setDescription($attributes['description']);
        }
        if (isset($attributes['content'])) {
            $amazonLTLPolicy->setContent($attributes['content']);
        }
        if (isset($attributes['status'])) {
            $amazonLTLPolicy->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $amazonLTLPolicy->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $amazonLTLPolicy->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $amazonLTLPolicy->setUpdateTime($attributes['updateTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['selfOperatedWarehouse'])) {
            $warehouseArray = $this->relationshipFill($relationships['selfOperatedWarehouse'], $included);
            $selfOperatedWarehouse = $this->getWarehouseRestfulTranslator()->arrayToObject($warehouseArray);
            $amazonLTLPolicy->setSelfOperatedWarehouse($selfOperatedWarehouse);
        }

        return $amazonLTLPolicy;
    }

    public function objectToArray($amazonLTLPolicy, array $keys = array())
    {
        if (!$amazonLTLPolicy instanceof AmazonLTLPolicy) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'content',
                'selfOperatedWarehouse',
                'staff',
                'publisher'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'amazonLTLPolicies'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $amazonLTLPolicy->getId();
        }

        $attributes = array();

        if (in_array('name', $keys)) {
            $attributes['name'] = $amazonLTLPolicy->getName();
        }
        if (in_array('description', $keys)) {
            $attributes['description'] = $amazonLTLPolicy->getDescription();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $amazonLTLPolicy->getContent();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('selfOperatedWarehouse', $keys)) {
            $warehouseRelationships = array(
                'type' => 'warehouse',
                'id' => strval($amazonLTLPolicy->getSelfOperatedWarehouse()->getId())
            );

            $expression['data']['relationships']['selfOperatedWarehouse']['data'] = $warehouseRelationships;
        }
        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        if (in_array('publisher', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['publisher']['data'] = $staffRelationships;
        }
        
        return $expression;
    }
}
