<?php
namespace Sdk\Policy\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Policy\Adapter\OrderFTLPolicy\IOrderFTLPolicyAdapter;
use Sdk\Policy\Adapter\OrderFTLPolicy\OrderFTLPolicyMockAdapter;
use Sdk\Policy\Adapter\OrderFTLPolicy\OrderFTLPolicyRestfulAdapter;

class OrderFTLPolicyRepository extends CommonRepository implements IOrderFTLPolicyAdapter
{
    const LIST_MODEL_UN = 'ORDERFTL_POLICY_LIST';
    const FETCH_ONE_MODEL_UN = 'ORDERFTL_POLICY_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new OrderFTLPolicyRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new OrderFTLPolicyMockAdapter()
        );
    }

    public function getTemplate()
    {
        return $this->getAdapter()->getTemplate();
    }
}
