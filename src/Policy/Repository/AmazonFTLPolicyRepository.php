<?php
namespace Sdk\Policy\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Policy\Adapter\AmazonFTLPolicy\IAmazonFTLPolicyAdapter;
use Sdk\Policy\Adapter\AmazonFTLPolicy\AmazonFTLPolicyMockAdapter;
use Sdk\Policy\Adapter\AmazonFTLPolicy\AmazonFTLPolicyRestfulAdapter;

class AmazonFTLPolicyRepository extends CommonRepository implements IAmazonFTLPolicyAdapter
{
    const LIST_MODEL_UN = 'AMAZONFTL_POLICY_LIST';
    const FETCH_ONE_MODEL_UN = 'AMAZONFTL_POLICY_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new AmazonFTLPolicyRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new AmazonFTLPolicyMockAdapter()
        );
    }

    public function getTemplate()
    {
        return $this->getAdapter()->getTemplate();
    }
}
