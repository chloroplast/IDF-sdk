<?php
namespace Sdk\Policy\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Policy\Model\StrategyPriceReport;
use Sdk\Policy\Adapter\AmazonLTLPolicy\IAmazonLTLPolicyAdapter;
use Sdk\Policy\Adapter\AmazonLTLPolicy\AmazonLTLPolicyMockAdapter;
use Sdk\Policy\Adapter\AmazonLTLPolicy\AmazonLTLPolicyRestfulAdapter;

class AmazonLTLPolicyRepository extends CommonRepository implements IAmazonLTLPolicyAdapter
{
    const LIST_MODEL_UN = 'AMAZONLTL_POLICY_LIST';
    const FETCH_ONE_MODEL_UN = 'AMAZONLTL_POLICY_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new AmazonLTLPolicyRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new AmazonLTLPolicyMockAdapter()
        );
    }

    public function getTemplate()
    {
        return $this->getAdapter()->getTemplate();
    }

    public function calculate(StrategyPriceReport $strategyPriceReport) : bool
    {
        return $this->getAdapter()->calculate($strategyPriceReport);
    }
}
