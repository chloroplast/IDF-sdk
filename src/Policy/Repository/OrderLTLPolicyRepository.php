<?php
namespace Sdk\Policy\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Policy\Adapter\OrderLTLPolicy\IOrderLTLPolicyAdapter;
use Sdk\Policy\Adapter\OrderLTLPolicy\OrderLTLPolicyMockAdapter;
use Sdk\Policy\Adapter\OrderLTLPolicy\OrderLTLPolicyRestfulAdapter;

class OrderLTLPolicyRepository extends CommonRepository implements IOrderLTLPolicyAdapter
{
    const LIST_MODEL_UN = 'ORDERLTL_POLICY_LIST';
    const FETCH_ONE_MODEL_UN = 'ORDERLTL_POLICY_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new OrderLTLPolicyRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new OrderLTLPolicyMockAdapter()
        );
    }

    public function getTemplate()
    {
        return $this->getAdapter()->getTemplate();
    }
}
