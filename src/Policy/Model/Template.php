<?php
namespace Sdk\Policy\Model;

class Template
{
    /**
     * @var array $content
     */
    private $content;

    public function __construct()
    {
        $this->content = array();
    }

    public function __destruct()
    {
        unset($this->content);
    }

    public function setContent(array $content): void
    {
        $this->content = $content;
    }

    public function getContent(): array
    {
        return $this->content;
    }
}
