<?php
namespace Sdk\Policy\Model;

use Sdk\Warehouse\Model\Warehouse;

use Sdk\Policy\Repository\AmazonLTLPolicyRepository;

class StrategyPriceReport
{
    private $id;
    /**
     * @var Warehouse $pickupWarehouse
     */
    private $pickupWarehouse;
    /**
     * @var Warehouse $targetWarehouse
     */
    private $targetWarehouse;
    /**
     * @var int $palletNumber
     */
    private $palletNumber;
    /**
     * @var float $weight
     */
    private $weight;
    /**
     * @var float $width
     */
    private $width;
    /**
     * @var float $length
     */
    private $length;
    /**
     * @var float $height
     */
    private $height;
    /**
     * @var int $idfPickup
     */
    private $idfPickup;
    /**
     * @var int $grade
     */
    private $grade;
    /**
     * @var array $content
     */
    private $content;
    /**
     * @var float $price
     */
    private $price;
    /**
     * @var array $priceRemark
     */
    private $priceRemark;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->pickupWarehouse = new Warehouse();
        $this->targetWarehouse = new Warehouse();
        $this->palletNumber = 0;
        $this->weight = 0;
        $this->width = 0;
        $this->length = 0;
        $this->height = 0;
        $this->idfPickup = 0;
        $this->grade = 0;
        $this->content = array();
        $this->price = 0;
        $this->priceRemark = array();
        $this->repository = new AmazonLTLPolicyRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->pickupWarehouse);
        unset($this->targetWarehouse);
        unset($this->palletNumber);
        unset($this->weight);
        unset($this->width);
        unset($this->length);
        unset($this->height);
        unset($this->idfPickup);
        unset($this->grade);
        unset($this->content);
        unset($this->price);
        unset($this->priceRemark);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setPickupWarehouse(Warehouse $pickupWarehouse) : void
    {
        $this->pickupWarehouse = $pickupWarehouse;
    }

    public function getPickupWarehouse() : Warehouse
    {
        return $this->pickupWarehouse;
    }

    public function setTargetWarehouse(Warehouse $targetWarehouse) : void
    {
        $this->targetWarehouse = $targetWarehouse;
    }

    public function getTargetWarehouse() : Warehouse
    {
        return $this->targetWarehouse;
    }

    public function setPalletNumber(int $palletNumber) : void
    {
        $this->palletNumber = $palletNumber;
    }

    public function getPalletNumber() : int
    {
        return $this->palletNumber;
    }

    public function setWeight(float $weight) : void
    {
        $this->weight = $weight;
    }

    public function getWeight() : float
    {
        return $this->weight;
    }

    public function setWidth(float $width) : void
    {
        $this->width = $width;
    }

    public function getWidth() : float
    {
        return $this->width;
    }

    public function setLength(float $length) : void
    {
        $this->length = $length;
    }

    public function getLength() : float
    {
        return $this->length;
    }

    public function setHeight(float $height) : void
    {
        $this->height = $height;
    }

    public function getHeight() : float
    {
        return $this->height;
    }

    public function setIdfPickup(int $idfPickup) : void
    {
        $this->idfPickup = $idfPickup;
    }

    public function getIdfPickup() : int
    {
        return $this->idfPickup;
    }

    public function setGrade(int $grade) : void
    {
        $this->grade = $grade;
    }

    public function getGrade() : int
    {
        return $this->grade;
    }

    public function setContent(array $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : array
    {
        return $this->content;
    }

    public function setPrice(float $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function setPriceRemark(array $priceRemark) : void
    {
        $this->priceRemark = $priceRemark;
    }

    public function getPriceRemark() : array
    {
        return $this->priceRemark;
    }

    protected function getRepository() : AmazonLTLPolicyRepository
    {
        return $this->repository;
    }

    public function calculate() : bool
    {
        return $this->getRepository()->calculate($this);
    }
}
