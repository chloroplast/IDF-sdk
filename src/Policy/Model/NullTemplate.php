<?php
namespace Sdk\Policy\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullTemplate extends Template implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }
}
