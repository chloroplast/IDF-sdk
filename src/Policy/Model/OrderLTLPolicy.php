<?php
namespace Sdk\Policy\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\Policy\Repository\OrderLTLPolicyRepository;

class OrderLTLPolicy implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    private $id;
    /**
     * @var string $name 策略名称
     */
    private $name;
    /**
     * @var string $description 策略描述
     */
    private $description;
    /**
     * @var array $content 策略内容
     */
    private $content;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id  : 0;
        $this->name = '';
        $this->description = '';
        $this->content = array();
        $this->status = self::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new OrderLTLPolicyRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->description);
        unset($this->content);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setName(string $name) : void
    {
        $this->name = $name;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function setDescription(string $description) : void
    {
        $this->description = $description;
    }

    public function getDescription() : string
    {
        return $this->description;
    }

    public function setContent(array $content) : void
    {
        $this->content = $content;
    }

    public function getContent() : array
    {
        return $this->content;
    }

    protected function getRepository() : OrderLTLPolicyRepository
    {
        return $this->repository;
    }
}
