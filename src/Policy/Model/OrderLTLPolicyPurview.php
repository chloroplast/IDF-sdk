<?php
namespace Sdk\Policy\Model;

use Sdk\Role\Purview\Model\Purview;
use Sdk\Role\Purview\Model\IPurviewAble;

class OrderLTLPolicyPurview extends Purview
{

    public function __construct()
    {
        parent::__construct(IPurviewAble::COLUMN['ORDERLTL_POLICY']);
    }

    public function add() : bool
    {
        return $this->operation('add');
    }

    public function edit() : bool
    {
        return $this->operation('edit');
    }

    public function enable() : bool
    {
        return $this->operation('enable');
    }

    public function disable() : bool
    {
        return $this->operation('disable');
    }

    public function deleted() : bool
    {
        return $this->operation('deleted');
    }
}
