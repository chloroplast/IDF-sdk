<?php
namespace Sdk\Policy\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

class PolicyWidgetRule
{
    const NAME_MIN_LENGTH = 1;
    const NAME_MAX_LENGTH = 20;
    //验证策略名称长度：1-20个字符
    public function name($name) : bool
    {
        if (!V::stringType()->length(
            self::NAME_MIN_LENGTH,
            self::NAME_MAX_LENGTH
        )->validate($name)) {
            Core::setLastError(POLICY_NAME_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const DESCRIPTION_MIN_LENGTH = 0;
    const DESCRIPTION_MAX_LENGTH = 200;
    //验证策略名称长度：1-20个字符
    public function description($description) : bool
    {
        if (!V::stringType()->length(
            self::DESCRIPTION_MIN_LENGTH,
            self::DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(POLICY_DESCRIPTION_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证所属自营仓库：整数
    public function selfOperatedWarehouse($selfOperatedWarehouse) : bool
    {
        if (!V::numeric()->validate($selfOperatedWarehouse)) {
            Core::setLastError(SELFOPERATED_WAREHOUSE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证策略内容格式为数组
    public function content($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(POLICY_CONTENT_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }
}
