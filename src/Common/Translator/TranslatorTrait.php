<?php
namespace Sdk\Common\Translator;

use Marmot\Interfaces\INull;
use Sdk\Common\Model\Interfaces\IOperateAble;

trait TranslatorTrait
{
    abstract protected function getNullObject() : INull;

    public function arrayToObject(array $expression, $object = null)
    {
        unset($object);
        unset($expression);
        return $this->getNullObject();
    }

    public function arrayToObjects(array $expression) : array
    {
        unset($expression);
        return array();
    }

    protected function typeFormatConversion(int $type, array $typeCnArray) : array
    {
        return array(
            'id' => marmot_encode($type),
            'name' => isset($typeCnArray[$type]) ? $typeCnArray[$type] : ''
        );
    }

    protected function statusFormatConversion(
        int $status,
        array $statusTypeArray = IOperateAble::STATUS_TYPE,
        array $statusCnArray = IOperateAble::STATUS_CN
    ) : array {
        return array(
            'id' => marmot_encode($status),
            'type' => isset($statusTypeArray[$status]) ? $statusTypeArray[$status] : '',
            'name' => isset($statusCnArray[$status]) ? $statusCnArray[$status] : ''
        );
    }

    protected function convertTimeZone(string $format, int $timestamp) : string
    {
        $headers = getallheaders();
        // 设置默认时区为东部时间（西五区时间）|美国/纽约
        $timeZone = isset($headers['Time-Zone']) ? $headers['Time-Zone'] : 2;

        if ($timeZone == 1) {
            // 设置时区为大西洋时间（西四区时间）|美国/波多黎各
            date_default_timezone_set("America/Puerto_Rico");
        }
        if ($timeZone == 2) {
            // 设置时区为东部时间（西五区时间）|美国/纽约
            date_default_timezone_set("America/New_York");
        }
        if ($timeZone == 3) {
            // 设置时区为中部时间（西六区时间）|美国/芝加哥
            date_default_timezone_set("America/Chicago");
        }
        if ($timeZone == 4) {
            // 设置时区为山地时间（西七区时间）|美国/丹佛
            date_default_timezone_set("America/Denver");
        }
        if ($timeZone == 5) {
            // 设置时区为太平洋时间（西八区时间）|美国/洛杉矶
            date_default_timezone_set("America/Los_Angeles");
        }
        if ($timeZone == 6) {
            // 设置时区为阿拉斯加时间（西九区时间）|美国/安克雷奇
            date_default_timezone_set("America/Anchorage");
        }
        if ($timeZone == 7) {
            // 设置时区为夏威夷时间（西十区时间）|太平洋/檀香山
            date_default_timezone_set("Pacific/Honolulu");
        }
        if ($timeZone == 8) {
            // 设置时区为美属萨摩亚时间（西十一区时间）|太平洋/帕果帕果
            date_default_timezone_set("Pacific/Pago_Pago");
        }
        if ($timeZone == 9) {
            // 设置时区为查莫罗时间（东十区时间）|太平洋/关岛
            date_default_timezone_set("Pacific/Guam");
        }

        // 转换为对应时区的日期字符串
        $timeFormat = date($format, $timestamp);
        return $timeFormat;
    }
}
