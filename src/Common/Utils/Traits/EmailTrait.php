<?php
namespace Sdk\Common\Utils\Traits;

use Sdk\Common\Utils\Email;
use Sdk\Common\WidgetRule\CommonWidgetRule;

trait EmailTrait
{
    protected function getEmail() : Email
    {
        return new Email();
    }

    protected function verificationFetchEmailCodeParameter(
        $email
    ) : bool {
        $commonWidgetRule = new CommonWidgetRule();

        return $commonWidgetRule->email($email);
    }
}
