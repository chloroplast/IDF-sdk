<?php
namespace Sdk\Common\Model\Interfaces;

interface IOperateAble
{
    const STATUS = array(
        'ENABLED' => 0,
        'DISABLED' => -2,
        'DELETED' => -4
    );

    const STATUS_CN = array(
        self::STATUS['ENABLED'] => '启用',
        self::STATUS['DISABLED'] => '禁用',
        self::STATUS['DELETED'] => '删除'
    );

    const STATUS_TYPE = array(
        self::STATUS['ENABLED'] => 'success',
        self::STATUS['DISABLED'] => 'danger',
        self::STATUS['DELETED'] => 'delete'
    );

    public function insert() : bool;
    
    public function update() : bool;

    public function enable() : bool;
    
    public function disable() : bool;

    public function deleted() : bool;
}
