<?php
namespace Sdk\Common\Adapter\Traits;

use Sdk\Common\Model\Interfaces\IOperateAble;

trait OperateAbleRestfulAdapterTrait
{
    abstract protected function getResource() : string;
    abstract protected function insertTranslatorKeys() : array;
    abstract protected function updateTranslatorKeys() : array;
    abstract protected function enableTranslatorKeys() : array;
    abstract protected function disableTranslatorKeys() : array;
    abstract protected function deletedTranslatorKeys() : array;

    public function insert(IOperateAble $operateAbleObject) : bool
    {
        $keys = $this->insertTranslatorKeys();
        $data = $this->getTranslator()->objectToArray($operateAbleObject, $keys);
        if (empty($data['data']['attributes'])) {
            unset($data['data']['attributes']);
        }

        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($operateAbleObject);
            return true;
        }

        return false;
    }

    public function update(IOperateAble $operateAbleObject) : bool
    {
        $keys = $this->updateTranslatorKeys();
        $data = $this->getTranslator()->objectToArray($operateAbleObject, $keys);
        if (empty($data['data']['attributes'])) {
            unset($data['data']['attributes']);
        }
        
        $this->patch(
            $this->getResource().'/'.$operateAbleObject->getId(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($operateAbleObject);
            return true;
        }

        return false;
    }

    public function enable(IOperateAble $operateAbleObject) : bool
    {
        $keys = $this->enableTranslatorKeys();
        $data = $this->getTranslator()->objectToArray($operateAbleObject, $keys);
        unset($data['data']['attributes']);

        $this->patch(
            $this->getResource().'/'.$operateAbleObject->getId().'/enable',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($operateAbleObject);
            return true;
        }

        return false;
    }

    public function disable(IOperateAble $operateAbleObject) : bool
    {
        $keys = $this->disableTranslatorKeys();
        $data = $this->getTranslator()->objectToArray($operateAbleObject, $keys);
        unset($data['data']['attributes']);

        $this->patch(
            $this->getResource().'/'.$operateAbleObject->getId().'/disable',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($operateAbleObject);
            return true;
        }

        return false;
    }

    public function deleted(IOperateAble $operateAbleObject) : bool
    {
        $keys = $this->deletedTranslatorKeys();
        $data = $this->getTranslator()->objectToArray($operateAbleObject, $keys);
        unset($data['data']['attributes']);

        $this->delete(
            $this->getResource().'/'.$operateAbleObject->getId(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($operateAbleObject);
            return true;
        }

        return false;
    }
}
