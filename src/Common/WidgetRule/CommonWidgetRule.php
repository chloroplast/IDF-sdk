<?php
namespace Sdk\Common\WidgetRule;

use Marmot\Core;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Respect\Validation\Validator as V;

class CommonWidgetRule
{
    //验证路由id为数字
    public function routeId($id) : bool
    {
        if (!V::numeric()->positive()->validate($id)) {
            Core::setLastError(ROUTE_NOT_EXIST);
            return false;
        }

        return true;
    }

    //验证数据格式为数组
    public function isArrayType($data, string $pointer = '') : bool
    {
        if (!V::arrayType()->validate($data)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    //验证数据格式为字符串
    public function isStringType($data, string $pointer = '') : bool
    {
        if (!V::stringType()->validate($data)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    //验证数据格式为数字
    public function isNumericType($data, string $pointer = '') : bool
    {
        if (!is_numeric($data)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }
        
        return true;
    }

    //验证按照邮箱规则验证
    public function email($email) : bool
    {
        $regex = '/^[A-Za-z0-9]+([-._][A-Za-z0-9]+)*@[A-Za-z0-9]+(-[A-Za-z0-9]+)*(\.[A-Za-z]{2,6}|[A-Za-z]{2,4}\.[A-Za-z]{2,3})$/';

        if (!preg_match($regex, $email)) {
            Core::setLastError(EMAIL_FORMAT_INCORRECT);
            return false;
        }
        
        return true;
    }

    //验证密码长度：8-30个字符，规则为：大小写字母+数字+特殊字符(!@#$%.&)
    public function password($password) : bool
    {
        $reg = '/^(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%.&])[\da-zA-Z!@#$%.&]{8,30}$/';

        if (!preg_match($reg, $password)) {
            Core::setLastError(PASSWORD_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    //验证旧密码长度：8-30个字符，规则为：大小写字母+数字+特殊字符(!@#$%.&)
    public function oldPassword($oldPassword) : bool
    {
        $reg = '/^(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%.&])[\da-zA-Z!@#$%.&]{8,30}$/';

        if (!preg_match($reg, $oldPassword)) {
            Core::setLastError(OLD_PASSWORD_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    //验证确认密码是否与密码一致
    public function confirmPassword($password, $confirmPassword) : bool
    {
        if ($password != $confirmPassword) {
            Core::setLastError(CONFIRM_PASSWORD_INCORRECT);
            return false;
        }

        return true;
    }

    const NAME_MIN_LENGTH = 1;
    const NAME_MAX_LENGTH = 200;
    //验证姓名长度：1-200个字符
    public function name($name) : bool
    {
        if (!V::stringType()->length(self::NAME_MIN_LENGTH, self::NAME_MAX_LENGTH)->validate($name)) {
            Core::setLastError(NAME_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    //验证电话长度：1-20个字符，规则为：支持数字、“-”的组合
    public function phone($phone) : bool
    {
        $regex = '/^[0-9-]{1,20}$/';

        if (!preg_match($regex, $phone)) {
            Core::setLastError(PHONE_FORMAT_INCORRECT);
            return false;
        }
        
        return true;
    }

    const TITLE_MIN_LENGTH = 1;
    const TITLE_MAX_LENGTH = 50;
    //验证标题长度：1-50个字符
    public function title($title) : bool
    {
        if (!V::stringType()->length(self::TITLE_MIN_LENGTH, self::TITLE_MAX_LENGTH)->validate($title)) {
            Core::setLastError(TITLE_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const CONTENT_MIN_LENGTH = 1;
    const CONTENT_MAX_LENGTH = 50;
    //验证内容长度：1-200个字符
    public function content($content) : bool
    {
        if (!V::stringType()->length(self::CONTENT_MIN_LENGTH, self::CONTENT_MAX_LENGTH)->validate($content)) {
            Core::setLastError(CONTENT_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const ENTERPRISE_NAME_MIN_LENGTH = 1;
    const ENTERPRISE_NAME_MAX_LENGTH = 200;
    //验证企业名称长度：1-200个字符
    public function enterpriseName($enterpriseName) : bool
    {
        if (!V::stringType()->length(
            self::ENTERPRISE_NAME_MIN_LENGTH,
            self::ENTERPRISE_NAME_MAX_LENGTH
        )->validate($enterpriseName)) {
            Core::setLastError(ENTERPRISE_NAME_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const ENTERPRISE_ADDRESS_MIN_LENGTH = 1;
    const ENTERPRISE_ADDRESS_MAX_LENGTH = 200;
    //验证企业地址长度：1-200个字符
    public function enterpriseAddress($enterpriseAddress) : bool
    {
        if (!V::stringType()->length(
            self::ENTERPRISE_ADDRESS_MIN_LENGTH,
            self::ENTERPRISE_ADDRESS_MAX_LENGTH
        )->validate($enterpriseAddress)) {
            Core::setLastError(ENTERPRISE_ADDRESS_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    //验证账户名称长度：4-20个字符，规则为：支持中文、英文、数字、“-”、“_”的组合
    public function userName($userName) : bool
    {
        $regex = '/^[a-zA-Z0-9-_\x{4e00}-\x{9fa5}]{4,20}$/u';

        if (!preg_match($regex, $userName)) {
            Core::setLastError(USER_NAME_FORMAT_INCORRECT);
            return false;
        }
        
        return true;
    }

    const ADDRESS_MIN_LENGTH = 1;
    const ADDRESS_MAX_LENGTH = 200;
    //验证地址长度：1-200个字符
    public function address($address, string $pointer = '') : bool
    {
        if (!V::stringType()->length(
            self::ADDRESS_MIN_LENGTH,
            self::ADDRESS_MAX_LENGTH
        )->validate($address)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => $pointer));
            return false;
        }

        return true;
    }

    const POSTALCODE_MIN_LENGTH = 1;
    const POSTALCODE_MAX_LENGTH = 50;
    //验证邮编长度：1-50个字符
    public function postalCode($postalCode) : bool
    {
        if (!V::stringType()->length(
            self::POSTALCODE_MIN_LENGTH,
            self::POSTALCODE_MAX_LENGTH
        )->validate($postalCode)) {
            Core::setLastError(PARAMETER_FORMAT_ERROR, array('pointer' => 'postalCode'));
            return false;
        }

        return true;
    }

    const STATE_MIN_LENGTH = 1;
    const STATE_MAX_LENGTH = 50;
    //验证州长度：1-50个字符
    public function state($state) : bool
    {
        if (!V::stringType()->length(
            self::STATE_MIN_LENGTH,
            self::STATE_MAX_LENGTH
        )->validate($state)) {
            Core::setLastError(STATE_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const CITY_MIN_LENGTH = 1;
    const CITY_MAX_LENGTH = 50;
    //验证市长度：1-50个字符
    public function city($city) : bool
    {
        if (!V::stringType()->length(
            self::CITY_MIN_LENGTH,
            self::CITY_MAX_LENGTH
        )->validate($city)) {
            Core::setLastError(CITY_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }
}
