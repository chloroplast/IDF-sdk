<?php
namespace Sdk\Role\Adapter\Role;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Role\Model\NullRole;
use Sdk\Role\Translator\RoleRestfulTranslator;

class RoleRestfulAdapter extends CommonRestfulAdapter implements IRoleAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'name' => ROLE_NAME_FORMAT_INCORRECT,
            'purview' => ROLE_PURVIEW_FORMAT_INCORRECT
        ),
        100002 => array(
            'role' => RESOURCE_CAN_NOT_MODIFY
        ),
        100003 => array(
            'name' => ROLE_NAME_EXISTS
        ),
        100200 => array(
            'purview' => ROLE_PURVIEW_FORMAT_INCORRECT
        )
    );
    
    const SCENARIOS = [
        'ROLE_LIST'=>[
            'fields' => [
                'roles'=>'name,status,createTime,updateTime',
            ],
            'include' => ''
        ],
        'ROLE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new RoleRestfulTranslator(),
            'roles',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullRole::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'name',
            'purview',
            'staff'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'name',
            'purview',
            'staff'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function disableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function deletedTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }
}
