<?php
namespace Sdk\Role\WidgetRule;

use Marmot\Core;

use Respect\Validation\Validator as V;

use Sdk\Role\Purview\Model\IPurviewAble;

class RoleWidgetRule
{

    const NAME_MIN_LENGTH = 1;
    const NAME_MAX_LENGTH = 200;
    //验证角色名称长度：1-200个字符
    public function name($name) : bool
    {
        if (!V::stringType()->length(self::NAME_MIN_LENGTH, self::NAME_MAX_LENGTH)->validate($name)) {
            Core::setLastError(ROLE_NAME_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    public function purview($purview) : bool
    {
        if (!V::arrayType()->validate($purview)) {
            Core::setLastError(ROLE_PURVIEW_FORMAT_INCORRECT);
            return false;
        }

        foreach ($purview as $each) {
            if (!isset($each['id']) || !isset($each['actions'])) {
                Core::setLastError(ROLE_PURVIEW_FORMAT_INCORRECT);
                return false;
            }
            $id = $each['id'];
            $actions = $each['actions'];

            if (!is_numeric($id) || !is_numeric($actions)) {
                Core::setLastError(ROLE_PURVIEW_FORMAT_INCORRECT);
                return false;
            }

            if (!in_array($id, IPurviewAble::COLUMN)) {
                Core::setLastError(ROLE_PURVIEW_FORMAT_INCORRECT);
                return false;
            }
        }

        return true;
    }
}
