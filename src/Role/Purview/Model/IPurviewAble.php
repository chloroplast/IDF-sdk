<?php
namespace Sdk\Role\Purview\Model;

interface IPurviewAble
{
    /**
     * 是否需要权限限制
     */
    const PURVIEW_SCENE = array(
        'YES' => 0,
        'NO' => -2,
    );
    /**
     * 导航栏目
     * ROLE 权限管理
     * STAFF 后台用户管理
     * DICTIONARY 字典管理
     * WAREHOUSE 仓库管理
     * APPLICATION_LOG 日志管理
     * APPLICATION_LOG_STATISTICS 日志管理统计分析
     * MESSAGE 消息管理
     * VIP VIP管理
     * MEMBER 客户管理
     * CARTYPE 车辆类型管理
     * AMAZONLTL 亚马逊散板
     * RECEIVE_ORDER 取货组单
     * DISPATCH_ORDER 出库组单
     * AMAZONLTL_POLICY 亚马逊散板价格策略
     * EXPORT_DATA_TASK 数据导出
     * RECORD_STATISTICS 统计分析（工作台、用户视角、订单视角、营收视角）
     * AMAZONFTL 亚马逊整板
     * DIS_AMAFTL_ORDER 亚马逊整板-发货订单
     * ORDERFTL 非亚马逊整板
     * DIS_ORDERFTL_ORDER 非亚马逊整板-发货订单
     * CUSTOMLIST 定制列表
     * ORDERLTL 非亚马逊散板
     * REC_ORDERLTL_ORDER 非亚马逊散板-取货组单
     * DIS_ORDERLTL_ORDER 非亚马逊散板-出库组单
     * AMAZONFTL_POLICY 亚马逊整板价格策略
     * ORDERLTL_POLICY 非亚马逊散板价格策略
     * ORDERFTL_POLICY 非亚马逊整板价格策略
     */
    const COLUMN = array(
        'ROLE' => 3,
        'STAFF' => 4,
        'DICTIONARY' => 5,
        'WAREHOUSE' => 6,
        'APPLICATION_LOG' => 7,
        // 'APPLICATION_LOG_STATISTICS' => 8,
        'MESSAGE' => 9,
        'VIP' => 10,
        'MEMBER' => 11,
        'CARTYPE' => 12,
        'AMAZONLTL' => 13,
        'RECEIVE_ORDER' => 14,
        'DISPATCH_ORDER' => 15,
        'AMAZONLTL_POLICY' => 16,
        'EXPORT_DATA_TASK' => 17,
        'RECORD_STATISTICS' => 18,
        'AMAZONFTL' => 19,
        'DIS_AMAFTL_ORDER' => 20,
        'ORDERFTL' => 21,
        'DIS_ORDERFTL_ORDER' => 22,
        'CUSTOMLIST' => 23,
        'ORDERLTL' => 24,
        'REC_ORDERLTL_ORDER' => 25,
        'DIS_ORDERLTL_ORDER' => 26,
        'AMAZONFTL_POLICY' => 27,
        'ORDERLTL_POLICY' => 28,
        'ORDERFTL_POLICY' => 29,
    );

    /**
     * 操作
     * 通用操作: add|edit=>1, enable|disable|deleted=>2, 4 预留
     */
    const ACTIONS = array(
        self::COLUMN['ROLE'] => array('add' => 1, 'edit' => 1, 'enable' => 2, 'disable' => 2, 'deleted' => 2),
        self::COLUMN['STAFF'] => array('add' => 1, 'edit' => 1, 'enable' => 2, 'disable' => 2, 'resetPassword' => 8),
        self::COLUMN['DICTIONARY'] => array('add' => 1, 'edit' => 1, 'enable' => 2, 'disable' => 2, 'deleted' => 2),
        self::COLUMN['WAREHOUSE'] => array('add' => 1, 'edit' => 1, 'enable' => 2, 'disable' => 2, 'deleted' => 2),
        self::COLUMN['APPLICATION_LOG'] => array('export' => 4),
        // self::COLUMN['APPLICATION_LOG_STATISTICS'] => array(),
        self::COLUMN['MESSAGE'] => array('add' => 1),
        self::COLUMN['VIP'] => array('edit' => 1),
        self::COLUMN['MEMBER'] => array('enable' => 2, 'disable' => 2, 'active' => 8),
        self::COLUMN['CARTYPE'] => array('add' => 1, 'edit' => 1, 'enable' => 2, 'disable' => 2),
        self::COLUMN['AMAZONLTL'] => array(
            'add' => 1,
            'edit' => 1,
            'staffCancel' => 4,
            'confirm' => 4,
            'batchConfirm' => 4,
            'registerPosition' => 4,
            'freeze' => 8,
            'unFreeze' => 8
        ),
        self::COLUMN['RECEIVE_ORDER'] => array(
            'add' => 1, 'edit' => 1, 'registerPickupDate' => 4, 'pickup' => 4, 'stockIn' => 4
        ),
        self::COLUMN['DISPATCH_ORDER'] => array(
            'add' => 1, 'edit' => 1, 'updateISA' => 4, 'registerDispatchDate' => 4, 'dispatch' => 4, 'accept' => 4
        ),
        self::COLUMN['AMAZONLTL_POLICY'] => array(
            'add' => 1, 'edit' => 1, 'enable' => 2, 'disable' => 2, 'deleted' => 2
        ),
        self::COLUMN['EXPORT_DATA_TASK'] => array(),
        self::COLUMN['RECORD_STATISTICS'] => array(),
        self::COLUMN['AMAZONFTL'] => array(
            'add' => 1,
            'edit' => 1,
            'staffCancel' => 4,
            'confirm' => 4,
            'batchConfirm' => 4,
            'freeze' => 8,
            'unFreeze' => 8
        ),
        self::COLUMN['DIS_AMAFTL_ORDER'] => array(
            'updateISA' => 4, 'registerDispatchDate' => 4, 'dispatch' => 4, 'accept' => 4
        ),
        self::COLUMN['ORDERFTL'] => array(
            'add' => 1,
            'edit' => 1,
            'staffCancel' => 4,
            'confirm' => 4,
            'batchConfirm' => 4,
            'freeze' => 8,
            'unFreeze' => 8
        ),
        self::COLUMN['DIS_ORDERFTL_ORDER'] => array(
            'registerDispatchDate' => 4, 'dispatch' => 4, 'accept' => 4
        ),
        self::COLUMN['CUSTOMLIST'] => array(),
        self::COLUMN['ORDERLTL'] => array(
            'add' => 1,
            'edit' => 1,
            'staffCancel' => 4,
            'confirm' => 4,
            'batchConfirm' => 4,
            'registerPosition' => 4,
            'freeze' => 8,
            'unFreeze' => 8
        ),
        self::COLUMN['REC_ORDERLTL_ORDER'] => array(
            'add' => 1, 'edit' => 1, 'registerPickupDate' => 4, 'pickup' => 4, 'stockIn' => 4
        ),
        self::COLUMN['DIS_ORDERLTL_ORDER'] => array(
            'add' => 1, 'edit' => 1, 'registerDispatchDate' => 4, 'dispatch' => 4, 'accept' => 4
        ),
        self::COLUMN['AMAZONFTL_POLICY'] => array(
            'add' => 1, 'edit' => 1, 'enable' => 2, 'disable' => 2, 'deleted' => 2
        ),
        self::COLUMN['ORDERLTL_POLICY'] => array(
            'add' => 1, 'edit' => 1, 'enable' => 2, 'disable' => 2, 'deleted' => 2
        ),
        self::COLUMN['ORDERFTL_POLICY'] => array(
            'add' => 1, 'edit' => 1, 'enable' => 2, 'disable' => 2, 'deleted' => 2
        ),
    );

    public function getColumn() : int;
}
