<?php
namespace Sdk\Role\Purview\Model;

class PurviewFactory
{

    const MAPS = array(
        'r'=>'\Sdk\Role\Model\RolePurview',
        'u/s'=>'\Sdk\User\Staff\Model\StaffPurview',
        'dc'=>'\Sdk\Dictionary\Item\Model\DictionaryPurview',
        'dc/di'=>'\Sdk\Dictionary\Item\Model\DictionaryPurview',
        'wh'=>'\Sdk\Warehouse\Model\WarehousePurview',
        'l/a'=>'\Sdk\Log\ApplicationLog\Model\ApplicationLogPurview',
        's/la'=>'\Sdk\Log\ApplicationLog\Model\ApplicationLogPurview',
        'et'=>'\Sdk\Application\ExportDataTask\Model\ExportDataTaskPurview',
        'm'=>'\Sdk\Message\Model\MessagePurview',
        'm/im'=>'\Sdk\Message\Model\MessagePurview',
        'm/email'=>'\Sdk\Message\Model\MessagePurview',
        'vip'=>'\Sdk\Vip\Model\VipPurview',
        'u/m'=>'\Sdk\User\Member\Model\MemberPurview',
        'ct'=>'\Sdk\CarType\Model\CarTypePurview',
        'al'=>'\Sdk\Order\Model\AmazonLTLPurview',
        'osh'=>'\Sdk\Order\Model\AmazonLTLPurview',
        'ro/al'=>'\Sdk\Order\Model\ReceiveOrderPurview',
        'rso/al'=>'\Sdk\Order\Model\ReceiveOrderPurview',
        'do/al'=>'\Sdk\Order\Model\DispatchOrderPurview',
        'dso/al'=>'\Sdk\Order\Model\DispatchOrderPurview',
        'st/al'=>'\Sdk\Policy\Model\AmazonLTLPolicyPurview',
        'ad'=>'\Sdk\Order\Model\AmazonLTLPurview',
        's/w'=>'\Sdk\Statistics\Record\Model\RecordPurview',
        's/m'=>'\Sdk\Statistics\Record\Model\RecordPurview',
        's/o'=>'\Sdk\Statistics\Record\Model\RecordPurview',
        's/r'=>'\Sdk\Statistics\Record\Model\RecordPurview',
        'af'=>'\Sdk\Order\Model\AmazonFTLPurview',
        'do/af'=>'\Sdk\Order\Model\DisAmaFTLOrderPurview',
        'dso/af'=>'\Sdk\Order\Model\DisAmaFTLOrderPurview',
        'osh/af'=>'\Sdk\Order\Model\AmazonFTLPurview',
        'of'=>'\Sdk\Order\Model\OrderFTLPurview',
        'do/of'=>'\Sdk\Order\Model\DisOrderFTLOrderPurview',
        'dso/of'=>'\Sdk\Order\Model\DisOrderFTLOrderPurview',
        'osh/of'=>'\Sdk\Order\Model\OrderFTLPurview',
        'c/cl'=>'\Sdk\Configuration\Model\CustomListPurview',
        'ol'=>'\Sdk\Order\Model\OrderLTLPurview',
        'ro/ol'=>'\Sdk\Order\Model\RecOrderLTLOrderPurview',
        'rso/ol'=>'\Sdk\Order\Model\RecOrderLTLOrderPurview',
        'do/ol'=>'\Sdk\Order\Model\DisOrderLTLOrderPurview',
        'dso/ol'=>'\Sdk\Order\Model\DisOrderLTLOrderPurview',
        'osh/ol'=>'\Sdk\Order\Model\OrderLTLPurview',
        'st/af'=>'\Sdk\Policy\Model\AmazonFTLPolicyPurview',
        'st/ol'=>'\Sdk\Policy\Model\OrderLTLPolicyPurview',
        'st/of'=>'\Sdk\Policy\Model\OrderFTLPolicyPurview',
    );

    public static function getPurview(string $resource) : IPurviewAble
    {
        $model = isset(self::MAPS[$resource]) ? self::MAPS[$resource] : '';

        return class_exists($model) ? new $model : new NullPurview();
    }
}
