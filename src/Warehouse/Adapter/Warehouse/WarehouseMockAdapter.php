<?php
namespace Sdk\Warehouse\Adapter\Warehouse;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Warehouse\Utils\MockObjectGenerate;

class WarehouseMockAdapter implements IWarehouseAdapter
{
    use OperateAbleMockAdapterTrait, FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return MockObjectGenerate::generateWarehouse($id);
    }
}
