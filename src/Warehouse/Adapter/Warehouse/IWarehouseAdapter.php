<?php
namespace Sdk\Warehouse\Adapter\Warehouse;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

interface IWarehouseAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    
}
