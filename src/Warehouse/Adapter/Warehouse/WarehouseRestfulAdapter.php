<?php
namespace Sdk\Warehouse\Adapter\Warehouse;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Warehouse\Model\NullWarehouse;
use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;

class WarehouseRestfulAdapter extends CommonRestfulAdapter implements IWarehouseAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'contact' => WAREHOUSE_CONTACT_FORMAT_INCORRECT,
            'phone' => PHONE_FORMAT_INCORRECT,
            'keeper' => WAREHOUSE_KEEPER_FORMAT_INCORRECT,
            'country' => WAREHOUSE_COUNTRY_FORMAT_INCORRECT,
            'state' => STATE_FORMAT_INCORRECT,
            'city' => CITY_FORMAT_INCORRECT,
            'code' => WAREHOUSE_CODE_FORMAT_INCORRECT,
            'numberCode' => WAREHOUSE_NUMBERCODE_FORMAT_INCORRECT,
            'address' => WAREHOUSE_ADDRESS_FORMAT_INCORRECT,
            'postalCode' => WAREHOUSE_POSTALCODE_FORMAT_INCORRECT,
            'email' => EMAIL_FORMAT_INCORRECT,
            'selfOperated' => PARAMETER_FORMAT_ERROR
        ),
        100002 => array(
            'warehouse' => RESOURCE_CAN_NOT_MODIFY
        ),
        100003 => array(
            'warehouse' => WAREHOUSE_CODE_EXISTS
        ),
        100004 => array(
            'staff' => USER_IDENTITY_AUTHENTICATION_FAILED
        )
    );
    
    const SCENARIOS = [
        'WAREHOUSE_LIST'=>[
            'fields' => [
                'warehouses'=>'selfOperated,code,country,address,postalCode,status,createTime,updateTime',
            ],
            'include' => ''
        ],
        'WAREHOUSE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new WarehouseRestfulTranslator(),
            'warehouses',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullWarehouse::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'selfOperated',
            'contact',
            'phone',
            'keeper',
            'country',
            'state',
            'city',
            'code',
            'numberCode',
            'address',
            'postalCode',
            'email',
            'staff'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'selfOperated',
            'contact',
            'phone',
            'keeper',
            'country',
            'state',
            'city',
            'code',
            'numberCode',
            'address',
            'postalCode',
            'email',
            'staff'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function disableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function deletedTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }
}
