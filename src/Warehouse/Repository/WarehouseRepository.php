<?php
namespace Sdk\Warehouse\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Warehouse\Model\Warehouse;
use Sdk\Warehouse\Adapter\Warehouse\IWarehouseAdapter;
use Sdk\Warehouse\Adapter\Warehouse\WarehouseMockAdapter;
use Sdk\Warehouse\Adapter\Warehouse\WarehouseRestfulAdapter;

class WarehouseRepository extends CommonRepository implements IWarehouseAdapter
{
    const LIST_MODEL_UN = 'WAREHOUSE_LIST';
    const FETCH_ONE_MODEL_UN = 'WAREHOUSE_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new WarehouseRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new WarehouseMockAdapter()
        );
    }
}
