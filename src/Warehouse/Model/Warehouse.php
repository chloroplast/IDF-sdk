<?php
namespace Sdk\Warehouse\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\Warehouse\Repository\WarehouseRepository;

class Warehouse implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    const SELF_OPERATED = array(
        'NO' => 0,
        'YES' => 1
    );
    const SELF_OPERATED_CN = array(
        self::SELF_OPERATED['NO'] => '否',
        self::SELF_OPERATED['YES'] => '是'
    );
    const SELF_OPERATED_TYPE = array(
        self::SELF_OPERATED['NO'] => 'danger',
        self::SELF_OPERATED['YES'] => 'success'
    );

    private $id;
    /**
     * @var string $contact 仓库联系人
     */
    private $contact;
    /**
     * @var string $phone 联系电话
     */
    private $phone;
    /**
     * @var string $keeper 仓库管理员
     */
    private $keeper;
    /**
     * @var int $country 国家
     */
    private $country;
    /**
     * @var string $state 州
     */
    private $state;
    /**
     * @var string $city 市
     */
    private $city;
    /**
     * @var string $code 仓库代码
     */
    private $code;
    /**
     * @var string $numberCode 编号代码
     */
    private $numberCode;
    /**
     * @var string $address 仓库地址
     */
    private $address;
    /**
     * @var string $postalCode 邮编
     */
    private $postalCode;
    /**
     * @var string $email 邮箱
     */
    private $email;
    /**
     * @var int $selfOperated 是否自营
     */
    private $selfOperated;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->contact = '';
        $this->phone = '';
        $this->keeper = '';
        $this->country = 0;
        $this->state = '';
        $this->city = '';
        $this->code = '';
        $this->numberCode = '';
        $this->address = '';
        $this->postalCode = '';
        $this->email = '';
        $this->selfOperated = 0;
        $this->status = self::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new WarehouseRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->contact);
        unset($this->phone);
        unset($this->keeper);
        unset($this->country);
        unset($this->state);
        unset($this->city);
        unset($this->code);
        unset($this->numberCode);
        unset($this->address);
        unset($this->postalCode);
        unset($this->email);
        unset($this->selfOperated);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setContact(string $contact): void
    {
        $this->contact = $contact;
    }

    public function getContact(): string
    {
        return $this->contact;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setKeeper(string $keeper): void
    {
        $this->keeper = $keeper;
    }

    public function getKeeper(): string
    {
        return $this->keeper;
    }

    public function setCountry(int $country): void
    {
        $this->country = $country;
    }

    public function getCountry(): int
    {
        return $this->country;
    }

    public function setState(string $state): void
    {
        $this->state = $state;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setNumberCode(string $numberCode): void
    {
        $this->numberCode = $numberCode;
    }

    public function getNumberCode(): string
    {
        return $this->numberCode;
    }

    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setPostalCode(string $postalCode): void
    {
        $this->postalCode = $postalCode;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setSelfOperated(int $selfOperated): void
    {
        $this->selfOperated = $selfOperated;
    }

    public function getSelfOperated(): int
    {
        return $this->selfOperated;
    }

    protected function getRepository() : WarehouseRepository
    {
        return $this->repository;
    }
}
