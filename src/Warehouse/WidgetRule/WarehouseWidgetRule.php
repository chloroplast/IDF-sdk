<?php
namespace Sdk\Warehouse\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

class WarehouseWidgetRule
{

    const CONTACT_MIN_LENGTH = 1;
    const CONTACT_MAX_LENGTH = 200;
    //验证仓库联系人长度：1-200个字符
    public function contact($contact) : bool
    {
        if (!empty($contact) && !V::stringType()->length(
            self::CONTACT_MIN_LENGTH,
            self::CONTACT_MAX_LENGTH
        )->validate($contact)) {
            Core::setLastError(WAREHOUSE_CONTACT_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const KEEPER_MIN_LENGTH = 1;
    const KEEPER_MAX_LENGTH = 200;
    //验证仓库管理员长度：1-200个字符
    public function keeper($keeper) : bool
    {
        if (!empty($keeper) && !V::stringType()->length(
            self::KEEPER_MIN_LENGTH,
            self::KEEPER_MAX_LENGTH
        )->validate($keeper)) {
            Core::setLastError(WAREHOUSE_KEEPER_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    //验证国家为数字
    public function country($country) : bool
    {
        if (!V::numeric()->positive()->validate($country)) {
            Core::setLastError(WAREHOUSE_COUNTRY_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const CODE_MIN_LENGTH = 1;
    const CODE_MAX_LENGTH = 50;
    //验证仓库代码长度：1-50个字符
    public function code($code) : bool
    {
        if (!V::stringType()->length(
            self::CODE_MIN_LENGTH,
            self::CODE_MAX_LENGTH
        )->validate($code)) {
            Core::setLastError(WAREHOUSE_CODE_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const NUMBERCODE_MIN_LENGTH = 0;
    const NUMBERCODE_MAX_LENGTH = 50;
    //验证编号代码长度：1-50个字符
    public function numberCode($numberCode) : bool
    {
        if (!V::stringType()->length(
            self::NUMBERCODE_MIN_LENGTH,
            self::NUMBERCODE_MAX_LENGTH
        )->validate($numberCode)) {
            Core::setLastError(WAREHOUSE_NUMBERCODE_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const ADDRESS_MIN_LENGTH = 1;
    const ADDRESS_MAX_LENGTH = 200;
    //验证仓库地址长度：1-200个字符
    public function address($address) : bool
    {
        if (!V::stringType()->length(
            self::ADDRESS_MIN_LENGTH,
            self::ADDRESS_MAX_LENGTH
        )->validate($address)) {
            Core::setLastError(WAREHOUSE_ADDRESS_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const POSTALCODE_MIN_LENGTH = 1;
    const POSTALCODE_MAX_LENGTH = 50;
    //验证邮编长度：1-50个字符
    public function postalCode($postalCode) : bool
    {
        if (!empty($postalCode) && !V::stringType()->length(
            self::POSTALCODE_MIN_LENGTH,
            self::POSTALCODE_MAX_LENGTH
        )->validate($postalCode)) {
            Core::setLastError(WAREHOUSE_POSTALCODE_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }
}
