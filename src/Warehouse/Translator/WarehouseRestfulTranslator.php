<?php
namespace Sdk\Warehouse\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Warehouse\Model\Warehouse;
use Sdk\Warehouse\Model\NullWarehouse;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class WarehouseRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $warehouse = null)
    {
        if (empty($expression)) {
            return NullWarehouse::getInstance();
        }

        if ($warehouse == null) {
            $warehouse = new Warehouse();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($data['id'])) {
            $warehouse->setId($data['id']);
        }
        if (isset($attributes['contact'])) {
            $warehouse->setContact($attributes['contact']);
        }
        if (isset($attributes['phone'])) {
            $warehouse->setPhone($attributes['phone']);
        }
        if (isset($attributes['keeper'])) {
            $warehouse->setKeeper($attributes['keeper']);
        }
        if (isset($attributes['country'])) {
            $warehouse->setCountry($attributes['country']);
        }
        if (isset($attributes['state'])) {
            $warehouse->setState($attributes['state']);
        }
        if (isset($attributes['city'])) {
            $warehouse->setCity($attributes['city']);
        }
        if (isset($attributes['code'])) {
            $warehouse->setCode($attributes['code']);
        }
        if (isset($attributes['numberCode'])) {
            $warehouse->setNumberCode($attributes['numberCode']);
        }
        if (isset($attributes['address'])) {
            $warehouse->setAddress($attributes['address']);
        }
        if (isset($attributes['postalCode'])) {
            $warehouse->setPostalCode($attributes['postalCode']);
        }
        if (isset($attributes['email'])) {
            $warehouse->setEmail($attributes['email']);
        }
        if (isset($attributes['selfOperated'])) {
            $warehouse->setSelfOperated($attributes['selfOperated']);
        }
        if (isset($attributes['status'])) {
            $warehouse->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $warehouse->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $warehouse->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $warehouse->setUpdateTime($attributes['updateTime']);
        }

        return $warehouse;
    }

    public function objectToArray($warehouse, array $keys = array())
    {
        if (!$warehouse instanceof Warehouse) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'contact',
                'phone',
                'keeper',
                'country',
                'state',
                'city',
                'code',
                'numberCode',
                'address',
                'postalCode',
                'email',
                'selfOperated',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'warehouses'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $warehouse->getId();
        }

        $attributes = array();

        if (in_array('contact', $keys)) {
            $attributes['contact'] = $warehouse->getContact();
        }
        if (in_array('phone', $keys)) {
            $attributes['phone'] = $warehouse->getPhone();
        }
        if (in_array('keeper', $keys)) {
            $attributes['keeper'] = $warehouse->getKeeper();
        }
        if (in_array('country', $keys)) {
            $attributes['country'] = $warehouse->getCountry();
        }
        if (in_array('state', $keys)) {
            $attributes['state'] = $warehouse->getState();
        }
        if (in_array('city', $keys)) {
            $attributes['city'] = $warehouse->getCity();
        }
        if (in_array('code', $keys)) {
            $attributes['code'] = $warehouse->getCode();
        }
        if (in_array('numberCode', $keys)) {
            $attributes['numberCode'] = $warehouse->getNumberCode();
        }
        if (in_array('address', $keys)) {
            $attributes['address'] = $warehouse->getAddress();
        }
        if (in_array('postalCode', $keys)) {
            $attributes['postalCode'] = $warehouse->getPostalCode();
        }
        if (in_array('email', $keys)) {
            $attributes['email'] = $warehouse->getEmail();
        }
        if (in_array('selfOperated', $keys)) {
            $attributes['selfOperated'] = $warehouse->getSelfOperated();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        
        return $expression;
    }
}
