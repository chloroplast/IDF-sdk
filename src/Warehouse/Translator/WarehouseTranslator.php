<?php
namespace Sdk\Warehouse\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Warehouse\Model\Warehouse;
use Sdk\Warehouse\Model\NullWarehouse;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class WarehouseTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullWarehouse::getInstance();
    }

    public function arrayToObject(array $expression, $warehouse = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($warehouse == null) {
            $warehouse = new Warehouse();
        }
        
        if (isset($expression['id'])) {
            $warehouse->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['contact'])) {
            $warehouse->setContact($expression['contact']);
        }
        if (isset($expression['phone'])) {
            $warehouse->setPhone($expression['phone']);
        }
        if (isset($expression['keeper'])) {
            $warehouse->setKeeper($expression['keeper']);
        }
        if (isset($expression['country'])) {
            $warehouse->setCountry($expression['country']);
        }
        if (isset($expression['state'])) {
            $warehouse->setState($expression['state']);
        }
        if (isset($expression['city'])) {
            $warehouse->setCity($expression['city']);
        }
        if (isset($expression['code'])) {
            $warehouse->setCode($expression['code']);
        }
        if (isset($expression['numberCode'])) {
            $warehouse->setNumberCode($expression['numberCode']);
        }
        if (isset($expression['address'])) {
            $warehouse->setAddress($expression['address']);
        }
        if (isset($expression['postalCode'])) {
            $warehouse->setPostalCode($expression['postalCode']);
        }
        if (isset($expression['email'])) {
            $warehouse->setEmail($expression['email']);
        }
        if (isset($expression['selfOperated'])) {
            $warehouse->setSelfOperated($expression['selfOperated']);
        }
        if (isset($expression['status']['id'])) {
            $warehouse->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $warehouse->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $warehouse->setUpdateTime($expression['updateTime']);
        }

        return $warehouse;
    }

    public function objectToArray($warehouse, array $keys = array())
    {
        if (!$warehouse instanceof Warehouse) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'contact',
                'phone',
                'keeper',
                'country',
                'state',
                'city',
                'code',
                'numberCode',
                'address',
                'postalCode',
                'email',
                'selfOperated',
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($warehouse->getId());
        }
        if (in_array('contact', $keys)) {
            $expression['contact'] = $warehouse->getContact();
        }
        if (in_array('phone', $keys)) {
            $expression['phone'] = $warehouse->getPhone();
        }
        if (in_array('keeper', $keys)) {
            $expression['keeper'] = $warehouse->getKeeper();
        }
        if (in_array('country', $keys)) {
            $expression['country'] = $warehouse->getCountry();
        }
        if (in_array('state', $keys)) {
            $expression['state'] = $warehouse->getState();
        }
        if (in_array('city', $keys)) {
            $expression['city'] = $warehouse->getCity();
        }
        if (in_array('code', $keys)) {
            $expression['code'] = $warehouse->getCode();
        }
        if (in_array('numberCode', $keys)) {
            $expression['numberCode'] = $warehouse->getNumberCode();
        }
        if (in_array('address', $keys)) {
            $expression['address'] = $warehouse->getAddress();
        }
        if (in_array('postalCode', $keys)) {
            $expression['postalCode'] = $warehouse->getPostalCode();
        }
        if (in_array('email', $keys)) {
            $expression['email'] = $warehouse->getEmail();
        }
        if (in_array('selfOperated', $keys)) {
            $expression['selfOperated'] = $this->statusFormatConversion(
                $warehouse->getSelfOperated(),
                Warehouse::SELF_OPERATED_TYPE,
                Warehouse::SELF_OPERATED_CN
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($warehouse->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $warehouse->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $warehouse->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $warehouse->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $warehouse->getUpdateTime());
        }

        return $expression;
    }
}
