<?php
namespace Sdk\Configuration\Adapter\CustomList;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Configuration\Model\CustomList;

class CustomListMockAdapter implements ICustomListAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new CustomList($id);
    }

    public function save(CustomList $customList) : bool
    {
        unset($customList);
        return true;
    }
}
