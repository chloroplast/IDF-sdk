<?php
namespace Sdk\Configuration\Adapter\CustomList;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;

use Sdk\Configuration\Model\CustomList;

interface ICustomListAdapter extends IFetchAbleAdapter
{
    public function save(CustomList $customList) : bool;
}
