<?php
namespace Sdk\Configuration\WidgetRule;

use Marmot\Core;

use Respect\Validation\Validator as V;

class CustomListWidgetRule
{
    //验证标识数据格式为数字
    public function identify($identify) : bool
    {
        if (!is_numeric($identify)) {
            Core::setLastError(CUSTOMLIST_IDENTIFY_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证配置信息数据格式为数组
    public function content($content) : bool
    {
        if (!V::arrayType()->validate($content)) {
            Core::setLastError(CUSTOMLIST_CONTENT_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }
}
