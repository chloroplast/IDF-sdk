<?php
namespace Sdk\Configuration\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Configuration\Model\CustomList;
use Sdk\Configuration\Adapter\CustomList\ICustomListAdapter;
use Sdk\Configuration\Adapter\CustomList\CustomListMockAdapter;
use Sdk\Configuration\Adapter\CustomList\CustomListRestfulAdapter;

class CustomListRepository extends CommonRepository implements ICustomListAdapter
{
    const LIST_MODEL_UN = 'CUSTOMLIST_LIST';
    const FETCH_ONE_MODEL_UN = 'CUSTOMLIST_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new CustomListRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new CustomListMockAdapter()
        );
    }

    public function save(CustomList $customList) : bool
    {
        return $this->getAdapter()->save($customList);
    }
}
