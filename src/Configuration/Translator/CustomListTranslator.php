<?php
namespace Sdk\Configuration\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Configuration\Model\CustomList;
use Sdk\Configuration\Model\NullCustomList;

use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class CustomListTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullCustomList::getInstance();
    }

    public function arrayToObject(array $expression, $customList = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($customList == null) {
            $customList = new CustomList();
        }
        
        if (isset($expression['id'])) {
            $customList->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['category'])) {
            $customList->setCategory(marmot_decode($expression['category']));
        }
        if (isset($expression['identify'])) {
            $customList->setIdentify(marmot_decode($expression['identify']));
        }
        if (isset($expression['content'])) {
            $customList->setContent($expression['content']);
        }
        if (isset($expression['status']['id'])) {
            $customList->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $customList->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $customList->setUpdateTime($expression['updateTime']);
        }

        return $customList;
    }

    public function objectToArray($customList, array $keys = array())
    {
        if (!$customList instanceof CustomList) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'category',
                'identify',
                'content',
                'staff' => ['id', 'name'],
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($customList->getId());
        }
        if (in_array('category', $keys)) {
            $expression['category'] = marmot_encode($customList->getCategory());
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = marmot_encode($customList->getIdentify());
        }
        if (in_array('content', $keys)) {
            $expression['content'] = $customList->getContent();
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $customList->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($customList->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $customList->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $customList->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $customList->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $customList->getUpdateTime());
        }

        return $expression;
    }
}
