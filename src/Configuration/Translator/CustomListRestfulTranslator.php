<?php
namespace Sdk\Configuration\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Configuration\Model\CustomList;
use Sdk\Configuration\Model\NullCustomList;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class CustomListRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;

    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $customList = null)
    {
        if (empty($expression)) {
            return NullCustomList::getInstance();
        }

        if ($customList == null) {
            $customList = new CustomList();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $customList->setId($data['id']);
        }
        if (isset($attributes['category'])) {
            $customList->setCategory($attributes['category']);
        }
        if (isset($attributes['identify'])) {
            $customList->setIdentify($attributes['identify']);
        }
        if (isset($attributes['content'])) {
            $customList->setContent($attributes['content']);
        }
        if (isset($attributes['status'])) {
            $customList->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $customList->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $customList->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $customList->setUpdateTime($attributes['updateTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }
        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $customList->setStaff($staff);
        }

        return $customList;
    }

    public function objectToArray($customList, array $keys = array())
    {
        if (!$customList instanceof CustomList) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'category',
                'identify',
                'content',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'staffConfigurations'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $customList->getId();
        }

        $attributes = array();

        if (in_array('category', $keys)) {
            $attributes['category'] = $customList->getCategory();
        }
        if (in_array('identify', $keys)) {
            $attributes['identify'] = $customList->getIdentify();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $customList->getContent();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        
        return $expression;
    }
}
