<?php
namespace Sdk\Configuration\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\User\Staff\Model\Staff;

use Sdk\Configuration\Repository\CustomListRepository;

class CustomList implements IObject
{
    use Object;

    const DEFAULT_CATEGORY = 1;

    private $id;
    /**
     * @var int $category 类型
     */
    private $category;
    /**
     * @var int $identify 标识
     */
    private $identify;
    /**
     * @var array $content 配置信息
     */
    private $content;
    /**
     * @var Staff $staff 发布人
     */
    private $staff;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->category = self::DEFAULT_CATEGORY;
        $this->identify = 0;
        $this->content = array();
        $this->staff = new Staff();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new CustomListRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->category);
        unset($this->identify);
        unset($this->content);
        unset($this->staff);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): int
    {
        return $this->category;
    }

    public function setIdentify(int $identify): void
    {
        $this->identify = $identify;
    }

    public function getIdentify(): int
    {
        return $this->identify;
    }

    public function setContent(array $content): void
    {
        $this->content = $content;
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function setStaff(Staff $staff): void
    {
        $this->staff = $staff;
    }

    public function getStaff(): Staff
    {
        return $this->staff;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    protected function getRepository() : CustomListRepository
    {
        return $this->repository;
    }

    public function save() : bool
    {
        return $this->getRepository()->save($this);
    }
}
