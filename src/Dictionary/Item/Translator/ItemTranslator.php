<?php
namespace Sdk\Dictionary\Item\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Dictionary\Item\Model\Item;
use Sdk\Dictionary\Item\Model\NullItem;

use Sdk\Dictionary\Category\Translator\CategoryTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ItemTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getCategoryTranslator() : CategoryTranslator
    {
        return new CategoryTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullItem::getInstance();
    }
    
    public function objectToArray($item, array $keys = array())
    {
        if (!$item instanceof Item) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'name',
                'description',
                'category' => ['id', 'name'],
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($item->getId());
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $item->getName();
        }
        if (in_array('description', $keys)) {
            $expression['description'] = $item->getDescription();
        }
        if (isset($keys['category'])) {
            $expression['category'] = $this->getCategoryTranslator()->objectToArray(
                $item->getCategory(),
                $keys['category']
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($item->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $item->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $item->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $item->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $item->getUpdateTime());
        }

        return $expression;
    }
}
