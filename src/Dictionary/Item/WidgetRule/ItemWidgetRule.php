<?php
namespace Sdk\Dictionary\Item\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

class ItemWidgetRule
{

    const NAME_MIN_LENGTH = 1;
    const NAME_MAX_LENGTH = 50;
    //验证名称长度：1-50个字符
    public function name($name) : bool
    {
        if (!V::stringType()->length(self::NAME_MIN_LENGTH, self::NAME_MAX_LENGTH)->validate($name)) {
            Core::setLastError(DICTIONARY_NAME_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }

    const DESCRIPTION_MIN_LENGTH = 1;
    const DESCRIPTION_MAX_LENGTH = 200;
    //验证描述长度：1-200个字符
    public function description($description) : bool
    {
        if (!V::stringType()->length(
            self::DESCRIPTION_MIN_LENGTH,
            self::DESCRIPTION_MAX_LENGTH
        )->validate($description)) {
            Core::setLastError(DICTIONARY_DESCRIPTION_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }
}
