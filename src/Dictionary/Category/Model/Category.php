<?php
namespace Sdk\Dictionary\Category\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class Category implements IObject
{
    use Object;

    /**
     * 分类id
     * MESSAGE_TYPE 消息类型
     * MESSAGE_SCOPE 消息作用域
     */
    const ID = array(
        'MESSAGE_TYPE' => 1,
        'MESSAGE_SCOPE' => 2
    );

    private $id;
    /**
     * @var string $name 分类名称
     */
    private $name;
    /**
     * @var string $description 分类描述
     */
    private $description;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->description = '';
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->description);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}
