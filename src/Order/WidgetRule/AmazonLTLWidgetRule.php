<?php
namespace Sdk\Order\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Sdk\Order\Model\AmazonLTL;

class AmazonLTLWidgetRule
{
    const COMMON_STRING_MIN_LENGTH = 1;
    const COMMON_STRING_MAX_LENGTH = 20;

    //验证货物位置长度：1-20个字符
    public function position($position) : bool
    {
        if (!V::stringType()->length(
            self::COMMON_STRING_MIN_LENGTH,
            self::COMMON_STRING_MAX_LENGTH
        )->validate($position)) {
            Core::setLastError(POSITION_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证佐证材料格式： pdf|jpg|png|bmp|jpeg
    public function attachments($attachments) : bool
    {
        if (!V::arrayType()->validate($attachments)) {
            Core::setLastError(ATTACHMENTS_FORMAT_INCORRECT);
            return false;
        }

        foreach ($attachments as $attachment) {
            if (!$this->validateAttachmentExtension($attachment['address'])) {
                Core::setLastError(ATTACHMENTS_FORMAT_INCORRECT);
                return false;
            }
        }

        return true;
    }

    protected function validateAttachmentExtension($attachment) : bool
    {
        if (!V::extension('pdf')->validate($attachment) &&
            !V::extension('jpg')->validate($attachment) &&
            !V::extension('png')->validate($attachment) &&
            !V::extension('bmp')->validate($attachment) &&
            !V::extension('jpeg')->validate($attachment)
        ) {
            return false;
        }

        return true;
    }

    //验证ISA编号长度：1-20个字符
    public function isaNumber($isaNumber) : bool
    {
        if (!V::stringType()->length(
            self::COMMON_STRING_MIN_LENGTH,
            self::COMMON_STRING_MAX_LENGTH
        )->validate($isaNumber)) {
            Core::setLastError(ISA_NUMBER_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证取货仓库：整数
    public function pickupWarehouse($pickupWarehouse) : bool
    {
        if (!V::numeric()->validate($pickupWarehouse)) {
            Core::setLastError(PICKUP_WAREHOUSE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证地址信息：整数
    public function address($address) : bool
    {
        if (!V::numeric()->validate($address)) {
            Core::setLastError(ADDRESS_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const REFERENCES_MIN_LENGTH = 0;
    const REFERENCES_MAX_LENGTH = 100;
    //验证发货备注长度：0-100个字符
    public function references($references) : bool
    {
        if (!V::stringType()->length(
            self::REFERENCES_MIN_LENGTH,
            self::REFERENCES_MAX_LENGTH
        )->validate($references)) {
            Core::setLastError(REFERENCES_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证是否IDF取货范围：[0,1]
    public function idfPickup($idfPickup) : bool
    {
        if (!in_array($idfPickup, AmazonLTL::IDF_PICKUP)) {
            Core::setLastError(IDF_PICKUP_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const KEYS = array(
        "id",
        "warehouse",
        "palletNumber",
        "itemNumber",
        "weight",
        "weightUnit",
        "fbaNumber",
        "poNumber",
        "soNumber",
        "coNumber",
        "length",
        "width",
        "height"
    );
    //验证货物信息格式为数组，且对应字段都存在
    public function items($items) : bool
    {
        if (empty($items) || !V::arrayType()->validate($items)) {
            Core::setLastError(ITEMS_FORMAT_INCORRECT);
            return false;
        }

        foreach ($items as $item) {
            foreach ($item as $key => $value) {
                if (!in_array($key, self::KEYS)) {
                    Core::setLastError(ITEMS_FORMAT_INCORRECT);
                    return false;
                }
                if ($key == 'warehouse' && !$this->warehouse($value)) {
                    return false;
                }
                if ($key == 'palletNumber' && !$this->palletNumber($value)) {
                    return false;
                }
                if ($key == 'itemNumber' && !$this->itemNumber($value)) {
                    return false;
                }
                if ($key == 'weight' && !$this->weight($value)) {
                    return false;
                }
                if ($key == 'weightUnit' && !$this->weightUnit($value)) {
                    return false;
                }
                if ($key == 'fbaNumber' && !$this->fbaNumber($value)) {
                    return false;
                }
                if ($key == 'poNumber' && !$this->poNumber($value)) {
                    return false;
                }
                if ($key == 'soNumber' && !$this->soNumber($value)) {
                    return false;
                }
                if ($key == 'coNumber' && !$this->coNumber($value)) {
                    return false;
                }
                if ($key == 'length' && !$this->length($value)) {
                    return false;
                }
                if ($key == 'width' && !$this->width($value)) {
                    return false;
                }
                if ($key == 'height' && !$this->height($value)) {
                    return false;
                }
            }
        }
        return true;
    }

    //验证目标仓库：整数
    public function warehouse($warehouse) : bool
    {
        if (!V::numeric()->validate($warehouse)) {
            Core::setLastError(WAREHOUSE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const PALLET_NUMBER_MIN_VAL = 1;
    const PALLET_NUMBER_MAX_VAL = 999;
    //验证卡板数范围：1-999
    public function palletNumber($palletNumber) : bool
    {
        if (!V::intVal()->between(
            self::PALLET_NUMBER_MIN_VAL,
            self::PALLET_NUMBER_MAX_VAL
        )->validate($palletNumber)) {
            Core::setLastError(PALLET_NUMBER_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const ITEM_NUMBER_MIN_VAL = 1;
    const ITEM_NUMBER_MAX_VAL = 9999;
    //验证件数范围：1-9999
    public function itemNumber($itemNumber) : bool
    {
        if (!V::intVal()->between(
            self::ITEM_NUMBER_MIN_VAL,
            self::ITEM_NUMBER_MAX_VAL
        )->validate($itemNumber)) {
            Core::setLastError(ITEM_NUMBER_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证重量长度：8位浮点数字
    public function weight($weight) : bool
    {
        if (!V::floatType()->validate($weight)) {
            Core::setLastError(WEIGHT_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证重量单位范围：[1,2]
    public function weightUnit($weightUnit) : bool
    {
        if (!in_array($weightUnit, AmazonLTL::WEIGHT_UNIT)) {
            Core::setLastError(WEIGHT_UNIT_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const STRING_MAX_LENGTH_IS_TWO_HUNDRED = 200;
    //验证FBA编号长度：1-200个字符
    public function fbaNumber($fbaNumber) : bool
    {
        if (!V::stringType()->length(
            self::COMMON_STRING_MIN_LENGTH,
            self::STRING_MAX_LENGTH_IS_TWO_HUNDRED
        )->validate($fbaNumber)) {
            Core::setLastError(FBA_NUMBER_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证PO编号长度：1-200个字符
    public function poNumber($poNumber) : bool
    {
        if (!V::stringType()->length(
            self::COMMON_STRING_MIN_LENGTH,
            self::STRING_MAX_LENGTH_IS_TWO_HUNDRED
        )->validate($poNumber)) {
            Core::setLastError(PO_NUMBER_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const SO_NUMBER_MIN_LENGTH = 0;
    //验证SO编号长度：0-20个字符
    public function soNumber($soNumber) : bool
    {
        if (!V::stringType()->length(
            self::SO_NUMBER_MIN_LENGTH,
            self::COMMON_STRING_MAX_LENGTH
        )->validate($soNumber)) {
            Core::setLastError(SO_NUMBER_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const CO_NUMBER_MIN_LENGTH = 0;
    //验证CO编号长度：0-20个字符
    public function coNumber($coNumber) : bool
    {
        if (!empty($coNumber) && !V::stringType()->length(
            self::CO_NUMBER_MIN_LENGTH,
            self::COMMON_STRING_MAX_LENGTH
        )->validate($coNumber)) {
            Core::setLastError(CO_NUMBER_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    // 946656000 代表 2000-01-01 00:00:00
    // 4102415999 代表 2099-12-31 23:59:59
    const MIN_TIMESTAMP = 946656000;
    const MAX_TIMESTAMP = 4102415999;
    public function pickupDate($pickupDate) : bool
    {
        if (!V::intVal()->between(self::MIN_TIMESTAMP, self::MAX_TIMESTAMP)->validate($pickupDate)) {
            Core::setLastError(PICKUP_DATE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    public function readyTime($readyTime) : bool
    {
        if (!V::intVal()->between(self::MIN_TIMESTAMP, self::MAX_TIMESTAMP)->validate($readyTime)) {
            Core::setLastError(READY_TIME_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    public function closeTime($closeTime) : bool
    {
        if (!V::intVal()->between(self::MIN_TIMESTAMP, self::MAX_TIMESTAMP)->validate($closeTime)) {
            Core::setLastError(CLOSE_TIME_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证取货码长度：1-20个字符
    public function pickupNumber($pickupNumber) : bool
    {
        if (!empty($pickupNumber) && !V::stringType()->length(
            self::COMMON_STRING_MIN_LENGTH,
            self::COMMON_STRING_MAX_LENGTH
        )->validate($pickupNumber)) {
            Core::setLastError(PICKUP_NUMBER_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const REMARK_MIN_LENGTH = 1;
    const REMARK_MAX_LENGTH = 500;
    //验证备注信息长度：1-500个字符
    public function remark($remark) : bool
    {
        if (!empty($remark) && !V::stringType()->length(
            self::REMARK_MIN_LENGTH,
            self::REMARK_MAX_LENGTH
        )->validate($remark)) {
            Core::setLastError(REMARK_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const STOCKIN_NUMBER_MIN_LENGTH = 1;
    const STOCKIN_NUMBER_MAX_LENGTH = 100;
    //验证入库编号长度：1-100个字符
    public function stockInNumber($stockInNumber) : bool
    {
        if (!V::stringType()->length(
            self::STOCKIN_NUMBER_MIN_LENGTH,
            self::STOCKIN_NUMBER_MAX_LENGTH
        )->validate($stockInNumber)) {
            Core::setLastError(STOCKIN_NUMBER_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    public function stockInTime($stockInTime) : bool
    {
        if (!V::intVal()->between(self::MIN_TIMESTAMP, self::MAX_TIMESTAMP)->validate($stockInTime)) {
            Core::setLastError(STOCKIN_TIME_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证车辆类型：整数
    public function carType($carType) : bool
    {
        if (!V::numeric()->validate($carType)) {
            Core::setLastError(CARTYPE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证散板订单：数组
    public function amazonLTL($amazonLTL) : bool
    {
        if (!V::arrayType()->validate($amazonLTL)) {
            Core::setLastError(AMAZONLTL_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证散板订单佐证材料：数组，pdf|jpg|png|bmp|jpeg
    public function itemsAttachments($itemsAttachments) : bool
    {
        if (!V::arrayType()->validate($itemsAttachments)) {
            Core::setLastError(ITEMS_ATTACHMENTS_FORMAT_INCORRECT);
            return false;
        }

        foreach ($itemsAttachments as $itemsAttachment) {
            foreach ($itemsAttachment as $value) {
                if (!$this->validateAttachmentExtension($value['address'])) {
                    Core::setLastError(ITEMS_ATTACHMENTS_FORMAT_INCORRECT);
                    return false;
                }
            }
        }
        return true;
    }

    //验证长度长度：5位浮点数字
    public function length($length) : bool
    {
        if (!empty($length) && !V::floatType()->validate($length)) {
            Core::setLastError(LENGTH_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证宽度长度：5位浮点数字
    public function width($width) : bool
    {
        if (!empty($width) && !V::floatType()->validate($width)) {
            Core::setLastError(WIDTH_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证高度长度：5位浮点数字
    public function height($height) : bool
    {
        if (!empty($height) && !V::floatType()->validate($height)) {
            Core::setLastError(HEIGHT_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    public function deliveryDate($deliveryDate) : bool
    {
        if (!V::intVal()->between(self::MIN_TIMESTAMP, self::MAX_TIMESTAMP)->validate($deliveryDate)) {
            Core::setLastError(DELIVERY_DATE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证亚马逊凭证格式： pdf|jpg|png|bmp|jpeg
    public function closePage($closePage) : bool
    {
        if (!V::arrayType()->validate($closePage)) {
            Core::setLastError(CLOSE_PAGE_FORMAT_INCORRECT);
            return false;
        }

        foreach ($closePage as $attachment) {
            if (!$this->validateAttachmentExtension($attachment['address'])) {
                Core::setLastError(CLOSE_PAGE_FORMAT_INCORRECT);
                return false;
            }
        }

        return true;
    }

    //验证文件名格式： xlsx
    public function fileName($fileName) : bool
    {
        if (!V::stringType()->validate($fileName) || !V::extension('xlsx')->validate($fileName)) {
            Core::setLastError(FILE_NAME_FORMAT_INCORRECT);
            return false;
        }

        return true;
    }
}
