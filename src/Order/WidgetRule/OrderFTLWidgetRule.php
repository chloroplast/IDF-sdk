<?php
namespace Sdk\Order\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Sdk\Order\Model\OrderFTL;

class OrderFTLWidgetRule
{
    // 946656000 代表 2000-01-01 00:00:00
    // 4102415999 代表 2099-12-31 23:59:59
    const MIN_TIMESTAMP = 946656000;
    const MAX_TIMESTAMP = 4102415999;
    public function deliveryDate($deliveryDate) : bool
    {
        if (!V::intVal()->between(
            self::MIN_TIMESTAMP,
            self::MAX_TIMESTAMP
        )->validate($deliveryDate)) {
            Core::setLastError(DELIVERY_DATE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    public function deliveryReadyTime($deliveryReadyTime) : bool
    {
        if (!V::intVal()->between(
            self::MIN_TIMESTAMP,
            self::MAX_TIMESTAMP
        )->validate($deliveryReadyTime)) {
            Core::setLastError(DELIVERY_READYTIME_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    public function deliveryCloseTime($deliveryCloseTime) : bool
    {
        if (!V::intVal()->between(
            self::MIN_TIMESTAMP,
            self::MAX_TIMESTAMP
        )->validate($deliveryCloseTime)) {
            Core::setLastError(DELIVERY_CLOSETIME_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证周末送货范围：[0,1]
    public function weekendDelivery($weekendDelivery) : bool
    {
        if (!in_array($weekendDelivery, OrderFTL::WEEKEND_DELIVERY)) {
            Core::setLastError(WEEKEND_DELIVERY_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证收货地址：整数
    public function deliveryAddress($deliveryAddress) : bool
    {
        if (!V::numeric()->validate($deliveryAddress)) {
            Core::setLastError(DELIVERYADDRESS_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证整板订单：数组
    public function orderFTL($orderFTL) : bool
    {
        if (!V::arrayType()->validate($orderFTL)) {
            Core::setLastError(ORDERFTL_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    const DELIVERYNAME_MIN_LENGTH = 1;
    const DELIVERYNAME_MAX_LENGTH = 50;
    //验证收货人长度：1-50个字符
    public function deliveryName($deliveryName) : bool
    {
        if (!V::stringType()->length(
            self::DELIVERYNAME_MIN_LENGTH,
            self::DELIVERYNAME_MAX_LENGTH
        )->validate($deliveryName)) {
            Core::setLastError(DELIVERYNAME_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }
}
