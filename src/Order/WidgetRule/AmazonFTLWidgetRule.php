<?php
namespace Sdk\Order\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Sdk\Order\Model\AmazonFTL;

class AmazonFTLWidgetRule
{
    const KEYS = array(
        "goodsType",
        "goodsValue",
        "packingType",
        "itemNumber",
        "palletNumber",
        "weight",
        "weightUnit",
        "poNumber",
        "length",
        "width",
        "height",
        "description"
    );
    //验证货物信息格式为数组，且对应字段都存在
    public function items($items) : bool
    {
        $amazonLTLWidgetRule = new AmazonLTLWidgetRule();
        $orderLTLWidgetRule = new OrderLTLWidgetRule();

        if (empty($items) || !V::arrayType()->validate($items)) {
            Core::setLastError(ITEMS_FORMAT_INCORRECT);
            return false;
        }

        foreach ($items as $item) {
            foreach ($item as $key => $value) {
                if (!in_array($key, self::KEYS)) {
                    Core::setLastError(ITEMS_FORMAT_INCORRECT);
                    return false;
                }
                if ($key == 'goodsType' && !$this->goodsType($value)) {
                    return false;
                }
                if ($key == 'goodsValue' && !$this->goodsValue($value)) {
                    return false;
                }
                if ($key == 'packingType' && !$this->packingType($value)) {
                    return false;
                }
                if ($key == 'itemNumber' && !$amazonLTLWidgetRule->itemNumber($value)) {
                    return false;
                }
                if ($key == 'palletNumber' && !$amazonLTLWidgetRule->palletNumber($value)) {
                    return false;
                }
                if ($key == 'weight' && !$amazonLTLWidgetRule->weight($value)) {
                    return false;
                }
                if ($key == 'weightUnit' && !$amazonLTLWidgetRule->weightUnit($value)) {
                    return false;
                }
                if ($key == 'poNumber' && !$amazonLTLWidgetRule->poNumber($value)) {
                    return false;
                }
                if ($key == 'length' && !$this->length($value)) {
                    return false;
                }
                if ($key == 'width' && !$this->width($value)) {
                    return false;
                }
                if ($key == 'height' && !$this->height($value)) {
                    return false;
                }
                if ($key == 'description' && !$orderLTLWidgetRule->description($value)) {
                    return false;
                }
            }
        }
        return true;
    }

    //验证货物类型：字符串
    public function goodsType($goodsType) : bool
    {
        if (!V::stringType()->validate($goodsType)) {
            Core::setLastError(GOODSTYPE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证货物价值长度：8位浮点数字
    public function goodsValue($goodsValue) : bool
    {
        if (!V::floatType()->validate($goodsValue)) {
            Core::setLastError(GOODSVALUE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证包装类型：字符串
    public function packingType($packingType) : bool
    {
        if (!V::stringType()->validate($packingType)) {
            Core::setLastError(PACKINGTYPE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证目标仓库：整数
    public function targetWarehouse($targetWarehouse) : bool
    {
        if (!V::numeric()->validate($targetWarehouse)) {
            Core::setLastError(TARGET_WAREHOUSE_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证整板订单：数组
    public function amazonFTL($amazonFTL) : bool
    {
        if (!V::arrayType()->validate($amazonFTL)) {
            Core::setLastError(AMAZONFTL_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证长度长度：5位浮点数字
    public function length($length) : bool
    {
        if (!V::floatType()->validate($length)) {
            Core::setLastError(LENGTH_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证宽度长度：5位浮点数字
    public function width($width) : bool
    {
        if (!V::floatType()->validate($width)) {
            Core::setLastError(WIDTH_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }

    //验证高度长度：5位浮点数字
    public function height($height) : bool
    {
        if (!V::floatType()->validate($height)) {
            Core::setLastError(HEIGHT_FORMAT_INCORRECT);
            return false;
        }
        return true;
    }
}
