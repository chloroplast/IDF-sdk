<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\ISubOrder;
use Sdk\Order\Model\DispatchShippingOrder;
use Sdk\Order\Model\NullDispatchShippingOrder;

use Sdk\Order\Translator\DispatchOrderTranslator;
use Sdk\Order\Translator\AmazonLTLTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DispatchShippingOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getDispatchOrderTranslator() : DispatchOrderTranslator
    {
        return new DispatchOrderTranslator();
    }

    protected function getAmazonLTLTranslator() : AmazonLTLTranslator
    {
        return new AmazonLTLTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullDispatchShippingOrder::getInstance();
    }

    public function objectToArray($dispatchShippingOrder, array $keys = array())
    {
        if (!$dispatchShippingOrder instanceof DispatchShippingOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'dispatchItemsAttachments',
                'acceptItemsAttachments',
                'position',
                'stockInNumber',
                'stockInTime',
                'dispatchOrder' => array(
                    'id',
                    'number',
                    'totalWeight',
                    'totalPalletNumber',
                    'totalOrderNumber',
                    'acceptAttachments',
                    'dispatchAttachments',
                    'references',
                    'closePage'
                ),
                'amazonLTL',
                'createTime'
            );
        }


        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($dispatchShippingOrder->getId());
        }
        if (in_array('dispatchItemsAttachments', $keys)) {
            $expression['dispatchItemsAttachments'] = $dispatchShippingOrder->getDispatchItemsAttachments();
        }
        if (in_array('acceptItemsAttachments', $keys)) {
            $expression['acceptItemsAttachments'] = $dispatchShippingOrder->getAcceptItemsAttachments();
        }
        if (in_array('position', $keys)) {
            $expression['position'] = $dispatchShippingOrder->getPosition();
        }
        if (in_array('stockInNumber', $keys)) {
            $expression['stockInNumber'] = $dispatchShippingOrder->getStockInNumber();
        }
        if (in_array('stockInTime', $keys)) {
            $stockInTime = $dispatchShippingOrder->getStockInTime();
            $expression['stockInTime'] = $stockInTime;
            $expression['stockInTimeFormatConvert'] = !empty($stockInTime)
            ? $this->convertTimeZone('Y-m-d H:i', $stockInTime)
            : '';
        }
        if (isset($keys['dispatchOrder'])) {
            $expression['dispatchOrder'] = $this->getDispatchOrderTranslator()->objectToArray(
                $dispatchShippingOrder->getDispatchOrder(),
                $keys['dispatchOrder']
            );
        }
        if (in_array('amazonLTL', $keys)) {
            $expression['amazonLTL'] = $this->getAmazonLTLTranslator()->objectToArray(
                $dispatchShippingOrder->getAmazonLTL()
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $dispatchShippingOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
