<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\RecOrderLTLOrder;
use Sdk\Order\Model\NullRecOrderLTLOrder;

use Sdk\CarType\Translator\CarTypeTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class RecOrderLTLOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getCarTypeTranslator() : CarTypeTranslator
    {
        return new CarTypeTranslator();
    }

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullRecOrderLTLOrder::getInstance();
    }

    public function arrayToObject(array $expression, $recOrderLTLOrder = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($recOrderLTLOrder == null) {
            $recOrderLTLOrder = new RecOrderLTLOrder();
        }
        
        if (isset($expression['id'])) {
            $recOrderLTLOrder->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['carType'])) {
            $carType = $this->getCarTypeTranslator()->arrayToObject($expression['carType']);
            $recOrderLTLOrder->setCarType($carType);
        }
        if (isset($expression['staff'])) {
            $staff = $this->getStaffTranslator()->arrayToObject($expression['staff']);
            $recOrderLTLOrder->setStaff($staff);
        }
        if (isset($expression['number'])) {
            $recOrderLTLOrder->setNumber($expression['number']);
        }
        if (isset($expression['totalWeight'])) {
            $recOrderLTLOrder->setTotalWeight($expression['totalWeight']);
        }
        if (isset($expression['totalPalletNumber'])) {
            $recOrderLTLOrder->setTotalPalletNumber($expression['totalPalletNumber']);
        }
        if (isset($expression['totalOrderNumber'])) {
            $recOrderLTLOrder->setTotalOrderNumber($expression['totalOrderNumber']);
        }
        if (isset($expression['pickUpAttachments'])) {
            $recOrderLTLOrder->setPickUpAttachments($expression['pickUpAttachments']);
        }
        if (isset($expression['references'])) {
            $recOrderLTLOrder->setReferences($expression['references']);
        }
        if (isset($expression['pickupDate'])) {
            $recOrderLTLOrder->setPickupDate($expression['pickupDate']);
        }
        if (isset($expression['deliveryDate'])) {
            $recOrderLTLOrder->setDeliveryDate($expression['deliveryDate']);
        }
        if (isset($expression['status']['id'])) {
            $recOrderLTLOrder->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $recOrderLTLOrder->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $recOrderLTLOrder->setUpdateTime($expression['updateTime']);
        }
        if (isset($expression['statusTime'])) {
            $recOrderLTLOrder->setStatusTime($expression['statusTime']);
        }

        return $recOrderLTLOrder;
    }

    public function objectToArray($recOrderLTLOrder, array $keys = array())
    {
        if (!$recOrderLTLOrder instanceof RecOrderLTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'carType' => array('id', 'name'),
                'staff' => array('id', 'name'),
                'number',
                'totalWeight',
                'totalPalletNumber',
                'totalOrderNumber',
                'pickUpAttachments',
                'stockInAttachments',
                'references',
                'pickupDate',
                'deliveryDate',
                'status',
                'createTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($recOrderLTLOrder->getId());
        }
        if (isset($keys['carType'])) {
            $expression['carType'] = $this->getCarTypeTranslator()->objectToArray(
                $recOrderLTLOrder->getCarType(),
                $keys['carType']
            );
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $recOrderLTLOrder->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $recOrderLTLOrder->getNumber();
        }
        if (in_array('totalWeight', $keys)) {
            $expression['totalWeight'] = $recOrderLTLOrder->getTotalWeight();
        }
        if (in_array('totalPalletNumber', $keys)) {
            $expression['totalPalletNumber'] = $recOrderLTLOrder->getTotalPalletNumber();
        }
        if (in_array('totalOrderNumber', $keys)) {
            $expression['totalOrderNumber'] = $recOrderLTLOrder->getTotalOrderNumber();
        }
        if (in_array('references', $keys)) {
            $expression['references'] = $recOrderLTLOrder->getReferences();
        }
        if (in_array('pickUpAttachments', $keys)) {
            $expression['pickUpAttachments'] = $recOrderLTLOrder->getPickUpAttachments();
        }
        if (in_array('stockInAttachments', $keys)) {
            $expression['stockInAttachments'] = $recOrderLTLOrder->getStockInAttachments();
        }
        if (in_array('pickupDate', $keys)) {
            $pickupDate = $recOrderLTLOrder->getPickupDate();
            $expression['pickupDate'] = $pickupDate;
            $expression['pickupDateFormatConvert'] = !empty($pickupDate)
            ? $this->convertTimeZone('Y-m-d', $pickupDate)
            : '';
        }
        if (in_array('deliveryDate', $keys)) {
            $deliveryDate = $recOrderLTLOrder->getDeliveryDate();
            $expression['deliveryDate'] = $deliveryDate;
            $expression['deliveryDateFormatConvert'] = !empty($deliveryDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryDate)
            : '';
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $recOrderLTLOrder->getStatus(),
                OrderLTL::A_STATUS_CN
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $recOrderLTLOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
