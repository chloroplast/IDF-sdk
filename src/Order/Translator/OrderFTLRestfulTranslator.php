<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\OrderFTL;
use Sdk\Order\Model\NullOrderFTL;

use Sdk\User\Member\Translator\MemberRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;
use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;
use Sdk\Address\Translator\AddressRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderFTLRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }
    
    protected function getWarehouseRestfulTranslator() : WarehouseRestfulTranslator
    {
        return new WarehouseRestfulTranslator();
    }
    
    protected function getAddressRestfulTranslator() : AddressRestfulTranslator
    {
        return new AddressRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    protected function getMemberOrderRestfulTranslator() : MemberOrderRestfulTranslator
    {
        return new MemberOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $orderFTL = null)
    {
        if (empty($expression)) {
            return NullOrderFTL::getInstance();
        }

        if ($orderFTL == null) {
            $orderFTL = new OrderFTL();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $orderFTL->setId($data['id']);
        }

        if (isset($attributes['deliveryName'])) {
            $orderFTL->setDeliveryName($attributes['deliveryName']);
        }
        if (isset($attributes['number'])) {
            $orderFTL->setNumber($attributes['number']);
        }
        if (isset($attributes['weekendDelivery'])) {
            $orderFTL->setWeekendDelivery($attributes['weekendDelivery']);
        }
        if (isset($attributes['palletNumber'])) {
            $orderFTL->setPalletNumber($attributes['palletNumber']);
        }
        if (isset($attributes['itemNumber'])) {
            $orderFTL->setItemNumber($attributes['itemNumber']);
        }
        if (isset($attributes['items'])) {
            $orderFTL->setItems($attributes['items']);
        }
        if (isset($attributes['weight'])) {
            $orderFTL->setWeight($attributes['weight']);
        }
        if (isset($attributes['poNumber'])) {
            $orderFTL->setPoNumber($attributes['poNumber']);
        }
        if (isset($attributes['goodsValue'])) {
            $orderFTL->setGoodsValue($attributes['goodsValue']);
        }
        if (isset($attributes['pickupDate'])) {
            $orderFTL->setPickupDate($attributes['pickupDate']);
        }
        if (isset($attributes['readyTime'])) {
            $orderFTL->setReadyTime($attributes['readyTime']);
        }
        if (isset($attributes['closeTime'])) {
            $orderFTL->setCloseTime($attributes['closeTime']);
        }
        if (isset($attributes['deliveryDate'])) {
            $orderFTL->setDeliveryDate($attributes['deliveryDate']);
        }
        if (isset($attributes['deliveryReadyTime'])) {
            $orderFTL->setDeliveryReadyTime($attributes['deliveryReadyTime']);
        }
        if (isset($attributes['deliveryCloseTime'])) {
            $orderFTL->setDeliveryCloseTime($attributes['deliveryCloseTime']);
        }
        if (isset($attributes['pickupNumber'])) {
            $orderFTL->setPickupNumber($attributes['pickupNumber']);
        }
        if (isset($attributes['remark'])) {
            $orderFTL->setRemark($attributes['remark']);
        }
        if (isset($attributes['stockInTime'])) {
            $orderFTL->setStockInTime($attributes['stockInTime']);
        }
        if (isset($attributes['frozen'])) {
            $orderFTL->setFrozen($attributes['frozen']);
        }
        if (isset($attributes['price'])) {
            $orderFTL->setPrice($attributes['price']);
        }
        if (isset($attributes['priceRemark'])) {
            $orderFTL->setPriceRemark($attributes['priceRemark']);
        }
        if (isset($attributes['pickupAddressType'])) {
            $orderFTL->setPickupAddressType($attributes['pickupAddressType']);
        }
        if (isset($attributes['deliveryAddressType'])) {
            $orderFTL->setDeliveryAddressType($attributes['deliveryAddressType']);
        }
        if (isset($attributes['priceApiIdentify'])) {
            $orderFTL->setPriceApiIdentify($attributes['priceApiIdentify']);
        }
        if (isset($attributes['priceApiIndex'])) {
            $orderFTL->setPriceApiIndex($attributes['priceApiIndex']);
        }
        if (isset($attributes['priceApiRecordId'])) {
            $orderFTL->setPriceApiRecordId($attributes['priceApiRecordId']);
        }
        if (isset($attributes['status'])) {
            $orderFTL->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $orderFTL->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $orderFTL->setUpdateTime($attributes['updateTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['member'])) {
            $memberArray = $this->relationshipFill($relationships['member'], $included);
            $member = $this->getMemberRestfulTranslator()->arrayToObject($memberArray);
            $orderFTL->setMember($member);
        }

        if (isset($relationships['pickupWarehouseSnapshot'])) {
            $pickupWarehouseArray = $this->relationshipFill($relationships['pickupWarehouseSnapshot'], $included);
            $pickupWarehouse = $this->getWarehouseRestfulTranslator()->arrayToObject($pickupWarehouseArray);
            $orderFTL->setPickupWarehouse($pickupWarehouse);
        }

        if (isset($relationships['addressSnapshot'])) {
            $addressArray = $this->relationshipFill($relationships['addressSnapshot'], $included);
            $address = $this->getAddressRestfulTranslator()->arrayToObject($addressArray);
            $orderFTL->setAddress($address);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $orderFTL->setStaff($staff);
        }

        if (isset($relationships['memberOrder'])) {
            $memberOrderArray = $this->relationshipFill($relationships['memberOrder'], $included);
            $memberOrder = $this->getMemberOrderRestfulTranslator()->arrayToObject($memberOrderArray);
            $orderFTL->setMemberOrder($memberOrder);
        }

        if (isset($relationships['deliveryAddressSnapshot'])) {
            $deliveryAddressArray = $this->relationshipFill($relationships['deliveryAddressSnapshot'], $included);
            $deliveryAddress = $this->getAddressRestfulTranslator()->arrayToObject($deliveryAddressArray);
            $orderFTL->setdeliveryAddress($deliveryAddress);
        }

        return $orderFTL;
    }

    public function objectToArray($orderFTL, array $keys = array())
    {
        if (!$orderFTL instanceof OrderFTL) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'member',
                'pickupWarehouse',
                'deliveryAddress',
                'deliveryName',
                'weekendDelivery',
                'address',
                'items',
                'pickupDate',
                'readyTime',
                'closeTime',
                'pickupNumber',
                'deliveryDate',
                'deliveryReadyTime',
                'deliveryCloseTime',
                'remark',
                'pickupAddressType',
                'deliveryAddressType',
                'priceApiRecordId',
                'priceApiIdentify',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'orderFTL'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $orderFTL->getId();
        }

        $attributes = array();
        if (in_array('deliveryName', $keys)) {
            $attributes['deliveryName'] = $orderFTL->getDeliveryName();
        }
        if (in_array('weekendDelivery', $keys)) {
            $attributes['weekendDelivery'] = $orderFTL->getWeekendDelivery();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $orderFTL->getItems();
        }
        if (in_array('pickupDate', $keys)) {
            $attributes['pickupDate'] = $orderFTL->getPickupDate();
        }
        if (in_array('readyTime', $keys)) {
            $attributes['readyTime'] = $orderFTL->getReadyTime();
        }
        if (in_array('closeTime', $keys)) {
            $attributes['closeTime'] = $orderFTL->getCloseTime();
        }
        if (in_array('pickupNumber', $keys)) {
            $attributes['pickupNumber'] = $orderFTL->getPickupNumber();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $orderFTL->getRemark();
        }
        if (in_array('deliveryDate', $keys)) {
            $attributes['deliveryDate'] = $orderFTL->getDeliveryDate();
        }
        if (in_array('deliveryReadyTime', $keys)) {
            $attributes['deliveryReadyTime'] = $orderFTL->getDeliveryReadyTime();
        }
        if (in_array('deliveryCloseTime', $keys)) {
            $attributes['deliveryCloseTime'] = $orderFTL->getDeliveryCloseTime();
        }
        if (in_array('pickupAddressType', $keys)) {
            $attributes['pickupAddressType'] = $orderFTL->getPickupAddressType();
        }
        if (in_array('deliveryAddressType', $keys)) {
            $attributes['deliveryAddressType'] = $orderFTL->getDeliveryAddressType();
        }
        if (in_array('priceApiRecordId', $keys)) {
            $attributes['priceApiRecordId'] = $orderFTL->getPriceApiRecordId();
        }
        if (in_array('priceApiIdentify', $keys)) {
            $attributes['priceApiIdentify'] = $orderFTL->getPriceApiIdentify();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('member', $keys) && !empty($orderFTL->getMember()->getId())) {
            $memberRelationships = array(
                'type' => 'member',
                'id' => strval($orderFTL->getMember()->getId())
            );

            $expression['data']['relationships']['member']['data'] = $memberRelationships;
        }
        if (in_array('pickupWarehouse', $keys)) {
            $pickupWarehouseRelationships = array(
                'type' => 'warehouse',
                'id' => strval($orderFTL->getPickupWarehouse()->getId())
            );

            $expression['data']['relationships']['pickupWarehouse']['data'] = $pickupWarehouseRelationships;
        }
        if (in_array('deliveryAddress', $keys)) {
            $deliveryAddressRelationships = array(
                'type' => 'address',
                'id' => strval($orderFTL->getDeliveryAddress()->getId())
            );

            $expression['data']['relationships']['deliveryAddress']['data'] = $deliveryAddressRelationships;
        }
        if (in_array('address', $keys)) {
            $addressRelationships = array(
                'type' => 'address',
                'id' => strval($orderFTL->getAddress()->getId())
            );

            $expression['data']['relationships']['address']['data'] = $addressRelationships;
        }
        if (in_array('staff', $keys) && !empty($orderFTL->getStaff()->getId())) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($orderFTL->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
