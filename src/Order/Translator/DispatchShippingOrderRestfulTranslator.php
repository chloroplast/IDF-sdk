<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\DispatchOrder;
use Sdk\Order\Model\DispatchShippingOrder;
use Sdk\Order\Model\NullDispatchShippingOrder;

use Sdk\Order\Translator\AmazonLTLRestfulTranslator;
use Sdk\Order\Translator\DispatchOrderRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DispatchShippingOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getAmazonLTLRestfulTranslator() : AmazonLTLRestfulTranslator
    {
        return new AmazonLTLRestfulTranslator();
    }
    
    protected function getDispatchOrderRestfulTranslator() : DispatchOrderRestfulTranslator
    {
        return new DispatchOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $dispatchShippingOrder = null)
    {
        if (empty($expression)) {
            return NullDispatchShippingOrder::getInstance();
        }

        if ($dispatchShippingOrder == null) {
            $dispatchShippingOrder = new DispatchShippingOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $dispatchShippingOrder->setId($data['id']);
        }
        if (isset($attributes['status'])) {
            $dispatchShippingOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['position'])) {
            $dispatchShippingOrder->setPosition($attributes['position']);
        }
        if (isset($attributes['stockInNumber'])) {
            $dispatchShippingOrder->setStockInNumber($attributes['stockInNumber']);
        }
        if (isset($attributes['stockInTime'])) {
            $dispatchShippingOrder->setStockInTime($attributes['stockInTime']);
        }
        if (isset($attributes['dispatchAttachments'])) {
            $dispatchShippingOrder->setDispatchItemsAttachments($attributes['dispatchAttachments']);
        }
        if (isset($attributes['acceptAttachments'])) {
            $dispatchShippingOrder->setAcceptItemsAttachments($attributes['acceptAttachments']);
        }
        if (isset($attributes['createTime'])) {
            $dispatchShippingOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['dispatchAmazonOrder'])) {
            $dispatchOrderArray = $this->relationshipFill($relationships['dispatchAmazonOrder'], $included);
            $dispatchOrder = $this->getDispatchOrderRestfulTranslator()->arrayToObject($dispatchOrderArray);
            $dispatchShippingOrder->setDispatchOrder($dispatchOrder);
        }
        if (isset($relationships['amazonLTL'])) {
            $amazonLTLArray = $this->relationshipFill($relationships['amazonLTL'], $included);
            $amazonLTL = $this->getAmazonLTLRestfulTranslator()->arrayToObject($amazonLTLArray);
            $dispatchShippingOrder->setAmazonLTL($amazonLTL);
        }

        return $dispatchShippingOrder;
    }

    public function objectToArray($dispatchShippingOrder, array $keys = array())
    {
        unset($dispatchShippingOrder);
        unset($keys);
        return array();
    }
}
