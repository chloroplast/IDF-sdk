<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\AmazonFTL;
use Sdk\Order\Model\DisAmaFTLOrder;
use Sdk\Order\Model\NullDisAmaFTLOrder;

use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisAmaFTLOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullDisAmaFTLOrder::getInstance();
    }

    public function arrayToObject(array $expression, $disAmaFTLOrder = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($disAmaFTLOrder == null) {
            $disAmaFTLOrder = new DisAmaFTLOrder();
        }
        
        if (isset($expression['id'])) {
            $disAmaFTLOrder->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['staff'])) {
            $staff = $this->getStaffTranslator()->arrayToObject($expression['staff']);
            $disAmaFTLOrder->setStaff($staff);
        }
        if (isset($expression['number'])) {
            $disAmaFTLOrder->setNumber($expression['number']);
        }
        if (isset($expression['totalWeight'])) {
            $disAmaFTLOrder->setTotalWeight($expression['totalWeight']);
        }
        if (isset($expression['totalPalletNumber'])) {
            $disAmaFTLOrder->setTotalPalletNumber($expression['totalPalletNumber']);
        }
        if (isset($expression['totalOrderNumber'])) {
            $disAmaFTLOrder->setTotalOrderNumber($expression['totalOrderNumber']);
        }
        if (isset($expression['dispatchAttachments'])) {
            $disAmaFTLOrder->setDispatchAttachments($expression['dispatchAttachments']);
        }
        if (isset($expression['acceptAttachments'])) {
            $disAmaFTLOrder->setAcceptAttachments($expression['acceptAttachments']);
        }
        if (isset($expression['references'])) {
            $disAmaFTLOrder->setReferences($expression['references']);
        }
        if (isset($expression['isaNumber'])) {
            $disAmaFTLOrder->setIsaNumber($expression['isaNumber']);
        }
        if (isset($expression['deliveryDate'])) {
            $disAmaFTLOrder->setDeliveryDate($expression['deliveryDate']);
        }
        if (isset($expression['pickupDate'])) {
            $disAmaFTLOrder->setPickupDate($expression['pickupDate']);
        }
        if (isset($expression['closePage'])) {
            $disAmaFTLOrder->setClosePage($expression['closePage']);
        }
        if (isset($expression['status']['id'])) {
            $disAmaFTLOrder->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $disAmaFTLOrder->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $disAmaFTLOrder->setUpdateTime($expression['updateTime']);
        }
        if (isset($expression['statusTime'])) {
            $disAmaFTLOrder->setStatusTime($expression['statusTime']);
        }

        return $disAmaFTLOrder;
    }

    public function objectToArray($disAmaFTLOrder, array $keys = array())
    {
        if (!$disAmaFTLOrder instanceof DisAmaFTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'staff' => array('id', 'name'),
                'number',
                'totalWeight',
                'totalPalletNumber',
                'totalOrderNumber',
                'dispatchAttachments',
                'acceptAttachments',
                'references',
                'isaNumber',
                'deliveryDate',
                'pickupDate',
                'closePage',
                'status',
                'createTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($disAmaFTLOrder->getId());
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $disAmaFTLOrder->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $disAmaFTLOrder->getNumber();
        }
        if (in_array('totalWeight', $keys)) {
            $expression['totalWeight'] = $disAmaFTLOrder->getTotalWeight();
        }
        if (in_array('totalPalletNumber', $keys)) {
            $expression['totalPalletNumber'] = $disAmaFTLOrder->getTotalPalletNumber();
        }
        if (in_array('totalOrderNumber', $keys)) {
            $expression['totalOrderNumber'] = $disAmaFTLOrder->getTotalOrderNumber();
        }
        if (in_array('references', $keys)) {
            $expression['references'] = $disAmaFTLOrder->getReferences();
        }
        if (in_array('dispatchAttachments', $keys)) {
            $expression['dispatchAttachments'] = $disAmaFTLOrder->getDispatchAttachments();
        }
        if (in_array('acceptAttachments', $keys)) {
            $expression['acceptAttachments'] = $disAmaFTLOrder->getAcceptAttachments();
        }
        if (in_array('isaNumber', $keys)) {
            $expression['isaNumber'] = $disAmaFTLOrder->getIsaNumber();
        }
        if (in_array('deliveryDate', $keys)) {
            $deliveryDate = $disAmaFTLOrder->getDeliveryDate();
            $expression['deliveryDate'] = $deliveryDate;
            $expression['deliveryDateFormatConvert'] = !empty($deliveryDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryDate)
            : '';
        }
        if (in_array('pickupDate', $keys)) {
            $pickupDate = $disAmaFTLOrder->getPickupDate();
            $expression['pickupDate'] = $pickupDate;
            $expression['pickupDateFormatConvert'] = !empty($pickupDate)
            ? $this->convertTimeZone('Y-m-d', $pickupDate)
            : '';
        }
        if (in_array('closePage', $keys)) {
            $expression['closePage'] = $disAmaFTLOrder->getClosePage();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $disAmaFTLOrder->getStatus(),
                AmazonFTL::A_STATUS_CN
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $disAmaFTLOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
