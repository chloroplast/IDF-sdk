<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\ISubOrder;
use Sdk\Order\Model\DisShipOrderLTLOrder;
use Sdk\Order\Model\NullDisShipOrderLTLOrder;

use Sdk\Order\Translator\DisOrderLTLOrderTranslator;
use Sdk\Order\Translator\OrderLTLTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisShipOrderLTLOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getDisOrderLTLOrderTranslator() : DisOrderLTLOrderTranslator
    {
        return new DisOrderLTLOrderTranslator();
    }

    protected function getOrderLTLTranslator() : OrderLTLTranslator
    {
        return new OrderLTLTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullDisShipOrderLTLOrder::getInstance();
    }

    public function objectToArray($disShipOrderLTLOrder, array $keys = array())
    {
        if (!$disShipOrderLTLOrder instanceof DisShipOrderLTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'dispatchItemsAttachments',
                'acceptItemsAttachments',
                // 'position',
                // 'stockInNumber',
                // 'stockInTime',
                'disOrderLTLOrder' => array(
                    'id',
                    'number',
                    'totalWeight',
                    'totalPalletNumber',
                    'totalOrderNumber',
                    'acceptAttachments',
                    'dispatchAttachments',
                    'references'
                ),
                'orderLTL',
                'createTime'
            );
        }


        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($disShipOrderLTLOrder->getId());
        }
        if (in_array('dispatchItemsAttachments', $keys)) {
            $expression['dispatchItemsAttachments'] = $disShipOrderLTLOrder->getDispatchItemsAttachments();
        }
        if (in_array('acceptItemsAttachments', $keys)) {
            $expression['acceptItemsAttachments'] = $disShipOrderLTLOrder->getAcceptItemsAttachments();
        }
        if (in_array('position', $keys)) {
            $expression['position'] = $disShipOrderLTLOrder->getPosition();
        }
        if (in_array('stockInNumber', $keys)) {
            $expression['stockInNumber'] = $disShipOrderLTLOrder->getStockInNumber();
        }
        if (in_array('stockInTime', $keys)) {
            $stockInTime = $disShipOrderLTLOrder->getStockInTime();
            $expression['stockInTime'] = $stockInTime;
            $expression['stockInTimeFormatConvert'] = !empty($stockInTime)
            ? $this->convertTimeZone('Y-m-d H:i', $stockInTime)
            : '';
        }
        if (isset($keys['disOrderLTLOrder'])) {
            $expression['disOrderLTLOrder'] = $this->getDisOrderLTLOrderTranslator()->objectToArray(
                $disShipOrderLTLOrder->getDisOrderLTLOrder(),
                $keys['disOrderLTLOrder']
            );
        }
        if (in_array('orderLTL', $keys)) {
            $expression['orderLTL'] = $this->getOrderLTLTranslator()->objectToArray(
                $disShipOrderLTLOrder->getOrderLTL()
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $disShipOrderLTLOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
