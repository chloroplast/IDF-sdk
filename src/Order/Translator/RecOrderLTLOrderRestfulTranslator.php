<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\RecOrderLTLOrder;
use Sdk\Order\Model\NullRecOrderLTLOrder;

use Sdk\CarType\Translator\CarTypeRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class RecOrderLTLOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getCarTypeRestfulTranslator() : CarTypeRestfulTranslator
    {
        return new CarTypeRestfulTranslator();
    }

    protected function getOrderLTLRestfulTranslator() : OrderLTLRestfulTranslator
    {
        return new OrderLTLRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $recOrderLTLOrder = null)
    {
        if (empty($expression)) {
            return NullRecOrderLTLOrder::getInstance();
        }

        if ($recOrderLTLOrder == null) {
            $recOrderLTLOrder = new RecOrderLTLOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $recOrderLTLOrder->setId($data['id']);
        }

        if (isset($attributes['number'])) {
            $recOrderLTLOrder->setNumber($attributes['number']);
        }
        if (isset($attributes['totalWeight'])) {
            $recOrderLTLOrder->setTotalWeight($attributes['totalWeight']);
        }
        if (isset($attributes['totalPalletNumber'])) {
            $recOrderLTLOrder->setTotalPalletNumber($attributes['totalPalletNumber']);
        }
        if (isset($attributes['totalOrderNumber'])) {
            $recOrderLTLOrder->setTotalOrderNumber($attributes['totalOrderNumber']);
        }
        if (isset($attributes['references'])) {
            $recOrderLTLOrder->setReferences($attributes['references']);
        }
        if (isset($attributes['pickUpAttachments'])) {
            $recOrderLTLOrder->setPickUpAttachments($attributes['pickUpAttachments']);
        }
        if (isset($attributes['stockInAttachments'])) {
            $recOrderLTLOrder->setStockInAttachments($attributes['stockInAttachments']);
        }
        if (isset($attributes['pickupDate'])) {
            $recOrderLTLOrder->setPickupDate($attributes['pickupDate']);
        }
        if (isset($attributes['deliveryDate'])) {
            $recOrderLTLOrder->setDeliveryDate($attributes['deliveryDate']);
        }
        if (isset($attributes['status'])) {
            $recOrderLTLOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $recOrderLTLOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['carType'])) {
            $carTypeArray = $this->relationshipFill($relationships['carType'], $included);
            $carType = $this->getCarTypeRestfulTranslator()->arrayToObject($carTypeArray);
            $recOrderLTLOrder->setCarType($carType);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $recOrderLTLOrder->setStaff($staff);
        }

        return $recOrderLTLOrder;
    }

    public function objectToArray($recOrderLTLOrder, array $keys = array())
    {
        if (!$recOrderLTLOrder instanceof RecOrderLTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'carType',
                'orderLTL',
                'stockInItemsAttachments',
                'references',
                'pickUpAttachments',
                'stockInAttachments',
                'pickupDate',
                'deliveryDate',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'receiveOrderLTLOrders'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $recOrderLTLOrder->getId();
        }

        $attributes = array();
        if (in_array('stockInItemsAttachments', $keys)) {
            $attributes['items'] = $recOrderLTLOrder->getStockInItemsAttachments();
        }
        if (in_array('references', $keys)) {
            $attributes['references'] = $recOrderLTLOrder->getReferences();
        }
        if (in_array('pickUpAttachments', $keys)) {
            $attributes['attachments'] = $recOrderLTLOrder->getPickUpAttachments();
        }
        if (in_array('stockInAttachments', $keys)) {
            $attributes['attachments'] = $recOrderLTLOrder->getStockInAttachments();
        }
        if (in_array('pickupDate', $keys)) {
            $attributes['pickupDate'] = $recOrderLTLOrder->getPickupDate();
        }
        if (in_array('deliveryDate', $keys)) {
            $attributes['deliveryDate'] = $recOrderLTLOrder->getDeliveryDate();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('carType', $keys)) {
            $carTypeRelationships = array(
                'type' => 'carType',
                'id' => strval($recOrderLTLOrder->getCarType()->getId())
            );

            $expression['data']['relationships']['carType']['data'] = $carTypeRelationships;
        }
        if (in_array('orderLTL', $keys)) {
            foreach ($recOrderLTLOrder->getOrderLTL() as $orderLTL) {
                $orderLTLRelationships[] = array(
                    'type' => 'orderLTL',
                    'id' => strval($orderLTL)
                );
            }

            $expression['data']['relationships']['orderLTL']['data'] = $orderLTLRelationships;
        }
        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($recOrderLTLOrder->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
