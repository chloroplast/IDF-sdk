<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\OrderFTL;
use Sdk\Order\Model\DisOrderFTLOrder;
use Sdk\Order\Model\NullDisOrderFTLOrder;

use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisOrderFTLOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullDisOrderFTLOrder::getInstance();
    }

    public function arrayToObject(array $expression, $disOrderFTLOrder = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($disOrderFTLOrder == null) {
            $disOrderFTLOrder = new DisOrderFTLOrder();
        }
        
        if (isset($expression['id'])) {
            $disOrderFTLOrder->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['staff'])) {
            $staff = $this->getStaffTranslator()->arrayToObject($expression['staff']);
            $disOrderFTLOrder->setStaff($staff);
        }
        if (isset($expression['number'])) {
            $disOrderFTLOrder->setNumber($expression['number']);
        }
        if (isset($expression['totalWeight'])) {
            $disOrderFTLOrder->setTotalWeight($expression['totalWeight']);
        }
        if (isset($expression['totalPalletNumber'])) {
            $disOrderFTLOrder->setTotalPalletNumber($expression['totalPalletNumber']);
        }
        if (isset($expression['totalOrderNumber'])) {
            $disOrderFTLOrder->setTotalOrderNumber($expression['totalOrderNumber']);
        }
        if (isset($expression['dispatchAttachments'])) {
            $disOrderFTLOrder->setDispatchAttachments($expression['dispatchAttachments']);
        }
        if (isset($expression['acceptAttachments'])) {
            $disOrderFTLOrder->setAcceptAttachments($expression['acceptAttachments']);
        }
        if (isset($expression['references'])) {
            $disOrderFTLOrder->setReferences($expression['references']);
        }
        if (isset($expression['deliveryDate'])) {
            $disOrderFTLOrder->setDeliveryDate($expression['deliveryDate']);
        }
        if (isset($expression['pickupDate'])) {
            $disOrderFTLOrder->setPickupDate($expression['pickupDate']);
        }
        if (isset($expression['status']['id'])) {
            $disOrderFTLOrder->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $disOrderFTLOrder->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $disOrderFTLOrder->setUpdateTime($expression['updateTime']);
        }
        if (isset($expression['statusTime'])) {
            $disOrderFTLOrder->setStatusTime($expression['statusTime']);
        }

        return $disOrderFTLOrder;
    }

    public function objectToArray($disOrderFTLOrder, array $keys = array())
    {
        if (!$disOrderFTLOrder instanceof DisOrderFTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'staff' => array('id', 'name'),
                'number',
                'totalWeight',
                'totalPalletNumber',
                'totalOrderNumber',
                'dispatchAttachments',
                'acceptAttachments',
                'references',
                'deliveryDate',
                'pickupDate',
                'status',
                'createTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($disOrderFTLOrder->getId());
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $disOrderFTLOrder->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $disOrderFTLOrder->getNumber();
        }
        if (in_array('totalWeight', $keys)) {
            $expression['totalWeight'] = $disOrderFTLOrder->getTotalWeight();
        }
        if (in_array('totalPalletNumber', $keys)) {
            $expression['totalPalletNumber'] = $disOrderFTLOrder->getTotalPalletNumber();
        }
        if (in_array('totalOrderNumber', $keys)) {
            $expression['totalOrderNumber'] = $disOrderFTLOrder->getTotalOrderNumber();
        }
        if (in_array('references', $keys)) {
            $expression['references'] = $disOrderFTLOrder->getReferences();
        }
        if (in_array('dispatchAttachments', $keys)) {
            $expression['dispatchAttachments'] = $disOrderFTLOrder->getDispatchAttachments();
        }
        if (in_array('acceptAttachments', $keys)) {
            $expression['acceptAttachments'] = $disOrderFTLOrder->getAcceptAttachments();
        }
        if (in_array('deliveryDate', $keys)) {
            $deliveryDate = $disOrderFTLOrder->getDeliveryDate();
            $expression['deliveryDate'] = $deliveryDate;
            $expression['deliveryDateFormatConvert'] = !empty($deliveryDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryDate)
            : '';
        }
        if (in_array('pickupDate', $keys)) {
            $pickupDate = $disOrderFTLOrder->getPickupDate();
            $expression['pickupDate'] = $pickupDate;
            $expression['pickupDateFormatConvert'] = !empty($pickupDate)
            ? $this->convertTimeZone('Y-m-d', $pickupDate)
            : '';
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $disOrderFTLOrder->getStatus(),
                OrderFTL::A_STATUS_CN
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $disOrderFTLOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
