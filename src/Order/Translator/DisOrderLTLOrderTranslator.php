<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\DisOrderLTLOrder;
use Sdk\Order\Model\NullDisOrderLTLOrder;

use Sdk\CarType\Translator\CarTypeTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisOrderLTLOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getCarTypeTranslator() : CarTypeTranslator
    {
        return new CarTypeTranslator();
    }

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullDisOrderLTLOrder::getInstance();
    }

    public function arrayToObject(array $expression, $disOrderLTLOrder = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($disOrderLTLOrder == null) {
            $disOrderLTLOrder = new DisOrderLTLOrder();
        }
        
        if (isset($expression['id'])) {
            $disOrderLTLOrder->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['carType'])) {
            $carType = $this->getCarTypeTranslator()->arrayToObject($expression['carType']);
            $disOrderLTLOrder->setCarType($carType);
        }
        if (isset($expression['staff'])) {
            $staff = $this->getStaffTranslator()->arrayToObject($expression['staff']);
            $disOrderLTLOrder->setStaff($staff);
        }
        if (isset($expression['number'])) {
            $disOrderLTLOrder->setNumber($expression['number']);
        }
        if (isset($expression['totalWeight'])) {
            $disOrderLTLOrder->setTotalWeight($expression['totalWeight']);
        }
        if (isset($expression['totalPalletNumber'])) {
            $disOrderLTLOrder->setTotalPalletNumber($expression['totalPalletNumber']);
        }
        if (isset($expression['totalOrderNumber'])) {
            $disOrderLTLOrder->setTotalOrderNumber($expression['totalOrderNumber']);
        }
        if (isset($expression['dispatchAttachments'])) {
            $disOrderLTLOrder->setDispatchAttachments($expression['dispatchAttachments']);
        }
        if (isset($expression['acceptAttachments'])) {
            $disOrderLTLOrder->setAcceptAttachments($expression['acceptAttachments']);
        }
        if (isset($expression['references'])) {
            $disOrderLTLOrder->setReferences($expression['references']);
        }
        if (isset($expression['deliveryDate'])) {
            $disOrderLTLOrder->setDeliveryDate($expression['deliveryDate']);
        }
        if (isset($expression['pickupDate'])) {
            $disOrderLTLOrder->setPickupDate($expression['pickupDate']);
        }
        if (isset($expression['status']['id'])) {
            $disOrderLTLOrder->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $disOrderLTLOrder->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $disOrderLTLOrder->setUpdateTime($expression['updateTime']);
        }
        if (isset($expression['statusTime'])) {
            $disOrderLTLOrder->setStatusTime($expression['statusTime']);
        }

        return $disOrderLTLOrder;
    }

    public function objectToArray($disOrderLTLOrder, array $keys = array())
    {
        if (!$disOrderLTLOrder instanceof DisOrderLTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'carType' => array('id', 'name'),
                'staff' => array('id', 'name'),
                'number',
                'totalWeight',
                'totalPalletNumber',
                'totalOrderNumber',
                'dispatchAttachments',
                'acceptAttachments',
                'references',
                'deliveryDate',
                'pickupDate',
                'status',
                'createTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($disOrderLTLOrder->getId());
        }
        if (isset($keys['carType'])) {
            $expression['carType'] = $this->getCarTypeTranslator()->objectToArray(
                $disOrderLTLOrder->getCarType(),
                $keys['carType']
            );
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $disOrderLTLOrder->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $disOrderLTLOrder->getNumber();
        }
        if (in_array('totalWeight', $keys)) {
            $expression['totalWeight'] = $disOrderLTLOrder->getTotalWeight();
        }
        if (in_array('totalPalletNumber', $keys)) {
            $expression['totalPalletNumber'] = $disOrderLTLOrder->getTotalPalletNumber();
        }
        if (in_array('totalOrderNumber', $keys)) {
            $expression['totalOrderNumber'] = $disOrderLTLOrder->getTotalOrderNumber();
        }
        if (in_array('references', $keys)) {
            $expression['references'] = $disOrderLTLOrder->getReferences();
        }
        if (in_array('dispatchAttachments', $keys)) {
            $expression['dispatchAttachments'] = $disOrderLTLOrder->getDispatchAttachments();
        }
        if (in_array('acceptAttachments', $keys)) {
            $expression['acceptAttachments'] = $disOrderLTLOrder->getAcceptAttachments();
        }
        if (in_array('deliveryDate', $keys)) {
            $deliveryDate = $disOrderLTLOrder->getDeliveryDate();
            $expression['deliveryDate'] = $deliveryDate;
            $expression['deliveryDateFormatConvert'] = !empty($deliveryDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryDate)
            : '';
        }
        if (in_array('pickupDate', $keys)) {
            $pickupDate = $disOrderLTLOrder->getPickupDate();
            $expression['pickupDate'] = $pickupDate;
            $expression['pickupDateFormatConvert'] = !empty($pickupDate)
            ? $this->convertTimeZone('Y-m-d', $pickupDate)
            : '';
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $disOrderLTLOrder->getStatus(),
                OrderLTL::A_STATUS_CN
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $disOrderLTLOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
