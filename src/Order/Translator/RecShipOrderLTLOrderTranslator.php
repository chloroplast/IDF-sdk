<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\ISubOrder;
use Sdk\Order\Model\RecShipOrderLTLOrder;
use Sdk\Order\Model\NullRecShipOrderLTLOrder;

use Sdk\Warehouse\Translator\WarehouseTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class RecShipOrderLTLOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getWarehouseTranslator() : WarehouseTranslator
    {
        return new WarehouseTranslator();
    }

    protected function getRecOrderLTLOrderTranslator() : RecOrderLTLOrderTranslator
    {
        return new RecOrderLTLOrderTranslator();
    }

    protected function getOrderLTLTranslator() : OrderLTLTranslator
    {
        return new OrderLTLTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullRecShipOrderLTLOrder::getInstance();
    }

    public function objectToArray($recShipOrderLTLOrder, array $keys = array())
    {
        if (!$recShipOrderLTLOrder instanceof RecShipOrderLTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'pickupWarehouse' => array('id', 'code'),
                'stockInItemsAttachments',
                'receiveOrderLTLOrder' => array(
                    'id',
                    'number',
                    'totalWeight',
                    'totalPalletNumber',
                    'totalOrderNumber',
                    'pickUpAttachments',
                    'stockInAttachments',
                    'references',
                    'pickupDate',
                    'deliveryDate'
                ),
                'orderLTL',
                'createTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($recShipOrderLTLOrder->getId());
        }
        if (isset($keys['pickupWarehouse'])) {
            $expression['pickupWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $recShipOrderLTLOrder->getPickupWarehouse(),
                $keys['pickupWarehouse']
            );
        }
        if (in_array('stockInItemsAttachments', $keys)) {
            $expression['stockInItemsAttachments'] = $recShipOrderLTLOrder->getStockInItemsAttachments();
        }
        if (isset($keys['receiveOrderLTLOrder'])) {
            $expression['receiveOrderLTLOrder'] = $this->getRecOrderLTLOrderTranslator()->objectToArray(
                $recShipOrderLTLOrder->getRecOrderLTLOrder(),
                $keys['receiveOrderLTLOrder']
            );
        }
        if (in_array('orderLTL', $keys)) {
            $expression['orderLTL'] = $this->getOrderLTLTranslator()->objectToArray(
                $recShipOrderLTLOrder->getOrderLTL()
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $recShipOrderLTLOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
