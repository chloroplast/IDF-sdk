<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\DispatchOrder;
use Sdk\Order\Model\NullDispatchOrder;

use Sdk\CarType\Translator\CarTypeRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DispatchOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getCarTypeRestfulTranslator() : CarTypeRestfulTranslator
    {
        return new CarTypeRestfulTranslator();
    }

    protected function getAmazonLTLRestfulTranslator() : AmazonLTLRestfulTranslator
    {
        return new AmazonLTLRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $dispatchOrder = null)
    {
        if (empty($expression)) {
            return NullDispatchOrder::getInstance();
        }

        if ($dispatchOrder == null) {
            $dispatchOrder = new DispatchOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $dispatchOrder->setId($data['id']);
        }

        if (isset($attributes['number'])) {
            $dispatchOrder->setNumber($attributes['number']);
        }
        if (isset($attributes['totalWeight'])) {
            $dispatchOrder->setTotalWeight($attributes['totalWeight']);
        }
        if (isset($attributes['totalPalletNumber'])) {
            $dispatchOrder->setTotalPalletNumber($attributes['totalPalletNumber']);
        }
        if (isset($attributes['totalOrderNumber'])) {
            $dispatchOrder->setTotalOrderNumber($attributes['totalOrderNumber']);
        }
        if (isset($attributes['references'])) {
            $dispatchOrder->setReferences($attributes['references']);
        }
        if (isset($attributes['dispatchAttachments'])) {
            $dispatchOrder->setDispatchAttachments($attributes['dispatchAttachments']);
        }
        if (isset($attributes['acceptAttachments'])) {
            $dispatchOrder->setAcceptAttachments($attributes['acceptAttachments']);
        }
        if (isset($attributes['isaNumber'])) {
            $dispatchOrder->setIsaNumber($attributes['isaNumber']);
        }
        if (isset($attributes['deliveryDate'])) {
            $dispatchOrder->setDeliveryDate($attributes['deliveryDate']);
        }
        if (isset($attributes['pickupDate'])) {
            $dispatchOrder->setPickupDate($attributes['pickupDate']);
        }
        if (isset($attributes['closePage'])) {
            $dispatchOrder->setClosePage($attributes['closePage']);
        }
        if (isset($attributes['status'])) {
            $dispatchOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $dispatchOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['carType'])) {
            $carTypeArray = $this->relationshipFill($relationships['carType'], $included);
            $carType = $this->getCarTypeRestfulTranslator()->arrayToObject($carTypeArray);
            $dispatchOrder->setCarType($carType);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $dispatchOrder->setStaff($staff);
        }

        return $dispatchOrder;
    }

    public function objectToArray($dispatchOrder, array $keys = array())
    {
        if (!$dispatchOrder instanceof DispatchOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'carType',
                'amazonLTL',
                'dispatchItemsAttachments',
                'acceptItemsAttachments',
                'references',
                'dispatchAttachments',
                'acceptAttachments',
                'isaNumber',
                'deliveryDate',
                'pickupDate',
                'closePage',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'dispatchAmazonOrders'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $dispatchOrder->getId();
        }

        $attributes = array();
        if (in_array('dispatchItemsAttachments', $keys)) {
            $attributes['items'] = $dispatchOrder->getDispatchItemsAttachments();
        }
        if (in_array('acceptItemsAttachments', $keys)) {
            $attributes['items'] = $dispatchOrder->getAcceptItemsAttachments();
        }
        if (in_array('references', $keys)) {
            $attributes['references'] = $dispatchOrder->getReferences();
        }
        if (in_array('dispatchAttachments', $keys)) {
            $attributes['attachments'] = $dispatchOrder->getDispatchAttachments();
        }
        if (in_array('acceptAttachments', $keys)) {
            $attributes['attachments'] = $dispatchOrder->getAcceptAttachments();
        }
        if (in_array('isaNumber', $keys)) {
            $attributes['isaNumber'] = $dispatchOrder->getIsaNumber();
        }
        if (in_array('deliveryDate', $keys)) {
            $attributes['deliveryDate'] = $dispatchOrder->getDeliveryDate();
        }
        if (in_array('pickupDate', $keys)) {
            $attributes['pickupDate'] = $dispatchOrder->getPickupDate();
        }
        if (in_array('closePage', $keys)) {
            $attributes['closePage'] = $dispatchOrder->getClosePage();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('carType', $keys)) {
            $carTypeRelationships = array(
                'type' => 'carType',
                'id' => strval($dispatchOrder->getCarType()->getId())
            );

            $expression['data']['relationships']['carType']['data'] = $carTypeRelationships;
        }
        if (in_array('amazonLTL', $keys)) {
            foreach ($dispatchOrder->getAmazonLTL() as $amazonLTL) {
                $amazonLTLRelationships[] = array(
                    'type' => 'amazonLTL',
                    'id' => strval($amazonLTL)
                );
            }

            $expression['data']['relationships']['amazonLTL']['data'] = $amazonLTLRelationships;
        }
        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($dispatchOrder->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
