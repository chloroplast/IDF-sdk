<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\OrderFTL;
use Sdk\Order\Model\DisOrderFTLOrder;
use Sdk\Order\Model\DisShipOrderFTLOrder;
use Sdk\Order\Model\NullDisShipOrderFTLOrder;

use Sdk\Order\Translator\OrderFTLRestfulTranslator;
use Sdk\Order\Translator\DisOrderFTLOrderRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisShipOrderFTLOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getOrderFTLRestfulTranslator() : OrderFTLRestfulTranslator
    {
        return new OrderFTLRestfulTranslator();
    }
    
    protected function getDisOrderFTLOrderRestfulTranslator() : DisOrderFTLOrderRestfulTranslator
    {
        return new DisOrderFTLOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $disShipOrderFTLOrder = null)
    {
        if (empty($expression)) {
            return NullDisShipOrderFTLOrder::getInstance();
        }

        if ($disShipOrderFTLOrder == null) {
            $disShipOrderFTLOrder = new DisShipOrderFTLOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $disShipOrderFTLOrder->setId($data['id']);
        }
        if (isset($attributes['status'])) {
            $disShipOrderFTLOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['dispatchAttachments'])) {
            $disShipOrderFTLOrder->setDispatchItemsAttachments($attributes['dispatchAttachments']);
        }
        if (isset($attributes['acceptAttachments'])) {
            $disShipOrderFTLOrder->setAcceptItemsAttachments($attributes['acceptAttachments']);
        }
        if (isset($attributes['createTime'])) {
            $disShipOrderFTLOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['dispatchOrderFTLOrder'])) {
            $disOrderFTLOrderArray = $this->relationshipFill($relationships['dispatchOrderFTLOrder'], $included);
            $disOrderFTLOrder = $this->getDisOrderFTLOrderRestfulTranslator()->arrayToObject($disOrderFTLOrderArray);
            $disShipOrderFTLOrder->setDisOrderFTLOrder($disOrderFTLOrder);
        }
        if (isset($relationships['orderFTL'])) {
            $orderFTLArray = $this->relationshipFill($relationships['orderFTL'], $included);
            $orderFTL = $this->getOrderFTLRestfulTranslator()->arrayToObject($orderFTLArray);
            $disShipOrderFTLOrder->setOrderFTL($orderFTL);
        }

        return $disShipOrderFTLOrder;
    }

    public function objectToArray($disShipOrderFTLOrder, array $keys = array())
    {
        unset($disShipOrderFTLOrder);
        unset($keys);
        return array();
    }
}
