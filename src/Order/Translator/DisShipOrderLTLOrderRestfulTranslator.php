<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\DisOrderLTLOrder;
use Sdk\Order\Model\DisShipOrderLTLOrder;
use Sdk\Order\Model\NullDisShipOrderLTLOrder;

use Sdk\Order\Translator\OrderLTLRestfulTranslator;
use Sdk\Order\Translator\DisOrderLTLOrderRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisShipOrderLTLOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getOrderLTLRestfulTranslator() : OrderLTLRestfulTranslator
    {
        return new OrderLTLRestfulTranslator();
    }
    
    protected function getDisOrderLTLOrderRestfulTranslator() : DisOrderLTLOrderRestfulTranslator
    {
        return new DisOrderLTLOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $disShipOrderLTLOrder = null)
    {
        if (empty($expression)) {
            return NullDisShipOrderLTLOrder::getInstance();
        }

        if ($disShipOrderLTLOrder == null) {
            $disShipOrderLTLOrder = new DisShipOrderLTLOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $disShipOrderLTLOrder->setId($data['id']);
        }
        if (isset($attributes['status'])) {
            $disShipOrderLTLOrder->setStatus($attributes['status']);
        }
        // if (isset($attributes['position'])) {
        //     $disShipOrderLTLOrder->setPosition($attributes['position']);
        // }
        // if (isset($attributes['stockInNumber'])) {
        //     $disShipOrderLTLOrder->setStockInNumber($attributes['stockInNumber']);
        // }
        // if (isset($attributes['stockInTime'])) {
        //     $disShipOrderLTLOrder->setStockInTime($attributes['stockInTime']);
        // }
        if (isset($attributes['dispatchAttachments'])) {
            $disShipOrderLTLOrder->setDispatchItemsAttachments($attributes['dispatchAttachments']);
        }
        if (isset($attributes['acceptAttachments'])) {
            $disShipOrderLTLOrder->setAcceptItemsAttachments($attributes['acceptAttachments']);
        }
        if (isset($attributes['createTime'])) {
            $disShipOrderLTLOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['dispatchOrderLTLOrder'])) {
            $disOrderLTLOrderArray = $this->relationshipFill($relationships['dispatchOrderLTLOrder'], $included);
            $disOrderLTLOrder = $this->getDisOrderLTLOrderRestfulTranslator()->arrayToObject($disOrderLTLOrderArray);
            $disShipOrderLTLOrder->setDisOrderLTLOrder($disOrderLTLOrder);
        }
        if (isset($relationships['orderLTL'])) {
            $orderLTLArray = $this->relationshipFill($relationships['orderLTL'], $included);
            $orderLTL = $this->getOrderLTLRestfulTranslator()->arrayToObject($orderLTLArray);
            $disShipOrderLTLOrder->setOrderLTL($orderLTL);
        }

        return $disShipOrderLTLOrder;
    }

    public function objectToArray($disShipOrderLTLOrder, array $keys = array())
    {
        unset($disShipOrderLTLOrder);
        unset($keys);
        return array();
    }
}
