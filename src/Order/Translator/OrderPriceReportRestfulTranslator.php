<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\OrderPriceReport;
use Sdk\Order\Model\NullOrderPriceReport;

class OrderPriceReportRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $orderPriceReport = null)
    {
        if (empty($expression)) {
            return NullOrderPriceReport::getInstance();
        }

        if ($orderPriceReport == null) {
            $orderPriceReport = new OrderPriceReport();
        }

        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $orderPriceReport->setId($data['id']);
        }
        if (isset($attributes['status'])) {
            $orderPriceReport->setStatus($attributes['status']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['priceResult'])) {
            $priceResult = $this->relationshipsFill($relationships['priceResult'], $included);
            $attributes = isset($priceResult[0]['data']['attributes']) 
            ? $priceResult[0]['data']['attributes'] 
            : array();
        }
        
        if (isset($attributes['price'])) {
            $orderPriceReport->setPrice($attributes['price']);
        }
        if (isset($attributes['priceRemark'])) {
            $orderPriceReport->setPriceRemark($attributes['priceRemark']);
        }

        if (isset($attributes['recordId'])) {
            $orderPriceReport->setRecordId($attributes['recordId']);
        }
        if (isset($attributes['apiIndex'])) {
            $orderPriceReport->setApiIndex($attributes['apiIndex']);
        }
        if (isset($attributes['identify'])) {
            $orderPriceReport->setIdentify($attributes['identify']);
        }
        if (isset($attributes['pickupEndDate'])) {
            $orderPriceReport->setPickupEndDate($attributes['pickupEndDate']);
        }
        if (isset($attributes['pickupStartDate'])) {
            $orderPriceReport->setPickupStartDate($attributes['pickupStartDate']);
        }
        if (isset($attributes['deliveryEndDate'])) {
            $orderPriceReport->setDeliveryEndDate($attributes['deliveryEndDate']);
        }
        if (isset($attributes['deliveryStartDate'])) {
            $orderPriceReport->setDeliveryStartDate($attributes['deliveryStartDate']);
        }

        return $orderPriceReport;
    }

    public function objectToArray($orderPriceReport, array $keys = array())
    {
        unset($orderPriceReport);
        unset($keys);
        return array();
    }
    
    public function arrayToObjects(array $expression) : array
    {
        $isExistNextPage = 0;
        $objects = array();
        
        if (isset($expression['meta']['existNextPage'])) {
            $isExistNextPage = $expression['meta']['existNextPage'];
        }

        if (!empty($expression) && isset($expression['data'])) {
            //判断expression数据是多条还是单条,如果单条则走singleArrayToObjects方法,如果是多条则走listArrayToObjects方法
            $objects = isset($expression['data'][0]) ?
                                    $this->listArrayToObjects($expression) :
                                    $this->singleArrayToObjects($expression);
        }

        return array($isExistNextPage, $objects);
    }

    //多条数据数组转对象
    protected function listArrayToObjects(array $expression) : array
    {
        $objects = array();
        $list = isset($expression['data']) ? $expression['data'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        foreach ($list as $objectArray) {
            $data = array(
                'data' => $objectArray,
                'included' => $included
            );

            $id = isset($objectArray['id']) ? $objectArray['id'] : 0;
            $object = $this->arrayToObject($data);
            $objects[] = $object;
        }

        return $objects;
    }
}
