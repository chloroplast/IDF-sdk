<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\NullOrderLTL;

use Sdk\User\Member\Translator\MemberTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;
use Sdk\Warehouse\Translator\WarehouseTranslator;
use Sdk\Address\Translator\AddressTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderLTLTranslator implements ITranslator
{
    use TranslatorTrait, OrderTranslatorTrait;

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }
    
    protected function getWarehouseTranslator() : WarehouseTranslator
    {
        return new WarehouseTranslator();
    }
    
    protected function getAddressTranslator() : AddressTranslator
    {
        return new AddressTranslator();
    }
    
    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }
    
    protected function getMemberOrderTranslator() : MemberOrderTranslator
    {
        return new MemberOrderTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullOrderLTL::getInstance();
    }

    public function objectToArray($orderLTL, array $keys = array())
    {
        if (!$orderLTL instanceof OrderLTL) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'member' => array('id', 'userName'),
                'pickupWarehouse' => array('id', 'code'),
                'address' => array('id', 'firstAddress', 'secondAddress', 'phone', 'postalCode'),
                'deliveryAddress' => array('id', 'firstAddress', 'secondAddress', 'phone', 'postalCode'),
                'staff' => array('id', 'name'),
                'memberOrder' => array('id', 'number', 'type', 'attribute'),
                'number',
                'bookingPickupDate',
                'pickupZipCode',
                'pickupAddressType',
                'deliveryZipCode',
                'deliveryAddressType',
                'pickupNeedTransport',
                'pickupNeedLiftgate',
                'deliveryNeedTransport',
                'deliveryNeedLiftgate',
                'poNumber',
                'items',
                'pickupVehicleType',
                'readyTime',
                'closeTime',
                'deliveryName',
                'deliveryReadyTime',
                'deliveryCloseTime',
                'weekendDelivery',
                'deliveryDescription',
                'remark',
                'pickupRequirement',
                'deliveryRequirement',
                'idfPickup',
                'position',
                'stockInNumber',
                'stockInTime',
                'frozen',
                'priceApiIdentify',
                'priceApiIndex',
                'priceApiRecordId',
                'price',
                'priceRemark',
                'pickupEndDate',
                'pickupStartDate',
                'deliveryEndDate',
                'deliveryStartDate',
                'palletNumber',
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($orderLTL->getId());
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $orderLTL->getMember(),
                $keys['member']
            );
        }
        if (isset($keys['pickupWarehouse'])) {
            $expression['pickupWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $orderLTL->getPickupWarehouse(),
                $keys['pickupWarehouse']
            );
        }
        if (isset($keys['address'])) {
            $expression['address'] = $this->getAddressTranslator()->objectToArray(
                $orderLTL->getAddress(),
                $keys['address']
            );
        }
        if (isset($keys['deliveryAddress'])) {
            $expression['deliveryAddress'] = $this->getAddressTranslator()->objectToArray(
                $orderLTL->getDeliveryAddress(),
                $keys['deliveryAddress']
            );
        }
        if (isset($keys['staff']) && ($orderLTL->getStaff() == null)) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $orderLTL->getStaff(),
                $keys['staff']
            );
        }
        if (isset($keys['memberOrder'])) {
            $expression['memberOrder'] = $this->getMemberOrderTranslator()->objectToArray(
                $orderLTL->getMemberOrder(),
                $keys['memberOrder']
            );
        }

        if (in_array('number', $keys)) {
            $expression['number'] = $orderLTL->getNumber();
        }
        if (in_array('pickupZipCode', $keys)) {
            $expression['pickupZipCode'] = $orderLTL->getPickupZipCode();
        }
        if (in_array('pickupAddressType', $keys)) {
            $expression['pickupAddressType'] = $orderLTL->getPickupAddressType();
        }
        if (in_array('deliveryZipCode', $keys)) {
            $expression['deliveryZipCode'] = $orderLTL->getDeliveryZipCode();
        }
        if (in_array('deliveryAddressType', $keys)) {
            $expression['deliveryAddressType'] = $orderLTL->getDeliveryAddressType();
        }
        if (in_array('pickupNeedTransport', $keys)) {
            $expression['pickupNeedTransport'] = $this->typeFormatConversion(
                $orderLTL->getPickupNeedTransport(),
                OrderLTL::NEEDTRANSPORT_CN
            );
        }
        if (in_array('pickupNeedLiftgate', $keys)) {
            $expression['pickupNeedLiftgate'] = $this->typeFormatConversion(
                $orderLTL->getPickupNeedLiftgate(),
                OrderLTL::NEEDLIFTGATE_CN
            );
        }
        if (in_array('deliveryNeedTransport', $keys)) {
            $expression['deliveryNeedTransport'] = $this->typeFormatConversion(
                $orderLTL->getDeliveryNeedTransport(),
                OrderLTL::NEEDTRANSPORT_CN
            );
        }
        if (in_array('deliveryNeedLiftgate', $keys)) {
            $expression['deliveryNeedLiftgate'] = $this->typeFormatConversion(
                $orderLTL->getDeliveryNeedLiftgate(),
                OrderLTL::NEEDLIFTGATE_CN
            );
        }
        if (in_array('poNumber', $keys)) {
            $expression['poNumber'] = $orderLTL->getPoNumber();
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $orderLTL->getItems();
        }
        if (in_array('pickupVehicleType', $keys)) {
            $expression['pickupVehicleType'] = $this->typeFormatConversion(
                $orderLTL->getPickupVehicleType(),
                OrderLTL::PICKUPVEHICLETYPE_CN
            );
        }
        if (in_array('deliveryName', $keys)) {
            $expression['deliveryName'] = $orderLTL->getDeliveryName();
        }
        if (in_array('weekendDelivery', $keys)) {
            $expression['weekendDelivery'] = $this->typeFormatConversion(
                $orderLTL->getWeekendDelivery(),
                OrderLTL::WEEKEND_DELIVERY_CN
            );
        }
        if (in_array('deliveryDescription', $keys)) {
            $expression['deliveryDescription'] = $orderLTL->getDeliveryDescription();
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = $orderLTL->getRemark();
        }
        if (in_array('pickupRequirement', $keys)) {
            $expression['pickupRequirement'] = $orderLTL->getPickupRequirement();
        }
        if (in_array('deliveryRequirement', $keys)) {
            $expression['deliveryRequirement'] = $orderLTL->getDeliveryRequirement();
        }

        if (in_array('readyTime', $keys)) {
            $expression['readyTime'] = $orderLTL->getReadyTime();
        }
        if (in_array('closeTime', $keys)) {
            $expression['closeTime'] = $orderLTL->getCloseTime();
        }
        if (in_array('deliveryReadyTime', $keys)) {
            $expression['deliveryReadyTime'] = $orderLTL->getDeliveryReadyTime();
        }
        if (in_array('deliveryCloseTime', $keys)) {
            $expression['deliveryCloseTime'] = $orderLTL->getDeliveryCloseTime();
        }

        if (in_array('position', $keys)) {
            $expression['position'] = $orderLTL->getPosition();
        }
        if (in_array('stockInNumber', $keys)) {
            $expression['stockInNumber'] = $orderLTL->getStockInNumber();
        }
        if (in_array('priceApiIdentify', $keys)) {
            $expression['priceApiIdentify'] = $orderLTL->getPriceApiIdentify();
        }
        if (in_array('priceApiIndex', $keys)) {
            $expression['priceApiIndex'] = $orderLTL->getPriceApiIndex();
        }
        if (in_array('priceApiRecordId', $keys)) {
            $expression['priceApiRecordId'] = $orderLTL->getPriceApiRecordId();
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $orderLTL->getPrice();
        }
        if (in_array('priceRemark', $keys)) {
            list($priceRemark, $priceRemarkEn) = $this->convertPriceRemark($orderLTL->getPriceRemark());
            $expression['priceRemark'] = $priceRemark;
            $expression['priceRemarkEn'] = $priceRemarkEn;
        }

        if (in_array('idfPickup', $keys)) {
            $expression['idfPickup'] = $this->typeFormatConversion(
                $orderLTL->getIdfPickup(),
                OrderLTL::IDF_PICKUP_CN
            );
        }
        if (in_array('frozen', $keys)) {
            $expression['frozen'] = $this->typeFormatConversion(
                $orderLTL->getFrozen(),
                OrderLTL::FROZEN_CN
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $orderLTL->getStatus(),
                OrderLTL::A_STATUS_CN
            );
        }
        if (in_array('palletNumber', $keys)) {
            $expression['palletNumber'] = $orderLTL->getPalletNumber();
        }

        if (in_array('bookingPickupDate', $keys)) {
            $bookingPickupDate = $orderLTL->getBookingPickupDate();
            $expression['bookingPickupDate'] = $bookingPickupDate;
            $expression['bookingPickupDateFormatConvert'] = !empty($bookingPickupDate)
            ? $this->convertTimeZone('Y-m-d', $bookingPickupDate)
            : '';
        }
        if (in_array('stockInTime', $keys)) {
            $stockInTime = $orderLTL->getStockInTime();
            $expression['stockInTime'] = $stockInTime;
            $expression['stockInTimeFormatConvert'] = !empty($stockInTime)
            ? $this->convertTimeZone('Y-m-d H:i', $stockInTime)
            : '';
        }
        if (in_array('pickupEndDate', $keys)) {
            $pickupEndDate = $orderLTL->getPickupEndDate();
            $expression['pickupEndDate'] = $pickupEndDate;
            $expression['pickupEndDateFormatConvert'] = !empty($pickupEndDate)
            ? $this->convertTimeZone('Y-m-d', $pickupEndDate)
            : '';
        }
        if (in_array('pickupStartDate', $keys)) {
            $pickupStartDate = $orderLTL->getPickupStartDate();
            $expression['pickupStartDate'] = $pickupStartDate;
            $expression['pickupStartDateFormatConvert'] = !empty($pickupStartDate)
            ? $this->convertTimeZone('Y-m-d', $pickupStartDate)
            : '';
        }
        if (in_array('deliveryEndDate', $keys)) {
            $deliveryEndDate = $orderLTL->getDeliveryEndDate();
            $expression['deliveryEndDate'] = $deliveryEndDate;
            $expression['deliveryEndDateFormatConvert'] = !empty($deliveryEndDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryEndDate)
            : '';
        }
        if (in_array('deliveryStartDate', $keys)) {
            $deliveryStartDate = $orderLTL->getDeliveryStartDate();
            $expression['deliveryStartDate'] = $deliveryStartDate;
            $expression['deliveryStartDateFormatConvert'] = !empty($deliveryStartDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryStartDate)
            : '';
        }
        if (in_array('createTime', $keys)) {
            $createTime = $orderLTL->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }
        if (in_array('updateTime', $keys)) {
            $updateTime = $orderLTL->getUpdateTime();
            $expression['updateTime'] = $updateTime;
            $expression['updateTimeFormatConvert'] = !empty($updateTime)
            ? $this->convertTimeZone('Y-m-d H:i', $updateTime)
            : '';
        }

        return $expression;
    }
}
