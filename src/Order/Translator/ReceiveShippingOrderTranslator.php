<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\ISubOrder;
use Sdk\Order\Model\ReceiveShippingOrder;
use Sdk\Order\Model\NullReceiveShippingOrder;

use Sdk\Warehouse\Translator\WarehouseTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ReceiveShippingOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getWarehouseTranslator() : WarehouseTranslator
    {
        return new WarehouseTranslator();
    }

    protected function getReceiveOrderTranslator() : ReceiveOrderTranslator
    {
        return new ReceiveOrderTranslator();
    }

    protected function getAmazonLTLTranslator() : AmazonLTLTranslator
    {
        return new AmazonLTLTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullReceiveShippingOrder::getInstance();
    }

    public function objectToArray($receiveShippingOrder, array $keys = array())
    {
        if (!$receiveShippingOrder instanceof ReceiveShippingOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'pickupWarehouse' => array('id', 'code'),
                'stockInItemsAttachments',
                'receiveOrder' => array(
                    'id',
                    'number',
                    'totalWeight',
                    'totalPalletNumber',
                    'totalOrderNumber',
                    'pickUpAttachments',
                    'stockInAttachments',
                    'references',
                    'pickupDate',
                    'deliveryDate'
                ),
                'amazonLTL',
                'createTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($receiveShippingOrder->getId());
        }
        if (isset($keys['pickupWarehouse'])) {
            $expression['pickupWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $receiveShippingOrder->getPickupWarehouse(),
                $keys['pickupWarehouse']
            );
        }
        if (in_array('stockInItemsAttachments', $keys)) {
            $expression['stockInItemsAttachments'] = $receiveShippingOrder->getStockInItemsAttachments();
        }
        if (isset($keys['receiveOrder'])) {
            $expression['receiveOrder'] = $this->getReceiveOrderTranslator()->objectToArray(
                $receiveShippingOrder->getReceiveOrder(),
                $keys['receiveOrder']
            );
        }
        if (in_array('amazonLTL', $keys)) {
            $expression['amazonLTL'] = $this->getAmazonLTLTranslator()->objectToArray(
                $receiveShippingOrder->getAmazonLTL()
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $receiveShippingOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
