<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\AmazonFTL;
use Sdk\Order\Model\DisAmaFTLOrder;
use Sdk\Order\Model\DisShipAmaFTLOrder;
use Sdk\Order\Model\NullDisShipAmaFTLOrder;

use Sdk\Order\Translator\AmazonFTLRestfulTranslator;
use Sdk\Order\Translator\DisAmaFTLOrderRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisShipAmaFTLOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getAmazonFTLRestfulTranslator() : AmazonFTLRestfulTranslator
    {
        return new AmazonFTLRestfulTranslator();
    }
    
    protected function getDisAmaFTLOrderRestfulTranslator() : DisAmaFTLOrderRestfulTranslator
    {
        return new DisAmaFTLOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $disShipAmaFTLOrder = null)
    {
        if (empty($expression)) {
            return NullDisShipAmaFTLOrder::getInstance();
        }

        if ($disShipAmaFTLOrder == null) {
            $disShipAmaFTLOrder = new DisShipAmaFTLOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $disShipAmaFTLOrder->setId($data['id']);
        }
        if (isset($attributes['status'])) {
            $disShipAmaFTLOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['dispatchAttachments'])) {
            $disShipAmaFTLOrder->setDispatchItemsAttachments($attributes['dispatchAttachments']);
        }
        if (isset($attributes['acceptAttachments'])) {
            $disShipAmaFTLOrder->setAcceptItemsAttachments($attributes['acceptAttachments']);
        }
        if (isset($attributes['createTime'])) {
            $disShipAmaFTLOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['dispatchAmazonFTLOrder'])) {
            $disAmaFTLOrderArray = $this->relationshipFill($relationships['dispatchAmazonFTLOrder'], $included);
            $disAmaFTLOrder = $this->getDisAmaFTLOrderRestfulTranslator()->arrayToObject($disAmaFTLOrderArray);
            $disShipAmaFTLOrder->setDisAmaFTLOrder($disAmaFTLOrder);
        }
        if (isset($relationships['amazonFTL'])) {
            $amazonFTLArray = $this->relationshipFill($relationships['amazonFTL'], $included);
            $amazonFTL = $this->getAmazonFTLRestfulTranslator()->arrayToObject($amazonFTLArray);
            $disShipAmaFTLOrder->setAmazonFTL($amazonFTL);
        }

        return $disShipAmaFTLOrder;
    }

    public function objectToArray($disShipAmaFTLOrder, array $keys = array())
    {
        unset($disShipAmaFTLOrder);
        unset($keys);
        return array();
    }
}
