<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\ISubOrder;
use Sdk\Order\Model\DisShipOrderFTLOrder;
use Sdk\Order\Model\NullDisShipOrderFTLOrder;

use Sdk\Order\Translator\DisOrderFTLOrderTranslator;
use Sdk\Order\Translator\OrderFTLTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisShipOrderFTLOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getDisOrderFTLOrderTranslator() : DisOrderFTLOrderTranslator
    {
        return new DisOrderFTLOrderTranslator();
    }

    protected function getOrderFTLTranslator() : OrderFTLTranslator
    {
        return new OrderFTLTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullDisShipOrderFTLOrder::getInstance();
    }

    public function objectToArray($disShipOrderFTLOrder, array $keys = array())
    {
        if (!$disShipOrderFTLOrder instanceof DisShipOrderFTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'dispatchItemsAttachments',
                'acceptItemsAttachments',
                'dispatchOrder' => array(
                    'id',
                    'number',
                    'totalWeight',
                    'totalPalletNumber',
                    'totalOrderNumber',
                    'acceptAttachments',
                    'dispatchAttachments',
                    'references'
                ),
                'orderFTL',
                'createTime'
            );
        }


        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($disShipOrderFTLOrder->getId());
        }
        if (in_array('dispatchItemsAttachments', $keys)) {
            $expression['dispatchItemsAttachments'] = $disShipOrderFTLOrder->getDispatchItemsAttachments();
        }
        if (in_array('acceptItemsAttachments', $keys)) {
            $expression['acceptItemsAttachments'] = $disShipOrderFTLOrder->getAcceptItemsAttachments();
        }
        if (isset($keys['dispatchOrder'])) {
            $expression['dispatchOrder'] = $this->getDisOrderFTLOrderTranslator()->objectToArray(
                $disShipOrderFTLOrder->getDisOrderFTLOrder(),
                $keys['dispatchOrder']
            );
        }
        if (in_array('orderFTL', $keys)) {
            $expression['orderFTL'] = $this->getOrderFTLTranslator()->objectToArray(
                $disShipOrderFTLOrder->getOrderFTL()
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $disShipOrderFTLOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
