<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\ISubOrder;
use Sdk\Order\Model\DisShipAmaFTLOrder;
use Sdk\Order\Model\NullDisShipAmaFTLOrder;

use Sdk\Order\Translator\DisAmaFTLOrderTranslator;
use Sdk\Order\Translator\AmazonFTLTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisShipAmaFTLOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getDisAmaFTLOrderTranslator() : DisAmaFTLOrderTranslator
    {
        return new DisAmaFTLOrderTranslator();
    }

    protected function getAmazonFTLTranslator() : AmazonFTLTranslator
    {
        return new AmazonFTLTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullDisShipAmaFTLOrder::getInstance();
    }

    public function objectToArray($disShipAmaFTLOrder, array $keys = array())
    {
        if (!$disShipAmaFTLOrder instanceof DisShipAmaFTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'dispatchItemsAttachments',
                'acceptItemsAttachments',
                'dispatchOrder' => array(
                    'id',
                    'number',
                    'totalWeight',
                    'totalPalletNumber',
                    'totalOrderNumber',
                    'acceptAttachments',
                    'dispatchAttachments',
                    'references',
                    'closePage'
                ),
                'amazonFTL',
                'createTime'
            );
        }


        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($disShipAmaFTLOrder->getId());
        }
        if (in_array('dispatchItemsAttachments', $keys)) {
            $expression['dispatchItemsAttachments'] = $disShipAmaFTLOrder->getDispatchItemsAttachments();
        }
        if (in_array('acceptItemsAttachments', $keys)) {
            $expression['acceptItemsAttachments'] = $disShipAmaFTLOrder->getAcceptItemsAttachments();
        }
        if (isset($keys['dispatchOrder'])) {
            $expression['dispatchOrder'] = $this->getDisAmaFTLOrderTranslator()->objectToArray(
                $disShipAmaFTLOrder->getDisAmaFTLOrder(),
                $keys['dispatchOrder']
            );
        }
        if (in_array('amazonFTL', $keys)) {
            $expression['amazonFTL'] = $this->getAmazonFTLTranslator()->objectToArray(
                $disShipAmaFTLOrder->getAmazonFTL()
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $disShipAmaFTLOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
