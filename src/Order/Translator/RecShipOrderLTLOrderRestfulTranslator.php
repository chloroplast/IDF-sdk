<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\RecOrderLTLOrder;
use Sdk\Order\Model\RecShipOrderLTLOrder;
use Sdk\Order\Model\NullRecShipOrderLTLOrder;

use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class RecShipOrderLTLOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getWarehouseRestfulTranslator() : WarehouseRestfulTranslator
    {
        return new WarehouseRestfulTranslator();
    }
    
    protected function getOrderLTLRestfulTranslator() : OrderLTLRestfulTranslator
    {
        return new OrderLTLRestfulTranslator();
    }
    
    protected function getRecOrderLTLOrderRestfulTranslator() : RecOrderLTLOrderRestfulTranslator
    {
        return new RecOrderLTLOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $recShipOrderLTLOrder = null)
    {
        if (empty($expression)) {
            return NullRecShipOrderLTLOrder::getInstance();
        }

        if ($recShipOrderLTLOrder == null) {
            $recShipOrderLTLOrder = new RecShipOrderLTLOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $recShipOrderLTLOrder->setId($data['id']);
        }
        if (isset($attributes['status'])) {
            $recShipOrderLTLOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['stockInAttachments'])) {
            $recShipOrderLTLOrder->setStockInItemsAttachments($attributes['stockInAttachments']);
        }
        if (isset($attributes['createTime'])) {
            $recShipOrderLTLOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['pickupWarehouseSnapshot'])) {
            $pickupWarehouseArray = $this->relationshipFill($relationships['pickupWarehouseSnapshot'], $included);
            $pickupWarehouse = $this->getWarehouseRestfulTranslator()->arrayToObject($pickupWarehouseArray);
            $recShipOrderLTLOrder->setPickupWarehouse($pickupWarehouse);
        }
        if (isset($relationships['receiveOrderLTLOrder'])) {
            $recOrderLTLOrderArray = $this->relationshipFill($relationships['receiveOrderLTLOrder'], $included);
            $recOrderLTLOrder = $this->getRecOrderLTLOrderRestfulTranslator()->arrayToObject($recOrderLTLOrderArray);
            $recShipOrderLTLOrder->setRecOrderLTLOrder($recOrderLTLOrder);
        }
        if (isset($relationships['orderLTL'])) {
            $orderLTLArray = $this->relationshipFill($relationships['orderLTL'], $included);
            $orderLTL = $this->getOrderLTLRestfulTranslator()->arrayToObject($orderLTLArray);
            $recShipOrderLTLOrder->setOrderLTL($orderLTL);
        }

        return $recShipOrderLTLOrder;
    }

    public function objectToArray($recShipOrderLTLOrder, array $keys = array())
    {
        unset($recShipOrderLTLOrder);
        unset($keys);
        return array();
    }
}
