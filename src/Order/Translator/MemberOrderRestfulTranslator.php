<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\NullMemberOrder;

use Sdk\User\Member\Translator\MemberRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class MemberOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $memberOrder = null)
    {
        if (empty($expression)) {
            return NullMemberOrder::getInstance();
        }

        if ($memberOrder == null) {
            $memberOrder = new MemberOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $memberOrder->setId($data['id']);
        }
        if (isset($attributes['number'])) {
            $memberOrder->setNumber($attributes['number']);
        }
        if (isset($attributes['price'])) {
            $memberOrder->setPrice($attributes['price']);
        }
        if (isset($attributes['attribute'])) {
            $memberOrder->setAttribute($attributes['attribute']);
        }
        if (isset($attributes['type'])) {
            $memberOrder->setType($attributes['type']);
        }
        if (isset($attributes['idfPickup'])) {
            $memberOrder->setIdfPickup($attributes['idfPickup']);
        }
        if (isset($attributes['status'])) {
            $memberOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $memberOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['member'])) {
            $memberArray = $this->relationshipFill($relationships['member'], $included);
            $member = $this->getMemberRestfulTranslator()->arrayToObject($memberArray);
            $memberOrder->setMember($member);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $memberOrder->setStaff($staff);
        }

        return $memberOrder;
    }

    public function objectToArray($memberOrder, array $keys = array())
    {
        unset($memberOrder);
        unset($keys);
        return array();
    }
}
