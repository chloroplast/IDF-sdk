<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\DisOrderLTLOrder;
use Sdk\Order\Model\NullDisOrderLTLOrder;

use Sdk\CarType\Translator\CarTypeRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisOrderLTLOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getCarTypeRestfulTranslator() : CarTypeRestfulTranslator
    {
        return new CarTypeRestfulTranslator();
    }

    protected function getOrderLTLRestfulTranslator() : OrderLTLRestfulTranslator
    {
        return new OrderLTLRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $disOrderLTLOrder = null)
    {
        if (empty($expression)) {
            return NullDisOrderLTLOrder::getInstance();
        }

        if ($disOrderLTLOrder == null) {
            $disOrderLTLOrder = new DisOrderLTLOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $disOrderLTLOrder->setId($data['id']);
        }

        if (isset($attributes['number'])) {
            $disOrderLTLOrder->setNumber($attributes['number']);
        }
        if (isset($attributes['totalWeight'])) {
            $disOrderLTLOrder->setTotalWeight($attributes['totalWeight']);
        }
        if (isset($attributes['totalPalletNumber'])) {
            $disOrderLTLOrder->setTotalPalletNumber($attributes['totalPalletNumber']);
        }
        if (isset($attributes['totalOrderNumber'])) {
            $disOrderLTLOrder->setTotalOrderNumber($attributes['totalOrderNumber']);
        }
        if (isset($attributes['references'])) {
            $disOrderLTLOrder->setReferences($attributes['references']);
        }
        if (isset($attributes['dispatchAttachments'])) {
            $disOrderLTLOrder->setDispatchAttachments($attributes['dispatchAttachments']);
        }
        if (isset($attributes['acceptAttachments'])) {
            $disOrderLTLOrder->setAcceptAttachments($attributes['acceptAttachments']);
        }
        if (isset($attributes['deliveryDate'])) {
            $disOrderLTLOrder->setDeliveryDate($attributes['deliveryDate']);
        }
        if (isset($attributes['pickupDate'])) {
            $disOrderLTLOrder->setPickupDate($attributes['pickupDate']);
        }
        if (isset($attributes['status'])) {
            $disOrderLTLOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $disOrderLTLOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['carType'])) {
            $carTypeArray = $this->relationshipFill($relationships['carType'], $included);
            $carType = $this->getCarTypeRestfulTranslator()->arrayToObject($carTypeArray);
            $disOrderLTLOrder->setCarType($carType);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $disOrderLTLOrder->setStaff($staff);
        }

        return $disOrderLTLOrder;
    }

    public function objectToArray($disOrderLTLOrder, array $keys = array())
    {
        if (!$disOrderLTLOrder instanceof DisOrderLTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'carType',
                'orderLTL',
                'dispatchItemsAttachments',
                'acceptItemsAttachments',
                'references',
                'dispatchAttachments',
                'acceptAttachments',
                'deliveryDate',
                'pickupDate',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'dispatchOrderLTLOrders'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $disOrderLTLOrder->getId();
        }

        $attributes = array();
        if (in_array('dispatchItemsAttachments', $keys)) {
            $attributes['items'] = $disOrderLTLOrder->getDispatchItemsAttachments();
        }
        if (in_array('acceptItemsAttachments', $keys)) {
            $attributes['items'] = $disOrderLTLOrder->getAcceptItemsAttachments();
        }
        if (in_array('references', $keys)) {
            $attributes['references'] = $disOrderLTLOrder->getReferences();
        }
        if (in_array('dispatchAttachments', $keys)) {
            $attributes['attachments'] = $disOrderLTLOrder->getDispatchAttachments();
        }
        if (in_array('acceptAttachments', $keys)) {
            $attributes['attachments'] = $disOrderLTLOrder->getAcceptAttachments();
        }
        if (in_array('deliveryDate', $keys)) {
            $attributes['deliveryDate'] = $disOrderLTLOrder->getDeliveryDate();
        }
        if (in_array('pickupDate', $keys)) {
            $attributes['pickupDate'] = $disOrderLTLOrder->getPickupDate();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('carType', $keys)) {
            $carTypeRelationships = array(
                'type' => 'carType',
                'id' => strval($disOrderLTLOrder->getCarType()->getId())
            );

            $expression['data']['relationships']['carType']['data'] = $carTypeRelationships;
        }
        if (in_array('orderLTL', $keys)) {
            foreach ($disOrderLTLOrder->getOrderLTL() as $orderLTL) {
                $orderLTLRelationships[] = array(
                    'type' => 'orderLTL',
                    'id' => strval($orderLTL)
                );
            }

            $expression['data']['relationships']['orderLTL']['data'] = $orderLTLRelationships;
        }
        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($disOrderLTLOrder->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
