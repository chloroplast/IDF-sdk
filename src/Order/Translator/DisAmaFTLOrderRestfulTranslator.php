<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\DisAmaFTLOrder;
use Sdk\Order\Model\NullDisAmaFTLOrder;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisAmaFTLOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $disAmaFTLOrder = null)
    {
        if (empty($expression)) {
            return NullDisAmaFTLOrder::getInstance();
        }

        if ($disAmaFTLOrder == null) {
            $disAmaFTLOrder = new DisAmaFTLOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $disAmaFTLOrder->setId($data['id']);
        }

        if (isset($attributes['number'])) {
            $disAmaFTLOrder->setNumber($attributes['number']);
        }
        if (isset($attributes['totalWeight'])) {
            $disAmaFTLOrder->setTotalWeight($attributes['totalWeight']);
        }
        if (isset($attributes['totalPalletNumber'])) {
            $disAmaFTLOrder->setTotalPalletNumber($attributes['totalPalletNumber']);
        }
        if (isset($attributes['totalOrderNumber'])) {
            $disAmaFTLOrder->setTotalOrderNumber($attributes['totalOrderNumber']);
        }
        if (isset($attributes['references'])) {
            $disAmaFTLOrder->setReferences($attributes['references']);
        }
        if (isset($attributes['dispatchAttachments'])) {
            $disAmaFTLOrder->setDispatchAttachments($attributes['dispatchAttachments']);
        }
        if (isset($attributes['acceptAttachments'])) {
            $disAmaFTLOrder->setAcceptAttachments($attributes['acceptAttachments']);
        }
        if (isset($attributes['isaNumber'])) {
            $disAmaFTLOrder->setIsaNumber($attributes['isaNumber']);
        }
        if (isset($attributes['deliveryDate'])) {
            $disAmaFTLOrder->setDeliveryDate($attributes['deliveryDate']);
        }
        if (isset($attributes['pickupDate'])) {
            $disAmaFTLOrder->setPickupDate($attributes['pickupDate']);
        }
        if (isset($attributes['closePage'])) {
            $disAmaFTLOrder->setClosePage($attributes['closePage']);
        }
        if (isset($attributes['status'])) {
            $disAmaFTLOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $disAmaFTLOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $disAmaFTLOrder->setStaff($staff);
        }

        return $disAmaFTLOrder;
    }

    public function objectToArray($disAmaFTLOrder, array $keys = array())
    {
        if (!$disAmaFTLOrder instanceof DisAmaFTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'amazonFTL',
                'dispatchItemsAttachments',
                'acceptItemsAttachments',
                'references',
                'dispatchAttachments',
                'acceptAttachments',
                'isaNumber',
                'deliveryDate',
                'pickupDate',
                'closePage',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'dispatchAmazonFTLOrders'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $disAmaFTLOrder->getId();
        }

        $attributes = array();
        if (in_array('dispatchItemsAttachments', $keys)) {
            $attributes['items'] = $disAmaFTLOrder->getDispatchItemsAttachments();
        }
        if (in_array('acceptItemsAttachments', $keys)) {
            $attributes['items'] = $disAmaFTLOrder->getAcceptItemsAttachments();
        }
        if (in_array('references', $keys)) {
            $attributes['references'] = $disAmaFTLOrder->getReferences();
        }
        if (in_array('dispatchAttachments', $keys)) {
            $attributes['attachments'] = $disAmaFTLOrder->getDispatchAttachments();
        }
        if (in_array('acceptAttachments', $keys)) {
            $attributes['attachments'] = $disAmaFTLOrder->getAcceptAttachments();
        }
        if (in_array('isaNumber', $keys)) {
            $attributes['isaNumber'] = $disAmaFTLOrder->getIsaNumber();
        }
        if (in_array('deliveryDate', $keys)) {
            $attributes['deliveryDate'] = $disAmaFTLOrder->getDeliveryDate();
        }
        if (in_array('pickupDate', $keys)) {
            $attributes['pickupDate'] = $disAmaFTLOrder->getPickupDate();
        }
        if (in_array('closePage', $keys)) {
            $attributes['closePage'] = $disAmaFTLOrder->getClosePage();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('amazonFTL', $keys)) {
            foreach ($disAmaFTLOrder->getAmazonFTL() as $amazonFTL) {
                $amazonFTLRelationships[] = array(
                    'type' => 'amazonFTL',
                    'id' => strval($amazonFTL)
                );
            }

            $expression['data']['relationships']['amazonFTL']['data'] = $amazonFTLRelationships;
        }
        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($disAmaFTLOrder->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
