<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\AmazonFTL;
use Sdk\Order\Model\OrderFTL;
use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\OrderStatusHistory;
use Sdk\Order\Model\NullOrderStatusHistory;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderStatusHistoryRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $orderStatusHistory = null)
    {
        if (empty($expression)) {
            return NullOrderStatusHistory::getInstance();
        }

        if ($orderStatusHistory == null) {
            $orderStatusHistory = new OrderStatusHistory();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $orderStatusHistory->setId($data['id']);
        }
        if (isset($attributes['orderAttribute'])) {
            $orderStatusHistory->setOrderAttribute($attributes['orderAttribute']);
        }
        if (isset($attributes['orderType'])) {
            $orderStatusHistory->setOrderType($attributes['orderType']);
        }
        if (isset($attributes['orderId'])) {
            if ($attributes['orderAttribute'] == MemberOrder::ATTRIBUTE['AMAZON']) {
                if ($attributes['orderType'] == MemberOrder::TYPE['LTL']) {
                    $orderStatusHistory->setSubOrder(new AmazonLTL($attributes['orderId']));
                }
                if ($attributes['orderType'] == MemberOrder::TYPE['FTL']) {
                    $orderStatusHistory->setSubOrder(new AmazonFTL($attributes['orderId']));
                }
            }
            if ($attributes['orderAttribute'] == MemberOrder::ATTRIBUTE['NO_AMAZON']) {
                if ($attributes['orderType'] == MemberOrder::TYPE['FTL']) {
                    $orderStatusHistory->setSubOrder(new OrderFTL($attributes['orderId']));
                }
            }
        }
        if (isset($attributes['orderStatus'])) {
            $orderStatusHistory->setStatus($attributes['orderStatus']);
        }
        if (isset($attributes['createTime'])) {
            $orderStatusHistory->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $orderStatusHistory->setStaff($staff);
        }

        return $orderStatusHistory;
    }

    public function objectToArray($orderStatusHistory, array $keys = array())
    {
        unset($orderStatusHistory);
        unset($keys);
        return array();
    }
}
