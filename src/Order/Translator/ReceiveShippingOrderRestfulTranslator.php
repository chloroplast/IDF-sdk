<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\ReceiveOrder;
use Sdk\Order\Model\ReceiveShippingOrder;
use Sdk\Order\Model\NullReceiveShippingOrder;

use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ReceiveShippingOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getWarehouseRestfulTranslator() : WarehouseRestfulTranslator
    {
        return new WarehouseRestfulTranslator();
    }
    
    protected function getAmazonLTLRestfulTranslator() : AmazonLTLRestfulTranslator
    {
        return new AmazonLTLRestfulTranslator();
    }
    
    protected function getReceiveOrderRestfulTranslator() : ReceiveOrderRestfulTranslator
    {
        return new ReceiveOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $receiveShippingOrder = null)
    {
        if (empty($expression)) {
            return NullReceiveShippingOrder::getInstance();
        }

        if ($receiveShippingOrder == null) {
            $receiveShippingOrder = new ReceiveShippingOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $receiveShippingOrder->setId($data['id']);
        }
        if (isset($attributes['status'])) {
            $receiveShippingOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['stockInAttachments'])) {
            $receiveShippingOrder->setStockInItemsAttachments($attributes['stockInAttachments']);
        }
        if (isset($attributes['createTime'])) {
            $receiveShippingOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['pickupWarehouse'])) {
            $pickupWarehouseArray = $this->relationshipFill($relationships['pickupWarehouse'], $included);
            $pickupWarehouse = $this->getWarehouseRestfulTranslator()->arrayToObject($pickupWarehouseArray);
            $receiveShippingOrder->setPickupWarehouse($pickupWarehouse);
        }
        if (isset($relationships['receiveAmazonOrder'])) {
            $receiveOrderArray = $this->relationshipFill($relationships['receiveAmazonOrder'], $included);
            $receiveOrder = $this->getReceiveOrderRestfulTranslator()->arrayToObject($receiveOrderArray);
            $receiveShippingOrder->setReceiveOrder($receiveOrder);
        }
        if (isset($relationships['amazonLTL'])) {
            $amazonLTLArray = $this->relationshipFill($relationships['amazonLTL'], $included);
            $amazonLTL = $this->getAmazonLTLRestfulTranslator()->arrayToObject($amazonLTLArray);
            $receiveShippingOrder->setAmazonLTL($amazonLTL);
        }

        return $receiveShippingOrder;
    }

    public function objectToArray($receiveShippingOrder, array $keys = array())
    {
        unset($receiveShippingOrder);
        unset($keys);
        return array();
    }
}
