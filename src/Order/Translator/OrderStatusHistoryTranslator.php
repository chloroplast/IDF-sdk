<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\AmazonFTL;
use Sdk\Order\Model\OrderFTL;
use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\ISubOrder;
use Sdk\Order\Model\OrderStatusHistory;
use Sdk\Order\Model\NullOrderStatusHistory;

use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderStatusHistoryTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullOrderStatusHistory::getInstance();
    }

    public function objectToArray($orderStatusHistory, array $keys = array())
    {
        if (!$orderStatusHistory instanceof OrderStatusHistory) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'orderId',
                'staff' => array('id', 'name'),
                'orderAttribute',
                'orderType',
                'status',
                'createTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($orderStatusHistory->getId());
        }
        if (in_array('orderId', $keys)) {
            $expression['orderId'] = marmot_encode($orderStatusHistory->getSubOrder()->getId());
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $orderStatusHistory->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('orderAttribute', $keys)) {
            $expression['orderAttribute'] = $this->typeFormatConversion(
                $orderStatusHistory->getOrderAttribute(),
                MemberOrder::ATTRIBUTE_CN
            );
        }
        if (in_array('orderType', $keys)) {
            $expression['orderType'] = $this->typeFormatConversion(
                $orderStatusHistory->getOrderType(),
                MemberOrder::TYPE_CN
            );
        }
        if (in_array('status', $keys)) {
            if ($orderStatusHistory->getOrderAttribute() == MemberOrder::ATTRIBUTE['AMAZON']) {
                if ($orderStatusHistory->getOrderType() == MemberOrder::TYPE['LTL']) {
                    $expression['status'] = $this->typeFormatConversion(
                        $orderStatusHistory->getStatus(),
                        AmazonLTL::A_STATUS_CN
                    );
                }
                if ($orderStatusHistory->getOrderType() == MemberOrder::TYPE['FTL']) {
                    $expression['status'] = $this->typeFormatConversion(
                        $orderStatusHistory->getStatus(),
                        AmazonFTL::A_STATUS_CN
                    );
                }
            }
            if ($orderStatusHistory->getOrderAttribute() == MemberOrder::ATTRIBUTE['NO_AMAZON']) {
                if ($orderStatusHistory->getOrderType() == MemberOrder::TYPE['FTL']) {
                    $expression['status'] = $this->typeFormatConversion(
                        $orderStatusHistory->getStatus(),
                        OrderFTL::A_STATUS_CN
                    );
                }
                if ($orderStatusHistory->getOrderAttribute() == MemberOrder::ATTRIBUTE['NO_AMAZON']) {
                    if ($orderStatusHistory->getOrderType() == MemberOrder::TYPE['LTL']) {
                        $expression['status'] = $this->typeFormatConversion(
                            $orderStatusHistory->getStatus(),
                            OrderLTL::A_STATUS_CN
                        );
                    }
                }
            }
        }
        if (in_array('createTime', $keys)) {
            $createTime = $orderStatusHistory->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
