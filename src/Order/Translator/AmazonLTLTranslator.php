<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\NullAmazonLTL;

use Sdk\User\Member\Translator\MemberTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;
use Sdk\Warehouse\Translator\WarehouseTranslator;
use Sdk\Address\Translator\AddressTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AmazonLTLTranslator implements ITranslator
{
    use TranslatorTrait, OrderTranslatorTrait;

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }
    
    protected function getWarehouseTranslator() : WarehouseTranslator
    {
        return new WarehouseTranslator();
    }
    
    protected function getAddressTranslator() : AddressTranslator
    {
        return new AddressTranslator();
    }
    
    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }
    
    protected function getMemberOrderTranslator() : MemberOrderTranslator
    {
        return new MemberOrderTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullAmazonLTL::getInstance();
    }

    public function objectToArray($amazonLTL, array $keys = array())
    {
        if (!$amazonLTL instanceof AmazonLTL) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'member' => array('id', 'userName'),
                'pickupWarehouse' => array('id', 'code'),
                'address' => array('id', 'firstAddress', 'secondAddress', 'phone', 'postalCode'),
                'idfPickup',
                'palletNumber',
                'itemNumber',
                'weight',
                'weightUnit',
                'fbaNumber',
                'poNumber',
                'soNumber',
                'coNumber',
                'length',
                'width',
                'height',
                'pickupDate',
                'readyTime',
                'closeTime',
                'pickupNumber',
                'remark',
                'isaNumber',
                'position',
                'stockInNumber',
                'stockInTime',
                'frozen',
                'price',
                'priceRemark',
                'targetWarehouse' => array('id', 'code'),
                'staff' => array('id', 'name'),
                'memberOrder' => array('id', 'number', 'type', 'attribute'),
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($amazonLTL->getId());
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $amazonLTL->getMember(),
                $keys['member']
            );
        }
        if (isset($keys['pickupWarehouse'])) {
            $expression['pickupWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $amazonLTL->getPickupWarehouse(),
                $keys['pickupWarehouse']
            );
        }
        if (isset($keys['address'])) {
            $expression['address'] = $this->getAddressTranslator()->objectToArray(
                $amazonLTL->getAddress(),
                $keys['address']
            );
        }
        if (isset($keys['staff']) && ($amazonLTL->getStaff() == null)) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $amazonLTL->getStaff(),
                $keys['staff']
            );
        }
        if (isset($keys['targetWarehouse'])) {
            $expression['targetWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $amazonLTL->getTargetWarehouse(),
                $keys['targetWarehouse']
            );
        }
        if (isset($keys['memberOrder'])) {
            $expression['memberOrder'] = $this->getMemberOrderTranslator()->objectToArray(
                $amazonLTL->getMemberOrder(),
                $keys['memberOrder']
            );
        }

        if (in_array('number', $keys)) {
            $expression['number'] = $amazonLTL->getNumber();
        }
        if (in_array('palletNumber', $keys)) {
            $expression['palletNumber'] = $amazonLTL->getPalletNumber();
        }
        if (in_array('itemNumber', $keys)) {
            $expression['itemNumber'] = $amazonLTL->getItemNumber();
        }
        if (in_array('weight', $keys)) {
            $expression['weight'] = $amazonLTL->getWeight();
        }
        if (in_array('weightUnit', $keys)) {
            $expression['weightUnit'] = $this->typeFormatConversion(
                $amazonLTL->getWeightUnit(),
                AmazonLTL::WEIGHT_UNIT_CN
            );
        }
        if (in_array('fbaNumber', $keys)) {
            $expression['fbaNumber'] = $amazonLTL->getFbaNumber();
        }
        if (in_array('poNumber', $keys)) {
            $expression['poNumber'] = $amazonLTL->getPoNumber();
        }
        if (in_array('soNumber', $keys)) {
            $expression['soNumber'] = $amazonLTL->getSoNumber();
        }
        if (in_array('coNumber', $keys)) {
            $expression['coNumber'] = $amazonLTL->getCoNumber();
        }
        if (in_array('length', $keys)) {
            $expression['length'] = $amazonLTL->getLength();
        }
        if (in_array('width', $keys)) {
            $expression['width'] = $amazonLTL->getWidth();
        }
        if (in_array('height', $keys)) {
            $expression['height'] = $amazonLTL->getHeight();
        }
        if (in_array('pickupNumber', $keys)) {
            $expression['pickupNumber'] = $amazonLTL->getPickupNumber();
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = $amazonLTL->getRemark();
        }
        if (in_array('isaNumber', $keys)) {
            $expression['isaNumber'] = $amazonLTL->getIsaNumber();
        }
        if (in_array('position', $keys)) {
            $expression['position'] = $amazonLTL->getPosition();
        }
        if (in_array('stockInNumber', $keys)) {
            $expression['stockInNumber'] = $amazonLTL->getStockInNumber();
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $amazonLTL->getPrice();
        }
        if (in_array('priceRemark', $keys)) {
            list($priceRemark, $priceRemarkEn) = $this->convertPriceRemark($amazonLTL->getPriceRemark());
            $expression['priceRemark'] = $priceRemark;
            $expression['priceRemarkEn'] = $priceRemarkEn;
        }

        if (in_array('idfPickup', $keys)) {
            $expression['idfPickup'] = $this->typeFormatConversion(
                $amazonLTL->getIdfPickup(),
                AmazonLTL::IDF_PICKUP_CN
            );
        }
        if (in_array('frozen', $keys)) {
            $expression['frozen'] = $this->typeFormatConversion(
                $amazonLTL->getFrozen(),
                AmazonLTL::FROZEN_CN
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $amazonLTL->getStatus(),
                AmazonLTL::A_STATUS_CN
            );
        }

        if (in_array('pickupDate', $keys)) {
            $pickupDate = $amazonLTL->getPickupDate();
            $expression['pickupDate'] = $pickupDate;
            $expression['pickupDateFormatConvert'] = !empty($pickupDate)
            ? $this->convertTimeZone('Y-m-d', $pickupDate)
            : '';
        }
        if (in_array('readyTime', $keys)) {
            $readyTime = $amazonLTL->getReadyTime();
            $expression['readyTime'] = $readyTime;
            $expression['readyTimeFormatConvert'] = !empty($readyTime)
            ? $this->convertTimeZone('H:i', $readyTime)
            : '';
        }
        if (in_array('closeTime', $keys)) {
            $closeTime = $amazonLTL->getCloseTime();
            $expression['closeTime'] = $closeTime;
            $expression['closeTimeFormatConvert'] = !empty($closeTime)
            ? $this->convertTimeZone('H:i', $closeTime)
            : '';
        }
        if (in_array('stockInTime', $keys)) {
            $stockInTime = $amazonLTL->getStockInTime();
            $expression['stockInTime'] = $stockInTime;
            $expression['stockInTimeFormatConvert'] = !empty($stockInTime)
            ? $this->convertTimeZone('Y-m-d H:i', $stockInTime)
            : '';
        }
        if (in_array('createTime', $keys)) {
            $createTime = $amazonLTL->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }
        if (in_array('updateTime', $keys)) {
            $updateTime = $amazonLTL->getUpdateTime();
            $expression['updateTime'] = $updateTime;
            $expression['updateTimeFormatConvert'] = !empty($updateTime)
            ? $this->convertTimeZone('Y-m-d H:i', $updateTime)
            : '';
        }

        return $expression;
    }
}
