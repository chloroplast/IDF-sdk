<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\ItemLTL;
use Sdk\Order\Model\NullItemLTL;

class ItemLTLRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $itemLTL = null)
    {
        if (empty($expression)) {
            return NullItemLTL::getInstance();
        }

        if ($itemLTL == null) {
            $itemLTL = new ItemLTL();
        }

        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($data['id'])) {
            $itemLTL->setId($data['id']);
        }
        if (isset($attributes['length'])) {
            $itemLTL->setLength($attributes['length']);
        }
        if (isset($attributes['width'])) {
            $itemLTL->setWidth($attributes['width']);
        }
        if (isset($attributes['height'])) {
            $itemLTL->setHeight($attributes['height']);
        }
        if (isset($attributes['weight'])) {
            $itemLTL->setWeight($attributes['weight']);
        }
        if (isset($attributes['weightLB'])) {
            $itemLTL->setWeightLB($attributes['weightLB']);
        }
        if (isset($attributes['weightUnit'])) {
            $itemLTL->setWeightUnit($attributes['weightUnit']);
        }
        if (isset($attributes['goodsLevel'])) {
            $itemLTL->setGoodsLevel($attributes['goodsLevel']);
        }
        if (isset($attributes['volumeInch'])) {
            $itemLTL->setVolumeInch($attributes['volumeInch']);
        }

        return $itemLTL;
    }

    public function objectToArray($itemLTL, array $keys = array())
    {
        if (!$itemLTL instanceof ItemLTL) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'length',
                'width',
                'height',
                'weight',
                'weightUnit',
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'itemLTL'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $itemLTL->getId();
        }

        $attributes = array();

        if (in_array('length', $keys)) {
            $attributes['length'] = $itemLTL->getLength();
        }
        if (in_array('width', $keys)) {
            $attributes['width'] = $itemLTL->getWidth();
        }
        if (in_array('height', $keys)) {
            $attributes['height'] = $itemLTL->getHeight();
        }
        if (in_array('weight', $keys)) {
            $attributes['weight'] = $itemLTL->getWeight();
        }
        if (in_array('weightUnit', $keys)) {
            $attributes['weightUnit'] = $itemLTL->getWeightUnit();
        }

        $expression['data']['attributes'] = $attributes;
        
        return $expression;
    }
}
