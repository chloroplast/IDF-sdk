<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\NullAmazonLTL;

use Sdk\User\Member\Translator\MemberRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;
use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;
use Sdk\Address\Translator\AddressRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AmazonLTLRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }
    
    protected function getWarehouseRestfulTranslator() : WarehouseRestfulTranslator
    {
        return new WarehouseRestfulTranslator();
    }
    
    protected function getAddressRestfulTranslator() : AddressRestfulTranslator
    {
        return new AddressRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    protected function getMemberOrderRestfulTranslator() : MemberOrderRestfulTranslator
    {
        return new MemberOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $amazonLTL = null)
    {
        if (empty($expression)) {
            return NullAmazonLTL::getInstance();
        }

        if ($amazonLTL == null) {
            $amazonLTL = new AmazonLTL();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $amazonLTL->setId($data['id']);
        }

        if (isset($attributes['number'])) {
            $amazonLTL->setNumber($attributes['number']);
        }
        if (isset($attributes['idfPickup'])) {
            $amazonLTL->setIdfPickup($attributes['idfPickup']);
        }
        if (isset($attributes['palletNumber'])) {
            $amazonLTL->setPalletNumber($attributes['palletNumber']);
        }
        if (isset($attributes['itemNumber'])) {
            $amazonLTL->setItemNumber($attributes['itemNumber']);
        }
        if (isset($attributes['weight'])) {
            $amazonLTL->setWeight($attributes['weight']);
        }
        if (isset($attributes['weightUnit'])) {
            $amazonLTL->setWeightUnit($attributes['weightUnit']);
        }
        if (isset($attributes['fbaNumber'])) {
            $amazonLTL->setFbaNumber($attributes['fbaNumber']);
        }
        if (isset($attributes['poNumber'])) {
            $amazonLTL->setPoNumber($attributes['poNumber']);
        }
        if (isset($attributes['soNumber'])) {
            $amazonLTL->setSoNumber($attributes['soNumber']);
        }
        if (isset($attributes['coNumber'])) {
            $amazonLTL->setCoNumber($attributes['coNumber']);
        }
        if (isset($attributes['length'])) {
            $amazonLTL->setLength($attributes['length']);
        }
        if (isset($attributes['width'])) {
            $amazonLTL->setWidth($attributes['width']);
        }
        if (isset($attributes['height'])) {
            $amazonLTL->setHeight($attributes['height']);
        }
        if (isset($attributes['pickupDate'])) {
            $amazonLTL->setPickupDate($attributes['pickupDate']);
        }
        if (isset($attributes['readyTime'])) {
            $amazonLTL->setReadyTime($attributes['readyTime']);
        }
        if (isset($attributes['closeTime'])) {
            $amazonLTL->setCloseTime($attributes['closeTime']);
        }
        if (isset($attributes['pickupNumber'])) {
            $amazonLTL->setPickupNumber($attributes['pickupNumber']);
        }
        if (isset($attributes['remark'])) {
            $amazonLTL->setRemark($attributes['remark']);
        }
        if (isset($attributes['isaNumber'])) {
            $amazonLTL->setIsaNumber($attributes['isaNumber']);
        }
        if (isset($attributes['position'])) {
            $amazonLTL->setPosition($attributes['position']);
        }
        if (isset($attributes['stockInNumber'])) {
            $amazonLTL->setStockInNumber($attributes['stockInNumber']);
        }
        if (isset($attributes['stockInTime'])) {
            $amazonLTL->setStockInTime($attributes['stockInTime']);
        }
        if (isset($attributes['frozen'])) {
            $amazonLTL->setFrozen($attributes['frozen']);
        }
        if (isset($attributes['price'])) {
            $amazonLTL->setPrice($attributes['price']);
        }
        if (isset($attributes['priceRemark'])) {
            $amazonLTL->setPriceRemark($attributes['priceRemark']);
        }
        if (isset($attributes['status'])) {
            $amazonLTL->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $amazonLTL->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $amazonLTL->setUpdateTime($attributes['updateTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['member'])) {
            $memberArray = $this->relationshipFill($relationships['member'], $included);
            $member = $this->getMemberRestfulTranslator()->arrayToObject($memberArray);
            $amazonLTL->setMember($member);
        }

        if (isset($relationships['pickupWarehouseSnapshot'])) {
            $pickupWarehouseArray = $this->relationshipFill($relationships['pickupWarehouseSnapshot'], $included);
            $pickupWarehouse = $this->getWarehouseRestfulTranslator()->arrayToObject($pickupWarehouseArray);
            $amazonLTL->setPickupWarehouse($pickupWarehouse);
        }

        if (isset($relationships['addressSnapshot'])) {
            $addressArray = $this->relationshipFill($relationships['addressSnapshot'], $included);
            $address = $this->getAddressRestfulTranslator()->arrayToObject($addressArray);
            $amazonLTL->setAddress($address);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $amazonLTL->setStaff($staff);
        }

        if (isset($relationships['memberOrder'])) {
            $memberOrderArray = $this->relationshipFill($relationships['memberOrder'], $included);
            $memberOrder = $this->getMemberOrderRestfulTranslator()->arrayToObject($memberOrderArray);
            $amazonLTL->setMemberOrder($memberOrder);
        }

        if (isset($relationships['targetWarehouseSnapshot'])) {
            $targetWarehouseArray = $this->relationshipFill($relationships['targetWarehouseSnapshot'], $included);
            $targetWarehouse = $this->getWarehouseRestfulTranslator()->arrayToObject($targetWarehouseArray);
            $amazonLTL->setTargetWarehouse($targetWarehouse);
        }

        return $amazonLTL;
    }

    public function objectToArray($amazonLTL, array $keys = array())
    {
        if (!$amazonLTL instanceof AmazonLTL) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'member',
                'pickupWarehouse',
                'address',
                'idfPickup',
                'items',
                'pickupDate',
                'readyTime',
                'closeTime',
                'pickupNumber',
                'remark',
                'isaNumber',
                'position',
                'stockInNumber',
                'stockInTime',
                'filter',
                'sort',
                'carType',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'amazonLTL'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $amazonLTL->getId();
        }

        $attributes = array();
        if (in_array('idfPickup', $keys)) {
            $attributes['idfPickup'] = $amazonLTL->getIdfPickup();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $amazonLTL->getItems();
        }
        if (in_array('pickupDate', $keys)) {
            $attributes['pickupDate'] = $amazonLTL->getPickupDate();
        }
        if (in_array('readyTime', $keys)) {
            $attributes['readyTime'] = $amazonLTL->getReadyTime();
        }
        if (in_array('closeTime', $keys)) {
            $attributes['closeTime'] = $amazonLTL->getCloseTime();
        }
        if (in_array('pickupNumber', $keys)) {
            $attributes['pickupNumber'] = $amazonLTL->getPickupNumber();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $amazonLTL->getRemark();
        }
        if (in_array('isaNumber', $keys)) {
            $attributes['isaNumber'] = $amazonLTL->getIsaNumber();
        }
        if (in_array('position', $keys)) {
            $attributes['position'] = $amazonLTL->getPosition();
        }
        if (in_array('stockInNumber', $keys)) {
            $attributes['stockInNumber'] = $amazonLTL->getStockInNumber();
        }
        if (in_array('stockInTime', $keys)) {
            $attributes['stockInTime'] = $amazonLTL->getStockInTime();
        }
        if (in_array('filter', $keys)) {
            $attributes['filter'] = $amazonLTL->getFilter();
        }
        if (in_array('sort', $keys)) {
            $attributes['sort'] = $amazonLTL->getSort();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('member', $keys) && !empty($amazonLTL->getMember()->getId())) {
            $memberRelationships = array(
                'type' => 'member',
                'id' => strval($amazonLTL->getMember()->getId())
            );

            $expression['data']['relationships']['member']['data'] = $memberRelationships;
        }
        if (in_array('pickupWarehouse', $keys)) {
            $pickupWarehouseRelationships = array(
                'type' => 'warehouse',
                'id' => strval($amazonLTL->getPickupWarehouse()->getId())
            );

            $expression['data']['relationships']['pickupWarehouse']['data'] = $pickupWarehouseRelationships;
        }
        if (in_array('address', $keys)) {
            $addressRelationships = array(
                'type' => 'address',
                'id' => strval($amazonLTL->getAddress()->getId())
            );

            $expression['data']['relationships']['address']['data'] = $addressRelationships;
        }
        if (in_array('staff', $keys) && !empty($amazonLTL->getStaff()->getId())) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($amazonLTL->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        if (in_array('carType', $keys)) {
            $carTypeRelationships = array(
                'type' => 'carType',
                'id' => strval($amazonLTL->getCarType()->getId())
            );

            $expression['data']['relationships']['carType']['data'] = $carTypeRelationships;
        }

        return $expression;
    }
}
