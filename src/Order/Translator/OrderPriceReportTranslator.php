<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\OrderPriceReport;
use Sdk\Order\Model\NullOrderPriceReport;

class OrderPriceReportTranslator implements ITranslator
{
    use TranslatorTrait, OrderTranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullOrderPriceReport::getInstance();
    }

    public function objectToArray($orderPriceReport, array $keys = array())
    {
        if (!$orderPriceReport instanceof OrderPriceReport) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'price',
                'priceRemark',
                'recordId',
                'apiIndex',
                'identify',
                'pickupEndDate',
                'pickupStartDate',
                'deliveryEndDate',
                'deliveryStartDate',
                'status',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($orderPriceReport->getId());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $orderPriceReport->getStatus();
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $orderPriceReport->getPrice();
        }
        if (in_array('priceRemark', $keys)) {
            $expression['priceRemark'] = $orderPriceReport->getPriceRemark();
            list($priceRemarkDetail, $priceRemarkDetailEn) = $this->convertPriceRemark(
                $orderPriceReport->getPriceRemark()
            );
            $expression['priceRemarkDetail'] = $priceRemarkDetail;
            $expression['priceRemarkDetailEn'] = $priceRemarkDetailEn;
        }

        if (in_array('recordId', $keys)) {
            $expression['recordId'] = $orderPriceReport->getRecordId();
        }
        if (in_array('apiIndex', $keys)) {
            $expression['apiIndex'] = $orderPriceReport->getApiIndex();
        }
        if (in_array('identify', $keys)) {
            $expression['identify'] = $orderPriceReport->getIdentify();
        }
        if (in_array('pickupEndDate', $keys)) {
            $pickupEndDate = $orderPriceReport->getPickupEndDate();
            $expression['pickupEndDate'] = $pickupEndDate;
            $expression['pickupEndDateFormatConvert'] = !empty($pickupEndDate)
            ? $this->convertTimeZone('Y-m-d', $pickupEndDate)
            : '';
        }
        if (in_array('pickupStartDate', $keys)) {
            $pickupStartDate = $orderPriceReport->getPickupStartDate();
            $expression['pickupStartDate'] = $pickupStartDate;
            $expression['pickupStartDateFormatConvert'] = !empty($pickupStartDate)
            ? $this->convertTimeZone('Y-m-d', $pickupStartDate)
            : '';
        }
        if (in_array('deliveryEndDate', $keys)) {
            $deliveryEndDate = $orderPriceReport->getDeliveryEndDate();
            $expression['deliveryEndDate'] = $deliveryEndDate;
            $expression['deliveryEndDateFormatConvert'] = !empty($deliveryEndDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryEndDate)
            : '';
        }
        if (in_array('deliveryStartDate', $keys)) {
            $deliveryStartDate = $orderPriceReport->getDeliveryStartDate();
            $expression['deliveryStartDate'] = $deliveryStartDate;
            $expression['deliveryStartDateFormatConvert'] = !empty($deliveryStartDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryStartDate)
            : '';
        }

        return $expression;
    }
}
