<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\ReceiveOrder;
use Sdk\Order\Model\NullReceiveOrder;

use Sdk\CarType\Translator\CarTypeRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ReceiveOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getCarTypeRestfulTranslator() : CarTypeRestfulTranslator
    {
        return new CarTypeRestfulTranslator();
    }

    protected function getAmazonLTLRestfulTranslator() : AmazonLTLRestfulTranslator
    {
        return new AmazonLTLRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $receiveOrder = null)
    {
        if (empty($expression)) {
            return NullReceiveOrder::getInstance();
        }

        if ($receiveOrder == null) {
            $receiveOrder = new ReceiveOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $receiveOrder->setId($data['id']);
        }

        if (isset($attributes['number'])) {
            $receiveOrder->setNumber($attributes['number']);
        }
        if (isset($attributes['totalWeight'])) {
            $receiveOrder->setTotalWeight($attributes['totalWeight']);
        }
        if (isset($attributes['totalPalletNumber'])) {
            $receiveOrder->setTotalPalletNumber($attributes['totalPalletNumber']);
        }
        if (isset($attributes['totalOrderNumber'])) {
            $receiveOrder->setTotalOrderNumber($attributes['totalOrderNumber']);
        }
        if (isset($attributes['references'])) {
            $receiveOrder->setReferences($attributes['references']);
        }
        if (isset($attributes['pickUpAttachments'])) {
            $receiveOrder->setPickUpAttachments($attributes['pickUpAttachments']);
        }
        if (isset($attributes['stockInAttachments'])) {
            $receiveOrder->setStockInAttachments($attributes['stockInAttachments']);
        }
        if (isset($attributes['pickupDate'])) {
            $receiveOrder->setPickupDate($attributes['pickupDate']);
        }
        if (isset($attributes['deliveryDate'])) {
            $receiveOrder->setDeliveryDate($attributes['deliveryDate']);
        }
        if (isset($attributes['status'])) {
            $receiveOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $receiveOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['carType'])) {
            $carTypeArray = $this->relationshipFill($relationships['carType'], $included);
            $carType = $this->getCarTypeRestfulTranslator()->arrayToObject($carTypeArray);
            $receiveOrder->setCarType($carType);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $receiveOrder->setStaff($staff);
        }

        return $receiveOrder;
    }

    public function objectToArray($receiveOrder, array $keys = array())
    {
        if (!$receiveOrder instanceof ReceiveOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'carType',
                'amazonLTL',
                'stockInItemsAttachments',
                'references',
                'pickUpAttachments',
                'stockInAttachments',
                'pickupDate',
                'deliveryDate',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'receiveAmazonOrders'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $receiveOrder->getId();
        }

        $attributes = array();
        if (in_array('stockInItemsAttachments', $keys)) {
            $attributes['items'] = $receiveOrder->getStockInItemsAttachments();
        }
        if (in_array('references', $keys)) {
            $attributes['references'] = $receiveOrder->getReferences();
        }
        if (in_array('pickUpAttachments', $keys)) {
            $attributes['attachments'] = $receiveOrder->getPickUpAttachments();
        }
        if (in_array('stockInAttachments', $keys)) {
            $attributes['attachments'] = $receiveOrder->getStockInAttachments();
        }
        if (in_array('pickupDate', $keys)) {
            $attributes['pickupDate'] = $receiveOrder->getPickupDate();
        }
        if (in_array('deliveryDate', $keys)) {
            $attributes['deliveryDate'] = $receiveOrder->getDeliveryDate();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('carType', $keys)) {
            $carTypeRelationships = array(
                'type' => 'carType',
                'id' => strval($receiveOrder->getCarType()->getId())
            );

            $expression['data']['relationships']['carType']['data'] = $carTypeRelationships;
        }
        if (in_array('amazonLTL', $keys)) {
            foreach ($receiveOrder->getAmazonLTL() as $amazonLTL) {
                $amazonLTLRelationships[] = array(
                    'type' => 'amazonLTL',
                    'id' => strval($amazonLTL)
                );
            }

            $expression['data']['relationships']['amazonLTL']['data'] = $amazonLTLRelationships;
        }
        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($receiveOrder->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
