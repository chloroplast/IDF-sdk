<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\AmazonFTL;
use Sdk\Order\Model\NullAmazonFTL;

use Sdk\User\Member\Translator\MemberTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;
use Sdk\Warehouse\Translator\WarehouseTranslator;
use Sdk\Address\Translator\AddressTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AmazonFTLTranslator implements ITranslator
{
    use TranslatorTrait, OrderTranslatorTrait;

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }
    
    protected function getWarehouseTranslator() : WarehouseTranslator
    {
        return new WarehouseTranslator();
    }
    
    protected function getAddressTranslator() : AddressTranslator
    {
        return new AddressTranslator();
    }
    
    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }
    
    protected function getMemberOrderTranslator() : MemberOrderTranslator
    {
        return new MemberOrderTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullAmazonFTL::getInstance();
    }

    public function objectToArray($amazonFTL, array $keys = array())
    {
        if (!$amazonFTL instanceof AmazonFTL) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'member' => array('id', 'userName'),
                'pickupWarehouse' => array('id', 'code'),
                'address' => array('id', 'firstAddress', 'secondAddress', 'phone', 'postalCode'),
                'idfPickup',
                'palletNumber',
                'itemNumber',
                'items',
                'weight',
                'poNumber',
                'goodsValue',
                'pickupDate',
                'readyTime',
                'closeTime',
                'pickupNumber',
                'remark',
                'isaNumber',
                'frozen',
                'price',
                'priceRemark',
                'pickupAddressType',
                'deliveryAddressType',
                'priceApiIdentify',
                'priceApiIndex',
                'priceApiRecordId',
                'targetWarehouse' => array('id', 'code'),
                'staff' => array('id', 'name'),
                'memberOrder' => array('id', 'number', 'type', 'attribute'),
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($amazonFTL->getId());
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $amazonFTL->getMember(),
                $keys['member']
            );
        }
        if (isset($keys['pickupWarehouse'])) {
            $expression['pickupWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $amazonFTL->getPickupWarehouse(),
                $keys['pickupWarehouse']
            );
        }
        if (isset($keys['address'])) {
            $expression['address'] = $this->getAddressTranslator()->objectToArray(
                $amazonFTL->getAddress(),
                $keys['address']
            );
        }
        if (isset($keys['staff']) && ($amazonFTL->getStaff() == null)) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $amazonFTL->getStaff(),
                $keys['staff']
            );
        }
        if (isset($keys['targetWarehouse'])) {
            $expression['targetWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $amazonFTL->getTargetWarehouse(),
                $keys['targetWarehouse']
            );
        }
        if (isset($keys['memberOrder'])) {
            $expression['memberOrder'] = $this->getMemberOrderTranslator()->objectToArray(
                $amazonFTL->getMemberOrder(),
                $keys['memberOrder']
            );
        }

        if (in_array('number', $keys)) {
            $expression['number'] = $amazonFTL->getNumber();
        }
        if (in_array('palletNumber', $keys)) {
            $expression['palletNumber'] = $amazonFTL->getPalletNumber();
        }
        if (in_array('itemNumber', $keys)) {
            $expression['itemNumber'] = $amazonFTL->getItemNumber();
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $amazonFTL->getItems();
        }
        if (in_array('weight', $keys)) {
            $expression['weight'] = $amazonFTL->getWeight();
        }
        if (in_array('goodsValue', $keys)) {
            $expression['goodsValue'] = $amazonFTL->getGoodsValue();
        }
        if (in_array('pickupNumber', $keys)) {
            $expression['pickupNumber'] = $amazonFTL->getPickupNumber();
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = $amazonFTL->getRemark();
        }
        if (in_array('isaNumber', $keys)) {
            $expression['isaNumber'] = $amazonFTL->getIsaNumber();
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $amazonFTL->getPrice();
        }
        if (in_array('priceRemark', $keys)) {
            list($priceRemark, $priceRemarkEn) = $this->convertPriceRemark($amazonFTL->getPriceRemark());
            $expression['priceRemark'] = $priceRemark;
            $expression['priceRemarkEn'] = $priceRemarkEn;
        }
        if (in_array('pickupAddressType', $keys)) {
            $expression['pickupAddressType'] = $amazonFTL->getPickupAddressType();
        }
        if (in_array('deliveryAddressType', $keys)) {
            $expression['deliveryAddressType'] = $amazonFTL->getDeliveryAddressType();
        }
        if (in_array('priceApiIdentify', $keys)) {
            $expression['priceApiIdentify'] = $amazonFTL->getPriceApiIdentify();
        }
        if (in_array('priceApiIndex', $keys)) {
            $expression['priceApiIndex'] = $amazonFTL->getPriceApiIndex();
        }
        if (in_array('priceApiRecordId', $keys)) {
            $expression['priceApiRecordId'] = $amazonFTL->getPriceApiRecordId();
        }

        if (in_array('idfPickup', $keys)) {
            $expression['idfPickup'] = $this->typeFormatConversion(
                $amazonFTL->getIdfPickup(),
                AmazonFTL::IDF_PICKUP_CN
            );
        }
        if (in_array('frozen', $keys)) {
            $expression['frozen'] = $this->typeFormatConversion(
                $amazonFTL->getFrozen(),
                AmazonFTL::FROZEN_CN
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $amazonFTL->getStatus(),
                AmazonFTL::A_STATUS_CN
            );
        }

        if (in_array('pickupDate', $keys)) {
            $pickupDate = $amazonFTL->getPickupDate();
            $expression['pickupDate'] = $pickupDate;
            $expression['pickupDateFormatConvert'] = !empty($pickupDate)
            ? $this->convertTimeZone('Y-m-d', $pickupDate)
            : '';
        }
        if (in_array('readyTime', $keys)) {
            $readyTime = $amazonFTL->getReadyTime();
            $expression['readyTime'] = $readyTime;
            $expression['readyTimeFormatConvert'] = !empty($readyTime)
            ? $this->convertTimeZone('H:i', $readyTime)
            : '';
        }
        if (in_array('closeTime', $keys)) {
            $closeTime = $amazonFTL->getCloseTime();
            $expression['closeTime'] = $closeTime;
            $expression['closeTimeFormatConvert'] = !empty($closeTime)
            ? $this->convertTimeZone('H:i', $closeTime)
            : '';
        }
        if (in_array('createTime', $keys)) {
            $createTime = $amazonFTL->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }
        if (in_array('updateTime', $keys)) {
            $updateTime = $amazonFTL->getUpdateTime();
            $expression['updateTime'] = $updateTime;
            $expression['updateTimeFormatConvert'] = !empty($updateTime)
            ? $this->convertTimeZone('Y-m-d H:i', $updateTime)
            : '';
        }

        return $expression;
    }
}
