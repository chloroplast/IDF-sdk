<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\DispatchOrder;
use Sdk\Order\Model\NullDispatchOrder;

use Sdk\CarType\Translator\CarTypeTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DispatchOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getCarTypeTranslator() : CarTypeTranslator
    {
        return new CarTypeTranslator();
    }

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullDispatchOrder::getInstance();
    }

    public function arrayToObject(array $expression, $dispatchOrder = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($dispatchOrder == null) {
            $dispatchOrder = new DispatchOrder();
        }
        
        if (isset($expression['id'])) {
            $dispatchOrder->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['carType'])) {
            $carType = $this->getCarTypeTranslator()->arrayToObject($expression['carType']);
            $dispatchOrder->setCarType($carType);
        }
        if (isset($expression['staff'])) {
            $staff = $this->getStaffTranslator()->arrayToObject($expression['staff']);
            $dispatchOrder->setStaff($staff);
        }
        if (isset($expression['number'])) {
            $dispatchOrder->setNumber($expression['number']);
        }
        if (isset($expression['totalWeight'])) {
            $dispatchOrder->setTotalWeight($expression['totalWeight']);
        }
        if (isset($expression['totalPalletNumber'])) {
            $dispatchOrder->setTotalPalletNumber($expression['totalPalletNumber']);
        }
        if (isset($expression['totalOrderNumber'])) {
            $dispatchOrder->setTotalOrderNumber($expression['totalOrderNumber']);
        }
        if (isset($expression['dispatchAttachments'])) {
            $dispatchOrder->setDispatchAttachments($expression['dispatchAttachments']);
        }
        if (isset($expression['acceptAttachments'])) {
            $dispatchOrder->setAcceptAttachments($expression['acceptAttachments']);
        }
        if (isset($expression['references'])) {
            $dispatchOrder->setReferences($expression['references']);
        }
        if (isset($expression['isaNumber'])) {
            $dispatchOrder->setIsaNumber($expression['isaNumber']);
        }
        if (isset($expression['deliveryDate'])) {
            $dispatchOrder->setDeliveryDate($expression['deliveryDate']);
        }
        if (isset($expression['pickupDate'])) {
            $dispatchOrder->setPickupDate($expression['pickupDate']);
        }
        if (isset($expression['closePage'])) {
            $dispatchOrder->setClosePage($expression['closePage']);
        }
        if (isset($expression['status']['id'])) {
            $dispatchOrder->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $dispatchOrder->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $dispatchOrder->setUpdateTime($expression['updateTime']);
        }
        if (isset($expression['statusTime'])) {
            $dispatchOrder->setStatusTime($expression['statusTime']);
        }

        return $dispatchOrder;
    }

    public function objectToArray($dispatchOrder, array $keys = array())
    {
        if (!$dispatchOrder instanceof DispatchOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'carType' => array('id', 'name'),
                'staff' => array('id', 'name'),
                'number',
                'totalWeight',
                'totalPalletNumber',
                'totalOrderNumber',
                'dispatchAttachments',
                'acceptAttachments',
                'references',
                'isaNumber',
                'deliveryDate',
                'pickupDate',
                'closePage',
                'status',
                'createTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($dispatchOrder->getId());
        }
        if (isset($keys['carType'])) {
            $expression['carType'] = $this->getCarTypeTranslator()->objectToArray(
                $dispatchOrder->getCarType(),
                $keys['carType']
            );
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $dispatchOrder->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $dispatchOrder->getNumber();
        }
        if (in_array('totalWeight', $keys)) {
            $expression['totalWeight'] = $dispatchOrder->getTotalWeight();
        }
        if (in_array('totalPalletNumber', $keys)) {
            $expression['totalPalletNumber'] = $dispatchOrder->getTotalPalletNumber();
        }
        if (in_array('totalOrderNumber', $keys)) {
            $expression['totalOrderNumber'] = $dispatchOrder->getTotalOrderNumber();
        }
        if (in_array('references', $keys)) {
            $expression['references'] = $dispatchOrder->getReferences();
        }
        if (in_array('dispatchAttachments', $keys)) {
            $expression['dispatchAttachments'] = $dispatchOrder->getDispatchAttachments();
        }
        if (in_array('acceptAttachments', $keys)) {
            $expression['acceptAttachments'] = $dispatchOrder->getAcceptAttachments();
        }
        if (in_array('isaNumber', $keys)) {
            $expression['isaNumber'] = $dispatchOrder->getIsaNumber();
        }
        if (in_array('deliveryDate', $keys)) {
            $deliveryDate = $dispatchOrder->getDeliveryDate();
            $expression['deliveryDate'] = $deliveryDate;
            $expression['deliveryDateFormatConvert'] = !empty($deliveryDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryDate)
            : '';
        }
        if (in_array('pickupDate', $keys)) {
            $pickupDate = $dispatchOrder->getPickupDate();
            $expression['pickupDate'] = $pickupDate;
            $expression['pickupDateFormatConvert'] = !empty($pickupDate)
            ? $this->convertTimeZone('Y-m-d', $pickupDate)
            : '';
        }
        if (in_array('closePage', $keys)) {
            $expression['closePage'] = $dispatchOrder->getClosePage();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $dispatchOrder->getStatus(),
                AmazonLTL::A_STATUS_CN
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $dispatchOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
