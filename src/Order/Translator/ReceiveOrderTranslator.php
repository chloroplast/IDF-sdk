<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\ReceiveOrder;
use Sdk\Order\Model\NullReceiveOrder;

use Sdk\CarType\Translator\CarTypeTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ReceiveOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getCarTypeTranslator() : CarTypeTranslator
    {
        return new CarTypeTranslator();
    }

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullReceiveOrder::getInstance();
    }

    public function arrayToObject(array $expression, $receiveOrder = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($receiveOrder == null) {
            $receiveOrder = new ReceiveOrder();
        }
        
        if (isset($expression['id'])) {
            $receiveOrder->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['carType'])) {
            $carType = $this->getCarTypeTranslator()->arrayToObject($expression['carType']);
            $receiveOrder->setCarType($carType);
        }
        if (isset($expression['staff'])) {
            $staff = $this->getStaffTranslator()->arrayToObject($expression['staff']);
            $receiveOrder->setStaff($staff);
        }
        if (isset($expression['number'])) {
            $receiveOrder->setNumber($expression['number']);
        }
        if (isset($expression['totalWeight'])) {
            $receiveOrder->setTotalWeight($expression['totalWeight']);
        }
        if (isset($expression['totalPalletNumber'])) {
            $receiveOrder->setTotalPalletNumber($expression['totalPalletNumber']);
        }
        if (isset($expression['totalOrderNumber'])) {
            $receiveOrder->setTotalOrderNumber($expression['totalOrderNumber']);
        }
        if (isset($expression['pickUpAttachments'])) {
            $receiveOrder->setPickUpAttachments($expression['pickUpAttachments']);
        }
        if (isset($expression['references'])) {
            $receiveOrder->setReferences($expression['references']);
        }
        if (isset($expression['pickupDate'])) {
            $receiveOrder->setPickupDate($expression['pickupDate']);
        }
        if (isset($expression['deliveryDate'])) {
            $receiveOrder->setDeliveryDate($expression['deliveryDate']);
        }
        if (isset($expression['status']['id'])) {
            $receiveOrder->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $receiveOrder->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $receiveOrder->setUpdateTime($expression['updateTime']);
        }
        if (isset($expression['statusTime'])) {
            $receiveOrder->setStatusTime($expression['statusTime']);
        }

        return $receiveOrder;
    }

    public function objectToArray($receiveOrder, array $keys = array())
    {
        if (!$receiveOrder instanceof ReceiveOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'carType' => array('id', 'name'),
                'staff' => array('id', 'name'),
                'number',
                'totalWeight',
                'totalPalletNumber',
                'totalOrderNumber',
                'pickUpAttachments',
                'stockInAttachments',
                'references',
                'pickupDate',
                'deliveryDate',
                'status',
                'createTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($receiveOrder->getId());
        }
        if (isset($keys['carType'])) {
            $expression['carType'] = $this->getCarTypeTranslator()->objectToArray(
                $receiveOrder->getCarType(),
                $keys['carType']
            );
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $receiveOrder->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $receiveOrder->getNumber();
        }
        if (in_array('totalWeight', $keys)) {
            $expression['totalWeight'] = $receiveOrder->getTotalWeight();
        }
        if (in_array('totalPalletNumber', $keys)) {
            $expression['totalPalletNumber'] = $receiveOrder->getTotalPalletNumber();
        }
        if (in_array('totalOrderNumber', $keys)) {
            $expression['totalOrderNumber'] = $receiveOrder->getTotalOrderNumber();
        }
        if (in_array('references', $keys)) {
            $expression['references'] = $receiveOrder->getReferences();
        }
        if (in_array('pickUpAttachments', $keys)) {
            $expression['pickUpAttachments'] = $receiveOrder->getPickUpAttachments();
        }
        if (in_array('stockInAttachments', $keys)) {
            $expression['stockInAttachments'] = $receiveOrder->getStockInAttachments();
        }
        if (in_array('pickupDate', $keys)) {
            $pickupDate = $receiveOrder->getPickupDate();
            $expression['pickupDate'] = $pickupDate;
            $expression['pickupDateFormatConvert'] = !empty($pickupDate)
            ? $this->convertTimeZone('Y-m-d', $pickupDate)
            : '';
        }
        if (in_array('deliveryDate', $keys)) {
            $deliveryDate = $receiveOrder->getDeliveryDate();
            $expression['deliveryDate'] = $deliveryDate;
            $expression['deliveryDateFormatConvert'] = !empty($deliveryDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryDate)
            : '';
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $receiveOrder->getStatus(),
                AmazonLTL::A_STATUS_CN
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $receiveOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
