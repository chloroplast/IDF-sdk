<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\ItemLTL;
use Sdk\Order\Model\NullItemLTL;

class ItemLTLTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullItemLTL::getInstance();
    }

    public function objectToArray($itemLTL, array $keys = array())
    {
        if (!$itemLTL instanceof ItemLTL) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'length',
                'width',
                'height',
                'weight',
                'weightLB',
                'weightUnit',
                'goodsLevel',
                'volumeInch',
            );
        }

        $expression = array();

        if (in_array('length', $keys)) {
            $expression['length'] = $itemLTL->getLength();
        }
        if (in_array('width', $keys)) {
            $expression['width'] = $itemLTL->getWidth();
        }
        if (in_array('height', $keys)) {
            $expression['height'] = $itemLTL->getHeight();
        }
        if (in_array('weight', $keys)) {
            $expression['weight'] = $itemLTL->getWeight();
        }
        if (in_array('weightLB', $keys)) {
            $expression['weightLB'] = $itemLTL->getWeightLB();
        }
        if (in_array('weightUnit', $keys)) {
            $expression['weightUnit'] = $itemLTL->getWeightUnit();
        }
        if (in_array('goodsLevel', $keys)) {
            $expression['goodsLevel'] = $itemLTL->getGoodsLevel();
        }
        if (in_array('volumeInch', $keys)) {
            $expression['volumeInch'] = $itemLTL->getVolumeInch();
        }

        return $expression;
    }
}
