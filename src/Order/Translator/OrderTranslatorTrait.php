<?php
namespace Sdk\Order\Translator;

trait OrderTranslatorTrait
{
    // 10  StrategyAmazonLTLBase   亚马逊散板基础策略
    // 15  StrategyDimension   体积
    // 16  StrategyWeight  重量
    // 17  StrategyDimension   IDF自提
    // 18  StrategyGrade   用户VIP
    // 13  StrategyOrderLTLBase   非亚马逊散板基础策略
    // 12  StrategyOrderFTLBase   非亚马逊整板基础策略
    // 11  StrategyAmazonFTLBase   亚马逊整板基础策略
    protected function convertPriceRemark(array $priceRemark) : array
    {
        $priceRemarkStr = '';
        $priceRemarkEnStr = '';
        foreach ($priceRemark as $key => $value) {
            if ($value['identify'] == 10) {
                $priceRemarkStr .= '基础价格：$'.$value['price'].'；';
                $priceRemarkEnStr .= 'Basic price: $'.$value['price'].'; ';
            }
            if ($value['identify'] == 15) {
                $priceRemarkStr .= '体积价格：$'.$value['price'].'；';
                $priceRemarkEnStr .= 'Volume price: $'.$value['price'].'; ';
            }
            if ($value['identify'] == 16) {
                $priceRemarkStr .= '重量价格：$'.$value['price'].'；';
                $priceRemarkEnStr .= 'Weight price: $'.$value['price'].'; ';
            }
            if ($value['identify'] == 17) {
                $priceRemarkStr .= 'IDF自提价格：$'.$value['price'].'；';
                $priceRemarkEnStr .= 'IDF self pickup price: $'.$value['price'].'; ';
            }
            if ($value['identify'] == 18) {
                $priceRemarkStr .= 'VIP折扣价格：享受'.$value['percentage'].'折；';
                $priceRemarkEnStr .= 'VIP discount price: enjoy '.$value['percentage'].' discount; ';
            }
            if ($value['identify'] == 13) {
                $priceRemarkStr .= '基础策略：享受'.$value['percentage'].'折；';
                $priceRemarkEnStr .= 'Basic price: enjoy '.$value['percentage'].' discount; ';
            }
            if ($value['identify'] == 12) {
                $priceRemarkStr .= '基础策略：享受'.$value['percentage'].'折；';
                $priceRemarkEnStr .= 'Basic price: enjoy '.$value['percentage'].' discount; ';
            }
            if ($value['identify'] == 11) {
                $priceRemarkStr .= '基础策略：享受'.$value['percentage'].'折；';
                $priceRemarkEnStr .= 'Basic price: enjoy '.$value['percentage'].' discount; ';
            }
        }
        return array($priceRemarkStr, $priceRemarkEnStr);
    }
}
