<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\NullMemberOrder;

use Sdk\User\Member\Translator\MemberTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class MemberOrderTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullMemberOrder::getInstance();
    }

    public function arrayToObject(array $expression, $memberOrder = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($memberOrder == null) {
            $memberOrder = new MemberOrder();
        }
        
        if (isset($expression['id'])) {
            $memberOrder->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['member'])) {
            $member = $this->getMemberTranslator()->arrayToObject($expression['member']);
            $memberOrder->setMember($member);
        }
        if (isset($expression['staff'])) {
            $staff = $this->getStaffTranslator()->arrayToObject($expression['staff']);
            $memberOrder->setStaff($staff);
        }
        if (isset($expression['number'])) {
            $memberOrder->setNumber($expression['number']);
        }
        if (isset($expression['attribute'])) {
            $memberOrder->setAttribute($expression['attribute']);
        }
        if (isset($expression['type'])) {
            $memberOrder->setType($expression['type']);
        }
        if (isset($expression['idfPickup'])) {
            $memberOrder->setIdfPickup($expression['idfPickup']);
        }
        if (isset($expression['status']['id'])) {
            $memberOrder->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $memberOrder->setCreateTime($expression['createTime']);
        }

        return $memberOrder;
    }

    public function objectToArray($memberOrder, array $keys = array())
    {
        if (!$memberOrder instanceof MemberOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'member',
                'staff',
                'number',
                'price',
                'attribute',
                'type',
                'idfPickup',
                'status',
                'createTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($memberOrder->getId());
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $memberOrder->getMember(),
                $keys['member']
            );
        }
        if (isset($keys['staff'])) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $memberOrder->getStaff(),
                $keys['staff']
            );
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $memberOrder->getNumber();
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $memberOrder->getPrice();
        }
        if (in_array('attribute', $keys)) {
            $expression['attribute'] = $this->typeFormatConversion(
                $memberOrder->getAttribute(),
                MemberOrder::ATTRIBUTE_CN
            );
        }
        if (in_array('type', $keys)) {
            if ($memberOrder->getAttribute() == MemberOrder::ATTRIBUTE['AMAZON']) {
                $expression['type'] = $this->typeFormatConversion(
                    $memberOrder->getType(),
                    MemberOrder::AMAZON_CN
                );
            }
            if ($memberOrder->getAttribute() == MemberOrder::ATTRIBUTE['NO_AMAZON']) {
                $expression['type'] = $this->typeFormatConversion(
                    $memberOrder->getType(),
                    MemberOrder::NO_AMAZON_CN
                );
            }
        }
        if (in_array('idfPickup', $keys)) {
            $expression['idfPickup'] = $this->typeFormatConversion(
                $memberOrder->getIdfPickup(),
                MemberOrder::IDF_PICKUP_CN
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $memberOrder->getStatus(),
                MemberOrder::A_STATUS_CN
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $memberOrder->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
