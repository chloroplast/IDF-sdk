<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\NullOrderLTL;

use Sdk\User\Member\Translator\MemberRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;
use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;
use Sdk\Address\Translator\AddressRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderLTLRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }
    
    protected function getWarehouseRestfulTranslator() : WarehouseRestfulTranslator
    {
        return new WarehouseRestfulTranslator();
    }
    
    protected function getAddressRestfulTranslator() : AddressRestfulTranslator
    {
        return new AddressRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    protected function getMemberOrderRestfulTranslator() : MemberOrderRestfulTranslator
    {
        return new MemberOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $orderLTL = null)
    {
        if (empty($expression)) {
            return NullOrderLTL::getInstance();
        }

        if ($orderLTL == null) {
            $orderLTL = new OrderLTL();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $orderLTL->setId($data['id']);
        }

        if (isset($attributes['number'])) {
            $orderLTL->setNumber($attributes['number']);
        }
        if (isset($attributes['bookingPickupDate'])) {
            $orderLTL->setBookingPickupDate($attributes['bookingPickupDate']);
        }
        if (isset($attributes['pickupZipCode'])) {
            $orderLTL->setPickupZipCode($attributes['pickupZipCode']);
        }
        if (isset($attributes['pickupAddressType'])) {
            $orderLTL->setPickupAddressType($attributes['pickupAddressType']);
        }
        if (isset($attributes['deliveryZipCode'])) {
            $orderLTL->setDeliveryZipCode($attributes['deliveryZipCode']);
        }
        if (isset($attributes['deliveryAddressType'])) {
            $orderLTL->setDeliveryAddressType($attributes['deliveryAddressType']);
        }
        if (isset($attributes['pickupNeedTransport'])) {
            $orderLTL->setPickupNeedTransport($attributes['pickupNeedTransport']);
        }
        if (isset($attributes['pickupNeedLiftgate'])) {
            $orderLTL->setPickupNeedLiftgate($attributes['pickupNeedLiftgate']);
        }
        if (isset($attributes['deliveryNeedTransport'])) {
            $orderLTL->setDeliveryNeedTransport($attributes['deliveryNeedTransport']);
        }
        if (isset($attributes['deliveryNeedLiftgate'])) {
            $orderLTL->setDeliveryNeedLiftgate($attributes['deliveryNeedLiftgate']);
        }
        if (isset($attributes['poNumber'])) {
            $orderLTL->setPoNumber($attributes['poNumber']);
        }
        if (isset($attributes['items'])) {
            $orderLTL->setItems($attributes['items']);
        }
        if (isset($attributes['pickupVehicleType'])) {
            $orderLTL->setPickupVehicleType($attributes['pickupVehicleType']);
        }
        if (isset($attributes['readyTime'])) {
            $orderLTL->setReadyTime($attributes['readyTime']);
        }
        if (isset($attributes['closeTime'])) {
            $orderLTL->setCloseTime($attributes['closeTime']);
        }
        if (isset($attributes['deliveryName'])) {
            $orderLTL->setDeliveryName($attributes['deliveryName']);
        }
        if (isset($attributes['deliveryReadyTime'])) {
            $orderLTL->setDeliveryReadyTime($attributes['deliveryReadyTime']);
        }
        if (isset($attributes['deliveryCloseTime'])) {
            $orderLTL->setDeliveryCloseTime($attributes['deliveryCloseTime']);
        }
        if (isset($attributes['weekendDelivery'])) {
            $orderLTL->setWeekendDelivery($attributes['weekendDelivery']);
        }
        if (isset($attributes['deliveryDescription'])) {
            $orderLTL->setDeliveryDescription($attributes['deliveryDescription']);
        }
        if (isset($attributes['remark'])) {
            $orderLTL->setRemark($attributes['remark']);
        }
        if (isset($attributes['pickupRequirement'])) {
            $orderLTL->setPickupRequirement($attributes['pickupRequirement']);
        }
        if (isset($attributes['deliveryRequirement'])) {
            $orderLTL->setDeliveryRequirement($attributes['deliveryRequirement']);
        }
        if (isset($attributes['idfPickup'])) {
            $orderLTL->setIdfPickup($attributes['idfPickup']);
        }
        if (isset($attributes['position'])) {
            $orderLTL->setPosition($attributes['position']);
        }
        if (isset($attributes['stockInNumber'])) {
            $orderLTL->setStockInNumber($attributes['stockInNumber']);
        }
        if (isset($attributes['stockInTime'])) {
            $orderLTL->setStockInTime($attributes['stockInTime']);
        }
        if (isset($attributes['frozen'])) {
            $orderLTL->setFrozen($attributes['frozen']);
        }
        if (isset($attributes['priceApiIdentify'])) {
            $orderLTL->setPriceApiIdentify($attributes['priceApiIdentify']);
        }
        if (isset($attributes['priceApiIndex'])) {
            $orderLTL->setPriceApiIndex($attributes['priceApiIndex']);
        }
        if (isset($attributes['priceApiRecordId'])) {
            $orderLTL->setPriceApiRecordId($attributes['priceApiRecordId']);
        }
        if (isset($attributes['price'])) {
            $orderLTL->setPrice($attributes['price']);
        }
        if (isset($attributes['priceRemark'])) {
            $orderLTL->setPriceRemark($attributes['priceRemark']);
        }
        if (isset($attributes['pickupEndDate'])) {
            $orderLTL->setPickupEndDate($attributes['pickupEndDate']);
        }
        if (isset($attributes['pickupStartDate'])) {
            $orderLTL->setPickupStartDate($attributes['pickupStartDate']);
        }
        if (isset($attributes['deliveryEndDate'])) {
            $orderLTL->setDeliveryEndDate($attributes['deliveryEndDate']);
        }
        if (isset($attributes['deliveryStartDate'])) {
            $orderLTL->setDeliveryStartDate($attributes['deliveryStartDate']);
        }
        if (isset($attributes['palletNumber'])) {
            $orderLTL->setPalletNumber($attributes['palletNumber']);
        }
        if (isset($attributes['status'])) {
            $orderLTL->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $orderLTL->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $orderLTL->setUpdateTime($attributes['updateTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['member'])) {
            $memberArray = $this->relationshipFill($relationships['member'], $included);
            $member = $this->getMemberRestfulTranslator()->arrayToObject($memberArray);
            $orderLTL->setMember($member);
        }

        if (isset($relationships['pickupWarehouseSnapshot'])) {
            $pickupWarehouseArray = $this->relationshipFill($relationships['pickupWarehouseSnapshot'], $included);
            $pickupWarehouse = $this->getWarehouseRestfulTranslator()->arrayToObject($pickupWarehouseArray);
            $orderLTL->setPickupWarehouse($pickupWarehouse);
        }

        if (isset($relationships['addressSnapshot'])) {
            $addressArray = $this->relationshipFill($relationships['addressSnapshot'], $included);
            $address = $this->getAddressRestfulTranslator()->arrayToObject($addressArray);
            $orderLTL->setAddress($address);
        }
        if (isset($relationships['deliveryAddressSnapshot'])) {
            $deliveryAddressArray = $this->relationshipFill($relationships['deliveryAddressSnapshot'], $included);
            $deliveryAddress = $this->getAddressRestfulTranslator()->arrayToObject($deliveryAddressArray);
            $orderLTL->setDeliveryAddress($deliveryAddress);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $orderLTL->setStaff($staff);
        }

        if (isset($relationships['memberOrder'])) {
            $memberOrderArray = $this->relationshipFill($relationships['memberOrder'], $included);
            $memberOrder = $this->getMemberOrderRestfulTranslator()->arrayToObject($memberOrderArray);
            $orderLTL->setMemberOrder($memberOrder);
        }

        return $orderLTL;
    }

    public function objectToArray($orderLTL, array $keys = array())
    {
        if (!$orderLTL instanceof OrderLTL) {
            return array();
        }


        if (empty($keys)) {
            $keys = array(
                'id',
                'member',
                'pickupWarehouse',
                'address',
                'deliveryAddress',
                'bookingPickupDate',
                'pickupZipCode',
                'pickupAddressType',
                'deliveryZipCode',
                'deliveryAddressType',
                'pickupNeedTransport',
                'pickupNeedLiftgate',
                'deliveryNeedTransport',
                'deliveryNeedLiftgate',
                'poNumber',
                'items',
                'pickupVehicleType',
                'readyTime',
                'closeTime',
                'deliveryName',
                'deliveryReadyTime',
                'deliveryCloseTime',
                'weekendDelivery',
                'deliveryDescription',
                'remark',
                'pickupRequirement',
                'deliveryRequirement',
                'idfPickup',
                'position',
                'stockInNumber',
                'stockInTime',
                'carType',
                'priceApiRecordId',
                'priceApiIdentify',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'orderLTL'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $orderLTL->getId();
        }

        $attributes = array();
        if (in_array('bookingPickupDate', $keys)) {
            $attributes['bookingPickupDate'] = $orderLTL->getBookingPickupDate();
        }
        if (in_array('pickupZipCode', $keys)) {
            $attributes['pickupZipCode'] = $orderLTL->getPickupZipCode();
        }
        if (in_array('pickupAddressType', $keys)) {
            $attributes['pickupAddressType'] = $orderLTL->getPickupAddressType();
        }
        if (in_array('deliveryZipCode', $keys)) {
            $attributes['deliveryZipCode'] = $orderLTL->getDeliveryZipCode();
        }
        if (in_array('deliveryAddressType', $keys)) {
            $attributes['deliveryAddressType'] = $orderLTL->getDeliveryAddressType();
        }
        if (in_array('pickupNeedTransport', $keys)) {
            $attributes['pickupNeedTransport'] = $orderLTL->getPickupNeedTransport();
        }
        if (in_array('pickupNeedLiftgate', $keys)) {
            $attributes['pickupNeedLiftgate'] = $orderLTL->getPickupNeedLiftgate();
        }
        if (in_array('deliveryNeedTransport', $keys)) {
            $attributes['deliveryNeedTransport'] = $orderLTL->getDeliveryNeedTransport();
        }
        if (in_array('deliveryNeedLiftgate', $keys)) {
            $attributes['deliveryNeedLiftgate'] = $orderLTL->getDeliveryNeedLiftgate();
        }
        if (in_array('poNumber', $keys)) {
            $attributes['poNumber'] = $orderLTL->getPoNumber();
        }
        if (in_array('items', $keys)) {
            $attributes['items'] = $orderLTL->getItems();
        }
        if (in_array('pickupVehicleType', $keys)) {
            $attributes['pickupVehicleType'] = $orderLTL->getPickupVehicleType();
        }
        if (in_array('readyTime', $keys)) {
            $attributes['readyTime'] = $orderLTL->getReadyTime();
        }
        if (in_array('closeTime', $keys)) {
            $attributes['closeTime'] = $orderLTL->getCloseTime();
        }
        if (in_array('deliveryName', $keys)) {
            $attributes['deliveryName'] = $orderLTL->getDeliveryName();
        }
        if (in_array('deliveryReadyTime', $keys)) {
            $attributes['deliveryReadyTime'] = $orderLTL->getDeliveryReadyTime();
        }
        if (in_array('deliveryCloseTime', $keys)) {
            $attributes['deliveryCloseTime'] = $orderLTL->getDeliveryCloseTime();
        }
        if (in_array('weekendDelivery', $keys)) {
            $attributes['weekendDelivery'] = $orderLTL->getWeekendDelivery();
        }
        if (in_array('deliveryDescription', $keys)) {
            $attributes['deliveryDescription'] = $orderLTL->getDeliveryDescription();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $orderLTL->getRemark();
        }
        if (in_array('pickupRequirement', $keys)) {
            $attributes['pickupRequirement'] = $orderLTL->getPickupRequirement();
        }
        if (in_array('deliveryRequirement', $keys)) {
            $attributes['deliveryRequirement'] = $orderLTL->getDeliveryRequirement();
        }
        if (in_array('idfPickup', $keys)) {
            $attributes['idfPickup'] = $orderLTL->getIdfPickup();
        }
        if (in_array('position', $keys)) {
            $attributes['position'] = $orderLTL->getPosition();
        }
        if (in_array('stockInNumber', $keys)) {
            $attributes['stockInNumber'] = $orderLTL->getStockInNumber();
        }
        if (in_array('stockInTime', $keys)) {
            $attributes['stockInTime'] = $orderLTL->getStockInTime();
        }
        if (in_array('priceApiRecordId', $keys)) {
            $attributes['priceApiRecordId'] = $orderLTL->getPriceApiRecordId();
        }
        if (in_array('priceApiIdentify', $keys)) {
            $attributes['priceApiIdentify'] = $orderLTL->getPriceApiIdentify();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('member', $keys) && !empty($orderLTL->getMember()->getId())) {
            $memberRelationships = array(
                'type' => 'member',
                'id' => strval($orderLTL->getMember()->getId())
            );

            $expression['data']['relationships']['member']['data'] = $memberRelationships;
        }
        if (in_array('pickupWarehouse', $keys)) {
            $pickupWarehouseRelationships = array(
                'type' => 'warehouse',
                'id' => strval($orderLTL->getPickupWarehouse()->getId())
            );

            $expression['data']['relationships']['pickupWarehouse']['data'] = $pickupWarehouseRelationships;
        }
        if (in_array('address', $keys)) {
            $addressRelationships = array(
                'type' => 'address',
                'id' => strval($orderLTL->getAddress()->getId())
            );

            $expression['data']['relationships']['address']['data'] = $addressRelationships;
        }
        if (in_array('deliveryAddress', $keys)) {
            $deliveryAddressRelationships = array(
                'type' => 'address',
                'id' => strval($orderLTL->getDeliveryAddress()->getId())
            );

            $expression['data']['relationships']['deliveryAddress']['data'] = $deliveryAddressRelationships;
        }
        if (in_array('staff', $keys) && !empty($orderLTL->getStaff()->getId())) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($orderLTL->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }
        if (in_array('carType', $keys)) {
            $carTypeRelationships = array(
                'type' => 'carType',
                'id' => strval($orderLTL->getCarType()->getId())
            );

            $expression['data']['relationships']['carType']['data'] = $carTypeRelationships;
        }

        return $expression;
    }
}
