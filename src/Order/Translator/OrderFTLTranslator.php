<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Order\Model\OrderFTL;
use Sdk\Order\Model\NullOrderFTL;

use Sdk\User\Member\Translator\MemberTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;
use Sdk\Warehouse\Translator\WarehouseTranslator;
use Sdk\Address\Translator\AddressTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class OrderFTLTranslator implements ITranslator
{
    use TranslatorTrait, OrderTranslatorTrait;

    protected function getMemberTranslator() : MemberTranslator
    {
        return new MemberTranslator();
    }
    
    protected function getWarehouseTranslator() : WarehouseTranslator
    {
        return new WarehouseTranslator();
    }
    
    protected function getAddressTranslator() : AddressTranslator
    {
        return new AddressTranslator();
    }
    
    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }
    
    protected function getMemberOrderTranslator() : MemberOrderTranslator
    {
        return new MemberOrderTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullOrderFTL::getInstance();
    }

    public function objectToArray($orderFTL, array $keys = array())
    {
        if (!$orderFTL instanceof OrderFTL) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'number',
                'member' => array('id', 'userName'),
                'pickupWarehouse' => array('id', 'code'),
                'address' => array('id', 'firstAddress', 'secondAddress', 'phone', 'postalCode'),
                'deliveryAddress' => array('id', 'firstAddress', 'secondAddress', 'phone', 'postalCode'),
                'deliveryName',
                'weekendDelivery',
                'palletNumber',
                'itemNumber',
                'items',
                'weight',
                'poNumber',
                'goodsValue',
                'pickupDate',
                'readyTime',
                'closeTime',
                'pickupNumber',
                'deliveryDate',
                'deliveryReadyTime',
                'deliveryCloseTime',
                'remark',
                'frozen',
                'price',
                'priceRemark',
                'pickupAddressType',
                'deliveryAddressType',
                'priceApiIdentify',
                'priceApiIndex',
                'priceApiRecordId',
                'staff' => array('id', 'name'),
                'memberOrder' => array('id', 'number', 'type', 'attribute'),
                'status',
                'createTime',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($orderFTL->getId());
        }
        if (isset($keys['member'])) {
            $expression['member'] = $this->getMemberTranslator()->objectToArray(
                $orderFTL->getMember(),
                $keys['member']
            );
        }
        if (isset($keys['pickupWarehouse'])) {
            $expression['pickupWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $orderFTL->getPickupWarehouse(),
                $keys['pickupWarehouse']
            );
        }
        if (isset($keys['address'])) {
            $expression['address'] = $this->getAddressTranslator()->objectToArray(
                $orderFTL->getAddress(),
                $keys['address']
            );
        }
        if (isset($keys['deliveryAddress'])) {
            $expression['deliveryAddress'] = $this->getAddressTranslator()->objectToArray(
                $orderFTL->getDeliveryAddress(),
                $keys['deliveryAddress']
            );
        }
        if (isset($keys['staff']) && ($orderFTL->getStaff() == null)) {
            $expression['staff'] = $this->getStaffTranslator()->objectToArray(
                $orderFTL->getStaff(),
                $keys['staff']
            );
        }
        if (isset($keys['targetWarehouse'])) {
            $expression['targetWarehouse'] = $this->getWarehouseTranslator()->objectToArray(
                $orderFTL->getTargetWarehouse(),
                $keys['targetWarehouse']
            );
        }
        if (isset($keys['memberOrder'])) {
            $expression['memberOrder'] = $this->getMemberOrderTranslator()->objectToArray(
                $orderFTL->getMemberOrder(),
                $keys['memberOrder']
            );
        }
        if (in_array('deliveryName', $keys)) {
            $expression['deliveryName'] = $orderFTL->getDeliveryName();
        }
        if (in_array('number', $keys)) {
            $expression['number'] = $orderFTL->getNumber();
        }
        if (in_array('palletNumber', $keys)) {
            $expression['palletNumber'] = $orderFTL->getPalletNumber();
        }
        if (in_array('itemNumber', $keys)) {
            $expression['itemNumber'] = $orderFTL->getItemNumber();
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $orderFTL->getItems();
        }
        if (in_array('weight', $keys)) {
            $expression['weight'] = $orderFTL->getWeight();
        }
        if (in_array('goodsValue', $keys)) {
            $expression['goodsValue'] = $orderFTL->getGoodsValue();
        }
        if (in_array('pickupNumber', $keys)) {
            $expression['pickupNumber'] = $orderFTL->getPickupNumber();
        }
        if (in_array('remark', $keys)) {
            $expression['remark'] = $orderFTL->getRemark();
        }
        if (in_array('price', $keys)) {
            $expression['price'] = $orderFTL->getPrice();
        }
        if (in_array('priceRemark', $keys)) {
            list($priceRemark, $priceRemarkEn) = $this->convertPriceRemark($orderFTL->getPriceRemark());
            $expression['priceRemark'] = $priceRemark;
            $expression['priceRemarkEn'] = $priceRemarkEn;
        }
        if (in_array('pickupAddressType', $keys)) {
            $expression['pickupAddressType'] = $orderFTL->getPickupAddressType();
        }
        if (in_array('deliveryAddressType', $keys)) {
            $expression['deliveryAddressType'] = $orderFTL->getDeliveryAddressType();
        }
        if (in_array('priceApiIdentify', $keys)) {
            $expression['priceApiIdentify'] = $orderFTL->getPriceApiIdentify();
        }
        if (in_array('priceApiIndex', $keys)) {
            $expression['priceApiIndex'] = $orderFTL->getPriceApiIndex();
        }
        if (in_array('priceApiRecordId', $keys)) {
            $expression['priceApiRecordId'] = $orderFTL->getPriceApiRecordId();
        }

        if (in_array('weekendDelivery', $keys)) {
            $expression['weekendDelivery'] = $this->typeFormatConversion(
                $orderFTL->getWeekendDelivery(),
                OrderFTL::WEEKEND_DELIVERY_CN
            );
        }
        if (in_array('frozen', $keys)) {
            $expression['frozen'] = $this->typeFormatConversion(
                $orderFTL->getFrozen(),
                OrderFTL::FROZEN_CN
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->typeFormatConversion(
                $orderFTL->getStatus(),
                OrderFTL::A_STATUS_CN
            );
        }

        if (in_array('pickupDate', $keys)) {
            $pickupDate = $orderFTL->getPickupDate();
            $expression['pickupDate'] = $pickupDate;
            $expression['pickupDateFormatConvert'] = !empty($pickupDate)
            ? $this->convertTimeZone('Y-m-d', $pickupDate)
            : '';
        }
        if (in_array('readyTime', $keys)) {
            $readyTime = $orderFTL->getReadyTime();
            $expression['readyTime'] = $readyTime;
            $expression['readyTimeFormatConvert'] = !empty($readyTime)
            ? $this->convertTimeZone('H:i', $readyTime)
            : '';
        }
        if (in_array('closeTime', $keys)) {
            $closeTime = $orderFTL->getCloseTime();
            $expression['closeTime'] = $closeTime;
            $expression['closeTimeFormatConvert'] = !empty($closeTime)
            ? $this->convertTimeZone('H:i', $closeTime)
            : '';
        }
        if (in_array('deliveryDate', $keys)) {
            $deliveryDate = $orderFTL->getDeliveryDate();
            $expression['deliveryDate'] = $deliveryDate;
            $expression['deliveryDateFormatConvert'] = !empty($deliveryDate)
            ? $this->convertTimeZone('Y-m-d', $deliveryDate)
            : '';
        }
        if (in_array('deliveryReadyTime', $keys)) {
            $deliveryReadyTime = $orderFTL->getDeliveryReadyTime();
            $expression['deliveryReadyTime'] = $deliveryReadyTime;
            $expression['deliveryReadyTimeFormatConvert'] = !empty($deliveryReadyTime)
            ? $this->convertTimeZone('H:i', $deliveryReadyTime)
            : '';
        }
        if (in_array('deliveryCloseTime', $keys)) {
            $deliveryCloseTime = $orderFTL->getDeliveryCloseTime();
            $expression['deliveryCloseTime'] = $deliveryCloseTime;
            $expression['deliveryCloseTimeFormatConvert'] = !empty($deliveryCloseTime)
            ? $this->convertTimeZone('H:i', $deliveryCloseTime)
            : '';
        }
        if (in_array('createTime', $keys)) {
            $createTime = $orderFTL->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }
        if (in_array('updateTime', $keys)) {
            $updateTime = $orderFTL->getUpdateTime();
            $expression['updateTime'] = $updateTime;
            $expression['updateTimeFormatConvert'] = !empty($updateTime)
            ? $this->convertTimeZone('Y-m-d H:i', $updateTime)
            : '';
        }

        return $expression;
    }
}
