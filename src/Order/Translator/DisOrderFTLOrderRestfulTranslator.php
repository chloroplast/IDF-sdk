<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\DisOrderFTLOrder;
use Sdk\Order\Model\NullDisOrderFTLOrder;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class DisOrderFTLOrderRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $disOrderFTLOrder = null)
    {
        if (empty($expression)) {
            return NullDisOrderFTLOrder::getInstance();
        }

        if ($disOrderFTLOrder == null) {
            $disOrderFTLOrder = new DisOrderFTLOrder();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $disOrderFTLOrder->setId($data['id']);
        }

        if (isset($attributes['number'])) {
            $disOrderFTLOrder->setNumber($attributes['number']);
        }
        if (isset($attributes['totalWeight'])) {
            $disOrderFTLOrder->setTotalWeight($attributes['totalWeight']);
        }
        if (isset($attributes['totalPalletNumber'])) {
            $disOrderFTLOrder->setTotalPalletNumber($attributes['totalPalletNumber']);
        }
        if (isset($attributes['totalOrderNumber'])) {
            $disOrderFTLOrder->setTotalOrderNumber($attributes['totalOrderNumber']);
        }
        if (isset($attributes['references'])) {
            $disOrderFTLOrder->setReferences($attributes['references']);
        }
        if (isset($attributes['dispatchAttachments'])) {
            $disOrderFTLOrder->setDispatchAttachments($attributes['dispatchAttachments']);
        }
        if (isset($attributes['acceptAttachments'])) {
            $disOrderFTLOrder->setAcceptAttachments($attributes['acceptAttachments']);
        }
        if (isset($attributes['deliveryDate'])) {
            $disOrderFTLOrder->setDeliveryDate($attributes['deliveryDate']);
        }
        if (isset($attributes['pickupDate'])) {
            $disOrderFTLOrder->setPickupDate($attributes['pickupDate']);
        }
        if (isset($attributes['status'])) {
            $disOrderFTLOrder->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $disOrderFTLOrder->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $disOrderFTLOrder->setStaff($staff);
        }

        return $disOrderFTLOrder;
    }

    public function objectToArray($disOrderFTLOrder, array $keys = array())
    {
        if (!$disOrderFTLOrder instanceof DisOrderFTLOrder) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'orderFTL',
                'dispatchItemsAttachments',
                'acceptItemsAttachments',
                'references',
                'dispatchAttachments',
                'acceptAttachments',
                'deliveryDate',
                'pickupDate',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'dispatchOrderFTLOrders'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $disOrderFTLOrder->getId();
        }

        $attributes = array();
        if (in_array('dispatchItemsAttachments', $keys)) {
            $attributes['items'] = $disOrderFTLOrder->getDispatchItemsAttachments();
        }
        if (in_array('acceptItemsAttachments', $keys)) {
            $attributes['items'] = $disOrderFTLOrder->getAcceptItemsAttachments();
        }
        if (in_array('references', $keys)) {
            $attributes['references'] = $disOrderFTLOrder->getReferences();
        }
        if (in_array('dispatchAttachments', $keys)) {
            $attributes['attachments'] = $disOrderFTLOrder->getDispatchAttachments();
        }
        if (in_array('acceptAttachments', $keys)) {
            $attributes['attachments'] = $disOrderFTLOrder->getAcceptAttachments();
        }
        if (in_array('deliveryDate', $keys)) {
            $attributes['deliveryDate'] = $disOrderFTLOrder->getDeliveryDate();
        }
        if (in_array('pickupDate', $keys)) {
            $attributes['pickupDate'] = $disOrderFTLOrder->getPickupDate();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('orderFTL', $keys)) {
            foreach ($disOrderFTLOrder->getAmazonFTL() as $orderFTL) {
                $orderFTLRelationships[] = array(
                    'type' => 'orderFTL',
                    'id' => strval($orderFTL)
                );
            }

            $expression['data']['relationships']['orderFTL']['data'] = $orderFTLRelationships;
        }
        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($disOrderFTLOrder->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
