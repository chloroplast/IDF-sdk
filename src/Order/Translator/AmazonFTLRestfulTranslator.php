<?php
namespace Sdk\Order\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Order\Model\AmazonFTL;
use Sdk\Order\Model\NullAmazonFTL;

use Sdk\User\Member\Translator\MemberRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;
use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;
use Sdk\Address\Translator\AddressRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class AmazonFTLRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }
    
    protected function getWarehouseRestfulTranslator() : WarehouseRestfulTranslator
    {
        return new WarehouseRestfulTranslator();
    }
    
    protected function getAddressRestfulTranslator() : AddressRestfulTranslator
    {
        return new AddressRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    protected function getMemberOrderRestfulTranslator() : MemberOrderRestfulTranslator
    {
        return new MemberOrderRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $amazonFTL = null)
    {
        if (empty($expression)) {
            return NullAmazonFTL::getInstance();
        }

        if ($amazonFTL == null) {
            $amazonFTL = new AmazonFTL();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $amazonFTL->setId($data['id']);
        }

        if (isset($attributes['number'])) {
            $amazonFTL->setNumber($attributes['number']);
        }
        if (isset($attributes['idfPickup'])) {
            $amazonFTL->setIdfPickup($attributes['idfPickup']);
        }
        if (isset($attributes['palletNumber'])) {
            $amazonFTL->setPalletNumber($attributes['palletNumber']);
        }
        if (isset($attributes['itemNumber'])) {
            $amazonFTL->setItemNumber($attributes['itemNumber']);
        }
        if (isset($attributes['items'])) {
            $amazonFTL->setItems($attributes['items']);
        }
        if (isset($attributes['weight'])) {
            $amazonFTL->setWeight($attributes['weight']);
        }
        if (isset($attributes['poNumber'])) {
            $amazonFTL->setPoNumber($attributes['poNumber']);
        }
        if (isset($attributes['goodsValue'])) {
            $amazonFTL->setGoodsValue($attributes['goodsValue']);
        }
        if (isset($attributes['pickupDate'])) {
            $amazonFTL->setPickupDate($attributes['pickupDate']);
        }
        if (isset($attributes['readyTime'])) {
            $amazonFTL->setReadyTime($attributes['readyTime']);
        }
        if (isset($attributes['closeTime'])) {
            $amazonFTL->setCloseTime($attributes['closeTime']);
        }
        if (isset($attributes['pickupNumber'])) {
            $amazonFTL->setPickupNumber($attributes['pickupNumber']);
        }
        if (isset($attributes['remark'])) {
            $amazonFTL->setRemark($attributes['remark']);
        }
        if (isset($attributes['isaNumber'])) {
            $amazonFTL->setIsaNumber($attributes['isaNumber']);
        }
        if (isset($attributes['stockInTime'])) {
            $amazonFTL->setStockInTime($attributes['stockInTime']);
        }
        if (isset($attributes['frozen'])) {
            $amazonFTL->setFrozen($attributes['frozen']);
        }
        if (isset($attributes['price'])) {
            $amazonFTL->setPrice($attributes['price']);
        }
        if (isset($attributes['priceRemark'])) {
            $amazonFTL->setPriceRemark($attributes['priceRemark']);
        }
        if (isset($attributes['pickupAddressType'])) {
            $amazonFTL->setPickupAddressType($attributes['pickupAddressType']);
        }
        if (isset($attributes['deliveryAddressType'])) {
            $amazonFTL->setDeliveryAddressType($attributes['deliveryAddressType']);
        }
        if (isset($attributes['priceApiIdentify'])) {
            $amazonFTL->setPriceApiIdentify($attributes['priceApiIdentify']);
        }
        if (isset($attributes['priceApiIndex'])) {
            $amazonFTL->setPriceApiIndex($attributes['priceApiIndex']);
        }
        if (isset($attributes['priceApiRecordId'])) {
            $amazonFTL->setPriceApiRecordId($attributes['priceApiRecordId']);
        }
        if (isset($attributes['status'])) {
            $amazonFTL->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $amazonFTL->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $amazonFTL->setUpdateTime($attributes['updateTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['member'])) {
            $memberArray = $this->relationshipFill($relationships['member'], $included);
            $member = $this->getMemberRestfulTranslator()->arrayToObject($memberArray);
            $amazonFTL->setMember($member);
        }

        if (isset($relationships['pickupWarehouseSnapshot'])) {
            $pickupWarehouseArray = $this->relationshipFill($relationships['pickupWarehouseSnapshot'], $included);
            $pickupWarehouse = $this->getWarehouseRestfulTranslator()->arrayToObject($pickupWarehouseArray);
            $amazonFTL->setPickupWarehouse($pickupWarehouse);
        }

        if (isset($relationships['addressSnapshot'])) {
            $addressArray = $this->relationshipFill($relationships['addressSnapshot'], $included);
            $address = $this->getAddressRestfulTranslator()->arrayToObject($addressArray);
            $amazonFTL->setAddress($address);
        }

        if (isset($relationships['staff'])) {
            $staffArray = $this->relationshipFill($relationships['staff'], $included);
            $staff = $this->getStaffRestfulTranslator()->arrayToObject($staffArray);
            $amazonFTL->setStaff($staff);
        }

        if (isset($relationships['memberOrder'])) {
            $memberOrderArray = $this->relationshipFill($relationships['memberOrder'], $included);
            $memberOrder = $this->getMemberOrderRestfulTranslator()->arrayToObject($memberOrderArray);
            $amazonFTL->setMemberOrder($memberOrder);
        }

        if (isset($relationships['targetWarehouseSnapshot'])) {
            $targetWarehouseArray = $this->relationshipFill($relationships['targetWarehouseSnapshot'], $included);
            $targetWarehouse = $this->getWarehouseRestfulTranslator()->arrayToObject($targetWarehouseArray);
            $amazonFTL->setTargetWarehouse($targetWarehouse);
        }

        return $amazonFTL;
    }

    public function objectToArray($amazonFTL, array $keys = array())
    {
        if (!$amazonFTL instanceof AmazonFTL) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'member',
                'pickupWarehouse',
                'targetWarehouse',
                'address',
                'items',
                'pickupDate',
                'readyTime',
                'closeTime',
                'pickupNumber',
                'remark',
                'pickupAddressType',
                'deliveryAddressType',
                'priceApiRecordId',
                'priceApiIdentify',
                'isaNumber',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'amazonFTL'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $amazonFTL->getId();
        }

        $attributes = array();
        if (in_array('items', $keys)) {
            $attributes['items'] = $amazonFTL->getItems();
        }
        if (in_array('pickupDate', $keys)) {
            $attributes['pickupDate'] = $amazonFTL->getPickupDate();
        }
        if (in_array('readyTime', $keys)) {
            $attributes['readyTime'] = $amazonFTL->getReadyTime();
        }
        if (in_array('closeTime', $keys)) {
            $attributes['closeTime'] = $amazonFTL->getCloseTime();
        }
        if (in_array('pickupNumber', $keys)) {
            $attributes['pickupNumber'] = $amazonFTL->getPickupNumber();
        }
        if (in_array('remark', $keys)) {
            $attributes['remark'] = $amazonFTL->getRemark();
        }
        if (in_array('pickupAddressType', $keys)) {
            $attributes['pickupAddressType'] = $amazonFTL->getPickupAddressType();
        }
        if (in_array('deliveryAddressType', $keys)) {
            $attributes['deliveryAddressType'] = $amazonFTL->getDeliveryAddressType();
        }
        if (in_array('priceApiRecordId', $keys)) {
            $attributes['priceApiRecordId'] = $amazonFTL->getPriceApiRecordId();
        }
        if (in_array('priceApiIdentify', $keys)) {
            $attributes['priceApiIdentify'] = $amazonFTL->getPriceApiIdentify();
        }
        if (in_array('isaNumber', $keys)) {
            $attributes['isaNumber'] = $amazonFTL->getIsaNumber();
        }

        $expression['data']['attributes'] = $attributes;
        if (in_array('member', $keys) && !empty($amazonFTL->getMember()->getId())) {
            $memberRelationships = array(
                'type' => 'member',
                'id' => strval($amazonFTL->getMember()->getId())
            );

            $expression['data']['relationships']['member']['data'] = $memberRelationships;
        }
        if (in_array('pickupWarehouse', $keys)) {
            $pickupWarehouseRelationships = array(
                'type' => 'warehouse',
                'id' => strval($amazonFTL->getPickupWarehouse()->getId())
            );

            $expression['data']['relationships']['pickupWarehouse']['data'] = $pickupWarehouseRelationships;
        }
        if (in_array('targetWarehouse', $keys)) {
            $targetWarehouseRelationships = array(
                'type' => 'warehouse',
                'id' => strval($amazonFTL->getTargetWarehouse()->getId())
            );

            $expression['data']['relationships']['targetWarehouse']['data'] = $targetWarehouseRelationships;
        }
        if (in_array('address', $keys)) {
            $addressRelationships = array(
                'type' => 'address',
                'id' => strval($amazonFTL->getAddress()->getId())
            );

            $expression['data']['relationships']['address']['data'] = $addressRelationships;
        }
        if (in_array('staff', $keys) && !empty($amazonFTL->getStaff()->getId())) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval($amazonFTL->getStaff()->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
