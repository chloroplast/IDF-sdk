<?php
namespace Sdk\Order\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Warehouse\Model\Warehouse;

class RecShipOrderLTLOrder implements IObject
{
    use Object;

    private $id;
    /**
     * @var RecOrderLTLOrder $recOrderLTLOrder
     */
    private $recOrderLTLOrder;
    /**
     * @var OrderLTL $orderLTL
     */
    private $orderLTL;
    /**
     * @var array $stockInItemsAttachments 散板订单佐证材料
     */
    private $stockInItemsAttachments;
    /**
     * @var Warehouse $pickupWarehouse 取货仓库
     */
    private $pickupWarehouse;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->recOrderLTLOrder = new RecOrderLTLOrder();
        $this->orderLTL = new OrderLTL();
        $this->stockInItemsAttachments = array();
        $this->pickupWarehouse = new Warehouse();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->recOrderLTLOrder);
        unset($this->orderLTL);
        unset($this->stockInItemsAttachments);
        unset($this->pickupWarehouse);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setRecOrderLTLOrder(RecOrderLTLOrder $recOrderLTLOrder) : void
    {
        $this->recOrderLTLOrder = $recOrderLTLOrder;
    }

    public function getRecOrderLTLOrder() : RecOrderLTLOrder
    {
        return $this->recOrderLTLOrder;
    }

    public function setOrderLTL(OrderLTL $orderLTL) : void
    {
        $this->orderLTL = $orderLTL;
    }

    public function getOrderLTL() : OrderLTL
    {
        return $this->orderLTL;
    }

    public function setStockInItemsAttachments(array $stockInItemsAttachments) : void
    {
        $this->stockInItemsAttachments = $stockInItemsAttachments;
    }

    public function getStockInItemsAttachments() : array
    {
        return $this->stockInItemsAttachments;
    }

    public function setPickupWarehouse(Warehouse $pickupWarehouse) : void
    {
        $this->pickupWarehouse = $pickupWarehouse;
    }

    public function getPickupWarehouse() : Warehouse
    {
        return $this->pickupWarehouse;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
