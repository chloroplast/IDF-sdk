<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\User\Member\Model\Member;
use Sdk\User\Staff\Model\Staff;

class MemberOrder implements IObject
{
    use Object;

    const ATTRIBUTE = array(
        'AMAZON' => 1,
        'NO_AMAZON' => 2
    );
    const ATTRIBUTE_CN = array(
        self::ATTRIBUTE['AMAZON'] => '亚马逊',
        self::ATTRIBUTE['NO_AMAZON'] => '非亚马逊'
    );

    const TYPE = array(
        'FTL' => 1,
        'LTL' => 2
    );
    const TYPE_CN = array(
        self::TYPE['FTL'] => '整板',
        self::TYPE['LTL'] => '散板',
    );
    const AMAZON_CN = array(
        self::TYPE['FTL'] => 'FTL-A',
        self::TYPE['LTL'] => 'LTL-A',
    );
    const NO_AMAZON_CN = array(
        self::TYPE['FTL'] => 'FTL',
        self::TYPE['LTL'] => 'LTL',
    );
    
    const IDF_PICKUP = array(
        'NO_AMAZON' => 0,
        'AMAZON' => 1
    );
    const IDF_PICKUP_CN = array(
        self::IDF_PICKUP['NO_AMAZON'] => '否',
        self::IDF_PICKUP['AMAZON'] => '是'
    );

    const A_STATUS = array(
        'CANCELED' => -2,
        'INCOMPLETE' => 0,
        'COMPLETED' => 10
    );
    const A_STATUS_CN = array(
        self::A_STATUS['CANCELED'] => '已取消',
        self::A_STATUS['INCOMPLETE'] => '未完成',
        self::A_STATUS['COMPLETED'] => '已完成'
    );

    private $id;
    /**
     * @var Member $member 官网用户
     */
    private $member;
    /**
     * @var Staff $staff 后台用户
     */
    private $staff;
    /**
     * @var string $number 订单编号
     */
    private $number;
    /**
     * @var float $price 价格
     */
    private $price;
    /**
     * @var int $attribute 订单属性
     */
    private $attribute;
    /**
     * @var int $type 订单类型
     */
    private $type;
    /**
     * @var int $idfPickup 是否IDF取货
     */
    private $idfPickup;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->member = new Member();
        $this->staff = new Staff();
        $this->number = '';
        $this->price = 0;
        $this->attribute = 0;
        $this->type = 0;
        $this->idfPickup = self::IDF_PICKUP['AMAZON'];
        $this->status = self::A_STATUS['INCOMPLETE'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->member);
        unset($this->staff);
        unset($this->number);
        unset($this->price);
        unset($this->attribute);
        unset($this->type);
        unset($this->idfPickup);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setMember(Member $member): void
    {
        $this->member = $member;
    }

    public function getMember(): Member
    {
        return $this->member;
    }

    public function setStaff(Staff $staff): void
    {
        $this->staff = $staff;
    }

    public function getStaff(): Staff
    {
        return $this->staff;
    }

    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setPrice(float $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function setAttribute(int $attribute): void
    {
        $this->attribute = $attribute;
    }

    public function getAttribute(): int
    {
        return $this->attribute;
    }

    public function setType(int $type): void
    {
        $this->type = $type;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setIdfPickup(int $idfPickup) : void
    {
        $this->idfPickup = $idfPickup;
    }

    public function getIdfPickup() : int
    {
        return $this->idfPickup;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}
