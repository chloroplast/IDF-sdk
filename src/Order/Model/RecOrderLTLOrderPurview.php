<?php
namespace Sdk\Order\Model;

use Sdk\Role\Purview\Model\Purview;
use Sdk\Role\Purview\Model\IPurviewAble;

class RecOrderLTLOrderPurview extends Purview
{
    public function __construct()
    {
        parent::__construct(IPurviewAble::COLUMN['REC_ORDERLTL_ORDER']);
    }

    public function add() : bool
    {
        return $this->operation('add');
    }

    public function edit() : bool
    {
        return $this->operation('edit');
    }

    public function registerPickupDate() : bool
    {
        return $this->operation('registerPickupDate');
    }

    public function pickup() : bool
    {
        return $this->operation('pickup');
    }

    public function stockIn() : bool
    {
        return $this->operation('stockIn');
    }
}
