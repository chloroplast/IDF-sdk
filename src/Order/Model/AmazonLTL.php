<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\User\Member\Model\Member;
use Sdk\User\Staff\Model\Staff;
use Sdk\Warehouse\Model\Warehouse;
use Sdk\Address\Model\Address;
use Sdk\CarType\Model\CarType;

use Sdk\Order\Repository\AmazonLTLRepository;

class AmazonLTL implements IObject, IOperateAble, ISubOrder
{
    use Object, OperateAbleTrait;

    const IDF_PICKUP = array(
        'NO_AMAZON' => 0,
        'AMAZON' => 1
    );
    const IDF_PICKUP_CN = array(
        self::IDF_PICKUP['NO_AMAZON'] => '否',
        self::IDF_PICKUP['AMAZON'] => '是'
    );

    const WEIGHT_UNIT = array(
        'LB' => 1,
        'KG' => 2
    );
    const WEIGHT_UNIT_CN = array(
        self::WEIGHT_UNIT['LB'] => 'LB',
        self::WEIGHT_UNIT['KG'] => 'KG'
    );

    const FROZEN = array(
        'NO' => 0,
        'YES' => 1
    );
    const FROZEN_CN = array(
        self::FROZEN['NO'] => '未冻结',
        self::FROZEN['YES'] => '冻结'
    );

    const A_STATUS = array(
        'CANCELED' => -2,
        'TO_BE_CONFIRMED' => 0,
        'TO_BE_PICKED_UP' => 2,
        'TO_BE_STOCKED' => 4,
        'TO_BE_STOCKED_OUT' => 6,
        'TO_BE_DELIVERED' => 8,
        'COMPLETED' => 10
    );
    const A_STATUS_CN = array(
        self::A_STATUS['CANCELED'] => '已取消',
        self::A_STATUS['TO_BE_CONFIRMED'] => '待确认',
        self::A_STATUS['TO_BE_PICKED_UP'] => '待取货',
        self::A_STATUS['TO_BE_STOCKED'] => '待入库',
        self::A_STATUS['TO_BE_STOCKED_OUT'] => '待出库',
        self::A_STATUS['TO_BE_DELIVERED'] => '待派送',
        self::A_STATUS['COMPLETED'] => '已完成'
    );

    private $id;
    /**
     * @var Member $member 官网用户
     */
    private $member;
    /**
     * @var Staff $staff 后台用户
     */
    private $staff;
    /**
     * @var Warehouse $pickupWarehouse 取货仓库
     */
    private $pickupWarehouse;
    /**
     * @var Address $address 地址信息
     */
    private $address;
    /**
     * @var string $number 订单编号
     */
    private $number;
    /**
     * @var int $idfPickup 是否IDF取货
     */
    private $idfPickup;
    /**
     * @var array $items 货物信息
     */
    private $items;
    /**
     * @var int $palletNumber
     */
    private $palletNumber;
    /**
     * @var int $itemNumber
     */
    private $itemNumber;
    /**
     * @var float $weight
     */
    private $weight;
    /**
     * @var int $weightUnit
     */
    private $weightUnit;
    /**
     * @var string $fbaNumber
     */
    private $fbaNumber;
    /**
     * @var string $poNumber
     */
    private $poNumber;
    /**
     * @var string $soNumber
     */
    private $soNumber;
    /**
     * @var string $coNumber
     */
    private $coNumber;
    /**
     * @var float $length
     */
    private $length;
    /**
     * @var float $width
     */
    private $width;
    /**
     * @var float $height
     */
    private $height;
    /**
     * @var int $pickupDate
     */
    private $pickupDate;
    /**
     * @var int $readyTime
     */
    private $readyTime;
    /**
     * @var int $closeTime
     */
    private $closeTime;
    /**
     * @var string $pickupNumber
     */
    private $pickupNumber;
    /**
     * @var string $remark
     */
    private $remark;
    /**
     * @var string $isaNumber
     */
    private $isaNumber;
    /**
     * @var string $position
     */
    private $position;
    /**
     * @var string $stockInNumber
     */
    private $stockInNumber;
    /**
     * @var int $stockInTime
     */
    private $stockInTime;
    /**
     * @var int $frozen
     */
    private $frozen;
    /**
     * @var MemberOrder $memberOrder 用户订单id
     */
    private $memberOrder;
    /**
     * @var Warehouse $targetWarehouse 目标仓库
     */
    private $targetWarehouse;
    /**
     * @var array $filter
     */
    private $filter;
    /**
     * @var string $sort
     */
    private $sort;
    /**
     * @var CarType $carType
     */
    private $carType;
    /**
     * @var float $price
     */
    private $price;
    /**
     * @var array $priceRemark
     */
    private $priceRemark;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->member = new Member();
        $this->staff = new Staff();
        $this->pickupWarehouse = new Warehouse();
        $this->address = new Address();
        $this->number = '';
        $this->idfPickup = self::IDF_PICKUP['AMAZON'];
        $this->items = array();
        $this->palletNumber = 0;
        $this->itemNumber = 0;
        $this->weight = 0;
        $this->weightUnit = 0;
        $this->fbaNumber = '';
        $this->poNumber = '';
        $this->soNumber = '';
        $this->coNumber = '';
        $this->length = 0;
        $this->width = 0;
        $this->height = 0;
        $this->pickupDate = 0;
        $this->readyTime = 0;
        $this->closeTime = 0;
        $this->pickupNumber = '';
        $this->remark = '';
        $this->isaNumber = '';
        $this->position = '';
        $this->stockInNumber = '';
        $this->stockInTime = 0;
        $this->frozen = self::FROZEN['NO'];
        $this->memberOrder = new MemberOrder();
        $this->targetWarehouse = new Warehouse();
        $this->filter = array();
        $this->sort = '';
        $this->carType = new CarType();
        $this->price = 0;
        $this->priceRemark = array();
        $this->status = self::A_STATUS['TO_BE_CONFIRMED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new AmazonLTLRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->member);
        unset($this->staff);
        unset($this->pickupWarehouse);
        unset($this->address);
        unset($this->number);
        unset($this->idfPickup);
        unset($this->items);
        unset($this->palletNumber);
        unset($this->itemNumber);
        unset($this->weight);
        unset($this->weightUnit);
        unset($this->fbaNumber);
        unset($this->poNumber);
        unset($this->soNumber);
        unset($this->coNumber);
        unset($this->length);
        unset($this->width);
        unset($this->height);
        unset($this->pickupDate);
        unset($this->readyTime);
        unset($this->closeTime);
        unset($this->pickupNumber);
        unset($this->remark);
        unset($this->isaNumber);
        unset($this->position);
        unset($this->stockInNumber);
        unset($this->stockInTime);
        unset($this->frozen);
        unset($this->memberOrder);
        unset($this->targetWarehouse);
        unset($this->filter);
        unset($this->sort);
        unset($this->carType);
        unset($this->price);
        unset($this->priceRemark);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setStaff(Staff $staff) : void
    {
        $this->staff = $staff;
    }

    public function getStaff() : Staff
    {
        return $this->staff;
    }

    public function setPickupWarehouse(Warehouse $pickupWarehouse) : void
    {
        $this->pickupWarehouse = $pickupWarehouse;
    }

    public function getPickupWarehouse() : Warehouse
    {
        return $this->pickupWarehouse;
    }

    public function setAddress(Address $address) : void
    {
        $this->address = $address;
    }

    public function getAddress() : Address
    {
        return $this->address;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setIdfPickup(int $idfPickup) : void
    {
        $this->idfPickup = $idfPickup;
    }

    public function getIdfPickup() : int
    {
        return $this->idfPickup;
    }

    public function setItems(array $items) : void
    {
        $this->items = $items;
    }

    public function getItems() : array
    {
        return $this->items;
    }

    public function setPalletNumber(int $palletNumber) : void
    {
        $this->palletNumber = $palletNumber;
    }

    public function getPalletNumber() : int
    {
        return $this->palletNumber;
    }

    public function setItemNumber(int $itemNumber) : void
    {
        $this->itemNumber = $itemNumber;
    }

    public function getItemNumber() : int
    {
        return $this->itemNumber;
    }

    public function setWeight(float $weight) : void
    {
        $this->weight = $weight;
    }

    public function getWeight() : float
    {
        return $this->weight;
    }

    public function setWeightUnit(int $weightUnit) : void
    {
        $this->weightUnit = $weightUnit;
    }

    public function getWeightUnit() : int
    {
        return $this->weightUnit;
    }

    public function setFbaNumber(string $fbaNumber) : void
    {
        $this->fbaNumber = $fbaNumber;
    }

    public function getFbaNumber() : string
    {
        return $this->fbaNumber;
    }

    public function setPoNumber(string $poNumber) : void
    {
        $this->poNumber = $poNumber;
    }

    public function getPoNumber() : string
    {
        return $this->poNumber;
    }

    public function setSoNumber(string $soNumber) : void
    {
        $this->soNumber = $soNumber;
    }

    public function getSoNumber() : string
    {
        return $this->soNumber;
    }

    public function setCoNumber(string $coNumber) : void
    {
        $this->coNumber = $coNumber;
    }

    public function getCoNumber() : string
    {
        return $this->coNumber;
    }

    public function setLength(float $length) : void
    {
        $this->length = $length;
    }

    public function getLength() : float
    {
        return $this->length;
    }

    public function setWidth(float $width) : void
    {
        $this->width = $width;
    }

    public function getWidth() : float
    {
        return $this->width;
    }

    public function setHeight(float $height) : void
    {
        $this->height = $height;
    }

    public function getHeight() : float
    {
        return $this->height;
    }

    public function setPickupDate(int $pickupDate) : void
    {
        $this->pickupDate = $pickupDate;
    }

    public function getPickupDate() : int
    {
        return $this->pickupDate;
    }

    public function setReadyTime(int $readyTime) : void
    {
        $this->readyTime = $readyTime;
    }

    public function getReadyTime() : int
    {
        return $this->readyTime;
    }

    public function setCloseTime(int $closeTime) : void
    {
        $this->closeTime = $closeTime;
    }

    public function getCloseTime() : int
    {
        return $this->closeTime;
    }

    public function setPickupNumber(string $pickupNumber) : void
    {
        $this->pickupNumber = $pickupNumber;
    }

    public function getPickupNumber() : string
    {
        return $this->pickupNumber;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setIsaNumber(string $isaNumber) : void
    {
        $this->isaNumber = $isaNumber;
    }

    public function getIsaNumber() : string
    {
        return $this->isaNumber;
    }

    public function setPosition(string $position) : void
    {
        $this->position = $position;
    }

    public function getPosition() : string
    {
        return $this->position;
    }

    public function setStockInNumber(string $stockInNumber) : void
    {
        $this->stockInNumber = $stockInNumber;
    }

    public function getStockInNumber() : string
    {
        return $this->stockInNumber;
    }

    public function setStockInTime(int $stockInTime) : void
    {
        $this->stockInTime = $stockInTime;
    }

    public function getStockInTime() : int
    {
        return $this->stockInTime;
    }

    public function setFrozen(int $frozen) : void
    {
        $this->frozen = $frozen;
    }

    public function getFrozen() : int
    {
        return $this->frozen;
    }

    public function setMemberOrder(MemberOrder $memberOrder) : void
    {
        $this->memberOrder = $memberOrder;
    }

    public function getMemberOrder() : MemberOrder
    {
        return $this->memberOrder;
    }

    public function setTargetWarehouse(Warehouse $targetWarehouse) : void
    {
        $this->targetWarehouse = $targetWarehouse;
    }

    public function getTargetWarehouse() : Warehouse
    {
        return $this->targetWarehouse;
    }

    public function setFilter(array $filter) : void
    {
        $this->filter = $filter;
    }

    public function getFilter() : array
    {
        return $this->filter;
    }

    public function setSort(string $sort) : void
    {
        $this->sort = $sort;
    }

    public function getSort() : string
    {
        return $this->sort;
    }

    public function setCarType(CarType $carType) : void
    {
        $this->carType = $carType;
    }

    public function getCarType() : CarType
    {
        return $this->carType;
    }

    public function setPrice(float $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function setPriceRemark(array $priceRemark) : void
    {
        $this->priceRemark = $priceRemark;
    }

    public function getPriceRemark() : array
    {
        return $this->priceRemark;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : AmazonLTLRepository
    {
        return $this->repository;
    }

    public function memberCancel() : bool
    {
        return $this->getRepository()->memberCancel($this);
    }

    public function staffCancel() : bool
    {
        return $this->getRepository()->staffCancel($this);
    }

    public function confirm() : bool
    {
        return $this->getRepository()->confirm($this);
    }

    public function batchConfirm(array $amazonLTLList) : bool
    {
        return $this->getRepository()->batchConfirm($amazonLTLList);
    }

    public function registerPosition() : bool
    {
        return $this->getRepository()->registerPosition($this);
    }

    public function freeze() : bool
    {
        return $this->getRepository()->freeze($this);
    }

    public function unFreeze() : bool
    {
        return $this->getRepository()->unFreeze($this);
    }

    public function autoGroup(array $amazonLTLList) : array
    {
        return $this->getRepository()->autoGroup($amazonLTLList);
    }
    
    public function calculate() : array
    {
        return $this->getRepository()->calculate($this);
    }
}
