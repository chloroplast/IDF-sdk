<?php
namespace Sdk\Order\Model;

use Sdk\Role\Purview\Model\Purview;
use Sdk\Role\Purview\Model\IPurviewAble;

class AmazonFTLPurview extends Purview
{
    public function __construct()
    {
        parent::__construct(IPurviewAble::COLUMN['AMAZONFTL']);
    }

    public function add() : bool
    {
        return $this->operation('add');
    }

    public function edit() : bool
    {
        return $this->operation('edit');
    }

    public function staffCancel() : bool
    {
        return $this->operation('staffCancel');
    }

    public function confirm() : bool
    {
        return $this->operation('confirm');
    }

    public function batchConfirm(array $amazonFTLList) : bool
    {
        unset($amazonFTLList);
        return $this->operation('batchConfirm');
    }

    public function freeze() : bool
    {
        return $this->operation('freeze');
    }

    public function unFreeze() : bool
    {
        return $this->operation('unFreeze');
    }
}
