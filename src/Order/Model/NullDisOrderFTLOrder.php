<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullDisOrderFTLOrder extends DisOrderFTLOrder implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function registerDispatchDate() : bool
    {
        return $this->resourceNotExist();
    }
    
    public function dispatch() : bool
    {
        return $this->resourceNotExist();
    }
    
    public function accept() : bool
    {
        return $this->resourceNotExist();
    }
}
