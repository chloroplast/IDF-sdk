<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\User\Member\Model\Member;
use Sdk\User\Staff\Model\Staff;
use Sdk\Warehouse\Model\Warehouse;
use Sdk\Address\Model\Address;
use Sdk\CarType\Model\CarType;

use Sdk\Order\Repository\OrderLTLRepository;

class OrderLTL implements IObject, IOperateAble, ISubOrder
{
    use Object, OperateAbleTrait;

    const WEEKEND_DELIVERY = array(
        'NO' => 0,
        'YES' => 1
    );
    const WEEKEND_DELIVERY_CN = array(
        self::WEEKEND_DELIVERY['NO'] => '否',
        self::WEEKEND_DELIVERY['YES'] => '是'
    );

    const IDF_PICKUP = array(
        'NO_AMAZON' => 0,
        'AMAZON' => 1
    );
    const IDF_PICKUP_CN = array(
        self::IDF_PICKUP['NO_AMAZON'] => '否',
        self::IDF_PICKUP['AMAZON'] => '是'
    );

    const WEIGHT_UNIT = array(
        'LB' => 1,
        'KG' => 2
    );
    const WEIGHT_UNIT_CN = array(
        self::WEIGHT_UNIT['LB'] => 'LB',
        self::WEIGHT_UNIT['KG'] => 'KG'
    );

    const FROZEN = array(
        'NO' => 0,
        'YES' => 1
    );
    const FROZEN_CN = array(
        self::FROZEN['NO'] => '未冻结',
        self::FROZEN['YES'] => '冻结'
    );

    const A_STATUS = array(
        'CANCELED' => -2,
        'TO_BE_CONFIRMED' => 0,
        'TO_BE_PICKED_UP' => 2,
        'TO_BE_STOCKED' => 4,
        'TO_BE_STOCKED_OUT' => 6,
        'TO_BE_DELIVERED' => 8,
        'COMPLETED' => 10
    );
    const A_STATUS_CN = array(
        self::A_STATUS['CANCELED'] => '已取消',
        self::A_STATUS['TO_BE_CONFIRMED'] => '待确认',
        self::A_STATUS['TO_BE_PICKED_UP'] => '待取货',
        self::A_STATUS['TO_BE_STOCKED'] => '待入库',
        self::A_STATUS['TO_BE_STOCKED_OUT'] => '待出库',
        self::A_STATUS['TO_BE_DELIVERED'] => '待派送',
        self::A_STATUS['COMPLETED'] => '已完成'
    );

    const NEEDTRANSPORT = array(
        'NO' => 0,
        'YES' => 1
    );
    const NEEDTRANSPORT_CN = array(
        self::NEEDTRANSPORT['NO'] => '不需要',
        self::NEEDTRANSPORT['YES'] => '需要'
    );

    const NEEDLIFTGATE = array(
        'NO' => 0,
        'YES' => 1
    );
    const NEEDLIFTGATE_CN = array(
        self::NEEDLIFTGATE['NO'] => '不需要',
        self::NEEDLIFTGATE['YES'] => '需要'
    );

    const STACKABLE = array(
        'NO' => 0,
        'YES' => 1
    );
    const STACKABLE_CN = array(
        self::STACKABLE['NO'] => '不可以',
        self::STACKABLE['YES'] => '可以'
    );

    const TURNABLE = array(
        'NO' => 0,
        'YES' => 1
    );
    const TURNABLE_CN = array(
        self::TURNABLE['NO'] => '不可以',
        self::TURNABLE['YES'] => '可以'
    );

    const CATEGORY = array(
        'NORMAL_MODE' => 0,
        'APPOINTMENT_ALREADY_MADE' => 1,
        'ADDRESS_OPEN' => 2,
        'APPOINTMENT_REQUIRED' => 3,
        'DRIVER_CONTACT' => 4
    );
    const CATEGORY_CN = array(
        self::CATEGORY['NORMAL_MODE'] => '普通模式',
        self::CATEGORY['APPOINTMENT_ALREADY_MADE'] => '有预约',
        self::CATEGORY['ADDRESS_OPEN'] => '根据地址开放时间型',
        self::CATEGORY['APPOINTMENT_REQUIRED'] => '需要平台代申请预约',
        self::CATEGORY['DRIVER_CONTACT'] => '司机提前电话联系'
    );

    const PICKUPVEHICLETYPE = array(
        'DRY_CABINET' => 1,
        'BOX_TRUCK' => 2
    );
    const PICKUPVEHICLETYPE_CN = array(
        self::PICKUPVEHICLETYPE['DRY_CABINET'] => '干柜',
        self::PICKUPVEHICLETYPE['BOX_TRUCK'] => '箱式货车'
    );

    private $id;
    /**
     * @var Member $member 官网用户
     */
    private $member;
    /**
     * @var Staff $staff 后台用户
     */
    private $staff;
    /**
     * @var Warehouse $pickupWarehouse 取货仓库
     */
    private $pickupWarehouse;
    /**
     * @var Address $address 地址信息
     */
    private $address;
    /**
     * @var string $number 订单编号
     */
    private $number;
    /**
     * @var int $idfPickup 是否IDF取货
     */
    private $idfPickup;
    /**
     * @var array $items 货物信息
     */
    private $items;
    /**
     * @var string $poNumber
     */
    private $poNumber;
    /**
     * @var int $pickupDate
     */
    private $pickupDate;
    /**
     * @var string $remark
     */
    private $remark;
    /**
     * @var string $position
     */
    private $position;
    /**
     * @var string $stockInNumber
     */
    private $stockInNumber;
    /**
     * @var int $stockInTime
     */
    private $stockInTime;
    /**
     * @var int $frozen
     */
    private $frozen;
    /**
     * @var MemberOrder $memberOrder 用户订单id
     */
    private $memberOrder;
    /**
     * @var CarType $carType
     */
    private $carType;
    /**
     * @var float $price
     */
    private $price;
    /**
     * @var array $priceRemark
     */
    private $priceRemark;
    /**
     * @var int $bookingPickupDate
     */
    private $bookingPickupDate;
    /**
     * @var string $pickupZipCode
     */
    private $pickupZipCode;
    /**
     * @var string $pickupAddressType
     */
    private $pickupAddressType;
    /**
     * @var string $deliveryZipCode
     */
    private $deliveryZipCode;
    /**
     * @var string $deliveryAddressType
     */
    private $deliveryAddressType;
    /**
     * @var int $pickupNeedTransport
     */
    private $pickupNeedTransport;
    /**
     * @var int $pickupNeedLiftgate
     */
    private $pickupNeedLiftgate;
    /**
     * @var int $deliveryNeedTransport
     */
    private $deliveryNeedTransport;
    /**
     * @var int $deliveryNeedLiftgate
     */
    private $deliveryNeedLiftgate;
    /**
     * @var int $pickupVehicleType
     */
    private $pickupVehicleType;
    /**
     * @var int $weekendDelivery
     */
    private $weekendDelivery;
    /**
     * @var string $deliveryDescription
     */
    private $deliveryDescription;
    /**
     * @var array $pickupRequirement
     */
    private $pickupRequirement;
    /**
     * @var array $deliveryRequirement
     */
    private $deliveryRequirement;
    /**
     * @var string $readyTime
     */
    private $readyTime;
    /**
     * @var string $closeTime
     */
    private $closeTime;
    /**
     * @var string $deliveryName
     */
    private $deliveryName;
    /**
     * @var string $deliveryReadyTime
     */
    private $deliveryReadyTime;
    /**
     * @var string $deliveryCloseTime
     */
    private $deliveryCloseTime;
    /**
     * @var Address $deliveryAddress
     */
    private $deliveryAddress;
    /**
     * @var int $priceApiIndex
     */
    private $priceApiIndex;
    /**
     * @var int $priceApiRecordId
     */
    private $priceApiRecordId;
    /**
     * @var string $priceApiIdentify
     */
    private $priceApiIdentify;
    /**
     * @var int $pickupEndDate
     */
    private $pickupEndDate;
    /**
     * @var int $pickupStartDate
     */
    private $pickupStartDate;
    /**
     * @var int $deliveryEndDate
     */
    private $deliveryEndDate;
    /**
     * @var int $deliveryStartDate
     */
    private $deliveryStartDate;
    /**
     * @var int $palletNumber
     */
    private $palletNumber;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->member = new Member();
        $this->staff = new Staff();
        $this->pickupWarehouse = new Warehouse();
        $this->address = new Address();
        $this->number = '';
        $this->idfPickup = self::IDF_PICKUP['AMAZON'];
        $this->items = array();
        $this->poNumber = '';
        $this->pickupDate = 0;
        $this->remark = '';
        $this->position = '';
        $this->stockInNumber = '';
        $this->stockInTime = 0;
        $this->frozen = self::FROZEN['NO'];
        $this->memberOrder = new MemberOrder();
        $this->carType = new CarType();
        $this->price = 0;
        $this->priceRemark = array();
        $this->bookingPickupDate = 0;
        $this->pickupZipCode = '';
        $this->pickupAddressType = '';
        $this->deliveryZipCode = '';
        $this->deliveryAddressType = '';
        $this->pickupNeedTransport = self::NEEDTRANSPORT['NO'];
        $this->pickupNeedLiftgate = self::NEEDLIFTGATE['NO'];
        $this->deliveryNeedTransport = self::NEEDTRANSPORT['NO'];
        $this->deliveryNeedLiftgate = self::NEEDLIFTGATE['NO'];
        $this->pickupVehicleType = 0;
        $this->weekendDelivery = self::WEEKEND_DELIVERY['NO'];
        $this->deliveryDescription = '';
        $this->pickupRequirement = array();
        $this->deliveryRequirement = array();
        $this->readyTime = '';
        $this->closeTime = '';
        $this->deliveryName = '';
        $this->deliveryReadyTime = '';
        $this->deliveryCloseTime = '';
        $this->deliveryAddress = new Address();
        $this->priceApiIndex = 0;
        $this->priceApiRecordId = 0;
        $this->priceApiIdentify = '';
        $this->pickupEndDate = 0;
        $this->pickupStartDate = 0;
        $this->deliveryEndDate = 0;
        $this->deliveryStartDate = 0;
        $this->palletNumber = 0;
        $this->status = self::A_STATUS['TO_BE_CONFIRMED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new OrderLTLRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->member);
        unset($this->staff);
        unset($this->pickupWarehouse);
        unset($this->address);
        unset($this->number);
        unset($this->idfPickup);
        unset($this->items);
        unset($this->poNumber);
        unset($this->pickupDate);
        unset($this->remark);
        unset($this->position);
        unset($this->stockInNumber);
        unset($this->stockInTime);
        unset($this->frozen);
        unset($this->memberOrder);
        unset($this->carType);
        unset($this->price);
        unset($this->priceRemark);
        unset($this->bookingPickupDate);
        unset($this->pickupZipCode);
        unset($this->pickupAddressType);
        unset($this->deliveryZipCode);
        unset($this->deliveryAddressType);
        unset($this->pickupNeedTransport);
        unset($this->pickupNeedLiftgate);
        unset($this->deliveryNeedTransport);
        unset($this->deliveryNeedLiftgate);
        unset($this->pickupVehicleType);
        unset($this->weekendDelivery);
        unset($this->deliveryDescription);
        unset($this->pickupRequirement);
        unset($this->deliveryRequirement);
        unset($this->readyTime);
        unset($this->closeTime);
        unset($this->deliveryName);
        unset($this->deliveryReadyTime);
        unset($this->deliveryCloseTime);
        unset($this->deliveryAddress);
        unset($this->priceApiIndex);
        unset($this->priceApiRecordId);
        unset($this->priceApiIdentify);
        unset($this->pickupEndDate);
        unset($this->pickupStartDate);
        unset($this->deliveryEndDate);
        unset($this->deliveryStartDate);
        unset($this->palletNumber);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setStaff(Staff $staff) : void
    {
        $this->staff = $staff;
    }

    public function getStaff() : Staff
    {
        return $this->staff;
    }

    public function setPickupWarehouse(Warehouse $pickupWarehouse) : void
    {
        $this->pickupWarehouse = $pickupWarehouse;
    }

    public function getPickupWarehouse() : Warehouse
    {
        return $this->pickupWarehouse;
    }

    public function setAddress(Address $address) : void
    {
        $this->address = $address;
    }

    public function getAddress() : Address
    {
        return $this->address;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setIdfPickup(int $idfPickup) : void
    {
        $this->idfPickup = $idfPickup;
    }

    public function getIdfPickup() : int
    {
        return $this->idfPickup;
    }

    public function setItems(array $items) : void
    {
        $this->items = $items;
    }

    public function getItems() : array
    {
        return $this->items;
    }

    public function setPoNumber(string $poNumber) : void
    {
        $this->poNumber = $poNumber;
    }

    public function getPoNumber() : string
    {
        return $this->poNumber;
    }

    public function setPickupDate(int $pickupDate) : void
    {
        $this->pickupDate = $pickupDate;
    }

    public function getPickupDate() : int
    {
        return $this->pickupDate;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setPosition(string $position) : void
    {
        $this->position = $position;
    }

    public function getPosition() : string
    {
        return $this->position;
    }

    public function setStockInNumber(string $stockInNumber) : void
    {
        $this->stockInNumber = $stockInNumber;
    }

    public function getStockInNumber() : string
    {
        return $this->stockInNumber;
    }

    public function setStockInTime(int $stockInTime) : void
    {
        $this->stockInTime = $stockInTime;
    }

    public function getStockInTime() : int
    {
        return $this->stockInTime;
    }

    public function setFrozen(int $frozen) : void
    {
        $this->frozen = $frozen;
    }

    public function getFrozen() : int
    {
        return $this->frozen;
    }

    public function setMemberOrder(MemberOrder $memberOrder) : void
    {
        $this->memberOrder = $memberOrder;
    }

    public function getMemberOrder() : MemberOrder
    {
        return $this->memberOrder;
    }

    public function setCarType(CarType $carType) : void
    {
        $this->carType = $carType;
    }

    public function getCarType() : CarType
    {
        return $this->carType;
    }

    public function setPrice(float $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function setPriceRemark(array $priceRemark) : void
    {
        $this->priceRemark = $priceRemark;
    }

    public function getPriceRemark() : array
    {
        return $this->priceRemark;
    }

    public function setBookingPickupDate(int $bookingPickupDate) : void
    {
        $this->bookingPickupDate = $bookingPickupDate;
    }

    public function getBookingPickupDate() : int
    {
        return $this->bookingPickupDate;
    }

    public function setPickupZipCode(string $pickupZipCode) : void
    {
        $this->pickupZipCode = $pickupZipCode;
    }

    public function getPickupZipCode() : string
    {
        return $this->pickupZipCode;
    }

    public function setPickupAddressType(string $pickupAddressType) : void
    {
        $this->pickupAddressType = $pickupAddressType;
    }

    public function getPickupAddressType() : string
    {
        return $this->pickupAddressType;
    }

    public function setDeliveryZipCode(string $deliveryZipCode) : void
    {
        $this->deliveryZipCode = $deliveryZipCode;
    }

    public function getDeliveryZipCode() : string
    {
        return $this->deliveryZipCode;
    }

    public function setDeliveryAddressType(string $deliveryAddressType) : void
    {
        $this->deliveryAddressType = $deliveryAddressType;
    }

    public function getDeliveryAddressType() : string
    {
        return $this->deliveryAddressType;
    }

    public function setPickupNeedTransport(int $pickupNeedTransport) : void
    {
        $this->pickupNeedTransport = $pickupNeedTransport;
    }

    public function getPickupNeedTransport() : int
    {
        return $this->pickupNeedTransport;
    }

    public function setPickupNeedLiftgate(int $pickupNeedLiftgate) : void
    {
        $this->pickupNeedLiftgate = $pickupNeedLiftgate;
    }

    public function getPickupNeedLiftgate() : int
    {
        return $this->pickupNeedLiftgate;
    }

    public function setDeliveryNeedTransport(int $deliveryNeedTransport) : void
    {
        $this->deliveryNeedTransport = $deliveryNeedTransport;
    }

    public function getDeliveryNeedTransport() : int
    {
        return $this->deliveryNeedTransport;
    }

    public function setDeliveryNeedLiftgate(int $deliveryNeedLiftgate) : void
    {
        $this->deliveryNeedLiftgate = $deliveryNeedLiftgate;
    }

    public function getDeliveryNeedLiftgate() : int
    {
        return $this->deliveryNeedLiftgate;
    }

    public function setPickupVehicleType(int $pickupVehicleType) : void
    {
        $this->pickupVehicleType = $pickupVehicleType;
    }

    public function getPickupVehicleType() : int
    {
        return $this->pickupVehicleType;
    }

    public function setWeekendDelivery(int $weekendDelivery) : void
    {
        $this->weekendDelivery = $weekendDelivery;
    }

    public function getWeekendDelivery() : int
    {
        return $this->weekendDelivery;
    }

    public function setDeliveryDescription(string $deliveryDescription) : void
    {
        $this->deliveryDescription = $deliveryDescription;
    }

    public function getDeliveryDescription() : string
    {
        return $this->deliveryDescription;
    }

    public function setReadyTime(string $readyTime) : void
    {
        $this->readyTime = $readyTime;
    }

    public function getReadyTime() : string
    {
        return $this->readyTime;
    }

    public function setCloseTime(string $closeTime) : void
    {
        $this->closeTime = $closeTime;
    }

    public function getCloseTime() : string
    {
        return $this->closeTime;
    }

    public function setDeliveryName(string $deliveryName) : void
    {
        $this->deliveryName = $deliveryName;
    }

    public function getDeliveryName() : string
    {
        return $this->deliveryName;
    }

    public function setDeliveryReadyTime(string $deliveryReadyTime) : void
    {
        $this->deliveryReadyTime = $deliveryReadyTime;
    }

    public function getDeliveryReadyTime() : string
    {
        return $this->deliveryReadyTime;
    }

    public function setDeliveryCloseTime(string $deliveryCloseTime) : void
    {
        $this->deliveryCloseTime = $deliveryCloseTime;
    }

    public function getDeliveryCloseTime() : string
    {
        return $this->deliveryCloseTime;
    }

    public function setPickupRequirement(array $pickupRequirement) : void
    {
        $this->pickupRequirement = $pickupRequirement;
    }

    public function getPickupRequirement() : array
    {
        return $this->pickupRequirement;
    }

    public function setDeliveryRequirement(array $deliveryRequirement) : void
    {
        $this->deliveryRequirement = $deliveryRequirement;
    }

    public function getDeliveryRequirement() : array
    {
        return $this->deliveryRequirement;
    }

    public function setDeliveryAddress(Address $deliveryAddress) : void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function getDeliveryAddress() : Address
    {
        return $this->deliveryAddress;
    }

    public function setPriceApiIndex(int $priceApiIndex) : void
    {
        $this->priceApiIndex = $priceApiIndex;
    }

    public function getPriceApiIndex() : int
    {
        return $this->priceApiIndex;
    }

    public function setPriceApiRecordId(int $priceApiRecordId) : void
    {
        $this->priceApiRecordId = $priceApiRecordId;
    }

    public function getPriceApiRecordId() : int
    {
        return $this->priceApiRecordId;
    }

    public function setPriceApiIdentify(string $priceApiIdentify) : void
    {
        $this->priceApiIdentify = $priceApiIdentify;
    }

    public function getPriceApiIdentify() : string
    {
        return $this->priceApiIdentify;
    }

    public function setPickupEndDate(int $pickupEndDate) : void
    {
        $this->pickupEndDate = $pickupEndDate;
    }

    public function getPickupEndDate() : int
    {
        return $this->pickupEndDate;
    }

    public function setPickupStartDate(int $pickupStartDate) : void
    {
        $this->pickupStartDate = $pickupStartDate;
    }

    public function getPickupStartDate() : int
    {
        return $this->pickupStartDate;
    }

    public function setDeliveryEndDate(int $deliveryEndDate) : void
    {
        $this->deliveryEndDate = $deliveryEndDate;
    }

    public function getDeliveryEndDate() : int
    {
        return $this->deliveryEndDate;
    }

    public function setDeliveryStartDate(int $deliveryStartDate) : void
    {
        $this->deliveryStartDate = $deliveryStartDate;
    }

    public function getDeliveryStartDate() : int
    {
        return $this->deliveryStartDate;
    }

    public function setPalletNumber(int $palletNumber) : void
    {
        $this->palletNumber = $palletNumber;
    }

    public function getPalletNumber() : int
    {
        return $this->palletNumber;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : OrderLTLRepository
    {
        return $this->repository;
    }

    public function memberCancel() : bool
    {
        return $this->getRepository()->memberCancel($this);
    }

    public function staffCancel() : bool
    {
        return $this->getRepository()->staffCancel($this);
    }

    public function confirm() : bool
    {
        return $this->getRepository()->confirm($this);
    }

    public function batchConfirm(array $orderLTLList) : bool
    {
        return $this->getRepository()->batchConfirm($orderLTLList);
    }

    public function registerPosition() : bool
    {
        return $this->getRepository()->registerPosition($this);
    }

    public function freeze() : bool
    {
        return $this->getRepository()->freeze($this);
    }

    public function unFreeze() : bool
    {
        return $this->getRepository()->unFreeze($this);
    }
    
    public function calculate() : array
    {
        return $this->getRepository()->calculate($this);
    }
}
