<?php
namespace Sdk\Order\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Warehouse\Model\Warehouse;

class DispatchShippingOrder implements IObject
{
    use Object;

    private $id;
    /**
     * @var DispatchOrder $dispatchOrder
     */
    private $dispatchOrder;
    /**
     * @var AmazonLTL $amazonLTL
     */
    private $amazonLTL;
    /**
     * @var array $dispatchItemsAttachments 散板订单佐证材料
     */
    private $dispatchItemsAttachments;
    /**
     * @var array $acceptItemsAttachments 散板订单佐证材料
     */
    private $acceptItemsAttachments;
    /**
     * @var string $position
     */
    private $position;
    /**
     * @var string $stockInNumber
     */
    private $stockInNumber;
    /**
     * @var int $stockInTime
     */
    private $stockInTime;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->dispatchOrder = new DispatchOrder();
        $this->amazonLTL = new AmazonLTL();
        $this->dispatchItemsAttachments = array();
        $this->acceptItemsAttachments = array();
        $this->position = '';
        $this->stockInNumber = '';
        $this->stockInTime = 0;
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->dispatchOrder);
        unset($this->amazonLTL);
        unset($this->dispatchItemsAttachments);
        unset($this->acceptItemsAttachments);
        unset($this->position);
        unset($this->stockInNumber);
        unset($this->stockInTime);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setDispatchOrder(DispatchOrder $dispatchOrder) : void
    {
        $this->dispatchOrder = $dispatchOrder;
    }

    public function getDispatchOrder() : DispatchOrder
    {
        return $this->dispatchOrder;
    }

    public function setAmazonLTL(AmazonLTL $amazonLTL) : void
    {
        $this->amazonLTL = $amazonLTL;
    }

    public function getAmazonLTL() : AmazonLTL
    {
        return $this->amazonLTL;
    }

    public function setDispatchItemsAttachments(array $dispatchItemsAttachments) : void
    {
        $this->dispatchItemsAttachments = $dispatchItemsAttachments;
    }

    public function getDispatchItemsAttachments() : array
    {
        return $this->dispatchItemsAttachments;
    }

    public function setAcceptItemsAttachments(array $acceptItemsAttachments) : void
    {
        $this->acceptItemsAttachments = $acceptItemsAttachments;
    }

    public function getAcceptItemsAttachments() : array
    {
        return $this->acceptItemsAttachments;
    }

    public function setPosition(string $position) : void
    {
        $this->position = $position;
    }

    public function getPosition() : string
    {
        return $this->position;
    }

    public function setStockInNumber(string $stockInNumber) : void
    {
        $this->stockInNumber = $stockInNumber;
    }

    public function getStockInNumber() : string
    {
        return $this->stockInNumber;
    }

    public function setStockInTime(int $stockInTime) : void
    {
        $this->stockInTime = $stockInTime;
    }

    public function getStockInTime() : int
    {
        return $this->stockInTime;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
