<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullRecOrderLTLOrder extends RecOrderLTLOrder implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function registerPickupDate() : bool
    {
        return $this->resourceNotExist();
    }
    
    public function pickup() : bool
    {
        return $this->resourceNotExist();
    }
    
    public function stockIn() : bool
    {
        return $this->resourceNotExist();
    }
}
