<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\OrderLTLRepository;

class ItemLTL
{

    private $id;
    /**
     * @var float $length
     */
    private $length;
    /**
     * @var float $width
     */
    private $width;
    /**
     * @var float $height
     */
    private $height;
    /**
     * @var float $weight
     */
    private $weight;
    /**
     * @var int $weightLB
     */
    private $weightLB;
    /**
     * @var int $weightUnit
     */
    private $weightUnit;
    /**
     * @var string $goodsLevel
     */
    private $goodsLevel;
    /**
     * @var int $volumeInch
     */
    private $volumeInch;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->length = 0;
        $this->width = 0;
        $this->height = 0;
        $this->weight = 0;
        $this->weightLB = 0;
        $this->weightUnit = 0;
        $this->goodsLevel = '';
        $this->volumeInch = 0;
        $this->repository = new OrderLTLRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->length);
        unset($this->width);
        unset($this->height);
        unset($this->weight);
        unset($this->weightLB);
        unset($this->weightUnit);
        unset($this->goodsLevel);
        unset($this->volumeInch);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setLength(float $length) : void
    {
        $this->length = $length;
    }

    public function getLength() : float
    {
        return $this->length;
    }

    public function setWidth(float $width) : void
    {
        $this->width = $width;
    }

    public function getWidth() : float
    {
        return $this->width;
    }

    public function setHeight(float $height) : void
    {
        $this->height = $height;
    }

    public function getHeight() : float
    {
        return $this->height;
    }

    public function setWeight(float $weight) : void
    {
        $this->weight = $weight;
    }

    public function getWeight() : float
    {
        return $this->weight;
    }

    public function setWeightLB(int $weightLB) : void
    {
        $this->weightLB = $weightLB;
    }

    public function getWeightLB() : int
    {
        return $this->weightLB;
    }

    public function setWeightUnit(int $weightUnit) : void
    {
        $this->weightUnit = $weightUnit;
    }

    public function getWeightUnit() : int
    {
        return $this->weightUnit;
    }

    public function setGoodsLevel(string $goodsLevel) : void
    {
        $this->goodsLevel = $goodsLevel;
    }

    public function getGoodsLevel() : string
    {
        return $this->goodsLevel;
    }

    public function setVolumeInch(int $volumeInch) : void
    {
        $this->volumeInch = $volumeInch;
    }

    public function getVolumeInch() : int
    {
        return $this->volumeInch;
    }

    protected function getRepository() : OrderLTLRepository
    {
        return $this->repository;
    }

    public function calculateGoodsLevel() : bool
    {
        return $this->getRepository()->calculateGoodsLevel($this);
    }
}
