<?php
namespace Sdk\Order\Model;

/**
 * 子订单接口
 */
interface ISubOrder
{
    /**
     * 设置id
     * @param $id
     */
    public function setId($id);

    /**
     * 获取 id
     * @return $id
     */
    public function getId();
}
