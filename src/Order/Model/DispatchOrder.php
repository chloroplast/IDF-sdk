<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\User\Staff\Model\Staff;
use Sdk\CarType\Model\CarType;

use Sdk\Order\Repository\DispatchOrderRepository;

class DispatchOrder implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    private $id;
    /**
     * @var CarType $carType 车辆类型
     */
    private $carType;
    /**
     * @var Staff $staff 后台用户
     */
    private $staff;
    /**
     * @var string $number 出库订单编号
     */
    private $number;
    /**
     * @var float $totalWeight 总重量
     */
    private $totalWeight;
    /**
     * @var int $totalPalletNumber 总板数
     */
    private $totalPalletNumber;
    /**
     * @var int $totalOrderNumber 总订单数
     */
    private $totalOrderNumber;
    /**
     * @var array $amazonLTL 亚马逊散板订单
     */
    private $amazonLTL;
    /**
     * @var string $references
     */
    private $references;
    /**
     * @var array $dispatchItemsAttachments 散板订单佐证材料
     */
    private $dispatchItemsAttachments;
    /**
     * @var array $acceptItemsAttachments 散板订单佐证材料
     */
    private $acceptItemsAttachments;
    /**
     * @var array $dispatchAttachments 出库佐证材料
     */
    private $dispatchAttachments;
    /**
     * @var array $acceptAttachments 签收佐证材料
     */
    private $acceptAttachments;
    /**
     * @var string $isaNumber
     */
    private $isaNumber;
    /**
     * @var int $pickupDate 取货日期
     */
    private $pickupDate;
    /**
     * @var int $deliveryDate 送达日期
     */
    private $deliveryDate;
    /**
     * @var array $closePage 亚马逊凭证
     */
    private $closePage;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->carType = new CarType();
        $this->staff = new Staff();
        $this->number = '';
        $this->totalWeight = 0;
        $this->totalPalletNumber = 0;
        $this->totalOrderNumber = 0;
        $this->amazonLTL = array();
        $this->references = '';
        $this->dispatchItemsAttachments = array();
        $this->acceptItemsAttachments = array();
        $this->dispatchAttachments = array();
        $this->acceptAttachments = array();
        $this->isaNumber = '';
        $this->pickupDate = 0;
        $this->deliveryDate = 0;
        $this->closePage = array();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new DispatchOrderRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->carType);
        unset($this->staff);
        unset($this->number);
        unset($this->totalWeight);
        unset($this->totalPalletNumber);
        unset($this->totalOrderNumber);
        unset($this->amazonLTL);
        unset($this->references);
        unset($this->dispatchItemsAttachments);
        unset($this->acceptItemsAttachments);
        unset($this->dispatchAttachments);
        unset($this->acceptAttachments);
        unset($this->isaNumber);
        unset($this->pickupDate);
        unset($this->deliveryDate);
        unset($this->closePage);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setCarType(CarType $carType) : void
    {
        $this->carType = $carType;
    }

    public function getCarType() : CarType
    {
        return $this->carType;
    }

    public function setStaff(Staff $staff) : void
    {
        $this->staff = $staff;
    }

    public function getStaff() : Staff
    {
        return $this->staff;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setTotalWeight(float $totalWeight) : void
    {
        $this->totalWeight = $totalWeight;
    }

    public function getTotalWeight() : float
    {
        return $this->totalWeight;
    }

    public function setTotalPalletNumber(int $totalPalletNumber) : void
    {
        $this->totalPalletNumber = $totalPalletNumber;
    }

    public function getTotalPalletNumber() : int
    {
        return $this->totalPalletNumber;
    }

    public function setTotalOrderNumber(int $totalOrderNumber) : void
    {
        $this->totalOrderNumber = $totalOrderNumber;
    }

    public function getTotalOrderNumber() : int
    {
        return $this->totalOrderNumber;
    }

    public function setAmazonLTL(array $amazonLTL) : void
    {
        $this->amazonLTL = $amazonLTL;
    }

    public function getAmazonLTL() : array
    {
        return $this->amazonLTL;
    }

    public function setReferences(string $references) : void
    {
        $this->references = $references;
    }

    public function getReferences() : string
    {
        return $this->references;
    }

    public function setDispatchItemsAttachments(array $dispatchItemsAttachments) : void
    {
        $this->dispatchItemsAttachments = $dispatchItemsAttachments;
    }

    public function getDispatchItemsAttachments() : array
    {
        return $this->dispatchItemsAttachments;
    }

    public function setAcceptItemsAttachments(array $acceptItemsAttachments) : void
    {
        $this->acceptItemsAttachments = $acceptItemsAttachments;
    }

    public function getAcceptItemsAttachments() : array
    {
        return $this->acceptItemsAttachments;
    }

    public function setDispatchAttachments(array $dispatchAttachments) : void
    {
        $this->dispatchAttachments = $dispatchAttachments;
    }

    public function getDispatchAttachments() : array
    {
        return $this->dispatchAttachments;
    }

    public function setAcceptAttachments(array $acceptAttachments) : void
    {
        $this->acceptAttachments = $acceptAttachments;
    }

    public function getAcceptAttachments() : array
    {
        return $this->acceptAttachments;
    }

    public function setIsaNumber(string $isaNumber) : void
    {
        $this->isaNumber = $isaNumber;
    }

    public function getIsaNumber() : string
    {
        return $this->isaNumber;
    }

    public function setPickupDate(int $pickupDate) : void
    {
        $this->pickupDate = $pickupDate;
    }

    public function getPickupDate() : int
    {
        return $this->pickupDate;
    }

    public function setDeliveryDate(int $deliveryDate) : void
    {
        $this->deliveryDate = $deliveryDate;
    }

    public function getDeliveryDate() : int
    {
        return $this->deliveryDate;
    }

    public function setClosePage(array $closePage) : void
    {
        $this->closePage = $closePage;
    }

    public function getClosePage() : array
    {
        return $this->closePage;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : DispatchOrderRepository
    {
        return $this->repository;
    }

    public function updateISA() : bool
    {
        return $this->getRepository()->updateISA($this);
    }

    public function registerDispatchDate() : bool
    {
        return $this->getRepository()->registerDispatchDate($this);
    }

    public function dispatch() : bool
    {
        return $this->getRepository()->dispatch($this);
    }

    public function accept() : bool
    {
        return $this->getRepository()->accept($this);
    }
}
