<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Interfaces\INull;

class NullOrderLTL extends OrderLTL implements INull
{
    private static $instance;

    public static function &getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    protected function resourceNotExist() : bool
    {
        Core::setLastError(RESOURCE_NOT_EXIST);
        return false;
    }

    public function memberCancel() : bool
    {
        return $this->resourceNotExist();
    }

    public function staffCancel() : bool
    {
        return $this->resourceNotExist();
    }

    public function confirm() : bool
    {
        return $this->resourceNotExist();
    }

    public function batchConfirm(array $orderLTLList) : bool
    {
        unset($orderLTLList);
        return $this->resourceNotExist();
    }

    public function registerPosition() : bool
    {
        return $this->resourceNotExist();
    }

    public function freeze() : bool
    {
        return $this->resourceNotExist();
    }

    public function unFreeze() : bool
    {
        return $this->resourceNotExist();
    }

    public function calculate() : array
    {
        return array();
    }
}
