<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\User\Staff\Model\Staff;

class OrderStatusHistory implements IObject
{
    use Object;

    private $id;
    /**
     * @var ISubOrder $subOrder 子订单
     */
    private $subOrder;
    /**
     * @var Staff $staff 后台用户
     */
    private $staff;
    /**
     * @var int $orderAttribute 订单属性
     */
    private $orderAttribute;
    /**
     * @var int $orderType 订单类型
     */
    private $orderType;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->subOrder = new AmazonLTL();
        $this->staff = new Staff();
        $this->orderAttribute = 0;
        $this->orderType = 0;
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->subOrder);
        unset($this->staff);
        unset($this->orderAttribute);
        unset($this->orderType);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setSubOrder(ISubOrder $subOrder) : void
    {
        $this->subOrder = $subOrder;
    }

    public function getSubOrder() : ISubOrder
    {
        return $this->subOrder;
    }

    public function setStaff(Staff $staff) : void
    {
        $this->staff = $staff;
    }

    public function getStaff() : Staff
    {
        return $this->staff;
    }

    public function setOrderAttribute(int $orderAttribute) : void
    {
        $this->orderAttribute = $orderAttribute;
    }

    public function getOrderAttribute() : int
    {
        return $this->orderAttribute;
    }

    public function setOrderType(int $orderType) : void
    {
        $this->orderType = $orderType;
    }

    public function getOrderType() : int
    {
        return $this->orderType;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
