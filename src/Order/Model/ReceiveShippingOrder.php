<?php
namespace Sdk\Order\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Warehouse\Model\Warehouse;

class ReceiveShippingOrder implements IObject
{
    use Object;

    private $id;
    /**
     * @var ReceiveOrder $receiveOrder
     */
    private $receiveOrder;
    /**
     * @var AmazonLTL $amazonLTL
     */
    private $amazonLTL;
    /**
     * @var array $stockInItemsAttachments 散板订单佐证材料
     */
    private $stockInItemsAttachments;
    /**
     * @var Warehouse $pickupWarehouse 取货仓库
     */
    private $pickupWarehouse;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->receiveOrder = new ReceiveOrder();
        $this->amazonLTL = new AmazonLTL();
        $this->stockInItemsAttachments = array();
        $this->pickupWarehouse = new Warehouse();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->receiveOrder);
        unset($this->amazonLTL);
        unset($this->stockInItemsAttachments);
        unset($this->pickupWarehouse);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setReceiveOrder(ReceiveOrder $receiveOrder) : void
    {
        $this->receiveOrder = $receiveOrder;
    }

    public function getReceiveOrder() : ReceiveOrder
    {
        return $this->receiveOrder;
    }

    public function setAmazonLTL(AmazonLTL $amazonLTL) : void
    {
        $this->amazonLTL = $amazonLTL;
    }

    public function getAmazonLTL() : AmazonLTL
    {
        return $this->amazonLTL;
    }

    public function setStockInItemsAttachments(array $stockInItemsAttachments) : void
    {
        $this->stockInItemsAttachments = $stockInItemsAttachments;
    }

    public function getStockInItemsAttachments() : array
    {
        return $this->stockInItemsAttachments;
    }

    public function setPickupWarehouse(Warehouse $pickupWarehouse) : void
    {
        $this->pickupWarehouse = $pickupWarehouse;
    }

    public function getPickupWarehouse() : Warehouse
    {
        return $this->pickupWarehouse;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
