<?php
namespace Sdk\Order\Model;

use Sdk\Role\Purview\Model\Purview;
use Sdk\Role\Purview\Model\IPurviewAble;

class DisOrderFTLOrderPurview extends Purview
{
    public function __construct()
    {
        parent::__construct(IPurviewAble::COLUMN['DIS_ORDERFTL_ORDER']);
    }

    public function registerDispatchDate() : bool
    {
        return $this->operation('registerDispatchDate');
    }

    public function dispatch() : bool
    {
        return $this->operation('dispatch');
    }

    public function accept() : bool
    {
        return $this->operation('accept');
    }
}
