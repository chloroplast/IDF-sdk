<?php
namespace Sdk\Order\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class DisShipAmaFTLOrder implements IObject
{
    use Object;

    private $id;
    /**
     * @var DisAmaFTLOrder $disAmaFTLOrder
     */
    private $disAmaFTLOrder;
    /**
     * @var AmazonFTL $amazonFTL
     */
    private $amazonFTL;
    /**
     * @var array $dispatchItemsAttachments 整板订单佐证材料
     */
    private $dispatchItemsAttachments;
    /**
     * @var array $acceptItemsAttachments 整板订单佐证材料
     */
    private $acceptItemsAttachments;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->disAmaFTLOrder = new DisAmaFTLOrder();
        $this->amazonFTL = new AmazonFTL();
        $this->dispatchItemsAttachments = array();
        $this->acceptItemsAttachments = array();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->disAmaFTLOrder);
        unset($this->amazonFTL);
        unset($this->dispatchItemsAttachments);
        unset($this->acceptItemsAttachments);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setDisAmaFTLOrder(DisAmaFTLOrder $disAmaFTLOrder) : void
    {
        $this->disAmaFTLOrder = $disAmaFTLOrder;
    }

    public function getDisAmaFTLOrder() : DisAmaFTLOrder
    {
        return $this->disAmaFTLOrder;
    }

    public function setAmazonFTL(AmazonFTL $amazonFTL) : void
    {
        $this->amazonFTL = $amazonFTL;
    }

    public function getAmazonFTL() : AmazonFTL
    {
        return $this->amazonFTL;
    }

    public function setDispatchItemsAttachments(array $dispatchItemsAttachments) : void
    {
        $this->dispatchItemsAttachments = $dispatchItemsAttachments;
    }

    public function getDispatchItemsAttachments() : array
    {
        return $this->dispatchItemsAttachments;
    }

    public function setAcceptItemsAttachments(array $acceptItemsAttachments) : void
    {
        $this->acceptItemsAttachments = $acceptItemsAttachments;
    }

    public function getAcceptItemsAttachments() : array
    {
        return $this->acceptItemsAttachments;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
