<?php
namespace Sdk\Order\Model;

use Sdk\Role\Purview\Model\Purview;
use Sdk\Role\Purview\Model\IPurviewAble;

class DispatchOrderPurview extends Purview
{
    public function __construct()
    {
        parent::__construct(IPurviewAble::COLUMN['DISPATCH_ORDER']);
    }

    public function add() : bool
    {
        return $this->operation('add');
    }

    public function edit() : bool
    {
        return $this->operation('edit');
    }

    public function updateISA() : bool
    {
        return $this->operation('updateISA');
    }

    public function registerDispatchDate() : bool
    {
        return $this->operation('registerDispatchDate');
    }

    public function dispatch() : bool
    {
        return $this->operation('dispatch');
    }

    public function accept() : bool
    {
        return $this->operation('accept');
    }
}
