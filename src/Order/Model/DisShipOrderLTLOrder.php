<?php
namespace Sdk\Order\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class DisShipOrderLTLOrder implements IObject
{
    use Object;

    private $id;
    /**
     * @var DisOrderLTLOrder $disOrderLTLOrder
     */
    private $disOrderLTLOrder;
    /**
     * @var OrderLTL $orderLTL
     */
    private $orderLTL;
    /**
     * @var array $dispatchItemsAttachments 整板订单佐证材料
     */
    private $dispatchItemsAttachments;
    /**
     * @var array $acceptItemsAttachments 整板订单佐证材料
     */
    private $acceptItemsAttachments;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->disOrderLTLOrder = new DisOrderLTLOrder();
        $this->orderLTL = new OrderLTL();
        $this->dispatchItemsAttachments = array();
        $this->acceptItemsAttachments = array();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->disOrderLTLOrder);
        unset($this->orderLTL);
        unset($this->dispatchItemsAttachments);
        unset($this->acceptItemsAttachments);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setDisOrderLTLOrder(DisOrderLTLOrder $disOrderLTLOrder) : void
    {
        $this->disOrderLTLOrder = $disOrderLTLOrder;
    }

    public function getDisOrderLTLOrder() : DisOrderLTLOrder
    {
        return $this->disOrderLTLOrder;
    }

    public function setOrderLTL(OrderLTL $orderLTL) : void
    {
        $this->orderLTL = $orderLTL;
    }

    public function getOrderLTL() : OrderLTL
    {
        return $this->orderLTL;
    }

    public function setDispatchItemsAttachments(array $dispatchItemsAttachments) : void
    {
        $this->dispatchItemsAttachments = $dispatchItemsAttachments;
    }

    public function getDispatchItemsAttachments() : array
    {
        return $this->dispatchItemsAttachments;
    }

    public function setAcceptItemsAttachments(array $acceptItemsAttachments) : void
    {
        $this->acceptItemsAttachments = $acceptItemsAttachments;
    }

    public function getAcceptItemsAttachments() : array
    {
        return $this->acceptItemsAttachments;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }
}
