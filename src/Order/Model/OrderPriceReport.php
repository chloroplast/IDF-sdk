<?php
namespace Sdk\Order\Model;

class OrderPriceReport
{

    private $id;
    /**
     * @var float $price
     */
    private $price;
    /**
     * @var array $priceRemark
     */
    private $priceRemark;
    /**
     * @var int $recordId
     */
    private $recordId;
    /**
     * @var int $apiIndex
     */
    private $apiIndex;
    /**
     * @var string $identify
     */
    private $identify;
    /**
     * @var int $pickupEndDate
     */
    private $pickupEndDate;
    /**
     * @var int $pickupStartDate
     */
    private $pickupStartDate;
    /**
     * @var int $deliveryEndDate
     */
    private $deliveryEndDate;
    /**
     * @var int $deliveryStartDate
     */
    private $deliveryStartDate;
    /**
     * @var int $status
     */
    private $status;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->price = 0;
        $this->priceRemark = array();
        $this->recordId = 0;
        $this->apiIndex = 0;
        $this->identify = '';
        $this->pickupEndDate = 0;
        $this->pickupStartDate = 0;
        $this->deliveryEndDate = 0;
        $this->deliveryStartDate = 0;
        $this->status = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->price);
        unset($this->priceRemark);
        unset($this->recordId);
        unset($this->apiIndex);
        unset($this->identify);
        unset($this->pickupEndDate);
        unset($this->pickupStartDate);
        unset($this->deliveryEndDate);
        unset($this->deliveryStartDate);
        unset($this->status);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setPrice(float $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function setPriceRemark(array $priceRemark) : void
    {
        $this->priceRemark = $priceRemark;
    }

    public function getPriceRemark() : array
    {
        return $this->priceRemark;
    }

    public function setRecordId(int $recordId) : void
    {
        $this->recordId = $recordId;
    }

    public function getRecordId() : int
    {
        return $this->recordId;
    }

    public function setApiIndex(int $apiIndex) : void
    {
        $this->apiIndex = $apiIndex;
    }

    public function getApiIndex() : int
    {
        return $this->apiIndex;
    }

    public function setIdentify(string $identify) : void
    {
        $this->identify = $identify;
    }

    public function getIdentify() : string
    {
        return $this->identify;
    }

    public function setPickupEndDate(int $pickupEndDate) : void
    {
        $this->pickupEndDate = $pickupEndDate;
    }

    public function getPickupEndDate() : int
    {
        return $this->pickupEndDate;
    }

    public function setPickupStartDate(int $pickupStartDate) : void
    {
        $this->pickupStartDate = $pickupStartDate;
    }

    public function getPickupStartDate() : int
    {
        return $this->pickupStartDate;
    }

    public function setDeliveryEndDate(int $deliveryEndDate) : void
    {
        $this->deliveryEndDate = $deliveryEndDate;
    }

    public function getDeliveryEndDate() : int
    {
        return $this->deliveryEndDate;
    }

    public function setDeliveryStartDate(int $deliveryStartDate) : void
    {
        $this->deliveryStartDate = $deliveryStartDate;
    }

    public function getDeliveryStartDate() : int
    {
        return $this->deliveryStartDate;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    public function getStatus() : int
    {
        return $this->status;
    }
}
