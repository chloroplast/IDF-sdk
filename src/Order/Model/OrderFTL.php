<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\User\Member\Model\Member;
use Sdk\User\Staff\Model\Staff;
use Sdk\Warehouse\Model\Warehouse;
use Sdk\Address\Model\Address;

use Sdk\Order\Repository\OrderFTLRepository;

class OrderFTL implements IObject, IOperateAble, ISubOrder
{
    use Object, OperateAbleTrait;

    const WEEKEND_DELIVERY = array(
        'NO' => 0,
        'YES' => 1
    );
    const WEEKEND_DELIVERY_CN = array(
        self::WEEKEND_DELIVERY['NO'] => '否',
        self::WEEKEND_DELIVERY['YES'] => '是'
    );

    const WEIGHT_UNIT = array(
        'LB' => 1,
        'KG' => 2
    );
    const WEIGHT_UNIT_CN = array(
        self::WEIGHT_UNIT['LB'] => 'LB',
        self::WEIGHT_UNIT['KG'] => 'KG'
    );

    const FROZEN = array(
        'NO' => 0,
        'YES' => 1
    );
    const FROZEN_CN = array(
        self::FROZEN['NO'] => '未冻结',
        self::FROZEN['YES'] => '冻结'
    );

    const A_STATUS = array(
        'CANCELED' => -2,
        'TO_BE_CONFIRMED' => 0,
        'TO_BE_STOCKED_OUT' => 6,
        'TO_BE_DELIVERED' => 8,
        'COMPLETED' => 10
    );
    const A_STATUS_CN = array(
        self::A_STATUS['CANCELED'] => '已取消',
        self::A_STATUS['TO_BE_CONFIRMED'] => '待确认',
        self::A_STATUS['TO_BE_STOCKED_OUT'] => '待出库',
        self::A_STATUS['TO_BE_DELIVERED'] => '待派送',
        self::A_STATUS['COMPLETED'] => '已完成'
    );

    private $id;
    /**
     * @var Member $member 官网用户
     */
    private $member;
    /**
     * @var Staff $staff 后台用户
     */
    private $staff;
    /**
     * @var Warehouse $pickupWarehouse 取货仓库
     */
    private $pickupWarehouse;
    /**
     * @var Address $address 发货地址
     */
    private $address;
    /**
     * @var Address $deliveryAddress 收货地址
     */
    private $deliveryAddress;
    /**
     * @var string $deliveryName 收货人
     */
    private $deliveryName;
    /**
     * @var string $number 订单编号
     */
    private $number;
    /**
     * @var int $weekendDelivery 周末送货
     */
    private $weekendDelivery;
    /**
     * @var array $items 货物信息
     */
    private $items;
    /**
     * @var string $goodsType
     */
    private $goodsType;
    /**
     * @var float $goodsValue
     */
    private $goodsValue;
    /**
     * @var string $packingType
     */
    private $packingType;
    /**
     * @var int $palletNumber
     */
    private $palletNumber;
    /**
     * @var int $itemNumber
     */
    private $itemNumber;
    /**
     * @var float $weight
     */
    private $weight;
    /**
     * @var int $weightUnit
     */
    private $weightUnit;
    /**
     * @var string $poNumber
     */
    private $poNumber;
    /**
     * @var int $pickupDate
     */
    private $pickupDate;
    /**
     * @var int $readyTime
     */
    private $readyTime;
    /**
     * @var int $closeTime
     */
    private $closeTime;
    /**
     * @var int $deliveryDate
     */
    private $deliveryDate;
    /**
     * @var int $deliveryReadyTime
     */
    private $deliveryReadyTime;
    /**
     * @var int $deliveryCloseTime
     */
    private $deliveryCloseTime;
    /**
     * @var string $pickupNumber
     */
    private $pickupNumber;
    /**
     * @var string $remark
     */
    private $remark;
    /**
     * @var int $frozen
     */
    private $frozen;
    /**
     * @var MemberOrder $memberOrder 用户订单id
     */
    private $memberOrder;
    /**
     * @var float $price
     */
    private $price;
    /**
     * @var array $priceRemark
     */
    private $priceRemark;

    /**
     * @var string $pickupAddressType
     */
    private $pickupAddressType;
    /**
     * @var string $deliveryAddressType
     */
    private $deliveryAddressType;
    /**
     * @var int $priceApiIndex
     */
    private $priceApiIndex;
    /**
     * @var int $priceApiRecordId
     */
    private $priceApiRecordId;
    /**
     * @var string $priceApiIdentify
     */
    private $priceApiIdentify;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->member = new Member();
        $this->staff = new Staff();
        $this->pickupWarehouse = new Warehouse();
        $this->address = new Address();
        $this->deliveryAddress = new Address();
        $this->deliveryName = '';
        $this->number = '';
        $this->weekendDelivery = self::WEEKEND_DELIVERY['NO'];
        $this->items = array();
        $this->goodsType = '';
        $this->goodsValue = 0;
        $this->packingType = '';
        $this->palletNumber = 0;
        $this->itemNumber = 0;
        $this->weight = 0;
        $this->weightUnit = 0;
        $this->poNumber = '';
        $this->pickupDate = 0;
        $this->readyTime = 0;
        $this->closeTime = 0;
        $this->deliveryDate = 0;
        $this->deliveryReadyTime = 0;
        $this->deliveryCloseTime = 0;
        $this->pickupNumber = '';
        $this->remark = '';
        $this->frozen = self::FROZEN['NO'];
        $this->memberOrder = new MemberOrder();
        $this->price = 0;
        $this->priceRemark = array();
        $this->pickupAddressType = '';
        $this->deliveryAddressType = '';
        $this->priceApiIndex = 0;
        $this->priceApiRecordId = 0;
        $this->priceApiIdentify = '';
        $this->status = self::A_STATUS['TO_BE_CONFIRMED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new OrderFTLRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->member);
        unset($this->staff);
        unset($this->pickupWarehouse);
        unset($this->address);
        unset($this->deliveryAddress);
        unset($this->deliveryName);
        unset($this->number);
        unset($this->weekendDelivery);
        unset($this->items);
        unset($this->goodsType);
        unset($this->goodsValue);
        unset($this->packingType);
        unset($this->palletNumber);
        unset($this->itemNumber);
        unset($this->weight);
        unset($this->weightUnit);
        unset($this->poNumber);
        unset($this->pickupDate);
        unset($this->readyTime);
        unset($this->closeTime);
        unset($this->deliveryDate);
        unset($this->deliveryReadyTime);
        unset($this->deliveryCloseTime);
        unset($this->pickupNumber);
        unset($this->remark);
        unset($this->frozen);
        unset($this->memberOrder);
        unset($this->price);
        unset($this->priceRemark);
        unset($this->pickupAddressType);
        unset($this->deliveryAddressType);
        unset($this->priceApiIndex);
        unset($this->priceApiRecordId);
        unset($this->priceApiIdentify);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setMember(Member $member) : void
    {
        $this->member = $member;
    }

    public function getMember() : Member
    {
        return $this->member;
    }

    public function setStaff(Staff $staff) : void
    {
        $this->staff = $staff;
    }

    public function getStaff() : Staff
    {
        return $this->staff;
    }

    public function setPickupWarehouse(Warehouse $pickupWarehouse) : void
    {
        $this->pickupWarehouse = $pickupWarehouse;
    }

    public function getPickupWarehouse() : Warehouse
    {
        return $this->pickupWarehouse;
    }

    public function setAddress(Address $address) : void
    {
        $this->address = $address;
    }

    public function getAddress() : Address
    {
        return $this->address;
    }

    public function setDeliveryAddress(Address $deliveryAddress) : void
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    public function getDeliveryAddress() : Address
    {
        return $this->deliveryAddress;
    }

    public function setDeliveryName(string $deliveryName) : void
    {
        $this->deliveryName = $deliveryName;
    }

    public function getDeliveryName() : string
    {
        return $this->deliveryName;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setWeekendDelivery(int $weekendDelivery) : void
    {
        $this->weekendDelivery = $weekendDelivery;
    }

    public function getWeekendDelivery() : int
    {
        return $this->weekendDelivery;
    }

    public function setItems(array $items) : void
    {
        $this->items = $items;
    }

    public function getItems() : array
    {
        return $this->items;
    }

    public function setGoodsType(string $goodsType) : void
    {
        $this->goodsType = $goodsType;
    }

    public function getGoodsType() : string
    {
        return $this->goodsType;
    }

    public function setGoodsValue(float $goodsValue) : void
    {
        $this->goodsValue = $goodsValue;
    }

    public function getGoodsValue() : float
    {
        return $this->goodsValue;
    }

    public function setPackingType(string $packingType) : void
    {
        $this->packingType = $packingType;
    }

    public function getPackingType() : string
    {
        return $this->packingType;
    }

    public function setPalletNumber(int $palletNumber) : void
    {
        $this->palletNumber = $palletNumber;
    }

    public function getPalletNumber() : int
    {
        return $this->palletNumber;
    }

    public function setItemNumber(int $itemNumber) : void
    {
        $this->itemNumber = $itemNumber;
    }

    public function getItemNumber() : int
    {
        return $this->itemNumber;
    }

    public function setWeight(float $weight) : void
    {
        $this->weight = $weight;
    }

    public function getWeight() : float
    {
        return $this->weight;
    }

    public function setWeightUnit(int $weightUnit) : void
    {
        $this->weightUnit = $weightUnit;
    }

    public function getWeightUnit() : int
    {
        return $this->weightUnit;
    }

    public function setPoNumber(string $poNumber) : void
    {
        $this->poNumber = $poNumber;
    }

    public function getPoNumber() : string
    {
        return $this->poNumber;
    }

    public function setPickupDate(int $pickupDate) : void
    {
        $this->pickupDate = $pickupDate;
    }

    public function getPickupDate() : int
    {
        return $this->pickupDate;
    }

    public function setReadyTime(int $readyTime) : void
    {
        $this->readyTime = $readyTime;
    }

    public function getReadyTime() : int
    {
        return $this->readyTime;
    }

    public function setCloseTime(int $closeTime) : void
    {
        $this->closeTime = $closeTime;
    }

    public function getCloseTime() : int
    {
        return $this->closeTime;
    }

    public function setDeliveryDate(int $deliveryDate) : void
    {
        $this->deliveryDate = $deliveryDate;
    }

    public function getDeliveryDate() : int
    {
        return $this->deliveryDate;
    }

    public function setDeliveryReadyTime(int $deliveryReadyTime) : void
    {
        $this->deliveryReadyTime = $deliveryReadyTime;
    }

    public function getDeliveryReadyTime() : int
    {
        return $this->deliveryReadyTime;
    }

    public function setDeliveryCloseTime(int $deliveryCloseTime) : void
    {
        $this->deliveryCloseTime = $deliveryCloseTime;
    }

    public function getDeliveryCloseTime() : int
    {
        return $this->deliveryCloseTime;
    }

    public function setPickupNumber(string $pickupNumber) : void
    {
        $this->pickupNumber = $pickupNumber;
    }

    public function getPickupNumber() : string
    {
        return $this->pickupNumber;
    }

    public function setRemark(string $remark) : void
    {
        $this->remark = $remark;
    }

    public function getRemark() : string
    {
        return $this->remark;
    }

    public function setFrozen(int $frozen) : void
    {
        $this->frozen = $frozen;
    }

    public function getFrozen() : int
    {
        return $this->frozen;
    }

    public function setMemberOrder(MemberOrder $memberOrder) : void
    {
        $this->memberOrder = $memberOrder;
    }

    public function getMemberOrder() : MemberOrder
    {
        return $this->memberOrder;
    }

    public function setPrice(float $price) : void
    {
        $this->price = $price;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function setPriceRemark(array $priceRemark) : void
    {
        $this->priceRemark = $priceRemark;
    }

    public function getPriceRemark() : array
    {
        return $this->priceRemark;
    }

    public function setPickupAddressType(string $pickupAddressType) : void
    {
        $this->pickupAddressType = $pickupAddressType;
    }

    public function getPickupAddressType() : string
    {
        return $this->pickupAddressType;
    }

    public function setDeliveryAddressType(string $deliveryAddressType) : void
    {
        $this->deliveryAddressType = $deliveryAddressType;
    }

    public function getDeliveryAddressType() : string
    {
        return $this->deliveryAddressType;
    }

    public function setPriceApiIndex(int $priceApiIndex) : void
    {
        $this->priceApiIndex = $priceApiIndex;
    }

    public function getPriceApiIndex() : int
    {
        return $this->priceApiIndex;
    }

    public function setPriceApiRecordId(int $priceApiRecordId) : void
    {
        $this->priceApiRecordId = $priceApiRecordId;
    }

    public function getPriceApiRecordId() : int
    {
        return $this->priceApiRecordId;
    }

    public function setPriceApiIdentify(string $priceApiIdentify) : void
    {
        $this->priceApiIdentify = $priceApiIdentify;
    }

    public function getPriceApiIdentify() : string
    {
        return $this->priceApiIdentify;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : OrderFTLRepository
    {
        return $this->repository;
    }

    public function memberCancel() : bool
    {
        return $this->getRepository()->memberCancel($this);
    }

    public function staffCancel() : bool
    {
        return $this->getRepository()->staffCancel($this);
    }

    public function confirm() : bool
    {
        return $this->getRepository()->confirm($this);
    }

    public function batchConfirm(array $amazonFTLList) : bool
    {
        return $this->getRepository()->batchConfirm($amazonFTLList);
    }

    public function freeze() : bool
    {
        return $this->getRepository()->freeze($this);
    }

    public function unFreeze() : bool
    {
        return $this->getRepository()->unFreeze($this);
    }
    
    public function calculate() : array
    {
        return $this->getRepository()->calculate($this);
    }
}
