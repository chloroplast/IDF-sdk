<?php
namespace Sdk\Order\Model;

use Sdk\Role\Purview\Model\Purview;
use Sdk\Role\Purview\Model\IPurviewAble;

class AmazonLTLPurview extends Purview
{
    public function __construct()
    {
        parent::__construct(IPurviewAble::COLUMN['AMAZONLTL']);
    }

    public function add() : bool
    {
        return $this->operation('add');
    }

    public function edit() : bool
    {
        return $this->operation('edit');
    }

    public function staffCancel() : bool
    {
        return $this->operation('staffCancel');
    }

    public function confirm() : bool
    {
        return $this->operation('confirm');
    }

    public function batchConfirm(array $amazonLTLList) : bool
    {
        unset($amazonLTLList);
        return $this->operation('batchConfirm');
    }

    public function registerPosition() : bool
    {
        return $this->operation('registerPosition');
    }

    public function freeze() : bool
    {
        return $this->operation('freeze');
    }

    public function unFreeze() : bool
    {
        return $this->operation('unFreeze');
    }
}
