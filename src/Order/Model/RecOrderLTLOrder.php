<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\User\Staff\Model\Staff;
use Sdk\CarType\Model\CarType;

use Sdk\Order\Repository\RecOrderLTLOrderRepository;

class RecOrderLTLOrder implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    private $id;
    /**
     * @var CarType $carType 车辆类型
     */
    private $carType;
    /**
     * @var Staff $staff 后台用户
     */
    private $staff;
    /**
     * @var string $number 取货订单编号
     */
    private $number;
    /**
     * @var float $totalWeight 总重量
     */
    private $totalWeight;
    /**
     * @var int $totalPalletNumber 总板数
     */
    private $totalPalletNumber;
    /**
     * @var int $totalOrderNumber 总订单数
     */
    private $totalOrderNumber;
    /**
     * @var array $orderLTL 非亚马逊散板订单
     */
    private $orderLTL;
    /**
     * @var string $references
     */
    private $references;
    /**
     * @var array $stockInItemsAttachments 散板订单佐证材料
     */
    private $stockInItemsAttachments;
    /**
     * @var array $pickUpAttachments 取货佐证材料
     */
    private $pickUpAttachments;
    /**
     * @var array $stockInAttachments 入库佐证材料
     */
    private $stockInAttachments;
    /**
     * @var int $pickupDate 取货日期
     */
    private $pickupDate;
    /**
     * @var int $deliveryDate 送达日期
     */
    private $deliveryDate;

    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->carType = new CarType();
        $this->staff = new Staff();
        $this->number = '';
        $this->totalWeight = 0;
        $this->totalPalletNumber = 0;
        $this->totalOrderNumber = 0;
        $this->orderLTL = array();
        $this->references = '';
        $this->stockInItemsAttachments = array();
        $this->pickUpAttachments = array();
        $this->stockInAttachments = array();
        $this->pickupDate = 0;
        $this->deliveryDate = 0;
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new RecOrderLTLOrderRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->carType);
        unset($this->staff);
        unset($this->number);
        unset($this->totalWeight);
        unset($this->totalPalletNumber);
        unset($this->totalOrderNumber);
        unset($this->orderLTL);
        unset($this->references);
        unset($this->stockInItemsAttachments);
        unset($this->pickUpAttachments);
        unset($this->stockInAttachments);
        unset($this->pickupDate);
        unset($this->deliveryDate);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id) : void
    {
        $this->id = $id;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setCarType(CarType $carType) : void
    {
        $this->carType = $carType;
    }

    public function getCarType() : CarType
    {
        return $this->carType;
    }

    public function setStaff(Staff $staff) : void
    {
        $this->staff = $staff;
    }

    public function getStaff() : Staff
    {
        return $this->staff;
    }

    public function setNumber(string $number) : void
    {
        $this->number = $number;
    }

    public function getNumber() : string
    {
        return $this->number;
    }

    public function setTotalWeight(float $totalWeight) : void
    {
        $this->totalWeight = $totalWeight;
    }

    public function getTotalWeight() : float
    {
        return $this->totalWeight;
    }

    public function setTotalPalletNumber(int $totalPalletNumber) : void
    {
        $this->totalPalletNumber = $totalPalletNumber;
    }

    public function getTotalPalletNumber() : int
    {
        return $this->totalPalletNumber;
    }

    public function setTotalOrderNumber(int $totalOrderNumber) : void
    {
        $this->totalOrderNumber = $totalOrderNumber;
    }

    public function getTotalOrderNumber() : int
    {
        return $this->totalOrderNumber;
    }

    public function setOrderLTL(array $orderLTL) : void
    {
        $this->orderLTL = $orderLTL;
    }

    public function getOrderLTL() : array
    {
        return $this->orderLTL;
    }

    public function setReferences(string $references) : void
    {
        $this->references = $references;
    }

    public function getReferences() : string
    {
        return $this->references;
    }

    public function setStockInItemsAttachments(array $stockInItemsAttachments) : void
    {
        $this->stockInItemsAttachments = $stockInItemsAttachments;
    }

    public function getStockInItemsAttachments() : array
    {
        return $this->stockInItemsAttachments;
    }

    public function setPickUpAttachments(array $pickUpAttachments) : void
    {
        $this->pickUpAttachments = $pickUpAttachments;
    }

    public function getPickUpAttachments() : array
    {
        return $this->pickUpAttachments;
    }

    public function setStockInAttachments(array $stockInAttachments) : void
    {
        $this->stockInAttachments = $stockInAttachments;
    }

    public function getStockInAttachments() : array
    {
        return $this->stockInAttachments;
    }

    public function setPickupDate(int $pickupDate) : void
    {
        $this->pickupDate = $pickupDate;
    }

    public function getPickupDate() : int
    {
        return $this->pickupDate;
    }

    public function setDeliveryDate(int $deliveryDate) : void
    {
        $this->deliveryDate = $deliveryDate;
    }

    public function getDeliveryDate() : int
    {
        return $this->deliveryDate;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : RecOrderLTLOrderRepository
    {
        return $this->repository;
    }

    public function registerPickupDate() : bool
    {
        return $this->getRepository()->registerPickupDate($this);
    }

    public function pickup() : bool
    {
        return $this->getRepository()->pickup($this);
    }

    public function stockIn() : bool
    {
        return $this->getRepository()->stockIn($this);
    }
}
