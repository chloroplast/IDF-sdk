<?php
namespace Sdk\Order\Adapter\DisAmaFTLOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\DisAmaFTLOrder;

class DisAmaFTLOrderMockAdapter implements IDisAmaFTLOrderAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new DisAmaFTLOrder($id);
    }

    public function updateISA(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        unset($disAmaFTLOrder);
        return true;
    }

    public function registerDispatchDate(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        unset($disAmaFTLOrder);
        return true;
    }

    public function dispatch(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        unset($disAmaFTLOrder);
        return true;
    }

    public function accept(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        unset($disAmaFTLOrder);
        return true;
    }
}
