<?php
namespace Sdk\Order\Adapter\DisAmaFTLOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\DisAmaFTLOrder;
use Sdk\Order\Model\NullDisAmaFTLOrder;
use Sdk\Order\Translator\DisAmaFTLOrderRestfulTranslator;

class DisAmaFTLOrderRestfulAdapter extends CommonRestfulAdapter implements IDisAmaFTLOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'attachments' => ATTACHMENTS_FORMAT_INCORRECT,//佐证材料格式错误
            'references' => REFERENCES_FORMAT_INCORRECT,//references格式错误
            'isaNumber' => ISA_NUMBER_FORMAT_INCORRECT,//isa编号格式错误
            'pickupDate' => PICKUP_DATE_FORMAT_INCORRECT,//取货日期格式错误
            'deliveryDate' => DELIVERY_DATE_FORMAT_INCORRECT,//送达日期格式错误
            'closePage' => CLOSE_PAGE_FORMAT_INCORRECT,//亚马逊凭证格式错误
        ),
        100002 => array(
            'dispatchOrder' => DISPATCHORDER_CAN_NOT_MODIFY,//发货订单不能操作(状态不对)|403
            'dispatchAmazonFTLOrder' => DISPATCHAMAZONORDER_CAN_NOT_MODIFY,//发货组单订单不能操作(状态不对)|403
            'amazonFTL' => AMAZONFTL_CAN_NOT_MODIFY,//整板订单不能操作(订单状态不对)|403
        ),
        100004 => array(
            'staff' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在
        ),
        100005 => RESOURCE_NOT_EXISTS,//主体不存在
        100400 => array(
            'amazonFTL' => AMAZONFTL_HAVE_BEEN_FROZEN,//整板订单被冻结|403
        ),
        100407 => ORDER_INCOMPLETE_DELIVERY_TIME_RECORD,//订单未完成发货时间记录|403
        100408 => array(
            'dispatchAmazonFTLOrder' => ORDER_NOT_UPDATED_ISA,//订单未更新ISA|403
            'amazonFTL' => AMAZONLTL_NOT_UPDATED_ISA,//散板订单未更新ISA|403
        ),
    );
    
    const SCENARIOS = [
        'DIS_AMAFTL_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'DIS_AMAFTL_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new DisAmaFTLOrderRestfulTranslator(),
            'dispatchOrders/amazonFTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullDisAmaFTLOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function updateISA(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray($disAmaFTLOrder, array('isaNumber', 'staff'));
       
        $this->patch(
            $this->getResource().'/'.$disAmaFTLOrder->getId().'/isa',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disAmaFTLOrder);
            return true;
        }

        return false;
    }

    public function registerDispatchDate(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $disAmaFTLOrder,
            array('pickupDate', 'deliveryDate', 'references', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$disAmaFTLOrder->getId().'/registerDispatchDate',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disAmaFTLOrder);
            return true;
        }

        return false;
    }

    public function dispatch(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $disAmaFTLOrder,
            array('dispatchItemsAttachments', 'dispatchAttachments', 'staff')
        );

        $this->patch(
            $this->getResource().'/'.$disAmaFTLOrder->getId().'/dispatch',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disAmaFTLOrder);
            return true;
        }

        return false;
    }

    public function accept(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $disAmaFTLOrder,
            array('acceptItemsAttachments', 'acceptAttachments', 'closePage', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$disAmaFTLOrder->getId().'/accept',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disAmaFTLOrder);
            return true;
        }

        return false;
    }
}
