<?php
namespace Sdk\Order\Adapter\DisAmaFTLOrder;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;

use Sdk\Order\Model\DisAmaFTLOrder;

interface IDisAmaFTLOrderAdapter extends IFetchAbleAdapter
{
    public function updateISA(DisAmaFTLOrder $disAmaFTLOrder) : bool;
    
    public function registerDispatchDate(DisAmaFTLOrder $disAmaFTLOrder) : bool;

    public function dispatch(DisAmaFTLOrder $disAmaFTLOrder) : bool;

    public function accept(DisAmaFTLOrder $disAmaFTLOrder) : bool;
}
