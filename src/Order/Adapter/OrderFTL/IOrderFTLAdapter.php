<?php
namespace Sdk\Order\Adapter\OrderFTL;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Order\Model\OrderFTL;
use Sdk\Application\ParseTask\Model\ParseTask;

interface IOrderFTLAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function memberCancel(OrderFTL $orderFTL) : bool;
    
    public function staffCancel(OrderFTL $orderFTL) : bool;

    public function confirm(OrderFTL $orderFTL) : bool;

    public function batchConfirm(array $orderFTLList) : bool;

    public function freeze(OrderFTL $orderFTL) : bool;

    public function unFreeze(OrderFTL $orderFTL) : bool;
    
    public function parse(ParseTask $parseTask) : bool;
    
    public function calculate(OrderFTL $orderFTL) : array;
    
    public function fetchOneCalculateRecord($id);
}
