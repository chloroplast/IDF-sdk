<?php
namespace Sdk\Order\Adapter\OrderFTL;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Order\Model\OrderFTL;
use Sdk\Order\Model\OrderPriceReport;
use Sdk\Application\ParseTask\Model\ParseTask;

class OrderFTLMockAdapter implements IOrderFTLAdapter
{
    use FetchAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new OrderFTL($id);
    }

    public function memberCancel(OrderFTL $orderFTL) : bool
    {
        unset($orderFTL);
        return true;
    }

    public function staffCancel(OrderFTL $orderFTL) : bool
    {
        unset($orderFTL);
        return true;
    }

    public function confirm(OrderFTL $orderFTL) : bool
    {
        unset($orderFTL);
        return true;
    }

    public function batchConfirm(array $orderFTLList) : bool
    {
        unset($orderFTLList);
        return true;
    }

    public function freeze(OrderFTL $orderFTL) : bool
    {
        unset($orderFTL);
        return true;
    }

    public function unFreeze(OrderFTL $orderFTL) : bool
    {
        unset($orderFTL);
        return true;
    }

    public function parse(ParseTask $parseTask) : bool
    {
        unset($parseTask);
        return true;
    }

    public function calculate(OrderFTL $orderFTL) : array
    {
        unset($orderFTL);
        return array();
    }

    public function fetchOneCalculateRecord($id)
    {
        return new OrderPriceReport($id);
    }
}
