<?php
namespace Sdk\Order\Adapter\DisShipOrderLTLOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\DisShipOrderLTLOrder;

class DisShipOrderLTLOrderMockAdapter implements IDisShipOrderLTLOrderAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new DisShipOrderLTLOrder($id);
    }
}
