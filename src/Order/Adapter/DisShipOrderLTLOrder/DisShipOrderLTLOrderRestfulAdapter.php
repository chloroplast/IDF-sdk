<?php
namespace Sdk\Order\Adapter\DisShipOrderLTLOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\DisShipOrderLTLOrder;
use Sdk\Order\Model\NullDisShipOrderLTLOrder;
use Sdk\Order\Translator\DisShipOrderLTLOrderRestfulTranslator;

class DisShipOrderLTLOrderRestfulAdapter extends CommonRestfulAdapter implements IDisShipOrderLTLOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array();
    
    const SCENARIOS = [
        'DIS_SHIP_ORDERLTL_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'DIS_SHIP_ORDERLTL_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new DisShipOrderLTLOrderRestfulTranslator(),
            'dispatchShippingOrders/orderLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullDisShipOrderLTLOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
