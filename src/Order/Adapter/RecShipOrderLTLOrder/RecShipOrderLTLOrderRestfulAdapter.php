<?php
namespace Sdk\Order\Adapter\RecShipOrderLTLOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\RecShipOrderLTLOrder;
use Sdk\Order\Model\NullRecShipOrderLTLOrder;
use Sdk\Order\Translator\RecShipOrderLTLOrderRestfulTranslator;

class RecShipOrderLTLOrderRestfulAdapter extends CommonRestfulAdapter implements IRecShipOrderLTLOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array();
    
    const SCENARIOS = [
        'REC_SHIP_ORDERLTL_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'REC_SHIP_ORDERLTL_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new RecShipOrderLTLOrderRestfulTranslator(),
            'receiveShippingOrders/orderLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullRecShipOrderLTLOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
