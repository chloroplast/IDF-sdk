<?php
namespace Sdk\Order\Adapter\RecShipOrderLTLOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\RecShipOrderLTLOrder;

class RecShipOrderLTLOrderMockAdapter implements IRecShipOrderLTLOrderAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new RecShipOrderLTLOrder($id);
    }
}
