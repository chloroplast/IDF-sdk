<?php
namespace Sdk\Order\Adapter\AmazonFTL;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Order\Model\AmazonFTL;
use Sdk\Application\ParseTask\Model\ParseTask;

interface IAmazonFTLAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function memberCancel(AmazonFTL $amazonFTL) : bool;
    
    public function staffCancel(AmazonFTL $amazonFTL) : bool;

    public function confirm(AmazonFTL $amazonFTL) : bool;

    public function batchConfirm(array $amazonFTLList) : bool;

    public function freeze(AmazonFTL $amazonFTL) : bool;

    public function unFreeze(AmazonFTL $amazonFTL) : bool;
    
    public function parse(ParseTask $parseTask) : bool;
    
    public function calculate(AmazonFTL $amazonFTL) : array;
    
    public function fetchOneCalculateRecord($id);
}
