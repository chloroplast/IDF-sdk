<?php
namespace Sdk\Order\Adapter\AmazonFTL;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Order\Model\AmazonFTL;
use Sdk\Order\Model\OrderPriceReport;
use Sdk\Application\ParseTask\Model\ParseTask;

class AmazonFTLMockAdapter implements IAmazonFTLAdapter
{
    use FetchAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new AmazonFTL($id);
    }

    public function memberCancel(AmazonFTL $amazonFTL) : bool
    {
        unset($amazonFTL);
        return true;
    }

    public function staffCancel(AmazonFTL $amazonFTL) : bool
    {
        unset($amazonFTL);
        return true;
    }

    public function confirm(AmazonFTL $amazonFTL) : bool
    {
        unset($amazonFTL);
        return true;
    }

    public function batchConfirm(array $amazonFTLList) : bool
    {
        unset($amazonFTLList);
        return true;
    }

    public function freeze(AmazonFTL $amazonFTL) : bool
    {
        unset($amazonFTL);
        return true;
    }

    public function unFreeze(AmazonFTL $amazonFTL) : bool
    {
        unset($amazonFTL);
        return true;
    }
    
    public function parse(ParseTask $parseTask) : bool
    {
        unset($parseTask);
        return true;
    }

    public function calculate(AmazonFTL $amazonFTL) : array
    {
        unset($amazonFTL);
        return array();
    }

    public function fetchOneCalculateRecord($id)
    {
        return new OrderPriceReport($id);
    }
}
