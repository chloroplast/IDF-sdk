<?php
namespace Sdk\Order\Adapter\MemberOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\MemberOrder;

//use Sdk\Order\Utils\MockObjectGenerate;

class MemberOrderMockAdapter implements IMemberOrderAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new MemberOrder($id);
       // return MockObjectGenerate::generateMemberOrder($id);
    }
}
