<?php
namespace Sdk\Order\Adapter\MemberOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\NullMemberOrder;
use Sdk\Order\Translator\MemberOrderRestfulTranslator;

class MemberOrderRestfulAdapter extends CommonRestfulAdapter implements IMemberOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array();
    
    const SCENARIOS = [
        'MEMBER_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'MEMBER_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new MemberOrderRestfulTranslator(),
            'memberOrders',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullMemberOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
