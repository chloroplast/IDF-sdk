<?php
namespace Sdk\Order\Adapter\DispatchShippingOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\DispatchShippingOrder;

//use Sdk\Order\Utils\MockObjectGenerate;

class DispatchShippingOrderMockAdapter implements IDispatchShippingOrderAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new DispatchShippingOrder($id);
       // return MockObjectGenerate::generateDispatchShippingOrder($id);
    }
}
