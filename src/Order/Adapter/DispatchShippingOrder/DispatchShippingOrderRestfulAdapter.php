<?php
namespace Sdk\Order\Adapter\DispatchShippingOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\DispatchShippingOrder;
use Sdk\Order\Model\NullDispatchShippingOrder;
use Sdk\Order\Translator\DispatchShippingOrderRestfulTranslator;

class DispatchShippingOrderRestfulAdapter extends CommonRestfulAdapter implements IDispatchShippingOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array();
    
    const SCENARIOS = [
        'DISPATCH_SHIPPING_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'DISPATCH_SHIPPING_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new DispatchShippingOrderRestfulTranslator(),
            'dispatchShippingOrders/amazonLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullDispatchShippingOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
