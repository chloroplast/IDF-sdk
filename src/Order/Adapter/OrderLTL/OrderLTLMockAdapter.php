<?php
namespace Sdk\Order\Adapter\OrderLTL;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\ItemLTL;
use Sdk\Order\Model\OrderPriceReport;
use Sdk\Application\ParseTask\Model\ParseTask;

class OrderLTLMockAdapter implements IOrderLTLAdapter
{
    use FetchAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new OrderLTL($id);
    }

    public function memberCancel(OrderLTL $orderLTL) : bool
    {
        unset($orderLTL);
        return true;
    }

    public function staffCancel(OrderLTL $orderLTL) : bool
    {
        unset($orderLTL);
        return true;
    }

    public function confirm(OrderLTL $orderLTL) : bool
    {
        unset($orderLTL);
        return true;
    }

    public function batchConfirm(array $orderLTLList) : bool
    {
        unset($orderLTLList);
        return true;
    }

    public function registerPosition(OrderLTL $orderLTL) : bool
    {
        unset($orderLTL);
        return true;
    }

    public function freeze(OrderLTL $orderLTL) : bool
    {
        unset($orderLTL);
        return true;
    }

    public function unFreeze(OrderLTL $orderLTL) : bool
    {
        unset($orderLTL);
        return true;
    }

    public function parse(ParseTask $parseTask) : bool
    {
        unset($parseTask);
        return true;
    }

    public function calculate(OrderLTL $orderLTL) : array
    {
        unset($orderLTL);
        return array();
    }

    public function fetchOneCalculateRecord($id)
    {
        return new OrderPriceReport($id);
    }

    public function calculateGoodsLevel(ItemLTL $itemLTL) : bool
    {
        unset($itemLTL);
        return true;
    }
}
