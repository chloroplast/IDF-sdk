<?php
namespace Sdk\Order\Adapter\OrderLTL;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\ItemLTL;
use Sdk\Application\ParseTask\Model\ParseTask;

interface IOrderLTLAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function memberCancel(OrderLTL $orderLTL) : bool;
    
    public function staffCancel(OrderLTL $orderLTL) : bool;

    public function confirm(OrderLTL $orderLTL) : bool;

    public function batchConfirm(array $orderLTLList) : bool;
    
    public function registerPosition(OrderLTL $orderLTL) : bool;

    public function freeze(OrderLTL $orderLTL) : bool;

    public function unFreeze(OrderLTL $orderLTL) : bool;
    
    public function parse(ParseTask $parseTask) : bool;
    
    public function calculate(OrderLTL $orderLTL) : array;

    public function fetchOneCalculateRecord($id);

    public function calculateGoodsLevel(ItemLTL $itemLTL) : bool;
}
