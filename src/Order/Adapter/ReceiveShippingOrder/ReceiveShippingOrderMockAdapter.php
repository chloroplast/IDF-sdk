<?php
namespace Sdk\Order\Adapter\ReceiveShippingOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\ReceiveShippingOrder;

//use Sdk\Order\Utils\MockObjectGenerate;

class ReceiveShippingOrderMockAdapter implements IReceiveShippingOrderAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new ReceiveShippingOrder($id);
       // return MockObjectGenerate::generateReceiveShippingOrder($id);
    }
}
