<?php
namespace Sdk\Order\Adapter\ReceiveShippingOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\ReceiveShippingOrder;
use Sdk\Order\Model\NullReceiveShippingOrder;
use Sdk\Order\Translator\ReceiveShippingOrderRestfulTranslator;

class ReceiveShippingOrderRestfulAdapter extends CommonRestfulAdapter implements IReceiveShippingOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array();
    
    const SCENARIOS = [
        'RECEIVE_SHIPPING_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'RECEIVE_SHIPPING_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new ReceiveShippingOrderRestfulTranslator(),
            'receiveShippingOrders/amazonLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullReceiveShippingOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
