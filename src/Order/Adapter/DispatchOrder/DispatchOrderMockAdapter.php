<?php
namespace Sdk\Order\Adapter\DispatchOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Order\Model\DispatchOrder;

//use Sdk\Order\Utils\MockObjectGenerate;

class DispatchOrderMockAdapter implements IDispatchOrderAdapter
{
    use FetchAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new DispatchOrder($id);
       // return MockObjectGenerate::generateDispatchOrder($id);
    }

    public function updateISA(DispatchOrder $dispatchOrder) : bool
    {
        unset($dispatchOrder);
        return true;
    }

    public function registerDispatchDate(DispatchOrder $dispatchOrder) : bool
    {
        unset($dispatchOrder);
        return true;
    }

    public function dispatch(DispatchOrder $dispatchOrder) : bool
    {
        unset($dispatchOrder);
        return true;
    }

    public function accept(DispatchOrder $dispatchOrder) : bool
    {
        unset($dispatchOrder);
        return true;
    }
}
