<?php
namespace Sdk\Order\Adapter\DispatchOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Order\Model\DispatchOrder;
use Sdk\Order\Model\NullDispatchOrder;
use Sdk\Order\Translator\DispatchOrderRestfulTranslator;

class DispatchOrderRestfulAdapter extends CommonRestfulAdapter implements IDispatchOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'attachments' => ATTACHMENTS_FORMAT_INCORRECT,//佐证材料格式错误
            'references' => REFERENCES_FORMAT_INCORRECT,//references格式错误
            'isaNumber' => ISA_NUMBER_FORMAT_INCORRECT,//isa编号格式错误
            'pickupDate' => PICKUP_DATE_FORMAT_INCORRECT,//取货日期格式错误
            'deliveryDate' => DELIVERY_DATE_FORMAT_INCORRECT,//送达日期格式错误
            'closePage' => CLOSE_PAGE_FORMAT_INCORRECT,//亚马逊凭证格式错误
        ),
        100002 => array(
            'dispatchOrder' => DISPATCHORDER_CAN_NOT_MODIFY,//发货订单不能操作(状态不对)|403
            'dispatchAmazonOrder' => DISPATCHAMAZONORDER_CAN_NOT_MODIFY,//发货组单订单不能操作(状态不对)|403
            'amazonLTL' => AMAZONLTL_CAN_NOT_MODIFY,//散板订单不能操作(订单状态不对)|403
        ),
        100004 => array(
            'staff' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在
            'carType' => CARTYPE_NOT_EXISTS,//车辆类型不存在|404
        ),
        100005 => RESOURCE_NOT_EXISTS,//主体不存在
        100400 => array(
            'amazonLTL' => AMAZONLTL_HAVE_BEEN_FROZEN,//散板订单被冻结|403
        ),
        // 100401 => array(
        //     'carType' => CARTYPE_PALLET_EXCEEDS_LIMIT,//车辆类型板数超过限制|403
        // ),
        // 100402 => array(
        //     'carType' => CARTYPE_WEIGHT_EXCEEDS_LIMIT,//车辆类型重量超过限制|403
        // ),
        100404 => array(
            'amazonLTL' => AMAZONLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS,//散板订单已被添加到其他组单内|403
        ),
        100407 => ORDER_INCOMPLETE_DELIVERY_TIME_RECORD,//订单未完成发货时间记录|403
        100408 => array(
            'dispatchAmazonOrder' => ORDER_NOT_UPDATED_ISA,//订单未更新ISA|403
            'amazonLTL' => AMAZONLTL_NOT_UPDATED_ISA,//散板订单未更新ISA|403
        ),
        100409 => array(
            'amazonLTL' => AMAZONLTL_CAN_NOT_REGISTER_POSITION,//散板订单未入库登记位置|403
        ),
    );
    
    const SCENARIOS = [
        'DISPATCH_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'DISPATCH_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new DispatchOrderRestfulTranslator(),
            'dispatchOrders/amazonLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullDispatchOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
    
    protected function insertTranslatorKeys() : array
    {
        return array(
            'carType',
            'staff',
            'amazonLTL'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'carType',
            'staff',
            'amazonLTL'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array();
    }

    protected function disableTranslatorKeys() : array
    {
        return array();
    }

    protected function deletedTranslatorKeys() : array
    {
        return array();
    }

    public function updateISA(DispatchOrder $dispatchOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray($dispatchOrder, array('isaNumber', 'staff'));
       
        $this->patch(
            $this->getResource().'/'.$dispatchOrder->getId().'/isa',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($dispatchOrder);
            return true;
        }

        return false;
    }

    public function registerDispatchDate(DispatchOrder $dispatchOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $dispatchOrder,
            array('pickupDate', 'deliveryDate', 'references', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$dispatchOrder->getId().'/registerDispatchDate',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($dispatchOrder);
            return true;
        }

        return false;
    }

    public function dispatch(DispatchOrder $dispatchOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $dispatchOrder,
            array('dispatchItemsAttachments', 'dispatchAttachments', 'staff')
        );

        $this->patch(
            $this->getResource().'/'.$dispatchOrder->getId().'/dispatch',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($dispatchOrder);
            return true;
        }

        return false;
    }

    public function accept(DispatchOrder $dispatchOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $dispatchOrder,
            array('acceptItemsAttachments', 'acceptAttachments', 'closePage', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$dispatchOrder->getId().'/accept',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($dispatchOrder);
            return true;
        }

        return false;
    }
}
