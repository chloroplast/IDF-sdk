<?php
namespace Sdk\Order\Adapter\DispatchOrder;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Order\Model\DispatchOrder;

interface IDispatchOrderAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function updateISA(DispatchOrder $dispatchOrder) : bool;
    
    public function registerDispatchDate(DispatchOrder $dispatchOrder) : bool;

    public function dispatch(DispatchOrder $dispatchOrder) : bool;

    public function accept(DispatchOrder $dispatchOrder) : bool;
}
