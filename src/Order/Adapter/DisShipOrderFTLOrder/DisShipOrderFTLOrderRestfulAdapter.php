<?php
namespace Sdk\Order\Adapter\DisShipOrderFTLOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\DisShipOrderFTLOrder;
use Sdk\Order\Model\NullDisShipOrderFTLOrder;
use Sdk\Order\Translator\DisShipOrderFTLOrderRestfulTranslator;

class DisShipOrderFTLOrderRestfulAdapter extends CommonRestfulAdapter implements IDisShipOrderFTLOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array();
    
    const SCENARIOS = [
        'DIS_SHIP_ORDERFTL_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'DIS_SHIP_ORDERFTL_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new DisShipOrderFTLOrderRestfulTranslator(),
            'dispatchShippingOrders/orderFTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullDisShipOrderFTLOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
