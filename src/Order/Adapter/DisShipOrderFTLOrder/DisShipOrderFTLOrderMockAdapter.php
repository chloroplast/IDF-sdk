<?php
namespace Sdk\Order\Adapter\DisShipOrderFTLOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\DisShipOrderFTLOrder;

class DisShipOrderFTLOrderMockAdapter implements IDisShipOrderFTLOrderAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new DisShipOrderFTLOrder($id);
    }
}
