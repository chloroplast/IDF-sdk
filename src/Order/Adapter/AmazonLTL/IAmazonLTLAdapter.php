<?php
namespace Sdk\Order\Adapter\AmazonLTL;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Order\Model\AmazonLTL;

use Sdk\Application\ParseTask\Model\ParseTask;

interface IAmazonLTLAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function memberCancel(AmazonLTL $amazonLTL) : bool;
    
    public function staffCancel(AmazonLTL $amazonLTL) : bool;

    public function confirm(AmazonLTL $amazonLTL) : bool;

    public function batchConfirm(array $amazonLTLList) : bool;

    public function registerPosition(AmazonLTL $amazonLTL) : bool;

    public function freeze(AmazonLTL $amazonLTL) : bool;

    public function unFreeze(AmazonLTL $amazonLTL) : bool;

    public function parse(ParseTask $parseTask) : bool;

    public function autoGroup(array $amazonLTLList) : array;
    
    public function calculate(AmazonLTL $amazonLTL) : array;
}
