<?php
namespace Sdk\Order\Adapter\AmazonLTL;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Order\Model\AmazonLTL;

use Sdk\Application\ParseTask\Model\ParseTask;

class AmazonLTLMockAdapter implements IAmazonLTLAdapter
{
    use FetchAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new AmazonLTL($id);
    }

    public function memberCancel(AmazonLTL $amazonLTL) : bool
    {
        unset($amazonLTL);
        return true;
    }

    public function staffCancel(AmazonLTL $amazonLTL) : bool
    {
        unset($amazonLTL);
        return true;
    }

    public function confirm(AmazonLTL $amazonLTL) : bool
    {
        unset($amazonLTL);
        return true;
    }

    public function batchConfirm(array $amazonLTLList) : bool
    {
        unset($amazonLTLList);
        return true;
    }

    public function registerPosition(AmazonLTL $amazonLTL) : bool
    {
        unset($amazonLTL);
        return true;
    }

    public function freeze(AmazonLTL $amazonLTL) : bool
    {
        unset($amazonLTL);
        return true;
    }

    public function unFreeze(AmazonLTL $amazonLTL) : bool
    {
        unset($amazonLTL);
        return true;
    }

    public function parse(ParseTask $parseTask) : bool
    {
        unset($parseTask);
        return true;
    }

    public function autoGroup(array $amazonLTLList) : array
    {
        unset($amazonLTL);
        return array();
    }

    public function calculate(AmazonLTL $amazonLTL) : array
    {
        unset($amazonLTL);
        return array();
    }
}
