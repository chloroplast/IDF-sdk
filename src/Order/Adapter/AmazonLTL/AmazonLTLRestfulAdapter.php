<?php
namespace Sdk\Order\Adapter\AmazonLTL;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;

use Sdk\Common\Model\Interfaces\IOperateAble;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\NullAmazonLTL;
use Sdk\Order\Translator\AmazonLTLRestfulTranslator;
use Sdk\Order\Translator\OrderPriceReportRestfulTranslator;

use Sdk\Application\ParseTask\Model\ParseTask;
use Sdk\Application\ParseTask\Model\NullParseTask;
use Sdk\Application\ParseTask\Translator\ParseTaskRestfulTranslator;

class AmazonLTLRestfulAdapter extends CommonRestfulAdapter implements IAmazonLTLAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'isaNumber' => ISA_NUMBER_FORMAT_INCORRECT,//isa格式错误
            'position' => POSITION_FORMAT_INCORRECT,//入库位置格式错误
            'idfPickup' => IDF_PICKUP_FORMAT_INCORRECT,//idfPickup格式错误
            'pickupDate' => PICKUP_DATE_FORMAT_INCORRECT,//取货日期格式错误
            'readyTime' => READY_TIME_FORMAT_INCORRECT,//取货时间格式错误
            'closeTime' => CLOSE_TIME_FORMAT_INCORRECT,//下班时间格式错误
            'pickupNumber' => PICKUP_NUMBER_FORMAT_INCORRECT,//取货码格式错误
            'remark' => REMARK_FORMAT_INCORRECT,//备注格式错误
            'items' => ITEMS_FORMAT_INCORRECT,//货物信息格式错误
            'stockInNumber' => STOCKIN_NUMBER_FORMAT_INCORRECT,//入库编号格式错误
            'stockInTime' => STOCKIN_TIME_FORMAT_INCORRECT,//入库时间格式错误
        ),
        100002 => array(
            'amazonLTL' => AMAZONLTL_CAN_NOT_MODIFY,//订单不能操作(订单状态不对)|403
        ),
        100004 => array(
            'staff' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在
            'member' => USER_IDENTITY_AUTHENTICATION_FAILED,//官网用户不存在
            'pickupWarehouse' => PICKUPWAREHOUSE_NOT_EXISTS,//取货仓库不存在|404
            'address' => ADDRESS_NOT_EXISTS,//地址不存在|404
            'items' => ITEMS_WAREHOUSE_NOT_EXISTS,//目标仓库不存在|404
            'amazonLTLPolicy' => AMAZONLTLPOLICY_NOT_EXISTS,//散板价格策略不存在|404
        ),
        100005 => RESOURCE_NOT_EXISTS,//主体不存在
        100400 => array(
            'amazonLTL' => AMAZONLTL_HAVE_BEEN_FROZEN,//订单被冻结|403
        ),
        100413 => array(
            'name' => FILE_PARSING_FAILED,//文件解析失败|403
        ),
        100414 => array(
            'name' => THE_NUMBER_OF_PARSES_EXCEEDS_THE_LIMIT,//解析数量超过限制(目前限制为100条)|403
        ),
        100415 => array(
            'name' => FILE_PARSING_RETURNED_NULL,//文件解析返回空|403
        ),
        100420 => array(
            'amazonLTLBasePriceStrategy' => AMAZONLTLBASEPRICESTRATEGY_NOT_FOUND,//亚马逊散板基础价格策略未匹配|404
            'dimensionPriceStrategy' => DIMENSIONPRICESTRATEGY_NOT_FOUND,//体积价格策略未匹配|404
            'gradePriceStrategy' => GRADEPRICESTRATEGY_NOT_FOUND,//vip等级价格策略未匹配|404
            'idfPickupPriceStrategy' => IDFPICKUPPRICESTRATEGY_NOT_FOUND,//idf取货价格策略未匹配|404
            'weightPriceStrategy' => WEIGHTPRICESTRATEGY_NOT_FOUND,//重量价格策略未匹配|404
            'maxPriceGroup' => MAXPRICEGROUP_NOT_FOUND,//货物属性(取高)策略未匹配|404
            'minPriceGroup' => MINPRICEGROUP_NOT_FOUND,//用户属性(取低)策略未匹配|404
            'sequenceGroup' => SEQUENCEGROUP_NOT_FOUND,//业务属性策略未匹配|404
        )
    );
    
    const SCENARIOS = [
        'AMAZONLTL_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'AMAZONLTL_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new AmazonLTLRestfulTranslator(),
            'amazonLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullAmazonLTL::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function insert(IOperateAble $operateAbleObject) : bool
    {
        $keys = $this->insertTranslatorKeys();
        $data = $this->getTranslator()->objectToArray($operateAbleObject, $keys);

        $this->post(
            'memberOrders/amazonLTL',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($operateAbleObject);
            return true;
        }

        return false;
    }

    public function update(IOperateAble $operateAbleObject) : bool
    {
        $keys = $this->updateTranslatorKeys();
        $data = $this->getTranslator()->objectToArray($operateAbleObject, $keys);

        $this->patch(
            'memberOrders/amazonLTL/'.$operateAbleObject->getMemberOrder()->getId(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($operateAbleObject);
            return true;
        }

        return false;
    }
    
    protected function insertTranslatorKeys() : array
    {
        return array(
            'pickupWarehouse',
            'address',
            'idfPickup',
            'items',
            'pickupDate',
            'readyTime',
            'closeTime',
            'pickupNumber',
            'remark',
            'member',
            'staff'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'pickupWarehouse',
            'address',
            'idfPickup',
            'items',
            'pickupDate',
            'readyTime',
            'closeTime',
            'pickupNumber',
            'remark',
            'member',
            'staff'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array();
    }

    protected function disableTranslatorKeys() : array
    {
        return array();
    }

    protected function deletedTranslatorKeys() : array
    {
        return array();
    }

    public function memberCancel(AmazonLTL $amazonLTL) : bool
    {
        $data = $this->getTranslator()->objectToArray($amazonLTL, array('member'));
        unset($data['data']['attributes']);

        $this->patch(
            $this->getResource().'/'.$amazonLTL->getId().'/cancel',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($amazonLTL);
            return true;
        }

        return false;
    }

    public function staffCancel(AmazonLTL $amazonLTL) : bool
    {
        $data = $this->getTranslator()->objectToArray($amazonLTL, array('staff'));
        unset($data['data']['attributes']);

        $this->patch(
            $this->getResource().'/'.$amazonLTL->getId().'/cancel',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($amazonLTL);
            return true;
        }

        return false;
    }

    public function confirm(AmazonLTL $amazonLTL) : bool
    {
        $data = $this->getTranslator()->objectToArray($amazonLTL, array('staff'));
        unset($data['data']['attributes']);
       
        $this->patch(
            $this->getResource().'/'.$amazonLTL->getId().'/confirm',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($amazonLTL);
            return true;
        }

        return false;
    }

    public function batchConfirm(array $amazonLTLList) : bool
    {
        foreach ($amazonLTLList as $amazonLTL) {
            $amazonLTLData[] = array(
                'type' => 'amazonLTL',
                'id' => strval($amazonLTL->getId())
            );

            $data = $this->getTranslator()->objectToArray($amazonLTL, array('staff'));
            unset($data['data']['attributes']);
        }

        $data['data']['relationships']['amazonLTL']['data'] = $amazonLTLData;

        $this->patch(
            $this->getResource().'/batchConfirm',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObjects();
            return true;
        }

        return false;
    }

    public function registerPosition(AmazonLTL $amazonLTL) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $amazonLTL,
            array('position', 'stockInNumber', 'stockInTime', 'staff')
        );
     
        $this->patch(
            $this->getResource().'/'.$amazonLTL->getId().'/register/position',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($amazonLTL);
            return true;
        }

        return false;
    }

    public function freeze(AmazonLTL $amazonLTL) : bool
    {
        $data = $this->getTranslator()->objectToArray($amazonLTL, array('staff'));
        unset($data['data']['attributes']);
       
        $this->patch(
            $this->getResource().'/'.$amazonLTL->getId().'/freeze',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($amazonLTL);
            return true;
        }

        return false;
    }

    public function unFreeze(AmazonLTL $amazonLTL) : bool
    {
        $data = $this->getTranslator()->objectToArray($amazonLTL, array('staff'));
        unset($data['data']['attributes']);
       
        $this->patch(
            $this->getResource().'/'.$amazonLTL->getId().'/unFreeze',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($amazonLTL);
            return true;
        }

        return false;
    }

    protected function getNullParseTask() : INull
    {
        return NullParseTask::getInstance();
    }

    protected function getParseTaskRestfulTranslator() : ParseTaskRestfulTranslator
    {
        return new ParseTaskRestfulTranslator();
    }

    protected function getOrderPriceReportRestfulTranslator() : OrderPriceReportRestfulTranslator
    {
        return new OrderPriceReportRestfulTranslator();
    }

    public function parse(ParseTask $parseTask) : bool
    {
        $data = $this->getParseTaskRestfulTranslator()->objectToArray($parseTask, array('fileName'));

        $this->post(
            $this->getResource().'/parse',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->getParseTaskRestfulTranslator()->arrayToObject($this->getContents(), $parseTask);
            return true;
        }

        return false;
    }

    public function autoGroup(array $amazonLTLList) : array
    {
        $amazonLTLData = array();
        foreach ($amazonLTLList as $amazonLTL) {
            if ($amazonLTL->getId()) {
                $amazonLTLData[] = array(
                    'type' => 'amazonLTL',
                    'id' => strval($amazonLTL->getId())
                );
            }
            
            $data = $this->getTranslator()->objectToArray($amazonLTL, array('filter', 'sort', 'carType'));
        }
        if ($amazonLTLData) {
            $data['data']['relationships']['amazonLTL']['data'] = $amazonLTLData;
        }

        $this->post(
            $this->getResource().'/autoGroup',
            $data
        );
        
        if ($this->isSuccess() && $this->getContents()['data']) {
            return $this->translateToObjects();
        }

        return array(0, array());
    }

    public function calculate(AmazonLTL $amazonLTL) : array
    {
        $keys = $this->updateTranslatorKeys();
        $data = $this->getTranslator()->objectToArray($amazonLTL, $keys);

        $this->post(
            'memberOrders/amazonLTL/calculate',
            $data
        );

        if ($this->isSuccess()) {
            return $this->getOrderPriceReportRestfulTranslator()->arrayToObjects($this->getContents());
        }

        return array(0, array());
    }
}
