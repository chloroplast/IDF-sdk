<?php
namespace Sdk\Order\Adapter\DisOrderFTLOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\DisOrderFTLOrder;

class DisOrderFTLOrderMockAdapter implements IDisOrderFTLOrderAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new DisOrderFTLOrder($id);
    }

    public function registerDispatchDate(DisOrderFTLOrder $disOrderFTLOrder) : bool
    {
        unset($disOrderFTLOrder);
        return true;
    }

    public function dispatch(DisOrderFTLOrder $disOrderFTLOrder) : bool
    {
        unset($disOrderFTLOrder);
        return true;
    }

    public function accept(DisOrderFTLOrder $disOrderFTLOrder) : bool
    {
        unset($disOrderFTLOrder);
        return true;
    }
}
