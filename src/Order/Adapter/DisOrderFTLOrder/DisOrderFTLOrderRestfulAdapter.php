<?php
namespace Sdk\Order\Adapter\DisOrderFTLOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\DisOrderFTLOrder;
use Sdk\Order\Model\NullDisOrderFTLOrder;
use Sdk\Order\Translator\DisOrderFTLOrderRestfulTranslator;

class DisOrderFTLOrderRestfulAdapter extends CommonRestfulAdapter implements IDisOrderFTLOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'attachments' => ATTACHMENTS_FORMAT_INCORRECT,//佐证材料格式错误
            'references' => REFERENCES_FORMAT_INCORRECT,//references格式错误
            'pickupDate' => PICKUP_DATE_FORMAT_INCORRECT,//取货日期格式错误
            'deliveryDate' => DELIVERY_DATE_FORMAT_INCORRECT,//送达日期格式错误
        ),
        100002 => array(
            'dispatchOrderFTLOrder' => DISPATCHORDER_CAN_NOT_MODIFY,//发货订单不能操作(状态不对)|403
            'orderFTL' => ORDERFTL_CAN_NOT_MODIFY,//整板订单不能操作(订单状态不对)|403
        ),
        100004 => array(
            'staff' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在
        ),
        100005 => RESOURCE_NOT_EXISTS,//主体不存在
        100400 => array(
            'orderFTL' => ORDERFTL_HAVE_BEEN_FROZEN,//整板订单被冻结|403
        ),
        100407 => ORDER_INCOMPLETE_DELIVERY_TIME_RECORD,//订单未完成发货时间记录|403
    );
    
    const SCENARIOS = [
        'DIS_ORDERFTL_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'DIS_ORDERFTL_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new DisOrderFTLOrderRestfulTranslator(),
            'dispatchOrders/orderFTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullDisOrderFTLOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function registerDispatchDate(DisOrderFTLOrder $disOrderFTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $disOrderFTLOrder,
            array('pickupDate', 'deliveryDate', 'references', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$disOrderFTLOrder->getId().'/registerDispatchDate',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disOrderFTLOrder);
            return true;
        }

        return false;
    }

    public function dispatch(DisOrderFTLOrder $disOrderFTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $disOrderFTLOrder,
            array('dispatchItemsAttachments', 'dispatchAttachments', 'staff')
        );

        $this->patch(
            $this->getResource().'/'.$disOrderFTLOrder->getId().'/dispatch',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disOrderFTLOrder);
            return true;
        }

        return false;
    }

    public function accept(DisOrderFTLOrder $disOrderFTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $disOrderFTLOrder,
            array('acceptItemsAttachments', 'acceptAttachments', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$disOrderFTLOrder->getId().'/accept',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disOrderFTLOrder);
            return true;
        }

        return false;
    }
}
