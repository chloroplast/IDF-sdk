<?php
namespace Sdk\Order\Adapter\DisOrderFTLOrder;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;

use Sdk\Order\Model\DisOrderFTLOrder;

interface IDisOrderFTLOrderAdapter extends IFetchAbleAdapter
{
    public function registerDispatchDate(DisOrderFTLOrder $disOrderFTLOrder) : bool;

    public function dispatch(DisOrderFTLOrder $disOrderFTLOrder) : bool;

    public function accept(DisOrderFTLOrder $disOrderFTLOrder) : bool;
}
