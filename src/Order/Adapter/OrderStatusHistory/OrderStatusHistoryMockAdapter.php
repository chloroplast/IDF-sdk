<?php
namespace Sdk\Order\Adapter\OrderStatusHistory;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\OrderStatusHistory;

class OrderStatusHistoryMockAdapter implements IOrderStatusHistoryAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new OrderStatusHistory($id);
    }
}
