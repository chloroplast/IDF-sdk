<?php
namespace Sdk\Order\Adapter\OrderStatusHistory;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\OrderStatusHistory;
use Sdk\Order\Model\NullOrderStatusHistory;
use Sdk\Order\Translator\OrderStatusHistoryRestfulTranslator;

class OrderStatusHistoryRestfulAdapter extends CommonRestfulAdapter implements IOrderStatusHistoryAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array();
    
    const SCENARIOS = [
        'ORDER_STATUS_HISTORY_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'ORDER_STATUS_HISTORY_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new OrderStatusHistoryRestfulTranslator(),
            'order/histories',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullOrderStatusHistory::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
