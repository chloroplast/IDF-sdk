<?php
namespace Sdk\Order\Adapter\ReceiveOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Order\Model\ReceiveOrder;
use Sdk\Order\Model\NullReceiveOrder;
use Sdk\Order\Translator\ReceiveOrderRestfulTranslator;

class ReceiveOrderRestfulAdapter extends CommonRestfulAdapter implements IReceiveOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'attachments' => ATTACHMENTS_FORMAT_INCORRECT,//佐证材料格式错误
            'references' => REFERENCES_FORMAT_INCORRECT,//references格式错误
            'pickupDate' => PICKUP_DATE_FORMAT_INCORRECT,//取货日期格式错误
            'deliveryDate' => DELIVERY_DATE_FORMAT_INCORRECT,//送达日期格式错误
        ),
        100002 => array(
            'receiveAmazonOrder' => RECEIVEAMAZONORDER_CAN_NOT_MODIFY,//取货订单不能操作(状态不对)|403
            'amazonLTL' => AMAZONLTL_CAN_NOT_MODIFY,//散板订单不能操作(订单状态不对)|403
        ),
        100004 => array(
            'staff' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在
            'carType' => CARTYPE_NOT_EXISTS,//车辆类型不存在|404
        ),
        100005 => RESOURCE_NOT_EXISTS,//主体不存在
        // 100401 => array(
        //     'carType' => CARTYPE_PALLET_EXCEEDS_LIMIT,//车辆类型板数超过限制|403
        // ),
        // 100402 => array(
        //     'carType' => CARTYPE_WEIGHT_EXCEEDS_LIMIT,//车辆类型重量超过限制|403
        // ),
        100400 => array(
            'amazonLTL' => AMAZONLTL_HAVE_BEEN_FROZEN,//散板订单被冻结|403
        ),
        100404 => array(
            'amazonLTL' => AMAZONLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS,//散板订单已被添加到其他组单内|403
        ),
        100406 => RECORD_OF_ORDER_INCOMPLETE_PICKUP_TIME,//订单未完成取货时间记录|403
    );
    
    const SCENARIOS = [
        'RECEIVE_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'RECEIVE_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new ReceiveOrderRestfulTranslator(),
            'receiveOrders/amazonLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullReceiveOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
    
    protected function insertTranslatorKeys() : array
    {
        return array(
            'carType',
            'staff',
            'amazonLTL'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'carType',
            'staff',
            'amazonLTL'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array();
    }

    protected function disableTranslatorKeys() : array
    {
        return array();
    }

    protected function deletedTranslatorKeys() : array
    {
        return array();
    }

    public function registerPickupDate(ReceiveOrder $receiveOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $receiveOrder,
            array('pickupDate', 'deliveryDate', 'references', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$receiveOrder->getId().'/registerPickupDate',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($receiveOrder);
            return true;
        }

        return false;
    }

    public function pickup(ReceiveOrder $receiveOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $receiveOrder,
            array('pickUpAttachments', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$receiveOrder->getId().'/pickup',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($receiveOrder);
            return true;
        }

        return false;
    }

    public function stockIn(ReceiveOrder $receiveOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $receiveOrder,
            array('stockInItemsAttachments', 'stockInAttachments', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$receiveOrder->getId().'/stockIn',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($receiveOrder);
            return true;
        }

        return false;
    }
}
