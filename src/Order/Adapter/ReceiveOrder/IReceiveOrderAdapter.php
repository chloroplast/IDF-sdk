<?php
namespace Sdk\Order\Adapter\ReceiveOrder;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Order\Model\ReceiveOrder;

interface IReceiveOrderAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function registerPickupDate(ReceiveOrder $receiveOrder) : bool;
    
    public function pickup(ReceiveOrder $receiveOrder) : bool;

    public function stockIn(ReceiveOrder $receiveOrder) : bool;
}
