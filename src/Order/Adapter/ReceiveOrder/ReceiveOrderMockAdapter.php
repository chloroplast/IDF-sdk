<?php
namespace Sdk\Order\Adapter\ReceiveOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Order\Model\ReceiveOrder;

//use Sdk\Order\Utils\MockObjectGenerate;

class ReceiveOrderMockAdapter implements IReceiveOrderAdapter
{
    use FetchAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new ReceiveOrder($id);
       // return MockObjectGenerate::generateReceiveOrder($id);
    }

    public function registerPickupDate(ReceiveOrder $receiveOrder) : bool
    {
        unset($receiveOrder);
        return true;
    }

    public function pickup(ReceiveOrder $receiveOrder) : bool
    {
        unset($receiveOrder);
        return true;
    }

    public function stockIn(ReceiveOrder $receiveOrder) : bool
    {
        unset($receiveOrder);
        return true;
    }
}
