<?php
namespace Sdk\Order\Adapter\DisShipAmaFTLOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Order\Model\DisShipAmaFTLOrder;

class DisShipAmaFTLOrderMockAdapter implements IDisShipAmaFTLOrderAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new DisShipAmaFTLOrder($id);
    }
}
