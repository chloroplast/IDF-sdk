<?php
namespace Sdk\Order\Adapter\DisShipAmaFTLOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Order\Model\DisShipAmaFTLOrder;
use Sdk\Order\Model\NullDisShipAmaFTLOrder;
use Sdk\Order\Translator\DisShipAmaFTLOrderRestfulTranslator;

class DisShipAmaFTLOrderRestfulAdapter extends CommonRestfulAdapter implements IDisShipAmaFTLOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array();
    
    const SCENARIOS = [
        'DIS_SHIP_AMAFTL_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'DIS_SHIP_AMAFTL_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new DisShipAmaFTLOrderRestfulTranslator(),
            'dispatchShippingOrders/amazonFTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullDisShipAmaFTLOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
