<?php
namespace Sdk\Order\Adapter\RecOrderLTLOrder;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Order\Model\RecOrderLTLOrder;

interface IRecOrderLTLOrderAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function registerPickupDate(RecOrderLTLOrder $recOrderLTLOrder) : bool;
    
    public function pickup(RecOrderLTLOrder $recOrderLTLOrder) : bool;

    public function stockIn(RecOrderLTLOrder $recOrderLTLOrder) : bool;
}
