<?php
namespace Sdk\Order\Adapter\RecOrderLTLOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Order\Model\RecOrderLTLOrder;

class RecOrderLTLOrderMockAdapter implements IRecOrderLTLOrderAdapter
{
    use FetchAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new RecOrderLTLOrder($id);
    }

    public function registerPickupDate(RecOrderLTLOrder $recOrderLTLOrder) : bool
    {
        unset($recOrderLTLOrder);
        return true;
    }

    public function pickup(RecOrderLTLOrder $recOrderLTLOrder) : bool
    {
        unset($recOrderLTLOrder);
        return true;
    }

    public function stockIn(RecOrderLTLOrder $recOrderLTLOrder) : bool
    {
        unset($recOrderLTLOrder);
        return true;
    }
}
