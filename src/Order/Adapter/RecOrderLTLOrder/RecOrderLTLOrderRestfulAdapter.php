<?php
namespace Sdk\Order\Adapter\RecOrderLTLOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Order\Model\RecOrderLTLOrder;
use Sdk\Order\Model\NullRecOrderLTLOrder;
use Sdk\Order\Translator\RecOrderLTLOrderRestfulTranslator;

class RecOrderLTLOrderRestfulAdapter extends CommonRestfulAdapter implements IRecOrderLTLOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'attachments' => ATTACHMENTS_FORMAT_INCORRECT,//佐证材料格式错误
            'references' => REFERENCES_FORMAT_INCORRECT,//references格式错误
            'pickupDate' => PICKUP_DATE_FORMAT_INCORRECT,//取货日期格式错误
            'deliveryDate' => DELIVERY_DATE_FORMAT_INCORRECT,//送达日期格式错误
        ),
        100002 => array(
            'receiveOrderLTLOrder' => RECEIVEORDERLTLORDER_CAN_NOT_MODIFY,//取货订单不能操作(状态不对)|403
            'orderLTL' => ORDERLTL_CAN_NOT_MODIFY,//散板订单不能操作(订单状态不对)|403
        ),
        100004 => array(
            'staff' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在
            'carType' => CARTYPE_NOT_EXISTS,//车辆类型不存在|404
        ),
        100005 => RESOURCE_NOT_EXISTS,//主体不存在
        100400 => array(
            'orderLTL' => ORDERLTL_HAVE_BEEN_FROZEN,//散板订单被冻结|403
        ),
        100404 => array(
            'orderLTL' => ORDERLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS,//散板订单已被添加到其他组单内|403
        ),
        100406 => RECORD_OF_ORDER_INCOMPLETE_PICKUP_TIME,//订单未完成取货时间记录|403
    );
    
    const SCENARIOS = [
        'REC_ORDERLTL_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'REC_ORDERLTL_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new RecOrderLTLOrderRestfulTranslator(),
            'receiveOrders/orderLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullRecOrderLTLOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
    
    protected function insertTranslatorKeys() : array
    {
        return array(
            'carType',
            'staff',
            'orderLTL'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'carType',
            'staff',
            'orderLTL'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array();
    }

    protected function disableTranslatorKeys() : array
    {
        return array();
    }

    protected function deletedTranslatorKeys() : array
    {
        return array();
    }

    public function registerPickupDate(RecOrderLTLOrder $recOrderLTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $recOrderLTLOrder,
            array('pickupDate', 'deliveryDate', 'references', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$recOrderLTLOrder->getId().'/registerPickupDate',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($recOrderLTLOrder);
            return true;
        }

        return false;
    }

    public function pickup(RecOrderLTLOrder $recOrderLTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $recOrderLTLOrder,
            array('pickUpAttachments', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$recOrderLTLOrder->getId().'/pickup',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($recOrderLTLOrder);
            return true;
        }

        return false;
    }

    public function stockIn(RecOrderLTLOrder $recOrderLTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $recOrderLTLOrder,
            array('stockInItemsAttachments', 'stockInAttachments', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$recOrderLTLOrder->getId().'/stockIn',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($recOrderLTLOrder);
            return true;
        }

        return false;
    }
}
