<?php
namespace Sdk\Order\Adapter\DisOrderLTLOrder;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\Order\Model\DisOrderLTLOrder;
use Sdk\Order\Model\NullDisOrderLTLOrder;
use Sdk\Order\Translator\DisOrderLTLOrderRestfulTranslator;

class DisOrderLTLOrderRestfulAdapter extends CommonRestfulAdapter implements IDisOrderLTLOrderAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'attachments' => ATTACHMENTS_FORMAT_INCORRECT,//佐证材料格式错误
            'references' => REFERENCES_FORMAT_INCORRECT,//references格式错误
            'pickupDate' => PICKUP_DATE_FORMAT_INCORRECT,//取货日期格式错误
            'deliveryDate' => DELIVERY_DATE_FORMAT_INCORRECT,//送达日期格式错误
        ),
        100002 => array(
            'dispatchOrderLTLOrder' => DISPATCHORDERLTLORDER_CAN_NOT_MODIFY,//发货订单不能操作(状态不对)|403
            'orderLTL' => ORDERLTL_CAN_NOT_MODIFY,//散板订单不能操作(订单状态不对)|403
        ),
        100004 => array(
            'staff' => USER_IDENTITY_AUTHENTICATION_FAILED,//后台用户不存在
            'carType' => CARTYPE_NOT_EXISTS,//车辆类型不存在|404
        ),
        100005 => RESOURCE_NOT_EXISTS,//主体不存在
        100400 => array(
            'orderLTL' => ORDERLTL_HAVE_BEEN_FROZEN,//散板订单被冻结|403
        ),
        100407 => ORDER_INCOMPLETE_DELIVERY_TIME_RECORD,//订单未完成发货时间记录|403
        100404 => array(
            'orderLTL' => ORDERLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS,//散板订单已被添加到其他组单内|403
        ),
        100409 => array(
            'orderLTL' => ORDERLTL_CAN_NOT_REGISTER_POSITION,//散板订单未入库登记位置|403
        ),
    );
    
    const SCENARIOS = [
        'DIS_ORDERLTL_ORDER_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'DIS_ORDERLTL_ORDER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new DisOrderLTLOrderRestfulTranslator(),
            'dispatchOrders/orderLTL',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullDisOrderLTLOrder::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
    
    protected function insertTranslatorKeys() : array
    {
        return array(
            'carType',
            'staff',
            'orderLTL'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'carType',
            'staff',
            'orderLTL'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array();
    }

    protected function disableTranslatorKeys() : array
    {
        return array();
    }

    protected function deletedTranslatorKeys() : array
    {
        return array();
    }

    public function registerDispatchDate(DisOrderLTLOrder $disOrderLTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $disOrderLTLOrder,
            array('pickupDate', 'deliveryDate', 'references', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$disOrderLTLOrder->getId().'/registerDispatchDate',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disOrderLTLOrder);
            return true;
        }

        return false;
    }

    public function dispatch(DisOrderLTLOrder $disOrderLTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $disOrderLTLOrder,
            array('dispatchItemsAttachments', 'dispatchAttachments', 'staff')
        );

        $this->patch(
            $this->getResource().'/'.$disOrderLTLOrder->getId().'/dispatch',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disOrderLTLOrder);
            return true;
        }

        return false;
    }

    public function accept(DisOrderLTLOrder $disOrderLTLOrder) : bool
    {
        $data = $this->getTranslator()->objectToArray(
            $disOrderLTLOrder,
            array('acceptItemsAttachments', 'acceptAttachments', 'staff')
        );
       
        $this->patch(
            $this->getResource().'/'.$disOrderLTLOrder->getId().'/accept',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($disOrderLTLOrder);
            return true;
        }

        return false;
    }
}
