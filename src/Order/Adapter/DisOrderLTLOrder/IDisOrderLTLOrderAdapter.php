<?php
namespace Sdk\Order\Adapter\DisOrderLTLOrder;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

use Sdk\Order\Model\DisOrderLTLOrder;

interface IDisOrderLTLOrderAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function registerDispatchDate(DisOrderLTLOrder $disOrderLTLOrder) : bool;

    public function dispatch(DisOrderLTLOrder $disOrderLTLOrder) : bool;

    public function accept(DisOrderLTLOrder $disOrderLTLOrder) : bool;
}
