<?php
namespace Sdk\Order\Adapter\DisOrderLTLOrder;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Order\Model\DisOrderLTLOrder;

class DisOrderLTLOrderMockAdapter implements IDisOrderLTLOrderAdapter
{
    use FetchAbleMockAdapterTrait, OperateAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new DisOrderLTLOrder($id);
    }

    public function registerDispatchDate(DisOrderLTLOrder $disOrderLTLOrder) : bool
    {
        unset($disOrderLTLOrder);
        return true;
    }

    public function dispatch(DisOrderLTLOrder $disOrderLTLOrder) : bool
    {
        unset($disOrderLTLOrder);
        return true;
    }

    public function accept(DisOrderLTLOrder $disOrderLTLOrder) : bool
    {
        unset($disOrderLTLOrder);
        return true;
    }
}
