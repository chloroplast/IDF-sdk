<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Adapter\AmazonLTL\IAmazonLTLAdapter;
use Sdk\Order\Adapter\AmazonLTL\AmazonLTLMockAdapter;
use Sdk\Order\Adapter\AmazonLTL\AmazonLTLRestfulAdapter;

use Sdk\Application\ParseTask\Model\ParseTask;

class AmazonLTLRepository extends CommonRepository implements IAmazonLTLAdapter
{
    const LIST_MODEL_UN = 'AMAZONLTL_LIST';
    const FETCH_ONE_MODEL_UN = 'AMAZONLTL_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new AmazonLTLRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new AmazonLTLMockAdapter()
        );
    }

    public function memberCancel(AmazonLTL $amazonLTL) : bool
    {
        return $this->getAdapter()->memberCancel($amazonLTL);
    }

    public function staffCancel(AmazonLTL $amazonLTL) : bool
    {
        return $this->getAdapter()->staffCancel($amazonLTL);
    }

    public function confirm(AmazonLTL $amazonLTL) : bool
    {
        return $this->getAdapter()->confirm($amazonLTL);
    }

    public function batchConfirm(array $amazonLTLList) : bool
    {
        return $this->getAdapter()->batchConfirm($amazonLTLList);
    }

    public function registerPosition(AmazonLTL $amazonLTL) : bool
    {
        return $this->getAdapter()->registerPosition($amazonLTL);
    }

    public function freeze(AmazonLTL $amazonLTL) : bool
    {
        return $this->getAdapter()->freeze($amazonLTL);
    }

    public function unFreeze(AmazonLTL $amazonLTL) : bool
    {
        return $this->getAdapter()->unFreeze($amazonLTL);
    }

    public function parse(ParseTask $parseTask) : bool
    {
        return $this->getAdapter()->parse($parseTask);
    }

    public function autoGroup(array $amazonLTLList) : array
    {
        return $this->getAdapter()->autoGroup($amazonLTLList);
    }

    public function calculate(AmazonLTL $amazonLTL) : array
    {
        return $this->getAdapter()->calculate($amazonLTL);
    }
}
