<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\DisShipOrderFTLOrder;
use Sdk\Order\Adapter\DisShipOrderFTLOrder\IDisShipOrderFTLOrderAdapter;
use Sdk\Order\Adapter\DisShipOrderFTLOrder\DisShipOrderFTLOrderMockAdapter;
use Sdk\Order\Adapter\DisShipOrderFTLOrder\DisShipOrderFTLOrderRestfulAdapter;

class DisShipOrderFTLOrderRepository extends CommonRepository implements IDisShipOrderFTLOrderAdapter
{
    const LIST_MODEL_UN = 'DIS_SHIP_ORDERFTL_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'DIS_SHIP_ORDERFTL_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new DisShipOrderFTLOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new DisShipOrderFTLOrderMockAdapter()
        );
    }
}
