<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\DisOrderLTLOrder;
use Sdk\Order\Adapter\DisOrderLTLOrder\IDisOrderLTLOrderAdapter;
use Sdk\Order\Adapter\DisOrderLTLOrder\DisOrderLTLOrderMockAdapter;
use Sdk\Order\Adapter\DisOrderLTLOrder\DisOrderLTLOrderRestfulAdapter;

class DisOrderLTLOrderRepository extends CommonRepository implements IDisOrderLTLOrderAdapter
{
    const LIST_MODEL_UN = 'DIS_ORDERLTL_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'DIS_ORDERLTL_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new DisOrderLTLOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new DisOrderLTLOrderMockAdapter()
        );
    }

    public function registerDispatchDate(DisOrderLTLOrder $disOrderLTLOrder) : bool
    {
        return $this->getAdapter()->registerDispatchDate($disOrderLTLOrder);
    }
    
    public function dispatch(DisOrderLTLOrder $disOrderLTLOrder) : bool
    {
        return $this->getAdapter()->dispatch($disOrderLTLOrder);
    }

    public function accept(DisOrderLTLOrder $disOrderLTLOrder) : bool
    {
        return $this->getAdapter()->accept($disOrderLTLOrder);
    }
}
