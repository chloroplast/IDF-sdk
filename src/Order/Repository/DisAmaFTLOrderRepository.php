<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\DisAmaFTLOrder;
use Sdk\Order\Adapter\DisAmaFTLOrder\IDisAmaFTLOrderAdapter;
use Sdk\Order\Adapter\DisAmaFTLOrder\DisAmaFTLOrderMockAdapter;
use Sdk\Order\Adapter\DisAmaFTLOrder\DisAmaFTLOrderRestfulAdapter;

class DisAmaFTLOrderRepository extends CommonRepository implements IDisAmaFTLOrderAdapter
{
    const LIST_MODEL_UN = 'DIS_AMAFTL_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'DIS_AMAFTL_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new DisAmaFTLOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new DisAmaFTLOrderMockAdapter()
        );
    }

    public function updateISA(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        return $this->getAdapter()->updateISA($disAmaFTLOrder);
    }

    public function registerDispatchDate(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        return $this->getAdapter()->registerDispatchDate($disAmaFTLOrder);
    }
    
    public function dispatch(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        return $this->getAdapter()->dispatch($disAmaFTLOrder);
    }

    public function accept(DisAmaFTLOrder $disAmaFTLOrder) : bool
    {
        return $this->getAdapter()->accept($disAmaFTLOrder);
    }
}
