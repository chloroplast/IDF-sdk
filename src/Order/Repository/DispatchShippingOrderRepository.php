<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\DispatchShippingOrder;
use Sdk\Order\Adapter\DispatchShippingOrder\IDispatchShippingOrderAdapter;
use Sdk\Order\Adapter\DispatchShippingOrder\DispatchShippingOrderMockAdapter;
use Sdk\Order\Adapter\DispatchShippingOrder\DispatchShippingOrderRestfulAdapter;

class DispatchShippingOrderRepository extends CommonRepository implements IDispatchShippingOrderAdapter
{
    const LIST_MODEL_UN = 'DISPATCH_SHIPPING_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'DISPATCH_SHIPPING_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new DispatchShippingOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new DispatchShippingOrderMockAdapter()
        );
    }
}
