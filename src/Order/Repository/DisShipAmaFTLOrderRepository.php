<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\DisShipAmaFTLOrder;
use Sdk\Order\Adapter\DisShipAmaFTLOrder\IDisShipAmaFTLOrderAdapter;
use Sdk\Order\Adapter\DisShipAmaFTLOrder\DisShipAmaFTLOrderMockAdapter;
use Sdk\Order\Adapter\DisShipAmaFTLOrder\DisShipAmaFTLOrderRestfulAdapter;

class DisShipAmaFTLOrderRepository extends CommonRepository implements IDisShipAmaFTLOrderAdapter
{
    const LIST_MODEL_UN = 'DIS_SHIP_AMAFTL_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'DIS_SHIP_AMAFTL_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new DisShipAmaFTLOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new DisShipAmaFTLOrderMockAdapter()
        );
    }
}
