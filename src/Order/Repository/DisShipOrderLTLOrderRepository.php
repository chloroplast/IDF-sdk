<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\DisShipOrderLTLOrder;
use Sdk\Order\Adapter\DisShipOrderLTLOrder\IDisShipOrderLTLOrderAdapter;
use Sdk\Order\Adapter\DisShipOrderLTLOrder\DisShipOrderLTLOrderMockAdapter;
use Sdk\Order\Adapter\DisShipOrderLTLOrder\DisShipOrderLTLOrderRestfulAdapter;

class DisShipOrderLTLOrderRepository extends CommonRepository implements IDisShipOrderLTLOrderAdapter
{
    const LIST_MODEL_UN = 'DIS_SHIP_ORDERLTL_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'DIS_SHIP_ORDERLTL_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new DisShipOrderLTLOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new DisShipOrderLTLOrderMockAdapter()
        );
    }
}
