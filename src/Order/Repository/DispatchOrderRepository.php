<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\DispatchOrder;
use Sdk\Order\Adapter\DispatchOrder\IDispatchOrderAdapter;
use Sdk\Order\Adapter\DispatchOrder\DispatchOrderMockAdapter;
use Sdk\Order\Adapter\DispatchOrder\DispatchOrderRestfulAdapter;

class DispatchOrderRepository extends CommonRepository implements IDispatchOrderAdapter
{
    const LIST_MODEL_UN = 'DISPATCH_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'DISPATCH_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new DispatchOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new DispatchOrderMockAdapter()
        );
    }

    public function updateISA(DispatchOrder $dispatchOrder) : bool
    {
        return $this->getAdapter()->updateISA($dispatchOrder);
    }

    public function registerDispatchDate(DispatchOrder $dispatchOrder) : bool
    {
        return $this->getAdapter()->registerDispatchDate($dispatchOrder);
    }
    
    public function dispatch(DispatchOrder $dispatchOrder) : bool
    {
        return $this->getAdapter()->dispatch($dispatchOrder);
    }

    public function accept(DispatchOrder $dispatchOrder) : bool
    {
        return $this->getAdapter()->accept($dispatchOrder);
    }
}
