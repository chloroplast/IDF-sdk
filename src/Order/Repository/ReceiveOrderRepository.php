<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\ReceiveOrder;
use Sdk\Order\Adapter\ReceiveOrder\IReceiveOrderAdapter;
use Sdk\Order\Adapter\ReceiveOrder\ReceiveOrderMockAdapter;
use Sdk\Order\Adapter\ReceiveOrder\ReceiveOrderRestfulAdapter;

class ReceiveOrderRepository extends CommonRepository implements IReceiveOrderAdapter
{
    const LIST_MODEL_UN = 'RECEIVE_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'RECEIVE_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new ReceiveOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new ReceiveOrderMockAdapter()
        );
    }

    public function registerPickupDate(ReceiveOrder $receiveOrder) : bool
    {
        return $this->getAdapter()->registerPickupDate($receiveOrder);
    }

    public function pickup(ReceiveOrder $receiveOrder) : bool
    {
        return $this->getAdapter()->pickup($receiveOrder);
    }

    public function stockIn(ReceiveOrder $receiveOrder) : bool
    {
        return $this->getAdapter()->stockIn($receiveOrder);
    }
}
