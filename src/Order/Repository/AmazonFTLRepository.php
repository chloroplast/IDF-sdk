<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\AmazonFTL;
use Sdk\Order\Adapter\AmazonFTL\IAmazonFTLAdapter;
use Sdk\Order\Adapter\AmazonFTL\AmazonFTLMockAdapter;
use Sdk\Order\Adapter\AmazonFTL\AmazonFTLRestfulAdapter;

use Sdk\Application\ParseTask\Model\ParseTask;

class AmazonFTLRepository extends CommonRepository implements IAmazonFTLAdapter
{
    const LIST_MODEL_UN = 'AMAZONFTL_LIST';
    const FETCH_ONE_MODEL_UN = 'AMAZONFTL_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new AmazonFTLRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new AmazonFTLMockAdapter()
        );
    }

    public function memberCancel(AmazonFTL $amazonFTL) : bool
    {
        return $this->getAdapter()->memberCancel($amazonFTL);
    }

    public function staffCancel(AmazonFTL $amazonFTL) : bool
    {
        return $this->getAdapter()->staffCancel($amazonFTL);
    }

    public function confirm(AmazonFTL $amazonFTL) : bool
    {
        return $this->getAdapter()->confirm($amazonFTL);
    }

    public function batchConfirm(array $amazonFTLList) : bool
    {
        return $this->getAdapter()->batchConfirm($amazonFTLList);
    }

    public function freeze(AmazonFTL $amazonFTL) : bool
    {
        return $this->getAdapter()->freeze($amazonFTL);
    }

    public function unFreeze(AmazonFTL $amazonFTL) : bool
    {
        return $this->getAdapter()->unFreeze($amazonFTL);
    }

    public function parse(ParseTask $parseTask) : bool
    {
        return $this->getAdapter()->parse($parseTask);
    }

    public function calculate(AmazonFTL $amazonFTL) : array
    {
        return $this->getAdapter()->calculate($amazonFTL);
    }

    public function fetchOneCalculateRecord($id)
    {
        return $this->getAdapter()->fetchOneCalculateRecord($id);
    }
}
