<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\OrderStatusHistory;
use Sdk\Order\Adapter\OrderStatusHistory\IOrderStatusHistoryAdapter;
use Sdk\Order\Adapter\OrderStatusHistory\OrderStatusHistoryMockAdapter;
use Sdk\Order\Adapter\OrderStatusHistory\OrderStatusHistoryRestfulAdapter;

class OrderStatusHistoryRepository extends CommonRepository implements IOrderStatusHistoryAdapter
{
    const LIST_MODEL_UN = 'ORDER_STATUS_HISTORY_LIST';
    const FETCH_ONE_MODEL_UN = 'ORDER_STATUS_HISTORY_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new OrderStatusHistoryRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new OrderStatusHistoryMockAdapter()
        );
    }
}
