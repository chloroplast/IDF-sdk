<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Adapter\MemberOrder\IMemberOrderAdapter;
use Sdk\Order\Adapter\MemberOrder\MemberOrderMockAdapter;
use Sdk\Order\Adapter\MemberOrder\MemberOrderRestfulAdapter;

class MemberOrderRepository extends CommonRepository implements IMemberOrderAdapter
{
    const LIST_MODEL_UN = 'MEMBER_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'MEMBER_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new MemberOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new MemberOrderMockAdapter()
        );
    }
}
