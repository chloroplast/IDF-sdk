<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\RecShipOrderLTLOrder;
use Sdk\Order\Adapter\RecShipOrderLTLOrder\IRecShipOrderLTLOrderAdapter;
use Sdk\Order\Adapter\RecShipOrderLTLOrder\RecShipOrderLTLOrderMockAdapter;
use Sdk\Order\Adapter\RecShipOrderLTLOrder\RecShipOrderLTLOrderRestfulAdapter;

class RecShipOrderLTLOrderRepository extends CommonRepository implements IRecShipOrderLTLOrderAdapter
{
    const LIST_MODEL_UN = 'REC_SHIP_ORDERLTL_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'REC_SHIP_ORDERLTL_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new RecShipOrderLTLOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new RecShipOrderLTLOrderMockAdapter()
        );
    }
}
