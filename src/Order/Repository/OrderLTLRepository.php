<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\ItemLTL;
use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Adapter\OrderLTL\IOrderLTLAdapter;
use Sdk\Order\Adapter\OrderLTL\OrderLTLMockAdapter;
use Sdk\Order\Adapter\OrderLTL\OrderLTLRestfulAdapter;

use Sdk\Application\ParseTask\Model\ParseTask;

class OrderLTLRepository extends CommonRepository implements IOrderLTLAdapter
{
    const LIST_MODEL_UN = 'ORDERLTL_LIST';
    const FETCH_ONE_MODEL_UN = 'ORDERLTL_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new OrderLTLRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new OrderLTLMockAdapter()
        );
    }

    public function memberCancel(OrderLTL $orderLTL) : bool
    {
        return $this->getAdapter()->memberCancel($orderLTL);
    }

    public function staffCancel(OrderLTL $orderLTL) : bool
    {
        return $this->getAdapter()->staffCancel($orderLTL);
    }

    public function confirm(OrderLTL $orderLTL) : bool
    {
        return $this->getAdapter()->confirm($orderLTL);
    }

    public function batchConfirm(array $orderLTLList) : bool
    {
        return $this->getAdapter()->batchConfirm($orderLTLList);
    }

    public function registerPosition(OrderLTL $orderLTL) : bool
    {
        return $this->getAdapter()->registerPosition($orderLTL);
    }

    public function freeze(OrderLTL $orderLTL) : bool
    {
        return $this->getAdapter()->freeze($orderLTL);
    }

    public function unFreeze(OrderLTL $orderLTL) : bool
    {
        return $this->getAdapter()->unFreeze($orderLTL);
    }

    public function parse(ParseTask $parseTask) : bool
    {
        return $this->getAdapter()->parse($parseTask);
    }

    public function calculate(OrderLTL $orderLTL) : array
    {
        return $this->getAdapter()->calculate($orderLTL);
    }

    public function fetchOneCalculateRecord($id)
    {
        return $this->getAdapter()->fetchOneCalculateRecord($id);
    }

    public function calculateGoodsLevel(ItemLTL $itemLTL) : bool
    {
        return $this->getAdapter()->calculateGoodsLevel($itemLTL);
    }
}
