<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\RecOrderLTLOrder;
use Sdk\Order\Adapter\RecOrderLTLOrder\IRecOrderLTLOrderAdapter;
use Sdk\Order\Adapter\RecOrderLTLOrder\RecOrderLTLOrderMockAdapter;
use Sdk\Order\Adapter\RecOrderLTLOrder\RecOrderLTLOrderRestfulAdapter;

class RecOrderLTLOrderRepository extends CommonRepository implements IRecOrderLTLOrderAdapter
{
    const LIST_MODEL_UN = 'REC_ORDERLTL_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'REC_ORDERLTL_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new RecOrderLTLOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new RecOrderLTLOrderMockAdapter()
        );
    }

    public function registerPickupDate(RecOrderLTLOrder $recOrderLTLOrder) : bool
    {
        return $this->getAdapter()->registerPickupDate($recOrderLTLOrder);
    }

    public function pickup(RecOrderLTLOrder $recOrderLTLOrder) : bool
    {
        return $this->getAdapter()->pickup($recOrderLTLOrder);
    }

    public function stockIn(RecOrderLTLOrder $recOrderLTLOrder) : bool
    {
        return $this->getAdapter()->stockIn($recOrderLTLOrder);
    }
}
