<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\DisOrderFTLOrder;
use Sdk\Order\Adapter\DisOrderFTLOrder\IDisOrderFTLOrderAdapter;
use Sdk\Order\Adapter\DisOrderFTLOrder\DisOrderFTLOrderMockAdapter;
use Sdk\Order\Adapter\DisOrderFTLOrder\DisOrderFTLOrderRestfulAdapter;

class DisOrderFTLOrderRepository extends CommonRepository implements IDisOrderFTLOrderAdapter
{
    const LIST_MODEL_UN = 'DIS_ORDERFTL_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'DIS_ORDERFTL_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new DisOrderFTLOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new DisOrderFTLOrderMockAdapter()
        );
    }

    public function registerDispatchDate(DisOrderFTLOrder $disOrderFTLOrder) : bool
    {
        return $this->getAdapter()->registerDispatchDate($disOrderFTLOrder);
    }
    
    public function dispatch(DisOrderFTLOrder $disOrderFTLOrder) : bool
    {
        return $this->getAdapter()->dispatch($disOrderFTLOrder);
    }

    public function accept(DisOrderFTLOrder $disOrderFTLOrder) : bool
    {
        return $this->getAdapter()->accept($disOrderFTLOrder);
    }
}
