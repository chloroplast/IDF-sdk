<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\ReceiveShippingOrder;
use Sdk\Order\Adapter\ReceiveShippingOrder\IReceiveShippingOrderAdapter;
use Sdk\Order\Adapter\ReceiveShippingOrder\ReceiveShippingOrderMockAdapter;
use Sdk\Order\Adapter\ReceiveShippingOrder\ReceiveShippingOrderRestfulAdapter;

class ReceiveShippingOrderRepository extends CommonRepository implements IReceiveShippingOrderAdapter
{
    const LIST_MODEL_UN = 'RECEIVE_SHIPPING_ORDER_LIST';
    const FETCH_ONE_MODEL_UN = 'RECEIVE_SHIPPING_ORDER_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new ReceiveShippingOrderRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new ReceiveShippingOrderMockAdapter()
        );
    }
}
