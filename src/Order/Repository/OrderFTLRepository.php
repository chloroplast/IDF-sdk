<?php
namespace Sdk\Order\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Order\Model\OrderFTL;
use Sdk\Order\Adapter\OrderFTL\IOrderFTLAdapter;
use Sdk\Order\Adapter\OrderFTL\OrderFTLMockAdapter;
use Sdk\Order\Adapter\OrderFTL\OrderFTLRestfulAdapter;

use Sdk\Application\ParseTask\Model\ParseTask;

class OrderFTLRepository extends CommonRepository implements IOrderFTLAdapter
{
    const LIST_MODEL_UN = 'ORDERFTL_LIST';
    const FETCH_ONE_MODEL_UN = 'ORDERFTL_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new OrderFTLRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new OrderFTLMockAdapter()
        );
    }

    public function memberCancel(OrderFTL $orderFTL) : bool
    {
        return $this->getAdapter()->memberCancel($orderFTL);
    }

    public function staffCancel(OrderFTL $orderFTL) : bool
    {
        return $this->getAdapter()->staffCancel($orderFTL);
    }

    public function confirm(OrderFTL $orderFTL) : bool
    {
        return $this->getAdapter()->confirm($orderFTL);
    }

    public function batchConfirm(array $orderFTLList) : bool
    {
        return $this->getAdapter()->batchConfirm($orderFTLList);
    }

    public function freeze(OrderFTL $orderFTL) : bool
    {
        return $this->getAdapter()->freeze($orderFTL);
    }

    public function unFreeze(OrderFTL $orderFTL) : bool
    {
        return $this->getAdapter()->unFreeze($orderFTL);
    }

    public function parse(ParseTask $parseTask) : bool
    {
        return $this->getAdapter()->parse($parseTask);
    }

    public function calculate(OrderFTL $orderFTL) : array
    {
        return $this->getAdapter()->calculate($orderFTL);
    }

    public function fetchOneCalculateRecord($id)
    {
        return $this->getAdapter()->fetchOneCalculateRecord($id);
    }
}
