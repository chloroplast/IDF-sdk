<?php
namespace Sdk\Message\Adapter\ImMessage;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Message\Model\ImMessage;

class ImMessageMockAdapter implements IImMessageAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new ImMessage($id);
    }

    public function read(ImMessage $imMessage, array $keys) : bool
    {
        unset($imMessage);
        unset($keys);
        return true;
    }
}
