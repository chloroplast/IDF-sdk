<?php
namespace Sdk\Message\Adapter\ImMessage;

use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Message\Model\ImMessage;

interface IImMessageAdapter extends IFetchAbleAdapter
{
    public function read(ImMessage $imMessage, array $keys) : bool;
}
