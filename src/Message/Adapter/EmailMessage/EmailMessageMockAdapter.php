<?php
namespace Sdk\Message\Adapter\EmailMessage;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;

use Sdk\Message\Model\EmailMessage;

//use Sdk\Message\Utils\MockObjectGenerate;

class EmailMessageMockAdapter implements IEmailMessageAdapter
{
    use FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new EmailMessage($id);
       // return MockObjectGenerate::generateEmailMessage($id);
    }
}
