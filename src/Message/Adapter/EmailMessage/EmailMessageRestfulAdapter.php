<?php
namespace Sdk\Message\Adapter\EmailMessage;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Message\Model\EmailMessage;
use Sdk\Message\Model\NullEmailMessage;
use Sdk\Message\Translator\EmailMessageRestfulTranslator;

class EmailMessageRestfulAdapter extends CommonRestfulAdapter implements IEmailMessageAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array();
    
    const SCENARIOS = [
        'EMAIL_MESSAGE_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'EMAIL_MESSAGE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new EmailMessageRestfulTranslator(),
            'messages/email',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullEmailMessage::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }
}
