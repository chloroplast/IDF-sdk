<?php
namespace Sdk\Message\Adapter\Message;

use Sdk\Common\Model\Interfaces\IOperateAble;
use Sdk\Common\Adapter\Interfaces\IFetchAbleAdapter;
use Sdk\Common\Adapter\Interfaces\IOperateAbleAdapter;

interface IMessageAdapter extends IFetchAbleAdapter, IOperateAbleAdapter
{
    public function insertIsPerson(IOperateAble $operateAbleObject) : bool;
}
