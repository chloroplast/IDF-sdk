<?php
namespace Sdk\Message\Adapter\Message;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\Message\Model\Message;
use Sdk\Message\Model\NullMessage;
use Sdk\Message\Translator\MessageRestfulTranslator;

class MessageRestfulAdapter extends CommonRestfulAdapter implements IMessageAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'title' => TITLE_FORMAT_INCORRECT,
            'content' => CONTENT_FORMAT_INCORRECT,
            'scope' => PARAMETER_FORMAT_ERROR,
            'type' => PARAMETER_FORMAT_ERROR,
            'expireTime' => PARAMETER_FORMAT_ERROR
        ),
        100002 => array(
            'internalMessage' => RESOURCE_CAN_NOT_MODIFY
        ),
        100004 => array(
            'publisher' => MESSAGE_PUBLISHER_NOT_EXISTS,
            'receiverStaff' => MESSAGE_RECEIVER_NOT_EXISTS,
            'receiverMember' => MESSAGE_RECEIVER_NOT_EXISTS
        )
    );
    
    const SCENARIOS = [
        'MESSAGE_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'MESSAGE_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new MessageRestfulTranslator(),
            'messages',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullMessage::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function insertIsPerson(IOperateAble $operateAbleObject) : bool
    {
        $keys = array(
            'title',
            'content',
            'messageScope',
            'messageType',
            'expireTime',
            'publisher',
            'receiver'
        );
        $data = $this->getTranslator()->objectToArray($operateAbleObject, $keys);

        $this->post(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($operateAbleObject);
            return true;
        }

        return false;
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'title',
            'content',
            'messageScope',
            'messageType',
            'expireTime',
            'publisher'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array();
    }

    protected function enableTranslatorKeys() : array
    {
        return array();
    }

    protected function disableTranslatorKeys() : array
    {
        return array();
    }

    protected function deletedTranslatorKeys() : array
    {
        return array();
    }
}
