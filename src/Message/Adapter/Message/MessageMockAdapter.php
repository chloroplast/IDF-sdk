<?php
namespace Sdk\Message\Adapter\Message;

use Sdk\Common\Model\Interfaces\IOperateAble;
use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Message\Model\Message;

//use Sdk\Message\Utils\MockObjectGenerate;

class MessageMockAdapter implements IMessageAdapter
{
    use OperateAbleMockAdapterTrait, FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new Message($id);
       // return MockObjectGenerate::generateMessage($id);
    }

    public function insertIsPerson(IOperateAble $operateAbleObject) : bool
    {
        unset($operateAbleObject);
        return true;
    }
}
