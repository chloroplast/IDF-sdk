<?php
namespace Sdk\Message\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Message\Model\ImMessage;
use Sdk\Message\Model\NullImMessage;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ImMessageRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getMessageRestfulTranslator() : MessageRestfulTranslator
    {
        return new MessageRestfulTranslator();
    }
    
    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $imMessage = null)
    {
        if (empty($expression)) {
            return NullMessage::getInstance();
        }

        if ($imMessage == null) {
            $imMessage = new ImMessage();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $imMessage->setId($data['id']);
        }
        if (isset($attributes['status'])) {
            $imMessage->setStatus($attributes['status']);
        }
        if (isset($attributes['readTime'])) {
            $imMessage->setReadTime($attributes['readTime']);
        }
        if (isset($attributes['createTime'])) {
            $imMessage->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['message'])) {
            $messageArray = $this->relationshipFill($relationships['message'], $included);
            $message = $this->getMessageRestfulTranslator()->arrayToObject($messageArray);
            $imMessage->setMessage($message);
        }

        if (isset($relationships['receiver'])) {
            $receiverArray = $this->relationshipFill($relationships['receiver'], $included);
            $receiver = $this->getStaffRestfulTranslator()->arrayToObject($receiverArray);
            $imMessage->setReceiver($receiver);
        }

        return $imMessage;
    }

    public function objectToArray($imMessage, array $keys = array())
    {
        if (!$imMessage instanceof ImMessage) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'receiverMember',
                'receiverStaff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'internalMessages'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $imMessage->getId();
        }

        if (in_array('receiverMember', $keys)) {
            $receiverMemberRelationships = array(
                'type' => 'member',
                'id' => strval(Core::$container->get('member')->getId())
            );

            $expression['data']['relationships']['receiverMember']['data'] = $receiverMemberRelationships;
        }
        if (in_array('receiverStaff', $keys)) {
            $receiverStaffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['receiverStaff']['data'] = $receiverStaffRelationships;
        }
        
        return $expression;
    }
}
