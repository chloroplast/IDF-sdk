<?php
namespace Sdk\Message\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Message\Model\ImMessage;
use Sdk\Message\Model\NullImMessage;

use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class ImMessageTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getMessageTranslator() : MessageTranslator
    {
        return new MessageTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullMessage::getInstance();
    }

    public function arrayToObject(array $expression, $imMessage = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($imMessage == null) {
            $imMessage = new ImMessage();
        }
        
        if (isset($expression['id'])) {
            $imMessage->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['receiver'])) {
            $receiver = $this->getStaffTranslator()->arrayToObject($expression['receiver']);
            $imMessage->setReceiver($receiver);
        }
        if (isset($expression['message'])) {
            $message = $this->getMessageTranslator()->arrayToObject($expression['message']);
            $imMessage->setMessage($message);
        }
        if (isset($expression['status']['id'])) {
            $imMessage->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $imMessage->setCreateTime($expression['createTime']);
        }
        if (isset($expression['readTime'])) {
            $imMessage->setReadTime($expression['readTime']);
        }

        return $imMessage;
    }

    public function objectToArray($imMessage, array $keys = array())
    {
        if (!$imMessage instanceof ImMessage) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'message',
                'receiver',
                'status',
                'readTime',
                'createTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($imMessage->getId());
        }
        if (isset($keys['message'])) {
            $expression['message'] = $this->getMessageTranslator()->objectToArray(
                $imMessage->getMessage(),
                $keys['message']
            );
        }
        if (isset($keys['receiver'])) {
            $expression['receiver'] = $this->getStaffTranslator()->objectToArray(
                $imMessage->getReceiver(),
                $keys['receiver']
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion(
                $imMessage->getStatus(),
                ImMessage::STATUS_TYPE,
                ImMessage::STATUS_CN
            );
        }
        if (in_array('readTime', $keys)) {
            $readTime = $imMessage->getReadTime();
            $expression['readTime'] = $readTime;
            $expression['readTimeFormatConvert'] = !empty($readTime)
            ? $this->convertTimeZone('Y-m-d H:i', $readTime)
            : '';
        }
        if (in_array('createTime', $keys)) {
            $createTime = $imMessage->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }

        return $expression;
    }
}
