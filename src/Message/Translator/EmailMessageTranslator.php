<?php
namespace Sdk\Message\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Message\Model\EmailMessage;
use Sdk\Message\Model\NullEmailMessage;

use Sdk\User\Staff\Translator\StaffTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class EmailMessageTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getStaffTranslator() : StaffTranslator
    {
        return new StaffTranslator();
    }

    protected function getMessageTranslator() : MessageTranslator
    {
        return new MessageTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullMessage::getInstance();
    }

    public function arrayToObject(array $expression, $emailMessage = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($emailMessage == null) {
            $emailMessage = new EmailMessage();
        }
        
        if (isset($expression['id'])) {
            $emailMessage->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['message'])) {
            $message = $this->getMessageTranslator()->arrayToObject($expression['message']);
            $emailMessage->setMessage($message);
        }
        if (isset($expression['status']['id'])) {
            $emailMessage->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $emailMessage->setCreateTime($expression['createTime']);
        }
        if (isset($expression['address'])) {
            $emailMessage->setAddress($expression['address']);
        }

        return $emailMessage;
    }

    public function objectToArray($emailMessage, array $keys = array())
    {
        if (!$emailMessage instanceof EmailMessage) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'address',
                'status',
                'errorDescription',
                'createTime',
                'message'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($emailMessage->getId());
        }
        if (in_array('address', $keys)) {
            $expression['address'] = $emailMessage->getAddress();
        }
        if (in_array('errorDescription', $keys)) {
            $expression['errorDescription'] = $emailMessage->getErrorDescription();
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion(
                $emailMessage->getStatus(),
                EmailMessage::STATUS_TYPE,
                EmailMessage::STATUS_CN
            );
        }
        if (in_array('createTime', $keys)) {
            $createTime = $emailMessage->getCreateTime();
            $expression['createTime'] = $createTime;
            $expression['createTimeFormatConvert'] = !empty($createTime)
            ? $this->convertTimeZone('Y-m-d H:i', $createTime)
            : '';
        }
        if (isset($keys['message'])) {
            $expression['message'] = $this->getMessageTranslator()->objectToArray(
                $emailMessage->getMessage(),
                $keys['message']
            );
        }

        return $expression;
    }
}
