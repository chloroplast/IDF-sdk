<?php
namespace Sdk\Message\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Message\Model\EmailMessage;
use Sdk\Message\Model\NullEmailMessage;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class EmailMessageRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getMessageRestfulTranslator() : MessageRestfulTranslator
    {
        return new MessageRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $emailMessage = null)
    {
        if (empty($expression)) {
            return NullMessage::getInstance();
        }

        if ($emailMessage == null) {
            $emailMessage = new EmailMessage();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $emailMessage->setId($data['id']);
        }
        if (isset($attributes['address'])) {
            $emailMessage->setAddress($attributes['address']);
        }
        if (isset($attributes['errorDescription'])) {
            $emailMessage->setErrorDescription($attributes['errorDescription']);
        }
        if (isset($attributes['status'])) {
            $emailMessage->setStatus($attributes['status']);
        }
        if (isset($attributes['createTime'])) {
            $emailMessage->setCreateTime($attributes['createTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['message'])) {
            $messageArray = $this->relationshipFill($relationships['message'], $included);
            $message = $this->getMessageRestfulTranslator()->arrayToObject($messageArray);
            $emailMessage->setMessage($message);
        }

        return $emailMessage;
    }

    public function objectToArray($emailMessage, array $keys = array())
    {
        unset($emailMessage);
        unset($keys);
        return array();
    }
}
