<?php
namespace Sdk\Message\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Message\Model\Message;
use Sdk\Message\Model\NullMessage;

use Sdk\User\Member\Translator\MemberRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class MessageRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getMemberRestfulTranslator() : MemberRestfulTranslator
    {
        return new MemberRestfulTranslator();
    }

    protected function getStaffRestfulTranslator() : StaffRestfulTranslator
    {
        return new StaffRestfulTranslator();
    }
    
    public function arrayToObject(array $expression, $message = null)
    {
        if (empty($expression)) {
            return NullMessage::getInstance();
        }

        if ($message == null) {
            $message = new Message();
        }
       
        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();

        if (isset($data['id'])) {
            $message->setId($data['id']);
        }
        if (isset($attributes['title'])) {
            $message->setTitle($attributes['title']);
        }
        if (isset($attributes['content'])) {
            $message->setContent($attributes['content']);
        }
        if (isset($attributes['scope'])) {
            $message->setMessageScope($attributes['scope']);
        }
        if (isset($attributes['messageType'])) {
            $message->setMessageType($attributes['messageType']);
        }
        if (isset($attributes['expireTime'])) {
            $message->setExpireTime($attributes['expireTime']);
        }
        if (isset($attributes['status'])) {
            $message->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $message->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $message->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $message->setUpdateTime($attributes['updateTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }

        if (isset($relationships['publisher'])) {
            $publisherArray = $this->relationshipFill($relationships['publisher'], $included);
            $publisher = $this->getStaffRestfulTranslator()->arrayToObject($publisherArray);
            $message->setPublisher($publisher);
        }

        if (isset($relationships['receiver'])) {
            $receiverArray = $this->relationshipFill($relationships['receiver'], $included);
            if ($attributes['scope'] == Message::MESSAGE_SCOPE_IS_PERSON['MEMBER']) {
                $receiver = $this->getMemberRestfulTranslator()->arrayToObject($receiverArray);
            }
            if ($attributes['scope'] == Message::MESSAGE_SCOPE_IS_PERSON['STAFF']) {
                $receiver = $this->getStaffRestfulTranslator()->arrayToObject($receiverArray);
            }
            $message->setReceiver($receiver);
        }

        return $message;
    }

    public function objectToArray($message, array $keys = array())
    {
        if (!$message instanceof Message) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'title',
                'content',
                'messageScope',
                'messageType',
                'expireTime',
                'publisher',
                'receiver'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'messages'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $message->getId();
        }

        $attributes = array();

        if (in_array('title', $keys)) {
            $attributes['title'] = $message->getTitle();
        }
        if (in_array('content', $keys)) {
            $attributes['content'] = $message->getContent();
        }

        if (in_array('messageScope', $keys)) {
            $attributes['scope'] = $message->getMessageScope();
        }
        if (in_array('messageType', $keys)) {
            $attributes['messageType'] = $message->getMessageType();
        }
        if (in_array('expireTime', $keys)) {
            $attributes['expireTime'] = $message->getExpireTime();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('publisher', $keys)) {
            $publisherRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['publisher']['data'] = $publisherRelationships;
        }
        if (in_array('receiver', $keys)) {
            if ($message->getMessageScope() == Message::MESSAGE_SCOPE_IS_PERSON['MEMBER']) {
                $receiverRelationships = array(
                    'type' => 'member',
                    'id' => strval($message->getReceiver()->getId())
                );
                $expression['data']['relationships']['receiverMember']['data'] = $receiverRelationships;
            }
            if ($message->getMessageScope() == Message::MESSAGE_SCOPE_IS_PERSON['STAFF']) {
                $receiverRelationships = array(
                    'type' => 'staff',
                    'id' => strval($message->getReceiver()->getId())
                );
                $expression['data']['relationships']['receiverStaff']['data'] = $receiverRelationships;
            }
        }
        
        return $expression;
    }
}
