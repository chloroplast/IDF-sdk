<?php
namespace Sdk\Message\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Message\Model\EmailMessage;
use Sdk\Message\Adapter\EmailMessage\IEmailMessageAdapter;
use Sdk\Message\Adapter\EmailMessage\EmailMessageMockAdapter;
use Sdk\Message\Adapter\EmailMessage\EmailMessageRestfulAdapter;

class EmailMessageRepository extends CommonRepository implements IEmailMessageAdapter
{
    const LIST_MODEL_UN = 'EMAIL_MESSAGE_LIST';
    const FETCH_ONE_MODEL_UN = 'EMAIL_MESSAGE_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new EmailMessageRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new EmailMessageMockAdapter()
        );
    }
}
