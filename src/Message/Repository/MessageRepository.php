<?php
namespace Sdk\Message\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\Message\Model\Message;
use Sdk\Message\Adapter\Message\IMessageAdapter;
use Sdk\Message\Adapter\Message\MessageMockAdapter;
use Sdk\Message\Adapter\Message\MessageRestfulAdapter;

class MessageRepository extends CommonRepository implements IMessageAdapter
{
    const LIST_MODEL_UN = 'MESSAGE_LIST';
    const FETCH_ONE_MODEL_UN = 'MESSAGE_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new MessageRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new MessageMockAdapter()
        );
    }

    public function insertIsPerson(IOperateAble $operateAbleObject) : bool
    {
        return $this->getAdapter()->insertIsPerson($operateAbleObject);
    }
}
