<?php
namespace Sdk\Message\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Message\Model\ImMessage;
use Sdk\Message\Adapter\ImMessage\IImMessageAdapter;
use Sdk\Message\Adapter\ImMessage\ImMessageMockAdapter;
use Sdk\Message\Adapter\ImMessage\ImMessageRestfulAdapter;

class ImMessageRepository extends CommonRepository implements IImMessageAdapter
{
    const LIST_MODEL_UN = 'IM_MESSAGE_LIST';
    const FETCH_ONE_MODEL_UN = 'IM_MESSAGE_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new ImMessageRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new ImMessageMockAdapter()
        );
    }

    public function read(ImMessage $imMessage, array $keys) : bool
    {
        return $this->getAdapter()->read($imMessage, $keys);
    }
}
