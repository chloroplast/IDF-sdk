<?php
namespace Sdk\Message\Model;

use Sdk\Role\Purview\Model\Purview;
use Sdk\Role\Purview\Model\IPurviewAble;

class MessagePurview extends Purview
{

    public function __construct()
    {
        parent::__construct(IPurviewAble::COLUMN['MESSAGE']);
    }

    public function add() : bool
    {
        return $this->operation('add');
    }
}
