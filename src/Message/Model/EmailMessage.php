<?php
namespace Sdk\Message\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Message\Repository\EmailMessageRepository;

class EmailMessage implements IObject
{
    use Object;

    const STATUS = array(
        'FAIL' => -1,
        'SUCCESS' => 0
    );
    const STATUS_CN = array(
        self::STATUS['FAIL'] => '发送失败',
        self::STATUS['SUCCESS'] => '发送成功'
    );
    const STATUS_TYPE = array(
        self::STATUS['FAIL'] => 'danger',
        self::STATUS['SUCCESS'] => 'success'
    );

    private $id;
    /**
     * @var array $address 接收地址
     */
    private $address;
    /**
     * @var Message $message 关联消息
     */
    private $message;
    /**
     * @var string $errorDescription 错误描述
     */
    private $errorDescription;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->status = self::STATUS['SUCCESS'];
        $this->address = array();
        $this->message = new Message();
        $this->errorDescription = '';
        $this->createTime = 0;
        $this->repository = new EmailMessageRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->status);
        unset($this->address);
        unset($this->message);
        unset($this->errorDescription);
        unset($this->createTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setAddress(array $address): void
    {
        $this->address = $address;
    }

    public function getAddress(): array
    {
        return $this->address;
    }

    public function setMessage(Message $message): void
    {
        $this->message = $message;
    }

    public function getMessage(): Message
    {
        return $this->message;
    }

    public function setErrorDescription(string $errorDescription): void
    {
        $this->errorDescription = $errorDescription;
    }

    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : EmailMessageRepository
    {
        return $this->repository;
    }
}
