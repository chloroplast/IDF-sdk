<?php
namespace Sdk\Message\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\Message\Repository\MessageRepository;
use Sdk\User\Staff\Model\Staff;
use Sdk\User\Model\User;

class Message implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    const M_STATUS = array(
        'ALL_SUCCESS' => 0,
        'PARTIALLY_SUCCESS' => 1,
        'ALL_FAIL' => 2
    );
    const M_STATUS_CN = array(
        self::M_STATUS['ALL_SUCCESS'] => '全部成功',
        self::M_STATUS['PARTIALLY_SUCCESS'] => '部分成功',
        self::M_STATUS['ALL_FAIL'] => '全部失败'
    );
    const M_STATUS_TYPE = array(
        self::M_STATUS['ALL_SUCCESS'] => 'success',
        self::M_STATUS['PARTIALLY_SUCCESS'] => 'danger',
        self::M_STATUS['ALL_FAIL'] => 'danger'
    );

    /**
     * 消息作用域为个人的id
     * 6 个人(前台)
     * 7 个人(后台)
     */
    const MESSAGE_SCOPE_IS_PERSON = array(
        'MEMBER' => 6, 
        'STAFF' => 7
    );

    private $id;
    /**
     * @var string $title 标题
     */
    private $title;
    /**
     * @var string $content 内容
     */
    private $content;
    /**
     * @var int $messageType 类型
     */
    private $messageType;
    /**
     * @var int $messageScope 作用域
     */
    private $messageScope;
    /**
     * @var User $receiver 接收人
     */
    private $receiver;
    /**
     * @var int $expireTime 有效期
     */
    private $expireTime;
    /**
     * @var Staff $publisher 发布人
     */
    private $publisher;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->title = '';
        $this->content = '';
        $this->messageType = 0;
        $this->messageScope = 0;
        $this->receiver = null;
        $this->expireTime = 0;
        $this->publisher = new Staff();
        $this->status = self::M_STATUS['ALL_SUCCESS'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new MessageRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->title);
        unset($this->content);
        unset($this->messageType);
        unset($this->messageScope);
        unset($this->receiver);
        unset($this->expireTime);
        unset($this->publisher);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setMessageType(int $messageType): void
    {
        $this->messageType = $messageType;
    }

    public function getMessageType(): int
    {
        return $this->messageType;
    }

    public function setMessageScope(int $messageScope): void
    {
        $this->messageScope = $messageScope;
    }

    public function getMessageScope(): int
    {
        return $this->messageScope;
    }

    public function setReceiver(User $receiver): void
    {
        $this->receiver = $receiver;
    }

    public function getReceiver(): User
    {
        return $this->receiver;
    }

    public function setExpireTime(int $expireTime): void
    {
        $this->expireTime = $expireTime;
    }

    public function getExpireTime(): int
    {
        return $this->expireTime;
    }

    public function setPublisher(Staff $publisher): void
    {
        $this->publisher = $publisher;
    }

    public function getPublisher(): Staff
    {
        return $this->publisher;
    }

    protected function getRepository() : MessageRepository
    {
        return $this->repository;
    }

    public function insert() : bool
    {
        if (in_array($this->getMessageScope(), self::MESSAGE_SCOPE_IS_PERSON)) {
            return $this->getRepository()->insertIsPerson($this);
        }
        return $this->getRepository()->insert($this);
    }
}
