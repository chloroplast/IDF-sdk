<?php
namespace Sdk\Message\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

use Sdk\Message\Repository\ImMessageRepository;
use Sdk\User\Staff\Model\Staff;

class ImMessage implements IObject
{
    use Object;

    const STATUS = array(
        'UNREAD' => 0,
        'READ' => 1
    );
    const STATUS_CN = array(
        self::STATUS['UNREAD'] => '未读',
        self::STATUS['READ'] => '已读'
    );
    const STATUS_TYPE = array(
        self::STATUS['UNREAD'] => 'danger',
        self::STATUS['READ'] => 'success'
    );

    private $id;
    /**
     * @var Staff $receiver 接收人
     */
    private $receiver;
    /**
     * @var int $readTime 接收时间
     */
    private $readTime;
    /**
     * @var Message $message 关联消息
     */
    private $message;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->receiver = new Staff();
        $this->status = self::STATUS['UNREAD'];
        $this->readTime = 0;
        $this->message = new Message();
        $this->createTime = 0;
        $this->repository = new ImMessageRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->receiver);
        unset($this->status);
        unset($this->readTime);
        unset($this->message);
        unset($this->createTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setReceiver(Staff $receiver): void
    {
        $this->receiver = $receiver;
    }

    public function getReceiver(): Staff
    {
        return $this->receiver;
    }

    public function setReadTime(int $readTime): void
    {
        $this->readTime = $readTime;
    }

    public function getReadTime(): int
    {
        return $this->readTime;
    }

    public function setMessage(Message $message): void
    {
        $this->message = $message;
    }

    public function getMessage(): Message
    {
        return $this->message;
    }

    public function setStatus(int $status) : void
    {
        $this->status = $status;
    }

    protected function getRepository() : ImMessageRepository
    {
        return $this->repository;
    }

    public function read(array $keys) : bool
    {
        return $this->getRepository()->read($this, $keys);
    }
}
