<?php
namespace Sdk\User\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

abstract class User implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    private $id;
    /**
     * @var string $name 姓名
     */
    private $name;
    /**
     * @var string $phone 电话
     */
    private $phone;
    /**
     * @var string $email 邮箱
     */
    private $email;
    /**
     * @var string $password 密码
     */
    private $password;
    /**
     * @var string $oldPassword 旧密码
     */
    private $oldPassword;
    /**
     * @var string $identification
     */
    private $identification;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->name = '';
        $this->phone = '';
        $this->email = '';
        $this->password = '';
        $this->oldPassword = '';
        $this->identification = '';
        $this->status = self::STATUS['ENABLED'];
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->name);
        unset($this->phone);
        unset($this->email);
        unset($this->password);
        unset($this->oldPassword);
        unset($this->identification);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setOldPassword(string $oldPassword): void
    {
        $this->oldPassword = $oldPassword;
    }

    public function getOldPassword(): string
    {
        return $this->oldPassword;
    }

    public function setIdentification(string $identification): void
    {
        $this->identification = $identification;
    }

    public function getIdentification(): string
    {
        return $this->identification;
    }

    abstract protected function getRepository();

    public function resetPassword() : bool
    {
        return $this->getRepository()->resetPassword($this);
    }

    public function updatePassword() : bool
    {
        return $this->getRepository()->updatePassword($this);
    }
}
