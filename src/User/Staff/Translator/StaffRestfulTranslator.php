<?php
namespace Sdk\User\Staff\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\User\Staff\Model\Staff;
use Sdk\User\Staff\Model\NullStaff;

use Sdk\Role\Translator\RoleRestfulTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class StaffRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    protected function getRoleRestfulTranslator() : RoleRestfulTranslator
    {
        return new RoleRestfulTranslator();
    }

    public function arrayToObject(array $expression, $staff = null)
    {
        if (empty($expression)) {
            return NullStaff::getInstance();
        }

        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();
        $relationships = isset($data['relationships']) ? $data['relationships'] : array();
        $included = isset($expression['included']) ? $expression['included'] : array();
            
        if ($staff == null) {
            $staff = new Staff();
        }
       
        if (isset($data['id'])) {
            $staff->setId($data['id']);
        }
        if (isset($attributes['email'])) {
            $staff->setEmail($attributes['email']);
        }
        if (isset($attributes['name'])) {
            $staff->setName($attributes['name']);
        }
        if (isset($attributes['phone'])) {
            $staff->setPhone($attributes['phone']);
        }
        if (isset($attributes['password'])) {
            $staff->setPassword($attributes['password']);
        }
        if (isset($attributes['purview'])) {
            $staff->setPurview($attributes['purview']);
        }
        if (isset($attributes['status'])) {
            $staff->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $staff->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $staff->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $staff->setUpdateTime($attributes['updateTime']);
        }

        if (!empty($included)) {
            $included = $this->includedFormatConversion($included);
        }
        if (isset($relationships['roles'])) {
            $roles = array();
            foreach ($relationships['roles']['data'] as $role) {
                $roleObject['data'] = $role;
                $roleArray = $this->relationshipFill($roleObject, $included);
                $role = $this->getRoleRestfulTranslator()->arrayToObject($roleArray);
                $roles[] = $role;
            }
            $staff->setRoles($roles);
        }

        return $staff;
    }

    public function objectToArray($staff, array $keys = array())
    {
        if (!$staff instanceof Staff) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'email',
                'name',
                'phone',
                'password',
                'oldPassword',
                'roles',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'staff'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $staff->getId();
        }

        $attributes = array();
        
        if (in_array('email', $keys)) {
            $attributes['email'] = $staff->getEmail();
        }
        if (in_array('name', $keys)) {
            $attributes['name'] = $staff->getName();
        }
        if (in_array('phone', $keys)) {
            $attributes['phone'] = $staff->getPhone();
        }
        if (in_array('password', $keys)) {
            $attributes['password'] = $staff->getPassword();
        }
        if (in_array('oldPassword', $keys)) {
            $attributes['oldPassword'] = $staff->getOldPassword();
        }
        $expression['data']['attributes'] = $attributes;

        if (in_array('roles', $keys)) {
            $roleRelationships = array();
            foreach ($staff->getRoles() as $role) {
                $roleRelationships[] = array(
                    'type' => 'roles',
                    'id' => strval($role->getId())
                );
            }

            $expression['data']['relationships']['roles']['data'] = $roleRelationships;
        }

        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
