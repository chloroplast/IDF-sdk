<?php
namespace Sdk\User\Staff\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\User\Staff\Model\Staff;
use Sdk\User\Staff\Model\NullStaff;

use Sdk\Role\Translator\RoleTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class StaffTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getRoleTranslator() : RoleTranslator
    {
        return new RoleTranslator();
    }

    protected function getNullObject() : INull
    {
        return NullStaff::getInstance();
    }

    public function arrayToObject(array $expression, $staff = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($staff == null) {
            $staff = new Staff();
        }

        if (isset($expression['id'])) {
            $staff->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['email'])) {
            $staff->setEmail($expression['email']);
        }
        if (isset($expression['name'])) {
            $staff->setName($expression['name']);
        }
        if (isset($expression['phone'])) {
            $staff->setPhone($expression['phone']);
        }
        if (isset($expression['roles'])) {
            $roleList = array();
            foreach ($expression['roles'] as $role) {
                $roleList[] = $this->getRoleTranslator()->arrayToObject($role);
            }

            $staff->setRoles($roleList);
        }
        if (isset($expression['purview'])) {
            $staff->setPurview($this->purviewFormatConversionToObject($expression['purview']));
        }
        if (isset($expression['status']['id'])) {
            $staff->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $staff->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $staff->setUpdateTime($expression['updateTime']);
        }

        return $staff;
    }

    public function objectToArray($staff, array $keys = array())
    {
        if (!$staff instanceof Staff) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'email',
                'name',
                'phone',
                'roles' => [],
                'purview',
                'status',
                'createTime',
                'updateTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($staff->getId());
        }
        if (in_array('email', $keys)) {
            $expression['email'] = $staff->getEmail();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $staff->getName();
        }
        if (in_array('phone', $keys)) {
            $expression['phone'] = $staff->getPhone();
        }
        if (isset($keys['roles'])) {
            $roles = $staff->getRoles();
            foreach ($roles as $role) {
                $expression['roles'][] = $this->getRoleTranslator()->objectToArray(
                    $role,
                    $keys['roles']
                );
            }
        }
        if (in_array('purview', $keys)) {
            $expression['purview'] = $this->purviewFormatConversionToArray($staff->getPurview());
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($staff->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $staff->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $staff->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $staff->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $staff->getUpdateTime());
        }

        return $expression;
    }

    protected function purviewFormatConversionToArray(array $purview) : array
    {
        $conversionPurview = array();

        foreach ($purview as $key => $each) {
            $conversionPurview[] = array(
                'id' => $key,
                'actions' => $each
            );
        }
        
        return $conversionPurview;
    }

    protected function purviewFormatConversionToObject(array $purview) : array
    {
        $conversionPurview = array();

        foreach ($purview as $each) {
            $conversionPurview[$each['id']] = $each['actions'];
        }
        
        return $conversionPurview;
    }
}
