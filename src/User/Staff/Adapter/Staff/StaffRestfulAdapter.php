<?php
namespace Sdk\User\Staff\Adapter\Staff;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\User\Staff\Model\Staff;
use Sdk\User\Staff\Model\NullStaff;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

class StaffRestfulAdapter extends CommonRestfulAdapter implements IStaffAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;

    const MAP_ERROR = array(
        100001 => array(
            'name' => NAME_FORMAT_INCORRECT,
            'email' => EMAIL_FORMAT_INCORRECT,
            'phone' => PHONE_FORMAT_INCORRECT,
            'password' => PASSWORD_FORMAT_INCORRECT,
            'roles' => STAFF_ROLES_FORMAT_INCORRECT
        ),
        100002 => array(
            'staff' => RESOURCE_CAN_NOT_MODIFY
        ),
        100003 => array(
            'email' => EMAIL_EXISTS
        ),
        100004 => array(
            'roles' => STAFF_ROLES_NOT_EXISTS,
            'staff' => ACCOUNT_NOT_EXISTS,
            'oldPassword' => OLD_PASSWORD_INCORRECT
        )
    );
    
    const SCENARIOS = [
        'STAFF_LIST'=>[
            'fields' => [
                'staff'=>'name,phone,email,roles,status,updateTime',
            ],
            'include' => 'roles'
        ],
        'STAFF_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>'roles'
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new StaffRestfulTranslator(),
            'staff',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullStaff::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'name',
            'phone',
            'email',
            'password',
            'roles',
            'staff'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'name',
            'phone',
            'email',
            'roles',
            'staff'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function disableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function deletedTranslatorKeys() : array
    {
        return array();
    }

    public function login(Staff $staff) : bool
    {
        $data = $this->getTranslator()->objectToArray($staff, array('email', 'password'));
       
        $this->post(
            $this->getResource().'/login',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }

    public function resetPassword(Staff $staff) : bool
    {
        $data = $this->getTranslator()->objectToArray($staff, array('password', 'staff'));
       
        $this->patch(
            $this->getResource().'/'.$staff->getId().'/resetPassword',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }

    public function updatePassword(Staff $staff) : bool
    {
        $data = $this->getTranslator()->objectToArray($staff, array('password', 'oldPassword', 'staff'));
       
        $this->patch(
            $this->getResource().'/'.$staff->getId().'/password',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($staff);
            return true;
        }

        return false;
    }
}
