<?php
namespace Sdk\User\Staff\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

class StaffWidgetRule
{
    public function roles($roles) : bool
    {
        if (!V::arrayType()->validate($roles)) {
            Core::setLastError(STAFF_ROLES_FORMAT_INCORRECT);
            return false;
        }

        foreach ($roles as $role) {
            if (!V::numeric()->positive()->validate($role)) {
                Core::setLastError(STAFF_ROLES_FORMAT_INCORRECT);
                return false;
            }
        }

        return true;
    }
}
