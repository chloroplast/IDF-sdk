<?php
namespace Sdk\User\Staff\Model;

use Sdk\User\Model\User;
use Sdk\User\Staff\Repository\StaffRepository;

use Sdk\Organization\Department\Model\Department;
use Sdk\Organization\Organization\Model\Organization;

class Staff extends User
{
    /**
     * @var array $purview 权限范围
     */
    private $purview;
    /**
     * @var array $roles 角色
     */
    private $roles;

    private $repository;

    private $staffJwtAuth;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->purview = array();
        $this->roles = array();
        $this->repository = new StaffRepository();
        $this->staffJwtAuth = new StaffJwtAuth();
    }

    public function __destruct()
    {
        unset($this->purview);
        unset($this->roles);
        unset($this->repository);
        unset($this->staffJwtAuth);
    }

    public function setPurview(array $purview): void
    {
        $this->purview = $purview;
    }

    public function getPurview(): array
    {
        return $this->purview;
    }
    
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function getRoles(): array
    {
        return $this->roles;
    }
    
    protected function getRepository() : StaffRepository
    {
        return $this->repository;
    }

    protected function getStaffJwtAuth() : StaffJwtAuth
    {
        return $this->staffJwtAuth;
    }

    public function login() : bool
    {
        return $this->getRepository()->login($this)
            && $this->getStaffJwtAuth()->generateJwtAndSaveStaffToCache($this);
    }

    public function logout() : bool
    {
        return $this->getStaffJwtAuth()->clearJwtAndStaffToCache($this);
    }
}
