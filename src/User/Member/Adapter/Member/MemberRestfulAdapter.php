<?php
namespace Sdk\User\Member\Adapter\Member;

use Marmot\Core;
use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleRestfulAdapterTrait;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Model\NullMember;
use Sdk\User\Member\Translator\MemberRestfulTranslator;

class MemberRestfulAdapter extends CommonRestfulAdapter implements IMemberAdapter
{
    use FetchAbleRestfulAdapterTrait,
        OperateAbleRestfulAdapterTrait,
        MapErrorsTrait;
        
    const MAP_ERROR = array(
        100001 => array(
            'email' => EMAIL_FORMAT_INCORRECT,
            'phone' => PHONE_FORMAT_INCORRECT,
            'userName' => USER_NAME_FORMAT_INCORRECT,
            'password' => PASSWORD_FORMAT_INCORRECT,
            'enterpriseName' => ENTERPRISE_NAME_FORMAT_INCORRECT,
            'enterpriseAddress' => ENTERPRISE_ADDRESS_FORMAT_INCORRECT
        ),
        100002 => array(
            'member' => RESOURCE_CAN_NOT_MODIFY
        ),
        100003 => array(
            'email' => EMAIL_EXISTS,
            'userName' => USER_NAME_EXISTS,
        ),
        100004 => array(
            'member' => ACCOUNT_NOT_EXISTS,
            'oldPassword' => OLD_PASSWORD_INCORRECT
        )
    );
    
    const SCENARIOS = [
        'MEMBER_LIST'=>[
            'fields'=>[],
            'include'=>''
        ],
        'MEMBER_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new MemberRestfulTranslator(),
            'members',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullMember::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    protected function insertTranslatorKeys() : array
    {
        return array(
            'email',
            'phone',
            'userName',
            'password',
            'enterpriseName',
            'enterpriseAddress'
        );
    }

    protected function updateTranslatorKeys() : array
    {
        return array(
            'phone',
            'enterpriseName',
            'enterpriseAddress'
        );
    }

    protected function enableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function disableTranslatorKeys() : array
    {
        return array(
            'staff'
        );
    }

    protected function deletedTranslatorKeys() : array
    {
        return array();
    }

    public function login(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray($member, array('identify', 'password'));
       
        $this->post(
            $this->getResource().'/signIn',
            $data
        );

        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }
        
        return false;
    }

    public function resetPassword(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray($member, array('password'));
       
        $this->patch(
            $this->getResource().'/'.$member->getId().'/resetPassword',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }

        return false;
    }

    public function updatePassword(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray($member, array('password', 'oldPassword'));
       
        $this->patch(
            $this->getResource().'/'.$member->getId().'/password',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }

        return false;
    }

    public function active(Member $member) : bool
    {
        $data = $this->getTranslator()->objectToArray($member, array('staff'));
        unset($data['data']['attributes']);
       
        $this->patch(
            $this->getResource().'/'.$member->getId().'/active',
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($member);
            return true;
        }

        return false;
    }
}
