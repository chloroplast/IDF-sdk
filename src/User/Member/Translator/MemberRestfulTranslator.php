<?php
namespace Sdk\User\Member\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Model\NullMember;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class MemberRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $member = null)
    {
        if (empty($expression)) {
            return NullMember::getInstance();
        }

        if ($member == null) {
            $member = new Member();
        }

        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($data['id'])) {
            $member->setId($data['id']);
        }
        if (isset($attributes['email'])) {
            $member->setEmail($attributes['email']);
        }
        if (isset($attributes['phone'])) {
            $member->setPhone($attributes['phone']);
        }
        if (isset($attributes['userName'])) {
            $member->setUserName($attributes['userName']);
        }
        if (isset($attributes['name'])) {
            $member->setUserName($attributes['name']);
        }
        if (isset($attributes['enterpriseName'])) {
            $member->setEnterpriseName($attributes['enterpriseName']);
        }
        if (isset($attributes['enterpriseAddress'])) {
            $member->setEnterpriseAddress($attributes['enterpriseAddress']);
        }
        if (isset($attributes['gradeIndex'])) {
            $member->setGradeIndex($attributes['gradeIndex']);
        }
        if (isset($attributes['gradeName'])) {
            $member->setGradeName($attributes['gradeName']);
        }
        if (isset($attributes['activeStatus'])) {
            $member->setActiveStatus($attributes['activeStatus']);
        }
        if (isset($attributes['status'])) {
            $member->setStatus($attributes['status']);
        }
        if (isset($attributes['statusTime'])) {
            $member->setStatusTime($attributes['statusTime']);
        }
        if (isset($attributes['createTime'])) {
            $member->setCreateTime($attributes['createTime']);
        }
        if (isset($attributes['updateTime'])) {
            $member->setUpdateTime($attributes['updateTime']);
        }

        return $member;
    }

    public function objectToArray($member, array $keys = array())
    {
        if (!$member instanceof Member) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'id',
                'identify',
                'email',
                'phone',
                'userName',
                'enterpriseName',
                'enterpriseAddress',
                'password',
                'oldPassword',
                'staff'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'members'
            )
        );

        if (in_array('id', $keys)) {
            $expression['data']['id'] = $member->getId();
        }

        $attributes = array();

        if (in_array('identify', $keys)) {
            $attributes['identify'] = $member->getIdentify();
        }
        if (in_array('email', $keys)) {
            $attributes['email'] = $member->getEmail();
        }
        if (in_array('phone', $keys)) {
            $attributes['phone'] = $member->getPhone();
        }
        if (in_array('userName', $keys)) {
            $attributes['userName'] = $member->getUserName();
        }
        if (in_array('enterpriseName', $keys)) {
            $attributes['enterpriseName'] = $member->getEnterpriseName();
        }
        if (in_array('enterpriseAddress', $keys)) {
            $attributes['enterpriseAddress'] = $member->getEnterpriseAddress();
        }
        if (in_array('password', $keys)) {
            $attributes['password'] = $member->getPassword();
        }
        if (in_array('oldPassword', $keys)) {
            $attributes['oldPassword'] = $member->getOldPassword();
        }
        
        $expression['data']['attributes'] = $attributes;

        if (in_array('staff', $keys)) {
            $staffRelationships = array(
                'type' => 'staff',
                'id' => strval(Core::$container->get('staff')->getId())
            );

            $expression['data']['relationships']['staff']['data'] = $staffRelationships;
        }

        return $expression;
    }
}
