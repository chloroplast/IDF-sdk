<?php
namespace Sdk\User\Member\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;
use Sdk\Common\Utils\Traits\DesensitizationTrait;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Model\NullMember;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
class MemberTranslator implements ITranslator
{
    use TranslatorTrait, DesensitizationTrait;

    protected function getNullObject() : INull
    {
        return NullMember::getInstance();
    }

    public function arrayToObject(array $expression, $member = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($member == null) {
            $member = new Member();
        }

        if (isset($expression['id'])) {
            $member->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['email'])) {
            $member->setEmail($expression['email']);
        }
        if (isset($expression['phone'])) {
            $member->setPhone($expression['phone']);
        }
        if (isset($expression['userName'])) {
            $member->setUserName($expression['userName']);
        }
        if (isset($expression['enterpriseName'])) {
            $member->setEnterpriseName($expression['enterpriseName']);
        }
        if (isset($expression['enterpriseAddress'])) {
            $member->setEnterpriseAddress($expression['enterpriseAddress']);
        }
        if (isset($expression['activeStatus']['id'])) {
            $member->setActiveStatus(marmot_decode($expression['activeStatus']['id']));
        }
        if (isset($expression['status']['id'])) {
            $member->setStatus(marmot_decode($expression['status']['id']));
        }
        if (isset($expression['createTime'])) {
            $member->setCreateTime($expression['createTime']);
        }
        if (isset($expression['updateTime'])) {
            $member->setUpdateTime($expression['updateTime']);
        }

        return $member;
    }

    public function objectToArray($member, array $keys = array())
    {
        if (!$member instanceof Member) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'email',
                'phone',
                'userName',
                'name',
                'enterpriseName',
                'enterpriseAddress',
                'gradeIndex',
                'gradeName',
                'activeStatus',
                'status',
                'createTime',
                'updateTime',
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($member->getId());
        }
        if (in_array('email', $keys)) {
            $expression['email'] = $member->getEmail();
        }
        if (in_array('phone', $keys)) {
            $expression['phone'] = $member->getPhone();
        }
        if (in_array('userName', $keys)) {
            $expression['userName'] = $member->getUserName();
        }
        if (in_array('name', $keys)) {
            $expression['name'] = $member->getUserName();
        }
        if (in_array('enterpriseName', $keys)) {
            $expression['enterpriseName'] = $member->getEnterpriseName();
        }
        if (in_array('enterpriseAddress', $keys)) {
            $expression['enterpriseAddress'] = $member->getEnterpriseAddress();
        }
        if (in_array('gradeIndex', $keys)) {
            $expression['gradeIndex'] = $member->getGradeIndex();
        }
        if (in_array('gradeName', $keys)) {
            $expression['gradeName'] = $member->getGradeName();
        }
        if (in_array('activeStatus', $keys)) {
            $expression['activeStatus'] = $this->statusFormatConversion(
                $member->getActiveStatus(),
                Member::ACTIVE_STATUS_TYPE,
                Member::ACTIVE_STATUS_CN
            );
        }
        if (in_array('status', $keys)) {
            $expression['status'] = $this->statusFormatConversion($member->getStatus());
        }
        if (in_array('createTime', $keys)) {
            $expression['createTime'] = $member->getCreateTime();
            $expression['createTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $member->getCreateTime());
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $member->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $member->getUpdateTime());
        }

        return $expression;
    }
}
