<?php
namespace Sdk\User\Member\Model;

use Marmot\Core;

use Sdk\User\Model\User;
use Sdk\User\Member\Repository\MemberRepository;

class Member extends User
{
    const ACTIVE_STATUS = array(
        'NO' => 0,
        'YES' => 1
    );

    const ACTIVE_STATUS_CN = array(
        self::ACTIVE_STATUS['NO'] => '未激活',
        self::ACTIVE_STATUS['YES'] => '已激活'
    );

    const ACTIVE_STATUS_TYPE = array(
        self::ACTIVE_STATUS['NO'] => 'danger',
        self::ACTIVE_STATUS['YES'] => 'success'
    );
    
    /**
     * @var string $identify 账户名称/邮箱
     */
    private $identify;
    /**
     * @var string $userName 账户名称
     */
    private $userName;
    /**
     * @var string $enterpriseName 企业名称
     */
    private $enterpriseName;
    /**
     * @var string $enterpriseAddress 企业地址
     */
    private $enterpriseAddress;
    /**
     * @var int $gradeIndex vip等级索引
     */
    private $gradeIndex;
    /**
     * @var string $gradeName vip等级名称
     */
    private $gradeName;
    /**
     * @var int $activeStatus 激活状态
     */
    private $activeStatus;

    private $repository;

    private $memberCookieAuth;

    private $memberJwtAuth;

    public function __construct(int $id = 0)
    {
        parent::__construct($id);
        $this->identify = '';
        $this->userName = '';
        $this->enterpriseName = '';
        $this->enterpriseAddress = '';
        $this->gradeIndex = 0;
        $this->gradeName = '';
        $this->activeStatus = self::ACTIVE_STATUS['NO'];
        $this->repository = new MemberRepository();
        $this->memberCookieAuth = new MemberCookieAuth();
        $this->memberJwtAuth = new MemberJwtAuth();
    }

    public function __destruct()
    {
        unset($this->identify);
        unset($this->userName);
        unset($this->enterpriseName);
        unset($this->enterpriseAddress);
        unset($this->gradeIndex);
        unset($this->gradeName);
        unset($this->activeStatus);
        unset($this->repository);
        unset($this->memberCookieAuth);
        unset($this->memberJwtAuth);
    }

    public function setIdentify(string $identify): void
    {
        $this->identify = $identify;
    }

    public function getIdentify(): string
    {
        return $this->identify;
    }

    public function setUserName(string $userName): void
    {
        $this->userName = $userName;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function setEnterpriseName(string $enterpriseName): void
    {
        $this->enterpriseName = $enterpriseName;
    }

    public function getEnterpriseName(): string
    {
        return $this->enterpriseName;
    }

    public function setEnterpriseAddress(string $enterpriseAddress): void
    {
        $this->enterpriseAddress = $enterpriseAddress;
    }

    public function getEnterpriseAddress(): string
    {
        return $this->enterpriseAddress;
    }

    public function setGradeIndex(int $gradeIndex): void
    {
        $this->gradeIndex = $gradeIndex;
    }

    public function getGradeIndex(): int
    {
        return $this->gradeIndex;
    }

    public function setGradeName(string $gradeName): void
    {
        $this->gradeName = $gradeName;
    }

    public function getGradeName(): string
    {
        return $this->gradeName;
    }

    public function setActiveStatus(int $activeStatus): void
    {
        $this->activeStatus = $activeStatus;
    }

    public function getActiveStatus(): int
    {
        return $this->activeStatus;
    }

    protected function getRepository() : MemberRepository
    {
        return $this->repository;
    }

    protected function getMemberCookieAuth() : MemberCookieAuth
    {
        return $this->memberCookieAuth;
    }

    protected function getMemberJwtAuth() : MemberJwtAuth
    {
        return $this->memberJwtAuth;
    }
    
    public function login() : bool
    {
        return $this->getRepository()->login($this)
            && $this->getMemberJwtAuth()->generateJwtAndSaveMemberToCache($this);
    }

    public function logout() : bool
    {
        return $this->getMemberJwtAuth()->clearJwtAndMemberToCache($this);
    }

    public function loginGateway() : bool
    {
        return $this->getRepository()->login($this)
            && $this->getMemberJwtAuth()->generateJwtAndSaveMemberToCache($this);
    }

    public function logoutGateway() : bool
    {
        return $this->getMemberJwtAuth()->clearJwtAndMemberToCache($this);
    }

    public function validatePassword() : bool
    {
        return $this->getRepository()->login($this);
    }

    public function active() : bool
    {
        if ($this->isActiveStatus()) {
            Core::setLastError(RESOURCE_CAN_NOT_MODIFY);
            return false;
        }

        return $this->getRepository()->active($this);
    }

    public function isActiveStatus() : bool
    {
        return $this->getActiveStatus() == self::ACTIVE_STATUS['YES'];
    }
}
