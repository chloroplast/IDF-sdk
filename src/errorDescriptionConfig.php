<?php

return [
    SLIDING_VERIFICATION_INCORRECT=>
    array(
        'id'=>SLIDING_VERIFICATION_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'SLIDING_VERIFICATION_INCORRECT',
        'title'=>'滑动验证失败，请刷新页面重新尝试',
        'detail'=>'滑动验证失败，请刷新页面重新尝试',
        'titleEn'=>'sliding verification incorrect',
        'detailEn'=>'sliding verification incorrect',
        'source'=>array(
            'pointer'=>'slidingVerification'
        ),
        'meta'=>array()
    ),
    FILE_NOT_EXIST=>
    array(
        'id'=>FILE_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_NOT_EXIST',
        'title'=>'文件不存在，请重新输入',
        'detail'=>'文件不存在，请重新输入',
        'titleEn'=>'file not exist',
        'detailEn'=>'file not exist',
        'source'=>array(
            'pointer'=>'file'
        ),
        'meta'=>array()
    ),
    FILE_EXTENSION_NOT_SUPPORTED=>
    array(
        'id'=>FILE_EXTENSION_NOT_SUPPORTED,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_EXTENSION_NOT_SUPPORTED',
        'title'=>'文件后缀不支持，请重新输入',
        'detail'=>'文件后缀不支持，请重新输入',
        'titleEn'=>'file extension not supported',
        'detailEn'=>'file extension not supported',
        'source'=>array(
            'pointer'=>'file'
        ),
        'meta'=>array()
    ),
    FILE_SIZE_LIMIT_EXCEEDED=>
    array(
        'id'=>FILE_SIZE_LIMIT_EXCEEDED,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_SIZE_LIMIT_EXCEEDED',
        'title'=>'文件大小超出限制，请重新输入',
        'detail'=>'文件大小超出限制，请重新输入',
        'titleEn'=>'file size limit exceeded',
        'detailEn'=>'file size limit exceeded',
        'source'=>array(
            'pointer'=>'file'
        ),
        'meta'=>array()
    ),
    FILE_UPLOAD_INCORRECT=>
    array(
        'id'=>FILE_UPLOAD_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_UPLOAD_INCORRECT',
        'title'=>'文件上传不正确，请重新输入',
        'detail'=>'文件上传不正确，请重新输入',
        'titleEn'=>'file upload incorrect',
        'detailEn'=>'file upload incorrect',
        'source'=>array(
            'pointer'=>'file'
        ),
        'meta'=>array()
    ),
    FILE_UPLOAD_PATH_NOT_EXIST=>
    array(
        'id'=>FILE_UPLOAD_PATH_NOT_EXIST,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_UPLOAD_PATH_NOT_EXIST',
        'title'=>'文件上传路径不存在，请重新输入',
        'detail'=>'文件上传路径不存在，请重新输入',
        'titleEn'=>'file upload path not exist',
        'detailEn'=>'file upload path not exist',
        'source'=>array(
            'pointer'=>'file'
        ),
        'meta'=>array()
    ),
    CSRF_VERIFICATION_FAILURE=>
    array(
        'id'=>CSRF_VERIFICATION_FAILURE,
        'link'=>'',
        'status'=>403,
        'code'=>'CSRF_VERIFICATION_FAILURE',
        'title'=>'页面信息失效，请刷新页面重新尝试',
        'detail'=>'页面信息失效，请刷新页面重新尝试',
        'titleEn'=>'csrf verification failure',
        'detailEn'=>'csrf verification failure',
        'source'=>array(
            'pointer'=>'csrf'
        ),
        'meta'=>array()
    ),
    PARAMETER_FORMAT_ERROR=>
    array(
        'id'=>PARAMETER_FORMAT_ERROR,
        'link'=>'',
        'status'=>403,
        'code'=>'PARAMETER_FORMAT_ERROR',
        'title'=>'参数格式不正确，请重新输入',
        'detail'=>'参数格式不正确，请重新输入',
        'titleEn'=>'parameter format error',
        'detailEn'=>'parameter format error',
        'source'=>array(
            'pointer'=>'parameter'
        ),
        'meta'=>array()
    ),
    PARAMETER_INCORRECT=>
    array(
        'id'=>PARAMETER_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PARAMETER_INCORRECT',
        'title'=>'参数不正确，请重新输入',
        'detail'=>'参数不正确，请重新输入',
        'titleEn'=>'parameter incorrect',
        'detailEn'=>'parameter incorrect',
        'source'=>array(
            'pointer'=>'parameter'
        ),
        'meta'=>array()
    ),
    PARAMETER_NOT_EMPTY=>
    array(
        'id'=>PARAMETER_NOT_EMPTY,
        'link'=>'',
        'status'=>403,
        'code'=>'PARAMETER_NOT_EMPTY',
        'title'=>'参数不能为空，请按照提示输入',
        'detail'=>'参数不能为空，请按照提示输入',
        'titleEn'=>'parameter not empty',
        'detailEn'=>'parameter not empty',
        'source'=>array(
            'pointer'=>'parameter'
        ),
        'meta'=>array()
    ),
    RESOURCE_CAN_NOT_MODIFY=>
    array(
        'id'=>RESOURCE_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'RESOURCE_CAN_NOT_MODIFY',
        'title'=>'资源不能被操作，请刷新页面重新尝试',
        'detail'=>'资源不能被操作，请刷新页面重新尝试',
        'titleEn'=>'resource can not modify',
        'detailEn'=>'resource can not modify',
        'source'=>array(
            'pointer'=>'resource'
        ),
        'meta'=>array()
    ),
    RESOURCE_EXISTS=>
    array(
        'id'=>RESOURCE_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'RESOURCE_EXISTS',
        'title'=>'资源已存在，请重新输入',
        'detail'=>'资源已存在，请重新输入',
        'titleEn'=>'resource exists',
        'detailEn'=>'resource exists',
        'source'=>array(
            'pointer'=>'resource'
        ),
        'meta'=>array()
    ),
    RESOURCE_NOT_EXISTS=>
    array(
        'id'=>RESOURCE_NOT_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'RESOURCE_NOT_EXISTS',
        'title'=>'资源不存在，请重新输入',
        'detail'=>'资源不存在，请重新输入',
        'titleEn'=>'resource not exists',
        'detailEn'=>'resource not exists',
        'source'=>array(
            'pointer'=>'resource'
        ),
        'meta'=>array()
    ),
    USER_NOT_LOGIN=>
    array(
        'id'=>USER_NOT_LOGIN,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_NOT_LOGIN',
        'title'=>'用户未登录，请登录后重新尝试',
        'detail'=>'用户未登录，请登录后重新尝试',
        'titleEn'=>'user not login',
        'detailEn'=>'user not login',
        'source'=>array(
            'pointer'=>'user'
        ),
        'meta'=>array()
    ),
    USER_STATUS_DISABLED=>
    array(
        'id'=>USER_STATUS_DISABLED,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_STATUS_DISABLED',
        'title'=>'该用户被禁用，不能对平台进行任何操作，如有疑问请联系平台管理员',
        'detail'=>'该用户被禁用，不能对平台进行任何操作，如有疑问请联系平台管理员',
        'titleEn'=>'user status disabled',
        'detailEn'=>'user status disabled',
        'source'=>array(
            'pointer'=>'user'
        ),
        'meta'=>array()
    ),
    USER_PURVIEW_UNASSIGNED=>
    array(
        'id'=>USER_PURVIEW_UNASSIGNED,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_PURVIEW_UNASSIGNED',
        'title'=>'该用户并未分配此权限，如有疑问请联系平台管理员',
        'detail'=>'该用户并未分配此权限，如有疑问请联系平台管理员',
        'titleEn'=>'user purview unassigned',
        'detailEn'=>'user purview unassigned',
        'source'=>array(
            'pointer'=>'user'
        ),
        'meta'=>array()
    ),
    USER_IDENTITY_AUTHENTICATION_FAILED=>
    array(
        'id'=>USER_IDENTITY_AUTHENTICATION_FAILED,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_IDENTITY_AUTHENTICATION_FAILED',
        'title'=>'用户身份认证失败，请刷新页面重新进行登录',
        'detail'=>'用户身份认证失败，请刷新页面重新进行登录',
        'titleEn'=>'user identity authentication failed',
        'detailEn'=>'user identity authentication failed',
        'source'=>array(
            'pointer'=>'user'
        ),
        'meta'=>array()
    ),
    STRATEGY_PRICE_NOT_FOUND=>
    array(
        'id'=>STRATEGY_PRICE_NOT_FOUND,
        'link'=>'',
        'status'=>403,
        'code'=>'STRATEGY_PRICE_NOT_FOUND',
        'title'=>'价格策略未匹配',
        'detail'=>'价格策略未匹配',
        'titleEn'=>'strategy price not found',
        'detailEn'=>'strategy price not found',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),

    EMAIL_FORMAT_INCORRECT=>
    array(
        'id'=>EMAIL_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'EMAIL_FORMAT_INCORRECT',
        'title'=>'邮箱格式不正确，请重新输入',
        'detail'=>'邮箱格式不正确，请重新输入',
        'titleEn'=>'email format incorrect',
        'detailEn'=>'email format incorrect',
        'source'=>array(
            'pointer'=>'email'
        ),
        'meta'=>array()
    ),
    EMAIL_EXISTS=>
    array(
        'id'=>EMAIL_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'EMAIL_EXISTS',
        'title'=>'邮箱已存在，请重新输入',
        'detail'=>'邮箱已存在，请重新输入',
        'titleEn'=>'email exists',
        'detailEn'=>'email exists',
        'source'=>array(
            'pointer'=>'email'
        ),
        'meta'=>array()
    ),
    PASSWORD_FORMAT_INCORRECT=>
    array(
        'id'=>PASSWORD_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PASSWORD_FORMAT_INCORRECT',
        'title'=>'密码格式不正确（支持大小写字母+数字+特殊字符(!@#$%.&)），请重新输入',
        'detail'=>'密码格式不正确（支持大小写字母+数字+特殊字符(!@#$%.&)），请重新输入',
        'titleEn'=>'password format incorrect(supports uppercase and lowercase letters, numbers, and special characters(!@#$%.&))',
        'detailEn'=>'password format incorrect(supports uppercase and lowercase letters, numbers, and special characters(!@#$%.&))',
        'source'=>array(
            'pointer'=>'password'
        ),
        'meta'=>array()
    ),
    CONFIRM_PASSWORD_INCORRECT=>
    array(
        'id'=>CONFIRM_PASSWORD_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CONFIRM_PASSWORD_INCORRECT',
        'title'=>'确认密码不正确，请重新输入',
        'detail'=>'确认密码不正确，请重新输入',
        'titleEn'=>'confirmPassword incorrect',
        'detailEn'=>'confirmPassword incorrect',
        'source'=>array(
            'pointer'=>'confirmPassword'
        ),
        'meta'=>array()
    ),
    OLD_PASSWORD_FORMAT_INCORRECT=>
    array(
        'id'=>OLD_PASSWORD_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'OLD_PASSWORD_FORMAT_INCORRECT',
        'title'=>'旧密码格式不正确（支持大小写字母+数字+特殊字符(!@#$%.&)），请重新输入',
        'detail'=>'旧密码格式不正确（支持大小写字母+数字+特殊字符(!@#$%.&)），请重新输入',
        'titleEn'=>'oldPassword format incorrect(supports uppercase and lowercase letters, numbers, and special characters(!@#$%.&))',
        'detailEn'=>'oldPassword format incorrect(supports uppercase and lowercase letters, numbers, and special characters(!@#$%.&))',
        'source'=>array(
            'pointer'=>'oldPassword'
        ),
        'meta'=>array()
    ),
    OLD_PASSWORD_INCORRECT=>
    array(
        'id'=>OLD_PASSWORD_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'OLD_PASSWORD_INCORRECT',
        'title'=>'旧密码不正确，请重新输入',
        'detail'=>'旧密码不正确，请重新输入',
        'titleEn'=>'oldPassword incorrect',
        'detailEn'=>'oldPassword incorrect',
        'source'=>array(
            'pointer'=>'oldPassword'
        ),
        'meta'=>array()
    ),
    NAME_FORMAT_INCORRECT=>
    array(
        'id'=>NAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'NAME_FORMAT_INCORRECT',
        'title'=>'姓名格式不正确，请重新输入',
        'detail'=>'姓名格式不正确，请重新输入',
        'titleEn'=>'name format incorrect',
        'detailEn'=>'name format incorrect',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    PHONE_FORMAT_INCORRECT=>
    array(
        'id'=>PHONE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PHONE_FORMAT_INCORRECT',
        'title'=>'电话格式不正确，请重新输入',
        'detail'=>'电话格式不正确，请重新输入',
        'titleEn'=>'phone format incorrect',
        'detailEn'=>'phone format incorrect',
        'source'=>array(
            'pointer'=>'phone'
        ),
        'meta'=>array()
    ),
    ACCOUNT_NOT_EXISTS=>
    array(
        'id'=>ACCOUNT_NOT_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'ACCOUNT_NOT_EXISTS',
        'title'=>'账号不存在，请重新输入',
        'detail'=>'账号不存在，请重新输入',
        'titleEn'=>'account not exists',
        'detailEn'=>'account not exists',
        'source'=>array(
            'pointer'=>'account'
        ),
        'meta'=>array()
    ),
    TITLE_FORMAT_INCORRECT=>
    array(
        'id'=>TITLE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'TITLE_FORMAT_INCORRECT',
        'title'=>'标题格式不正确，请重新输入',
        'detail'=>'标题格式不正确，请重新输入',
        'titleEn'=>'title format incorrect',
        'detailEn'=>'title format incorrect',
        'source'=>array(
            'pointer'=>'title'
        ),
        'meta'=>array()
    ),
    CONTENT_FORMAT_INCORRECT=>
    array(
        'id'=>CONTENT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CONTENT_FORMAT_INCORRECT',
        'title'=>'内容格式不正确，请重新输入',
        'detail'=>'内容格式不正确，请重新输入',
        'titleEn'=>'content format incorrect',
        'detailEn'=>'content format incorrect',
        'source'=>array(
            'pointer'=>'content'
        ),
        'meta'=>array()
    ),
    ENTERPRISE_NAME_FORMAT_INCORRECT=>
    array(
        'id'=>ENTERPRISE_NAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ENTERPRISE_NAME_FORMAT_INCORRECT',
        'title'=>'企业名称格式不正确，请重新输入',
        'detail'=>'企业名称格式不正确，请重新输入',
        'titleEn'=>'enterpriseName format incorrect',
        'detailEn'=>'enterpriseName format incorrect',
        'source'=>array(
            'pointer'=>'enterpriseName'
        ),
        'meta'=>array()
    ),
    ENTERPRISE_ADDRESS_FORMAT_INCORRECT=>
    array(
        'id'=>ENTERPRISE_ADDRESS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ENTERPRISE_ADDRESS_FORMAT_INCORRECT',
        'title'=>'企业地址格式不正确，请重新输入',
        'detail'=>'企业地址格式不正确，请重新输入',
        'titleEn'=>'enterpriseAddress format incorrect',
        'detailEn'=>'enterpriseAddress format incorrect',
        'source'=>array(
            'pointer'=>'enterpriseAddress'
        ),
        'meta'=>array()
    ),
    USER_NAME_FORMAT_INCORRECT=>
    array(
        'id'=>USER_NAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_NAME_FORMAT_INCORRECT',
        'title'=>'账户名称格式不正确，请重新输入',
        'detail'=>'账户名称格式不正确，请重新输入',
        'titleEn'=>'username format incorrect',
        'detailEn'=>'username format incorrect',
        'source'=>array(
            'pointer'=>'username'
        ),
        'meta'=>array()
    ),
    USER_NAME_EXISTS=>
    array(
        'id'=>USER_NAME_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'USER_NAME_EXISTS',
        'title'=>'账户名称已存在，请重新输入',
        'detail'=>'账户名称已存在，请重新输入',
        'titleEn'=>'username exists',
        'detailEn'=>'username exists',
        'source'=>array(
            'pointer'=>'username'
        ),
        'meta'=>array()
    ),
    CODE_INCORRECT=>
    array(
        'id'=>CODE_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CODE_INCORRECT',
        'title'=>'验证码不正确，请重新输入',
        'detail'=>'验证码不正确，请重新输入',
        'titleEn'=>'code incorrect',
        'detailEn'=>'code incorrect',
        'source'=>array(
            'pointer'=>'code'
        ),
        'meta'=>array()
    ),
    STATE_FORMAT_INCORRECT=>
    array(
        'id'=>STATE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'STATE_FORMAT_INCORRECT',
        'title'=>'州格式不正确，请重新输入',
        'detail'=>'州格式不正确，请重新输入',
        'titleEn'=>'state format incorrect',
        'detailEn'=>'state format incorrect',
        'source'=>array(
            'pointer'=>'state'
        ),
        'meta'=>array()
    ),
    CITY_FORMAT_INCORRECT=>
    array(
        'id'=>CITY_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CITY_FORMAT_INCORRECT',
        'title'=>'市格式不正确，请重新输入',
        'detail'=>'市格式不正确，请重新输入',
        'titleEn'=>'city format incorrect',
        'detailEn'=>'city format incorrect',
        'source'=>array(
            'pointer'=>'city'
        ),
        'meta'=>array()
    ),
    EMAIL_CODE_ALREADY_SENT=>
    array(
        'id'=>EMAIL_CODE_ALREADY_SENT,
        'link'=>'',
        'status'=>403,
        'code'=>'EMAIL_CODE_ALREADY_SENT',
        'title'=>'邮箱验证码已发送',
        'detail'=>'邮箱验证码已发送',
        'titleEn'=>'email code already sent',
        'detailEn'=>'email code already sent',
        'source'=>array(
            'pointer'=>'code'
        ),
        'meta'=>array()
    ),

    ROLE_NAME_FORMAT_INCORRECT=>
    array(
        'id'=>ROLE_NAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ROLE_NAME_FORMAT_INCORRECT',
        'title'=>'角色名称格式不正确，请重新输入',
        'detail'=>'角色名称格式不正确，请重新输入',
        'titleEn'=>'name format incorrect',
        'detailEn'=>'name format incorrect',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    ROLE_NAME_EXISTS=>
    array(
        'id'=>ROLE_NAME_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'ROLE_NAME_EXISTS',
        'title'=>'角色名称已存在，请重新输入',
        'detail'=>'角色名称已存在，请重新输入',
        'titleEn'=>'name exists',
        'detailEn'=>'name exists',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    ROLE_PURVIEW_FORMAT_INCORRECT=>
    array(
        'id'=>ROLE_PURVIEW_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ROLE_PURVIEW_FORMAT_INCORRECT',
        'title'=>'权限范围格式不正确，请重新输入',
        'detail'=>'权限范围格式不正确，请重新输入',
        'titleEn'=>'purview format incorrect',
        'detailEn'=>'purview format incorrect',
        'source'=>array(
            'pointer'=>'purview'
        ),
        'meta'=>array()
    ),

    STAFF_ROLES_FORMAT_INCORRECT=>
    array(
        'id'=>STAFF_ROLES_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'STAFF_ROLES_FORMAT_INCORRECT',
        'title'=>'角色格式不正确，请重新输入',
        'detail'=>'角色格式不正确，请重新输入',
        'titleEn'=>'roles format incorrect',
        'detailEn'=>'roles format incorrect',
        'source'=>array(
            'pointer'=>'roles'
        ),
        'meta'=>array()
    ),
    STAFF_ROLES_NOT_EXISTS=>
    array(
        'id'=>STAFF_ROLES_NOT_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'STAFF_ROLES_NOT_EXISTS',
        'title'=>'角色不存在，请重新输入',
        'detail'=>'角色不存在，请重新输入',
        'titleEn'=>'roles not exists',
        'detailEn'=>'roles not exists',
        'source'=>array(
            'pointer'=>'roles'
        ),
        'meta'=>array()
    ),

    DICTIONARY_NAME_FORMAT_INCORRECT=>
    array(
        'id'=>DICTIONARY_NAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DICTIONARY_NAME_FORMAT_INCORRECT',
        'title'=>'名称格式不正确，请重新输入',
        'detail'=>'名称格式不正确，请重新输入',
        'titleEn'=>'name format incorrect',
        'detailEn'=>'name format incorrect',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    DICTIONARY_NAME_EXISTS=>
    array(
        'id'=>DICTIONARY_NAME_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'DICTIONARY_NAME_EXISTS',
        'title'=>'名称已存在，请重新输入',
        'detail'=>'名称已存在，请重新输入',
        'titleEn'=>'name exists',
        'detailEn'=>'name exists',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    DICTIONARY_CATEGORY_NOT_EXISTS=>
    array(
        'id'=>DICTIONARY_CATEGORY_NOT_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'DICTIONARY_CATEGORY_NOT_EXISTS',
        'title'=>'所属分类不存在，请重新输入',
        'detail'=>'所属分类不存在，请重新输入',
        'titleEn'=>'category not exists',
        'detailEn'=>'category not exists',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    DICTIONARY_DESCRIPTION_FORMAT_INCORRECT=>
    array(
        'id'=>DICTIONARY_DESCRIPTION_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DICTIONARY_DESCRIPTION_FORMAT_INCORRECT',
        'title'=>'描述格式不正确，请重新输入',
        'detail'=>'描述格式不正确，请重新输入',
        'titleEn'=>'description format incorrect',
        'detailEn'=>'description format incorrect',
        'source'=>array(
            'pointer'=>'description'
        ),
        'meta'=>array()
    ),

    WAREHOUSE_CONTACT_FORMAT_INCORRECT=>
    array(
        'id'=>WAREHOUSE_CONTACT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WAREHOUSE_CONTACT_FORMAT_INCORRECT',
        'title'=>'仓库联系人格式不正确，请重新输入',
        'detail'=>'仓库联系人格式不正确，请重新输入',
        'titleEn'=>'contact format incorrect',
        'detailEn'=>'contact format incorrect',
        'source'=>array(
            'pointer'=>'contact'
        ),
        'meta'=>array()
    ),
    WAREHOUSE_KEEPER_FORMAT_INCORRECT=>
    array(
        'id'=>WAREHOUSE_KEEPER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WAREHOUSE_KEEPER_FORMAT_INCORRECT',
        'title'=>'仓库管理员格式不正确，请重新输入',
        'detail'=>'仓库管理员格式不正确，请重新输入',
        'titleEn'=>'keeper format incorrect',
        'detailEn'=>'keeper format incorrect',
        'source'=>array(
            'pointer'=>'keeper'
        ),
        'meta'=>array()
    ),
    WAREHOUSE_COUNTRY_FORMAT_INCORRECT=>
    array(
        'id'=>WAREHOUSE_COUNTRY_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WAREHOUSE_COUNTRY_FORMAT_INCORRECT',
        'title'=>'国家格式不正确，请重新输入',
        'detail'=>'国家格式不正确，请重新输入',
        'titleEn'=>'country format incorrect',
        'detailEn'=>'country format incorrect',
        'source'=>array(
            'pointer'=>'country'
        ),
        'meta'=>array()
    ),
    WAREHOUSE_CODE_FORMAT_INCORRECT=>
    array(
        'id'=>WAREHOUSE_CODE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WAREHOUSE_CODE_FORMAT_INCORRECT',
        'title'=>'仓库代码格式不正确，请重新输入',
        'detail'=>'仓库代码格式不正确，请重新输入',
        'titleEn'=>'code format incorrect',
        'detailEn'=>'code format incorrect',
        'source'=>array(
            'pointer'=>'code'
        ),
        'meta'=>array()
    ),
    WAREHOUSE_CODE_EXISTS=>
    array(
        'id'=>WAREHOUSE_CODE_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'WAREHOUSE_CODE_EXISTS',
        'title'=>'仓库代码已存在，请重新输入',
        'detail'=>'仓库代码已存在，请重新输入',
        'titleEn'=>'code exists',
        'detailEn'=>'code exists',
        'source'=>array(
            'pointer'=>'code'
        ),
        'meta'=>array()
    ),
    WAREHOUSE_NUMBERCODE_FORMAT_INCORRECT=>
    array(
        'id'=>WAREHOUSE_NUMBERCODE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WAREHOUSE_NUMBERCODE_FORMAT_INCORRECT',
        'title'=>'编号代码格式不正确，请重新输入',
        'detail'=>'编号代码格式不正确，请重新输入',
        'titleEn'=>'numberCode format incorrect',
        'detailEn'=>'numberCode format incorrect',
        'source'=>array(
            'pointer'=>'numberCode'
        ),
        'meta'=>array()
    ),
    WAREHOUSE_ADDRESS_FORMAT_INCORRECT=>
    array(
        'id'=>WAREHOUSE_ADDRESS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WAREHOUSE_ADDRESS_FORMAT_INCORRECT',
        'title'=>'仓库地址格式不正确，请重新输入',
        'detail'=>'仓库地址格式不正确，请重新输入',
        'titleEn'=>'address format incorrect',
        'detailEn'=>'address format incorrect',
        'source'=>array(
            'pointer'=>'address'
        ),
        'meta'=>array()
    ),
    WAREHOUSE_POSTALCODE_FORMAT_INCORRECT=>
    array(
        'id'=>WAREHOUSE_POSTALCODE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WAREHOUSE_POSTALCODE_FORMAT_INCORRECT',
        'title'=>'邮编格式不正确，请重新输入',
        'detail'=>'邮编格式不正确，请重新输入',
        'titleEn'=>'postalCode format incorrect',
        'detailEn'=>'postalCode format incorrect',
        'source'=>array(
            'pointer'=>'postalCode'
        ),
        'meta'=>array()
    ),

    EXPORT_TASK_SIZE_FORMAT_INCORRECT=>
    array(
        'id'=>EXPORT_TASK_SIZE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'EXPORT_TASK_SIZE_FORMAT_INCORRECT',
        'title'=>'导出数量格式不正确，请重新输入',
        'detail'=>'导出数量格式不正确，请重新输入',
        'titleEn'=>'size format incorrect',
        'detailEn'=>'size format incorrect',
        'source'=>array(
            'pointer'=>'size'
        ),
        'meta'=>array()
    ),
    EXPORT_TASK_OFFSET_FORMAT_INCORRECT=>
    array(
        'id'=>EXPORT_TASK_OFFSET_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'EXPORT_TASK_OFFSET_FORMAT_INCORRECT',
        'title'=>'起始数量格式不正确，请重新输入',
        'detail'=>'起始数量格式不正确，请重新输入',
        'titleEn'=>'offset format incorrect',
        'detailEn'=>'offset format incorrect',
        'source'=>array(
            'pointer'=>'offset'
        ),
        'meta'=>array()
    ),
    EXPORT_TASK_NAME_NOT_EXISTS=>
    array(
        'id'=>EXPORT_TASK_NAME_NOT_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'EXPORT_TASK_NAME_NOT_EXISTS',
        'title'=>'文件不存在，请重新输入',
        'detail'=>'文件不存在，请重新输入',
        'titleEn'=>'name not exists',
        'detailEn'=>'name not exists',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    EXPORT_TASK_SIZE_EXCEED_MAX_LINES_LIMIT=>
    array(
        'id'=>EXPORT_TASK_SIZE_EXCEED_MAX_LINES_LIMIT,
        'link'=>'',
        'status'=>403,
        'code'=>'EXPORT_TASK_SIZE_EXCEED_MAX_LINES_LIMIT',
        'title'=>'导出数量超过最大限制，请重新输入',
        'detail'=>'导出数量超过最大限制，请重新输入',
        'titleEn'=>'size exceed max lines limit',
        'detailEn'=>'size exceed max lines limit',
        'source'=>array(
            'pointer'=>'size'
        ),
        'meta'=>array()
    ),
    EXPORT_TASK_CATEGORY_NOT_EXISTS=>
    array(
        'id'=>EXPORT_TASK_CATEGORY_NOT_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'EXPORT_TASK_CATEGORY_NOT_EXISTS',
        'title'=>'导出类型不存在，请重新输入',
        'detail'=>'导出类型不存在，请重新输入',
        'titleEn'=>'category not exists',
        'detailEn'=>'category not exists',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    
    MESSAGE_PUBLISHER_NOT_EXISTS=>
    array(
        'id'=>MESSAGE_PUBLISHER_NOT_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'MESSAGE_PUBLISHER_NOT_EXISTS',
        'title'=>'发布人不存在，请重新输入',
        'detail'=>'发布人不存在，请重新输入',
        'titleEn'=>'publisher not exists',
        'detailEn'=>'publisher not exists',
        'source'=>array(
            'pointer'=>'publisher'
        ),
        'meta'=>array()
    ),
    MESSAGE_RECEIVER_NOT_EXISTS=>
    array(
        'id'=>MESSAGE_RECEIVER_NOT_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'MESSAGE_RECEIVER_NOT_EXISTS',
        'title'=>'接收人不存在，请重新输入',
        'detail'=>'接收人不存在，请重新输入',
        'titleEn'=>'receiver not exists',
        'detailEn'=>'receiver not exists',
        'source'=>array(
            'pointer'=>'receiver'
        ),
        'meta'=>array()
    ),
    
    VIP_NAME_FORMAT_INCORRECT=>
    array(
        'id'=>VIP_NAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'VIP_NAME_FORMAT_INCORRECT',
        'title'=>'名称格式不正确，请重新输入',
        'detail'=>'名称格式不正确，请重新输入',
        'titleEn'=>'name format incorrect',
        'detailEn'=>'name format incorrect',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    VIP_CONSUMER_MIN_PRICE_FORMAT_INCORRECT=>
    array(
        'id'=>VIP_CONSUMER_MIN_PRICE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'VIP_CONSUMER_MIN_PRICE_FORMAT_INCORRECT',
        'title'=>'消费额度下限格式不正确，请重新输入',
        'detail'=>'消费额度下限格式不正确，请重新输入',
        'titleEn'=>'consumerMinPrice format incorrect',
        'detailEn'=>'consumerMinPrice format incorrect',
        'source'=>array(
            'pointer'=>'consumerMinPrice'
        ),
        'meta'=>array()
    ),
    VIP_CONSUMER_MAX_PRICE_FORMAT_INCORRECT=>
    array(
        'id'=>VIP_CONSUMER_MAX_PRICE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'VIP_CONSUMER_MAX_PRICE_FORMAT_INCORRECT',
        'title'=>'消费额度上限格式不正确，请重新输入',
        'detail'=>'消费额度上限格式不正确，请重新输入',
        'titleEn'=>'consumerMaxPrice format incorrect',
        'detailEn'=>'consumerMaxPrice format incorrect',
        'source'=>array(
            'pointer'=>'consumerMaxPrice'
        ),
        'meta'=>array()
    ),
    VIP_INDICATORS_OVERLAPPING=>
    array(
        'id'=>VIP_INDICATORS_OVERLAPPING,
        'link'=>'',
        'status'=>403,
        'code'=>'VIP_INDICATORS_OVERLAPPING',
        'title'=>'vip消费额度有重叠，请重新输入',
        'detail'=>'vip消费额度有重叠，请重新输入',
        'titleEn'=>'indicators overlapping',
        'detailEn'=>'indicators overlapping',
        'source'=>array(
            'pointer'=>'indicators'
        ),
        'meta'=>array()
    ),
    VIP_INDICATORS_INCOHERENT=>
    array(
        'id'=>VIP_INDICATORS_INCOHERENT,
        'link'=>'',
        'status'=>403,
        'code'=>'VIP_INDICATORS_INCOHERENT',
        'title'=>'vip消费额度不连贯，请重新输入',
        'detail'=>'vip消费额度不连贯，请重新输入',
        'titleEn'=>'indicators incoherent',
        'detailEn'=>'indicators incoherent',
        'source'=>array(
            'pointer'=>'indicators'
        ),
        'meta'=>array()
    ),
    VIP_INDICATORS_FORMAT_INCORRECT=>
    array(
        'id'=>VIP_INDICATORS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'VIP_INDICATORS_FORMAT_INCORRECT',
        'title'=>'vip等级信息格式不正确，请重新输入',
        'detail'=>'vip等级信息格式不正确，请重新输入',
        'titleEn'=>'indicators format incorrect',
        'detailEn'=>'indicators format incorrect',
        'source'=>array(
            'pointer'=>'indicators'
        ),
        'meta'=>array()
    ),

    CARTYPE_NAME_FORMAT_INCORRECT=>
    array(
        'id'=>CARTYPE_NAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CARTYPE_NAME_FORMAT_INCORRECT',
        'title'=>'车辆类型格式不正确，请重新输入',
        'detail'=>'车辆类型格式不正确，请重新输入',
        'titleEn'=>'name format incorrect',
        'detailEn'=>'name format incorrect',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    CARTYPE_NAME_EXISTS=>
    array(
        'id'=>CARTYPE_NAME_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'CARTYPE_NAME_EXISTS',
        'title'=>'车辆类型已存在，请重新输入',
        'detail'=>'车辆类型已存在，请重新输入',
        'titleEn'=>'name exists',
        'detailEn'=>'name exists',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    CARTYPE_PALLET_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>CARTYPE_PALLET_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CARTYPE_PALLET_NUMBER_FORMAT_INCORRECT',
        'title'=>'板数上限格式不正确，请重新输入',
        'detail'=>'板数上限格式不正确，请重新输入',
        'titleEn'=>'palletNumber format incorrect',
        'detailEn'=>'palletNumber format incorrect',
        'source'=>array(
            'pointer'=>'palletNumber'
        ),
        'meta'=>array()
    ),
    CARTYPE_WEIGHT_FORMAT_INCORRECT=>
    array(
        'id'=>CARTYPE_WEIGHT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CARTYPE_WEIGHT_FORMAT_INCORRECT',
        'title'=>'载重上限格式不正确，请重新输入',
        'detail'=>'载重上限格式不正确，请重新输入',
        'titleEn'=>'weight format incorrect',
        'detailEn'=>'weight format incorrect',
        'source'=>array(
            'pointer'=>'weight'
        ),
        'meta'=>array()
    ),

    POSITION_FORMAT_INCORRECT=>
    array(
        'id'=>POSITION_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'POSITION_FORMAT_INCORRECT',
        'title'=>'货物位置格式不正确，请重新输入',
        'detail'=>'货物位置格式不正确，请重新输入',
        'titleEn'=>'position format incorrect',
        'detailEn'=>'position format incorrect',
        'source'=>array(
            'pointer'=>'position'
        ),
        'meta'=>array()
    ),
    ATTACHMENTS_FORMAT_INCORRECT=>
    array(
        'id'=>ATTACHMENTS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ATTACHMENTS_FORMAT_INCORRECT',
        'title'=>'佐证材料格式不正确，请重新输入',
        'detail'=>'佐证材料格式不正确，请重新输入',
        'titleEn'=>'attachments format incorrect',
        'detailEn'=>'attachments format incorrect',
        'source'=>array(
            'pointer'=>'attachments'
        ),
        'meta'=>array()
    ),
    ISA_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>ISA_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ISA_NUMBER_FORMAT_INCORRECT',
        'title'=>'ISA编号格式不正确，请重新输入',
        'detail'=>'ISA编号格式不正确，请重新输入',
        'titleEn'=>'isaNumber format incorrect',
        'detailEn'=>'isaNumber format incorrect',
        'source'=>array(
            'pointer'=>'isaNumber'
        ),
        'meta'=>array()
    ),
    PICKUP_WAREHOUSE_FORMAT_INCORRECT=>
    array(
        'id'=>PICKUP_WAREHOUSE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUP_WAREHOUSE_FORMAT_INCORRECT',
        'title'=>'取货仓库格式不正确，请重新输入',
        'detail'=>'取货仓库格式不正确，请重新输入',
        'titleEn'=>'pickupWarehouse format incorrect',
        'detailEn'=>'pickupWarehouse format incorrect',
        'source'=>array(
            'pointer'=>'pickupWarehouse'
        ),
        'meta'=>array()
    ),
    ADDRESS_FORMAT_INCORRECT=>
    array(
        'id'=>ADDRESS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ADDRESS_FORMAT_INCORRECT',
        'title'=>'地址信息格式不正确，请重新输入',
        'detail'=>'地址信息格式不正确，请重新输入',
        'titleEn'=>'address format incorrect',
        'detailEn'=>'address format incorrect',
        'source'=>array(
            'pointer'=>'address'
        ),
        'meta'=>array()
    ),
    REFERENCES_FORMAT_INCORRECT=>
    array(
        'id'=>REFERENCES_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'REFERENCES_FORMAT_INCORRECT',
        'title'=>'发货备注格式不正确，请重新输入',
        'detail'=>'发货备注格式不正确，请重新输入',
        'titleEn'=>'references format incorrect',
        'detailEn'=>'references format incorrect',
        'source'=>array(
            'pointer'=>'references'
        ),
        'meta'=>array()
    ),
    IDF_PICKUP_FORMAT_INCORRECT=>
    array(
        'id'=>IDF_PICKUP_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'IDF_PICKUP_FORMAT_INCORRECT',
        'title'=>'是否IDF取货格式不正确，请重新输入',
        'detail'=>'是否IDF取货格式不正确，请重新输入',
        'titleEn'=>'idfPickup format incorrect',
        'detailEn'=>'idfPickup format incorrect',
        'source'=>array(
            'pointer'=>'idfPickup'
        ),
        'meta'=>array()
    ),
    WAREHOUSE_FORMAT_INCORRECT=>
    array(
        'id'=>WAREHOUSE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WAREHOUSE_FORMAT_INCORRECT',
        'title'=>'目标仓库格式不正确，请重新输入',
        'detail'=>'目标仓库格式不正确，请重新输入',
        'titleEn'=>'warehouse format incorrect',
        'detailEn'=>'warehouse format incorrect',
        'source'=>array(
            'pointer'=>'warehouse'
        ),
        'meta'=>array()
    ),
    PALLET_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>PALLET_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PALLET_NUMBER_FORMAT_INCORRECT',
        'title'=>'卡板数格式不正确，请重新输入',
        'detail'=>'卡板数格式不正确，请重新输入',
        'titleEn'=>'palletNumber format incorrect',
        'detailEn'=>'palletNumber format incorrect',
        'source'=>array(
            'pointer'=>'palletNumber'
        ),
        'meta'=>array()
    ),
    ITEM_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>ITEM_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ITEM_NUMBER_FORMAT_INCORRECT',
        'title'=>'件数格式不正确，请重新输入',
        'detail'=>'件数格式不正确，请重新输入',
        'titleEn'=>'itemNumber format incorrect',
        'detailEn'=>'itemNumber format incorrect',
        'source'=>array(
            'pointer'=>'itemNumber'
        ),
        'meta'=>array()
    ),
    WEIGHT_FORMAT_INCORRECT=>
    array(
        'id'=>WEIGHT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WEIGHT_FORMAT_INCORRECT',
        'title'=>'重量格式不正确，请重新输入',
        'detail'=>'重量格式不正确，请重新输入',
        'titleEn'=>'weight format incorrect',
        'detailEn'=>'weight format incorrect',
        'source'=>array(
            'pointer'=>'weight'
        ),
        'meta'=>array()
    ),
    WEIGHT_UNIT_FORMAT_INCORRECT=>
    array(
        'id'=>WEIGHT_UNIT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WEIGHT_UNIT_FORMAT_INCORRECT',
        'title'=>'重量单位格式不正确，请重新输入',
        'detail'=>'重量单位格式不正确，请重新输入',
        'titleEn'=>'weightUnit format incorrect',
        'detailEn'=>'weightUnit format incorrect',
        'source'=>array(
            'pointer'=>'weightUnit'
        ),
        'meta'=>array()
    ),
    FBA_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>FBA_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'FBA_NUMBER_FORMAT_INCORRECT',
        'title'=>'FBA编号格式不正确，请重新输入',
        'detail'=>'FBA编号格式不正确，请重新输入',
        'titleEn'=>'fbaNumber format incorrect',
        'detailEn'=>'fbaNumber format incorrect',
        'source'=>array(
            'pointer'=>'fbaNumber'
        ),
        'meta'=>array()
    ),
    PO_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>PO_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PO_NUMBER_FORMAT_INCORRECT',
        'title'=>'PO编号格式不正确，请重新输入',
        'detail'=>'PO编号格式不正确，请重新输入',
        'titleEn'=>'poNumber format incorrect',
        'detailEn'=>'poNumber format incorrect',
        'source'=>array(
            'pointer'=>'poNumber'
        ),
        'meta'=>array()
    ),
    SO_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>SO_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'SO_NUMBER_FORMAT_INCORRECT',
        'title'=>'SO编号格式不正确，请重新输入',
        'detail'=>'SO编号格式不正确，请重新输入',
        'titleEn'=>'soNumber format incorrect',
        'detailEn'=>'soNumber format incorrect',
        'source'=>array(
            'pointer'=>'soNumber'
        ),
        'meta'=>array()
    ),
    CO_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>CO_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CO_NUMBER_FORMAT_INCORRECT',
        'title'=>'CO编号格式不正确，请重新输入',
        'detail'=>'CO编号格式不正确，请重新输入',
        'titleEn'=>'coNumber format incorrect',
        'detailEn'=>'coNumber format incorrect',
        'source'=>array(
            'pointer'=>'coNumber'
        ),
        'meta'=>array()
    ),
    PICKUP_DATE_FORMAT_INCORRECT=>
    array(
        'id'=>PICKUP_DATE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUP_DATE_FORMAT_INCORRECT',
        'title'=>'取货日期格式不正确，请重新输入',
        'detail'=>'取货日期格式不正确，请重新输入',
        'titleEn'=>'pickupDate format incorrect',
        'detailEn'=>'pickupDate format incorrect',
        'source'=>array(
            'pointer'=>'pickupDate'
        ),
        'meta'=>array()
    ),
    READY_TIME_FORMAT_INCORRECT=>
    array(
        'id'=>READY_TIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'READY_TIME_FORMAT_INCORRECT',
        'title'=>'取货时间格式不正确，请重新输入',
        'detail'=>'取货时间格式不正确，请重新输入',
        'titleEn'=>'readyTime format incorrect',
        'detailEn'=>'readyTime format incorrect',
        'source'=>array(
            'pointer'=>'readyTime'
        ),
        'meta'=>array()
    ),
    CLOSE_TIME_FORMAT_INCORRECT=>
    array(
        'id'=>CLOSE_TIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CLOSE_TIME_FORMAT_INCORRECT',
        'title'=>'下班时间格式不正确，请重新输入',
        'detail'=>'下班时间格式不正确，请重新输入',
        'titleEn'=>'closeTime format incorrect',
        'detailEn'=>'closeTime format incorrect',
        'source'=>array(
            'pointer'=>'closeTime'
        ),
        'meta'=>array()
    ),
    PICKUP_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>PICKUP_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUP_NUMBER_FORMAT_INCORRECT',
        'title'=>'取货码格式不正确，请重新输入',
        'detail'=>'取货码格式不正确，请重新输入',
        'titleEn'=>'pickupNumber format incorrect',
        'detailEn'=>'pickupNumber format incorrect',
        'source'=>array(
            'pointer'=>'pickupNumber'
        ),
        'meta'=>array()
    ),
    REMARK_FORMAT_INCORRECT=>
    array(
        'id'=>REMARK_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'REMARK_FORMAT_INCORRECT',
        'title'=>'备注信息格式不正确，请重新输入',
        'detail'=>'备注信息格式不正确，请重新输入',
        'titleEn'=>'remark format incorrect',
        'detailEn'=>'remark format incorrect',
        'source'=>array(
            'pointer'=>'remark'
        ),
        'meta'=>array()
    ),
    ITEMS_FORMAT_INCORRECT=>
    array(
        'id'=>ITEMS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ITEMS_FORMAT_INCORRECT',
        'title'=>'货物信息格式不正确，请重新输入',
        'detail'=>'货物信息格式不正确，请重新输入',
        'titleEn'=>'items format incorrect',
        'detailEn'=>'items format incorrect',
        'source'=>array(
            'pointer'=>'items'
        ),
        'meta'=>array()
    ),
    STOCKIN_NUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>STOCKIN_NUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'STOCKIN_NUMBER_FORMAT_INCORRECT',
        'title'=>'入库编号格式不正确，请重新输入',
        'detail'=>'入库编号格式不正确，请重新输入',
        'titleEn'=>'stockInNumber format incorrect',
        'detailEn'=>'stockInNumber format incorrect',
        'source'=>array(
            'pointer'=>'stockInNumber'
        ),
        'meta'=>array()
    ),
    STOCKIN_TIME_FORMAT_INCORRECT=>
    array(
        'id'=>STOCKIN_TIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'STOCKIN_TIME_FORMAT_INCORRECT',
        'title'=>'入库时间格式不正确，请重新输入',
        'detail'=>'入库时间格式不正确，请重新输入',
        'titleEn'=>'stockInTime format incorrect',
        'detailEn'=>'stockInTime format incorrect',
        'source'=>array(
            'pointer'=>'stockInTime'
        ),
        'meta'=>array()
    ),
    CARTYPE_FORMAT_INCORRECT=>
    array(
        'id'=>CARTYPE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CARTYPE_FORMAT_INCORRECT',
        'title'=>'车辆类型格式不正确，请重新输入',
        'detail'=>'车辆类型格式不正确，请重新输入',
        'titleEn'=>'carType format incorrect',
        'detailEn'=>'carType format incorrect',
        'source'=>array(
            'pointer'=>'carType'
        ),
        'meta'=>array()
    ),
    ITEMS_ATTACHMENTS_FORMAT_INCORRECT=>
    array(
        'id'=>ITEMS_ATTACHMENTS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ITEMS_ATTACHMENTS_FORMAT_INCORRECT',
        'title'=>'散板订单佐证材料格式不正确，请重新输入',
        'detail'=>'散板订单佐证材料格式不正确，请重新输入',
        'titleEn'=>'itemsAttachments format incorrect',
        'detailEn'=>'itemsAttachments format incorrect',
        'source'=>array(
            'pointer'=>'itemsAttachments'
        ),
        'meta'=>array()
    ),
    AMAZONLTL_FORMAT_INCORRECT=>
    array(
        'id'=>AMAZONLTL_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONLTL_FORMAT_INCORRECT',
        'title'=>'散板订单组合格式不正确，请重新输入',
        'detail'=>'散板订单组合格式不正确，请重新输入',
        'titleEn'=>'amazonLTL format incorrect',
        'detailEn'=>'amazonLTL format incorrect',
        'source'=>array(
            'pointer'=>'amazonLTL'
        ),
        'meta'=>array()
    ),
    LENGTH_FORMAT_INCORRECT=>
    array(
        'id'=>LENGTH_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'LENGTH_FORMAT_INCORRECT',
        'title'=>'长度格式不正确，请重新输入',
        'detail'=>'长度格式不正确，请重新输入',
        'titleEn'=>'length format incorrect',
        'detailEn'=>'length format incorrect',
        'source'=>array(
            'pointer'=>'length'
        ),
        'meta'=>array()
    ),
    WIDTH_FORMAT_INCORRECT=>
    array(
        'id'=>WIDTH_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'WIDTH_FORMAT_INCORRECT',
        'title'=>'宽度格式不正确，请重新输入',
        'detail'=>'宽度格式不正确，请重新输入',
        'titleEn'=>'width format incorrect',
        'detailEn'=>'width format incorrect',
        'source'=>array(
            'pointer'=>'width'
        ),
        'meta'=>array()
    ),
    HEIGHT_FORMAT_INCORRECT=>
    array(
        'id'=>HEIGHT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'HEIGHT_FORMAT_INCORRECT',
        'title'=>'高度不正确，请重新输入',
        'detail'=>'高度不正确，请重新输入',
        'titleEn'=>'height format incorrect',
        'detailEn'=>'height format incorrect',
        'source'=>array(
            'pointer'=>'height'
        ),
        'meta'=>array()
    ),
    DELIVERY_DATE_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERY_DATE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERY_DATE_FORMAT_INCORRECT',
        'title'=>'送达日期格式不正确，请重新输入',
        'detail'=>'送达日期格式不正确，请重新输入',
        'titleEn'=>'deliveryDate format incorrect',
        'detailEn'=>'deliveryDate format incorrect',
        'source'=>array(
            'pointer'=>'deliveryDate'
        ),
        'meta'=>array()
    ),
    CLOSE_PAGE_FORMAT_INCORRECT=>
    array(
        'id'=>CLOSE_PAGE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CLOSE_PAGE_FORMAT_INCORRECT',
        'title'=>'亚马逊凭证格式不正确，请重新输入',
        'detail'=>'亚马逊凭证格式不正确，请重新输入',
        'titleEn'=>'closePage format incorrect',
        'detailEn'=>'closePage format incorrect',
        'source'=>array(
            'pointer'=>'closePage'
        ),
        'meta'=>array()
    ),
    FILE_NAME_FORMAT_INCORRECT=>
    array(
        'id'=>FILE_NAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_NAME_FORMAT_INCORRECT',
        'title'=>'文件名格式不正确，请重新输入',
        'detail'=>'文件名格式不正确，请重新输入',
        'titleEn'=>'fileName format incorrect',
        'detailEn'=>'fileName format incorrect',
        'source'=>array(
            'pointer'=>'fileName'
        ),
        'meta'=>array()
    ),
    AMAZONLTL_CAN_NOT_MODIFY=>
    array(
        'id'=>AMAZONLTL_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONLTL_CAN_NOT_MODIFY',
        'title'=>'订单不能操作(订单状态不对)',
        'detail'=>'订单不能操作(订单状态不对)',
        'titleEn'=>'amazonLTL can not modify',
        'detailEn'=>'amazonLTL can not modify',
        'source'=>array(
            'pointer'=>'amazonLTL'
        ),
        'meta'=>array()
    ),
    PICKUPWAREHOUSE_NOT_EXISTS=>
    array(
        'id'=>PICKUPWAREHOUSE_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'PICKUPWAREHOUSE_NOT_EXISTS',
        'title'=>'取货仓库不存在',
        'detail'=>'取货仓库不存在',
        'titleEn'=>'pickupWarehouse not exists',
        'detailEn'=>'pickupWarehouse not exists',
        'source'=>array(
            'pointer'=>'pickupWarehouse'
        ),
        'meta'=>array()
    ),
    ADDRESS_NOT_EXISTS=>
    array(
        'id'=>ADDRESS_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'ADDRESS_NOT_EXISTS',
        'title'=>'地址不存在',
        'detail'=>'地址不存在',
        'titleEn'=>'address not exists',
        'detailEn'=>'address not exists',
        'source'=>array(
            'pointer'=>'address'
        ),
        'meta'=>array()
    ),
    ITEMS_WAREHOUSE_NOT_EXISTS=>
    array(
        'id'=>ITEMS_WAREHOUSE_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'ITEMS_WAREHOUSE_NOT_EXISTS',
        'title'=>'目标仓库不存在',
        'detail'=>'目标仓库不存在',
        'titleEn'=>'items warehouse not exists',
        'detailEn'=>'items warehouse not exists',
        'source'=>array(
            'pointer'=>'items.warehouse'
        ),
        'meta'=>array()
    ),
    AMAZONLTLPOLICY_NOT_EXISTS=>
    array(
        'id'=>AMAZONLTLPOLICY_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'AMAZONLTLPOLICY_NOT_EXISTS',
        'title'=>'散板价格策略不存在',
        'detail'=>'散板价格策略不存在',
        'titleEn'=>'amazonLTLPolicy not exists',
        'detailEn'=>'amazonLTLPolicy not exists',
        'source'=>array(
            'pointer'=>'amazonLTLPolicy'
        ),
        'meta'=>array()
    ),
    AMAZONLTL_HAVE_BEEN_FROZEN=>
    array(
        'id'=>AMAZONLTL_HAVE_BEEN_FROZEN,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONLTL_HAVE_BEEN_FROZEN',
        'title'=>'订单被冻结',
        'detail'=>'订单被冻结',
        'titleEn'=>'amazonLTL have been frozen',
        'detailEn'=>'amazonLTL have been frozen',
        'source'=>array(
            'pointer'=>'amazonLTL'
        ),
        'meta'=>array()
    ),
    FILE_PARSING_FAILED=>
    array(
        'id'=>FILE_PARSING_FAILED,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_PARSING_FAILED',
        'title'=>'文件解析失败',
        'detail'=>'文件解析失败',
        'titleEn'=>'file parsing failed',
        'detailEn'=>'file parsing failed',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    THE_NUMBER_OF_PARSES_EXCEEDS_THE_LIMIT=>
    array(
        'id'=>THE_NUMBER_OF_PARSES_EXCEEDS_THE_LIMIT,
        'link'=>'',
        'status'=>403,
        'code'=>'THE_NUMBER_OF_PARSES_EXCEEDS_THE_LIMIT',
        'title'=>'解析数量超过限制(目前限制为100条)',
        'detail'=>'解析数量超过限制(目前限制为100条)',
        'titleEn'=>'the number of parses exceeds the limit',
        'detailEn'=>'the number of parses exceeds the limit',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    FILE_PARSING_RETURNED_NULL=>
    array(
        'id'=>FILE_PARSING_RETURNED_NULL,
        'link'=>'',
        'status'=>403,
        'code'=>'FILE_PARSING_RETURNED_NULL',
        'title'=>'文件解析返回空',
        'detail'=>'文件解析返回空',
        'titleEn'=>'file parsing returned null',
        'detailEn'=>'file parsing returned null',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    RECEIVEAMAZONORDER_CAN_NOT_MODIFY=>
    array(
        'id'=>RECEIVEAMAZONORDER_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'RECEIVEAMAZONORDER_CAN_NOT_MODIFY',
        'title'=>'取货订单不能操作(状态不对)',
        'detail'=>'取货订单不能操作(状态不对)',
        'titleEn'=>'receiveAmazonOrder can not modify',
        'detailEn'=>'receiveAmazonOrder can not modify',
        'source'=>array(
            'pointer'=>'receiveAmazonOrder'
        ),
        'meta'=>array()
    ),
    CARTYPE_NOT_EXISTS=>
    array(
        'id'=>CARTYPE_NOT_EXISTS,
        'link'=>'',
        'status'=>403,
        'code'=>'CARTYPE_NOT_EXISTS',
        'title'=>'车辆类型不存在',
        'detail'=>'车辆类型不存在',
        'titleEn'=>'carType not exists',
        'detailEn'=>'carType not exists',
        'source'=>array(
            'pointer'=>'carType'
        ),
        'meta'=>array()
    ),
    CARTYPE_PALLET_EXCEEDS_LIMIT=>
    array(
        'id'=>CARTYPE_PALLET_EXCEEDS_LIMIT,
        'link'=>'',
        'status'=>403,
        'code'=>'CARTYPE_PALLET_EXCEEDS_LIMIT',
        'title'=>'车辆类型板数超过限制',
        'detail'=>'车辆类型板数超过限制',
        'titleEn'=>'carType pallet exceeds limit',
        'detailEn'=>'carType pallet exceeds limit',
        'source'=>array(
            'pointer'=>'carType'
        ),
        'meta'=>array()
    ),
    CARTYPE_WEIGHT_EXCEEDS_LIMIT=>
    array(
        'id'=>CARTYPE_WEIGHT_EXCEEDS_LIMIT,
        'link'=>'',
        'status'=>403,
        'code'=>'CARTYPE_WEIGHT_EXCEEDS_LIMIT',
        'title'=>'车辆类型重量超过限制',
        'detail'=>'车辆类型重量超过限制',
        'titleEn'=>'carType weight exceeds limit',
        'detailEn'=>'carType weight exceeds limit',
        'source'=>array(
            'pointer'=>'carType'
        ),
        'meta'=>array()
    ),
    AMAZONLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS=>
    array(
        'id'=>AMAZONLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS',
        'title'=>'散板订单已被添加到其他组单内',
        'detail'=>'散板订单已被添加到其他组单内',
        'titleEn'=>'amazonLTL has been added to other group orders',
        'detailEn'=>'amazonLTL has been added to other group orders',
        'source'=>array(
            'pointer'=>'amazonLTL'
        ),
        'meta'=>array()
    ),
    RECORD_OF_ORDER_INCOMPLETE_PICKUP_TIME=>
    array(
        'id'=>RECORD_OF_ORDER_INCOMPLETE_PICKUP_TIME,
        'link'=>'',
        'status'=>403,
        'code'=>'RECORD_OF_ORDER_INCOMPLETE_PICKUP_TIME',
        'title'=>'订单未完成取货时间记录',
        'detail'=>'订单未完成取货时间记录',
        'titleEn'=>'record of order incomplete pickup time',
        'detailEn'=>'record of order incomplete pickup time',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),
    AMAZONLTL_CAN_NOT_REGISTER_POSITION=>
    array(
        'id'=>AMAZONLTL_CAN_NOT_REGISTER_POSITION,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONLTL_CAN_NOT_REGISTER_POSITION',
        'title'=>'散板订单未入库登记位置',
        'detail'=>'散板订单未入库登记位置',
        'titleEn'=>'amazonLTL can not register position',
        'detailEn'=>'amazonLTL can not register position',
        'source'=>array(
            'pointer'=>'amazonLTL'
        ),
        'meta'=>array()
    ),
    ORDER_INCOMPLETE_DELIVERY_TIME_RECORD=>
    array(
        'id'=>ORDER_INCOMPLETE_DELIVERY_TIME_RECORD,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDER_INCOMPLETE_DELIVERY_TIME_RECORD',
        'title'=>'订单未完成发货时间记录',
        'detail'=>'订单未完成发货时间记录',
        'titleEn'=>'order incomplete delivery time record',
        'detailEn'=>'order incomplete delivery time record',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),
    ORDER_NOT_UPDATED_ISA=>
    array(
        'id'=>ORDER_NOT_UPDATED_ISA,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDER_NOT_UPDATED_ISA',
        'title'=>'订单未更新ISA',
        'detail'=>'订单未更新ISA',
        'titleEn'=>'order not updated ISA',
        'detailEn'=>'order not updated ISA',
        'source'=>array(
            'pointer'=>'dispatchAmazonOrder'
        ),
        'meta'=>array()
    ),
    DISPATCHAMAZONORDER_CAN_NOT_MODIFY=>
    array(
        'id'=>DISPATCHAMAZONORDER_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'DISPATCHAMAZONORDER_CAN_NOT_MODIFY',
        'title'=>'发货组单订单不能操作(状态不对)',
        'detail'=>'发货组单订单不能操作(状态不对)',
        'titleEn'=>'dispatchAmazonOrder can not modify',
        'detailEn'=>'dispatchAmazonOrder can not modify',
        'source'=>array(
            'pointer'=>'dispatchAmazonOrder'
        ),
        'meta'=>array()
    ),
    AMAZONLTL_NOT_UPDATED_ISA=>
    array(
        'id'=>AMAZONLTL_NOT_UPDATED_ISA,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONLTL_NOT_UPDATED_ISA',
        'title'=>'散板订单未更新ISA',
        'detail'=>'散板订单未更新ISA',
        'titleEn'=>'amazonLTL not updated ISA',
        'detailEn'=>'amazonLTL not updated ISA',
        'source'=>array(
            'pointer'=>'amazonLTL'
        ),
        'meta'=>array()
    ),
    GOODSTYPE_FORMAT_INCORRECT=>
    array(
        'id'=>GOODSTYPE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'GOODSTYPE_FORMAT_INCORRECT',
        'title'=>'货物类型格式不正确，请重新输入',
        'detail'=>'货物类型格式不正确，请重新输入',
        'titleEn'=>'goodsType format incorrect',
        'detailEn'=>'goodsType format incorrect',
        'source'=>array(
            'pointer'=>'goodsType'
        ),
        'meta'=>array()
    ),
    GOODSVALUE_FORMAT_INCORRECT=>
    array(
        'id'=>GOODSVALUE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'GOODSVALUE_FORMAT_INCORRECT',
        'title'=>'货物价值格式不正确，请重新输入',
        'detail'=>'货物价值格式不正确，请重新输入',
        'titleEn'=>'goodsValue format incorrect',
        'detailEn'=>'goodsValue format incorrect',
        'source'=>array(
            'pointer'=>'goodsValue'
        ),
        'meta'=>array()
    ),
    PACKINGTYPE_FORMAT_INCORRECT=>
    array(
        'id'=>PACKINGTYPE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PACKINGTYPE_FORMAT_INCORRECT',
        'title'=>'包装类型格式不正确，请重新输入',
        'detail'=>'包装类型格式不正确，请重新输入',
        'titleEn'=>'packingType format incorrect',
        'detailEn'=>'packingType format incorrect',
        'source'=>array(
            'pointer'=>'packingType'
        ),
        'meta'=>array()
    ),
    AMAZONFTL_CAN_NOT_MODIFY=>
    array(
        'id'=>AMAZONFTL_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONFTL_CAN_NOT_MODIFY',
        'title'=>'整板订单不能操作(订单状态不对)',
        'detail'=>'整板订单不能操作(订单状态不对)',
        'titleEn'=>'amazonFTL can not modify',
        'detailEn'=>'amazonFTL can not modify',
        'source'=>array(
            'pointer'=>'amazonFTL'
        ),
        'meta'=>array()
    ),
    AMAZONFTL_HAVE_BEEN_FROZEN=>
    array(
        'id'=>AMAZONFTL_HAVE_BEEN_FROZEN,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONFTL_HAVE_BEEN_FROZEN',
        'title'=>'整板订单被冻结',
        'detail'=>'整板订单被冻结',
        'titleEn'=>'amazonFTL have benn frozen',
        'detailEn'=>'amazonFTL have benn frozen',
        'source'=>array(
            'pointer'=>'amazonFTL'
        ),
        'meta'=>array()
    ),
    AMAZONFTL_NOT_UPDATED_ISA=>
    array(
        'id'=>AMAZONFTL_NOT_UPDATED_ISA,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONFTL_NOT_UPDATED_ISA',
        'title'=>'整板订单未更新ISA',
        'detail'=>'整板订单未更新ISA',
        'titleEn'=>'amazonFTL not updated ISA',
        'detailEn'=>'amazonFTL not updated ISA',
        'source'=>array(
            'pointer'=>'amazonFTL'
        ),
        'meta'=>array()
    ),
    TARGET_WAREHOUSE_FORMAT_INCORRECT=>
    array(
        'id'=>TARGET_WAREHOUSE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'TARGET_WAREHOUSE_FORMAT_INCORRECT',
        'title'=>'目标仓库格式不正确，请重新输入',
        'detail'=>'目标仓库格式不正确，请重新输入',
        'titleEn'=>'targetWarehouse format incorrect',
        'detailEn'=>'targetWarehouse format incorrect',
        'source'=>array(
            'pointer'=>'targetWarehouse'
        ),
        'meta'=>array()
    ),
    AMAZONFTL_FORMAT_INCORRECT=>
    array(
        'id'=>AMAZONFTL_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONFTL_FORMAT_INCORRECT',
        'title'=>'整板订单组合格式不正确，请重新输入',
        'detail'=>'整板订单组合格式不正确，请重新输入',
        'titleEn'=>'amazonFTL format incorrect',
        'detailEn'=>'amazonFTL format incorrect',
        'source'=>array(
            'pointer'=>'amazonFTL'
        ),
        'meta'=>array()
    ),
    TARGETWAREHOUSE_NOT_EXISTS=>
    array(
        'id'=>TARGETWAREHOUSE_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'TARGETWAREHOUSE_NOT_EXISTS',
        'title'=>'目标仓库不存在',
        'detail'=>'目标仓库不存在',
        'titleEn'=>'targetWarehouse not exists',
        'detailEn'=>'targetWarehouse not exists',
        'source'=>array(
            'pointer'=>'targetWarehouse'
        ),
        'meta'=>array()
    ),

    DELIVERY_READYTIME_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERY_READYTIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>404,
        'code'=>'DELIVERY_READYTIME_FORMAT_INCORRECT',
        'title'=>'送货时间格式不正确，请重新输入',
        'detail'=>'送货时间格式不正确，请重新输入',
        'titleEn'=>'deliveryReadyTime format incorrect',
        'detailEn'=>'deliveryReadyTime format incorrect',
        'source'=>array(
            'pointer'=>'deliveryReadyTime'
        ),
        'meta'=>array()
    ),
    DELIVERY_CLOSETIME_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERY_CLOSETIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>404,
        'code'=>'DELIVERY_CLOSETIME_FORMAT_INCORRECT',
        'title'=>'送货下班时间格式不正确，请重新输入',
        'detail'=>'送货下班时间格式不正确，请重新输入',
        'titleEn'=>'deliveryCloseTime format incorrect',
        'detailEn'=>'deliveryCloseTime format incorrect',
        'source'=>array(
            'pointer'=>'deliveryCloseTime'
        ),
        'meta'=>array()
    ),
    WEEKEND_DELIVERY_FORMAT_INCORRECT=>
    array(
        'id'=>WEEKEND_DELIVERY_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>404,
        'code'=>'WEEKEND_DELIVERY_FORMAT_INCORRECT',
        'title'=>'周末送货格式不正确，请重新输入',
        'detail'=>'周末送货格式不正确，请重新输入',
        'titleEn'=>'weekendDelivery format incorrect',
        'detailEn'=>'weekendDelivery format incorrect',
        'source'=>array(
            'pointer'=>'weekendDelivery'
        ),
        'meta'=>array()
    ),
    ORDERFTL_CAN_NOT_MODIFY=>
    array(
        'id'=>ORDERFTL_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>404,
        'code'=>'ORDERFTL_CAN_NOT_MODIFY',
        'title'=>'整板订单不能操作（订单状态不对）',
        'detail'=>'整板订单不能操作（订单状态不对）',
        'titleEn'=>'orderFTL can not modify',
        'detailEn'=>'orderFTL can not modify',
        'source'=>array(
            'pointer'=>'orderFTL'
        ),
        'meta'=>array()
    ),
    ORDERFTL_HAVE_BEEN_FROZEN=>
    array(
        'id'=>ORDERFTL_HAVE_BEEN_FROZEN,
        'link'=>'',
        'status'=>404,
        'code'=>'ORDERFTL_HAVE_BEEN_FROZEN',
        'title'=>'整板订单被冻结',
        'detail'=>'整板订单被冻结',
        'titleEn'=>'orderFTL have been frozen',
        'detailEn'=>'orderFTL have been frozen',
        'source'=>array(
            'pointer'=>'orderFTL'
        ),
        'meta'=>array()
    ),
    DELIVERYADDRESS_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYADDRESS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>404,
        'code'=>'DELIVERYADDRESS_FORMAT_INCORRECT',
        'title'=>'收货地址格式不正确，请重新输入',
        'detail'=>'收货地址格式不正确，请重新输入',
        'titleEn'=>'deliveryAddress format incorrect',
        'detailEn'=>'deliveryAddress format incorrect',
        'source'=>array(
            'pointer'=>'deliveryAddress'
        ),
        'meta'=>array()
    ),
    ORDERFTL_FORMAT_INCORRECT=>
    array(
        'id'=>ORDERFTL_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>404,
        'code'=>'ORDERFTL_FORMAT_INCORRECT',
        'title'=>'整板订单组合格式不正确，请重新输入',
        'detail'=>'整板订单组合格式不正确，请重新输入',
        'titleEn'=>'orderFTL format incorrect',
        'detailEn'=>'orderFTL format incorrect',
        'source'=>array(
            'pointer'=>'orderFTL'
        ),
        'meta'=>array()
    ),
    DELIVERYADDRESS_NOT_EXISTS=>
    array(
        'id'=>DELIVERYADDRESS_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'DELIVERYADDRESS_NOT_EXISTS',
        'title'=>'收货地址不存在',
        'detail'=>'收货地址不存在',
        'titleEn'=>'deliveryAddress not exists',
        'detailEn'=>'deliveryAddress not exists',
        'source'=>array(
            'pointer'=>'deliveryAddress'
        ),
        'meta'=>array()
    ),
    DISPATCHORDER_CAN_NOT_MODIFY=>
    array(
        'id'=>DISPATCHORDER_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>404,
        'code'=>'DISPATCHORDER_CAN_NOT_MODIFY',
        'title'=>'发货订单不能操作（状态不对）',
        'detail'=>'发货订单不能操作（状态不对）',
        'titleEn'=>'dispatchOrder can not modify',
        'detailEn'=>'dispatchOrder can not modify',
        'source'=>array(
            'pointer'=>'dispatchOrder'
        ),
        'meta'=>array()
    ),
    DELIVERYNAME_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYNAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYNAME_FORMAT_INCORRECT',
        'title'=>'收货人格式不正确，请重新输入',
        'detail'=>'收货人格式不正确，请重新输入',
        'titleEn'=>'deliveryName format incorrect',
        'detailEn'=>'deliveryName format incorrect',
        'source'=>array(
            'pointer'=>'deliveryName'
        ),
        'meta'=>array()
    ),

    BOOKINGPICKUPDATE_FORMAT_INCORRECT=>
    array(
        'id'=>BOOKINGPICKUPDATE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'BOOKINGPICKUPDATE_FORMAT_INCORRECT',
        'title'=>'预订取货时间（日期）格式不正确，请重新输入',
        'detail'=>'预订取货时间（日期）格式不正确，请重新输入',
        'titleEn'=>'bookingPickupDate format incorrect',
        'detailEn'=>'bookingPickupDate format incorrect',
        'source'=>array(
            'pointer'=>'bookingPickupDate'
        ),
        'meta'=>array()
    ),
    PICKUPZIPCODE_FORMAT_INCORRECT=>
    array(
        'id'=>PICKUPZIPCODE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUPZIPCODE_FORMAT_INCORRECT',
        'title'=>'取货邮编格式不正确，请重新输入',
        'detail'=>'取货邮编格式不正确，请重新输入',
        'titleEn'=>'pickupZipCode format incorrect',
        'detailEn'=>'pickupZipCode format incorrect',
        'source'=>array(
            'pointer'=>'pickupZipCode'
        ),
        'meta'=>array()
    ),
    PICKUPADDRESSTYPE_FORMAT_INCORRECT=>
    array(
        'id'=>PICKUPADDRESSTYPE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUPADDRESSTYPE_FORMAT_INCORRECT',
        'title'=>'发货地址类型格式不正确，请重新输入',
        'detail'=>'发货地址类型格式不正确，请重新输入',
        'titleEn'=>'pickupAddressType format incorrect',
        'detailEn'=>'pickupAddressType format incorrect',
        'source'=>array(
            'pointer'=>'pickupAddressType'
        ),
        'meta'=>array()
    ),
    DELIVERYZIPCODE_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYZIPCODE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYZIPCODE_FORMAT_INCORRECT',
        'title'=>'送货邮编格式不正确，请重新输入',
        'detail'=>'送货邮编格式不正确，请重新输入',
        'titleEn'=>'deliveryZipCode format incorrect',
        'detailEn'=>'deliveryZipCode format incorrect',
        'source'=>array(
            'pointer'=>'deliveryZipCode'
        ),
        'meta'=>array()
    ),
    DELIVERYADDRESSTYPE_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYADDRESSTYPE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYADDRESSTYPE_FORMAT_INCORRECT',
        'title'=>'送货地址类型格式不正确，请重新输入',
        'detail'=>'送货地址类型格式不正确，请重新输入',
        'titleEn'=>'deliveryAddressType format incorrect',
        'detailEn'=>'deliveryAddressType format incorrect',
        'source'=>array(
            'pointer'=>'deliveryAddressType'
        ),
        'meta'=>array()
    ),
    PICKUPNEEDTRANSPORT_FORMAT_INCORRECT=>
    array(
        'id'=>PICKUPNEEDTRANSPORT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUPNEEDTRANSPORT_FORMAT_INCORRECT',
        'title'=>'取货需要室内搬运型格式不正确，请重新输入',
        'detail'=>'取货需要室内搬运型格式不正确，请重新输入',
        'titleEn'=>'pickupNeedTransport format incorrect',
        'detailEn'=>'pickupNeedTransport format incorrect',
        'source'=>array(
            'pointer'=>'pickupNeedTransport'
        ),
        'meta'=>array()
    ),
    PICKUPNEEDLIFTGATE_FORMAT_INCORRECT=>
    array(
        'id'=>PICKUPNEEDLIFTGATE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUPNEEDLIFTGATE_FORMAT_INCORRECT',
        'title'=>'取货需要卡车升降台格式不正确，请重新输入',
        'detail'=>'取货需要卡车升降台格式不正确，请重新输入',
        'titleEn'=>'pickupNeedLiftgate format incorrect',
        'detailEn'=>'pickupNeedLiftgate format incorrect',
        'source'=>array(
            'pointer'=>'pickupNeedLiftgate'
        ),
        'meta'=>array()
    ),
    PONUMBER_FORMAT_INCORRECT=>
    array(
        'id'=>PONUMBER_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PONUMBER_FORMAT_INCORRECT',
        'title'=>'poNumber格式不正确，请重新输入',
        'detail'=>'poNumber格式不正确，请重新输入',
        'titleEn'=>'poNumber format incorrect',
        'detailEn'=>'poNumber format incorrect',
        'source'=>array(
            'pointer'=>'poNumber'
        ),
        'meta'=>array()
    ),
    PICKUPVEHICLETYPE_FORMAT_INCORRECT=>
    array(
        'id'=>PICKUPVEHICLETYPE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUPVEHICLETYPE_FORMAT_INCORRECT',
        'title'=>'取货车辆类型格式不正确，请重新输入',
        'detail'=>'取货车辆类型格式不正确，请重新输入',
        'titleEn'=>'pickupVehicleType format incorrect',
        'detailEn'=>'pickupVehicleType format incorrect',
        'source'=>array(
            'pointer'=>'pickupVehicleType'
        ),
        'meta'=>array()
    ),
    DELIVERYDESCRIPTION_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYDESCRIPTION_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYDESCRIPTION_FORMAT_INCORRECT',
        'title'=>'整体货物描述格式不正确，请重新输入',
        'detail'=>'整体货物描述格式不正确，请重新输入',
        'titleEn'=>'deliveryDescription format incorrect',
        'detailEn'=>'deliveryDescription format incorrect',
        'source'=>array(
            'pointer'=>'deliveryDescription'
        ),
        'meta'=>array()
    ),
    PICKUPREQUIREMENT_FORMAT_INCORRECT=>
    array(
        'id'=>PICKUPREQUIREMENT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUPREQUIREMENT_FORMAT_INCORRECT',
        'title'=>'取货要求格式不正确，请重新输入',
        'detail'=>'取货要求格式不正确，请重新输入',
        'titleEn'=>'pickupRequirement format incorrect',
        'detailEn'=>'pickupRequirement format incorrect',
        'source'=>array(
            'pointer'=>'pickupRequirement'
        ),
        'meta'=>array()
    ),
    DELIVERYREQUIREMENT_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYREQUIREMENT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYREQUIREMENT_FORMAT_INCORRECT',
        'title'=>'送货要求格式不正确，请重新输入',
        'detail'=>'送货要求格式不正确，请重新输入',
        'titleEn'=>'deliveryRequirement format incorrect',
        'detailEn'=>'deliveryRequirement format incorrect',
        'source'=>array(
            'pointer'=>'deliveryRequirement'
        ),
        'meta'=>array()
    ),
    READYTIME_FORMAT_INCORRECT=>
    array(
        'id'=>READYTIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'READYTIME_FORMAT_INCORRECT',
        'title'=>'取货最早时间（时分）格式不正确，请重新输入',
        'detail'=>'取货最早时间（时分）格式不正确，请重新输入',
        'titleEn'=>'readyTime format incorrect',
        'detailEn'=>'readyTime format incorrect',
        'source'=>array(
            'pointer'=>'readyTime'
        ),
        'meta'=>array()
    ),
    CLOSETIME_FORMAT_INCORRECT=>
    array(
        'id'=>CLOSETIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CLOSETIME_FORMAT_INCORRECT',
        'title'=>'取货最晚时间（时分）格式不正确，请重新输入',
        'detail'=>'取货最晚时间（时分）格式不正确，请重新输入',
        'titleEn'=>'closeTime format incorrect',
        'detailEn'=>'closeTime format incorrect',
        'source'=>array(
            'pointer'=>'closeTime'
        ),
        'meta'=>array()
    ),
    DELIVERYREADYTIME_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYREADYTIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYREADYTIME_FORMAT_INCORRECT',
        'title'=>'送达卸货最早时间（时分）格式不正确，请重新输入',
        'detail'=>'送达卸货最早时间（时分）格式不正确，请重新输入',
        'titleEn'=>'deliveryReadyTime format incorrect',
        'detailEn'=>'deliveryReadyTime format incorrect',
        'source'=>array(
            'pointer'=>'deliveryReadyTime'
        ),
        'meta'=>array()
    ),
    DELIVERYCLOSETIME_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYCLOSETIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYCLOSETIME_FORMAT_INCORRECT',
        'title'=>'送达卸货最晚时间（时分）格式不正确，请重新输入',
        'detail'=>'送达卸货最晚时间（时分）格式不正确，请重新输入',
        'titleEn'=>'deliveryCloseTime format incorrect',
        'detailEn'=>'deliveryCloseTime format incorrect',
        'source'=>array(
            'pointer'=>'deliveryCloseTime'
        ),
        'meta'=>array()
    ),
    ORDERLTL_ADDRESS_NOT_EXISTS=>
    array(
        'id'=>ORDERLTL_ADDRESS_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'ORDERLTL_ADDRESS_NOT_EXISTS',
        'title'=>'取货地址不存在',
        'detail'=>'取货地址不存在',
        'titleEn'=>'address not exists',
        'detailEn'=>'address not exists',
        'source'=>array(
            'pointer'=>'address'
        ),
        'meta'=>array()
    ),
    ORDERLTL_DELIVERYADDRESS_NOT_EXISTS=>
    array(
        'id'=>ORDERLTL_DELIVERYADDRESS_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'ORDERLTL_DELIVERYADDRESS_NOT_EXISTS',
        'title'=>'送货地址不存在',
        'detail'=>'送货地址不存在',
        'titleEn'=>'deliveryAddress not exists',
        'detailEn'=>'deliveryAddress not exists',
        'source'=>array(
            'pointer'=>'deliveryAddress'
        ),
        'meta'=>array()
    ),
    RECEIVEORDERLTLORDER_CAN_NOT_MODIFY=>
    array(
        'id'=>RECEIVEORDERLTLORDER_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>404,
        'code'=>'RECEIVEORDERLTLORDER_CAN_NOT_MODIFY',
        'title'=>'取货订单不能操作（状态不对）',
        'detail'=>'取货订单不能操作（状态不对）',
        'titleEn'=>'receiveOrderLTLOrder can not modify',
        'detailEn'=>'receiveOrderLTLOrder can not modify',
        'source'=>array(
            'pointer'=>'receiveOrderLTLOrder'
        ),
        'meta'=>array()
    ),
    ORDERLTL_CAN_NOT_MODIFY=>
    array(
        'id'=>ORDERLTL_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>404,
        'code'=>'ORDERLTL_CAN_NOT_MODIFY',
        'title'=>'散板订单不能操作（订单状态不对）',
        'detail'=>'散板订单不能操作（订单状态不对）',
        'titleEn'=>'orderLTL can not modify',
        'detailEn'=>'orderLTL can not modify',
        'source'=>array(
            'pointer'=>'orderLTL'
        ),
        'meta'=>array()
    ),
    ORDERLTL_HAVE_BEEN_FROZEN=>
    array(
        'id'=>ORDERLTL_HAVE_BEEN_FROZEN,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDERLTL_HAVE_BEEN_FROZEN',
        'title'=>'散板订单被冻结',
        'detail'=>'散板订单被冻结',
        'titleEn'=>'orderLTL have been frozen',
        'detailEn'=>'orderLTL have been frozen',
        'source'=>array(
            'pointer'=>'orderLTL'
        ),
        'meta'=>array()
    ),
    ORDERLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS=>
    array(
        'id'=>ORDERLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDERLTL_HAS_BEEN_ADDED_TO_OTHER_GROUP_ORDERS',
        'title'=>'散板订单已被添加到其他组单内',
        'detail'=>'散板订单已被添加到其他组单内',
        'titleEn'=>'orderLTL has been added to other group orders',
        'detailEn'=>'orderLTL has been added to other group orders',
        'source'=>array(
            'pointer'=>'orderLTL'
        ),
        'meta'=>array()
    ),
    DISPATCHORDERLTLORDER_CAN_NOT_MODIFY=>
    array(
        'id'=>DISPATCHORDERLTLORDER_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'DISPATCHORDERLTLORDER_CAN_NOT_MODIFY',
        'title'=>'发货订单不能操作（状态不对）',
        'detail'=>'发货订单不能操作（状态不对）',
        'titleEn'=>'dispatchOrderLTLOrder can not modify',
        'detailEn'=>'dispatchOrderLTLOrder can not modify',
        'source'=>array(
            'pointer'=>'dispatchOrderLTLOrder'
        ),
        'meta'=>array()
    ),
    ORDERLTL_FORMAT_INCORRECT=>
    array(
        'id'=>ORDERLTL_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDERLTL_FORMAT_INCORRECT',
        'title'=>'散板订单组合格式不正确，请重新输入',
        'detail'=>'散板订单组合格式不正确，请重新输入',
        'titleEn'=>'orderLTL format incorrect',
        'detailEn'=>'orderLTL format incorrect',
        'source'=>array(
            'pointer'=>'orderLTL'
        ),
        'meta'=>array()
    ),
    ORDERLTL_ADDRESS_FORMAT_INCORRECT=>
    array(
        'id'=>ORDERLTL_ADDRESS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDERLTL_ADDRESS_FORMAT_INCORRECT',
        'title'=>'取货地址格式不正确，请重新输入',
        'detail'=>'取货地址格式不正确，请重新输入',
        'titleEn'=>'address format incorrect',
        'detailEn'=>'address format incorrect',
        'source'=>array(
            'pointer'=>'address'
        ),
        'meta'=>array()
    ),
    ORDERLTL_DELIVERYADDRESS_FORMAT_INCORRECT=>
    array(
        'id'=>ORDERLTL_DELIVERYADDRESS_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDERLTL_DELIVERYADDRESS_FORMAT_INCORRECT',
        'title'=>'送货地址格式不正确，请重新输入',
        'detail'=>'送货地址格式不正确，请重新输入',
        'titleEn'=>'deliveryAddress format incorrect',
        'detailEn'=>'deliveryAddress format incorrect',
        'source'=>array(
            'pointer'=>'deliveryAddress'
        ),
        'meta'=>array()
    ),
    GOODSLEVEL_FORMAT_INCORRECT=>
    array(
        'id'=>GOODSLEVEL_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'GOODSLEVEL_FORMAT_INCORRECT',
        'title'=>'货物等级格式不正确，请重新输入',
        'detail'=>'货物等级格式不正确，请重新输入',
        'titleEn'=>'goodsLevel format incorrect',
        'detailEn'=>'goodsLevel format incorrect',
        'source'=>array(
            'pointer'=>'goodsLevel'
        ),
        'meta'=>array()
    ),
    STACKABLE_FORMAT_INCORRECT=>
    array(
        'id'=>STACKABLE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'STACKABLE_FORMAT_INCORRECT',
        'title'=>'是否可叠放格式不正确，请重新输入',
        'detail'=>'是否可叠放格式不正确，请重新输入',
        'titleEn'=>'stackable format incorrect',
        'detailEn'=>'stackable format incorrect',
        'source'=>array(
            'pointer'=>'stackable'
        ),
        'meta'=>array()
    ),
    TURNABLE_FORMAT_INCORRECT=>
    array(
        'id'=>TURNABLE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'TURNABLE_FORMAT_INCORRECT',
        'title'=>'是否可转动格式不正确，请重新输入',
        'detail'=>'是否可转动格式不正确，请重新输入',
        'titleEn'=>'turnable format incorrect',
        'detailEn'=>'turnable format incorrect',
        'source'=>array(
            'pointer'=>'turnable'
        ),
        'meta'=>array()
    ),
    REQUIREMENT_CATEGORY_FORMAT_INCORRECT=>
    array(
        'id'=>REQUIREMENT_CATEGORY_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'REQUIREMENT_CATEGORY_FORMAT_INCORRECT',
        'title'=>'送货或取货要求格式不正确，请重新输入',
        'detail'=>'送货或取货要求格式不正确，请重新输入',
        'titleEn'=>'category format incorrect',
        'detailEn'=>'category format incorrect',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    REQUIREMENT_STARTTIME_FORMAT_INCORRECT=>
    array(
        'id'=>REQUIREMENT_STARTTIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'REQUIREMENT_STARTTIME_FORMAT_INCORRECT',
        'title'=>'取货或送货开放开始时间格式不正确，请重新输入',
        'detail'=>'取货或送货开放开始时间格式不正确，请重新输入',
        'titleEn'=>'startTime format incorrect',
        'detailEn'=>'startTime format incorrect',
        'source'=>array(
            'pointer'=>'startTime'
        ),
        'meta'=>array()
    ),
    REQUIREMENT_ENDTIME_FORMAT_INCORRECT=>
    array(
        'id'=>REQUIREMENT_ENDTIME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'REQUIREMENT_ENDTIME_FORMAT_INCORRECT',
        'title'=>'取货或送货开放结束时间格式不正确，请重新输入',
        'detail'=>'取货或送货开放结束时间格式不正确，请重新输入',
        'titleEn'=>'endTime format incorrect',
        'detailEn'=>'endTime format incorrect',
        'source'=>array(
            'pointer'=>'endTime'
        ),
        'meta'=>array()
    ),

    DELIVERYNEEDTRANSPORT_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYNEEDTRANSPORT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYNEEDTRANSPORT_FORMAT_INCORRECT',
        'title'=>'送货需要室内搬运型格式不正确，请重新输入',
        'detail'=>'送货需要室内搬运型格式不正确，请重新输入',
        'titleEn'=>'deliveryNeedTransport format incorrect',
        'detailEn'=>'deliveryNeedTransport format incorrect',
        'source'=>array(
            'pointer'=>'deliveryNeedTransport'
        ),
        'meta'=>array()
    ),
    DELIVERYNEEDLIFTGATE_FORMAT_INCORRECT=>
    array(
        'id'=>DELIVERYNEEDLIFTGATE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYNEEDLIFTGATE_FORMAT_INCORRECT',
        'title'=>'送货需要卡车升降台格式不正确，请重新输入',
        'detail'=>'送货需要卡车升降台格式不正确，请重新输入',
        'titleEn'=>'deliveryNeedLiftgate format incorrect',
        'detailEn'=>'deliveryNeedLiftgate format incorrect',
        'source'=>array(
            'pointer'=>'deliveryNeedLiftgate'
        ),
        'meta'=>array()
    ),
    PRICEAPIIDENTIFY_FORMAT_INCORRECT=>
    array(
        'id'=>PRICEAPIIDENTIFY_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PRICEAPIIDENTIFY_FORMAT_INCORRECT',
        'title'=>'价格标识格式不正确，请重新输入',
        'detail'=>'价格标识格式不正确，请重新输入',
        'titleEn'=>'priceApiIdentify format incorrect',
        'detailEn'=>'priceApiIdentify format incorrect',
        'source'=>array(
            'pointer'=>'priceApiIdentify'
        ),
        'meta'=>array()
    ),
    PRICE_FORMAT_INCORRECT=>
    array(
        'id'=>PRICE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'PRICE_FORMAT_INCORRECT',
        'title'=>'价格格式（一般为priceApiIdentify没有正确匹配）不正确，请重新输入',
        'detail'=>'价格格式（一般为priceApiIdentify没有正确匹配）不正确，请重新输入',
        'titleEn'=>'price format incorrect',
        'detailEn'=>'price format incorrect',
        'source'=>array(
            'pointer'=>'price'
        ),
        'meta'=>array()
    ),
    PRICEAPIIDENTIFY_NOT_EXISTS=>
    array(
        'id'=>PRICEAPIIDENTIFY_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'PRICEAPIIDENTIFY_NOT_EXISTS',
        'title'=>'价格记录不存在',
        'detail'=>'价格记录不存在',
        'titleEn'=>'priceApiRecord not exists',
        'detailEn'=>'priceApiRecord not exists',
        'source'=>array(
            'pointer'=>'priceApiRecord'
        ),
        'meta'=>array()
    ),
    DESCRIPTION_FORMAT_INCORRECT=>
    array(
        'id'=>DESCRIPTION_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'DESCRIPTION_FORMAT_INCORRECT',
        'title'=>'货物描述格式不正确，请重新输入',
        'detail'=>'货物描述格式不正确，请重新输入',
        'titleEn'=>'description format incorrect',
        'detailEn'=>'description format incorrect',
        'source'=>array(
            'pointer'=>'description'
        ),
        'meta'=>array()
    ),
    ITEMS_EXCEEDS_THE_LIMIT=>
    array(
        'id'=>ITEMS_EXCEEDS_THE_LIMIT,
        'link'=>'',
        'status'=>403,
        'code'=>'ITEMS_EXCEEDS_THE_LIMIT',
        'title'=>'货物数量超过限制',
        'detail'=>'货物数量超过限制',
        'titleEn'=>'items exceeds the limit',
        'detailEn'=>'items exceeds the limit',
        'source'=>array(
            'pointer'=>'items'
        ),
        'meta'=>array()
    ),
    PICKUPNEEDLIFTGATE_PARAMETER_FORMAT_ERRORS=>
    array(
        'id'=>PICKUPNEEDLIFTGATE_PARAMETER_FORMAT_ERRORS,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUPNEEDLIFTGATE_PARAMETER_FORMAT_ERRORS',
        'title'=>'是否需要尾板（升降台）在pickup的type=BUSINESS_DOCK, 必须为false',
        'detail'=>'是否需要尾板（升降台）在pickup的type=BUSINESS_DOCK, 必须为false',
        'titleEn'=>'pickupNeedLiftgate parameter format errors',
        'detailEn'=>'pickupNeedLiftgate parameter format errors',
        'source'=>array(
            'pointer'=>'pickupNeedLiftgate'
        ),
        'meta'=>array()
    ),
    DELIVERYNEEDLIFTGATE_PARAMETER_FORMAT_ERRORS=>
    array(
        'id'=>DELIVERYNEEDLIFTGATE_PARAMETER_FORMAT_ERRORS,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYNEEDLIFTGATE_PARAMETER_FORMAT_ERRORS',
        'title'=>'是否需要尾板（升降台）在delivery的type=BUSINESS_DOCK, 必须为false',
        'detail'=>'是否需要尾板（升降台）在delivery的type=BUSINESS_DOCK, 必须为false',
        'titleEn'=>'deliveryNeedLiftgate parameter format errors',
        'detailEn'=>'deliveryNeedLiftgate parameter format errors',
        'source'=>array(
            'pointer'=>'deliveryNeedLiftgate'
        ),
        'meta'=>array()
    ),
    PICKUPNEEDTRANSPORT_PARAMETER_FORMAT_ERRORS=>
    array(
        'id'=>PICKUPNEEDTRANSPORT_PARAMETER_FORMAT_ERRORS,
        'link'=>'',
        'status'=>403,
        'code'=>'PICKUPNEEDTRANSPORT_PARAMETER_FORMAT_ERRORS',
        'title'=>'是否需要室内搬运在pickup的type=BUSINESS_DOCK, 必须为false',
        'detail'=>'是否需要室内搬运在pickup的type=BUSINESS_DOCK, 必须为false',
        'titleEn'=>'pickupNeedTransport parameter format errors',
        'detailEn'=>'pickupNeedTransport parameter format errors',
        'source'=>array(
            'pointer'=>'pickupNeedTransport'
        ),
        'meta'=>array()
    ),
    DELIVERYNEEDTRANSPORT_PARAMETER_FORMAT_ERRORS=>
    array(
        'id'=>DELIVERYNEEDTRANSPORT_PARAMETER_FORMAT_ERRORS,
        'link'=>'',
        'status'=>403,
        'code'=>'DELIVERYNEEDTRANSPORT_PARAMETER_FORMAT_ERRORS',
        'title'=>'是否需要室内搬运在delivery的type=BUSINESS_DOCK, 必须为false',
        'detail'=>'是否需要室内搬运在delivery的type=BUSINESS_DOCK, 必须为false',
        'titleEn'=>'deliveryNeedTransport parameter format errors',
        'detailEn'=>'deliveryNeedTransport parameter format errors',
        'source'=>array(
            'pointer'=>'deliveryNeedTransport'
        ),
        'meta'=>array()
    ),
    INTERFACE_QUOTATION_NOT_MATCHED=>
    array(
        'id'=>INTERFACE_QUOTATION_NOT_MATCHED,
        'link'=>'',
        'status'=>403,
        'code'=>'INTERFACE_QUOTATION_NOT_MATCHED',
        'title'=>'未匹配到报价',
        'detail'=>'未匹配到报价',
        'titleEn'=>'not matched to quotation',
        'detailEn'=>'not matched to quotation',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),
    ORDERLTL_CAN_NOT_REGISTER_POSITION=>
    array(
        'id'=>ORDERLTL_CAN_NOT_REGISTER_POSITION,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDERLTL_CAN_NOT_REGISTER_POSITION',
        'title'=>'散板订单未入库登记位置',
        'detail'=>'散板订单未入库登记位置',
        'titleEn'=>'orderLTL can not register position',
        'detailEn'=>'orderLTL can not register position',
        'source'=>array(
            'pointer'=>'orderLTL'
        ),
        'meta'=>array()
    ),
    PATYPE_AND_DATYPE_CANNOT_BE_BOTH_RESIDENTIAL=>
    array(
        'id'=>PATYPE_AND_DATYPE_CANNOT_BE_BOTH_RESIDENTIAL,
        'link'=>'',
        'status'=>403,
        'code'=>'PATYPE_AND_DATYPE_CANNOT_BE_BOTH_RESIDENTIAL',
        'title'=>'PickupAddressType 和 deliveryAddressType 不能同时为 Residential',
        'detail'=>'PickupAddressType 和 deliveryAddressType 不能同时为 Residential',
        'titleEn'=>'PickupAddressType and deliveryAddressType cannot be both Residential',
        'detailEn'=>'PickupAddressType and deliveryAddressType cannot be both Residential',
        'source'=>array(
            'pointer'=>''
        ),
        'meta'=>array()
    ),

    POLICY_NAME_FORMAT_INCORRECT=>
    array(
        'id'=>POLICY_NAME_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'POLICY_NAME_FORMAT_INCORRECT',
        'title'=>'策略名称格式不正确，请重新输入',
        'detail'=>'策略名称格式不正确，请重新输入',
        'titleEn'=>'name format incorrect',
        'detailEn'=>'name format incorrect',
        'source'=>array(
            'pointer'=>'name'
        ),
        'meta'=>array()
    ),
    POLICY_DESCRIPTION_FORMAT_INCORRECT=>
    array(
        'id'=>POLICY_DESCRIPTION_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'POLICY_DESCRIPTION_FORMAT_INCORRECT',
        'title'=>'策略描述格式不正确，请重新输入',
        'detail'=>'策略描述格式不正确，请重新输入',
        'titleEn'=>'description format incorrect',
        'detailEn'=>'description format incorrect',
        'source'=>array(
            'pointer'=>'description'
        ),
        'meta'=>array()
    ),
    SELFOPERATED_WAREHOUSE_FORMAT_INCORRECT=>
    array(
        'id'=>SELFOPERATED_WAREHOUSE_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'SELFOPERATED_WAREHOUSE_FORMAT_INCORRECT',
        'title'=>'所属自营仓库格式不正确，请重新输入',
        'detail'=>'所属自营仓库格式不正确，请重新输入',
        'titleEn'=>'selfOperatedWarehouse format incorrect',
        'detailEn'=>'selfOperatedWarehouse format incorrect',
        'source'=>array(
            'pointer'=>'selfOperatedWarehouse'
        ),
        'meta'=>array()
    ),
    POLICY_CONTENT_FORMAT_INCORRECT=>
    array(
        'id'=>POLICY_CONTENT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'POLICY_CONTENT_FORMAT_INCORRECT',
        'title'=>'策略内容格式不正确，请重新输入',
        'detail'=>'策略内容格式不正确，请重新输入',
        'titleEn'=>'content format incorrect',
        'detailEn'=>'content format incorrect',
        'source'=>array(
            'pointer'=>'content'
        ),
        'meta'=>array()
    ),

    AMAZONLTLPOLICY_CAN_NOT_MODIFY=>
    array(
        'id'=>AMAZONLTLPOLICY_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONLTLPOLICY_CAN_NOT_MODIFY',
        'title'=>'亚马逊散板订单价格策略不能操作',
        'detail'=>'亚马逊散板订单价格策略不能操作',
        'titleEn'=>'amazonLTLPolicy can not modify',
        'detailEn'=>'amazonLTLPolicy can not modify',
        'source'=>array(
            'pointer'=>'amazonLTLPolicy'
        ),
        'meta'=>array()
    ),
    SELFOPERATEDWAREHOUSE_NOT_EXISTS=>
    array(
        'id'=>SELFOPERATEDWAREHOUSE_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'SELFOPERATEDWAREHOUSE_NOT_EXISTS',
        'title'=>'自营仓库不存在',
        'detail'=>'自营仓库不存在',
        'titleEn'=>'selfOperatedWarehouse not exists',
        'detailEn'=>'selfOperatedWarehouse not exists',
        'source'=>array(
            'pointer'=>'selfOperatedWarehouse'
        ),
        'meta'=>array()
    ),
    STRATEGIES_NOT_EXISTS=>
    array(
        'id'=>STRATEGIES_NOT_EXISTS,
        'link'=>'',
        'status'=>404,
        'code'=>'STRATEGIES_NOT_EXISTS',
        'title'=>'策略不存在',
        'detail'=>'策略不存在',
        'titleEn'=>'strategies not exists',
        'detailEn'=>'strategies not exists',
        'source'=>array(
            'pointer'=>'strategies'
        ),
        'meta'=>array()
    ),
    AMAZONLTLBASEPRICESTRATEGY_NOT_FOUND=>
    array(
        'id'=>AMAZONLTLBASEPRICESTRATEGY_NOT_FOUND,
        'link'=>'',
        'status'=>404,
        'code'=>'AMAZONLTLBASEPRICESTRATEGY_NOT_FOUND',
        'title'=>'亚马逊散板基础价格策略未匹配',
        'detail'=>'亚马逊散板基础价格策略未匹配',
        'titleEn'=>'amazonLTLBasePriceStrategy not found',
        'detailEn'=>'amazonLTLBasePriceStrategy not found',
        'source'=>array(
            'pointer'=>'amazonLTLBasePriceStrategy'
        ),
        'meta'=>array()
    ),
    DIMENSIONPRICESTRATEGY_NOT_FOUND=>
    array(
        'id'=>DIMENSIONPRICESTRATEGY_NOT_FOUND,
        'link'=>'',
        'status'=>404,
        'code'=>'DIMENSIONPRICESTRATEGY_NOT_FOUND',
        'title'=>'体积价格策略未匹配',
        'detail'=>'体积价格策略未匹配',
        'titleEn'=>'dimensionPriceStrategy not found',
        'detailEn'=>'dimensionPriceStrategy not found',
        'source'=>array(
            'pointer'=>'dimensionPriceStrategy'
        ),
        'meta'=>array()
    ),
    GRADEPRICESTRATEGY_NOT_FOUND=>
    array(
        'id'=>GRADEPRICESTRATEGY_NOT_FOUND,
        'link'=>'',
        'status'=>404,
        'code'=>'GRADEPRICESTRATEGY_NOT_FOUND',
        'title'=>'vip等级价格策略未匹配',
        'detail'=>'vip等级价格策略未匹配',
        'titleEn'=>'gradePriceStrategy not found',
        'detailEn'=>'gradePriceStrategy not found',
        'source'=>array(
            'pointer'=>'gradePriceStrategy'
        ),
        'meta'=>array()
    ),
    IDFPICKUPPRICESTRATEGY_NOT_FOUND=>
    array(
        'id'=>IDFPICKUPPRICESTRATEGY_NOT_FOUND,
        'link'=>'',
        'status'=>404,
        'code'=>'IDFPICKUPPRICESTRATEGY_NOT_FOUND',
        'title'=>'idf取货价格策略未匹配',
        'detail'=>'idf取货价格策略未匹配',
        'titleEn'=>'idfPickupPriceStrategy not found',
        'detailEn'=>'idfPickupPriceStrategy not found',
        'source'=>array(
            'pointer'=>'idfPickupPriceStrategy'
        ),
        'meta'=>array()
    ),
    WEIGHTPRICESTRATEGY_NOT_FOUND=>
    array(
        'id'=>WEIGHTPRICESTRATEGY_NOT_FOUND,
        'link'=>'',
        'status'=>404,
        'code'=>'WEIGHTPRICESTRATEGY_NOT_FOUND',
        'title'=>'重量价格策略未匹配',
        'detail'=>'重量价格策略未匹配',
        'titleEn'=>'weightPriceStrategy not found',
        'detailEn'=>'weightPriceStrategy not found',
        'source'=>array(
            'pointer'=>'weightPriceStrategy'
        ),
        'meta'=>array()
    ),
    MAXPRICEGROUP_NOT_FOUND=>
    array(
        'id'=>MAXPRICEGROUP_NOT_FOUND,
        'link'=>'',
        'status'=>404,
        'code'=>'MAXPRICEGROUP_NOT_FOUND',
        'title'=>'货物属性(取高)策略未匹配',
        'detail'=>'货物属性(取高)策略未匹配',
        'titleEn'=>'maxPriceGroup not found',
        'detailEn'=>'maxPriceGroup not found',
        'source'=>array(
            'pointer'=>'策略未匹配'
        ),
        'meta'=>array()
    ),
    MINPRICEGROUP_NOT_FOUND=>
    array(
        'id'=>MINPRICEGROUP_NOT_FOUND,
        'link'=>'',
        'status'=>404,
        'code'=>'MINPRICEGROUP_NOT_FOUND',
        'title'=>'用户属性(取低)策略未匹配',
        'detail'=>'用户属性(取低)策略未匹配',
        'titleEn'=>'minPriceGroup not found',
        'detailEn'=>'minPriceGroup not found',
        'source'=>array(
            'pointer'=>'策略未匹配'
        ),
        'meta'=>array()
    ),
    SEQUENCEGROUP_NOT_FOUND=>
    array(
        'id'=>SEQUENCEGROUP_NOT_FOUND,
        'link'=>'',
        'status'=>404,
        'code'=>'SEQUENCEGROUP_NOT_FOUND',
        'title'=>'业务属性策略未匹配',
        'detail'=>'业务属性策略未匹配',
        'titleEn'=>'sequenceGroup not found',
        'detailEn'=>'sequenceGroup not found',
        'source'=>array(
            'pointer'=>'sequenceGroup'
        ),
        'meta'=>array()
    ),
    AMAZONFTLPOLICY_CAN_NOT_MODIFY=>
    array(
        'id'=>AMAZONFTLPOLICY_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'AMAZONFTLPOLICY_CAN_NOT_MODIFY',
        'title'=>'亚马逊整板订单价格策略不能操作',
        'detail'=>'亚马逊整板订单价格策略不能操作',
        'titleEn'=>'amazonFTLPolicy can not modify',
        'detailEn'=>'amazonFTLPolicy can not modify',
        'source'=>array(
            'pointer'=>'amazonFTLPolicy'
        ),
        'meta'=>array()
    ),
    ORDERLTLPOLICY_CAN_NOT_MODIFY=>
    array(
        'id'=>ORDERLTLPOLICY_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDERLTLPOLICY_CAN_NOT_MODIFY',
        'title'=>'非亚马逊散板订单价格策略不能操作',
        'detail'=>'非亚马逊散板订单价格策略不能操作',
        'titleEn'=>'orderLTLPolicy can not modify',
        'detailEn'=>'orderLTLPolicy can not modify',
        'source'=>array(
            'pointer'=>'orderLTLPolicy'
        ),
        'meta'=>array()
    ),
    ORDERFTLPOLICY_CAN_NOT_MODIFY=>
    array(
        'id'=>ORDERFTLPOLICY_CAN_NOT_MODIFY,
        'link'=>'',
        'status'=>403,
        'code'=>'ORDERFTLPOLICY_CAN_NOT_MODIFY',
        'title'=>'非亚马逊整板订单价格策略不能操作',
        'detail'=>'非亚马逊整板订单价格策略不能操作',
        'titleEn'=>'orderFTLPolicy can not modify',
        'detailEn'=>'orderFTLPolicy can not modify',
        'source'=>array(
            'pointer'=>'orderFTLPolicy'
        ),
        'meta'=>array()
    ),
    
    CUSTOMLIST_CATEGORY_FORMAT_INCORRECT=>
    array(
        'id'=>CUSTOMLIST_CATEGORY_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CUSTOMLIST_CATEGORY_FORMAT_INCORRECT',
        'title'=>'类型格式不正确，请重新输入',
        'detail'=>'类型格式不正确，请重新输入',
        'titleEn'=>'category format incorrect',
        'detailEn'=>'category format incorrect',
        'source'=>array(
            'pointer'=>'category'
        ),
        'meta'=>array()
    ),
    CUSTOMLIST_IDENTIFY_FORMAT_INCORRECT=>
    array(
        'id'=>CUSTOMLIST_IDENTIFY_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CUSTOMLIST_IDENTIFY_FORMAT_INCORRECT',
        'title'=>'标识格式不正确，请重新输入',
        'detail'=>'标识格式不正确，请重新输入',
        'titleEn'=>'identify format incorrect',
        'detailEn'=>'identify format incorrect',
        'source'=>array(
            'pointer'=>'identify'
        ),
        'meta'=>array()
    ),
    CUSTOMLIST_CONTENT_FORMAT_INCORRECT=>
    array(
        'id'=>CUSTOMLIST_CONTENT_FORMAT_INCORRECT,
        'link'=>'',
        'status'=>403,
        'code'=>'CUSTOMLIST_CONTENT_FORMAT_INCORRECT',
        'title'=>'配置信息格式不正确，请重新输入',
        'detail'=>'配置信息格式不正确，请重新输入',
        'titleEn'=>'content format incorrect',
        'detailEn'=>'content format incorrect',
        'source'=>array(
            'pointer'=>'content'
        ),
        'meta'=>array()
    ),
];
