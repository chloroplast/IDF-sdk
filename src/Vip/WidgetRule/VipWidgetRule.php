<?php
namespace Sdk\Vip\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

class VipWidgetRule
{
    const NAME_MIN_LENGTH = 1;
    const NAME_MAX_LENGTH = 10;
    public function indicators($indicators) : bool
    {
        if (!V::arrayType()->validate($indicators)) {
            Core::setLastError(VIP_INDICATORS_FORMAT_INCORRECT);
            return false;
        }

        foreach ($indicators as $each) {
            if (!isset($each['name']) || !isset($each['consumerMinPrice']) || !isset($each['consumerMaxPrice'])) {
                Core::setLastError(VIP_INDICATORS_FORMAT_INCORRECT);
                return false;
            }

            //验证名称长度：1-10个字符
            if (!V::stringType()->length(self::NAME_MIN_LENGTH, self::NAME_MAX_LENGTH)->validate($each['name'])) {
                Core::setLastError(VIP_NAME_FORMAT_INCORRECT);
                return false;
            }

            //验证消费额度下限为数字
            if (!is_numeric($each['consumerMinPrice'])) {
                Core::setLastError(VIP_CONSUMER_MIN_PRICE_FORMAT_INCORRECT);
                return false;
            }

            //验证消费额度上限为数字
            if (!is_numeric($each['consumerMaxPrice'])) {
                Core::setLastError(VIP_CONSUMER_MAX_PRICE_FORMAT_INCORRECT);
                return false;
            }
        }
        return true;
    }
}
