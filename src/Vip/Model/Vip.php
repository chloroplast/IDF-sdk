<?php
namespace Sdk\Vip\Model;

use Marmot\Core;
use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;
use Sdk\Common\Model\Traits\OperateAbleTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\Vip\Repository\VipRepository;

class Vip implements IObject, IOperateAble
{
    use Object, OperateAbleTrait;

    private $id;
    /**
     * @var array $indicators vip等级信息
     */
    private $indicators;
    
    private $repository;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->indicators = array();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
        $this->repository = new VipRepository();
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->indicators);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
        unset($this->repository);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setIndicators(array $indicators): void
    {
        $this->indicators = $indicators;
    }

    public function getIndicators(): array
    {
        return $this->indicators;
    }

    protected function getRepository() : VipRepository
    {
        return $this->repository;
    }
}
