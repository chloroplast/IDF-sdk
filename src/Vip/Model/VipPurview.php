<?php
namespace Sdk\Vip\Model;

use Sdk\Role\Purview\Model\Purview;
use Sdk\Role\Purview\Model\IPurviewAble;

class VipPurview extends Purview
{

    public function __construct()
    {
        parent::__construct(IPurviewAble::COLUMN['VIP']);
    }

    public function edit() : bool
    {
        return $this->operation('edit');
    }
}
