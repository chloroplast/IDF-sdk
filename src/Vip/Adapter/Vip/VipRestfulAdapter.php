<?php
namespace Sdk\Vip\Adapter\Vip;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Adapter\CommonRestfulAdapter;

use Sdk\Common\Model\Interfaces\IOperateAble;
use Sdk\Common\Adapter\Traits\MapErrorsTrait;
use Sdk\Common\Adapter\Traits\FetchAbleRestfulAdapterTrait;

use Sdk\Vip\Model\NullVip;
use Sdk\Vip\Translator\VipRestfulTranslator;

class VipRestfulAdapter extends CommonRestfulAdapter implements IVipAdapter
{
    use FetchAbleRestfulAdapterTrait,
        MapErrorsTrait;
            
    const MAP_ERROR = array(
        100001 => array(
            'indicators' => VIP_INDICATORS_FORMAT_INCORRECT
        ),
        100240 => array(
            'indicators' => VIP_INDICATORS_OVERLAPPING
        ),
        100241 => array(
            'indicators' => VIP_INDICATORS_INCOHERENT
        )
    );
    
    const SCENARIOS = [
        'VIP_LIST'=>[
            'fields' => [],
            'include' => ''
        ],
        'VIP_FETCH_ONE'=>[
            'fields'=>[],
            'include'=>''
        ]
    ];

    public function __construct(string $baseurl = '', array $headers = [])
    {
        parent::__construct(
            new VipRestfulTranslator(),
            'config/vip',
            $baseurl,
            $headers
        );
    }

    protected function getNullObject() : INull
    {
        return NullVip::getInstance();
    }

    protected function getAlonePossessMapErrors() : array
    {
        return self::MAP_ERROR;
    }

    public function scenario($scenario) : void
    {
        $this->scenario = isset(self::SCENARIOS[$scenario]) ? self::SCENARIOS[$scenario] : array();
    }

    public function fetchOne($id)
    {
        unset($id);
        
        $this->get(
            $this->getResource()
        );
       
        return $this->isSuccess() ? $this->translateToObject() : $this->getNullObject();
    }

    public function update(IOperateAble $operateAbleObject) : bool
    {
        $keys = array('indicators', 'staff');
        $data = $this->getTranslator()->objectToArray($operateAbleObject, $keys);

        $this->patch(
            $this->getResource(),
            $data
        );
        
        if ($this->isSuccess()) {
            $this->translateToObject($operateAbleObject);
            return true;
        }

        return false;
    }
}
