<?php
namespace Sdk\Vip\Adapter\Vip;

use Sdk\Common\Adapter\Traits\FetchAbleMockAdapterTrait;
use Sdk\Common\Adapter\Traits\OperateAbleMockAdapterTrait;

use Sdk\Vip\Model\Vip;

//use Sdk\Vip\Utils\MockObjectGenerate;

class VipMockAdapter implements IVipAdapter
{
    use OperateAbleMockAdapterTrait, FetchAbleMockAdapterTrait;

    public function fetchObject($id)
    {
        return new Vip($id);
       // return MockObjectGenerate::generateVip($id);
    }
}
