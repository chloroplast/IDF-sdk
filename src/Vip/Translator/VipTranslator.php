<?php
namespace Sdk\Vip\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Vip\Model\Vip;
use Sdk\Vip\Model\NullVip;

class VipTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullVip::getInstance();
    }

    public function arrayToObject(array $expression, $vip = null)
    {
        if (empty($expression)) {
            return $this->getNullObject();
        }
        
        if ($vip == null) {
            $vip = new Vip();
        }
        
        if (isset($expression['id'])) {
            $vip->setId(marmot_decode($expression['id']));
        }
        if (isset($expression['indicators'])) {
            $vip->setIndicators($expression['indicators']);
        }
        if (isset($expression['updateTime'])) {
            $vip->setUpdateTime($expression['updateTime']);
        }

        return $vip;
    }

    public function objectToArray($vip, array $keys = array())
    {
        if (!$vip instanceof Vip) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'id',
                'indicators',
                'updateTime'
            );
        }

        $expression = array();

        if (in_array('id', $keys)) {
            $expression['id'] = marmot_encode($vip->getId());
        }
        if (in_array('indicators', $keys)) {
            $expression['indicators'] = $vip->getIndicators();
        }
        if (in_array('updateTime', $keys)) {
            $expression['updateTime'] = $vip->getUpdateTime();
            $expression['updateTimeFormatConvert'] = $this->convertTimeZone('Y-m-d H:i', $vip->getUpdateTime());
        }

        return $expression;
    }
}
