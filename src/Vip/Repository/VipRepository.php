<?php
namespace Sdk\Vip\Repository;

use Marmot\Core;
use Sdk\Common\Repository\CommonRepository;

use Sdk\Vip\Model\Vip;
use Sdk\Vip\Adapter\Vip\IVipAdapter;
use Sdk\Vip\Adapter\Vip\VipMockAdapter;
use Sdk\Vip\Adapter\Vip\VipRestfulAdapter;

class VipRepository extends CommonRepository implements IVipAdapter
{
    const LIST_MODEL_UN = 'VIP_LIST';
    const FETCH_ONE_MODEL_UN = 'VIP_FETCH_ONE';

    public function __construct()
    {
        parent::__construct(
            new VipRestfulAdapter(
                Core::$container->has('baseurl') ? Core::$container->get('baseurl') : '',
                Core::$container->has('headers') ? Core::$container->get('headers') : []
            ),
            new VipMockAdapter()
        );
    }
}
