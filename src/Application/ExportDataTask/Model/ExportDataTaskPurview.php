<?php
namespace Sdk\Application\ExportDataTask\Model;

use Sdk\Role\Purview\Model\Purview;
use Sdk\Role\Purview\Model\IPurviewAble;

class ExportDataTaskPurview extends Purview
{
    public function __construct()
    {
        parent::__construct(IPurviewAble::COLUMN['EXPORT_DATA_TASK']);
    }
}
