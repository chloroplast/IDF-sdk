<?php
namespace Sdk\Application\ExportDataTask\WidgetRule;

use Marmot\Core;
use Respect\Validation\Validator as V;

use Sdk\Application\ExportDataTask\Model\ExportDataTask;

class ExportDataTaskWidgetRule
{
    public function category($category) : bool
    {
        if (!V::numeric()->positive()->validate($category) || !in_array($category, ExportDataTask::CATEGORY)) {
            Core::setLastError(EXPORT_TASK_CATEGORY_NOT_EXISTS);
            return false;
        }

        return true;
    }
}
