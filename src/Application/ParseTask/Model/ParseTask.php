<?php
namespace Sdk\Application\ParseTask\Model;

use Sdk\Order\Repository\AmazonLTLRepository;
use Sdk\Order\Repository\AmazonFTLRepository;
use Sdk\Order\Repository\OrderLTLRepository;
use Sdk\Order\Repository\OrderFTLRepository;

class ParseTask
{
    /**
     * @var string $fileName
     */
    private $fileName;
    /**
     * @var array $items
     */
    private $items;

    private $amazonLTLRepository;
    private $amazonFTLRepository;
    private $orderLTLRepository;
    private $orderFTLRepository;

    public function __construct()
    {
        $this->fileName = '';
        $this->items = array();
        $this->amazonLTLRepository = new AmazonLTLRepository();
        $this->amazonFTLRepository = new AmazonFTLRepository();
        $this->orderLTLRepository = new OrderLTLRepository();
        $this->orderFTLRepository = new OrderFTLRepository();
    }

    public function __destruct()
    {
        unset($this->fileName);
        unset($this->items);
        unset($this->amazonLTLRepository);
        unset($this->amazonFTLRepository);
        unset($this->orderLTLRepository);
        unset($this->orderFTLRepository);
    }

    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    public function getFileName(): string
    {
        return $this->fileName;
    }

    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    protected function getAmazonLTLRepository(): AmazonLTLRepository
    {
        return $this->amazonLTLRepository;
    }

    protected function getAmazonFTLRepository(): AmazonFTLRepository
    {
        return $this->amazonFTLRepository;
    }

    protected function getOrderLTLRepository(): OrderLTLRepository
    {
        return $this->orderLTLRepository;
    }

    protected function getOrderFTLRepository(): OrderFTLRepository
    {
        return $this->orderFTLRepository;
    }

    public function parseAmazonLTL() : bool
    {
        return $this->getAmazonLTLRepository()->parse($this);
    }

    public function parseAmazonFTL() : bool
    {
        return $this->getAmazonFTLRepository()->parse($this);
    }

    public function parseOrderLTL() : bool
    {
        return $this->getOrderLTLRepository()->parse($this);
    }

    public function parseOrderFTL() : bool
    {
        return $this->getOrderFTLRepository()->parse($this);
    }
}
