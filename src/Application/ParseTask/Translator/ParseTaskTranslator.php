<?php
namespace Sdk\Application\ParseTask\Translator;

use Marmot\Interfaces\INull;
use Marmot\Interfaces\ITranslator;
use Sdk\Common\Translator\TranslatorTrait;

use Sdk\Application\ParseTask\Model\ParseTask;
use Sdk\Application\ParseTask\Model\NullParseTask;

class ParseTaskTranslator implements ITranslator
{
    use TranslatorTrait;

    protected function getNullObject() : INull
    {
        return NullParseTask::getInstance();
    }

    public function objectToArray($parseTask, array $keys = array())
    {
        if (!$parseTask instanceof ParseTask) {
            return array();
        }
        
        if (empty($keys)) {
            $keys = array(
                'fileName',
                'items'
            );
        }

        $expression = array();

        if (in_array('fileName', $keys)) {
            $expression['fileName'] = $parseTask->getFileName();
        }
        if (in_array('items', $keys)) {
            $expression['items'] = $parseTask->getItems();
        }

        return $expression;
    }
}
