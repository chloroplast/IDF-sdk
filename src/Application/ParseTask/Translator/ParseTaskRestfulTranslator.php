<?php
namespace Sdk\Application\ParseTask\Translator;

use Marmot\Core;
use Marmot\Interfaces\IRestfulTranslator;
use Sdk\Common\Translator\RestfulTranslatorTrait;

use Sdk\Application\ParseTask\Model\ParseTask;
use Sdk\Application\ParseTask\Model\NullParseTask;

class ParseTaskRestfulTranslator implements IRestfulTranslator
{
    use RestfulTranslatorTrait;
    
    public function arrayToObject(array $expression, $parseTask = null)
    {
        if (empty($expression)) {
            return NullParseTask::getInstance();
        }

        if ($parseTask == null) {
            $parseTask = new ParseTask();
        }

        $data = $expression['data'];
        $attributes = isset($data['attributes']) ? $data['attributes'] : array();

        if (isset($attributes['name'])) {
            $parseTask->setFileName($attributes['name']);
        }
        if (isset($attributes['items'])) {
            $parseTask->setItems($attributes['items']);
        }

        return $parseTask;
    }

    public function objectToArray($parseTask, array $keys = array())
    {
        if (!$parseTask instanceof ParseTask) {
            return array();
        }

        if (empty($keys)) {
            $keys = array(
                'fileName'
            );
        }

        $expression = array(
            'data' => array(
                'type' => 'parseTasks'
            )
        );

        $attributes = array();

        if (in_array('fileName', $keys)) {
            $attributes['name'] = $parseTask->getFileName();
        }
        $expression['data']['attributes'] = $attributes;
        
        return $expression;
    }
}
