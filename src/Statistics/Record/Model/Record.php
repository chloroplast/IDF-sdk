<?php
namespace Sdk\Statistics\Record\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class Record implements IObject
{
    use Object;

    /**
     * 分类
     * WORKBENCH_OA oa工作台 9
     * APPLICATION_LOG_OA oa日志统计 10
     * MEMBER_OA oa用户视角 21
     * ORDER_OA oa订单视角 30
     * REVENUE_OA oa营收视角 40
     * LOGIN_OA oa登录视角 50
     */
    const CATEGORY = array(
        'WORKBENCH_OA' => 9,
        'APPLICATION_LOG_OA' => 10,
        'MEMBER_OA' => 21,
        'ORDER_OA' => 30,
        'REVENUE_OA' => 40,
        'LOGIN_OA' => 50,
    );

    private $id;
    /**
     * @var int $category 分类
     */
    private $category;
    /**
     * @var array $result 统计结果
     */
    private $result;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->category = 0;
        $this->result = array();
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->category);
        unset($this->result);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setCategory(int $category): void
    {
        $this->category = $category;
    }

    public function getCategory(): int
    {
        return $this->category;
    }

    public function setResult(array $result): void
    {
        $this->result = $result;
    }

    public function getResult(): array
    {
        return $this->result;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}
