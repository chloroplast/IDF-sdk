<?php
namespace Sdk\Log\ApplicationLog\Model;

use Marmot\Common\Model\Object;
use Marmot\Common\Model\IObject;

class ApplicationLog implements IObject
{
    use Object;

    /**
     * 用户类型
     * PORTAL 前台
     * OA 后台
     */
    const OPERATOR_CATEGORY = array(
        'PORTAL' => 1,
        'OA' => 2
    );

    const OPERATOR_CATEGORY_CN = array(
        self::OPERATOR_CATEGORY['PORTAL'] => '前台',
        self::OPERATOR_CATEGORY['OA'] => '后台'
    );

    /**
     * 日志所属类型
     * SYSTEM => 0 系统
     * ROLE => 210 角色
     * STAFF => 220 后台-用户
     * VIP => 230 VIP
     * MEMBER => 240 官网-用户
     * DICTIONARY_CATEGORY => 310 字典分类
     * DICTIONARY_ITEM => 320 字典项
     * WAREHOUSE => 410 仓库
     * MESSAGE => 510 消息
     * CARTYPE => 610 车辆类型
     * AMAZONLTL => 620 亚马逊散板
     * AMAZONFTL => 621 亚马逊整板
     * ORDERFTL => 622 非亚马逊整板
     * ORDERLTL => 623 非亚马逊散板
     * RECEIVE_AMAZON_ORDER => 630 亚马逊散板取货组合订单
     * DISPATCH_AMAZON_ORDER => 640 亚马逊散板发货组合订单
     * DISPATCH_AMAZONFTL_ORDER => 641 亚马逊整板发货组合订单
     * DISPATCH_FTL_ORDER => 642 非亚马逊整板发货组合订单
     * DISPATCH_LTL_ORDER => 643 非亚马逊散板发货组合订单
     * MEMBER_ORDER => 650 订单
     * AMAZONLTL_POLICY => 660 亚马逊散板价格策略
     */
    const TARGET_CATEGORY = array(
        'SYSTEM' => 0,
        'ROLE' => 210,
        'STAFF' => 220,
        'VIP' => 230,
        'MEMBER' => 240,
        'DICTIONARY_CATEGORY' => 310,
        'DICTIONARY_ITEM' => 320,
        'WAREHOUSE' => 410,
        'MESSAGE' => 510,
        'CARTYPE' => 610,
        'AMAZONLTL' => 620,
        'AMAZONFTL' => 621,
        'ORDERFTL' => 622,
        'ORDERLTL' => 623,
        'RECEIVE_AMAZON_ORDER' => 630,
        'DISPATCH_AMAZON_ORDER' => 640,
        'DISPATCH_AMAZONFTL_ORDER' => 641,
        'DISPATCH_FTL_ORDER' => 642,
        'DISPATCH_LTL_ORDER' => 643,
        'MEMBER_ORDER' => 650,
        'AMAZONLTL_POLICY' => 660,
    );

    const TARGET_CATEGORY_CN = array(
        self::TARGET_CATEGORY['SYSTEM'] => '系统',
        self::TARGET_CATEGORY['ROLE'] => '角色',
        self::TARGET_CATEGORY['STAFF'] => '后台-用户',
        self::TARGET_CATEGORY['VIP'] => 'VIP',
        self::TARGET_CATEGORY['MEMBER'] => '官网-用户',
        self::TARGET_CATEGORY['DICTIONARY_CATEGORY'] => '字典分类',
        self::TARGET_CATEGORY['DICTIONARY_ITEM'] => '字典项',
        self::TARGET_CATEGORY['WAREHOUSE'] => '仓库',
        self::TARGET_CATEGORY['MESSAGE'] => '消息',
        self::TARGET_CATEGORY['CARTYPE'] => '车辆类型',
        self::TARGET_CATEGORY['AMAZONLTL'] => '亚马逊散板',
        self::TARGET_CATEGORY['AMAZONFTL'] => '亚马逊整板',
        self::TARGET_CATEGORY['ORDERFTL'] => '非亚马逊整板',
        self::TARGET_CATEGORY['ORDERLTL'] => '非亚马逊散板',
        self::TARGET_CATEGORY['RECEIVE_AMAZON_ORDER'] => '亚马逊散板取货组合订单',
        self::TARGET_CATEGORY['DISPATCH_AMAZON_ORDER'] => '亚马逊散板发货组合订单',
        self::TARGET_CATEGORY['DISPATCH_AMAZONFTL_ORDER'] => '亚马逊整板发货组合订单',
        self::TARGET_CATEGORY['DISPATCH_FTL_ORDER'] => '非亚马逊整板发货组合订单',
        self::TARGET_CATEGORY['DISPATCH_LTL_ORDER'] => '非亚马逊散板发货组合订单',
        self::TARGET_CATEGORY['MEMBER_ORDER'] => '订单',
        self::TARGET_CATEGORY['AMAZONLTL_POLICY'] => '亚马逊散板价格策略',
    );

    /**
     * 日志对应动作
     * VIEW => 0, //查看
     * INSERT => 10, //添加
     * UPDATE => 20, //编辑
     * TOP => 30, //置顶
     * CANCEL_TOP => 31, //取消置顶
     * APPROVE => 40, //审核通过
     * REJECT => 41, //审核驳回
     * ENABLE => 50, //启用
     * DISABLE => 51, //禁用
     * DELETED => 52, //删除
     * ACTIVE => 53, //激活
     * INACTIVE => 54, //取消激活
     * DEFAULT => 55, //设置默认
     * PUBLISH => 60, //发布
     * AUTHENTICATE => 61, //认证
     * REAUTHENTICATE => 62, //重新认证
     * AUTHORIZE => 63, //授权
     * UNAUTHORIZE => 64, //取消授权
     * UPLOAD => 65, //上传
     * EXPORT => 66, //导出
     * ACCEPT => 70, //受理
     * REVOKE => 71, //撤销
     * ROLLBACK => 72, //回滚
     * IGNORE => 73, //忽略
     * FORWARD => 74, //转交
     * UPDATE_PASSWORD => 101, //修改密码
     * RESET_PASSWORD => 102, //重置密码
     * UPDATE_SECURITY_QUESTION => 103, //设定安全问题
     * VALIDATE_SECURITY_ANSWER => 104, //验证安全问题
     * LOGIN => 105, //登录
     * CANCEL => 106, //取消
     * UPDATEISA => 107, //确认ISA
     * CONFIRM => 108, //确认
     * FREEZE => 109, //冻结
     * UNFREEZE => 110, //解冻
     * PICKUP => 111, //取货
     * STOCKIN => 112, //入库
     * REGISTER_POSITION => 113, //登记位置
     * DISPATCH => 114, //出库
     * UPDATE_REFERENCES => 115, //更新references
     * REGISTER_PICKUP_DATE => 116, //取货时间记录
     * REGISTER_DISPATCH_DATE => 117, //发货时间记录
     */
    const TARGET_ACTION = array(
        'VIEW' => 0,
        'ADD' => 10,
        'EDIT' => 20,
        'TOP' => 30,
        'CANCEL_TOP' => 31,
        'APPROVE' => 40,
        'REJECT' => 41,
        'ENABLE' => 50,
        'DISABLE' => 51,
        'DELETED' => 52,
        'ACTIVE' => 53,
        'INACTIVE' => 54,
        'DEFAULT' => 55,
        'PUBLISH' => 60,
        'AUTHENTICATE' => 61,
        'REAUTHENTICATE' => 62,
        'AUTHORIZE' => 63,
        'UNAUTHORIZE' => 64,
        'UPLOAD' => 65,
        'EXPORT' => 66,
        'ACCEPT' => 70,
        'REVOKE' => 71,
        'ROLLBACK' => 72,
        'IGNORE' => 73,
        'FORWARD' => 74,
        'UPDATE_PASSWORD' => 101,
        'RESET_PASSWORD' => 102,
        'UPDATE_SECURITY_QUESTION' => 103,
        'VALIDATE_SECURITY_ANSWER' => 104,
        'LOGIN' => 105,
        'CANCEL' => 106,
        'UPDATEISA' => 107,
        'CONFIRM' => 108,
        'FREEZE' => 109,
        'UNFREEZE' => 110,
        'PICKUP' => 111,
        'STOCKIN' => 112,
        'REGISTER_POSITION' => 113,
        'DISPATCH' => 114,
        'UPDATE_REFERENCES' => 115,
        'REGISTER_PICKUP_DATE' => 116,
        'REGISTER_DISPATCH_DATE' => 117,
    );

    const TARGET_ACTION_CN = array(
        self::TARGET_ACTION['VIEW'] => '查看',
        self::TARGET_ACTION['ADD'] => '添加',
        self::TARGET_ACTION['EDIT'] => '编辑',
        self::TARGET_ACTION['TOP'] => '置顶',
        self::TARGET_ACTION['CANCEL_TOP'] => '取消置顶',
        self::TARGET_ACTION['APPROVE'] => '审核通过',
        self::TARGET_ACTION['REJECT'] => '审核驳回',
        self::TARGET_ACTION['ENABLE'] => '启用',
        self::TARGET_ACTION['DISABLE'] => '禁用',
        self::TARGET_ACTION['DELETED'] => '删除',
        self::TARGET_ACTION['ACTIVE'] => '激活',
        self::TARGET_ACTION['INACTIVE'] => '取消激活',
        self::TARGET_ACTION['DEFAULT'] => '设置默认',
        self::TARGET_ACTION['PUBLISH'] => '发布',
        self::TARGET_ACTION['AUTHENTICATE'] => '认证',
        self::TARGET_ACTION['REAUTHENTICATE'] => '重新认证',
        self::TARGET_ACTION['AUTHORIZE'] => '授权',
        self::TARGET_ACTION['UNAUTHORIZE'] => '取消授权',
        self::TARGET_ACTION['UPLOAD'] => '上传',
        self::TARGET_ACTION['EXPORT'] => '导出',
        self::TARGET_ACTION['ACCEPT'] => '受理',
        self::TARGET_ACTION['REVOKE'] => '撤销',
        self::TARGET_ACTION['ROLLBACK'] => '回滚',
        self::TARGET_ACTION['IGNORE'] => '忽略',
        self::TARGET_ACTION['FORWARD'] => '转交',
        self::TARGET_ACTION['UPDATE_PASSWORD'] => '修改密码',
        self::TARGET_ACTION['RESET_PASSWORD'] => '重置密码',
        self::TARGET_ACTION['UPDATE_SECURITY_QUESTION'] => '设定安全问题',
        self::TARGET_ACTION['VALIDATE_SECURITY_ANSWER'] => '验证安全问题',
        self::TARGET_ACTION['LOGIN'] => '登录',
        self::TARGET_ACTION['CANCEL'] => '取消',
        self::TARGET_ACTION['UPDATEISA'] => '确认ISA',
        self::TARGET_ACTION['CONFIRM'] => '确认',
        self::TARGET_ACTION['FREEZE'] => '冻结',
        self::TARGET_ACTION['UNFREEZE'] => '解冻',
        self::TARGET_ACTION['PICKUP'] => '取货',
        self::TARGET_ACTION['STOCKIN'] => '入库',
        self::TARGET_ACTION['REGISTER_POSITION'] => '登记位置',
        self::TARGET_ACTION['DISPATCH'] => '出库',
        self::TARGET_ACTION['UPDATE_REFERENCES'] => '更新references',
        self::TARGET_ACTION['REGISTER_PICKUP_DATE'] => '取货时间记录',
        self::TARGET_ACTION['REGISTER_DISPATCH_DATE'] => '发货时间记录',
    );

    private $id;
    /**
     * @var int $operatorId 日志所属用户id
     */
    private $operatorId;
    /**
     * @var string $operatorIdentify 日志所属用户标识
     */
    private $operatorIdentify;
    /**
     * @var int $operatorCategory 用户类型
     */
    private $operatorCategory;
    /**
     * @var int $targetCategory 日志所属类型
     */
    private $targetCategory;
    /**
     * @var int $targetAction 日志对应动作
     */
    private $targetAction;
    /**
     * @var int $targetId 操作目标id
     */
    private $targetId;
    /**
     * @var string $targetName 操作目标名称
     */
    private $targetName;
    /**
     * @var string $description 描述
     */
    private $description;
    /**
     * @var int $errorId 错误id
     */
    private $errorId;

    public function __construct(int $id = 0)
    {
        $this->id = !empty($id) ? $id : 0;
        $this->operatorId = 0;
        $this->operatorIdentify = '';
        $this->operatorCategory = 0;
        $this->targetCategory = 0;
        $this->targetAction = 0;
        $this->targetId = 0;
        $this->targetName = '';
        $this->description = '';
        $this->errorId = 0;
        $this->status = 0;
        $this->createTime = 0;
        $this->updateTime = 0;
        $this->statusTime = 0;
    }

    public function __destruct()
    {
        unset($this->id);
        unset($this->operatorId);
        unset($this->operatorIdentify);
        unset($this->operatorCategory);
        unset($this->targetCategory);
        unset($this->targetAction);
        unset($this->targetId);
        unset($this->targetName);
        unset($this->description);
        unset($this->errorId);
        unset($this->status);
        unset($this->createTime);
        unset($this->updateTime);
        unset($this->statusTime);
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setOperatorId(int $operatorId): void
    {
        $this->operatorId = $operatorId;
    }

    public function getOperatorId(): int
    {
        return $this->operatorId;
    }

    public function setOperatorIdentify(string $operatorIdentify): void
    {
        $this->operatorIdentify = $operatorIdentify;
    }

    public function getOperatorIdentify(): string
    {
        return $this->operatorIdentify;
    }

    public function setOperatorCategory(int $operatorCategory): void
    {
        $this->operatorCategory = $operatorCategory;
    }

    public function getOperatorCategory(): int
    {
        return $this->operatorCategory;
    }

    public function setTargetCategory(int $targetCategory): void
    {
        $this->targetCategory = $targetCategory;
    }

    public function getTargetCategory(): int
    {
        return $this->targetCategory;
    }

    public function setTargetAction(int $targetAction): void
    {
        $this->targetAction = $targetAction;
    }

    public function getTargetAction(): int
    {
        return $this->targetAction;
    }

    public function setTargetId(int $targetId): void
    {
        $this->targetId = $targetId;
    }

    public function getTargetId(): int
    {
        return $this->targetId;
    }

    public function setTargetName(string $targetName): void
    {
        $this->targetName = $targetName;
    }

    public function getTargetName(): string
    {
        return $this->targetName;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setErrorId(int $errorId): void
    {
        $this->errorId = $errorId;
    }

    public function getErrorId(): int
    {
        return $this->errorId;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}
