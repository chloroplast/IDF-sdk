<?php
namespace Sdk\Common\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Sdk\Common\Model\Interfaces\IOperateAble;

use Sdk\Common\Utils\Traits\CharacterGeneratorTrait;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */
class CommonWidgetRuleTest extends TestCase
{
    use CharacterGeneratorTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CommonWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //routeId
    /**
     * @dataProvider additionProviderRouteId
     */
    public function testRouteId($parameter, $expected)
    {
        $result = $this->stub->routeId($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ROUTE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function additionProviderRouteId()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->title(), false),
            array($faker->randomNumber(2), true)
        );
    }

    //isArrayType
    /**
     * @dataProvider additionProviderIsArrayType
     */
    public function testIsArrayType($parameter, $expected, $pointer)
    {
        $result = $this->stub->isArrayType($parameter, $pointer);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => $pointer), Core::getLastError()->getSource());
    }

    public function additionProviderIsArrayType()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->title(), false, 'string'),
            array($faker->randomNumber(), false, 'number'),
            array($faker->shuffle([1,2,3,4]), true, 'array')
        );
    }

    //isStringType
    /**
     * @dataProvider additionProviderIsStringType
     */
    public function testIsStringType($parameter, $expected, $pointer)
    {
        $result = $this->stub->isStringType($parameter, $pointer);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => $pointer), Core::getLastError()->getSource());
    }

    public function additionProviderIsStringType()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->title(), true, 'string'),
            array($faker->randomNumber(), false, 'number'),
            array($faker->shuffle([1,2,3,4]), false, 'array')
        );
    }

    //isNumericType
    /**
     * @dataProvider additionProviderIsNumericType
     */
    public function testIsNumericType($parameter, $expected, $pointer)
    {
        $result = $this->stub->isNumericType($parameter, $pointer);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => $pointer), Core::getLastError()->getSource());
    }

    public function additionProviderIsNumericType()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->title(), false, 'string'),
            array($faker->randomNumber(), true, 'number'),
            array($faker->shuffle([1,2,3,4]), false, 'array')
        );
    }

    //email
    /**
     * @dataProvider additionProviderEmail
     */
    public function testEmail($parameter, $expected)
    {
        $result = $this->stub->email($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(EMAIL_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderEmail()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->regexify('/^[A-Za-z0-9]+([-._][A-Za-z0-9]+)*[A-Za-z0-9]+(-[A-Za-z0-9]+)*(\.[A-Za-z]{2,6}|[A-Za-z]{2,4}\.[A-Za-z]{2,3})$/'), false),
            array($faker->regexify('/^[A-Za-z0-9]+([-._][A-Za-z0-9]+)*@[A-Za-z0-9]+(-[A-Za-z0-9]+)*(\.[A-Za-z]{2,6}|[A-Za-z]{2,4}\.[A-Za-z]{2,3})$/'), true),
            array($faker->regexify('/^[A-Za-z]{8,10})$/'), false)
        );
    }

    //password
    /**
     * @dataProvider additionProviderPassword
     */
    public function testPassword($parameter, $expected)
    {
        $result = $this->stub->password($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PASSWORD_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    //oldPassword
    /**
     * @dataProvider additionProviderPassword
     */
    public function testOldPassword($parameter, $expected)
    {
        $result = $this->stub->oldPassword($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(OLD_PASSWORD_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPassword()
    {
        $faker = \Faker\Factory::create('zh_CN');
        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array('Password0101#', true),
        );
    }

    //confirmPassword
    /**
     * @dataProvider additionProviderConfirmPassword
     */
    public function testConfirmPassword($parameter, $expected)
    {
        $result = $this->stub->confirmPassword($parameter['password'], $parameter['confirmPassword']);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CONFIRM_PASSWORD_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderConfirmPassword()
    {
        return array(
            array(array('password' => 'password', 'confirmPassword' => 'confirmPassword'), false),
            array(array('password' => '', 'confirmPassword' => 'confirmPassword'), false),
            array(array('password' => 'password', 'confirmPassword' => ''), false),
            array(array('password' => 'password', 'confirmPassword' => 'password'), true)
        );
    }
    
    //name
    /**
     * @dataProvider additionProviderName
     */
    public function testName($parameter, $expected)
    {
        $result = $this->stub->name($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(NAME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderName()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //phone
    /**
     * @dataProvider additionProviderPhone
     */
    public function testPhone($parameter, $expected)
    {
        $result = $this->stub->phone($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PHONE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPhone()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->regexify('/^[A-Za-z]{1,20}$/'), false),
            array($faker->regexify('/^[0-9-]{1,20}$/'), true),
            array($faker->regexify('/^[0-9-]{20,30}$/'), false)
        );
    }
    
    //title
    /**
     * @dataProvider additionProviderTitle
     */
    public function testTitle($parameter, $expected)
    {
        $result = $this->stub->title($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TITLE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderTitle()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
    
    //content
    /**
     * @dataProvider additionProviderContent
     */
    public function testContent($parameter, $expected)
    {
        $result = $this->stub->content($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CONTENT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderContent()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
    
    //enterpriseName
    /**
     * @dataProvider additionProviderEnterpriseName
     */
    public function testEnterpriseName($parameter, $expected)
    {
        $result = $this->stub->enterpriseName($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ENTERPRISE_NAME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderEnterpriseName()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
    
    //enterpriseAddress
    /**
     * @dataProvider additionProviderEnterpriseAddress
     */
    public function testEnterpriseAddress($parameter, $expected)
    {
        $result = $this->stub->enterpriseAddress($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ENTERPRISE_ADDRESS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderEnterpriseAddress()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
    
    //userName
    /**
     * @dataProvider additionProviderUserName
     */
    public function testUserName($parameter, $expected)
    {
        $result = $this->stub->userName($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(USER_NAME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderUserName()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->regexify('/^[a-zA-Z0-9]{0,3}$/'), false),
            array($faker->regexify('/^[a-zA-Z0-9-_\x{4e00}-\x{9fa5}]{4,20}$/'), true),
            array($faker->regexify('/^[a-zA-Z0-9-_\x{4e00}-\x{9fa5}]{21,100}$/'), false)
        );
    }
    
    //address
    /**
     * @dataProvider additionProviderAddress
     */
    public function testAddress($parameter, $expected, $pointer)
    {
        $result = $this->stub->address($parameter, $pointer);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => $pointer), Core::getLastError()->getSource());
    }

    public function additionProviderAddress()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false, 'address'),
            array($faker->randomNumber(), false, 'address'),
            array($faker->name(), true, 'address'),
            array($faker->words(), false, 'address')
        );
    }
    
    //postalCode
    /**
     * @dataProvider additionProviderPostalCode
     */
    public function testPostalCode($parameter, $expected)
    {
        $result = $this->stub->postalCode($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PARAMETER_FORMAT_ERROR, Core::getLastError()->getId());
        $this->assertEquals(array('pointer' => 'postalCode'), Core::getLastError()->getSource());
    }

    public function additionProviderPostalCode()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
    
    //state
    /**
     * @dataProvider additionProviderState
     */
    public function testState($parameter, $expected)
    {
        $result = $this->stub->state($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(STATE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderState()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
    
    //city
    /**
     * @dataProvider additionProviderCity
     */
    public function testCity($parameter, $expected)
    {
        $result = $this->stub->city($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CITY_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderCity()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
}
