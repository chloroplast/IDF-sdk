<?php
namespace Sdk\Warehouse\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class WarehouseWidgetRuleTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new WarehouseWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //contact
    /**
     * @dataProvider additionProviderContact
     */
    public function testContact($parameter, $expected)
    {
        $result = $this->stub->contact($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WAREHOUSE_CONTACT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderContact()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
    
    //keeper
    /**
     * @dataProvider additionProviderKeeper
     */
    public function testKeeper($parameter, $expected)
    {
        $result = $this->stub->keeper($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WAREHOUSE_KEEPER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderKeeper()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->randomNumber(), false),
            array($faker->realText(), true),
            array($faker->words(), false)
        );
    }

    //country
    /**
     * @dataProvider additionProviderCountry
     */
    public function testCountry($parameter, $expected)
    {
        $result = $this->stub->country($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WAREHOUSE_COUNTRY_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderCountry()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->title(), false),
            array($faker->randomNumber(2), true)
        );
    }

    //code
    /**
     * @dataProvider additionProviderCode
     */
    public function testCode($parameter, $expected)
    {
        $result = $this->stub->code($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WAREHOUSE_CODE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderCode()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->word(), true),
            array($faker->words(), false)
        );
    }
    
    //numberCode
    /**
     * @dataProvider additionProviderNumberCode
     */
    public function testNumberCode($parameter, $expected)
    {
        $result = $this->stub->numberCode($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WAREHOUSE_NUMBERCODE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderNumberCode()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->randomDigitNotNull(), false),
            array($faker->word(), true),
            array($faker->words(), false)
        );
    }
    
    //address
    /**
     * @dataProvider additionProviderAddress
     */
    public function testAddress($parameter, $expected)
    {
        $result = $this->stub->address($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WAREHOUSE_ADDRESS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderAddress()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->realText(), true),
            array($faker->words(), false)
        );
    }
    
    //postalCode
    /**
     * @dataProvider additionProviderPostalCode
     */
    public function testPostalCode($parameter, $expected)
    {
        $result = $this->stub->postalCode($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WAREHOUSE_POSTALCODE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPostalCode()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->randomNumber(), false),
            array($faker->word(), true),
            array($faker->words(), false)
        );
    }
}
