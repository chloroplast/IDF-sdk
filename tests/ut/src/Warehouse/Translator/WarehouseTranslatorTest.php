<?php
namespace Sdk\Warehouse\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Warehouse\Utils\MockObjectGenerate;
use Sdk\Warehouse\Utils\TranslatorUtilsTrait;
use Sdk\Warehouse\Model\Warehouse;

class WarehouseTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new WarehouseTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new WarehouseTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Warehouse\Model\NullWarehouse',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Warehouse\Model\NullWarehouse',
            $result
        );
    }

    public function testArrayToObject()
    {
        $warehouse = MockObjectGenerate::generateWarehouse(1);
        
        $expression['id'] = marmot_encode($warehouse->getId());
        $expression['contact'] = $warehouse->getContact();
        $expression['phone'] = $warehouse->getPhone();
        $expression['keeper'] = $warehouse->getKeeper();
        $expression['country'] = $warehouse->getCountry();
        $expression['state'] = $warehouse->getState();
        $expression['city'] = $warehouse->getCity();
        $expression['code'] = $warehouse->getCode();
        $expression['numberCode'] = $warehouse->getNumberCode();
        $expression['address'] = $warehouse->getAddress();
        $expression['postalCode'] = $warehouse->getPostalCode();
        $expression['email'] = $warehouse->getEmail();
        $expression['selfOperated'] = $warehouse->getSelfOperated();
        $expression['status']['id'] = marmot_encode($warehouse->getStatus());
        $expression['statusTime'] = $warehouse->getStatusTime();
        $expression['createTime'] = $warehouse->getCreateTime();
        $expression['updateTime'] = $warehouse->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Warehouse\Model\Warehouse',
            $result
        );

        $this->compareTranslatorEquals($expression, $result);
    }

    public function testObjectToArrayEmpty()
    {
        $warehouse = array();
        $result = $this->stub->objectToArray($warehouse);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(WarehouseTranslatorMock::class)
                           ->setMethods([
                               'statusFormatConversion'
                            ])->getMock();

        $warehouse = MockObjectGenerate::generateWarehouse(1);
        $selfOperated = $warehouse->getSelfOperated();
        $status = $warehouse->getStatus();
        $selfOperatedArray = array('selfOperated');
        $statusArray = array('status');

        $stub->expects($this->exactly(2))->method('statusFormatConversion')
            ->will($this->returnValueMap([
                [$selfOperated, Warehouse::SELF_OPERATED_TYPE, Warehouse::SELF_OPERATED_CN, $selfOperatedArray],
                [$status, Warehouse::STATUS_TYPE, Warehouse::STATUS_CN, $statusArray]
            ]));

        $result = $stub->objectToArray($warehouse);

        $this->assertEquals($result['selfOperated'], $selfOperatedArray);
        $this->assertEquals($result['status'], $statusArray);

        $this->compareTranslatorEquals($result, $warehouse);
    }
}
