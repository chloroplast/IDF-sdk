<?php
namespace Sdk\Warehouse\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Warehouse\Utils\MockObjectGenerate;
use Sdk\Warehouse\Utils\TranslatorUtilsTrait;

class WarehouseRestfulTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new WarehouseRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Warehouse\Model\NullWarehouse',
            $result
        );
    }

    public function testArrayToObject()
    {
        $warehouse = MockObjectGenerate::generateWarehouse(1);

        $expression['data']['id'] = $warehouse->getId();
        $expression['data']['attributes']['contact'] = $warehouse->getContact();
        $expression['data']['attributes']['phone'] = $warehouse->getPhone();
        $expression['data']['attributes']['keeper'] = $warehouse->getKeeper();
        $expression['data']['attributes']['country'] = $warehouse->getCountry();
        $expression['data']['attributes']['state'] = $warehouse->getState();
        $expression['data']['attributes']['city'] = $warehouse->getCity();
        $expression['data']['attributes']['code'] = $warehouse->getCode();
        $expression['data']['attributes']['numberCode'] = $warehouse->getNumberCode();
        $expression['data']['attributes']['address'] = $warehouse->getAddress();
        $expression['data']['attributes']['postalCode'] = $warehouse->getPostalCode();
        $expression['data']['attributes']['email'] = $warehouse->getEmail();
        $expression['data']['attributes']['selfOperated'] = $warehouse->getSelfOperated();
        $expression['data']['attributes']['status'] = $warehouse->getStatus();
        $expression['data']['attributes']['statusTime'] = $warehouse->getStatusTime();
        $expression['data']['attributes']['createTime'] = $warehouse->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $warehouse->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Warehouse\Model\Warehouse',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $warehouse = array();
        $result = $this->stub->objectToArray($warehouse);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $warehouse = MockObjectGenerate::generateWarehouse(1);

        $result = $this->stub->objectToArray($warehouse);
        $this->compareRestfulTranslatorEquals($warehouse, $result);
    }
}
