<?php
namespace Sdk\Warehouse\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class WarehouseTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new Warehouse();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * Warehouse 领域对象,测试构造函数
     */
    public function testWarehouseConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getContact());
        $this->assertEmpty($this->stub->getPhone());
        $this->assertEmpty($this->stub->getKeeper());
        $this->assertEmpty($this->stub->getCountry());
        $this->assertEmpty($this->stub->getState());
        $this->assertEmpty($this->stub->getCity());
        $this->assertEmpty($this->stub->getCode());
        $this->assertEmpty($this->stub->getNumberCode());
        $this->assertEmpty($this->stub->getAddress());
        $this->assertEmpty($this->stub->getPostalCode());
        $this->assertEmpty($this->stub->getEmail());
        $this->assertEmpty($this->stub->getSelfOperated());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Warehouse setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 Warehouse setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //contact 测试 -------------------------------------------------------- start
    /**
     * 设置 Warehouse setContact() 正确的传参类型,期望传值正确
     */
    public function testSetContactCorrectType()
    {
        $this->stub->setContact('contact');
        $this->assertEquals('contact', $this->stub->getContact());
    }

    /**
     * 设置 Warehouse setContact() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContactWrongType()
    {
        $this->stub->setContact(array('contact'));
    }
    //contact 测试 --------------------------------------------------------   end

    //phone 测试 -------------------------------------------------------- start
    /**
     * 设置 Warehouse setPhone() 正确的传参类型,期望传值正确
     */
    public function testSetPhoneCorrectType()
    {
        $this->stub->setPhone('phone');
        $this->assertEquals('phone', $this->stub->getPhone());
    }

    /**
     * 设置 Warehouse setPhone() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPhoneWrongType()
    {
        $this->stub->setPhone(array('phone'));
    }
    //phone 测试 --------------------------------------------------------   end

    //keeper 测试 -------------------------------------------------------- start
    /**
     * 设置 Warehouse setKeeper() 正确的传参类型,期望传值正确
     */
    public function testSetKeeperCorrectType()
    {
        $this->stub->setKeeper('keeper');
        $this->assertEquals('keeper', $this->stub->getKeeper());
    }

    /**
     * 设置 Warehouse setKeeper() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetKeeperWrongType()
    {
        $this->stub->setKeeper(array('keeper'));
    }
    //keeper 测试 --------------------------------------------------------   end

    //code 测试 -------------------------------------------------------- start
    /**
     * 设置 Warehouse setCode() 正确的传参类型,期望传值正确
     */
    public function testSetCodeCorrectType()
    {
        $this->stub->setCode('code');
        $this->assertEquals('code', $this->stub->getCode());
    }

    /**
     * 设置 Warehouse setCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCodeWrongType()
    {
        $this->stub->setCode(array('code'));
    }
    //code 测试 --------------------------------------------------------   end

    //numberCode 测试 -------------------------------------------------------- start
    /**
     * 设置 Warehouse setNumberCode() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCodeCorrectType()
    {
        $this->stub->setNumberCode('numberCode');
        $this->assertEquals('numberCode', $this->stub->getNumberCode());
    }

    /**
     * 设置 Warehouse setNumberCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberCodeWrongType()
    {
        $this->stub->setNumberCode(array('numberCode'));
    }
    //numberCode 测试 --------------------------------------------------------   end

    //address 测试 -------------------------------------------------------- start
    /**
     * 设置 Warehouse setAddress() 正确的传参类型,期望传值正确
     */
    public function testSetAddressCorrectType()
    {
        $this->stub->setAddress('address');
        $this->assertEquals('address', $this->stub->getAddress());
    }

    /**
     * 设置 Warehouse setAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAddressWrongType()
    {
        $this->stub->setAddress(array('address'));
    }
    //address 测试 --------------------------------------------------------   end

    //postalCode 测试 -------------------------------------------------------- start
    /**
     * 设置 Warehouse setPostalCode() 正确的传参类型,期望传值正确
     */
    public function testSetPostalCodeCorrectType()
    {
        $this->stub->setPostalCode('postalCode');
        $this->assertEquals('postalCode', $this->stub->getPostalCode());
    }

    /**
     * 设置 Warehouse setPostalCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPostalCodeWrongType()
    {
        $this->stub->setPostalCode(array('postalCode'));
    }
    //postalCode 测试 --------------------------------------------------------   end

    //email 测试 -------------------------------------------------------- start
    /**
     * 设置 Warehouse setEmail() 正确的传参类型,期望传值正确
     */
    public function testSetEmailCorrectType()
    {
        $this->stub->setEmail('email');
        $this->assertEquals('email', $this->stub->getEmail());
    }

    /**
     * 设置 Warehouse setEmail() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEmailWrongType()
    {
        $this->stub->setEmail(array('email'));
    }
    //email 测试 --------------------------------------------------------   end

    //selfOperated 测试 -------------------------------------------------------- start
    /**
     * 设置 Warehouse setSelfOperated() 正确的传参类型,期望传值正确
     */
    public function testSetSelfOperatedCorrectType()
    {
        $this->stub->setSelfOperated(0);
        $this->assertEquals(0, $this->stub->getSelfOperated());
    }

    /**
     * 设置 Warehouse setSelfOperated() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSelfOperatedWrongType()
    {
        $this->stub->setSelfOperated('selfOperated');
    }
    //selfOperated 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new WarehouseMock();
        $this->assertInstanceOf(
            'Sdk\Warehouse\Repository\WarehouseRepository',
            $stub->getRepositoryPublic()
        );
    }
}
