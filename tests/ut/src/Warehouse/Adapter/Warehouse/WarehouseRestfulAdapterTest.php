<?php
namespace Sdk\Warehouse\Adapter\Warehouse;

use PHPUnit\Framework\TestCase;

class WarehouseRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new WarehouseRestfulAdapterMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIWarehouseAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Warehouse\Adapter\Warehouse\IWarehouseAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Warehouse\Model\NullWarehouse',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array('WAREHOUSE_LIST', WarehouseRestfulAdapter::SCENARIOS['WAREHOUSE_LIST']),
            array('WAREHOUSE_FETCH_ONE', WarehouseRestfulAdapter::SCENARIOS['WAREHOUSE_FETCH_ONE']),
            array('', [])
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(WarehouseRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    public function testInsertTranslatorKeys()
    {
        $this->assertEquals(array(
            'selfOperated',
            'contact',
            'phone',
            'keeper',
            'country',
            'state',
            'city',
            'code',
            'numberCode',
            'address',
            'postalCode',
            'email',
            'staff'
        ), $this->stub->insertTranslatorKeysPublic());
    }

    public function testUpdateTranslatorKeys()
    {
        $this->assertEquals(array(
            'selfOperated',
            'contact',
            'phone',
            'keeper',
            'country',
            'state',
            'city',
            'code',
            'numberCode',
            'address',
            'postalCode',
            'email',
            'staff'
        ), $this->stub->updateTranslatorKeysPublic());
    }

    public function testEnableTranslatorKeys()
    {
        $this->assertEquals(array(
            'staff'
        ), $this->stub->enableTranslatorKeysPublic());
    }

    public function testDisableTranslatorKeys()
    {
        $this->assertEquals(array(
            'staff'
        ), $this->stub->disableTranslatorKeysPublic());
    }

    public function testDeletedTranslatorKeys()
    {
        $this->assertEquals(array(
            'staff'
        ), $this->stub->deletedTranslatorKeysPublic());
    }
}
