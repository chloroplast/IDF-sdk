<?php
namespace Sdk\Warehouse\Adapter\Warehouse;

use PHPUnit\Framework\TestCase;

class WarehouseMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new WarehouseMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIWarehouseAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Warehouse\Adapter\Warehouse\IWarehouseAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Warehouse\Model\Warehouse',
            $this->stub->fetchObject(1)
        );
    }
}
