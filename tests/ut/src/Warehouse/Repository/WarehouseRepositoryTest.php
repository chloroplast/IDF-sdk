<?php
namespace Sdk\Warehouse\Repository;

use PHPUnit\Framework\TestCase;

class WarehouseRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new WarehouseRepository();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIWarehouseAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Warehouse\Adapter\Warehouse\IWarehouseAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Warehouse\Adapter\Warehouse\WarehouseRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Warehouse\Adapter\Warehouse\WarehouseMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
