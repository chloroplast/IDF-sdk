<?php
namespace Sdk\Warehouse\Utils;

use Sdk\Warehouse\Model\Warehouse;

class MockObjectGenerate
{
    public static function generateWarehouse(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Warehouse {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $warehouse = new Warehouse($id);
        $warehouse->setId($id);

        //contact
        $contact = isset($value['contact']) ? $value['contact'] : $faker->name();
        $warehouse->setContact($contact);
        
        //phone
        $phone = isset($value['phone']) ? $value['phone'] : $faker->phoneNumber();
        $warehouse->setPhone($phone);
        
        //keeper
        $keeper = isset($value['keeper']) ? $value['keeper'] : $faker->name();
        $warehouse->setKeeper($keeper);
        
        //country
        $country = isset($value['country']) ? $value['country'] : $faker->randomDigitNotNull();
        $warehouse->setCountry($country);
        
        //state
        $state = isset($value['state']) ? $value['state'] : $faker->randomDigitNotNull();
        $warehouse->setState($state);
        
        //city
        $city = isset($value['city']) ? $value['city'] : $faker->randomDigitNotNull();
        $warehouse->setCity($city);
        
        //code
        $code = isset($value['code']) ? $value['code'] : $faker->word();
        $warehouse->setCode($code);
        
        //numberCode
        $numberCode = isset($value['numberCode']) ? $value['numberCode'] : $faker->word();
        $warehouse->setNumberCode($numberCode);
        
        //address
        $address = isset($value['address']) ? $value['address'] : $faker->address();
        $warehouse->setAddress($address);
        
        //postalCode
        $postalCode = isset($value['postalCode']) ? $value['postalCode'] : $faker->postcode();
        $warehouse->setPostalCode($postalCode);
        
        //email
        $email = isset($value['email']) ? $value['email'] : $faker->email();
        $warehouse->setEmail($email);

        //selfOperated
        $warehouse->setSelfOperated(Warehouse::SELF_OPERATED['YES']);

        $warehouse->setStatus(0);
        $warehouse->setCreateTime($faker->unixTime());
        $warehouse->setUpdateTime($faker->unixTime());
        $warehouse->setStatusTime($faker->unixTime());

        return $warehouse;
    }
}
