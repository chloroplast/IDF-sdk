<?php
namespace Sdk\Warehouse\Utils;

use Sdk\Warehouse\Model\Warehouse;

/**
 * @todo
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait TranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(Warehouse $warehouse, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $warehouse->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['contact'])) {
            $this->assertEquals($attributes['contact'], $warehouse->getContact());
        }
        if (isset($attributes['phone'])) {
            $this->assertEquals($attributes['phone'], $warehouse->getPhone());
        }
        if (isset($attributes['keeper'])) {
            $this->assertEquals($attributes['keeper'], $warehouse->getKeeper());
        }
        if (isset($attributes['country'])) {
            $this->assertEquals($attributes['country'], $warehouse->getCountry());
        }
        if (isset($attributes['state'])) {
            $this->assertEquals($attributes['state'], $warehouse->getState());
        }
        if (isset($attributes['city'])) {
            $this->assertEquals($attributes['city'], $warehouse->getCity());
        }
        if (isset($attributes['code'])) {
            $this->assertEquals($attributes['code'], $warehouse->getCode());
        }
        if (isset($attributes['numberCode'])) {
            $this->assertEquals($attributes['numberCode'], $warehouse->getNumberCode());
        }
        if (isset($attributes['address'])) {
            $this->assertEquals($attributes['address'], $warehouse->getAddress());
        }
        if (isset($attributes['postalCode'])) {
            $this->assertEquals($attributes['postalCode'], $warehouse->getPostalCode());
        }
        if (isset($attributes['email'])) {
            $this->assertEquals($attributes['email'], $warehouse->getEmail());
        }
        if (isset($attributes['selfOperated'])) {
            $this->assertEquals($attributes['selfOperated'], $warehouse->getSelfOperated());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $warehouse->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $warehouse->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $warehouse->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $warehouse->getUpdateTime());
        }
    }

    public function compareTranslatorEquals(array $expression, Warehouse $warehouse)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($warehouse->getId()));
        }
        if (isset($expression['contact'])) {
            $this->assertEquals($expression['contact'], $warehouse->getContact());
        }
        if (isset($expression['phone'])) {
            $this->assertEquals($expression['phone'], $warehouse->getPhone());
        }
        if (isset($expression['keeper'])) {
            $this->assertEquals($expression['keeper'], $warehouse->getKeeper());
        }
        if (isset($expression['country'])) {
            $this->assertEquals($expression['country'], $warehouse->getCountry());
        }
        if (isset($expression['state'])) {
            $this->assertEquals($expression['state'], $warehouse->getState());
        }
        if (isset($expression['city'])) {
            $this->assertEquals($expression['city'], $warehouse->getCity());
        }
        if (isset($expression['code'])) {
            $this->assertEquals($expression['code'], $warehouse->getCode());
        }
        if (isset($expression['numberCode'])) {
            $this->assertEquals($expression['numberCode'], $warehouse->getNumberCode());
        }
        if (isset($expression['address'])) {
            $this->assertEquals($expression['address'], $warehouse->getAddress());
        }
        if (isset($expression['postalCode'])) {
            $this->assertEquals($expression['postalCode'], $warehouse->getPostalCode());
        }
        if (isset($expression['email'])) {
            $this->assertEquals($expression['email'], $warehouse->getEmail());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $warehouse->getCreateTime());
        }
        if (isset($expression['createTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $warehouse->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $warehouse->getUpdateTime());
        }
        if (isset($expression['updateTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $warehouse->getUpdateTime())
            );
        }
    }
}
