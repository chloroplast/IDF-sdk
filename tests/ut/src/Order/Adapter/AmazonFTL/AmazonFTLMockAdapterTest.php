<?php
namespace Sdk\Order\Adapter\AmazonFTL;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\AmazonFTL;
use Sdk\Application\ParseTask\Model\ParseTask;

class AmazonFTLMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonFTLMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIAmazonFTLAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\AmazonFTL\IAmazonFTLAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\AmazonFTL',
            $this->stub->fetchObject(1)
        );
    }

    public function testMemberCancel()
    {
        $object = new AmazonFTL(1);

        $this->assertTrue($this->stub->memberCancel($object));
    }

    public function testStaffCancel()
    {
        $object = new AmazonFTL(1);

        $this->assertTrue($this->stub->staffCancel($object));
    }

    public function testConfirm()
    {
        $object = new AmazonFTL(1);

        $this->assertTrue($this->stub->confirm($object));
    }

    public function testBatchConfirm()
    {
        $objectList = array(new AmazonFTL(1));

        $this->assertTrue($this->stub->batchConfirm($objectList));
    }

    public function testFreeze()
    {
        $object = new AmazonFTL(1);

        $this->assertTrue($this->stub->freeze($object));
    }

    public function testUnFreeze()
    {
        $object = new AmazonFTL(1);

        $this->assertTrue($this->stub->unFreeze($object));
    }

    public function testParse()
    {
        $object = new ParseTask(1);

        $this->assertTrue($this->stub->parse($object));
    }

    public function testCalculate()
    {
        $object = new AmazonFTL(1);

        $this->assertEmpty($this->stub->calculate($object));
    }
}
