<?php
namespace Sdk\Order\Adapter\OrderStatusHistory;

use PHPUnit\Framework\TestCase;

class OrderStatusHistoryRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderStatusHistoryRestfulAdapterMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIOrderStatusHistoryAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\OrderStatusHistory\IOrderStatusHistoryAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullOrderStatusHistory',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'ORDER_STATUS_HISTORY_LIST',
                OrderStatusHistoryRestfulAdapter::SCENARIOS['ORDER_STATUS_HISTORY_LIST']
            ),
            array(
                'ORDER_STATUS_HISTORY_FETCH_ONE',
                OrderStatusHistoryRestfulAdapter::SCENARIOS['ORDER_STATUS_HISTORY_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(
            OrderStatusHistoryRestfulAdapter::MAP_ERROR,
            $this->stub->getAlonePossessMapErrorsPublic()
        );
    }
}
