<?php
namespace Sdk\Order\Adapter\OrderStatusHistory;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\OrderStatusHistory;

class OrderStatusHistoryMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderStatusHistoryMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIOrderStatusHistoryAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\OrderStatusHistory\IOrderStatusHistoryAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\OrderStatusHistory',
            $this->stub->fetchObject(1)
        );
    }
}
