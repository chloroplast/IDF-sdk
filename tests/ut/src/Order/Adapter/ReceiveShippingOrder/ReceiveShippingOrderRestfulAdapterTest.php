<?php
namespace Sdk\Order\Adapter\ReceiveShippingOrder;

use PHPUnit\Framework\TestCase;

class ReceiveShippingOrderRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ReceiveShippingOrderRestfulAdapterMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIReceiveShippingOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\ReceiveShippingOrder\IReceiveShippingOrderAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullReceiveShippingOrder',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'RECEIVE_SHIPPING_ORDER_LIST',
                ReceiveShippingOrderRestfulAdapter::SCENARIOS['RECEIVE_SHIPPING_ORDER_LIST']
            ),
            array(
                'RECEIVE_SHIPPING_ORDER_FETCH_ONE',
                ReceiveShippingOrderRestfulAdapter::SCENARIOS['RECEIVE_SHIPPING_ORDER_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(
            ReceiveShippingOrderRestfulAdapter::MAP_ERROR,
            $this->stub->getAlonePossessMapErrorsPublic()
        );
    }
}
