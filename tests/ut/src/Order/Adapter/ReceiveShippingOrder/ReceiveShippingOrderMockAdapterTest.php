<?php
namespace Sdk\Order\Adapter\ReceiveShippingOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\ReceiveShippingOrder;

class ReceiveShippingOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ReceiveShippingOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIReceiveShippingOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\ReceiveShippingOrder\IReceiveShippingOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\ReceiveShippingOrder',
            $this->stub->fetchObject(1)
        );
    }
}
