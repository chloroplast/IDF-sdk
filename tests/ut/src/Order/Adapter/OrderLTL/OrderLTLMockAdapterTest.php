<?php
namespace Sdk\Order\Adapter\OrderLTL;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\OrderLTL;
use Sdk\Order\Model\ItemLTL;
use Sdk\Application\ParseTask\Model\ParseTask;

class OrderLTLMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderLTLMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIOrderLTLAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\OrderLTL\IOrderLTLAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\OrderLTL',
            $this->stub->fetchObject(1)
        );
    }

    public function testMemberCancel()
    {
        $object = new OrderLTL(1);

        $this->assertTrue($this->stub->memberCancel($object));
    }

    public function testStaffCancel()
    {
        $object = new OrderLTL(1);

        $this->assertTrue($this->stub->staffCancel($object));
    }

    public function testConfirm()
    {
        $object = new OrderLTL(1);

        $this->assertTrue($this->stub->confirm($object));
    }

    public function testBatchConfirm()
    {
        $objectList = array(new OrderLTL(1));

        $this->assertTrue($this->stub->batchConfirm($objectList));
    }

    public function testRegisterPosition()
    {
        $object = new OrderLTL(1);

        $this->assertTrue($this->stub->registerPosition($object));
    }

    public function testFreeze()
    {
        $object = new OrderLTL(1);

        $this->assertTrue($this->stub->freeze($object));
    }

    public function testUnFreeze()
    {
        $object = new OrderLTL(1);

        $this->assertTrue($this->stub->unFreeze($object));
    }

    public function testParse()
    {
        $parseTask = new ParseTask(1);

        $this->assertTrue($this->stub->parse($parseTask));
    }

    public function testCalculate()
    {
        $object = new OrderLTL(1);

        $this->assertEmpty($this->stub->calculate($object));
    }

    public function testCalculateGoodsLevel()
    {
        $object = new ItemLTL(1);

        $this->assertTrue($this->stub->calculateGoodsLevel($object));
    }
}
