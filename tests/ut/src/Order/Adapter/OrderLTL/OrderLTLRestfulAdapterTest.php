<?php
namespace Sdk\Order\Adapter\OrderLTL;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\MemberOrder;
use Sdk\Order\Model\OrderLTLMock;
use Sdk\Order\Translator\OrderLTLRestfulTranslator;
use Sdk\Order\Translator\OrderPriceReportRestfulTranslator;

class OrderLTLRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(OrderLTLRestfulAdapterMock::class)
                           ->setMethods([
                               'insertTranslatorKeys',
                               'updateTranslatorKeys',
                               'getTranslator',
                               'post',
                               'patch',
                               'getResource',
                               'isSuccess',
                               'translateToObject',
                               'translateToObjects'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIOrderLTLAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\OrderLTL\IOrderLTLAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullOrderLTL',
            $this->stub->getNullObjectPublic()
        );
    }

    public function testGetNullParseTask()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullParseTaskPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Application\ParseTask\Model\NullParseTask',
            $this->stub->getNullParseTaskPublic()
        );
    }

    public function testGetParseTaskRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Application\ParseTask\Translator\ParseTaskRestfulTranslator',
            $this->stub->getParseTaskRestfulTranslatorPublic()
        );
    }

    public function testGetOrderPriceReportRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Translator\OrderPriceReportRestfulTranslator',
            $this->stub->getOrderPriceReportRestfulTranslatorPublic()
        );
    }

    public function testGetItemLTLRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Translator\ItemLTLRestfulTranslator',
            $this->stub->getItemLTLRestfulTranslatorPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array('ORDERLTL_LIST', OrderLTLRestfulAdapter::SCENARIOS['ORDERLTL_LIST']),
            array('ORDERLTL_FETCH_ONE', OrderLTLRestfulAdapter::SCENARIOS['ORDERLTL_FETCH_ONE']),
            array('', [])
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(OrderLTLRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    public function testInsertTranslatorKeys()
    {
        $this->assertEquals(array(
            'bookingPickupDate',
            'pickupZipCode',
            'pickupAddressType',
            'deliveryZipCode',
            'deliveryAddressType',
            'pickupNeedTransport',
            'pickupNeedLiftgate',
            'deliveryNeedTransport',
            'deliveryNeedLiftgate',
            'poNumber',
            'items',
            'pickupVehicleType',
            'readyTime',
            'closeTime',
            'deliveryName',
            'deliveryReadyTime',
            'deliveryCloseTime',
            'weekendDelivery',
            'deliveryDescription',
            'remark',
            'pickupRequirement',
            'deliveryRequirement',
            'idfPickup',
            'priceApiRecordId',
            'priceApiIdentify',
            'address',
            'deliveryAddress',
            'pickupWarehouse',
            'member',
            'staff'
        ), $this->stub->insertTranslatorKeysPublic());
    }

    public function testUpdateTranslatorKeys()
    {
        $this->assertEquals(array(
            'bookingPickupDate',
            'pickupZipCode',
            'pickupAddressType',
            'deliveryZipCode',
            'deliveryAddressType',
            'pickupNeedTransport',
            'pickupNeedLiftgate',
            'deliveryNeedTransport',
            'deliveryNeedLiftgate',
            'poNumber',
            'items',
            'pickupVehicleType',
            'readyTime',
            'closeTime',
            'deliveryName',
            'deliveryReadyTime',
            'deliveryCloseTime',
            'weekendDelivery',
            'deliveryDescription',
            'remark',
            'pickupRequirement',
            'deliveryRequirement',
            'idfPickup',
            'priceApiRecordId',
            'priceApiIdentify',
            'address',
            'deliveryAddress',
            'pickupWarehouse',
            'member',
            'staff'
        ), $this->stub->updateTranslatorKeysPublic());
    }

    public function testEnableTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->enableTranslatorKeysPublic());
    }

    public function testDisableTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->disableTranslatorKeysPublic());
    }

    public function testDeletedTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->deletedTranslatorKeysPublic());
    }

    private function insert(bool $result)
    {
        $resource = 'memberOrders/orderLTL';
        $data = array('member');
        $keys = array('member');
        $object = new OrderLTLMock();

        $this->stub->expects($this->exactly(1))->method('insertTranslatorKeys')->willReturn($keys);

        // 为 OrderLTLRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(OrderLTLRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('post')->with($resource, $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testInsertTrue()
    {
        $object = $this->insert(true);

        $result = $this->stub->insert($object);

        $this->assertTrue($result);
    }

    public function testInsertFalse()
    {
        $object = $this->insert(false);

        $result = $this->stub->insert($object);

        $this->assertFalse($result);
    }

    private function update(bool $result)
    {
        $id = 1;
        $resource = 'memberOrders/orderLTL';
        $data = array('member');
        $keys = array('member');
        $object = new OrderLTLMock($id);
        $object->setMemberOrder(new MemberOrder($id));

        $this->stub->expects($this->exactly(1))->method('updateTranslatorKeys')->willReturn($keys);

        // 为 OrderLTLRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(OrderLTLRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id, $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testUpdateTrue()
    {
        $object = $this->update(true);

        $result = $this->stub->update($object);

        $this->assertTrue($result);
    }

    public function testUpdateFalse()
    {
        $object = $this->update(false);

        $result = $this->stub->update($object);

        $this->assertFalse($result);
    }

    private function memberCancel(bool $result)
    {
        $id = 1;
        $resource = 'orderLTL';
        $data = array();
        $keys = array('member');
        $object = new OrderLTLMock($id);

        // 为 OrderLTLRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(OrderLTLRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/cancel', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testMemberCancelTrue()
    {
        $object = $this->memberCancel(true);

        $result = $this->stub->memberCancel($object);

        $this->assertTrue($result);
    }

    public function testMemberCancelFalse()
    {
        $object = $this->memberCancel(false);

        $result = $this->stub->memberCancel($object);

        $this->assertFalse($result);
    }

    private function staffCancel(bool $result)
    {
        $id = 1;
        $resource = 'orderLTL';
        $data = array();
        $keys = array('staff');
        $object = new OrderLTLMock($id);

        // 为 OrderLTLRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(OrderLTLRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/cancel', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testStaffCancelTrue()
    {
        $object = $this->staffCancel(true);

        $result = $this->stub->staffCancel($object);

        $this->assertTrue($result);
    }

    public function testStaffCancelFalse()
    {
        $object = $this->staffCancel(false);

        $result = $this->stub->staffCancel($object);

        $this->assertFalse($result);
    }

    private function confirm(bool $result)
    {
        $id = 1;
        $resource = 'orderLTL';
        $data = array();
        $keys = array('staff');
        $object = new OrderLTLMock($id);

        // 为 OrderLTLRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(OrderLTLRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/confirm', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testConfirmTrue()
    {
        $object = $this->confirm(true);

        $result = $this->stub->confirm($object);

        $this->assertTrue($result);
    }

    public function testConfirmFalse()
    {
        $object = $this->confirm(false);

        $result = $this->stub->confirm($object);

        $this->assertFalse($result);
    }

    private function freeze(bool $result)
    {
        $id = 1;
        $resource = 'orderLTL';
        $data = array();
        $keys = array('staff');
        $object = new OrderLTLMock($id);

        // 为 OrderLTLRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(OrderLTLRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/freeze', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testFreezeTrue()
    {
        $object = $this->freeze(true);

        $result = $this->stub->freeze($object);

        $this->assertTrue($result);
    }

    public function testFreezeFalse()
    {
        $object = $this->freeze(false);

        $result = $this->stub->freeze($object);

        $this->assertFalse($result);
    }

    private function unFreeze(bool $result)
    {
        $id = 1;
        $resource = 'orderLTL';
        $data = array();
        $keys = array('staff');
        $object = new OrderLTLMock($id);

        // 为 OrderLTLRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(OrderLTLRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/unFreeze', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testUnFreezeTrue()
    {
        $object = $this->unFreeze(true);

        $result = $this->stub->unFreeze($object);

        $this->assertTrue($result);
    }

    public function testUnFreezeFalse()
    {
        $object = $this->unFreeze(false);

        $result = $this->stub->unFreeze($object);

        $this->assertFalse($result);
    }

    private function batchConfirm(bool $result)
    {
        $id = 1;
        $object = new OrderLTLMock($id);
        $objectList = array($object);
        $resource = 'orderLTL';
        $keys = array('staff');
        $data = array(
            'data' => array(
                'relationships' => array(
                    'orderLTL' => array(
                        'data' => array(
                            array(
                                'type' => 'orderLTL',
                                'id' => strval($object->getId())
                            )
                        )
                    )
                )
            )
        );

        // 为 OrderLTLRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(OrderLTLRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/batchConfirm', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObjects')->willReturn($objectList);
        }

        return $objectList;
    }

    public function testBatchConfirmTrue()
    {
        $objectList = $this->batchConfirm(true);

        $result = $this->stub->batchConfirm($objectList);

        $this->assertTrue($result);
    }

    public function testBatchConfirmFalse()
    {
        $objectList = $this->batchConfirm(false);

        $result = $this->stub->batchConfirm($objectList);

        $this->assertFalse($result);
    }
    private function registerPosition(bool $result)
    {
        $id = 1;
        $resource = 'orderLTL';
        $data = array();
        $keys = array('position', 'stockInNumber', 'stockInTime', 'staff');
        $object = new OrderLTLMock($id);

        // 为 OrderLTLRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(OrderLTLRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/register/position', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testRegisterPositionTrue()
    {
        $object = $this->registerPosition(true);

        $result = $this->stub->registerPosition($object);

        $this->assertTrue($result);
    }

    public function testRegisterPositionFalse()
    {
        $object = $this->registerPosition(false);

        $result = $this->stub->registerPosition($object);

        $this->assertFalse($result);
    }
}
