<?php
namespace Sdk\Order\Adapter\DispatchShippingOrder;

use PHPUnit\Framework\TestCase;

class DispatchShippingOrderRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DispatchShippingOrderRestfulAdapterMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIDispatchShippingOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DispatchShippingOrder\IDispatchShippingOrderAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullDispatchShippingOrder',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'DISPATCH_SHIPPING_ORDER_LIST',
                DispatchShippingOrderRestfulAdapter::SCENARIOS['DISPATCH_SHIPPING_ORDER_LIST']
            ),
            array(
                'DISPATCH_SHIPPING_ORDER_FETCH_ONE',
                DispatchShippingOrderRestfulAdapter::SCENARIOS['DISPATCH_SHIPPING_ORDER_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(
            DispatchShippingOrderRestfulAdapter::MAP_ERROR,
            $this->stub->getAlonePossessMapErrorsPublic()
        );
    }
}
