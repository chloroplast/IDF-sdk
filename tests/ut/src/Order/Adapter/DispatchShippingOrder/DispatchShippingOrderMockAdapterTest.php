<?php
namespace Sdk\Order\Adapter\DispatchShippingOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DispatchShippingOrder;

class DispatchShippingOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DispatchShippingOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDispatchShippingOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DispatchShippingOrder\IDispatchShippingOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DispatchShippingOrder',
            $this->stub->fetchObject(1)
        );
    }
}
