<?php
namespace Sdk\Order\Adapter\ReceiveOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\ReceiveOrder;

class ReceiveOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ReceiveOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIReceiveOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\ReceiveOrder\IReceiveOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\ReceiveOrder',
            $this->stub->fetchObject(1)
        );
    }

    public function testRegisterPickupDate()
    {
        $object = new ReceiveOrder(1);

        $this->assertTrue($this->stub->registerPickupDate($object));
    }

    public function testPickup()
    {
        $object = new ReceiveOrder(1);

        $this->assertTrue($this->stub->pickup($object));
    }

    public function testStockIn()
    {
        $object = new ReceiveOrder(1);

        $this->assertTrue($this->stub->stockIn($object));
    }
}
