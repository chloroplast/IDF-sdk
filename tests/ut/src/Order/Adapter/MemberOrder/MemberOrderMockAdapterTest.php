<?php
namespace Sdk\Order\Adapter\MemberOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\MemberOrder;

class MemberOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new MemberOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIMemberOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\MemberOrder\IMemberOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\MemberOrder',
            $this->stub->fetchObject(1)
        );
    }
}
