<?php
namespace Sdk\Order\Adapter\DisAmaFTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DisAmaFTLOrderMock;
use Sdk\Order\Translator\DisAmaFTLOrderRestfulTranslator;

class DisAmaFTLOrderRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(DisAmaFTLOrderRestfulAdapterMock::class)
                           ->setMethods([
                               'getTranslator',
                               'patch',
                               'getResource',
                               'isSuccess',
                               'translateToObject'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIDisAmaFTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisAmaFTLOrder\IDisAmaFTLOrderAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullDisAmaFTLOrder',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array('DIS_AMAFTL_ORDER_LIST', DisAmaFTLOrderRestfulAdapter::SCENARIOS['DIS_AMAFTL_ORDER_LIST']),
            array('DIS_AMAFTL_ORDER_FETCH_ONE', DisAmaFTLOrderRestfulAdapter::SCENARIOS['DIS_AMAFTL_ORDER_FETCH_ONE']),
            array('', [])
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(DisAmaFTLOrderRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    private function updateISA(bool $result)
    {
        $id = 1;
        $resource = 'dispatchOrders/amazonFTL';
        $data = array();
        $keys = array('isaNumber', 'staff');
        $object = new DisAmaFTLOrderMock($id);

        // 为 DisAmaFTLOrderRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(DisAmaFTLOrderRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/isa', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testUpdateISATrue()
    {
        $object = $this->updateISA(true);

        $result = $this->stub->updateISA($object);

        $this->assertTrue($result);
    }

    public function testUpdateISAFalse()
    {
        $object = $this->updateISA(false);

        $result = $this->stub->updateISA($object);

        $this->assertFalse($result);
    }

    private function registerDispatchDate(bool $result)
    {
        $id = 1;
        $resource = 'dispatchOrders/amazonFTL';
        $data = array();
        $keys = array('pickupDate', 'deliveryDate', 'references', 'staff');
        $object = new DisAmaFTLOrderMock($id);

        // 为 DisAmaFTLOrderRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(DisAmaFTLOrderRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with(
            $resource.'/'.$id.'/registerDispatchDate',
            $data
        );
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testRegisterDispatchDateTrue()
    {
        $object = $this->registerDispatchDate(true);

        $result = $this->stub->registerDispatchDate($object);

        $this->assertTrue($result);
    }

    public function testRegisterDispatchDateFalse()
    {
        $object = $this->registerDispatchDate(false);

        $result = $this->stub->registerDispatchDate($object);

        $this->assertFalse($result);
    }

    private function dispatch(bool $result)
    {
        $id = 1;
        $resource = 'dispatchOrders/amazonFTL';
        $data = array();
        $keys = array('dispatchItemsAttachments', 'dispatchAttachments', 'staff');
        $object = new DisAmaFTLOrderMock($id);

        // 为 DisAmaFTLOrderRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(DisAmaFTLOrderRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/dispatch', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testDispatchTrue()
    {
        $object = $this->dispatch(true);

        $result = $this->stub->dispatch($object);

        $this->assertTrue($result);
    }

    public function testDispatchFalse()
    {
        $object = $this->dispatch(false);

        $result = $this->stub->dispatch($object);

        $this->assertFalse($result);
    }

    private function accept(bool $result)
    {
        $id = 1;
        $resource = 'dispatchOrders/amazonFTL';
        $data = array();
        $keys = array('acceptItemsAttachments', 'acceptAttachments', 'closePage', 'staff');
        $object = new DisAmaFTLOrderMock($id);

        // 为 DisAmaFTLOrderRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(DisAmaFTLOrderRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/accept', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testAcceptTrue()
    {
        $object = $this->accept(true);

        $result = $this->stub->accept($object);

        $this->assertTrue($result);
    }

    public function testAcceptFalse()
    {
        $object = $this->accept(false);

        $result = $this->stub->accept($object);

        $this->assertFalse($result);
    }
}
