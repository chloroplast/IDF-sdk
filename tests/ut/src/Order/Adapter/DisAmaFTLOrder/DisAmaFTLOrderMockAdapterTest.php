<?php
namespace Sdk\Order\Adapter\DisAmaFTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DisAmaFTLOrder;

class DisAmaFTLOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisAmaFTLOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDisAmaFTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisAmaFTLOrder\IDisAmaFTLOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DisAmaFTLOrder',
            $this->stub->fetchObject(1)
        );
    }

    public function testUpdateISA()
    {
        $object = new DisAmaFTLOrder(1);

        $this->assertTrue($this->stub->updateISA($object));
    }

    public function testRegisterDispatchDate()
    {
        $object = new DisAmaFTLOrder(1);

        $this->assertTrue($this->stub->registerDispatchDate($object));
    }

    public function testDispatch()
    {
        $object = new DisAmaFTLOrder(1);

        $this->assertTrue($this->stub->dispatch($object));
    }

    public function testAccept()
    {
        $object = new DisAmaFTLOrder(1);

        $this->assertTrue($this->stub->accept($object));
    }
}
