<?php
namespace Sdk\Order\Adapter\AmazonLTL;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\AmazonLTL;

use Sdk\Application\ParseTask\Model\ParseTask;

class AmazonLTLMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonLTLMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIAmazonLTLAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\AmazonLTL\IAmazonLTLAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\AmazonLTL',
            $this->stub->fetchObject(1)
        );
    }

    public function testMemberCancel()
    {
        $object = new AmazonLTL(1);

        $this->assertTrue($this->stub->memberCancel($object));
    }

    public function testStaffCancel()
    {
        $object = new AmazonLTL(1);

        $this->assertTrue($this->stub->staffCancel($object));
    }

    public function testConfirm()
    {
        $object = new AmazonLTL(1);

        $this->assertTrue($this->stub->confirm($object));
    }

    public function testBatchConfirm()
    {
        $objectList = array(new AmazonLTL(1));

        $this->assertTrue($this->stub->batchConfirm($objectList));
    }

    public function testRegisterPosition()
    {
        $object = new AmazonLTL(1);

        $this->assertTrue($this->stub->registerPosition($object));
    }

    public function testFreeze()
    {
        $object = new AmazonLTL(1);

        $this->assertTrue($this->stub->freeze($object));
    }

    public function testUnFreeze()
    {
        $object = new AmazonLTL(1);

        $this->assertTrue($this->stub->unFreeze($object));
    }

    public function testParse()
    {
        $object = new ParseTask(1);

        $this->assertTrue($this->stub->parse($object));
    }

    public function testAutoGroup()
    {
        $object = array(new AmazonLTL(1));

        $this->assertEmpty($this->stub->autoGroup($object));
    }

    public function testCalculate()
    {
        $object = new AmazonLTL(1);

        $this->assertEmpty($this->stub->calculate($object));
    }
}
