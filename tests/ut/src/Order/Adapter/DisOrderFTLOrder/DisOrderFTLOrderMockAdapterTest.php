<?php
namespace Sdk\Order\Adapter\DisOrderFTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DisOrderFTLOrder;

class DisOrderFTLOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisOrderFTLOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDisOrderFTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisOrderFTLOrder\IDisOrderFTLOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DisOrderFTLOrder',
            $this->stub->fetchObject(1)
        );
    }

    public function testRegisterDispatchDate()
    {
        $object = new DisOrderFTLOrder(1);

        $this->assertTrue($this->stub->registerDispatchDate($object));
    }

    public function testDispatch()
    {
        $object = new DisOrderFTLOrder(1);

        $this->assertTrue($this->stub->dispatch($object));
    }

    public function testAccept()
    {
        $object = new DisOrderFTLOrder(1);

        $this->assertTrue($this->stub->accept($object));
    }
}
