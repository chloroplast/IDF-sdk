<?php
namespace Sdk\Order\Adapter\DisShipAmaFTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DisShipAmaFTLOrder;

class DisShipAmaFTLOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisShipAmaFTLOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDisShipAmaFTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisShipAmaFTLOrder\IDisShipAmaFTLOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DisShipAmaFTLOrder',
            $this->stub->fetchObject(1)
        );
    }
}
