<?php
namespace Sdk\Order\Adapter\DisShipAmaFTLOrder;

use PHPUnit\Framework\TestCase;

class DisShipAmaFTLOrderRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisShipAmaFTLOrderRestfulAdapterMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIDisShipAmaFTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisShipAmaFTLOrder\IDisShipAmaFTLOrderAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullDisShipAmaFTLOrder',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'DIS_SHIP_AMAFTL_ORDER_LIST',
                DisShipAmaFTLOrderRestfulAdapter::SCENARIOS['DIS_SHIP_AMAFTL_ORDER_LIST']
            ),
            array(
                'DIS_SHIP_AMAFTL_ORDER_FETCH_ONE',
                DisShipAmaFTLOrderRestfulAdapter::SCENARIOS['DIS_SHIP_AMAFTL_ORDER_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(
            DisShipAmaFTLOrderRestfulAdapter::MAP_ERROR,
            $this->stub->getAlonePossessMapErrorsPublic()
        );
    }
}
