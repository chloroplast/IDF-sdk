<?php
namespace Sdk\Order\Adapter\RecOrderLTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\RecOrderLTLOrderMock;
use Sdk\Order\Translator\RecOrderLTLOrderRestfulTranslator;

class RecOrderLTLOrderRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(RecOrderLTLOrderRestfulAdapterMock::class)
                           ->setMethods([
                               'getTranslator',
                               'patch',
                               'getResource',
                               'isSuccess',
                               'translateToObject'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIRecOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecOrderLTLOrder\IRecOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullRecOrderLTLOrder',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'REC_ORDERLTL_ORDER_LIST', 
                RecOrderLTLOrderRestfulAdapter::SCENARIOS['REC_ORDERLTL_ORDER_LIST']
            ),
            array(
                'REC_ORDERLTL_ORDER_FETCH_ONE', 
                RecOrderLTLOrderRestfulAdapter::SCENARIOS['REC_ORDERLTL_ORDER_FETCH_ONE']
            ),
            array(
                '', 
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(RecOrderLTLOrderRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    public function testInsertTranslatorKeys()
    {
        $this->assertEquals(array(
            'carType',
            'staff',
            'orderLTL'
        ), $this->stub->insertTranslatorKeysPublic());
    }

    public function testUpdateTranslatorKeys()
    {
        $this->assertEquals(array(
            'carType',
            'staff',
            'orderLTL'
        ), $this->stub->updateTranslatorKeysPublic());
    }

    public function testEnableTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->enableTranslatorKeysPublic());
    }

    public function testDisableTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->disableTranslatorKeysPublic());
    }

    public function testDeletedTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->deletedTranslatorKeysPublic());
    }

    private function registerPickupDate(bool $result)
    {
        $id = 1;
        $resource = 'receiveOrders/orderLTL';
        $data = array();
        $keys = array('pickupDate', 'deliveryDate', 'references', 'staff');
        $object = new RecOrderLTLOrderMock($id);

        // 为 RecOrderLTLOrderRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(RecOrderLTLOrderRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with(
            $resource.'/'.$id.'/registerPickupDate',
            $data
        );
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testRegisterPickupDateTrue()
    {
        $object = $this->registerPickupDate(true);

        $result = $this->stub->registerPickupDate($object);

        $this->assertTrue($result);
    }

    public function testRegisterPickupDateFalse()
    {
        $object = $this->registerPickupDate(false);

        $result = $this->stub->registerPickupDate($object);

        $this->assertFalse($result);
    }

    private function pickup(bool $result)
    {
        $id = 1;
        $resource = 'receiveOrders/orderLTL';
        $data = array();
        $keys = array('pickUpAttachments', 'staff');
        $object = new RecOrderLTLOrderMock($id);

        // 为 RecOrderLTLOrderRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(RecOrderLTLOrderRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/pickup', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testPickupTrue()
    {
        $object = $this->pickup(true);

        $result = $this->stub->pickup($object);

        $this->assertTrue($result);
    }

    public function testPickupFalse()
    {
        $object = $this->pickup(false);

        $result = $this->stub->pickup($object);

        $this->assertFalse($result);
    }

    private function stockIn(bool $result)
    {
        $id = 1;
        $resource = 'receiveOrders/orderLTL';
        $data = array();
        $keys = array('stockInItemsAttachments', 'stockInAttachments', 'staff');
        $object = new RecOrderLTLOrderMock($id);

        // 为 RecOrderLTLOrderRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(RecOrderLTLOrderRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($object, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/stockIn', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);
        }

        return $object;
    }

    public function testStockInTrue()
    {
        $object = $this->stockIn(true);

        $result = $this->stub->stockIn($object);

        $this->assertTrue($result);
    }

    public function testStockInFalse()
    {
        $object = $this->stockIn(false);

        $result = $this->stub->stockIn($object);

        $this->assertFalse($result);
    }
}
