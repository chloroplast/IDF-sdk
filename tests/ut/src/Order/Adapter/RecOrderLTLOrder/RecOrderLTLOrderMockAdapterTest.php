<?php
namespace Sdk\Order\Adapter\RecOrderLTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\RecOrderLTLOrder;

class RecOrderLTLOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new RecOrderLTLOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRecOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecOrderLTLOrder\IRecOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\RecOrderLTLOrder',
            $this->stub->fetchObject(1)
        );
    }

    public function testRegisterPickupDate()
    {
        $object = new RecOrderLTLOrder(1);

        $this->assertTrue($this->stub->registerPickupDate($object));
    }

    public function testPickup()
    {
        $object = new RecOrderLTLOrder(1);

        $this->assertTrue($this->stub->pickup($object));
    }

    public function testStockIn()
    {
        $object = new RecOrderLTLOrder(1);

        $this->assertTrue($this->stub->stockIn($object));
    }
}
