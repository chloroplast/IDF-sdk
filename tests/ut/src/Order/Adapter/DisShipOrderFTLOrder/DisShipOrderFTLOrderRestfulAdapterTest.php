<?php
namespace Sdk\Order\Adapter\DisShipOrderFTLOrder;

use PHPUnit\Framework\TestCase;

class DisShipOrderFTLOrderRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisShipOrderFTLOrderRestfulAdapterMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIDisShipOrderFTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisShipOrderFTLOrder\IDisShipOrderFTLOrderAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullDisShipOrderFTLOrder',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'DIS_SHIP_ORDERFTL_ORDER_LIST',
                DisShipOrderFTLOrderRestfulAdapter::SCENARIOS['DIS_SHIP_ORDERFTL_ORDER_LIST']
            ),
            array(
                'DIS_SHIP_ORDERFTL_ORDER_FETCH_ONE',
                DisShipOrderFTLOrderRestfulAdapter::SCENARIOS['DIS_SHIP_ORDERFTL_ORDER_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(
            DisShipOrderFTLOrderRestfulAdapter::MAP_ERROR,
            $this->stub->getAlonePossessMapErrorsPublic()
        );
    }
}
