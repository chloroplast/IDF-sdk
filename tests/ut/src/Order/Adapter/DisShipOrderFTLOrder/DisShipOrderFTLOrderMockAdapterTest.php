<?php
namespace Sdk\Order\Adapter\DisShipOrderFTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DisShipOrderFTLOrder;

class DisShipOrderFTLOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisShipOrderFTLOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDisShipOrderFTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisShipOrderFTLOrder\IDisShipOrderFTLOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DisShipOrderFTLOrder',
            $this->stub->fetchObject(1)
        );
    }
}
