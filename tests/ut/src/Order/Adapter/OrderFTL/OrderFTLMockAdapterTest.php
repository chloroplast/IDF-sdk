<?php
namespace Sdk\Order\Adapter\OrderFTL;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\OrderFTL;
use Sdk\Application\ParseTask\Model\ParseTask;

class OrderFTLMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderFTLMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIOrderFTLAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\OrderFTL\IOrderFTLAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\OrderFTL',
            $this->stub->fetchObject(1)
        );
    }

    public function testMemberCancel()
    {
        $object = new OrderFTL(1);

        $this->assertTrue($this->stub->memberCancel($object));
    }

    public function testStaffCancel()
    {
        $object = new OrderFTL(1);

        $this->assertTrue($this->stub->staffCancel($object));
    }

    public function testConfirm()
    {
        $object = new OrderFTL(1);

        $this->assertTrue($this->stub->confirm($object));
    }

    public function testBatchConfirm()
    {
        $objectList = array(new OrderFTL(1));

        $this->assertTrue($this->stub->batchConfirm($objectList));
    }

    public function testFreeze()
    {
        $object = new OrderFTL(1);

        $this->assertTrue($this->stub->freeze($object));
    }

    public function testUnFreeze()
    {
        $object = new OrderFTL(1);

        $this->assertTrue($this->stub->unFreeze($object));
    }

    public function testParse()
    {
        $parseTask = new ParseTask(1);

        $this->assertTrue($this->stub->parse($parseTask));
    }

    public function testCalculate()
    {
        $object = new OrderFTL(1);

        $this->assertEmpty($this->stub->calculate($object));
    }
}
