<?php
namespace Sdk\Order\Adapter\DispatchOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DispatchOrder;

class DispatchOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DispatchOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDispatchOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DispatchOrder\IDispatchOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DispatchOrder',
            $this->stub->fetchObject(1)
        );
    }

    public function testUpdateISA()
    {
        $object = new DispatchOrder(1);

        $this->assertTrue($this->stub->updateISA($object));
    }

    public function testRegisterDispatchDate()
    {
        $object = new DispatchOrder(1);

        $this->assertTrue($this->stub->registerDispatchDate($object));
    }

    public function testDispatch()
    {
        $object = new DispatchOrder(1);

        $this->assertTrue($this->stub->dispatch($object));
    }

    public function testAccept()
    {
        $object = new DispatchOrder(1);

        $this->assertTrue($this->stub->accept($object));
    }
}
