<?php
namespace Sdk\Order\Adapter\DisOrderLTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DisOrderLTLOrder;

class DisOrderLTLOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisOrderLTLOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDisOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisOrderLTLOrder\IDisOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DisOrderLTLOrder',
            $this->stub->fetchObject(1)
        );
    }

    public function testRegisterDispatchDate()
    {
        $object = new DisOrderLTLOrder(1);

        $this->assertTrue($this->stub->registerDispatchDate($object));
    }

    public function testDispatch()
    {
        $object = new DisOrderLTLOrder(1);

        $this->assertTrue($this->stub->dispatch($object));
    }

    public function testAccept()
    {
        $object = new DisOrderLTLOrder(1);

        $this->assertTrue($this->stub->accept($object));
    }
}
