<?php
namespace Sdk\Order\Adapter\DisShipOrderLTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DisShipOrderLTLOrder;

class DisShipOrderLTLOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisShipOrderLTLOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDisShipOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisShipOrderLTLOrder\IDisShipOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DisShipOrderLTLOrder',
            $this->stub->fetchObject(1)
        );
    }
}
