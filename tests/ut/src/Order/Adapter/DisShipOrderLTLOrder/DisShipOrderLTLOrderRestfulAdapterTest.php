<?php
namespace Sdk\Order\Adapter\DisShipOrderLTLOrder;

use PHPUnit\Framework\TestCase;

class DisShipOrderLTLOrderRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisShipOrderLTLOrderRestfulAdapterMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIDisShipOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisShipOrderLTLOrder\IDisShipOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullDisShipOrderLTLOrder',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'DIS_SHIP_ORDERLTL_ORDER_LIST',
                DisShipOrderLTLOrderRestfulAdapter::SCENARIOS['DIS_SHIP_ORDERLTL_ORDER_LIST']
            ),
            array(
                'DIS_SHIP_ORDERLTL_ORDER_FETCH_ONE',
                DisShipOrderLTLOrderRestfulAdapter::SCENARIOS['DIS_SHIP_ORDERLTL_ORDER_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(
            DisShipOrderLTLOrderRestfulAdapter::MAP_ERROR,
            $this->stub->getAlonePossessMapErrorsPublic()
        );
    }
}
