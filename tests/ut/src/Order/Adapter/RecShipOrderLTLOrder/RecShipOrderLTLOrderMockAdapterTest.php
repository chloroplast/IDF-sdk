<?php
namespace Sdk\Order\Adapter\RecShipOrderLTLOrder;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\RecShipOrderLTLOrder;

class RecShipOrderLTLOrderMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new RecShipOrderLTLOrderMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRecShipOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecShipOrderLTLOrder\IRecShipOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\RecShipOrderLTLOrder',
            $this->stub->fetchObject(1)
        );
    }
}
