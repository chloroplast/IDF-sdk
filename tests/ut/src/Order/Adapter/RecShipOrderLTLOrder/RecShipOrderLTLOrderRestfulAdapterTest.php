<?php
namespace Sdk\Order\Adapter\RecShipOrderLTLOrder;

use PHPUnit\Framework\TestCase;

class RecShipOrderLTLOrderRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new RecShipOrderLTLOrderRestfulAdapterMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIRecShipOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecShipOrderLTLOrder\IRecShipOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullRecShipOrderLTLOrder',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'REC_SHIP_ORDERLTL_ORDER_LIST',
                RecShipOrderLTLOrderRestfulAdapter::SCENARIOS['REC_SHIP_ORDERLTL_ORDER_LIST']
            ),
            array(
                'REC_SHIP_ORDERLTL_ORDER_FETCH_ONE',
                RecShipOrderLTLOrderRestfulAdapter::SCENARIOS['REC_SHIP_ORDERLTL_ORDER_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(
            RecShipOrderLTLOrderRestfulAdapter::MAP_ERROR,
            $this->stub->getAlonePossessMapErrorsPublic()
        );
    }
}
