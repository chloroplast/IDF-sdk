<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class DisShipOrderFTLOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisShipOrderFTLOrder();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * DisShipOrderFTLOrder 领域对象,测试构造函数
     */
    public function testDisShipOrderFTLOrderConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\Order\Model\DisOrderFTLOrder', $this->stub->getDisOrderFTLOrder());
        $this->assertInstanceOf('Sdk\Order\Model\OrderFTL', $this->stub->getOrderFTL());
        $this->assertEmpty($this->stub->getDispatchItemsAttachments());
        $this->assertEmpty($this->stub->getAcceptItemsAttachments());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 DisShipOrderFTLOrder setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 DisShipOrderFTLOrder setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //disOrderFTLOrder 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderFTLOrder setDisOrderFTLOrder() 正确的传参类型,期望传值正确
     */
    public function testSetDisOrderFTLOrderCorrectType()
    {
        $disOrderFTLOrder = new DisOrderFTLOrder();
        $this->stub->setDisOrderFTLOrder($disOrderFTLOrder);
        $this->assertEquals($disOrderFTLOrder, $this->stub->getDisOrderFTLOrder());
    }

    /**
     * 设置 DisShipOrderFTLOrder setDisOrderFTLOrder() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDisOrderFTLOrderWrongType()
    {
        $this->stub->setDisOrderFTLOrder(array('disOrderFTLOrder'));
    }
    //disOrderFTLOrder 测试 --------------------------------------------------------   end

    //orderFTL 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderFTLOrder setOrderFTL() 正确的传参类型,期望传值正确
     */
    public function testSetOrderFTLCorrectType()
    {
        $orderFTL = new OrderFTL();
        $this->stub->setOrderFTL($orderFTL);
        $this->assertEquals($orderFTL, $this->stub->getOrderFTL());
    }

    /**
     * 设置 DisShipOrderFTLOrder setOrderFTL() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOrderFTLWrongType()
    {
        $this->stub->setOrderFTL(array('orderFTL'));
    }
    //orderFTL 测试 --------------------------------------------------------   end

    //dispatchItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderFTLOrder setDispatchItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetDispatchItemsAttachmentsCorrectType()
    {
        $this->stub->setDispatchItemsAttachments(array('dispatchItemsAttachments'));
        $this->assertEquals(array('dispatchItemsAttachments'), $this->stub->getDispatchItemsAttachments());
    }

    /**
     * 设置 DisShipOrderFTLOrder setDispatchItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchItemsAttachmentsWrongType()
    {
        $this->stub->setDispatchItemsAttachments('dispatchItemsAttachments');
    }
    //dispatchItemsAttachments 测试 --------------------------------------------------------   end

    //acceptItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderFTLOrder setAcceptItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptItemsAttachmentsCorrectType()
    {
        $this->stub->setAcceptItemsAttachments(array('acceptItemsAttachments'));
        $this->assertEquals(array('acceptItemsAttachments'), $this->stub->getAcceptItemsAttachments());
    }

    /**
     * 设置 DisShipOrderFTLOrder setAcceptItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptItemsAttachmentsWrongType()
    {
        $this->stub->setAcceptItemsAttachments('acceptItemsAttachments');
    }
    //acceptItemsAttachments 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderFTLOrder setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 DisShipOrderFTLOrder setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end
}
