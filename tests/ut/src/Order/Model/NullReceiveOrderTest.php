<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullReceiveOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullReceiveOrder::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsRole()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\ReceiveOrder',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullReceiveOrderMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function initOperation($method)
    {
        $stub = $this->getMockBuilder(NullReceiveOrderMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
        $stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $stub->$method();
 
        $this->assertFalse($result);
    }

    public function testRegisterPickupDate()
    {
        $this->initOperation('registerPickupDate');
    }

    public function testPickup()
    {
        $this->initOperation('pickup');
    }

    public function testStockIn()
    {
        $this->initOperation('stockIn');
    }
}
