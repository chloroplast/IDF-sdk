<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Repository\DisAmaFTLOrderRepository;

use Sdk\User\Staff\Model\Staff;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class DisAmaFTLOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(DisAmaFTLOrderMock::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * DisAmaFTLOrder 领域对象,测试构造函数
     */
    public function testDisAmaFTLOrderConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertEmpty($this->stub->getNumber());
        $this->assertEmpty($this->stub->getTotalWeight());
        $this->assertEmpty($this->stub->getTotalPalletNumber());
        $this->assertEmpty($this->stub->getTotalOrderNumber());
        $this->assertEmpty($this->stub->getAmazonFTL());
        $this->assertEmpty($this->stub->getReferences());
        $this->assertEmpty($this->stub->getDispatchItemsAttachments());
        $this->assertEmpty($this->stub->getAcceptItemsAttachments());
        $this->assertEmpty($this->stub->getDispatchAttachments());
        $this->assertEmpty($this->stub->getAcceptAttachments());
        $this->assertEmpty($this->stub->getIsaNumber());
        $this->assertEmpty($this->stub->getPickupDate());
        $this->assertEmpty($this->stub->getDeliveryDate());
        $this->assertEmpty($this->stub->getClosePage());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 DisAmaFTLOrder setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 DisAmaFTLOrder setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //number 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }

    /**
     * 设置 DisAmaFTLOrder setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array('number'));
    }
    //number 测试 --------------------------------------------------------   end

    //totalWeight 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setTotalWeight() 正确的传参类型,期望传值正确
     */
    public function testSetTotalWeightCorrectType()
    {
        $this->stub->setTotalWeight(1);
        $this->assertEquals(1, $this->stub->getTotalWeight());
    }

    /**
     * 设置 DisAmaFTLOrder setTotalWeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalWeightWrongType()
    {
        $this->stub->setTotalWeight('totalWeight');
    }
    //totalWeight 测试 --------------------------------------------------------   end

    //totalPalletNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setTotalPalletNumber() 正确的传参类型,期望传值正确
     */
    public function testSetTotalPalletNumberCorrectType()
    {
        $this->stub->setTotalPalletNumber(1);
        $this->assertEquals(1, $this->stub->getTotalPalletNumber());
    }

    /**
     * 设置 DisAmaFTLOrder setTotalPalletNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalPalletNumberWrongType()
    {
        $this->stub->setTotalPalletNumber('totalPalletNumber');
    }
    //totalPalletNumber 测试 --------------------------------------------------------   end

    //totalOrderNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setTotalOrderNumber() 正确的传参类型,期望传值正确
     */
    public function testSetTotalOrderNumberCorrectType()
    {
        $this->stub->setTotalOrderNumber(1);
        $this->assertEquals(1, $this->stub->getTotalOrderNumber());
    }

    /**
     * 设置 DisAmaFTLOrder setTotalOrderNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalOrderNumberWrongType()
    {
        $this->stub->setTotalOrderNumber('totalOrderNumber');
    }
    //totalOrderNumber 测试 --------------------------------------------------------   end

    //amazonFTL 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setAmazonFTL() 正确的传参类型,期望传值正确
     */
    public function testSetAmazonFTLCorrectType()
    {
        $this->stub->setAmazonFTL(array('amazonFTL'));
        $this->assertEquals(array('amazonFTL'), $this->stub->getAmazonFTL());
    }

    /**
     * 设置 DisAmaFTLOrder setAmazonFTL() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAmazonFTLWrongType()
    {
        $this->stub->setAmazonFTL('amazonFTL');
    }
    //amazonFTL 测试 --------------------------------------------------------   end

    //references 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setReferences() 正确的传参类型,期望传值正确
     */
    public function testSetReferencesCorrectType()
    {
        $this->stub->setReferences('references');
        $this->assertEquals('references', $this->stub->getReferences());
    }

    /**
     * 设置 DisAmaFTLOrder setReferences() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReferencesWrongType()
    {
        $this->stub->setReferences(array('references'));
    }
    //references 测试 --------------------------------------------------------   end

    //dispatchItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setDispatchItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetDispatchItemsAttachmentsCorrectType()
    {
        $this->stub->setDispatchItemsAttachments(array('dispatchItemsAttachments'));
        $this->assertEquals(array('dispatchItemsAttachments'), $this->stub->getDispatchItemsAttachments());
    }

    /**
     * 设置 DisAmaFTLOrder setDispatchItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchItemsAttachmentsWrongType()
    {
        $this->stub->setDispatchItemsAttachments('dispatchItemsAttachments');
    }
    //dispatchItemsAttachments 测试 --------------------------------------------------------   end

    //acceptItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setAcceptItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptItemsAttachmentsCorrectType()
    {
        $this->stub->setAcceptItemsAttachments(array('acceptItemsAttachments'));
        $this->assertEquals(array('acceptItemsAttachments'), $this->stub->getAcceptItemsAttachments());
    }

    /**
     * 设置 DisAmaFTLOrder setAcceptItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptItemsAttachmentsWrongType()
    {
        $this->stub->setAcceptItemsAttachments('acceptItemsAttachments');
    }
    //acceptItemsAttachments 测试 --------------------------------------------------------   end

    //dispatchAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setDispatchAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetDispatchAttachmentsCorrectType()
    {
        $this->stub->setDispatchAttachments(array('dispatchAttachments'));
        $this->assertEquals(array('dispatchAttachments'), $this->stub->getDispatchAttachments());
    }

    /**
     * 设置 DisAmaFTLOrder setDispatchAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchAttachmentsWrongType()
    {
        $this->stub->setDispatchAttachments('dispatchAttachments');
    }
    //dispatchAttachments 测试 --------------------------------------------------------   end

    //acceptAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setAcceptAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptAttachmentsCorrectType()
    {
        $this->stub->setAcceptAttachments(array('acceptAttachments'));
        $this->assertEquals(array('acceptAttachments'), $this->stub->getAcceptAttachments());
    }

    /**
     * 设置 DisAmaFTLOrder setAcceptAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptAttachmentsWrongType()
    {
        $this->stub->setAcceptAttachments('acceptAttachments');
    }
    //acceptAttachments 测试 --------------------------------------------------------   end

    //isaNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setIsaNumber() 正确的传参类型,期望传值正确
     */
    public function testSetIsaNumberCorrectType()
    {
        $this->stub->setIsaNumber('isaNumber');
        $this->assertEquals('isaNumber', $this->stub->getIsaNumber());
    }

    /**
     * 设置 DisAmaFTLOrder setIsaNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIsaNumberWrongType()
    {
        $this->stub->setIsaNumber(array('isaNumber'));
    }
    //isaNumber 测试 --------------------------------------------------------   end

    //pickupDate 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setPickupDate() 正确的传参类型,期望传值正确
     */
    public function testSetPickupDateCorrectType()
    {
        $this->stub->setPickupDate(1);
        $this->assertEquals(1, $this->stub->getPickupDate());
    }

    /**
     * 设置 DisAmaFTLOrder setPickupDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupDateWrongType()
    {
        $this->stub->setPickupDate('pickupDate');
    }
    //pickupDate 测试 --------------------------------------------------------   end

    //deliveryDate 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setDeliveryDate() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryDateCorrectType()
    {
        $this->stub->setDeliveryDate(1);
        $this->assertEquals(1, $this->stub->getDeliveryDate());
    }

    /**
     * 设置 DisAmaFTLOrder setDeliveryDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryDateWrongType()
    {
        $this->stub->setDeliveryDate('deliveryDate');
    }
    //deliveryDate 测试 --------------------------------------------------------   end

    //closePage 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setClosePage() 正确的传参类型,期望传值正确
     */
    public function testSetClosePageCorrectType()
    {
        $this->stub->setClosePage(array('closePage'));
        $this->assertEquals(array('closePage'), $this->stub->getClosePage());
    }

    /**
     * 设置 DisAmaFTLOrder setClosePage() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetClosePageWrongType()
    {
        $this->stub->setClosePage('closePage');
    }
    //closePage 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 DisAmaFTLOrder setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 DisAmaFTLOrder setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new DisAmaFTLOrderMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\DisAmaFTLOrderRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testUpdateISA()
    {
        // 为 DisAmaFTLOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(DisAmaFTLOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->updateISA($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->updateISA();

        $this->assertTrue($result);
    }

    public function testRegisterDispatchDate()
    {
        // 为 DisAmaFTLOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(DisAmaFTLOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->registerDispatchDate($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->registerDispatchDate();

        $this->assertTrue($result);
    }

    public function testDispatch()
    {
        // 为 DisAmaFTLOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(DisAmaFTLOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->dispatch($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->dispatch();

        $this->assertTrue($result);
    }

    public function testAccept()
    {
        // 为 DisAmaFTLOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(DisAmaFTLOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->accept($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->accept();

        $this->assertTrue($result);
    }
}
