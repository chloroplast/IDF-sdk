<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Warehouse\Model\Warehouse;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ReceiveShippingOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ReceiveShippingOrder();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * ReceiveShippingOrder 领域对象,测试构造函数
     */
    public function testReceiveShippingOrderConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\Order\Model\ReceiveOrder', $this->stub->getReceiveOrder());
        $this->assertInstanceOf('Sdk\Order\Model\AmazonLTL', $this->stub->getAmazonLTL());
        $this->assertEmpty($this->stub->getStockInItemsAttachments());
        $this->assertInstanceOf('Sdk\Warehouse\Model\Warehouse', $this->stub->getPickupWarehouse());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 ReceiveShippingOrder setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 ReceiveShippingOrder setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //receiveOrder 测试 -------------------------------------------------------- start
    /**
     * 设置 ReceiveShippingOrder setReceiveOrder() 正确的传参类型,期望传值正确
     */
    public function testSetReceiveOrderCorrectType()
    {
        $receiveOrder = new ReceiveOrder();
        $this->stub->setReceiveOrder($receiveOrder);
        $this->assertEquals($receiveOrder, $this->stub->getReceiveOrder());
    }

    /**
     * 设置 ReceiveShippingOrder setReceiveOrder() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReceiveOrderWrongType()
    {
        $this->stub->setReceiveOrder(array('receiveOrder'));
    }
    //receiveOrder 测试 --------------------------------------------------------   end

    //amazonLTL 测试 -------------------------------------------------------- start
    /**
     * 设置 ReceiveShippingOrder setAmazonLTL() 正确的传参类型,期望传值正确
     */
    public function testSetAmazonLTLCorrectType()
    {
        $amazonLTL = new AmazonLTL();
        $this->stub->setAmazonLTL($amazonLTL);
        $this->assertEquals($amazonLTL, $this->stub->getAmazonLTL());
    }

    /**
     * 设置 ReceiveShippingOrder setAmazonLTL() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAmazonLTLWrongType()
    {
        $this->stub->setAmazonLTL(array('amazonLTL'));
    }
    //amazonLTL 测试 --------------------------------------------------------   end

    //stockInItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 ReceiveShippingOrder setStockInItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetStockInItemsAttachmentsCorrectType()
    {
        $this->stub->setStockInItemsAttachments(array('stockInItemsAttachments'));
        $this->assertEquals(array('stockInItemsAttachments'), $this->stub->getStockInItemsAttachments());
    }

    /**
     * 设置 ReceiveShippingOrder setStockInItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStockInItemsAttachmentsWrongType()
    {
        $this->stub->setStockInItemsAttachments('stockInItemsAttachments');
    }
    //stockInItemsAttachments 测试 --------------------------------------------------------   end

    //pickupWarehouse 测试 -------------------------------------------------------- start
    /**
     * 设置 ReceiveShippingOrder setPickupWarehouse() 正确的传参类型,期望传值正确
     */
    public function testSetPickupWarehouseCorrectType()
    {
        $pickupWarehouse = new Warehouse();
        $this->stub->setPickupWarehouse($pickupWarehouse);
        $this->assertEquals($pickupWarehouse, $this->stub->getPickupWarehouse());
    }

    /**
     * 设置 ReceiveShippingOrder setPickupWarehouse() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupWarehouseWrongType()
    {
        $this->stub->setPickupWarehouse(array('pickupWarehouse'));
    }
    //pickupWarehouse 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 ReceiveShippingOrder setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 ReceiveShippingOrder setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end
}
