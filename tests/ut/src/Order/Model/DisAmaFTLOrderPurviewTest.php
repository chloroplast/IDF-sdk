<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class DisAmaFTLOrderPurviewTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(DisAmaFTLOrderPurview::class)
                           ->setMethods(['operation'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsPurview()
    {
        $this->assertInstanceOf(
            'Sdk\Role\Purview\Model\Purview',
            $this->stub
        );
    }

    protected function initOperation($method)
    {
        $this->stub->expects($this->exactly(1))->method('operation')->with($method)->willReturn(true);

        $result = $this->stub->$method();

        $this->assertTrue($result);
    }

    public function testUpdateISA()
    {
        $this->initOperation('updateISA');
    }

    public function testRegisterDispatchDate()
    {
        $this->initOperation('registerDispatchDate');
    }

    public function testDispatch()
    {
        $this->initOperation('dispatch');
    }

    public function testAccept()
    {
        $this->initOperation('accept');
    }
}
