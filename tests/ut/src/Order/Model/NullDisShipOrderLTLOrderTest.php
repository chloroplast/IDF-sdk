<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullDisShipOrderLTLOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullDisShipOrderLTLOrder::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsRole()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DisShipOrderLTLOrder',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullDisShipOrderLTLOrderMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
