<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Repository\DispatchOrderRepository;

use Sdk\CarType\Model\CarType;
use Sdk\User\Staff\Model\Staff;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class DispatchOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(DispatchOrderMock::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * DispatchOrder 领域对象,测试构造函数
     */
    public function testDispatchOrderConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\CarType\Model\CarType', $this->stub->getCarType());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertEmpty($this->stub->getNumber());
        $this->assertEmpty($this->stub->getTotalWeight());
        $this->assertEmpty($this->stub->getTotalPalletNumber());
        $this->assertEmpty($this->stub->getTotalOrderNumber());
        $this->assertEmpty($this->stub->getAmazonLTL());
        $this->assertEmpty($this->stub->getReferences());
        $this->assertEmpty($this->stub->getDispatchItemsAttachments());
        $this->assertEmpty($this->stub->getAcceptItemsAttachments());
        $this->assertEmpty($this->stub->getDispatchAttachments());
        $this->assertEmpty($this->stub->getAcceptAttachments());
        $this->assertEmpty($this->stub->getIsaNumber());
        $this->assertEmpty($this->stub->getPickupDate());
        $this->assertEmpty($this->stub->getDeliveryDate());
        $this->assertEmpty($this->stub->getClosePage());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 DispatchOrder setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //carType 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setCarType() 正确的传参类型,期望传值正确
     */
    public function testSetCarTypeCorrectType()
    {
        $carType = new CarType();
        $this->stub->setCarType($carType);
        $this->assertEquals($carType, $this->stub->getCarType());
    }

    /**
     * 设置 DispatchOrder setCarType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCarTypeWrongType()
    {
        $this->stub->setCarType(array('carType'));
    }
    //carType 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 DispatchOrder setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //number 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }

    /**
     * 设置 DispatchOrder setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array('number'));
    }
    //number 测试 --------------------------------------------------------   end

    //totalWeight 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setTotalWeight() 正确的传参类型,期望传值正确
     */
    public function testSetTotalWeightCorrectType()
    {
        $this->stub->setTotalWeight(1);
        $this->assertEquals(1, $this->stub->getTotalWeight());
    }

    /**
     * 设置 DispatchOrder setTotalWeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalWeightWrongType()
    {
        $this->stub->setTotalWeight('totalWeight');
    }
    //totalWeight 测试 --------------------------------------------------------   end

    //totalPalletNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setTotalPalletNumber() 正确的传参类型,期望传值正确
     */
    public function testSetTotalPalletNumberCorrectType()
    {
        $this->stub->setTotalPalletNumber(1);
        $this->assertEquals(1, $this->stub->getTotalPalletNumber());
    }

    /**
     * 设置 DispatchOrder setTotalPalletNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalPalletNumberWrongType()
    {
        $this->stub->setTotalPalletNumber('totalPalletNumber');
    }
    //totalPalletNumber 测试 --------------------------------------------------------   end

    //totalOrderNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setTotalOrderNumber() 正确的传参类型,期望传值正确
     */
    public function testSetTotalOrderNumberCorrectType()
    {
        $this->stub->setTotalOrderNumber(1);
        $this->assertEquals(1, $this->stub->getTotalOrderNumber());
    }

    /**
     * 设置 DispatchOrder setTotalOrderNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalOrderNumberWrongType()
    {
        $this->stub->setTotalOrderNumber('totalOrderNumber');
    }
    //totalOrderNumber 测试 --------------------------------------------------------   end

    //amazonLTL 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setAmazonLTL() 正确的传参类型,期望传值正确
     */
    public function testSetAmazonLTLCorrectType()
    {
        $this->stub->setAmazonLTL(array('amazonLTL'));
        $this->assertEquals(array('amazonLTL'), $this->stub->getAmazonLTL());
    }

    /**
     * 设置 DispatchOrder setAmazonLTL() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAmazonLTLWrongType()
    {
        $this->stub->setAmazonLTL('amazonLTL');
    }
    //amazonLTL 测试 --------------------------------------------------------   end

    //references 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setReferences() 正确的传参类型,期望传值正确
     */
    public function testSetReferencesCorrectType()
    {
        $this->stub->setReferences('references');
        $this->assertEquals('references', $this->stub->getReferences());
    }

    /**
     * 设置 DispatchOrder setReferences() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReferencesWrongType()
    {
        $this->stub->setReferences(array('references'));
    }
    //references 测试 --------------------------------------------------------   end

    //dispatchItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setDispatchItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetDispatchItemsAttachmentsCorrectType()
    {
        $this->stub->setDispatchItemsAttachments(array('dispatchItemsAttachments'));
        $this->assertEquals(array('dispatchItemsAttachments'), $this->stub->getDispatchItemsAttachments());
    }

    /**
     * 设置 DispatchOrder setDispatchItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchItemsAttachmentsWrongType()
    {
        $this->stub->setDispatchItemsAttachments('dispatchItemsAttachments');
    }
    //dispatchItemsAttachments 测试 --------------------------------------------------------   end

    //acceptItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setAcceptItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptItemsAttachmentsCorrectType()
    {
        $this->stub->setAcceptItemsAttachments(array('acceptItemsAttachments'));
        $this->assertEquals(array('acceptItemsAttachments'), $this->stub->getAcceptItemsAttachments());
    }

    /**
     * 设置 DispatchOrder setAcceptItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptItemsAttachmentsWrongType()
    {
        $this->stub->setAcceptItemsAttachments('acceptItemsAttachments');
    }
    //acceptItemsAttachments 测试 --------------------------------------------------------   end

    //dispatchAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setDispatchAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetDispatchAttachmentsCorrectType()
    {
        $this->stub->setDispatchAttachments(array('dispatchAttachments'));
        $this->assertEquals(array('dispatchAttachments'), $this->stub->getDispatchAttachments());
    }

    /**
     * 设置 DispatchOrder setDispatchAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchAttachmentsWrongType()
    {
        $this->stub->setDispatchAttachments('dispatchAttachments');
    }
    //dispatchAttachments 测试 --------------------------------------------------------   end

    //acceptAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setAcceptAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptAttachmentsCorrectType()
    {
        $this->stub->setAcceptAttachments(array('acceptAttachments'));
        $this->assertEquals(array('acceptAttachments'), $this->stub->getAcceptAttachments());
    }

    /**
     * 设置 DispatchOrder setAcceptAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptAttachmentsWrongType()
    {
        $this->stub->setAcceptAttachments('acceptAttachments');
    }
    //acceptAttachments 测试 --------------------------------------------------------   end

    //isaNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setIsaNumber() 正确的传参类型,期望传值正确
     */
    public function testSetIsaNumberCorrectType()
    {
        $this->stub->setIsaNumber('isaNumber');
        $this->assertEquals('isaNumber', $this->stub->getIsaNumber());
    }

    /**
     * 设置 DispatchOrder setIsaNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIsaNumberWrongType()
    {
        $this->stub->setIsaNumber(array('isaNumber'));
    }
    //isaNumber 测试 --------------------------------------------------------   end

    //pickupDate 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setPickupDate() 正确的传参类型,期望传值正确
     */
    public function testSetPickupDateCorrectType()
    {
        $this->stub->setPickupDate(1);
        $this->assertEquals(1, $this->stub->getPickupDate());
    }

    /**
     * 设置 DispatchOrder setPickupDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupDateWrongType()
    {
        $this->stub->setPickupDate('pickupDate');
    }
    //pickupDate 测试 --------------------------------------------------------   end

    //deliveryDate 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setDeliveryDate() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryDateCorrectType()
    {
        $this->stub->setDeliveryDate(1);
        $this->assertEquals(1, $this->stub->getDeliveryDate());
    }

    /**
     * 设置 DispatchOrder setDeliveryDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryDateWrongType()
    {
        $this->stub->setDeliveryDate('deliveryDate');
    }
    //deliveryDate 测试 --------------------------------------------------------   end

    //closePage 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setClosePage() 正确的传参类型,期望传值正确
     */
    public function testSetClosePageCorrectType()
    {
        $this->stub->setClosePage(array('closePage'));
        $this->assertEquals(array('closePage'), $this->stub->getClosePage());
    }

    /**
     * 设置 DispatchOrder setClosePage() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetClosePageWrongType()
    {
        $this->stub->setClosePage('closePage');
    }
    //closePage 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchOrder setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 DispatchOrder setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new DispatchOrderMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\DispatchOrderRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testUpdateISA()
    {
        // 为 DispatchOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(DispatchOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->updateISA($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->updateISA();

        $this->assertTrue($result);
    }

    public function testRegisterDispatchDate()
    {
        // 为 DispatchOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(DispatchOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->registerDispatchDate($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->registerDispatchDate();

        $this->assertTrue($result);
    }

    public function testDispatch()
    {
        // 为 DispatchOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(DispatchOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->dispatch($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->dispatch();

        $this->assertTrue($result);
    }

    public function testAccept()
    {
        // 为 DispatchOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(DispatchOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->accept($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->accept();

        $this->assertTrue($result);
    }
}
