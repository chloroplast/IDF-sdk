<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullAmazonLTLTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullAmazonLTL::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsRole()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\AmazonLTL',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullAmazonLTLMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function initOperation($method)
    {
        $stub = $this->getMockBuilder(NullAmazonLTLMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
        $stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $stub->$method();
 
        $this->assertFalse($result);
    }

    public function testMemberCancel()
    {
        $this->initOperation('memberCancel');
    }

    public function testStaffCancel()
    {
        $this->initOperation('staffCancel');
    }

    public function testConfirm()
    {
        $this->initOperation('confirm');
    }

    public function testBatchConfirm()
    {
        $objectList = array();
        $stub = $this->getMockBuilder(NullAmazonLTLMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
        $stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $stub->batchConfirm($objectList);
 
        $this->assertFalse($result);
    }

    public function testRegisterPosition()
    {
        $this->initOperation('registerPosition');
    }

    public function testFreeze()
    {
        $this->initOperation('freeze');
    }

    public function testUnFreeze()
    {
        $this->initOperation('unFreeze');
    }

    public function testAutoGroup()
    {
        $list = array();
        $result = $this->stub->autoGroup($list);
        $this->assertEquals($result, $list);
    }

    public function testCalculate()
    {
        $list = array();
        $result = $this->stub->calculate();
        $this->assertEquals($result, $list);
    }
}
