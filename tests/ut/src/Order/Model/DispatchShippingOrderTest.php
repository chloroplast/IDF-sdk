<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class DispatchShippingOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DispatchShippingOrder();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * DispatchShippingOrder 领域对象,测试构造函数
     */
    public function testDispatchShippingOrderConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\Order\Model\DispatchOrder', $this->stub->getDispatchOrder());
        $this->assertInstanceOf('Sdk\Order\Model\AmazonLTL', $this->stub->getAmazonLTL());
        $this->assertEmpty($this->stub->getDispatchItemsAttachments());
        $this->assertEmpty($this->stub->getAcceptItemsAttachments());
        $this->assertEmpty($this->stub->getPosition());
        $this->assertEmpty($this->stub->getStockInNumber());
        $this->assertEmpty($this->stub->getStockInTime());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 DispatchShippingOrder setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 DispatchShippingOrder setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //dispatchOrder 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchShippingOrder setDispatchOrder() 正确的传参类型,期望传值正确
     */
    public function testSetDispatchOrderCorrectType()
    {
        $dispatchOrder = new DispatchOrder();
        $this->stub->setDispatchOrder($dispatchOrder);
        $this->assertEquals($dispatchOrder, $this->stub->getDispatchOrder());
    }

    /**
     * 设置 DispatchShippingOrder setDispatchOrder() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchOrderWrongType()
    {
        $this->stub->setDispatchOrder(array('dispatchOrder'));
    }
    //dispatchOrder 测试 --------------------------------------------------------   end

    //amazonLTL 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchShippingOrder setAmazonLTL() 正确的传参类型,期望传值正确
     */
    public function testSetAmazonLTLCorrectType()
    {
        $amazonLTL = new AmazonLTL();
        $this->stub->setAmazonLTL($amazonLTL);
        $this->assertEquals($amazonLTL, $this->stub->getAmazonLTL());
    }

    /**
     * 设置 DispatchShippingOrder setAmazonLTL() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAmazonLTLWrongType()
    {
        $this->stub->setAmazonLTL(array('amazonLTL'));
    }
    //amazonLTL 测试 --------------------------------------------------------   end

    //dispatchItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchShippingOrder setDispatchItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetDispatchItemsAttachmentsCorrectType()
    {
        $this->stub->setDispatchItemsAttachments(array('dispatchItemsAttachments'));
        $this->assertEquals(array('dispatchItemsAttachments'), $this->stub->getDispatchItemsAttachments());
    }

    /**
     * 设置 DispatchShippingOrder setDispatchItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchItemsAttachmentsWrongType()
    {
        $this->stub->setDispatchItemsAttachments('dispatchItemsAttachments');
    }
    //dispatchItemsAttachments 测试 --------------------------------------------------------   end

    //acceptItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchShippingOrder setAcceptItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptItemsAttachmentsCorrectType()
    {
        $this->stub->setAcceptItemsAttachments(array('acceptItemsAttachments'));
        $this->assertEquals(array('acceptItemsAttachments'), $this->stub->getAcceptItemsAttachments());
    }

    /**
     * 设置 DispatchShippingOrder setAcceptItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptItemsAttachmentsWrongType()
    {
        $this->stub->setAcceptItemsAttachments('acceptItemsAttachments');
    }
    //acceptItemsAttachments 测试 --------------------------------------------------------   end

    //position 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchShippingOrder setPosition() 正确的传参类型,期望传值正确
     */
    public function testSetPositionCorrectType()
    {
        $this->stub->setPosition('position');
        $this->assertEquals('position', $this->stub->getPosition());
    }

    /**
     * 设置 DispatchShippingOrder setPosition() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPositionWrongType()
    {
        $this->stub->setPosition(array('position'));
    }
    //position 测试 --------------------------------------------------------   end

    //stockInNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchShippingOrder setStockInNumber() 正确的传参类型,期望传值正确
     */
    public function testSetStockInNumberCorrectType()
    {
        $this->stub->setStockInNumber('stockInNumber');
        $this->assertEquals('stockInNumber', $this->stub->getStockInNumber());
    }

    /**
     * 设置 DispatchShippingOrder setStockInNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStockInNumberWrongType()
    {
        $this->stub->setStockInNumber(array('stockInNumber'));
    }
    //stockInNumber 测试 --------------------------------------------------------   end

    //stockInTime 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchShippingOrder setStockInTime() 正确的传参类型,期望传值正确
     */
    public function testSetStockInTimeCorrectType()
    {
        $this->stub->setStockInTime(0);
        $this->assertEquals(0, $this->stub->getStockInTime());
    }

    /**
     * 设置 DispatchShippingOrder setStockInTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStockInTimeWrongType()
    {
        $this->stub->setStockInTime('stockInTime');
    }
    //stockInTime 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 DispatchShippingOrder setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 DispatchShippingOrder setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end
}
