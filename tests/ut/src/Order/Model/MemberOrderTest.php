<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Repository\MemberOrderRepository;

use Sdk\User\Member\Model\Member;
use Sdk\User\Staff\Model\Staff;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MemberOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new MemberOrder();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * MemberOrder 领域对象,测试构造函数
     */
    public function testMemberOrderConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\User\Member\Model\Member', $this->stub->getMember());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertEmpty($this->stub->getNumber());
        $this->assertEmpty($this->stub->getPrice());
        $this->assertEmpty($this->stub->getAttribute());
        $this->assertEmpty($this->stub->getType());
        $this->assertEquals(MemberOrder::IDF_PICKUP['AMAZON'], $this->stub->getIdfPickup());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 MemberOrder setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 MemberOrder setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 MemberOrder setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $member = new Member();
        $this->stub->setMember($member);
        $this->assertEquals($member, $this->stub->getMember());
    }

    /**
     * 设置 MemberOrder setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $this->stub->setMember(array('member'));
    }
    //member 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 MemberOrder setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 MemberOrder setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //number 测试 -------------------------------------------------------- start
    /**
     * 设置 MemberOrder setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }

    /**
     * 设置 MemberOrder setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array('number'));
    }
    //number 测试 --------------------------------------------------------   end

    //price 测试 -------------------------------------------------------- start
    /**
     * 设置 MemberOrder setPrice() 正确的传参类型,期望传值正确
     */
    public function testSetPriceCorrectType()
    {
        $this->stub->setPrice(1);
        $this->assertEquals(1, $this->stub->getPrice());
    }

    /**
     * 设置 MemberOrder setPrice() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceWrongType()
    {
        $this->stub->setPrice('price');
    }
    //price 测试 --------------------------------------------------------   end

    //attribute 测试 -------------------------------------------------------- start
    /**
     * 设置 MemberOrder setAttribute() 正确的传参类型,期望传值正确
     */
    public function testSetAttributeCorrectType()
    {
        $this->stub->setAttribute(1);
        $this->assertEquals(1, $this->stub->getAttribute());
    }

    /**
     * 设置 MemberOrder setAttribute() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAttributeWrongType()
    {
        $this->stub->setAttribute('attribute');
    }
    //attribute 测试 --------------------------------------------------------   end

    //type 测试 -------------------------------------------------------- start
    /**
     * 设置 MemberOrder setType() 正确的传参类型,期望传值正确
     */
    public function testSetTypeCorrectType()
    {
        $this->stub->setType(1);
        $this->assertEquals(1, $this->stub->getType());
    }

    /**
     * 设置 MemberOrder setType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTypeWrongType()
    {
        $this->stub->setType('type');
    }
    //type 测试 --------------------------------------------------------   end

    //idfPickup 测试 -------------------------------------------------------- start
    /**
     * 设置 MemberOrder setIdfPickup() 正确的传参类型,期望传值正确
     */
    public function testSetIdfPickupCorrectType()
    {
        $this->stub->setIdfPickup(0);
        $this->assertEquals(0, $this->stub->getIdfPickup());
    }

    /**
     * 设置 MemberOrder setIdfPickup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdfPickupWrongType()
    {
        $this->stub->setIdfPickup('idfPickup');
    }
    //idfPickup 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 MemberOrder setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 MemberOrder setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end
}
