<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class OrderPriceReportTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderPriceReport();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    /**
     * OrderPriceReport 领域对象,测试构造函数
     */
    public function testOrderPriceReportConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getPrice());
        $this->assertEmpty($this->stub->getPriceRemark());
        $this->assertEmpty($this->stub->getRecordId());
        $this->assertEmpty($this->stub->getApiIndex());
        $this->assertEmpty($this->stub->getIdentify());
        $this->assertEmpty($this->stub->getPickupEndDate());
        $this->assertEmpty($this->stub->getPickupStartDate());
        $this->assertEmpty($this->stub->getDeliveryEndDate());
        $this->assertEmpty($this->stub->getDeliveryStartDate());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 OrderPriceReport setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 OrderPriceReport setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //price 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderPriceReport setPrice() 正确的传参类型,期望传值正确
     */
    public function testSetPriceCorrectType()
    {
        $this->stub->setPrice(1);
        $this->assertEquals(1, $this->stub->getPrice());
    }

    /**
     * 设置 OrderPriceReport setPrice() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceWrongType()
    {
        $this->stub->setPrice('price');
    }
    //price 测试 --------------------------------------------------------   end

    //priceRemark 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderPriceReport setPriceRemark() 正确的传参类型,期望传值正确
     */
    public function testSetPriceRemarkCorrectType()
    {
        $this->stub->setPriceRemark(array('priceRemark'));
        $this->assertEquals(array('priceRemark'), $this->stub->getPriceRemark());
    }

    /**
     * 设置 OrderPriceReport setPriceRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceRemarkWrongType()
    {
        $this->stub->setPriceRemark('priceRemark');
    }
    //priceRemark 测试 --------------------------------------------------------   end

    //recordId 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderPriceReport setRecordId() 正确的传参类型,期望传值正确
     */
    public function testSetRecordIdCorrectType()
    {
        $this->stub->setRecordId(1);
        $this->assertEquals(1, $this->stub->getRecordId());
    }

    /**
     * 设置 OrderRecordIdReport setRecordId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRecordIdWrongType()
    {
        $this->stub->setRecordId('recordId');
    }
    //recordId 测试 --------------------------------------------------------   end

    //apiIndex 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderPriceReport setApiIndex() 正确的传参类型,期望传值正确
     */
    public function testSetApiIndexCorrectType()
    {
        $this->stub->setApiIndex(1);
        $this->assertEquals(1, $this->stub->getApiIndex());
    }

    /**
     * 设置 OrderApiIndexReport setApiIndex() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetApiIndexWrongType()
    {
        $this->stub->setApiIndex('apiIndex');
    }
    //apiIndex 测试 --------------------------------------------------------   end

    //identify 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderPriceReport setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->stub->setIdentify('identify');
        $this->assertEquals('identify', $this->stub->getIdentify());
    }

    /**
     * 设置 OrderIdentifyReport setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->stub->setIdentify(array('identify'));
    }
    //identify 测试 --------------------------------------------------------   end

    //pickupEndDate 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderPriceReport setPickupEndDate() 正确的传参类型,期望传值正确
     */
    public function testSetPickupEndDateCorrectType()
    {
        $this->stub->setPickupEndDate(1);
        $this->assertEquals(1, $this->stub->getPickupEndDate());
    }

    /**
     * 设置 OrderPickupEndDateReport setPickupEndDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupEndDateWrongType()
    {
        $this->stub->setPickupEndDate('pickupEndDate');
    }
    //pickupEndDate 测试 --------------------------------------------------------   end

    //pickupStartDate 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderPickupStartDateReport setPickupStartDate() 正确的传参类型,期望传值正确
     */
    public function testSetPickupStartDateCorrectType()
    {
        $this->stub->setPickupStartDate(1);
        $this->assertEquals(1, $this->stub->getPickupStartDate());
    }

    /**
     * 设置 OrderPickupStartDateReport setPickupStartDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupStartDateWrongType()
    {
        $this->stub->setPickupStartDate('pickupStartDate');
    }
    //pickupStartDate 测试 --------------------------------------------------------   end

    //deliveryEndDate 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderPriceReport setDeliveryEndDate() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryEndDateCorrectType()
    {
        $this->stub->setDeliveryEndDate(1);
        $this->assertEquals(1, $this->stub->getDeliveryEndDate());
    }

    /**
     * 设置 OrderDeliveryEndDateReport setDeliveryEndDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryEndDateWrongType()
    {
        $this->stub->setDeliveryEndDate('deliveryEndDate');
    }
    //deliveryEndDate 测试 --------------------------------------------------------   end

    //deliveryStartDate 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderPriceReport setDeliveryStartDate() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryStartDateCorrectType()
    {
        $this->stub->setDeliveryStartDate(1);
        $this->assertEquals(1, $this->stub->getDeliveryStartDate());
    }

    /**
     * 设置 OrderDeliveryStartDateReport setDeliveryStartDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryStartDateWrongType()
    {
        $this->stub->setDeliveryStartDate('deliveryStartDate');
    }
    //deliveryStartDate 测试 --------------------------------------------------------   end
}
