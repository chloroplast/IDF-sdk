<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullDisAmaFTLOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullDisAmaFTLOrder::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsRole()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\DisAmaFTLOrder',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullDisAmaFTLOrderMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function initOperation($method)
    {
        $stub = $this->getMockBuilder(NullDisAmaFTLOrderMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
        $stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $stub->$method();
 
        $this->assertFalse($result);
    }

    public function testUpdateISA()
    {
        $this->initOperation('updateISA');
    }

    public function testRegisterDispatchDate()
    {
        $this->initOperation('registerDispatchDate');
    }

    public function testDispatch()
    {
        $this->initOperation('dispatch');
    }

    public function testAccept()
    {
        $this->initOperation('accept');
    }
}
