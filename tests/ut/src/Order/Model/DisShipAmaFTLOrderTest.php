<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class DisShipAmaFTLOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisShipAmaFTLOrder();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * DisShipAmaFTLOrder 领域对象,测试构造函数
     */
    public function testDisShipAmaFTLOrderConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\Order\Model\DisAmaFTLOrder', $this->stub->getDisAmaFTLOrder());
        $this->assertInstanceOf('Sdk\Order\Model\AmazonFTL', $this->stub->getAmazonFTL());
        $this->assertEmpty($this->stub->getDispatchItemsAttachments());
        $this->assertEmpty($this->stub->getAcceptItemsAttachments());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 DisShipAmaFTLOrder setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 DisShipAmaFTLOrder setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //disAmaFTLOrder 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipAmaFTLOrder setDisAmaFTLOrder() 正确的传参类型,期望传值正确
     */
    public function testSetDisAmaFTLOrderCorrectType()
    {
        $disAmaFTLOrder = new DisAmaFTLOrder();
        $this->stub->setDisAmaFTLOrder($disAmaFTLOrder);
        $this->assertEquals($disAmaFTLOrder, $this->stub->getDisAmaFTLOrder());
    }

    /**
     * 设置 DisShipAmaFTLOrder setDisAmaFTLOrder() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDisAmaFTLOrderWrongType()
    {
        $this->stub->setDisAmaFTLOrder(array('disAmaFTLOrder'));
    }
    //disAmaFTLOrder 测试 --------------------------------------------------------   end

    //amazonFTL 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipAmaFTLOrder setAmazonFTL() 正确的传参类型,期望传值正确
     */
    public function testSetAmazonFTLCorrectType()
    {
        $amazonFTL = new AmazonFTL();
        $this->stub->setAmazonFTL($amazonFTL);
        $this->assertEquals($amazonFTL, $this->stub->getAmazonFTL());
    }

    /**
     * 设置 DisShipAmaFTLOrder setAmazonFTL() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAmazonFTLWrongType()
    {
        $this->stub->setAmazonFTL(array('amazonFTL'));
    }
    //amazonFTL 测试 --------------------------------------------------------   end

    //dispatchItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipAmaFTLOrder setDispatchItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetDispatchItemsAttachmentsCorrectType()
    {
        $this->stub->setDispatchItemsAttachments(array('dispatchItemsAttachments'));
        $this->assertEquals(array('dispatchItemsAttachments'), $this->stub->getDispatchItemsAttachments());
    }

    /**
     * 设置 DisShipAmaFTLOrder setDispatchItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchItemsAttachmentsWrongType()
    {
        $this->stub->setDispatchItemsAttachments('dispatchItemsAttachments');
    }
    //dispatchItemsAttachments 测试 --------------------------------------------------------   end

    //acceptItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipAmaFTLOrder setAcceptItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptItemsAttachmentsCorrectType()
    {
        $this->stub->setAcceptItemsAttachments(array('acceptItemsAttachments'));
        $this->assertEquals(array('acceptItemsAttachments'), $this->stub->getAcceptItemsAttachments());
    }

    /**
     * 设置 DisShipAmaFTLOrder setAcceptItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptItemsAttachmentsWrongType()
    {
        $this->stub->setAcceptItemsAttachments('acceptItemsAttachments');
    }
    //acceptItemsAttachments 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipAmaFTLOrder setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 DisShipAmaFTLOrder setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end
}
