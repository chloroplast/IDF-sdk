<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Repository\AmazonFTLRepository;

use Sdk\User\Member\Model\Member;
use Sdk\User\Staff\Model\Staff;
use Sdk\Warehouse\Model\Warehouse;
use Sdk\Address\Model\Address;
use Sdk\CarType\Model\CarType;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class AmazonFTLTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(AmazonFTLMock::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * AmazonFTL 领域对象,测试构造函数
     */
    public function testAmazonFTLConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\User\Member\Model\Member', $this->stub->getMember());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertInstanceOf('Sdk\Warehouse\Model\Warehouse', $this->stub->getPickupWarehouse());
        $this->assertInstanceOf('Sdk\Address\Model\Address', $this->stub->getAddress());
        $this->assertEmpty($this->stub->getNumber());
        $this->assertEquals(AmazonFTL::IDF_PICKUP['AMAZON'], $this->stub->getIdfPickup());
        $this->assertEmpty($this->stub->getItems());
        $this->assertEmpty($this->stub->getPalletNumber());
        $this->assertEmpty($this->stub->getItemNumber());
        $this->assertEmpty($this->stub->getWeight());
        $this->assertEmpty($this->stub->getWeightUnit());
        $this->assertEmpty($this->stub->getPoNumber());
        $this->assertEmpty($this->stub->getGoodsType());
        $this->assertEmpty($this->stub->getGoodsValue());
        $this->assertEmpty($this->stub->getPackingType());
        $this->assertEmpty($this->stub->getPickupDate());
        $this->assertEmpty($this->stub->getReadyTime());
        $this->assertEmpty($this->stub->getCloseTime());
        $this->assertEmpty($this->stub->getPickupNumber());
        $this->assertEmpty($this->stub->getRemark());
        $this->assertEmpty($this->stub->getIsaNumber());
        $this->assertEmpty($this->stub->getFrozen());
        $this->assertInstanceOf('Sdk\Order\Model\MemberOrder', $this->stub->getMemberOrder());
        $this->assertInstanceOf('Sdk\Warehouse\Model\Warehouse', $this->stub->getTargetWarehouse());
        $this->assertEmpty($this->stub->getPrice());
        $this->assertEmpty($this->stub->getPriceRemark());
        $this->assertEmpty($this->stub->getPickupAddressType());
        $this->assertEmpty($this->stub->getDeliveryAddressType());
        $this->assertEmpty($this->stub->getPriceApiIndex());
        $this->assertEmpty($this->stub->getPriceApiRecordId());
        $this->assertEmpty($this->stub->getPriceApiIdentify());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }


    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 AmazonFTL setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $member = new Member();
        $this->stub->setMember($member);
        $this->assertEquals($member, $this->stub->getMember());
    }

    /**
     * 设置 AmazonFTL setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $this->stub->setMember(array('member'));
    }
    //member 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 AmazonFTL setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //pickupWarehouse 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPickupWarehouse() 正确的传参类型,期望传值正确
     */
    public function testSetPickupWarehouseCorrectType()
    {
        $pickupWarehouse = new Warehouse();
        $this->stub->setPickupWarehouse($pickupWarehouse);
        $this->assertEquals($pickupWarehouse, $this->stub->getPickupWarehouse());
    }

    /**
     * 设置 AmazonFTL setPickupWarehouse() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupWarehouseWrongType()
    {
        $this->stub->setPickupWarehouse(array('pickupWarehouse'));
    }
    //pickupWarehouse 测试 --------------------------------------------------------   end

    //address 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setAddress() 正确的传参类型,期望传值正确
     */
    public function testSetAddressCorrectType()
    {
        $address = new Address();
        $this->stub->setAddress($address);
        $this->assertEquals($address, $this->stub->getAddress());
    }

    /**
     * 设置 AmazonFTL setAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAddressWrongType()
    {
        $this->stub->setAddress(array('address'));
    }
    //address 测试 --------------------------------------------------------   end

    //number 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }

    /**
     * 设置 AmazonFTL setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array('number'));
    }
    //number 测试 --------------------------------------------------------   end

    //idfPickup 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setIdfPickup() 正确的传参类型,期望传值正确
     */
    public function testSetIdfPickupCorrectType()
    {
        $this->stub->setIdfPickup(0);
        $this->assertEquals(0, $this->stub->getIdfPickup());
    }

    /**
     * 设置 AmazonFTL setIdfPickup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdfPickupWrongType()
    {
        $this->stub->setIdfPickup('idfPickup');
    }
    //idfPickup 测试 --------------------------------------------------------   end

    //items 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setItems() 正确的传参类型,期望传值正确
     */
    public function testSetItemsCorrectType()
    {
        $this->stub->setItems(array('items'));
        $this->assertEquals(array('items'), $this->stub->getItems());
    }

    /**
     * 设置 AmazonFTL setItems() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsWrongType()
    {
        $this->stub->setItems('items');
    }
    //items 测试 --------------------------------------------------------   end

    //palletNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPalletNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPalletNumberCorrectType()
    {
        $this->stub->setPalletNumber(0);
        $this->assertEquals(0, $this->stub->getPalletNumber());
    }

    /**
     * 设置 AmazonFTL setPalletNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPalletNumberWrongType()
    {
        $this->stub->setPalletNumber('palletNumber');
    }
    //palletNumber 测试 --------------------------------------------------------   end

    //itemNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setItemNumber() 正确的传参类型,期望传值正确
     */
    public function testSetItemNumberCorrectType()
    {
        $this->stub->setItemNumber(0);
        $this->assertEquals(0, $this->stub->getItemNumber());
    }

    /**
     * 设置 AmazonFTL setItemNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemNumberWrongType()
    {
        $this->stub->setItemNumber('itemNumber');
    }
    //itemNumber 测试 --------------------------------------------------------   end

    //weight 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setWeight() 正确的传参类型,期望传值正确
     */
    public function testSetWeightCorrectType()
    {
        $this->stub->setWeight(0);
        $this->assertEquals(0, $this->stub->getWeight());
    }

    /**
     * 设置 AmazonFTL setWeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightWrongType()
    {
        $this->stub->setWeight('weight');
    }
    //weight 测试 --------------------------------------------------------   end

    //weightUnit 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setWeightUnit() 正确的传参类型,期望传值正确
     */
    public function testSetWeightUnitCorrectType()
    {
        $this->stub->setWeightUnit(0);
        $this->assertEquals(0, $this->stub->getWeightUnit());
    }

    /**
     * 设置 AmazonFTL setWeightUnit() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightUnitWrongType()
    {
        $this->stub->setWeightUnit('weightUnit');
    }
    //weightUnit 测试 --------------------------------------------------------   end

    //poNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPoNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPoNumberCorrectType()
    {
        $this->stub->setPoNumber('poNumber');
        $this->assertEquals('poNumber', $this->stub->getPoNumber());
    }

    /**
     * 设置 AmazonFTL setPoNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPoNumberWrongType()
    {
        $this->stub->setPoNumber(array('poNumber'));
    }
    //poNumber 测试 --------------------------------------------------------   end

    //goodsType 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setGoodsType() 正确的传参类型,期望传值正确
     */
    public function testSetGoodsTypeCorrectType()
    {
        $this->stub->setGoodsType('goodsType');
        $this->assertEquals('goodsType', $this->stub->getGoodsType());
    }

    /**
     * 设置 AmazonFTL setGoodsType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGoodsTypeWrongType()
    {
        $this->stub->setGoodsType(array('goodsType'));
    }
    //goodsType 测试 --------------------------------------------------------   end

    //goodsValue 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setGoodsValue() 正确的传参类型,期望传值正确
     */
    public function testSetGoodsValueCorrectType()
    {
        $this->stub->setGoodsValue(0);
        $this->assertEquals(0, $this->stub->getGoodsValue());
    }

    /**
     * 设置 AmazonFTL setGoodsValue() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGoodsValueWrongType()
    {
        $this->stub->setGoodsValue('goodsValue');
    }
    //goodsValue 测试 --------------------------------------------------------   end

    //packingType 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPackingType() 正确的传参类型,期望传值正确
     */
    public function testSetPackingTypeCorrectType()
    {
        $this->stub->setPackingType('packingType');
        $this->assertEquals('packingType', $this->stub->getPackingType());
    }

    /**
     * 设置 AmazonFTL setPackingType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPackingTypeWrongType()
    {
        $this->stub->setPackingType(array('packingType'));
    }
    //packingType 测试 --------------------------------------------------------   end

    //pickupDate 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPickupDate() 正确的传参类型,期望传值正确
     */
    public function testSetPickupDateCorrectType()
    {
        $this->stub->setPickupDate(0);
        $this->assertEquals(0, $this->stub->getPickupDate());
    }

    /**
     * 设置 AmazonFTL setPickupDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupDateWrongType()
    {
        $this->stub->setPickupDate('pickupDate');
    }
    //pickupDate 测试 --------------------------------------------------------   end

    //readyTime 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setReadyTime() 正确的传参类型,期望传值正确
     */
    public function testSetReadyTimeCorrectType()
    {
        $this->stub->setReadyTime(0);
        $this->assertEquals(0, $this->stub->getReadyTime());
    }

    /**
     * 设置 AmazonFTL setReadyTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReadyTimeWrongType()
    {
        $this->stub->setReadyTime('readyTime');
    }
    //readyTime 测试 --------------------------------------------------------   end

    //closeTime 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setCloseTime() 正确的传参类型,期望传值正确
     */
    public function testSetCloseTimeCorrectType()
    {
        $this->stub->setCloseTime(0);
        $this->assertEquals(0, $this->stub->getCloseTime());
    }

    /**
     * 设置 AmazonFTL setCloseTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCloseTimeWrongType()
    {
        $this->stub->setCloseTime('closeTime');
    }
    //closeTime 测试 --------------------------------------------------------   end

    //pickupNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPickupNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPickupNumberCorrectType()
    {
        $this->stub->setPickupNumber('pickupNumber');
        $this->assertEquals('pickupNumber', $this->stub->getPickupNumber());
    }

    /**
     * 设置 AmazonFTL setPickupNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupNumberWrongType()
    {
        $this->stub->setPickupNumber(array('pickupNumber'));
    }
    //pickupNumber 测试 --------------------------------------------------------   end

    //remark 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setRemark() 正确的传参类型,期望传值正确
     */
    public function testSetRemarkCorrectType()
    {
        $this->stub->setRemark('remark');
        $this->assertEquals('remark', $this->stub->getRemark());
    }

    /**
     * 设置 AmazonFTL setRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRemarkWrongType()
    {
        $this->stub->setRemark(array('remark'));
    }
    //remark 测试 --------------------------------------------------------   end

    //isaNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setIsaNumber() 正确的传参类型,期望传值正确
     */
    public function testSetIsaNumberCorrectType()
    {
        $this->stub->setIsaNumber('isaNumber');
        $this->assertEquals('isaNumber', $this->stub->getIsaNumber());
    }

    /**
     * 设置 AmazonFTL setIsaNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIsaNumberWrongType()
    {
        $this->stub->setIsaNumber(array('isaNumber'));
    }
    //isaNumber 测试 --------------------------------------------------------   end

    //frozen 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setFrozen() 正确的传参类型,期望传值正确
     */
    public function testSetFrozenCorrectType()
    {
        $this->stub->setFrozen(0);
        $this->assertEquals(0, $this->stub->getFrozen());
    }

    /**
     * 设置 AmazonFTL setFrozen() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFrozenWrongType()
    {
        $this->stub->setFrozen('frozen');
    }
    //frozen 测试 --------------------------------------------------------   end

    //memberOrder 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setMemberOrder() 正确的传参类型,期望传值正确
     */
    public function testSetMemberOrderCorrectType()
    {
        $memberOrder = new MemberOrder();
        $this->stub->setMemberOrder($memberOrder);
        $this->assertEquals($memberOrder, $this->stub->getMemberOrder());
    }

    /**
     * 设置 AmazonFTL setMemberOrder() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberOrderWrongType()
    {
        $this->stub->setMemberOrder(array('memberOrder'));
    }
    //memberOrder 测试 --------------------------------------------------------   end

    //targetWarehouse 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setTargetWarehouse() 正确的传参类型,期望传值正确
     */
    public function testSetTargetWarehouseCorrectType()
    {
        $targetWarehouse = new Warehouse();
        $this->stub->setTargetWarehouse($targetWarehouse);
        $this->assertEquals($targetWarehouse, $this->stub->getTargetWarehouse());
    }

    /**
     * 设置 AmazonFTL setTargetWarehouse() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetWarehouseWrongType()
    {
        $this->stub->setTargetWarehouse(array('targetWarehouse'));
    }
    //targetWarehouse 测试 --------------------------------------------------------   end

    //price 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPrice() 正确的传参类型,期望传值正确
     */
    public function testSetPriceCorrectType()
    {
        $this->stub->setPrice(0);
        $this->assertEquals(0, $this->stub->getPrice());
    }

    /**
     * 设置 AmazonFTL setPrice() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceWrongType()
    {
        $this->stub->setPrice('price');
    }
    //price 测试 --------------------------------------------------------   end

    //priceRemark 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPriceRemark() 正确的传参类型,期望传值正确
     */
    public function testSetPriceRemarkCorrectType()
    {
        $this->stub->setPriceRemark(array('priceRemark'));
        $this->assertEquals(array('priceRemark'), $this->stub->getPriceRemark());
    }

    /**
     * 设置 AmazonFTL setPriceRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceRemarkWrongType()
    {
        $this->stub->setPriceRemark('priceRemark');
    }
    //priceRemark 测试 --------------------------------------------------------   end
    
    //pickupAddressType 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPickupAddressType() 正确的传参类型,期望传值正确
     */
    public function testSetPickupAddressTypeCorrectType()
    {
        $this->stub->setPickupAddressType('pickupAddressType');
        $this->assertEquals('pickupAddressType', $this->stub->getPickupAddressType());
    }

    /**
     * 设置 AmazonFTL setPickupAddressType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupAddressTypeWrongType()
    {
        $this->stub->setPickupAddressType(array('pickupAddressType'));
    }
    //pickupAddressType 测试 --------------------------------------------------------   end

    //deliveryAddressType 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setDeliveryAddressType() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryAddressTypeCorrectType()
    {
        $this->stub->setDeliveryAddressType('deliveryAddressType');
        $this->assertEquals('deliveryAddressType', $this->stub->getDeliveryAddressType());
    }

    /**
     * 设置 AmazonFTL setDeliveryAddressType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryAddressTypeWrongType()
    {
        $this->stub->setDeliveryAddressType(array('deliveryAddressType'));
    }
    //deliveryAddressType 测试 --------------------------------------------------------   end

    //priceApiIndex 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPriceApiIndex() 正确的传参类型,期望传值正确
     */
    public function testSetPriceApiIndexCorrectType()
    {
        $this->stub->setPriceApiIndex(0);
        $this->assertEquals(0, $this->stub->getPriceApiIndex());
    }

    /**
     * 设置 AmazonFTL setPriceApiIndex() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceApiIndexWrongType()
    {
        $this->stub->setPriceApiIndex('priceApiIndex');
    }
    //priceApiIndex 测试 --------------------------------------------------------   end

    //priceApiRecordId 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPriceApiRecordId() 正确的传参类型,期望传值正确
     */
    public function testSetPriceApiRecordIdCorrectType()
    {
        $this->stub->setPriceApiRecordId(0);
        $this->assertEquals(0, $this->stub->getPriceApiRecordId());
    }

    /**
     * 设置 AmazonFTL setPriceApiRecordId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceApiRecordIdWrongType()
    {
        $this->stub->setPriceApiRecordId('priceApiRecordId');
    }
    //priceApiRecordId 测试 --------------------------------------------------------   end

    //priceApiIdentify 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setPriceApiIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetPriceApiIdentifyCorrectType()
    {
        $this->stub->setPriceApiIdentify('priceApiIdentify');
        $this->assertEquals('priceApiIdentify', $this->stub->getPriceApiIdentify());
    }

    /**
     * 设置 AmazonFTL setPriceApiIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceApiIdentifyWrongType()
    {
        $this->stub->setPriceApiIdentify(array('priceApiIdentify'));
    }
    //priceApiIdentify 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 AmazonFTL setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new AmazonFTLMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\AmazonFTLRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testMemberCancel()
    {
        // 为 AmazonFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->memberCancel($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->memberCancel();

        $this->assertTrue($result);
    }

    public function testStaffCancel()
    {
        // 为 AmazonFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->staffCancel($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->staffCancel();

        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        // 为 AmazonFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->confirm($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->confirm();

        $this->assertTrue($result);
    }

    public function testBatchConfirm()
    {
        $objectList = array();
        // 为 AmazonFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->batchConfirm($objectList)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->batchConfirm($objectList);

        $this->assertTrue($result);
    }

    public function testFreeze()
    {
        // 为 AmazonFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->freeze($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->freeze();

        $this->assertTrue($result);
    }

    public function testUnFreeze()
    {
        // 为 AmazonFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->unFreeze($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->unFreeze();

        $this->assertTrue($result);
    }

    public function testCalculate()
    {
        $list = array(new AmazonFTL(1));
        // 为 AmazonFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->calculate($this->stub)->shouldBeCalled(1)->willReturn($list);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->calculate();

        $this->assertEquals($result, $list);
    }
}
