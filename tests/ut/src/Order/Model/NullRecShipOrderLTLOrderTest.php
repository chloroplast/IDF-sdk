<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullRecShipOrderLTLOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullRecShipOrderLTLOrder::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsRole()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Model\RecShipOrderLTLOrder',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullRecShipOrderLTLOrderMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
