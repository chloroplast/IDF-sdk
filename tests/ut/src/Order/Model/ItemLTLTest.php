<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Repository\OrderLTLRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ItemLTLTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(ItemLTLMock::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    /**
     * ItemLTL 领域对象,测试构造函数
     */
    public function testItemLTLConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getLength());
        $this->assertEmpty($this->stub->getWidth());
        $this->assertEmpty($this->stub->getHeight());
        $this->assertEmpty($this->stub->getWeight());
        $this->assertEmpty($this->stub->getWeightLB());
        $this->assertEmpty($this->stub->getWeightUnit());
        $this->assertEmpty($this->stub->getGoodsLevel());
        $this->assertEmpty($this->stub->getVolumeInch());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 ItemLTL setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 ItemLTL setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //length 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemLTL setLength() 正确的传参类型,期望传值正确
     */
    public function testSetLengthCorrectType()
    {
        $this->stub->setLength(1);
        $this->assertEquals(1, $this->stub->getLength());
    }

    /**
     * 设置 ItemLTL setLength() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLengthWrongType()
    {
        $this->stub->setLength('length');
    }
    //length 测试 --------------------------------------------------------   end

    //width 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemLTL setWidth() 正确的传参类型,期望传值正确
     */
    public function testSetWidthCorrectType()
    {
        $this->stub->setWidth(1);
        $this->assertEquals(1, $this->stub->getWidth());
    }

    /**
     * 设置 ItemLTL setWidth() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWidthWrongType()
    {
        $this->stub->setWidth('width');
    }
    //width 测试 --------------------------------------------------------   end

    //height 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemLTL setHeight() 正确的传参类型,期望传值正确
     */
    public function testSetHeightCorrectType()
    {
        $this->stub->setHeight(1);
        $this->assertEquals(1, $this->stub->getHeight());
    }

    /**
     * 设置 ItemLTL setHeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHeightWrongType()
    {
        $this->stub->setHeight('height');
    }
    //height 测试 --------------------------------------------------------   end

    //weight 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemLTL setWeight() 正确的传参类型,期望传值正确
     */
    public function testSetWeightCorrectType()
    {
        $this->stub->setWeight(1);
        $this->assertEquals(1, $this->stub->getWeight());
    }

    /**
     * 设置 ItemLTL setWeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightWrongType()
    {
        $this->stub->setWeight('weight');
    }
    //weight 测试 --------------------------------------------------------   end

    //weightLB 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemLTL setWeightLB() 正确的传参类型,期望传值正确
     */
    public function testSetWeightLBCorrectType()
    {
        $this->stub->setWeightLB(1);
        $this->assertEquals(1, $this->stub->getWeightLB());
    }

    /**
     * 设置 ItemLTL setWeightLB() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightLBWrongType()
    {
        $this->stub->setWeightLB('weightLB');
    }
    //weightLB 测试 --------------------------------------------------------   end

    //weightUnit 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemLTL setWeightUnit() 正确的传参类型,期望传值正确
     */
    public function testSetWeightUnitCorrectType()
    {
        $this->stub->setWeightUnit(1);
        $this->assertEquals(1, $this->stub->getWeightUnit());
    }

    /**
     * 设置 ItemLTL setWeightUnit() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightUnitWrongType()
    {
        $this->stub->setWeightUnit('weightUnit');
    }
    //weightUnit 测试 --------------------------------------------------------   end

    //goodsLevel 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemLTL setGoodsLevel() 正确的传参类型,期望传值正确
     */
    public function testSetGoodsLevelCorrectType()
    {
        $this->stub->setGoodsLevel('goodsLevel');
        $this->assertEquals('goodsLevel', $this->stub->getGoodsLevel());
    }

    /**
     * 设置 ItemLTL setGoodsLevel() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGoodsLevelWrongType()
    {
        $this->stub->setGoodsLevel(array('goodsLevel'));
    }
    //goodsLevel 测试 --------------------------------------------------------   end

    //volumeInch 测试 -------------------------------------------------------- start
    /**
     * 设置 ItemLTL setVolumeInch() 正确的传参类型,期望传值正确
     */
    public function testSetVolumeInchCorrectType()
    {
        $this->stub->setVolumeInch(1);
        $this->assertEquals(1, $this->stub->getVolumeInch());
    }

    /**
     * 设置 ItemLTL setVolumeInch() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetVolumeInchWrongType()
    {
        $this->stub->setVolumeInch('volumeInch');
    }
    //volumeInch 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new ItemLTLMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\OrderLTLRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testCalculateGoodsLevel()
    {
        // 为 OrderLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->calculateGoodsLevel($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->calculateGoodsLevel();

        $this->assertTrue($result);
    }
}
