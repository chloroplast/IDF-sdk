<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class DisShipOrderLTLOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisShipOrderLTLOrder();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * DisShipOrderLTLOrder 领域对象,测试构造函数
     */
    public function testDisShipOrderLTLOrderConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\Order\Model\DisOrderLTLOrder', $this->stub->getDisOrderLTLOrder());
        $this->assertInstanceOf('Sdk\Order\Model\OrderLTL', $this->stub->getOrderLTL());
        $this->assertEmpty($this->stub->getDispatchItemsAttachments());
        $this->assertEmpty($this->stub->getAcceptItemsAttachments());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 DisShipOrderLTLOrder setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 DisShipOrderLTLOrder setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //disOrderLTLOrder 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderLTLOrder setDisOrderLTLOrder() 正确的传参类型,期望传值正确
     */
    public function testSetDisOrderLTLOrderCorrectType()
    {
        $disOrderLTLOrder = new DisOrderLTLOrder();
        $this->stub->setDisOrderLTLOrder($disOrderLTLOrder);
        $this->assertEquals($disOrderLTLOrder, $this->stub->getDisOrderLTLOrder());
    }

    /**
     * 设置 DisShipOrderLTLOrder setDisOrderLTLOrder() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDisOrderLTLOrderWrongType()
    {
        $this->stub->setDisOrderLTLOrder(array('disOrderLTLOrder'));
    }
    //disOrderLTLOrder 测试 --------------------------------------------------------   end

    //orderLTL 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderLTLOrder setOrderLTL() 正确的传参类型,期望传值正确
     */
    public function testSetOrderLTLCorrectType()
    {
        $orderLTL = new OrderLTL();
        $this->stub->setOrderLTL($orderLTL);
        $this->assertEquals($orderLTL, $this->stub->getOrderLTL());
    }

    /**
     * 设置 DisShipOrderLTLOrder setOrderLTL() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOrderLTLWrongType()
    {
        $this->stub->setOrderLTL(array('orderLTL'));
    }
    //orderLTL 测试 --------------------------------------------------------   end

    //dispatchItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderLTLOrder setDispatchItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetDispatchItemsAttachmentsCorrectType()
    {
        $this->stub->setDispatchItemsAttachments(array('dispatchItemsAttachments'));
        $this->assertEquals(array('dispatchItemsAttachments'), $this->stub->getDispatchItemsAttachments());
    }

    /**
     * 设置 DisShipOrderLTLOrder setDispatchItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDispatchItemsAttachmentsWrongType()
    {
        $this->stub->setDispatchItemsAttachments('dispatchItemsAttachments');
    }
    //dispatchItemsAttachments 测试 --------------------------------------------------------   end

    //acceptItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderLTLOrder setAcceptItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetAcceptItemsAttachmentsCorrectType()
    {
        $this->stub->setAcceptItemsAttachments(array('acceptItemsAttachments'));
        $this->assertEquals(array('acceptItemsAttachments'), $this->stub->getAcceptItemsAttachments());
    }

    /**
     * 设置 DisShipOrderLTLOrder setAcceptItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAcceptItemsAttachmentsWrongType()
    {
        $this->stub->setAcceptItemsAttachments('acceptItemsAttachments');
    }
    //acceptItemsAttachments 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 DisShipOrderLTLOrder setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 DisShipOrderLTLOrder setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end
}
