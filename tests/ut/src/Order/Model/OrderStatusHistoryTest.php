<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\User\Staff\Model\Staff;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class OrderStatusHistoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderStatusHistory();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * OrderStatusHistory 领域对象,测试构造函数
     */
    public function testOrderStatusHistoryConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\Order\Model\AmazonLTL', $this->stub->getSubOrder());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertEmpty($this->stub->getOrderAttribute());
        $this->assertEmpty($this->stub->getOrderType());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 OrderStatusHistory setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 OrderStatusHistory setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //subOrder 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderStatusHistory setSubOrder() 正确的传参类型,期望传值正确
     */
    public function testSetSubOrderCorrectType()
    {
        $subOrder = new AmazonLTL();
        $this->stub->setSubOrder($subOrder);
        $this->assertEquals($subOrder, $this->stub->getSubOrder());
    }

    /**
     * 设置 OrderStatusHistory setSubOrder() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSubOrderWrongType()
    {
        $this->stub->setSubOrder(array('subOrder'));
    }
    //subOrder 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderStatusHistory setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 OrderStatusHistory setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //orderAttribute 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderStatusHistory setOrderAttribute() 正确的传参类型,期望传值正确
     */
    public function testSetOrderAttributeCorrectType()
    {
        $this->stub->setOrderAttribute(1);
        $this->assertEquals(1, $this->stub->getOrderAttribute());
    }

    /**
     * 设置 OrderStatusHistory setOrderAttribute() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOrderAttributeWrongType()
    {
        $this->stub->setOrderAttribute('orderAttribute');
    }
    //orderAttribute 测试 --------------------------------------------------------   end

    //orderType 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderStatusHistory setOrderType() 正确的传参类型,期望传值正确
     */
    public function testSetOrderTypeCorrectType()
    {
        $this->stub->setOrderType(1);
        $this->assertEquals(1, $this->stub->getOrderType());
    }

    /**
     * 设置 OrderStatusHistory setOrderType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOrderTypeWrongType()
    {
        $this->stub->setOrderType('orderType');
    }
    //orderType 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderStatusHistory setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 OrderStatusHistory setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end
}
