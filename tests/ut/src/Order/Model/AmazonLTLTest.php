<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Repository\AmazonLTLRepository;

use Sdk\User\Member\Model\Member;
use Sdk\User\Staff\Model\Staff;
use Sdk\Warehouse\Model\Warehouse;
use Sdk\Address\Model\Address;
use Sdk\CarType\Model\CarType;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class AmazonLTLTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(AmazonLTLMock::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * AmazonLTL 领域对象,测试构造函数
     */
    public function testAmazonLTLConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\User\Member\Model\Member', $this->stub->getMember());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertInstanceOf('Sdk\Warehouse\Model\Warehouse', $this->stub->getPickupWarehouse());
        $this->assertInstanceOf('Sdk\Address\Model\Address', $this->stub->getAddress());
        $this->assertEmpty($this->stub->getNumber());
        $this->assertEquals(AmazonLTL::IDF_PICKUP['AMAZON'], $this->stub->getIdfPickup());
        $this->assertEmpty($this->stub->getItems());
        $this->assertEmpty($this->stub->getPalletNumber());
        $this->assertEmpty($this->stub->getItemNumber());
        $this->assertEmpty($this->stub->getWeight());
        $this->assertEmpty($this->stub->getWeightUnit());
        $this->assertEmpty($this->stub->getFbaNumber());
        $this->assertEmpty($this->stub->getPoNumber());
        $this->assertEmpty($this->stub->getSoNumber());
        $this->assertEmpty($this->stub->getCoNumber());
        $this->assertEmpty($this->stub->getLength());
        $this->assertEmpty($this->stub->getWidth());
        $this->assertEmpty($this->stub->getHeight());
        $this->assertEmpty($this->stub->getPickupDate());
        $this->assertEmpty($this->stub->getReadyTime());
        $this->assertEmpty($this->stub->getCloseTime());
        $this->assertEmpty($this->stub->getPickupNumber());
        $this->assertEmpty($this->stub->getRemark());
        $this->assertEmpty($this->stub->getIsaNumber());
        $this->assertEmpty($this->stub->getPosition());
        $this->assertEmpty($this->stub->getStockInNumber());
        $this->assertEmpty($this->stub->getStockInTime());
        $this->assertEmpty($this->stub->getFrozen());
        $this->assertInstanceOf('Sdk\Order\Model\MemberOrder', $this->stub->getMemberOrder());
        $this->assertInstanceOf('Sdk\Warehouse\Model\Warehouse', $this->stub->getTargetWarehouse());
        $this->assertEmpty($this->stub->getFilter());
        $this->assertEmpty($this->stub->getSort());
        $this->assertEmpty($this->stub->getPrice());
        $this->assertEmpty($this->stub->getPriceRemark());
        $this->assertInstanceOf('Sdk\CarType\Model\CarType', $this->stub->getCarType());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }


    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 AmazonLTL setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $member = new Member();
        $this->stub->setMember($member);
        $this->assertEquals($member, $this->stub->getMember());
    }

    /**
     * 设置 AmazonLTL setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $this->stub->setMember(array('member'));
    }
    //member 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 AmazonLTL setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //pickupWarehouse 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setPickupWarehouse() 正确的传参类型,期望传值正确
     */
    public function testSetPickupWarehouseCorrectType()
    {
        $pickupWarehouse = new Warehouse();
        $this->stub->setPickupWarehouse($pickupWarehouse);
        $this->assertEquals($pickupWarehouse, $this->stub->getPickupWarehouse());
    }

    /**
     * 设置 AmazonLTL setPickupWarehouse() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupWarehouseWrongType()
    {
        $this->stub->setPickupWarehouse(array('pickupWarehouse'));
    }
    //pickupWarehouse 测试 --------------------------------------------------------   end

    //address 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setAddress() 正确的传参类型,期望传值正确
     */
    public function testSetAddressCorrectType()
    {
        $address = new Address();
        $this->stub->setAddress($address);
        $this->assertEquals($address, $this->stub->getAddress());
    }

    /**
     * 设置 AmazonLTL setAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAddressWrongType()
    {
        $this->stub->setAddress(array('address'));
    }
    //address 测试 --------------------------------------------------------   end

    //number 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }

    /**
     * 设置 AmazonLTL setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array('number'));
    }
    //number 测试 --------------------------------------------------------   end

    //idfPickup 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setIdfPickup() 正确的传参类型,期望传值正确
     */
    public function testSetIdfPickupCorrectType()
    {
        $this->stub->setIdfPickup(0);
        $this->assertEquals(0, $this->stub->getIdfPickup());
    }

    /**
     * 设置 AmazonLTL setIdfPickup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdfPickupWrongType()
    {
        $this->stub->setIdfPickup('idfPickup');
    }
    //idfPickup 测试 --------------------------------------------------------   end

    //items 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setItems() 正确的传参类型,期望传值正确
     */
    public function testSetItemsCorrectType()
    {
        $this->stub->setItems(array('items'));
        $this->assertEquals(array('items'), $this->stub->getItems());
    }

    /**
     * 设置 AmazonLTL setItems() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsWrongType()
    {
        $this->stub->setItems('items');
    }
    //items 测试 --------------------------------------------------------   end

    //palletNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setPalletNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPalletNumberCorrectType()
    {
        $this->stub->setPalletNumber(0);
        $this->assertEquals(0, $this->stub->getPalletNumber());
    }

    /**
     * 设置 AmazonLTL setPalletNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPalletNumberWrongType()
    {
        $this->stub->setPalletNumber('palletNumber');
    }
    //palletNumber 测试 --------------------------------------------------------   end

    //itemNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setItemNumber() 正确的传参类型,期望传值正确
     */
    public function testSetItemNumberCorrectType()
    {
        $this->stub->setItemNumber(0);
        $this->assertEquals(0, $this->stub->getItemNumber());
    }

    /**
     * 设置 AmazonLTL setItemNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemNumberWrongType()
    {
        $this->stub->setItemNumber('itemNumber');
    }
    //itemNumber 测试 --------------------------------------------------------   end

    //weight 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setWeight() 正确的传参类型,期望传值正确
     */
    public function testSetWeightCorrectType()
    {
        $this->stub->setWeight(0);
        $this->assertEquals(0, $this->stub->getWeight());
    }

    /**
     * 设置 AmazonLTL setWeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightWrongType()
    {
        $this->stub->setWeight('weight');
    }
    //weight 测试 --------------------------------------------------------   end

    //weightUnit 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setWeightUnit() 正确的传参类型,期望传值正确
     */
    public function testSetWeightUnitCorrectType()
    {
        $this->stub->setWeightUnit(0);
        $this->assertEquals(0, $this->stub->getWeightUnit());
    }

    /**
     * 设置 AmazonLTL setWeightUnit() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightUnitWrongType()
    {
        $this->stub->setWeightUnit('weightUnit');
    }
    //weightUnit 测试 --------------------------------------------------------   end

    //fbaNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setFbaNumber() 正确的传参类型,期望传值正确
     */
    public function testSetFbaNumberCorrectType()
    {
        $this->stub->setFbaNumber('fbaNumber');
        $this->assertEquals('fbaNumber', $this->stub->getFbaNumber());
    }

    /**
     * 设置 AmazonLTL setFbaNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFbaNumberWrongType()
    {
        $this->stub->setFbaNumber(array('fbaNumber'));
    }
    //fbaNumber 测试 --------------------------------------------------------   end

    //poNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setPoNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPoNumberCorrectType()
    {
        $this->stub->setPoNumber('poNumber');
        $this->assertEquals('poNumber', $this->stub->getPoNumber());
    }

    /**
     * 设置 AmazonLTL setPoNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPoNumberWrongType()
    {
        $this->stub->setPoNumber(array('poNumber'));
    }
    //poNumber 测试 --------------------------------------------------------   end

    //soNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setSoNumber() 正确的传参类型,期望传值正确
     */
    public function testSetSoNumberCorrectType()
    {
        $this->stub->setSoNumber('soNumber');
        $this->assertEquals('soNumber', $this->stub->getSoNumber());
    }

    /**
     * 设置 AmazonLTL setSoNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSoNumberWrongType()
    {
        $this->stub->setSoNumber(array('soNumber'));
    }
    //soNumber 测试 --------------------------------------------------------   end

    //coNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setCoNumber() 正确的传参类型,期望传值正确
     */
    public function testSetCoNumberCorrectType()
    {
        $this->stub->setCoNumber('coNumber');
        $this->assertEquals('coNumber', $this->stub->getCoNumber());
    }

    /**
     * 设置 AmazonLTL setCoNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCoNumberWrongType()
    {
        $this->stub->setCoNumber(array('coNumber'));
    }
    //coNumber 测试 --------------------------------------------------------   end

    //length 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setLength() 正确的传参类型,期望传值正确
     */
    public function testSetLengthCorrectType()
    {
        $this->stub->setLength(0);
        $this->assertEquals(0, $this->stub->getLength());
    }

    /**
     * 设置 AmazonLTL setLength() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLengthWrongType()
    {
        $this->stub->setLength('length');
    }
    //length 测试 --------------------------------------------------------   end

    //width 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setWidth() 正确的传参类型,期望传值正确
     */
    public function testSetWidthCorrectType()
    {
        $this->stub->setWidth(0);
        $this->assertEquals(0, $this->stub->getWidth());
    }

    /**
     * 设置 AmazonLTL setWidth() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWidthWrongType()
    {
        $this->stub->setWidth('width');
    }
    //width 测试 --------------------------------------------------------   end

    //height 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setHeight() 正确的传参类型,期望传值正确
     */
    public function testSetHeightCorrectType()
    {
        $this->stub->setHeight(0);
        $this->assertEquals(0, $this->stub->getHeight());
    }

    /**
     * 设置 AmazonLTL setHeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHeightWrongType()
    {
        $this->stub->setHeight('height');
    }
    //height 测试 --------------------------------------------------------   end

    //pickupDate 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setPickupDate() 正确的传参类型,期望传值正确
     */
    public function testSetPickupDateCorrectType()
    {
        $this->stub->setPickupDate(0);
        $this->assertEquals(0, $this->stub->getPickupDate());
    }

    /**
     * 设置 AmazonLTL setPickupDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupDateWrongType()
    {
        $this->stub->setPickupDate('pickupDate');
    }
    //pickupDate 测试 --------------------------------------------------------   end

    //readyTime 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setReadyTime() 正确的传参类型,期望传值正确
     */
    public function testSetReadyTimeCorrectType()
    {
        $this->stub->setReadyTime(0);
        $this->assertEquals(0, $this->stub->getReadyTime());
    }

    /**
     * 设置 AmazonLTL setReadyTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReadyTimeWrongType()
    {
        $this->stub->setReadyTime('readyTime');
    }
    //readyTime 测试 --------------------------------------------------------   end

    //closeTime 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setCloseTime() 正确的传参类型,期望传值正确
     */
    public function testSetCloseTimeCorrectType()
    {
        $this->stub->setCloseTime(0);
        $this->assertEquals(0, $this->stub->getCloseTime());
    }

    /**
     * 设置 AmazonLTL setCloseTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCloseTimeWrongType()
    {
        $this->stub->setCloseTime('closeTime');
    }
    //closeTime 测试 --------------------------------------------------------   end

    //pickupNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setPickupNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPickupNumberCorrectType()
    {
        $this->stub->setPickupNumber('pickupNumber');
        $this->assertEquals('pickupNumber', $this->stub->getPickupNumber());
    }

    /**
     * 设置 AmazonLTL setPickupNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupNumberWrongType()
    {
        $this->stub->setPickupNumber(array('pickupNumber'));
    }
    //pickupNumber 测试 --------------------------------------------------------   end

    //remark 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setRemark() 正确的传参类型,期望传值正确
     */
    public function testSetRemarkCorrectType()
    {
        $this->stub->setRemark('remark');
        $this->assertEquals('remark', $this->stub->getRemark());
    }

    /**
     * 设置 AmazonLTL setRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRemarkWrongType()
    {
        $this->stub->setRemark(array('remark'));
    }
    //remark 测试 --------------------------------------------------------   end

    //isaNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setIsaNumber() 正确的传参类型,期望传值正确
     */
    public function testSetIsaNumberCorrectType()
    {
        $this->stub->setIsaNumber('isaNumber');
        $this->assertEquals('isaNumber', $this->stub->getIsaNumber());
    }

    /**
     * 设置 AmazonLTL setIsaNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIsaNumberWrongType()
    {
        $this->stub->setIsaNumber(array('isaNumber'));
    }
    //isaNumber 测试 --------------------------------------------------------   end

    //position 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setPosition() 正确的传参类型,期望传值正确
     */
    public function testSetPositionCorrectType()
    {
        $this->stub->setPosition('position');
        $this->assertEquals('position', $this->stub->getPosition());
    }

    /**
     * 设置 AmazonLTL setPosition() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPositionWrongType()
    {
        $this->stub->setPosition(array('position'));
    }
    //position 测试 --------------------------------------------------------   end

    //stockInNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setStockInNumber() 正确的传参类型,期望传值正确
     */
    public function testSetStockInNumberCorrectType()
    {
        $this->stub->setStockInNumber('stockInNumber');
        $this->assertEquals('stockInNumber', $this->stub->getStockInNumber());
    }

    /**
     * 设置 AmazonLTL setStockInNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStockInNumberWrongType()
    {
        $this->stub->setStockInNumber(array('stockInNumber'));
    }
    //stockInNumber 测试 --------------------------------------------------------   end

    //stockInTime 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setStockInTime() 正确的传参类型,期望传值正确
     */
    public function testSetStockInTimeCorrectType()
    {
        $this->stub->setStockInTime(0);
        $this->assertEquals(0, $this->stub->getStockInTime());
    }

    /**
     * 设置 AmazonLTL setStockInTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStockInTimeWrongType()
    {
        $this->stub->setStockInTime('stockInTime');
    }
    //stockInTime 测试 --------------------------------------------------------   end

    //frozen 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setFrozen() 正确的传参类型,期望传值正确
     */
    public function testSetFrozenCorrectType()
    {
        $this->stub->setFrozen(0);
        $this->assertEquals(0, $this->stub->getFrozen());
    }

    /**
     * 设置 AmazonLTL setFrozen() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFrozenWrongType()
    {
        $this->stub->setFrozen('frozen');
    }
    //frozen 测试 --------------------------------------------------------   end

    //memberOrder 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setMemberOrder() 正确的传参类型,期望传值正确
     */
    public function testSetMemberOrderCorrectType()
    {
        $memberOrder = new MemberOrder();
        $this->stub->setMemberOrder($memberOrder);
        $this->assertEquals($memberOrder, $this->stub->getMemberOrder());
    }

    /**
     * 设置 AmazonLTL setMemberOrder() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberOrderWrongType()
    {
        $this->stub->setMemberOrder(array('memberOrder'));
    }
    //memberOrder 测试 --------------------------------------------------------   end

    //targetWarehouse 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setTargetWarehouse() 正确的传参类型,期望传值正确
     */
    public function testSetTargetWarehouseCorrectType()
    {
        $targetWarehouse = new Warehouse();
        $this->stub->setTargetWarehouse($targetWarehouse);
        $this->assertEquals($targetWarehouse, $this->stub->getTargetWarehouse());
    }

    /**
     * 设置 AmazonLTL setTargetWarehouse() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetWarehouseWrongType()
    {
        $this->stub->setTargetWarehouse(array('targetWarehouse'));
    }
    //targetWarehouse 测试 --------------------------------------------------------   end

    //filter 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setFilter() 正确的传参类型,期望传值正确
     */
    public function testSetFilterCorrectType()
    {
        $this->stub->setFilter(array('filter'));
        $this->assertEquals(array('filter'), $this->stub->getFilter());
    }

    /**
     * 设置 AmazonLTL setFilter() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFilterWrongType()
    {
        $this->stub->setFilter('filter');
    }
    //filter 测试 --------------------------------------------------------   end

    //sort 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setSort() 正确的传参类型,期望传值正确
     */
    public function testSetSortCorrectType()
    {
        $this->stub->setSort('sort');
        $this->assertEquals('sort', $this->stub->getSort());
    }

    /**
     * 设置 AmazonLTL setSort() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSortWrongType()
    {
        $this->stub->setSort(array('sort'));
    }
    //sort 测试 --------------------------------------------------------   end

    //price 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setPrice() 正确的传参类型,期望传值正确
     */
    public function testSetPriceCorrectType()
    {
        $this->stub->setPrice(0);
        $this->assertEquals(0, $this->stub->getPrice());
    }

    /**
     * 设置 AmazonLTL setPrice() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceWrongType()
    {
        $this->stub->setPrice('price');
    }
    //price 测试 --------------------------------------------------------   end

    //priceRemark 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setPriceRemark() 正确的传参类型,期望传值正确
     */
    public function testSetPriceRemarkCorrectType()
    {
        $this->stub->setPriceRemark(array('priceRemark'));
        $this->assertEquals(array('priceRemark'), $this->stub->getPriceRemark());
    }

    /**
     * 设置 AmazonLTL setPriceRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceRemarkWrongType()
    {
        $this->stub->setPriceRemark('priceRemark');
    }
    //priceRemark 测试 --------------------------------------------------------   end

    //carType 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setCarType() 正确的传参类型,期望传值正确
     */
    public function testSetCarTypeCorrectType()
    {
        $carType = new CarType();
        $this->stub->setCarType($carType);
        $this->assertEquals($carType, $this->stub->getCarType());
    }

    /**
     * 设置 AmazonLTL setCarType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCarTypeWrongType()
    {
        $this->stub->setCarType(array('carType'));
    }
    //carType 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonLTL setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 AmazonLTL setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new AmazonLTLMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\AmazonLTLRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testMemberCancel()
    {
        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->memberCancel($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->memberCancel();

        $this->assertTrue($result);
    }

    public function testStaffCancel()
    {
        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->staffCancel($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->staffCancel();

        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->confirm($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->confirm();

        $this->assertTrue($result);
    }

    public function testBatchConfirm()
    {
        $objectList = array();
        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->batchConfirm($objectList)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->batchConfirm($objectList);

        $this->assertTrue($result);
    }

    public function testRegisterPosition()
    {
        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->registerPosition($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->registerPosition();

        $this->assertTrue($result);
    }

    public function testFreeze()
    {
        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->freeze($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->freeze();

        $this->assertTrue($result);
    }

    public function testUnFreeze()
    {
        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->unFreeze($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->unFreeze();

        $this->assertTrue($result);
    }

    public function testAutoGroup()
    {
        $list = array(new AmazonLTL(1));

        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->autoGroup($list)->shouldBeCalled(1)->willReturn($list);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->autoGroup($list);

        $this->assertEquals($result, $list);
    }

    public function testCalculate()
    {
        $list = array(new AmazonLTL(1));

        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->calculate($this->stub)->shouldBeCalled(1)->willReturn($list);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->calculate();

        $this->assertEquals($result, $list);
    }
}
