<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Repository\RecOrderLTLOrderRepository;

use Sdk\CarType\Model\CarType;
use Sdk\User\Staff\Model\Staff;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class RecOrderLTLOrderTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(RecOrderLTLOrderMock::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * RecOrderLTLOrder 领域对象,测试构造函数
     */
    public function testRecOrderLTLOrderConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\CarType\Model\CarType', $this->stub->getCarType());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertEmpty($this->stub->getNumber());
        $this->assertEmpty($this->stub->getTotalWeight());
        $this->assertEmpty($this->stub->getTotalPalletNumber());
        $this->assertEmpty($this->stub->getTotalOrderNumber());
        $this->assertEmpty($this->stub->getOrderLTL());
        $this->assertEmpty($this->stub->getReferences());
        $this->assertEmpty($this->stub->getStockInItemsAttachments());
        $this->assertEmpty($this->stub->getPickUpAttachments());
        $this->assertEmpty($this->stub->getStockInAttachments());
        $this->assertEmpty($this->stub->getPickupDate());
        $this->assertEmpty($this->stub->getDeliveryDate());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 RecOrderLTLOrder setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //carType 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setCarType() 正确的传参类型,期望传值正确
     */
    public function testSetCarTypeCorrectType()
    {
        $carType = new CarType();
        $this->stub->setCarType($carType);
        $this->assertEquals($carType, $this->stub->getCarType());
    }

    /**
     * 设置 RecOrderLTLOrder setCarType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCarTypeWrongType()
    {
        $this->stub->setCarType(array('carType'));
    }
    //carType 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 RecOrderLTLOrder setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //number 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }

    /**
     * 设置 RecOrderLTLOrder setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array('number'));
    }
    //number 测试 --------------------------------------------------------   end

    //totalWeight 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setTotalWeight() 正确的传参类型,期望传值正确
     */
    public function testSetTotalWeightCorrectType()
    {
        $this->stub->setTotalWeight(1);
        $this->assertEquals(1, $this->stub->getTotalWeight());
    }

    /**
     * 设置 RecOrderLTLOrder setTotalWeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalWeightWrongType()
    {
        $this->stub->setTotalWeight('totalWeight');
    }
    //totalWeight 测试 --------------------------------------------------------   end

    //totalPalletNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setTotalPalletNumber() 正确的传参类型,期望传值正确
     */
    public function testSetTotalPalletNumberCorrectType()
    {
        $this->stub->setTotalPalletNumber(1);
        $this->assertEquals(1, $this->stub->getTotalPalletNumber());
    }

    /**
     * 设置 RecOrderLTLOrder setTotalPalletNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalPalletNumberWrongType()
    {
        $this->stub->setTotalPalletNumber('totalPalletNumber');
    }
    //totalPalletNumber 测试 --------------------------------------------------------   end

    //totalOrderNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setTotalOrderNumber() 正确的传参类型,期望传值正确
     */
    public function testSetTotalOrderNumberCorrectType()
    {
        $this->stub->setTotalOrderNumber(1);
        $this->assertEquals(1, $this->stub->getTotalOrderNumber());
    }

    /**
     * 设置 RecOrderLTLOrder setTotalOrderNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalOrderNumberWrongType()
    {
        $this->stub->setTotalOrderNumber('totalOrderNumber');
    }
    //totalOrderNumber 测试 --------------------------------------------------------   end

    //orderLTL 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setOrderLTL() 正确的传参类型,期望传值正确
     */
    public function testSetOrderLTLCorrectType()
    {
        $this->stub->setOrderLTL(array('orderLTL'));
        $this->assertEquals(array('orderLTL'), $this->stub->getOrderLTL());
    }

    /**
     * 设置 RecOrderLTLOrder setOrderLTL() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOrderLTLWrongType()
    {
        $this->stub->setOrderLTL('orderLTL');
    }
    //orderLTL 测试 --------------------------------------------------------   end

    //references 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setReferences() 正确的传参类型,期望传值正确
     */
    public function testSetReferencesCorrectType()
    {
        $this->stub->setReferences('references');
        $this->assertEquals('references', $this->stub->getReferences());
    }

    /**
     * 设置 RecOrderLTLOrder setReferences() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReferencesWrongType()
    {
        $this->stub->setReferences(array('references'));
    }
    //references 测试 --------------------------------------------------------   end

    //stockInItemsAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setStockInItemsAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetStockInItemsAttachmentsCorrectType()
    {
        $this->stub->setStockInItemsAttachments(array('stockInItemsAttachments'));
        $this->assertEquals(array('stockInItemsAttachments'), $this->stub->getStockInItemsAttachments());
    }

    /**
     * 设置 RecOrderLTLOrder setStockInItemsAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStockInItemsAttachmentsWrongType()
    {
        $this->stub->setStockInItemsAttachments('stockInItemsAttachments');
    }
    //stockInItemsAttachments 测试 --------------------------------------------------------   end

    //pickUpAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setPickUpAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetPickUpAttachmentsCorrectType()
    {
        $this->stub->setPickUpAttachments(array('pickUpAttachments'));
        $this->assertEquals(array('pickUpAttachments'), $this->stub->getPickUpAttachments());
    }

    /**
     * 设置 RecOrderLTLOrder setPickUpAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickUpAttachmentsWrongType()
    {
        $this->stub->setPickUpAttachments('pickUpAttachments');
    }
    //pickUpAttachments 测试 --------------------------------------------------------   end

    //stockInAttachments 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setStockInAttachments() 正确的传参类型,期望传值正确
     */
    public function testSetStockInAttachmentsCorrectType()
    {
        $this->stub->setStockInAttachments(array('stockInAttachments'));
        $this->assertEquals(array('stockInAttachments'), $this->stub->getStockInAttachments());
    }

    /**
     * 设置 RecOrderLTLOrder setStockInAttachments() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStockInAttachmentsWrongType()
    {
        $this->stub->setStockInAttachments('stockInAttachments');
    }
    //stockInAttachments 测试 --------------------------------------------------------   end

    //pickupDate 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setPickupDate() 正确的传参类型,期望传值正确
     */
    public function testSetPickupDateCorrectType()
    {
        $this->stub->setPickupDate(1);
        $this->assertEquals(1, $this->stub->getPickupDate());
    }

    /**
     * 设置 RecOrderLTLOrder setPickupDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupDateWrongType()
    {
        $this->stub->setPickupDate('pickupDate');
    }
    //pickupDate 测试 --------------------------------------------------------   end

    //deliveryDate 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setDeliveryDate() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryDateCorrectType()
    {
        $this->stub->setDeliveryDate(1);
        $this->assertEquals(1, $this->stub->getDeliveryDate());
    }

    /**
     * 设置 RecOrderLTLOrder setDeliveryDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryDateWrongType()
    {
        $this->stub->setDeliveryDate('deliveryDate');
    }
    //deliveryDate 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 RecOrderLTLOrder setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 RecOrderLTLOrder setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new RecOrderLTLOrderMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\RecOrderLTLOrderRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testRegisterPickupDate()
    {
        // 为 RecOrderLTLOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(RecOrderLTLOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->registerPickupDate($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->registerPickupDate();

        $this->assertTrue($result);
    }

    public function testPickup()
    {
        // 为 RecOrderLTLOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(RecOrderLTLOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->pickup($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->pickup();

        $this->assertTrue($result);
    }

    public function testStockIn()
    {
        // 为 RecOrderLTLOrderRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(RecOrderLTLOrderRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->stockIn($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->stockIn();

        $this->assertTrue($result);
    }
}
