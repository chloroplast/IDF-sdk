<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Repository\OrderFTLRepository;

use Sdk\User\Member\Model\Member;
use Sdk\User\Staff\Model\Staff;
use Sdk\Warehouse\Model\Warehouse;
use Sdk\Address\Model\Address;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class OrderFTLTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(OrderFTLMock::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * OrderFTL 领域对象,测试构造函数
     */
    public function testOrderFTLConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\User\Member\Model\Member', $this->stub->getMember());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertInstanceOf('Sdk\Warehouse\Model\Warehouse', $this->stub->getPickupWarehouse());
        $this->assertInstanceOf('Sdk\Address\Model\Address', $this->stub->getAddress());
        $this->assertInstanceOf('Sdk\Address\Model\Address', $this->stub->getDeliveryAddress());
        $this->assertEmpty($this->stub->getDeliveryName());
        $this->assertEmpty($this->stub->getNumber());
        $this->assertEquals(OrderFTL::WEEKEND_DELIVERY['NO'], $this->stub->getWeekendDelivery());
        $this->assertEmpty($this->stub->getItems());
        $this->assertEmpty($this->stub->getPalletNumber());
        $this->assertEmpty($this->stub->getItemNumber());
        $this->assertEmpty($this->stub->getWeight());
        $this->assertEmpty($this->stub->getWeightUnit());
        $this->assertEmpty($this->stub->getPoNumber());
        $this->assertEmpty($this->stub->getGoodsType());
        $this->assertEmpty($this->stub->getGoodsValue());
        $this->assertEmpty($this->stub->getPackingType());
        $this->assertEmpty($this->stub->getPickupDate());
        $this->assertEmpty($this->stub->getReadyTime());
        $this->assertEmpty($this->stub->getCloseTime());
        $this->assertEmpty($this->stub->getDeliveryDate());
        $this->assertEmpty($this->stub->getDeliveryReadyTime());
        $this->assertEmpty($this->stub->getDeliveryCloseTime());
        $this->assertEmpty($this->stub->getPickupNumber());
        $this->assertEmpty($this->stub->getRemark());
        $this->assertEmpty($this->stub->getFrozen());
        $this->assertInstanceOf('Sdk\Order\Model\MemberOrder', $this->stub->getMemberOrder());
        $this->assertEmpty($this->stub->getPrice());
        $this->assertEmpty($this->stub->getPriceRemark());
        $this->assertEmpty($this->stub->getPickupAddressType());
        $this->assertEmpty($this->stub->getDeliveryAddressType());
        $this->assertEmpty($this->stub->getPriceApiIndex());
        $this->assertEmpty($this->stub->getPriceApiRecordId());
        $this->assertEmpty($this->stub->getPriceApiIdentify());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 OrderFTL setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 OrderFTL setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //deliveryName 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setDeliveryName() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryNameCorrectType()
    {
        $this->stub->setDeliveryName('deliveryName');
        $this->assertEquals('deliveryName', $this->stub->getDeliveryName());
    }

    /**
     * 设置 OrderFTL setDeliveryName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryNameWrongType()
    {
        $this->stub->setDeliveryName(array('deliveryName'));
    }
    //deliveryName 测试 --------------------------------------------------------   end

    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $member = new Member();
        $this->stub->setMember($member);
        $this->assertEquals($member, $this->stub->getMember());
    }

    /**
     * 设置 OrderFTL setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $this->stub->setMember(array('member'));
    }
    //member 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 OrderFTL setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //pickupWarehouse 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPickupWarehouse() 正确的传参类型,期望传值正确
     */
    public function testSetPickupWarehouseCorrectType()
    {
        $pickupWarehouse = new Warehouse();
        $this->stub->setPickupWarehouse($pickupWarehouse);
        $this->assertEquals($pickupWarehouse, $this->stub->getPickupWarehouse());
    }

    /**
     * 设置 OrderFTL setPickupWarehouse() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupWarehouseWrongType()
    {
        $this->stub->setPickupWarehouse(array('pickupWarehouse'));
    }
    //pickupWarehouse 测试 --------------------------------------------------------   end

    //address 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setAddress() 正确的传参类型,期望传值正确
     */
    public function testSetAddressCorrectType()
    {
        $address = new Address();
        $this->stub->setAddress($address);
        $this->assertEquals($address, $this->stub->getAddress());
    }

    /**
     * 设置 OrderFTL setAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAddressWrongType()
    {
        $this->stub->setAddress(array('address'));
    }
    //address 测试 --------------------------------------------------------   end

    //number 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setNumber() 正确的传参类型,期望传值正确
     */
    public function testSetNumberCorrectType()
    {
        $this->stub->setNumber('number');
        $this->assertEquals('number', $this->stub->getNumber());
    }

    /**
     * 设置 OrderFTL setNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNumberWrongType()
    {
        $this->stub->setNumber(array('number'));
    }
    //number 测试 --------------------------------------------------------   end

    //weekendDelivery 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setWeekendDelivery() 正确的传参类型,期望传值正确
     */
    public function testSetWeekendDeliveryCorrectType()
    {
        $this->stub->setWeekendDelivery(0);
        $this->assertEquals(0, $this->stub->getWeekendDelivery());
    }

    /**
     * 设置 OrderFTL setWeekendDelivery() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeekendDeliveryWrongType()
    {
        $this->stub->setWeekendDelivery('weekendDelivery');
    }
    //weekendDelivery 测试 --------------------------------------------------------   end

    //items 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setItems() 正确的传参类型,期望传值正确
     */
    public function testSetItemsCorrectType()
    {
        $this->stub->setItems(array('items'));
        $this->assertEquals(array('items'), $this->stub->getItems());
    }

    /**
     * 设置 OrderFTL setItems() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsWrongType()
    {
        $this->stub->setItems('items');
    }
    //items 测试 --------------------------------------------------------   end

    //palletNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPalletNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPalletNumberCorrectType()
    {
        $this->stub->setPalletNumber(0);
        $this->assertEquals(0, $this->stub->getPalletNumber());
    }

    /**
     * 设置 OrderFTL setPalletNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPalletNumberWrongType()
    {
        $this->stub->setPalletNumber('palletNumber');
    }
    //palletNumber 测试 --------------------------------------------------------   end

    //itemNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setItemNumber() 正确的传参类型,期望传值正确
     */
    public function testSetItemNumberCorrectType()
    {
        $this->stub->setItemNumber(0);
        $this->assertEquals(0, $this->stub->getItemNumber());
    }

    /**
     * 设置 OrderFTL setItemNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemNumberWrongType()
    {
        $this->stub->setItemNumber('itemNumber');
    }
    //itemNumber 测试 --------------------------------------------------------   end

    //weight 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setWeight() 正确的传参类型,期望传值正确
     */
    public function testSetWeightCorrectType()
    {
        $this->stub->setWeight(0);
        $this->assertEquals(0, $this->stub->getWeight());
    }

    /**
     * 设置 OrderFTL setWeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightWrongType()
    {
        $this->stub->setWeight('weight');
    }
    //weight 测试 --------------------------------------------------------   end

    //weightUnit 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setWeightUnit() 正确的传参类型,期望传值正确
     */
    public function testSetWeightUnitCorrectType()
    {
        $this->stub->setWeightUnit(0);
        $this->assertEquals(0, $this->stub->getWeightUnit());
    }

    /**
     * 设置 OrderFTL setWeightUnit() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightUnitWrongType()
    {
        $this->stub->setWeightUnit('weightUnit');
    }
    //weightUnit 测试 --------------------------------------------------------   end

    //poNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPoNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPoNumberCorrectType()
    {
        $this->stub->setPoNumber('poNumber');
        $this->assertEquals('poNumber', $this->stub->getPoNumber());
    }

    /**
     * 设置 OrderFTL setPoNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPoNumberWrongType()
    {
        $this->stub->setPoNumber(array('poNumber'));
    }
    //poNumber 测试 --------------------------------------------------------   end

    //goodsType 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setGoodsType() 正确的传参类型,期望传值正确
     */
    public function testSetGoodsTypeCorrectType()
    {
        $this->stub->setGoodsType('goodsType');
        $this->assertEquals('goodsType', $this->stub->getGoodsType());
    }

    /**
     * 设置 OrderFTL setGoodsType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGoodsTypeWrongType()
    {
        $this->stub->setGoodsType(array('goodsType'));
    }
    //goodsType 测试 --------------------------------------------------------   end

    //goodsValue 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setGoodsValue() 正确的传参类型,期望传值正确
     */
    public function testSetGoodsValueCorrectType()
    {
        $this->stub->setGoodsValue(0);
        $this->assertEquals(0, $this->stub->getGoodsValue());
    }

    /**
     * 设置 OrderFTL setGoodsValue() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGoodsValueWrongType()
    {
        $this->stub->setGoodsValue('goodsValue');
    }
    //goodsValue 测试 --------------------------------------------------------   end

    //packingType 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPackingType() 正确的传参类型,期望传值正确
     */
    public function testSetPackingTypeCorrectType()
    {
        $this->stub->setPackingType('packingType');
        $this->assertEquals('packingType', $this->stub->getPackingType());
    }

    /**
     * 设置 OrderFTL setPackingType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPackingTypeWrongType()
    {
        $this->stub->setPackingType(array('packingType'));
    }
    //packingType 测试 --------------------------------------------------------   end

    //pickupDate 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPickupDate() 正确的传参类型,期望传值正确
     */
    public function testSetPickupDateCorrectType()
    {
        $this->stub->setPickupDate(0);
        $this->assertEquals(0, $this->stub->getPickupDate());
    }

    /**
     * 设置 OrderFTL setPickupDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupDateWrongType()
    {
        $this->stub->setPickupDate('pickupDate');
    }
    //pickupDate 测试 --------------------------------------------------------   end

    //readyTime 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setReadyTime() 正确的传参类型,期望传值正确
     */
    public function testSetReadyTimeCorrectType()
    {
        $this->stub->setReadyTime(0);
        $this->assertEquals(0, $this->stub->getReadyTime());
    }

    /**
     * 设置 OrderFTL setReadyTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReadyTimeWrongType()
    {
        $this->stub->setReadyTime('readyTime');
    }
    //readyTime 测试 --------------------------------------------------------   end

    //closeTime 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setCloseTime() 正确的传参类型,期望传值正确
     */
    public function testSetCloseTimeCorrectType()
    {
        $this->stub->setCloseTime(0);
        $this->assertEquals(0, $this->stub->getCloseTime());
    }

    /**
     * 设置 OrderFTL setCloseTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCloseTimeWrongType()
    {
        $this->stub->setCloseTime('closeTime');
    }
    //closeTime 测试 --------------------------------------------------------   end

    //deliveryDate 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setDeliveryDate() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryDateCorrectType()
    {
        $this->stub->setDeliveryDate(0);
        $this->assertEquals(0, $this->stub->getDeliveryDate());
    }

    /**
     * 设置 OrderFTL setDeliveryDate() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryDateWrongType()
    {
        $this->stub->setDeliveryDate('deliveryDate');
    }
    //deliveryDate 测试 --------------------------------------------------------   end

    //deliveryReadyTime 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setDeliveryReadyTime() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryReadyTimeCorrectType()
    {
        $this->stub->setDeliveryReadyTime(0);
        $this->assertEquals(0, $this->stub->getDeliveryReadyTime());
    }

    /**
     * 设置 OrderFTL setDeliveryReadyTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryReadyTimeWrongType()
    {
        $this->stub->setDeliveryReadyTime('deliveryReadyTime');
    }
    //deliveryReadyTime 测试 --------------------------------------------------------   end

    //deliveryCloseTime 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setDeliveryCloseTime() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryCloseTimeCorrectType()
    {
        $this->stub->setDeliveryCloseTime(0);
        $this->assertEquals(0, $this->stub->getDeliveryCloseTime());
    }

    /**
     * 设置 OrderFTL setDeliveryCloseTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryCloseTimeWrongType()
    {
        $this->stub->setDeliveryCloseTime('deliveryCloseTime');
    }
    //deliveryCloseTime 测试 --------------------------------------------------------   end

    //pickupNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPickupNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPickupNumberCorrectType()
    {
        $this->stub->setPickupNumber('pickupNumber');
        $this->assertEquals('pickupNumber', $this->stub->getPickupNumber());
    }

    /**
     * 设置 OrderFTL setPickupNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupNumberWrongType()
    {
        $this->stub->setPickupNumber(array('pickupNumber'));
    }
    //pickupNumber 测试 --------------------------------------------------------   end

    //remark 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setRemark() 正确的传参类型,期望传值正确
     */
    public function testSetRemarkCorrectType()
    {
        $this->stub->setRemark('remark');
        $this->assertEquals('remark', $this->stub->getRemark());
    }

    /**
     * 设置 OrderFTL setRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRemarkWrongType()
    {
        $this->stub->setRemark(array('remark'));
    }
    //remark 测试 --------------------------------------------------------   end

    //frozen 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setFrozen() 正确的传参类型,期望传值正确
     */
    public function testSetFrozenCorrectType()
    {
        $this->stub->setFrozen(0);
        $this->assertEquals(0, $this->stub->getFrozen());
    }

    /**
     * 设置 OrderFTL setFrozen() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFrozenWrongType()
    {
        $this->stub->setFrozen('frozen');
    }
    //frozen 测试 --------------------------------------------------------   end

    //memberOrder 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setMemberOrder() 正确的传参类型,期望传值正确
     */
    public function testSetMemberOrderCorrectType()
    {
        $memberOrder = new MemberOrder();
        $this->stub->setMemberOrder($memberOrder);
        $this->assertEquals($memberOrder, $this->stub->getMemberOrder());
    }

    /**
     * 设置 OrderFTL setMemberOrder() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberOrderWrongType()
    {
        $this->stub->setMemberOrder(array('memberOrder'));
    }
    //memberOrder 测试 --------------------------------------------------------   end

    //deliveryAddress 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setDeliveryAddress() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryAddressCorrectType()
    {
        $deliveryAddress = new Address();
        $this->stub->setDeliveryAddress($deliveryAddress);
        $this->assertEquals($deliveryAddress, $this->stub->getDeliveryAddress());
    }

    /**
     * 设置 OrderFTL setDeliveryAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryAddressWrongType()
    {
        $this->stub->setDeliveryAddress(array('deliveryAddress'));
    }
    //deliveryAddress 测试 --------------------------------------------------------   end

    //price 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPrice() 正确的传参类型,期望传值正确
     */
    public function testSetPriceCorrectType()
    {
        $this->stub->setPrice(0);
        $this->assertEquals(0, $this->stub->getPrice());
    }

    /**
     * 设置 OrderFTL setPrice() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceWrongType()
    {
        $this->stub->setPrice('price');
    }
    //price 测试 --------------------------------------------------------   end

    //priceRemark 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPriceRemark() 正确的传参类型,期望传值正确
     */
    public function testSetPriceRemarkCorrectType()
    {
        $this->stub->setPriceRemark(array('priceRemark'));
        $this->assertEquals(array('priceRemark'), $this->stub->getPriceRemark());
    }

    /**
     * 设置 OrderFTL setPriceRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceRemarkWrongType()
    {
        $this->stub->setPriceRemark('priceRemark');
    }
    //priceRemark 测试 --------------------------------------------------------   end

    //pickupAddressType 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPickupAddressType() 正确的传参类型,期望传值正确
     */
    public function testSetPickupAddressTypeCorrectType()
    {
        $this->stub->setPickupAddressType('pickupAddressType');
        $this->assertEquals('pickupAddressType', $this->stub->getPickupAddressType());
    }

    /**
     * 设置 OrderFTL setPickupAddressType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupAddressTypeWrongType()
    {
        $this->stub->setPickupAddressType(array('pickupAddressType'));
    }
    //pickupAddressType 测试 --------------------------------------------------------   end

    //deliveryAddressType 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setDeliveryAddressType() 正确的传参类型,期望传值正确
     */
    public function testSetDeliveryAddressTypeCorrectType()
    {
        $this->stub->setDeliveryAddressType('deliveryAddressType');
        $this->assertEquals('deliveryAddressType', $this->stub->getDeliveryAddressType());
    }

    /**
     * 设置 OrderFTL setDeliveryAddressType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDeliveryAddressTypeWrongType()
    {
        $this->stub->setDeliveryAddressType(array('deliveryAddressType'));
    }
    //deliveryAddressType 测试 --------------------------------------------------------   end

    //priceApiIndex 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPriceApiIndex() 正确的传参类型,期望传值正确
     */
    public function testSetPriceApiIndexCorrectType()
    {
        $this->stub->setPriceApiIndex(0);
        $this->assertEquals(0, $this->stub->getPriceApiIndex());
    }

    /**
     * 设置 OrderFTL setPriceApiIndex() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceApiIndexWrongType()
    {
        $this->stub->setPriceApiIndex('priceApiIndex');
    }
    //priceApiIndex 测试 --------------------------------------------------------   end

    //priceApiRecordId 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPriceApiRecordId() 正确的传参类型,期望传值正确
     */
    public function testSetPriceApiRecordIdCorrectType()
    {
        $this->stub->setPriceApiRecordId(0);
        $this->assertEquals(0, $this->stub->getPriceApiRecordId());
    }

    /**
     * 设置 OrderFTL setPriceApiRecordId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceApiRecordIdWrongType()
    {
        $this->stub->setPriceApiRecordId('priceApiRecordId');
    }
    //priceApiRecordId 测试 --------------------------------------------------------   end

    //priceApiIdentify 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setPriceApiIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetPriceApiIdentifyCorrectType()
    {
        $this->stub->setPriceApiIdentify('priceApiIdentify');
        $this->assertEquals('priceApiIdentify', $this->stub->getPriceApiIdentify());
    }

    /**
     * 设置 OrderFTL setPriceApiIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceApiIdentifyWrongType()
    {
        $this->stub->setPriceApiIdentify(array('priceApiIdentify'));
    }
    //priceApiIdentify 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTL setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 OrderFTL setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new OrderFTLMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\OrderFTLRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testMemberCancel()
    {
        // 为 OrderFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->memberCancel($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->memberCancel();

        $this->assertTrue($result);
    }

    public function testStaffCancel()
    {
        // 为 OrderFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->staffCancel($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->staffCancel();

        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        // 为 OrderFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->confirm($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->confirm();

        $this->assertTrue($result);
    }

    public function testBatchConfirm()
    {
        $objectList = array();
        // 为 OrderFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->batchConfirm($objectList)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->batchConfirm($objectList);

        $this->assertTrue($result);
    }

    public function testFreeze()
    {
        // 为 OrderFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->freeze($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->freeze();

        $this->assertTrue($result);
    }

    public function testUnFreeze()
    {
        // 为 OrderFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->unFreeze($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->unFreeze();

        $this->assertTrue($result);
    }

    public function testCalculate()
    {
        $list = array(new OrderFTL(1));
        // 为 OrderFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->calculate($this->stub)->shouldBeCalled(1)->willReturn($list);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->calculate();

        $this->assertEquals($result, $list);
    }
}
