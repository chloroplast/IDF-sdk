<?php
namespace Sdk\Order\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class OrderFTLPurviewTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(OrderFTLPurview::class)
                           ->setMethods(['operation'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsPurview()
    {
        $this->assertInstanceOf(
            'Sdk\Role\Purview\Model\Purview',
            $this->stub
        );
    }

    protected function initOperation($method)
    {
        $this->stub->expects($this->exactly(1))->method('operation')->with($method)->willReturn(true);

        $result = $this->stub->$method();

        $this->assertTrue($result);
    }

    public function testAdd()
    {
        $this->initOperation('add');
    }

    public function testEdit()
    {
        $this->initOperation('edit');
    }

    public function testStaffCancel()
    {
        $this->initOperation('staffCancel');
    }

    public function testConfirm()
    {
        $this->initOperation('confirm');
    }

    public function testBatchConfirm()
    {
        $objectList = array();
        $this->stub->expects($this->exactly(1))->method('operation')->with('batchConfirm')->willReturn(true);
        $result = $this->stub->batchConfirm($objectList);

        $this->assertTrue($result);
    }

    public function testFreeze()
    {
        $this->initOperation('freeze');
    }

    public function testUnFreeze()
    {
        $this->initOperation('unFreeze');
    }
}
