<?php
namespace Sdk\Order\Utils;

use Sdk\Order\Model\ItemLTL;

trait ItemLTLTranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(ItemLTL $itemLTL, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $itemLTL->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['width'])) {
            $this->assertEquals($attributes['width'], $itemLTL->getWidth());
        }
        if (isset($attributes['height'])) {
            $this->assertEquals($attributes['height'], $itemLTL->getHeight());
        }
        if (isset($attributes['weight'])) {
            $this->assertEquals($attributes['weight'], $itemLTL->getWeight());
        }
        if (isset($attributes['weightLB'])) {
            $this->assertEquals($attributes['weightLB'], $itemLTL->getWeightLB());
        }
        if (isset($attributes['weightUnit'])) {
            $this->assertEquals($attributes['weightUnit'], $itemLTL->getWeightUnit());
        }
        if (isset($attributes['goodsLevel'])) {
            $this->assertEquals($attributes['goodsLevel'], $itemLTL->getGoodsLevel());
        }
        if (isset($attributes['volumeInch'])) {
            $this->assertEquals($attributes['volumeInch'], $itemLTL->getVolumeInch());
        }
    }

    public function compareTranslatorEquals(array $expression, ItemLTL $itemLTL)
    {
        if (isset($expression['length'])) {
            $this->assertEquals($expression['length'], $itemLTL->getLength());
        }
        if (isset($expression['width'])) {
            $this->assertEquals($expression['width'], $itemLTL->getWidth());
        }
        if (isset($expression['height'])) {
            $this->assertEquals($expression['height'], $itemLTL->getHeight());
        }
        if (isset($expression['weight'])) {
            $this->assertEquals($expression['weight'], $itemLTL->getWeight());
        }
        if (isset($expression['weightLB'])) {
            $this->assertEquals($expression['weightLB'], $itemLTL->getWeightLB());
        }
        if (isset($expression['weightUnit'])) {
            $this->assertEquals($expression['weightUnit'], $itemLTL->getWeightUnit());
        }
        if (isset($expression['goodsLevel'])) {
            $this->assertEquals($expression['goodsLevel'], $itemLTL->getGoodsLevel());
        }
        if (isset($expression['volumeInch'])) {
            $this->assertEquals($expression['volumeInch'], $itemLTL->getVolumeInch());
        }
    }
}
