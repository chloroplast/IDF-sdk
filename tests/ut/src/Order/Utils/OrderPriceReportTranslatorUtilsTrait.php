<?php
namespace Sdk\Order\Utils;

use Sdk\Order\Model\OrderPriceReport;

trait OrderPriceReportTranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(OrderPriceReport $orderPriceReport, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $orderPriceReport->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['price'])) {
            $this->assertEquals($attributes['price'], $orderPriceReport->getPrice());
        }
        if (isset($attributes['priceRemark'])) {
            $this->assertEquals($attributes['priceRemark'], $orderPriceReport->getPriceRemark());
        }
    }

    public function compareTranslatorEquals(array $expression, OrderPriceReport $orderPriceReport)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($orderPriceReport->getId()));
        }
        if (isset($expression['price'])) {
            $this->assertEquals($expression['price'], $orderPriceReport->getPrice());
        }
        if (isset($expression['priceRemark'])) {
            $this->assertEquals($expression['priceRemark'], $orderPriceReport->getPriceRemark());
        }
    }
}
