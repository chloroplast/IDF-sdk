<?php
namespace Sdk\Order\Utils;

use Sdk\Order\Model\OrderPriceReport;
use Sdk\Order\Model\ItemLTL;

class MockObjectGenerate
{
    public static function generateOrderPriceReport(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : OrderPriceReport {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $orderPriceReport = new OrderPriceReport($id);
        $orderPriceReport->setId($id);

        //price
        $price = isset($value['price']) ? $value['price'] : $faker->randomDigitNotNull();
        $orderPriceReport->setPrice($price);

        //priceRemark
        $priceRemark = isset($value['priceRemark'])
        ? $value['priceRemark']
        : array(
            array(
                'identify' => 10,
                'percentage' => 0,
                'price' => 21
            ),
            array(
                'identify' => 16,
                'percentage' => 119,
                'price' => 0
            ),
            array(
                'identify' => 17,
                'percentage' => 0,
                'price' => 31
            )
        );
        $orderPriceReport->setPriceRemark($priceRemark);

        return $orderPriceReport;
    }
    public static function generateItemLTL(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ItemLTL {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $itemLTL = new ItemLTL($id);
        $itemLTL->setId($id);

        //length
        $length = isset($value['length']) ? $value['length'] : $faker->randomDigitNotNull();
        $itemLTL->setLength($length);

        //width
        $width = isset($value['width']) ? $value['width'] : $faker->randomDigitNotNull();
        $itemLTL->setWidth($width);

        //height
        $height = isset($value['height']) ? $value['height'] : $faker->randomDigitNotNull();
        $itemLTL->setHeight($height);
        
        //weight
        $weight = isset($value['weight']) ? $value['weight'] : $faker->randomDigitNotNull();
        $itemLTL->setWeight($weight);

        //weightLB
        $weightLB = isset($value['weightLB']) ? $value['weightLB'] : $faker->randomDigitNotNull();
        $itemLTL->setWeightLB($weightLB);
        
        //weightUnit
        $weightUnit = isset($value['weightUnit']) ? $value['weightUnit'] : $faker->randomDigitNotNull();
        $itemLTL->setWeightUnit($weightUnit);
        
        //goodsLevel
        $goodsLevel = isset($value['goodsLevel']) ? $value['goodsLevel'] : $faker->name();
        $itemLTL->setGoodsLevel($goodsLevel);
        
        //volumeInch
        $volumeInch = isset($value['volumeInch']) ? $value['volumeInch'] : $faker->randomDigitNotNull();
        $itemLTL->setVolumeInch($volumeInch);


        return $itemLTL;
    }
}
