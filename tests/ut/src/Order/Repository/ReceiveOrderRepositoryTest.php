<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\ReceiveOrder;
use Sdk\Order\Adapter\ReceiveOrder\IReceiveOrderAdapter;

class ReceiveOrderRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(ReceiveOrderRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIReceiveOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\ReceiveOrder\IReceiveOrderAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\ReceiveOrder\ReceiveOrderRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\ReceiveOrder\ReceiveOrderMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testRegisterPickupDate()
    {
        $object = new ReceiveOrder(1);
        // 为 IReceiveOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IReceiveOrderAdapter::class);
        // 建立预期状况:registerPickupDate() 方法将会被调用一次。
        $adapter->registerPickupDate($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->registerPickupDate($object);

        $this->assertTrue($result);
    }

    public function testPickup()
    {
        $object = new ReceiveOrder(1);
        // 为 IReceiveOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IReceiveOrderAdapter::class);
        // 建立预期状况:pickup() 方法将会被调用一次。
        $adapter->pickup($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->pickup($object);

        $this->assertTrue($result);
    }

    public function testStockIn()
    {
        $object = new ReceiveOrder(1);
        // 为 IReceiveOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IReceiveOrderAdapter::class);
        // 建立预期状况:stockIn() 方法将会被调用一次。
        $adapter->stockIn($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->stockIn($object);

        $this->assertTrue($result);
    }
}
