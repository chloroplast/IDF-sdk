<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\RecShipOrderLTLOrder;
use Sdk\Order\Adapter\RecShipOrderLTLOrder\IRecShipOrderLTLOrderAdapter;

class RecShipOrderLTLOrderRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(RecShipOrderLTLOrderRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRecShipOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecShipOrderLTLOrder\IRecShipOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecShipOrderLTLOrder\RecShipOrderLTLOrderRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecShipOrderLTLOrder\RecShipOrderLTLOrderMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
