<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Adapter\AmazonLTL\IAmazonLTLAdapter;

use Sdk\Application\ParseTask\Model\ParseTask;

class AmazonLTLRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(AmazonLTLRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIAmazonLTLAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\AmazonLTL\IAmazonLTLAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\AmazonLTL\AmazonLTLRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\AmazonLTL\AmazonLTLMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testMemberCancel()
    {
        $object = new AmazonLTL(1);
        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:memberCancel() 方法将会被调用一次。
        $adapter->memberCancel($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->memberCancel($object);

        $this->assertTrue($result);
    }

    public function testStaffCancel()
    {
        $object = new AmazonLTL(1);
        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:staffCancel() 方法将会被调用一次。
        $adapter->staffCancel($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->staffCancel($object);

        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        $object = new AmazonLTL(1);
        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:confirm() 方法将会被调用一次。
        $adapter->confirm($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->confirm($object);

        $this->assertTrue($result);
    }

    public function testBatchConfirm()
    {
        $objectList = array();
        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:batchConfirm() 方法将会被调用一次。
        $adapter->batchConfirm($objectList)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->batchConfirm($objectList);

        $this->assertTrue($result);
    }

    public function testRegisterPosition()
    {
        $object = new AmazonLTL(1);
        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:registerPosition() 方法将会被调用一次。
        $adapter->registerPosition($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->registerPosition($object);

        $this->assertTrue($result);
    }

    public function testFreeze()
    {
        $object = new AmazonLTL(1);
        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:freeze() 方法将会被调用一次。
        $adapter->freeze($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->freeze($object);

        $this->assertTrue($result);
    }

    public function testUnFreeze()
    {
        $object = new AmazonLTL(1);
        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:unFreeze() 方法将会被调用一次。
        $adapter->unFreeze($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->unFreeze($object);

        $this->assertTrue($result);
    }

    public function testParse()
    {
        $object = new ParseTask(1);
        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:parse() 方法将会被调用一次。
        $adapter->parse($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->parse($object);

        $this->assertTrue($result);
    }

    public function testAutoGroup()
    {
        $list = array(new AmazonLTL(1));

        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:autoGroup() 方法将会被调用一次。
        $adapter->autoGroup($list)->shouldBeCalled(1)->willReturn($list);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->autoGroup($list);

        $this->assertEquals($result, $list);
    }

    public function testCalculate()
    {
        $list = array(new AmazonLTL(1));

        $object = new AmazonLTL(1);
        // 为 IAmazonLTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLAdapter::class);
        // 建立预期状况:calculate() 方法将会被调用一次。
        $adapter->calculate($object)->shouldBeCalled(1)->willReturn($list);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->calculate($object);

        $this->assertEquals($result, $list);
    }
}
