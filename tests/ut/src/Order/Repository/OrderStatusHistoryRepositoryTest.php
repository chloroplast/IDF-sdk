<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

class OrderStatusHistoryRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderStatusHistoryRepository();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIOrderStatusHistoryAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\OrderStatusHistory\IOrderStatusHistoryAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\OrderStatusHistory\OrderStatusHistoryRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\OrderStatusHistory\OrderStatusHistoryMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
