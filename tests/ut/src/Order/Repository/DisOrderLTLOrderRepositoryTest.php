<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DisOrderLTLOrder;
use Sdk\Order\Adapter\DisOrderLTLOrder\IDisOrderLTLOrderAdapter;

class DisOrderLTLOrderRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(DisOrderLTLOrderRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDisOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisOrderLTLOrder\IDisOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisOrderLTLOrder\DisOrderLTLOrderRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisOrderLTLOrder\DisOrderLTLOrderMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testRegisterDispatchDate()
    {
        $object = new DisOrderLTLOrder(1);
        // 为 IDisOrderLTLOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IDisOrderLTLOrderAdapter::class);
        // 建立预期状况:registerDispatchDate() 方法将会被调用一次。
        $adapter->registerDispatchDate($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->registerDispatchDate($object);

        $this->assertTrue($result);
    }

    public function testDispatch()
    {
        $object = new DisOrderLTLOrder(1);
        // 为 IDisOrderLTLOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IDisOrderLTLOrderAdapter::class);
        // 建立预期状况:dispatch() 方法将会被调用一次。
        $adapter->dispatch($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->dispatch($object);

        $this->assertTrue($result);
    }

    public function testAccept()
    {
        $object = new DisOrderLTLOrder(1);
        // 为 IDisOrderLTLOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IDisOrderLTLOrderAdapter::class);
        // 建立预期状况:accept() 方法将会被调用一次。
        $adapter->accept($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->accept($object);

        $this->assertTrue($result);
    }
}
