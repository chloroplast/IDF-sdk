<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\ReceiveShippingOrder;
use Sdk\Order\Adapter\ReceiveShippingOrder\IReceiveShippingOrderAdapter;

class ReceiveShippingOrderRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(ReceiveShippingOrderRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIReceiveShippingOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\ReceiveShippingOrder\IReceiveShippingOrderAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\ReceiveShippingOrder\ReceiveShippingOrderRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\ReceiveShippingOrder\ReceiveShippingOrderMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
