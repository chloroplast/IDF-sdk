<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

class MemberOrderRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new MemberOrderRepository();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIMemberOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\MemberOrder\IMemberOrderAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\MemberOrder\MemberOrderRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\MemberOrder\MemberOrderMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
