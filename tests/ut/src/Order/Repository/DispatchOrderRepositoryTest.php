<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DispatchOrder;
use Sdk\Order\Adapter\DispatchOrder\IDispatchOrderAdapter;

class DispatchOrderRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(DispatchOrderRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDispatchOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DispatchOrder\IDispatchOrderAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DispatchOrder\DispatchOrderRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DispatchOrder\DispatchOrderMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testUpdateISA()
    {
        $object = new DispatchOrder(1);
        // 为 IDispatchOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IDispatchOrderAdapter::class);
        // 建立预期状况:updateISA() 方法将会被调用一次。
        $adapter->updateISA($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->updateISA($object);

        $this->assertTrue($result);
    }

    public function testRegisterDispatchDate()
    {
        $object = new DispatchOrder(1);
        // 为 IDispatchOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IDispatchOrderAdapter::class);
        // 建立预期状况:registerDispatchDate() 方法将会被调用一次。
        $adapter->registerDispatchDate($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->registerDispatchDate($object);

        $this->assertTrue($result);
    }

    public function testDispatch()
    {
        $object = new DispatchOrder(1);
        // 为 IDispatchOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IDispatchOrderAdapter::class);
        // 建立预期状况:dispatch() 方法将会被调用一次。
        $adapter->dispatch($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->dispatch($object);

        $this->assertTrue($result);
    }

    public function testAccept()
    {
        $object = new DispatchOrder(1);
        // 为 IDispatchOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IDispatchOrderAdapter::class);
        // 建立预期状况:accept() 方法将会被调用一次。
        $adapter->accept($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->accept($object);

        $this->assertTrue($result);
    }
}
