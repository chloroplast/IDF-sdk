<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\RecOrderLTLOrder;
use Sdk\Order\Adapter\RecOrderLTLOrder\IRecOrderLTLOrderAdapter;

class RecOrderLTLOrderRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(RecOrderLTLOrderRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRecOrderLTLOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecOrderLTLOrder\IRecOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecOrderLTLOrder\RecOrderLTLOrderRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\RecOrderLTLOrder\RecOrderLTLOrderMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testRegisterPickupDate()
    {
        $object = new RecOrderLTLOrder(1);
        // 为 IRecOrderLTLOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IRecOrderLTLOrderAdapter::class);
        // 建立预期状况:registerPickupDate() 方法将会被调用一次。
        $adapter->registerPickupDate($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->registerPickupDate($object);

        $this->assertTrue($result);
    }

    public function testPickup()
    {
        $object = new RecOrderLTLOrder(1);
        // 为 IRecOrderLTLOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IRecOrderLTLOrderAdapter::class);
        // 建立预期状况:pickup() 方法将会被调用一次。
        $adapter->pickup($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->pickup($object);

        $this->assertTrue($result);
    }

    public function testStockIn()
    {
        $object = new RecOrderLTLOrder(1);
        // 为 IRecOrderLTLOrderAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IRecOrderLTLOrderAdapter::class);
        // 建立预期状况:stockIn() 方法将会被调用一次。
        $adapter->stockIn($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->stockIn($object);

        $this->assertTrue($result);
    }
}
