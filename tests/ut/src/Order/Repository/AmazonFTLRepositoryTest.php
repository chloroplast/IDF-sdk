<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\AmazonFTL;
use Sdk\Order\Adapter\AmazonFTL\IAmazonFTLAdapter;

use Sdk\Application\ParseTask\Model\ParseTask;

class AmazonFTLRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(AmazonFTLRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIAmazonFTLAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\AmazonFTL\IAmazonFTLAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\AmazonFTL\AmazonFTLRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\AmazonFTL\AmazonFTLMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testMemberCancel()
    {
        $object = new AmazonFTL(1);
        // 为 IAmazonFTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonFTLAdapter::class);
        // 建立预期状况:memberCancel() 方法将会被调用一次。
        $adapter->memberCancel($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->memberCancel($object);

        $this->assertTrue($result);
    }

    public function testStaffCancel()
    {
        $object = new AmazonFTL(1);
        // 为 IAmazonFTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonFTLAdapter::class);
        // 建立预期状况:staffCancel() 方法将会被调用一次。
        $adapter->staffCancel($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->staffCancel($object);

        $this->assertTrue($result);
    }

    public function testConfirm()
    {
        $object = new AmazonFTL(1);
        // 为 IAmazonFTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonFTLAdapter::class);
        // 建立预期状况:confirm() 方法将会被调用一次。
        $adapter->confirm($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->confirm($object);

        $this->assertTrue($result);
    }

    public function testBatchConfirm()
    {
        $objectList = array();
        // 为 IAmazonFTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonFTLAdapter::class);
        // 建立预期状况:batchConfirm() 方法将会被调用一次。
        $adapter->batchConfirm($objectList)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->batchConfirm($objectList);

        $this->assertTrue($result);
    }

    public function testFreeze()
    {
        $object = new AmazonFTL(1);
        // 为 IAmazonFTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonFTLAdapter::class);
        // 建立预期状况:freeze() 方法将会被调用一次。
        $adapter->freeze($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->freeze($object);

        $this->assertTrue($result);
    }

    public function testUnFreeze()
    {
        $object = new AmazonFTL(1);
        // 为 IAmazonFTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonFTLAdapter::class);
        // 建立预期状况:unFreeze() 方法将会被调用一次。
        $adapter->unFreeze($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->unFreeze($object);

        $this->assertTrue($result);
    }

    public function testParse()
    {
        $object = new ParseTask(1);
        // 为 IAmazonFTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonFTLAdapter::class);
        // 建立预期状况:parse() 方法将会被调用一次。
        $adapter->parse($object)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->parse($object);

        $this->assertTrue($result);
    }

    public function testCalculate()
    {
        $list = array(new AmazonFTL(1));

        $object = new AmazonFTL(1);
        // 为 IAmazonFTLAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonFTLAdapter::class);
        // 建立预期状况:calculate() 方法将会被调用一次。
        $adapter->calculate($object)->shouldBeCalled(1)->willReturn($list);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->calculate($object);

        $this->assertEquals($result, $list);
    }
}
