<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DisShipOrderLTLOrder;
use Sdk\Order\Adapter\DisShipOrderLTLOrderRepository\IDisShipOrderLTLOrderRepositoryAdapter;

class DisShipOrderLTLOrderRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(DisShipOrderLTLOrderRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDisShipOrderLTLOrderRepositoryAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisShipOrderLTLOrder\IDisShipOrderLTLOrderAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisShipOrderLTLOrder\DisShipOrderLTLOrderRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DisShipOrderLTLOrder\DisShipOrderLTLOrderMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
