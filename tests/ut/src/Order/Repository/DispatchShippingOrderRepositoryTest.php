<?php
namespace Sdk\Order\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\DispatchShippingOrder;
use Sdk\Order\Adapter\DispatchShippingOrder\IDispatchShippingOrderAdapter;

class DispatchShippingOrderRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(DispatchShippingOrderRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIDispatchShippingOrderAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DispatchShippingOrder\IDispatchShippingOrderAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DispatchShippingOrder\DispatchShippingOrderRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Order\Adapter\DispatchShippingOrder\DispatchShippingOrderMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
