<?php
namespace Sdk\Order\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\OrderFTL;

class OrderFTLWidgetRuleTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderFTLWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //deliveryDate
    /**
     * @dataProvider additionProviderDeliveryDate
     */
    public function testDeliveryDate($parameter, $expected)
    {
        $result = $this->stub->deliveryDate($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERY_DATE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryDate()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(
                $faker->numberBetween(OrderFTLWidgetRule::MIN_TIMESTAMP, OrderFTLWidgetRule::MAX_TIMESTAMP),
                true
            ),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }
    
    //deliveryReadyTime
    /**
     * @dataProvider additionProviderDeliveryReadyTime
     */
    public function testDeliveryReadyTime($parameter, $expected)
    {
        $result = $this->stub->deliveryReadyTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERY_READYTIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryReadyTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(
                $faker->numberBetween(OrderFTLWidgetRule::MIN_TIMESTAMP, OrderFTLWidgetRule::MAX_TIMESTAMP),
                true
            ),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }
    
    //deliveryCloseTime
    /**
     * @dataProvider additionProviderDeliveryCloseTime
     */
    public function testDeliveryCloseTime($parameter, $expected)
    {
        $result = $this->stub->deliveryCloseTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERY_CLOSETIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryCloseTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(
                $faker->numberBetween(OrderFTLWidgetRule::MIN_TIMESTAMP, OrderFTLWidgetRule::MAX_TIMESTAMP),
                true
            ),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //weekendDelivery
    /**
     * @dataProvider additionProviderWeekendDelivery
     */
    public function testWeekendDelivery($parameter, $expected)
    {
        $result = $this->stub->weekendDelivery($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WEEKEND_DELIVERY_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderWeekendDelivery()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(OrderFTL::WEEKEND_DELIVERY)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //deliveryAddress
    /**
     * @dataProvider additionProviderDeliveryAddress
     */
    public function testDeliveryAddress($parameter, $expected)
    {
        $result = $this->stub->deliveryAddress($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYADDRESS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryAddress()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), true),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //orderFTL
    /**
     * @dataProvider additionProviderOrderFTL
     */
    public function testOrderFTL($parameter, $expected)
    {
        $result = $this->stub->orderFTL($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ORDERFTL_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderOrderFTL()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array($faker->randomNumber()), true),
            array($faker->name(), false),
            array($faker->words(), true)
        );
    }

    //deliveryName
    /**
     * @dataProvider additionProviderDeliveryName
     */
    public function testDeliveryName($parameter, $expected)
    {
        $result = $this->stub->deliveryName($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYNAME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryName()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
}
