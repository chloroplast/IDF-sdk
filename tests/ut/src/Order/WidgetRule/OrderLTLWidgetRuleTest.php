<?php
namespace Sdk\Order\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\AmazonLTL;
use Sdk\Order\Model\OrderLTL;

class OrderLTLWidgetRuleTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderLTLWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //items
    /**
     * @dataProvider additionProviderItems
     */
    public function testItems($parameter, $expected)
    {
        $result = $this->stub->items($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ITEMS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderItems()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array(), false),
            array(
                array(
                    array(
                        'description' => $faker->name(),
                        'itemNumber' => $faker->numberBetween(1, 9999),
                        'length' => $faker->randomFloat(),
                        'width' => $faker->randomFloat(),
                        'height' => $faker->randomFloat(),
                        'weight' => $faker->randomFloat(),
                        'weightUnit' => $faker->randomElement(array_values(AmazonLTL::WEIGHT_UNIT)),
                        'goodsLevel' => $faker->name(),
                        'stackable' => $faker->randomElement(array_values(OrderLTL::STACKABLE)),
                        'turnable' => $faker->randomElement(array_values(OrderLTL::TURNABLE)),
                        'packingType' => $faker->name()
                    ),
                    array(
                        'description' => $faker->name(),
                        'itemNumber' => $faker->numberBetween(1, 9999),
                        'length' => $faker->randomFloat(),
                        'width' => $faker->randomFloat(),
                        'height' => $faker->randomFloat(),
                        'weight' => $faker->randomFloat(),
                        'weightUnit' => $faker->randomElement(array_values(AmazonLTL::WEIGHT_UNIT)),
                        'goodsLevel' => $faker->name(),
                        'stackable' => $faker->randomElement(array_values(OrderLTL::STACKABLE)),
                        'turnable' => $faker->randomElement(array_values(OrderLTL::TURNABLE)),
                        'packingType' => $faker->name()
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'name' => $faker->numberBetween(1, 999)
                    )
                ),
                false
            ),
            array($faker->name(), false),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //itemsInfo
    /**
     * @dataProvider additionProviderItemsInfo
     */
    public function testItemsInfo($parameter, $expected)
    {
        $result = $this->stub->items($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
    }

    public function additionProviderItemsInfo()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(array(array('description' => $faker->words())), false),
            array(array(array('itemNumber' => $faker->name())), false),
            array(array(array('length' => $faker->numberBetween(100010001, 999999999))), false),
            array(array(array('width' => $faker->numberBetween(100010001, 999999999))), false),
            array(array(array('height' => $faker->numberBetween(100010001, 999999999))), false),
            array(array(array('weight' => $faker->numberBetween(100010001, 999999999))), false),
            array(array(array('weightUnit' => $faker->numberBetween(10, 100))), false),
            array(array(array('goodsLevel' => $faker->words())), false),
            array(array(array('stackable' => $faker->numberBetween(10, 100))), false),
            array(array(array('turnable' => $faker->numberBetween(10, 100))), false),
            array(array(array('packingType' => $faker->words())), false),
        );
    }

    //goodsLevel
    /**
     * @dataProvider additionProviderGoodsLevel
     */
    public function testGoodsLevel($parameter, $expected)
    {
        $result = $this->stub->goodsLevel($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(GOODSLEVEL_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderGoodsLevel()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //stackable
    /**
     * @dataProvider additionProviderStackable
     */
    public function testStackable($parameter, $expected)
    {
        $result = $this->stub->stackable($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(STACKABLE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderStackable()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(OrderLTL::STACKABLE)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //turnable
    /**
     * @dataProvider additionProviderTurnable
     */
    public function testTurnable($parameter, $expected)
    {
        $result = $this->stub->turnable($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TURNABLE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderTurnable()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(OrderLTL::TURNABLE)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //bookingPickupDate
    /**
     * @dataProvider additionProviderBookingPickupDate
     */
    public function testBookingPickupDate($parameter, $expected)
    {
        $result = $this->stub->bookingPickupDate($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(BOOKINGPICKUPDATE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderBookingPickupDate()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(
                $faker->numberBetween(
                    OrderLTLWidgetRule::MIN_TIMESTAMP, 
                    OrderLTLWidgetRule::MAX_TIMESTAMP
                ),
                true
            ),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //pickupZipCode
    /**
     * @dataProvider additionProviderPickupZipCode
     */
    public function testPickupZipCode($parameter, $expected)
    {
        $result = $this->stub->pickupZipCode($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PICKUPZIPCODE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPickupZipCode()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //pickupAddressType
    /**
     * @dataProvider additionProviderPickupAddressType
     */
    public function testPickupAddressType($parameter, $expected)
    {
        $result = $this->stub->pickupAddressType($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PICKUPADDRESSTYPE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPickupAddressType()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //deliveryZipCode
    /**
     * @dataProvider additionProviderDeliveryZipCode
     */
    public function testDeliveryZipCode($parameter, $expected)
    {
        $result = $this->stub->deliveryZipCode($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYZIPCODE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryZipCode()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //deliveryAddressType
    /**
     * @dataProvider additionProviderDeliveryAddressType
     */
    public function testDeliveryAddressType($parameter, $expected)
    {
        $result = $this->stub->deliveryAddressType($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYADDRESSTYPE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryAddressType()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //pickupNeedTransport
    /**
     * @dataProvider additionProviderPickupNeedTransport
     */
    public function testPickupNeedTransport($parameter, $expected)
    {
        $result = $this->stub->pickupNeedTransport($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PICKUPNEEDTRANSPORT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPickupNeedTransport()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(OrderLTL::NEEDTRANSPORT)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //pickupNeedLiftgate
    /**
     * @dataProvider additionProviderPickupNeedLiftgate
     */
    public function testPickupNeedLiftgate($parameter, $expected)
    {
        $result = $this->stub->pickupNeedLiftgate($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PICKUPNEEDLIFTGATE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPickupNeedLiftgate()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(OrderLTL::NEEDLIFTGATE)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //deliveryNeedTransport
    /**
     * @dataProvider additionProviderDeliveryNeedTransport
     */
    public function testDeliveryNeedTransport($parameter, $expected)
    {
        $result = $this->stub->deliveryNeedTransport($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYNEEDTRANSPORT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryNeedTransport()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(OrderLTL::NEEDTRANSPORT)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //deliveryNeedLiftgate
    /**
     * @dataProvider additionProviderDeliveryNeedLiftgate
     */
    public function testDeliveryNeedLiftgate($parameter, $expected)
    {
        $result = $this->stub->deliveryNeedLiftgate($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYNEEDLIFTGATE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryNeedLiftgate()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(OrderLTL::NEEDLIFTGATE)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //poNumber
    /**
     * @dataProvider additionProviderPoNumber
     */
    public function testPoNumber($parameter, $expected)
    {
        $result = $this->stub->poNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PONUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPoNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //pickupVehicleType
    /**
     * @dataProvider additionProviderPickupVehicleType
     */
    public function testPickupVehicleType($parameter, $expected)
    {
        $result = $this->stub->pickupVehicleType($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PICKUPVEHICLETYPE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPickupVehicleType()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(OrderLTL::PICKUPVEHICLETYPE)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //description
    /**
     * @dataProvider additionProviderDescription
     */
    public function testDescription($parameter, $expected)
    {
        $result = $this->stub->description($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DESCRIPTION_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDescription()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //deliveryDescription
    /**
     * @dataProvider additionProviderDeliveryDescription
     */
    public function testDeliveryDescription($parameter, $expected)
    {
        $result = $this->stub->deliveryDescription($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYDESCRIPTION_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryDescription()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //pickupRequirement
    /**
     * @dataProvider additionProviderPickupRequirement
     */
    public function testPickupRequirement($parameter, $expected)
    {
        $result = $this->stub->pickupRequirement($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PICKUPREQUIREMENT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPickupRequirement()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array(), false),
            array(
                array(
                    'category' => OrderLTL::CATEGORY['ADDRESS_OPEN'],
                    'day' => $faker->numberBetween(
                        OrderLTLWidgetRule::MIN_TIMESTAMP, 
                        OrderLTLWidgetRule::MAX_TIMESTAMP
                    ),
                    'startTime' => $faker->name(),
                    'endTime' => $faker->name()
                ),
                true
            ),
            array(
                array('name' => $faker->randomElement(array_values(OrderLTL::CATEGORY))), 
                false
            )
        );
    }

    //deliveryRequirement
    /**
     * @dataProvider additionProviderDeliveryRequirement
     */
    public function testDeliveryRequirement($parameter, $expected)
    {
        $result = $this->stub->deliveryRequirement($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYREQUIREMENT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryRequirement()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array(), false),
            array(
                array(
                    'category' => OrderLTL::CATEGORY['ADDRESS_OPEN'],
                    'day' => $faker->numberBetween(
                        OrderLTLWidgetRule::MIN_TIMESTAMP, 
                        OrderLTLWidgetRule::MAX_TIMESTAMP
                    ),
                    'startTime' => $faker->name(),
                    'endTime' => $faker->name()
                ),
                true
            ),
            array(
                array('name' => $faker->randomElement(array_values(OrderLTL::CATEGORY))), 
                false
            )
        );
    }

    //requirementInfo
    /**
     * @dataProvider additionProviderRequirementInfo
     */
    public function testRequirementInfo($parameter, $expected)
    {
        $result = $this->stub->requirement($parameter, PICKUPREQUIREMENT_FORMAT_INCORRECT);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
    }

    public function additionProviderRequirementInfo()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(
                array('category' => $faker->numberBetween(10, 100)), 
                false
            ),
            array(
                array(
                    'category' => OrderLTL::CATEGORY['ADDRESS_OPEN'],
                    'day' => $faker->name(),
                    'startTime' => $faker->name(),
                    'endTime' => $faker->name()
                ),
                false
            ),
            array(
                array(
                    'category' => OrderLTL::CATEGORY['ADDRESS_OPEN'],
                    'day' => $faker->numberBetween(
                        OrderLTLWidgetRule::MIN_TIMESTAMP, 
                        OrderLTLWidgetRule::MAX_TIMESTAMP
                    ),
                    'startTime' => $faker->words(),
                    'endTime' => $faker->name()
                ),
                false
            ),
            array(
                array(
                    'category' => OrderLTL::CATEGORY['ADDRESS_OPEN'],
                    'day' => $faker->numberBetween(
                        OrderLTLWidgetRule::MIN_TIMESTAMP, 
                        OrderLTLWidgetRule::MAX_TIMESTAMP
                    ),
                    'startTime' => $faker->name(),
                    'endTime' => $faker->words()
                ),
                false
            )
        );
    }

    //category
    /**
     * @dataProvider additionProviderCategory
     */
    public function testCategory($parameter, $expected)
    {
        $result = $this->stub->category($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(REQUIREMENT_CATEGORY_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderCategory()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(OrderLTL::CATEGORY)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //startTime
    /**
     * @dataProvider additionProviderStartTime
     */
    public function testStartTime($parameter, $expected)
    {
        $result = $this->stub->startTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(REQUIREMENT_STARTTIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderStartTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //endTime
    /**
     * @dataProvider additionProviderEndTime
     */
    public function testEndTime($parameter, $expected)
    {
        $result = $this->stub->endTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(REQUIREMENT_ENDTIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderEndTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //readyTime
    /**
     * @dataProvider additionProviderReadyTime
     */
    public function testReadyTime($parameter, $expected)
    {
        $result = $this->stub->readyTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(READYTIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderReadyTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //closeTime
    /**
     * @dataProvider additionProviderCloseTime
     */
    public function testCloseTime($parameter, $expected)
    {
        $result = $this->stub->closeTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CLOSETIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderCloseTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //deliveryReadyTime
    /**
     * @dataProvider additionProviderDeliveryReadyTime
     */
    public function testDeliveryReadyTime($parameter, $expected)
    {
        $result = $this->stub->deliveryReadyTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYREADYTIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryReadyTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //deliveryCloseTime
    /**
     * @dataProvider additionProviderDeliveryCloseTime
     */
    public function testDeliveryCloseTime($parameter, $expected)
    {
        $result = $this->stub->deliveryCloseTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERYCLOSETIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryCloseTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //address
    /**
     * @dataProvider additionProviderAddress
     */
    public function testAddress($parameter, $expected)
    {
        $result = $this->stub->address($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ORDERLTL_ADDRESS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderAddress()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), true),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //deliveryAddress
    /**
     * @dataProvider additionProviderDeliveryAddress
     */
    public function testDeliveryAddress($parameter, $expected)
    {
        $result = $this->stub->deliveryAddress($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ORDERLTL_DELIVERYADDRESS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryAddress()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), true),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //orderLTL
    /**
     * @dataProvider additionProviderOrderLTL
     */
    public function testOrderLTL($parameter, $expected)
    {
        $result = $this->stub->orderLTL($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ORDERLTL_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderOrderLTL()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array($faker->randomNumber()), true),
            array($faker->name(), false),
            array($faker->words(), true)
        );
    }

    //priceApiIdentify
    /**
     * @dataProvider additionProviderPriceApiIdentify
     */
    public function testPriceApiIdentify($parameter, $expected)
    {
        $result = $this->stub->priceApiIdentify($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PRICEAPIIDENTIFY_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPriceApiIdentify()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
}
