<?php
namespace Sdk\Order\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\AmazonLTL;

class AmazonLTLWidgetRuleTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonLTLWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //position
    /**
     * @dataProvider additionProviderPosition
     */
    public function testPosition($parameter, $expected)
    {
        $result = $this->stub->position($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(POSITION_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPosition()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //attachments
    /**
     * @dataProvider additionProviderAttachments
     */
    public function testAttachments($parameter, $expected)
    {
        $result = $this->stub->attachments($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ATTACHMENTS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderAttachments()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array(array('address' => $faker->name().'.pdf')), true),
            array(array(array('address' => $faker->name().'.jpg')), true),
            array(array(array('address' => $faker->name().'.png')), true),
            array(array(array('address' => $faker->name().'.bmp')), true),
            array(array(array('address' => $faker->name().'.jpeg')), true),
            array(array(array('address' => $faker->name().'.docx')), false),
            array($faker->name(), false)
        );
    }

    //isaNumber
    /**
     * @dataProvider additionProviderIsaNumber
     */
    public function testIsaNumber($parameter, $expected)
    {
        $result = $this->stub->isaNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ISA_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderIsaNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //pickupWarehouse
    /**
     * @dataProvider additionProviderPickupWarehouse
     */
    public function testPickupWarehouse($parameter, $expected)
    {
        $result = $this->stub->pickupWarehouse($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PICKUP_WAREHOUSE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPickupWarehouse()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->randomNumber(2), true)
        );
    }

    //address
    /**
     * @dataProvider additionProviderAddress
     */
    public function testAddress($parameter, $expected)
    {
        $result = $this->stub->address($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ADDRESS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderAddress()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->randomNumber(2), true)
        );
    }

    //references
    /**
     * @dataProvider additionProviderReferences
     */
    public function testReferences($parameter, $expected)
    {
        $result = $this->stub->references($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(REFERENCES_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderReferences()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //idfPickup
    /**
     * @dataProvider additionProviderIdfPickup
     */
    public function testIdfPickup($parameter, $expected)
    {
        $result = $this->stub->idfPickup($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(IDF_PICKUP_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderIdfPickup()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomElement(array_values(AmazonLTL::IDF_PICKUP)), true),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //items
    /**
     * @dataProvider additionProviderItems
     */
    public function testItems($parameter, $expected)
    {
        $result = $this->stub->items($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ITEMS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderItems()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array(), false),
            array(
                array(
                    array(
                        'warehouse' => $faker->numberBetween(1, 999),
                        'palletNumber' => $faker->numberBetween(1, 999),
                        'itemNumber' => $faker->numberBetween(1, 9999),
                        'weight' => $faker->randomFloat(),
                        'weightUnit' => $faker->randomElement(array_values(AmazonLTL::WEIGHT_UNIT)),
                        'fbaNumber' => $faker->name(),
                        'poNumber' => $faker->name(),
                        'soNumber' => $faker->name(),
                        'coNumber' => $faker->name(),
                        'length' => $faker->randomFloat(),
                        'width' => $faker->randomFloat(),
                        'height' => $faker->randomFloat()
                    ),
                    array(
                        'id' => $faker->numberBetween(1, 999),
                        'warehouse' => $faker->numberBetween(1, 999),
                        'palletNumber' => $faker->numberBetween(1, 999),
                        'itemNumber' => $faker->numberBetween(1, 9999),
                        'weight' => $faker->randomFloat(),
                        'weightUnit' => $faker->randomElement(array_values(AmazonLTL::WEIGHT_UNIT)),
                        'fbaNumber' => $faker->name(),
                        'poNumber' => $faker->name(),
                        'soNumber' => $faker->name(),
                        'coNumber' => $faker->name(),
                        'length' => $faker->randomFloat(),
                        'width' => $faker->randomFloat(),
                        'height' => $faker->randomFloat()
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'name' => $faker->numberBetween(1, 999)
                    )
                ),
                false
            ),
            array($faker->name(), false),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //itemsInfo
    /**
     * @dataProvider additionProviderItemsInfo
     */
    public function testItemsInfo($parameter, $expected)
    {
        $result = $this->stub->items($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
    }

    public function additionProviderItemsInfo()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(array(array('warehouse' => $faker->name())), false),
            array(array(array('palletNumber' => $faker->name())), false),
            array(array(array('itemNumber' => $faker->numberBetween(10000, 99999))), false),
            array(array(array('weight' => $faker->numberBetween(100010001, 999999999))), false),
            array(array(array('weightUnit' => $faker->numberBetween(10, 100))), false),
            array(array(array('fbaNumber' => $faker->words())), false),
            array(array(array('poNumber' => $faker->words())), false),
            array(array(array('soNumber' => $faker->words())), false),
            array(array(array('coNumber' => $faker->words())), false),
            array(array(array('length' => $faker->numberBetween(100010, 999999))), false),
            array(array(array('width' => $faker->numberBetween(100010, 999999))), false),
            array(array(array('height' => $faker->numberBetween(100010, 999999))), false)
        );
    }

    //warehouse
    /**
     * @dataProvider additionProviderWarehouse
     */
    public function testWarehouse($parameter, $expected)
    {
        $result = $this->stub->warehouse($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WAREHOUSE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderWarehouse()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), true),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //palletNumber
    /**
     * @dataProvider additionProviderPalletNumber
     */
    public function testPalletNumber($parameter, $expected)
    {
        $result = $this->stub->palletNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PALLET_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPalletNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->numberBetween(1, 999), true)
        );
    }

    //itemNumber
    /**
     * @dataProvider additionProviderItemNumber
     */
    public function testItemNumber($parameter, $expected)
    {
        $result = $this->stub->itemNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ITEM_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderItemNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->numberBetween(1, 9999), true)
        );
    }

    //weight
    /**
     * @dataProvider additionProviderWeight
     */
    public function testWeight($parameter, $expected)
    {
        $result = $this->stub->weight($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WEIGHT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderWeight()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->randomFloat(), true)
        );
    }

    //weightUnit
    /**
     * @dataProvider additionProviderWeightUnit
     */
    public function testWeightUnit($parameter, $expected)
    {
        $result = $this->stub->weightUnit($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WEIGHT_UNIT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderWeightUnit()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->randomElement(array_values(AmazonLTL::WEIGHT_UNIT)), true),
            array($faker->name(), false)
        );
    }

    //fbaNumber
    /**
     * @dataProvider additionProviderFbaNumber
     */
    public function testFbaNumber($parameter, $expected)
    {
        $result = $this->stub->fbaNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(FBA_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderFbaNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //poNumber
    /**
     * @dataProvider additionProviderPoNumber
     */
    public function testPoNumber($parameter, $expected)
    {
        $result = $this->stub->poNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PO_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPoNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //soNumber
    /**
     * @dataProvider additionProviderSoNumber
     */
    public function testSoNumber($parameter, $expected)
    {
        $result = $this->stub->soNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(SO_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderSoNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //coNumber
    /**
     * @dataProvider additionProviderCoNumber
     */
    public function testCoNumber($parameter, $expected)
    {
        $result = $this->stub->coNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CO_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderCoNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //pickupDate
    /**
     * @dataProvider additionProviderPickupDate
     */
    public function testPickupDate($parameter, $expected)
    {
        $result = $this->stub->pickupDate($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PICKUP_DATE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPickupDate()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(
                $faker->numberBetween(AmazonLTLWidgetRule::MIN_TIMESTAMP, AmazonLTLWidgetRule::MAX_TIMESTAMP),
                true
            ),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //readyTime
    /**
     * @dataProvider additionProviderReadyTime
     */
    public function testReadyTime($parameter, $expected)
    {
        $result = $this->stub->readyTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(READY_TIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderReadyTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(
                $faker->numberBetween(AmazonLTLWidgetRule::MIN_TIMESTAMP, AmazonLTLWidgetRule::MAX_TIMESTAMP),
                true
            ),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //closeTime
    /**
     * @dataProvider additionProviderCloseTime
     */
    public function testCloseTime($parameter, $expected)
    {
        $result = $this->stub->closeTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CLOSE_TIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderCloseTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(
                $faker->numberBetween(AmazonLTLWidgetRule::MIN_TIMESTAMP, AmazonLTLWidgetRule::MAX_TIMESTAMP),
                true
            ),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //pickupNumber
    /**
     * @dataProvider additionProviderPickupNumber
     */
    public function testPickupNumber($parameter, $expected)
    {
        $result = $this->stub->pickupNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PICKUP_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPickupNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //remark
    /**
     * @dataProvider additionProviderRemark
     */
    public function testRemark($parameter, $expected)
    {
        $result = $this->stub->remark($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(REMARK_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderRemark()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //stockInNumber
    /**
     * @dataProvider additionProviderStockInNumber
     */
    public function testStockInNumber($parameter, $expected)
    {
        $result = $this->stub->stockInNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(STOCKIN_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderStockInNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //stockInTime
    /**
     * @dataProvider additionProviderStockInTime
     */
    public function testStockInTime($parameter, $expected)
    {
        $result = $this->stub->stockInTime($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(STOCKIN_TIME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderStockInTime()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(
                $faker->numberBetween(AmazonLTLWidgetRule::MIN_TIMESTAMP, AmazonLTLWidgetRule::MAX_TIMESTAMP),
                true
            ),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //carType
    /**
     * @dataProvider additionProviderCarType
     */
    public function testCarType($parameter, $expected)
    {
        $result = $this->stub->carType($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CARTYPE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderCarType()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), true),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //amazonLTL
    /**
     * @dataProvider additionProviderAmazonLTL
     */
    public function testAmazonLTL($parameter, $expected)
    {
        $result = $this->stub->amazonLTL($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(AMAZONLTL_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderAmazonLTL()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array($faker->randomNumber()), true),
            array($faker->name(), false),
            array($faker->words(), true)
        );
    }

    //itemsAttachments
    /**
     * @dataProvider additionProviderItemsAttachments
     */
    public function testItemsAttachments($parameter, $expected)
    {
        $result = $this->stub->itemsAttachments($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ITEMS_ATTACHMENTS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderItemsAttachments()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array(array(array('address' => $faker->name().'.jpg'))), true),
            array(array(array(array('address' => $faker->name().'.docx'))), false),
            array($faker->name(), false)
        );
    }

    //length
    /**
     * @dataProvider additionProviderLength
     */
    public function testLength($parameter, $expected)
    {
        $result = $this->stub->length($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(LENGTH_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderLength()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->title(), false),
            array($faker->randomFloat(), true)
        );
    }

    //width
    /**
     * @dataProvider additionProviderWidth
     */
    public function testWidth($parameter, $expected)
    {
        $result = $this->stub->width($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WIDTH_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderWidth()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->title(), false),
            array($faker->randomFloat(), true)
        );
    }

    //height
    /**
     * @dataProvider additionProviderHeight
     */
    public function testHeight($parameter, $expected)
    {
        $result = $this->stub->height($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(HEIGHT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderHeight()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', true),
            array($faker->title(), false),
            array($faker->randomFloat(), true)
        );
    }

    //deliveryDate
    /**
     * @dataProvider additionProviderDeliveryDate
     */
    public function testDeliveryDate($parameter, $expected)
    {
        $result = $this->stub->deliveryDate($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DELIVERY_DATE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDeliveryDate()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(
                $faker->numberBetween(AmazonLTLWidgetRule::MIN_TIMESTAMP, AmazonLTLWidgetRule::MAX_TIMESTAMP),
                true
            ),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //closePage
    /**
     * @dataProvider additionProviderClosePage
     */
    public function testClosePage($parameter, $expected)
    {
        $result = $this->stub->closePage($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CLOSE_PAGE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderClosePage()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array(array('address' => $faker->name().'.jpg')), true),
            array(array(array('address' => $faker->name().'.docx')), false),
            array($faker->name(), false)
        );
    }

    //fileName
    /**
     * @dataProvider additionProviderFileName
     */
    public function testFileName($parameter, $expected)
    {
        $result = $this->stub->fileName($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(FILE_NAME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderFileName()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->name().'.xlsx', true),
            array($faker->name(), false)
        );
    }
}
