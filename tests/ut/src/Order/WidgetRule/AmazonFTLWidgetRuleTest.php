<?php
namespace Sdk\Order\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Order\Model\AmazonLTL;

class AmazonFTLWidgetRuleTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonFTLWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //items
    /**
     * @dataProvider additionProviderItems
     */
    public function testItems($parameter, $expected)
    {
        $result = $this->stub->items($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(ITEMS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderItems()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array(), false),
            array(
                array(
                    array(
                        'goodsType' => $faker->name(),
                        'goodsValue' => $faker->randomFloat(),
                        'packingType' => $faker->name(),
                        'palletNumber' => $faker->numberBetween(1, 999),
                        'itemNumber' => $faker->numberBetween(1, 9999),
                        'weight' => $faker->randomFloat(),
                        'weightUnit' => $faker->randomElement(array_values(AmazonLTL::WEIGHT_UNIT)),
                        'poNumber' => $faker->name(),
                        'length' => $faker->randomFloat(),
                        'width' => $faker->randomFloat(),
                        'height' => $faker->randomFloat()
                    ),
                    array(
                        'goodsType' => $faker->name(),
                        'goodsValue' => $faker->randomFloat(),
                        'packingType' => $faker->name(),
                        'palletNumber' => $faker->numberBetween(1, 999),
                        'itemNumber' => $faker->numberBetween(1, 9999),
                        'weight' => $faker->randomFloat(),
                        'weightUnit' => $faker->randomElement(array_values(AmazonLTL::WEIGHT_UNIT)),
                        'poNumber' => $faker->name(),
                        'length' => $faker->randomFloat(),
                        'width' => $faker->randomFloat(),
                        'height' => $faker->randomFloat()
                    )
                ),
                true
            ),
            array(
                array(
                    array(
                        'name' => $faker->numberBetween(1, 999)
                    )
                ),
                false
            ),
            array($faker->name(), false),
            array($faker->numberBetween(100, 999), false)
        );
    }

    //itemsInfo
    /**
     * @dataProvider additionProviderItemsInfo
     */
    public function testItemsInfo($parameter, $expected)
    {
        $result = $this->stub->items($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
    }

    public function additionProviderItemsInfo()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(array(array('goodsType' => $faker->words())), false),
            array(array(array('goodsValue' => $faker->numberBetween(100010001, 999999999))), false),
            array(array(array('packingType' => $faker->words())), false),
            array(array(array('palletNumber' => $faker->name())), false),
            array(array(array('itemNumber' => $faker->numberBetween(10000, 99999))), false),
            array(array(array('weight' => $faker->numberBetween(100010001, 999999999))), false),
            array(array(array('weightUnit' => $faker->numberBetween(10, 100))), false),
            array(array(array('poNumber' => $faker->words())), false),
            array(array(array('length' => $faker->numberBetween(100010001, 999999999))), false),
            array(array(array('width' => $faker->numberBetween(100010001, 999999999))), false),
            array(array(array('height' => $faker->numberBetween(100010001, 999999999))), false),
        );
    }

    //goodsType
    /**
     * @dataProvider additionProviderGoodsType
     */
    public function testGoodsType($parameter, $expected)
    {
        $result = $this->stub->goodsType($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(GOODSTYPE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderGoodsType()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //goodsValue
    /**
     * @dataProvider additionProviderGoodsValue
     */
    public function testGoodsValue($parameter, $expected)
    {
        $result = $this->stub->goodsValue($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(GOODSVALUE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderGoodsValue()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->randomFloat(), true)
        );
    }

    //packingType
    /**
     * @dataProvider additionProviderPackingType
     */
    public function testPackingType($parameter, $expected)
    {
        $result = $this->stub->packingType($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(PACKINGTYPE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPackingType()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //targetWarehouse
    /**
     * @dataProvider additionProviderTargetWarehouse
     */
    public function testTargetWarehouse($parameter, $expected)
    {
        $result = $this->stub->targetWarehouse($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(TARGET_WAREHOUSE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderTargetWarehouse()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), true),
            array($faker->name(), false),
            array($faker->words(), false)
        );
    }

    //amazonFTL
    /**
     * @dataProvider additionProviderAmazonFTL
     */
    public function testAmazonFTL($parameter, $expected)
    {
        $result = $this->stub->amazonFTL($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(AMAZONFTL_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderAmazonFTL()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array(array($faker->randomNumber()), true),
            array($faker->name(), false),
            array($faker->words(), true)
        );
    }

    //length
    /**
     * @dataProvider additionProviderLength
     */
    public function testLength($parameter, $expected)
    {
        $result = $this->stub->length($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(LENGTH_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderLength()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->randomFloat(), true)
        );
    }

    //width
    /**
     * @dataProvider additionProviderWidth
     */
    public function testWidth($parameter, $expected)
    {
        $result = $this->stub->width($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(WIDTH_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderWidth()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->randomFloat(), true)
        );
    }

    //height
    /**
     * @dataProvider additionProviderHeight
     */
    public function testHeight($parameter, $expected)
    {
        $result = $this->stub->height($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(HEIGHT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderHeight()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->randomFloat(), true)
        );
    }
}
