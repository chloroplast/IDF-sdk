<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

class ReceiveOrderRestfulTranslatorTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ReceiveOrderRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testGetStaffRestfulTranslator()
    {
        $stub = new ReceiveOrderRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffRestfulTranslator',
            $stub->getStaffRestfulTranslatorPublic()
        );
    }

    public function testGetCarTypeRestfulTranslator()
    {
        $stub = new ReceiveOrderRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\CarType\Translator\CarTypeRestfulTranslator',
            $stub->getCarTypeRestfulTranslatorPublic()
        );
    }

    public function testGetAmazonLTLRestfulTranslator()
    {
        $stub = new ReceiveOrderRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Order\Translator\AmazonLTLRestfulTranslator',
            $stub->getAmazonLTLRestfulTranslatorPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullReceiveOrder',
            $result
        );
    }

    public function testObjectToArrayEmpty()
    {
        $object = array();
        $result = $this->stub->objectToArray($object);

        $this->assertEmpty($result);
    }
}
