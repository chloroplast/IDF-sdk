<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

class ReceiveShippingOrderRestfulTranslatorTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ReceiveShippingOrderRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testGetWarehouseRestfulTranslator()
    {
        $stub = new ReceiveShippingOrderRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Warehouse\Translator\WarehouseRestfulTranslator',
            $stub->getWarehouseRestfulTranslatorPublic()
        );
    }

    public function testGetAmazonLTLRestfulTranslator()
    {
        $stub = new ReceiveShippingOrderRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Order\Translator\AmazonLTLRestfulTranslator',
            $stub->getAmazonLTLRestfulTranslatorPublic()
        );
    }

    public function testGetReceiveOrderRestfulTranslator()
    {
        $stub = new ReceiveShippingOrderRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Order\Translator\ReceiveOrderRestfulTranslator',
            $stub->getReceiveOrderRestfulTranslatorPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullReceiveShippingOrder',
            $result
        );
    }

    public function testObjectToArrayEmpty()
    {
        $object = array();
        $result = $this->stub->objectToArray($object);

        $this->assertEmpty($result);
    }
}
