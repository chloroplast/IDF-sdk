<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

class ReceiveOrderTranslatorTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ReceiveOrderTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetCarTypeTranslator()
    {
        $stub = new ReceiveOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\CarType\Translator\CarTypeTranslator',
            $stub->getCarTypeTranslatorPublic()
        );
    }

    public function testGetStaffTranslator()
    {
        $stub = new ReceiveOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffTranslator',
            $stub->getStaffTranslatorPublic()
        );
    }

    public function testGetNullObject()
    {
        $stub = new ReceiveOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullReceiveOrder',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullReceiveOrder',
            $result
        );
    }

    public function testObjectToArrayEmpty()
    {
        $object = array();
        $result = $this->stub->objectToArray($object);

        $this->assertEmpty($result);
    }
}
