<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

class AmazonFTLRestfulTranslatorTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonFTLRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testGetMemberRestfulTranslator()
    {
        $stub = new AmazonFTLRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\User\Member\Translator\MemberRestfulTranslator',
            $stub->getMemberRestfulTranslatorPublic()
        );
    }

    public function testGetWarehouseRestfulTranslator()
    {
        $stub = new AmazonFTLRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Warehouse\Translator\WarehouseRestfulTranslator',
            $stub->getWarehouseRestfulTranslatorPublic()
        );
    }

    public function testGetAddressRestfulTranslator()
    {
        $stub = new AmazonFTLRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Address\Translator\AddressRestfulTranslator',
            $stub->getAddressRestfulTranslatorPublic()
        );
    }

    public function testGetStaffRestfulTranslator()
    {
        $stub = new AmazonFTLRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffRestfulTranslator',
            $stub->getStaffRestfulTranslatorPublic()
        );
    }


    public function testGetMemberOrderRestfulTranslator()
    {
        $stub = new AmazonFTLRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Order\Translator\MemberOrderRestfulTranslator',
            $stub->getMemberOrderRestfulTranslatorPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullAmazonFTL',
            $result
        );
    }

    public function testObjectToArrayEmpty()
    {
        $object = array();
        $result = $this->stub->objectToArray($object);

        $this->assertEmpty($result);
    }
}
