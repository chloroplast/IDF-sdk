<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Utils\MockObjectGenerate;
use Sdk\Order\Utils\OrderPriceReportTranslatorUtilsTrait;

class OrderPriceReportRestfulTranslatorTest extends TestCase
{
    use OrderPriceReportTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderPriceReportRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullOrderPriceReport',
            $result
        );
    }

    public function testArrayToObject()
    {
        $orderPriceReport = MockObjectGenerate::generateOrderPriceReport();
        $expression['data']['id'] = $orderPriceReport->getId();
        $expression['data']['attributes']['price'] = $orderPriceReport->getPrice();
        $expression['data']['attributes']['priceRemark'] = $orderPriceReport->getPriceRemark();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Order\Model\OrderPriceReport',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $orderPriceReport = array();
        $result = $this->stub->objectToArray($orderPriceReport);

        $this->assertEmpty($result);
    }
}
