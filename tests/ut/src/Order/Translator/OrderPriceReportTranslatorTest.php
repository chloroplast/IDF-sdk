<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Utils\MockObjectGenerate;
use Sdk\Order\Utils\OrderPriceReportTranslatorUtilsTrait;

class OrderPriceReportTranslatorTest extends TestCase
{
    use OrderPriceReportTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderPriceReportTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new OrderPriceReportTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullOrderPriceReport',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $orderPriceReport = array();
        $result = $this->stub->objectToArray($orderPriceReport);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $orderPriceReport = MockObjectGenerate::generateOrderPriceReport();
        $result = $this->stub->objectToArray($orderPriceReport);

        $this->compareTranslatorEquals($result, $orderPriceReport);
    }
}
