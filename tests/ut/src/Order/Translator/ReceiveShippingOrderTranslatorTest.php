<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

class ReceiveShippingOrderTranslatorTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ReceiveShippingOrderTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetWarehouseTranslator()
    {
        $stub = new ReceiveShippingOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Warehouse\Translator\WarehouseTranslator',
            $stub->getWarehouseTranslatorPublic()
        );
    }

    public function testGetReceiveOrderTranslator()
    {
        $stub = new ReceiveShippingOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Translator\ReceiveOrderTranslator',
            $stub->getReceiveOrderTranslatorPublic()
        );
    }

    public function testGetAmazonLTLTranslator()
    {
        $stub = new ReceiveShippingOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Translator\AmazonLTLTranslator',
            $stub->getAmazonLTLTranslatorPublic()
        );
    }

    public function testGetNullObject()
    {
        $stub = new ReceiveShippingOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullReceiveShippingOrder',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $object = array();
        $result = $this->stub->objectToArray($object);

        $this->assertEmpty($result);
    }
}
