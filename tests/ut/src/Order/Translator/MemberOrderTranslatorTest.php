<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

class MemberOrderTranslatorTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new MemberOrderTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetMemberTranslator()
    {
        $stub = new MemberOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\User\Member\Translator\MemberTranslator',
            $stub->getMemberTranslatorPublic()
        );
    }

    public function testGetStaffTranslator()
    {
        $stub = new MemberOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffTranslator',
            $stub->getStaffTranslatorPublic()
        );
    }

    public function testGetNullObject()
    {
        $stub = new MemberOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullMemberOrder',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullMemberOrder',
            $result
        );
    }

    public function testObjectToArrayEmpty()
    {
        $object = array();
        $result = $this->stub->objectToArray($object);

        $this->assertEmpty($result);
    }
}
