<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Utils\MockObjectGenerate;
use Sdk\Order\Utils\ItemLTLTranslatorUtilsTrait;

class ItemLTLTranslatorTest extends TestCase
{
    use ItemLTLTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ItemLTLTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new ItemLTLTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullItemLTL',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $template = array();
        $result = $this->stub->objectToArray($template);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $template = MockObjectGenerate::generateItemLTL();
        $result = $this->stub->objectToArray($template);

        $this->compareTranslatorEquals($result, $template);
    }
}
