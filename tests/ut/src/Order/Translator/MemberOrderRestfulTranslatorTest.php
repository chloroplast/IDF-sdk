<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

class MemberOrderRestfulTranslatorTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new MemberOrderRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testGetMemberRestfulTranslator()
    {
        $stub = new MemberOrderRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\User\Member\Translator\MemberRestfulTranslator',
            $stub->getMemberRestfulTranslatorPublic()
        );
    }

    public function testGetStaffRestfulTranslator()
    {
        $stub = new MemberOrderRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffRestfulTranslator',
            $stub->getStaffRestfulTranslatorPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullMemberOrder',
            $result
        );
    }

    public function testObjectToArrayEmpty()
    {
        $object = array();
        $result = $this->stub->objectToArray($object);

        $this->assertEmpty($result);
    }
}
