<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Utils\MockObjectGenerate;
use Sdk\Order\Utils\ItemLTLTranslatorUtilsTrait;

class ItemLTLRestfulTranslatorTest extends TestCase
{
    use ItemLTLTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ItemLTLRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullItemLTL',
            $result
        );
    }

    public function testArrayToObject()
    {
        $itemLTL = MockObjectGenerate::generateItemLTL(1);

        $expression['data']['id'] = $itemLTL->getId();
        $expression['data']['attributes']['length'] = $itemLTL->getLength();
        $expression['data']['attributes']['width'] = $itemLTL->getWidth();
        $expression['data']['attributes']['height'] = $itemLTL->getHeight();
        $expression['data']['attributes']['weight'] = $itemLTL->getWeight();
        $expression['data']['attributes']['weightLB'] = $itemLTL->getWeightLB();
        $expression['data']['attributes']['weightUnit'] = $itemLTL->getWeightUnit();
        $expression['data']['attributes']['goodsLevel'] = $itemLTL->getGoodsLevel();
        $expression['data']['attributes']['volumeInch'] = $itemLTL->getVolumeInch();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Order\Model\ItemLTL',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $itemLTL = array();
        $result = $this->stub->objectToArray($itemLTL);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $itemLTL = MockObjectGenerate::generateItemLTL(1);

        $result = $this->stub->objectToArray($itemLTL);
        $this->compareRestfulTranslatorEquals($itemLTL, $result);
    }
}
