<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

class DisAmaFTLOrderTranslatorTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new DisAmaFTLOrderTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetStaffTranslator()
    {
        $stub = new DisAmaFTLOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffTranslator',
            $stub->getStaffTranslatorPublic()
        );
    }

    public function testGetNullObject()
    {
        $stub = new DisAmaFTLOrderTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullDisAmaFTLOrder',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullDisAmaFTLOrder',
            $result
        );
    }

    public function testObjectToArrayEmpty()
    {
        $object = array();
        $result = $this->stub->objectToArray($object);

        $this->assertEmpty($result);
    }
}
