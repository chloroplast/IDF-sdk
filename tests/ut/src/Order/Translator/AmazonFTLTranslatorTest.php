<?php
namespace Sdk\Order\Translator;

use PHPUnit\Framework\TestCase;

class AmazonFTLTranslatorTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonFTLTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetMemberTranslator()
    {
        $stub = new AmazonFTLTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\User\Member\Translator\MemberTranslator',
            $stub->getMemberTranslatorPublic()
        );
    }

    public function testGetStaffTranslator()
    {
        $stub = new AmazonFTLTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffTranslator',
            $stub->getStaffTranslatorPublic()
        );
    }

    public function testGetWarehouseTranslator()
    {
        $stub = new AmazonFTLTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Warehouse\Translator\WarehouseTranslator',
            $stub->getWarehouseTranslatorPublic()
        );
    }

    public function testGetAddressTranslator()
    {
        $stub = new AmazonFTLTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Address\Translator\AddressTranslator',
            $stub->getAddressTranslatorPublic()
        );
    }

    public function testGetMemberOrderTranslator()
    {
        $stub = new AmazonFTLTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Translator\MemberOrderTranslator',
            $stub->getMemberOrderTranslatorPublic()
        );
    }

    public function testGetNullObject()
    {
        $stub = new AmazonFTLTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Order\Model\NullAmazonFTL',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $object = array();
        $result = $this->stub->objectToArray($object);

        $this->assertEmpty($result);
    }
}
