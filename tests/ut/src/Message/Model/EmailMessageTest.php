<?php
namespace Sdk\Message\Model;

use PHPUnit\Framework\TestCase;

class EmailMessageTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new EmailMessage();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testEmailplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * EmailMessage 领域对象,测试构造函数
     */
    public function testMessageConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEquals(EmailMessage::STATUS['SUCCESS'], $this->stub->getStatus());
        $this->assertEmpty($this->stub->getAddress());
        $this->assertEmpty($this->stub->getErrorDescription());
        $this->assertInstanceOf('Sdk\Message\Model\Message', $this->stub->getMessage());
        $this->assertEmpty($this->stub->getCreateTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 EmailMessage setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 EmailMessage setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //address 测试 -------------------------------------------------------- start
    /**
     * 设置 EmailMessage setAddress() 正确的传参类型,期望传值正确
     */
    public function testSetAddressCorrectType()
    {
        $this->stub->setAddress(array('address'));
        $this->assertEquals(array('address'), $this->stub->getAddress());
    }

    /**
     * 设置 EmailMessage setAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetAddressWrongType()
    {
        $this->stub->setAddress('address');
    }
    //address 测试 --------------------------------------------------------   end

    //message 测试 -------------------------------------------------------- start
    /**
     * 设置 EmailMessage setMessage() 正确的传参类型,期望传值正确
     */
    public function testSetMessageCorrectType()
    {
        $message = new Message();
        $this->stub->setMessage($message);
        $this->assertEquals($message, $this->stub->getMessage());
    }

    /**
     * 设置 EmailMessage setMessage() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMessageWrongType()
    {
        $this->stub->setMessage(array('message'));
    }
    //message 测试 --------------------------------------------------------   end

    //errorDescription 测试 -------------------------------------------------------- start
    /**
     * 设置 EmailMessage setErrorDescription() 正确的传参类型,期望传值正确
     */
    public function testSetErrorDescriptionCorrectType()
    {
        $this->stub->setErrorDescription('errorDescription');
        $this->assertEquals('errorDescription', $this->stub->getErrorDescription());
    }

    /**
     * 设置 EmailMessage setErrorDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorDescriptionWrongType()
    {
        $this->stub->setErrorDescription(array('errorDescription'));
    }
    //errorDescription 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 EmailMessage setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 EmailMessage setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new EmailMessageMock();
        $this->assertInstanceOf(
            'Sdk\Message\Repository\EmailMessageRepository',
            $stub->getRepositoryPublic()
        );
    }
}
