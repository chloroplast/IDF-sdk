<?php
namespace Sdk\Message\Model;

use PHPUnit\Framework\TestCase;

use Sdk\User\Staff\Model\Staff;
use Sdk\Message\Repository\MessageRepository;

class MessageTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(Message::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    
    /**
     * Message 领域对象,测试构造函数
     */
    public function testMessageConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getTitle());
        $this->assertEmpty($this->stub->getContent());
        $this->assertEmpty($this->stub->getMessageType());
        $this->assertEmpty($this->stub->getMessageScope());
        $this->assertEmpty($this->stub->getExpireTime());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getPublisher());
        $this->assertEquals(Message::M_STATUS['ALL_SUCCESS'], $this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Message setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Message setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //title 测试 -------------------------------------------------------- start
    /**
     * 设置 Message setTitle() 正确的传参类型,期望传值正确
     */
    public function testSetTitleCorrectType()
    {
        $this->stub->setTitle('title');
        $this->assertEquals('title', $this->stub->getTitle());
    }

    /**
     * 设置 Message setTitle() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTitleWrongType()
    {
        $this->stub->setTitle(array('title'));
    }
    //title 测试 --------------------------------------------------------   end

    //content 测试 -------------------------------------------------------- start
    /**
     * 设置 Message setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->stub->setContent('content');
        $this->assertEquals('content', $this->stub->getContent());
    }

    /**
     * 设置 Message setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->stub->setContent(array('content'));
    }
    //content 测试 --------------------------------------------------------   end

    //messageType 测试 -------------------------------------------------------- start
    /**
     * 设置 Message setMessageType() 正确的传参类型,期望传值正确
     */
    public function testSetMessageTypeCorrectType()
    {
        $this->stub->setMessageType(1);
        $this->assertEquals(1, $this->stub->getMessageType());
    }

    /**
     * 设置 Message setMessageType() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMessageTypeWrongType()
    {
        $this->stub->setMessageType('messageType');
    }
    //messageType 测试 --------------------------------------------------------   end

    //messageScope 测试 -------------------------------------------------------- start
    /**
     * 设置 Message setMessageScope() 正确的传参类型,期望传值正确
     */
    public function testSetMessageScopeCorrectType()
    {
        $this->stub->setMessageScope(1);
        $this->assertEquals(1, $this->stub->getMessageScope());
    }

    /**
     * 设置 Message setMessageScope() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMessageScopeWrongType()
    {
        $this->stub->setMessageScope('messageScope');
    }
    //messageScope 测试 --------------------------------------------------------   end

    //receiver 测试 -------------------------------------------------------- start
    /**
     * 设置 Message setReceiver() 正确的传参类型,期望传值正确
     */
    public function testSetReceiverCorrectType()
    {
        $receiver = new Staff();
        $this->stub->setReceiver($receiver);
        $this->assertEquals($receiver, $this->stub->getReceiver());
    }

    /**
     * 设置 Message setReceiver() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReceiverWrongType()
    {
        $this->stub->setReceiver(array('receiver'));
    }
    //receiver 测试 --------------------------------------------------------   end

    //expireTime 测试 -------------------------------------------------------- start
    /**
     * 设置 Message setExpireTime() 正确的传参类型,期望传值正确
     */
    public function testSetExpireTimeCorrectType()
    {
        $this->stub->setExpireTime(1);
        $this->assertEquals(1, $this->stub->getExpireTime());
    }

    /**
     * 设置 Message setExpireTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetExpireTimeWrongType()
    {
        $this->stub->setExpireTime('expireTime');
    }
    //expireTime 测试 --------------------------------------------------------   end

    //publisher 测试 -------------------------------------------------------- start
    /**
     * 设置 Message setPublisher() 正确的传参类型,期望传值正确
     */
    public function testSetPublisherCorrectType()
    {
        $publisher = new Staff();
        $this->stub->setPublisher($publisher);
        $this->assertEquals($publisher, $this->stub->getPublisher());
    }

    /**
     * 设置 Message setPublisher() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPublisherWrongType()
    {
        $this->stub->setPublisher(array('publisher'));
    }
    //publisher 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new MessageMock();
        $this->assertInstanceOf(
            'Sdk\Message\Repository\MessageRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testInsert()
    {
        // 为 MessageRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(MessageRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->insert($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->insert();

        $this->assertTrue($result);
    }

    public function testInsertIsPerson()
    {
        $this->stub->setMessageScope(Message::MESSAGE_SCOPE_IS_PERSON['MEMBER']);
        // 为 MessageRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(MessageRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->insertIsPerson($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->insert();

        $this->assertTrue($result);
    }
}
