<?php
namespace Sdk\Message\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullEmailMessageTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullEmailMessage::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsEmailMessage()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Model\EmailMessage',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullEmailMessageMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
