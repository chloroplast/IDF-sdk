<?php
namespace Sdk\Message\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullMessageTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullMessage::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsMessage()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Model\Message',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullMessageMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function initOperation($method)
    {
        $stub = $this->getMockBuilder(NullMessageMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
        $stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $stub->$method();
 
        $this->assertFalse($result);
    }

    public function testInsert()
    {
        $this->initOperation('insert');
    }
}
