<?php
namespace Sdk\Message\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullImMessageTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullImMessage::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsImMessage()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Model\ImMessage',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullImMessageMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
