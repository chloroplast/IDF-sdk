<?php
namespace Sdk\Message\Model;

use PHPUnit\Framework\TestCase;

use Sdk\User\Staff\Model\Staff;
use Sdk\Message\Repository\ImMessageRepository;

class ImMessageTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(ImMessageMock::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * ImMessage 领域对象,测试构造函数
     */
    public function testMessageConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getReceiver());
        $this->assertEquals(ImMessage::STATUS['UNREAD'], $this->stub->getStatus());
        $this->assertEmpty($this->stub->getReadTime());
        $this->assertInstanceOf('Sdk\Message\Model\Message', $this->stub->getMessage());
        $this->assertEmpty($this->stub->getCreateTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 ImMessage setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 ImMessage setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //receiver 测试 -------------------------------------------------------- start
    /**
     * 设置 ImMessage setReceiver() 正确的传参类型,期望传值正确
     */
    public function testSetReceiverCorrectType()
    {
        $receiver = new Staff();
        $this->stub->setReceiver($receiver);
        $this->assertEquals($receiver, $this->stub->getReceiver());
    }

    /**
     * 设置 ImMessage setReceiver() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReceiverWrongType()
    {
        $this->stub->setReceiver(array('receiver'));
    }
    //receiver 测试 --------------------------------------------------------   end

    //readTime 测试 -------------------------------------------------------- start
    /**
     * 设置 ImMessage setReadTime() 正确的传参类型,期望传值正确
     */
    public function testSetReadTimeCorrectType()
    {
        $this->stub->setReadTime(1);
        $this->assertEquals(1, $this->stub->getReadTime());
    }

    /**
     * 设置 ImMessage setReadTime() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetReadTimeWrongType()
    {
        $this->stub->setReadTime('readTime');
    }
    //readTime 测试 --------------------------------------------------------   end

    //message 测试 -------------------------------------------------------- start
    /**
     * 设置 ImMessage setMessage() 正确的传参类型,期望传值正确
     */
    public function testSetMessageCorrectType()
    {
        $message = new Message();
        $this->stub->setMessage($message);
        $this->assertEquals($message, $this->stub->getMessage());
    }

    /**
     * 设置 ImMessage setMessage() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMessageWrongType()
    {
        $this->stub->setMessage(array('message'));
    }
    //message 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 Message setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(ImMessage::STATUS['UNREAD']);
        $this->assertEquals(ImMessage::STATUS['UNREAD'], $this->stub->getStatus());
    }

    /**
     * 设置 Message setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new ImMessageMock();
        $this->assertInstanceOf(
            'Sdk\Message\Repository\ImMessageRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testRead()
    {
        $key = array('receiverStaff');
        // 为 ImMessageRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(ImMessageRepository::class);
        // 建立预期状况:read() 方法将会被调用一次。
        $repository->read($this->stub, $key)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->read($key);

        $this->assertTrue($result);
    }
}
