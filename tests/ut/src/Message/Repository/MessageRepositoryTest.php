<?php
namespace Sdk\Message\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Message\Model\Message;
use Sdk\Message\Adapter\Message\IMessageAdapter;

class MessageRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(MessageRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIMessageAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\Message\IMessageAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\Message\MessageRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\Message\MessageMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testInsertIsPerson()
    {
        $message = new Message(1);
        // 为 IMessageAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IMessageAdapter::class);
        // 建立预期状况:insertIsPerson() 方法将会被调用一次。
        $adapter->insertIsPerson($message)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->insertIsPerson($message);

        $this->assertTrue($result);
    }
}
