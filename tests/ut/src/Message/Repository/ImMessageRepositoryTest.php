<?php
namespace Sdk\Message\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Message\Model\ImMessage;
use Sdk\Message\Adapter\ImMessage\IImMessageAdapter;

class ImMessageRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(ImMessageRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIImMessageAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\ImMessage\IImMessageAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\ImMessage\ImMessageRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\ImMessage\ImMessageMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testRead()
    {
        $imMessage = new ImMessage(1);
        $key = array('receiverStaff');
        // 为 IImMessageAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IImMessageAdapter::class);
        // 建立预期状况:read() 方法将会被调用一次。
        $adapter->read($imMessage, $key)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->read($imMessage, $key);

        $this->assertTrue($result);
    }
}
