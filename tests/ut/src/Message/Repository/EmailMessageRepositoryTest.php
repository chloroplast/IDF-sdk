<?php
namespace Sdk\Message\Repository;

use PHPUnit\Framework\TestCase;

class EmailMessageRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new EmailMessageRepository();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIEmailMessageAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\EmailMessage\IEmailMessageAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\EmailMessage\EmailMessageRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\EmailMessage\EmailMessageMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
