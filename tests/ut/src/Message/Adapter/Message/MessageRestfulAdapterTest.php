<?php
namespace Sdk\Message\Adapter\Message;

use PHPUnit\Framework\TestCase;

use Sdk\Message\Model\MessageMock;
use Sdk\Message\Translator\MessageRestfulTranslator;

class MessageRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(MessageRestfulAdapterMock::class)
                           ->setMethods([
                               'getTranslator',
                               'post',
                               'getResource',
                               'isSuccess',
                               'translateToObject'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIMessageAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\Message\IMessageAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Message\Model\NullMessage',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array('MESSAGE_LIST', MessageRestfulAdapter::SCENARIOS['MESSAGE_LIST']),
            array('MESSAGE_FETCH_ONE', MessageRestfulAdapter::SCENARIOS['MESSAGE_FETCH_ONE']),
            array('', [])
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(MessageRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    public function testInsertTranslatorKeys()
    {
        $this->assertEquals(array(
            'title',
            'content',
            'messageScope',
            'messageType',
            'expireTime',
            'publisher'
        ), $this->stub->insertTranslatorKeysPublic());
    }

    public function testUpdateTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->updateTranslatorKeysPublic());
    }

    public function testEnableTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->enableTranslatorKeysPublic());
    }

    public function testDisableTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->disableTranslatorKeysPublic());
    }

    public function testDeletedTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->deletedTranslatorKeysPublic());
    }

    private function insertIsPerson(bool $result)
    {
        $id = 1;
        $resource = 'messages';
        $data = array();
        $keys = array(
            'title',
            'content',
            'messageScope',
            'messageType',
            'expireTime',
            'publisher',
            'receiver'
        );
        $message = new MessageMock($id);

        // 为 MessageRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(MessageRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($message, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('post')->with($resource, $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($message);
        }

        return $message;
    }

    public function testInsertIsPersonTrue()
    {
        $message = $this->insertIsPerson(true);

        $result = $this->stub->insertIsPerson($message);

        $this->assertTrue($result);
    }

    public function testInsertIsPersonFalse()
    {
        $message = $this->insertIsPerson(false);

        $result = $this->stub->insertIsPerson($message);

        $this->assertFalse($result);
    }
}
