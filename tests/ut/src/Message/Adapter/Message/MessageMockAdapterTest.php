<?php
namespace Sdk\Message\Adapter\Message;

use PHPUnit\Framework\TestCase;

use Sdk\Message\Model\Message;

class MessageMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new MessageMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIMessageAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\Message\IMessageAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Model\Message',
            $this->stub->fetchObject(1)
        );
    }

    public function testInsertIsPerson()
    {
        $messagem = new Message(1);

        $this->assertTrue($this->stub->insertIsPerson($messagem));
    }
}
