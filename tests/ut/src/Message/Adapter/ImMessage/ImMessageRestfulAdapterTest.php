<?php
namespace Sdk\Message\Adapter\ImMessage;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Sdk\Message\Model\ImMessageMock;
use Sdk\Message\Translator\ImMessageRestfulTranslator;

class ImMessageRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(ImMessageRestfulAdapterMock::class)
                           ->setMethods([
                               'patch',
                               'getResource',
                               'isSuccess',
                               'translateToObject'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIImMessageAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\ImMessage\IImMessageAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Message\Model\NullImMessage',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array('IM_MESSAGE_LIST', ImMessageRestfulAdapter::SCENARIOS['IM_MESSAGE_LIST']),
            array('IM_MESSAGE_FETCH_ONE', ImMessageRestfulAdapter::SCENARIOS['IM_MESSAGE_FETCH_ONE']),
            array('', [])
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(ImMessageRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    private function read(bool $result)
    {
        $id = 1;
        $resource = 'messages/im';
        $data = array(
            'data' => array(
                'type' => 'internalMessages',
                'relationships' => array(
                    'receiverStaff' => array(
                        'data' => array(
                            'type' => 'staff',
                            'id' => strval(Core::$container->get('staff')->getId())
                        )
                    )
                )
            )
        );
        $imMessage = new ImMessageMock($id);

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/read', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($imMessage);
        }

        return $imMessage;
    }

    public function testReadTrue()
    {
        $imMessage = $this->read(true);
        $key = array('receiverStaff');

        $result = $this->stub->read($imMessage, $key);

        $this->assertTrue($result);
    }

    public function testReadFalse()
    {
        $imMessage = $this->read(false);
        $key = array('receiverStaff');

        $result = $this->stub->read($imMessage, $key);

        $this->assertFalse($result);
    }
}
