<?php
namespace Sdk\Message\Adapter\ImMessage;

use PHPUnit\Framework\TestCase;

use Sdk\Message\Model\ImMessage;

class ImMessageMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ImMessageMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIMessageAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\ImMessage\IImMessageAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Model\ImMessage',
            $this->stub->fetchObject(1)
        );
    }

    public function testRead()
    {
        $imMessage = new ImMessage(1);
        $key = array('receiverStaff');

        $this->assertTrue($this->stub->read($imMessage, $key));
    }
}
