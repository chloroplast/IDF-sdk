<?php
namespace Sdk\Message\Adapter\EmailMessage;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Sdk\Message\Model\EmailMessageMock;
use Sdk\Message\Translator\EmailMessageRestfulTranslator;

class EmailMessageRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new EmailMessageRestfulAdapterMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIEmailMessageAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\EmailMessage\IEmailMessageAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Message\Model\NullEmailMessage',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array('EMAIL_MESSAGE_LIST', EmailMessageRestfulAdapter::SCENARIOS['EMAIL_MESSAGE_LIST']),
            array('EMAIL_MESSAGE_FETCH_ONE', EmailMessageRestfulAdapter::SCENARIOS['EMAIL_MESSAGE_FETCH_ONE']),
            array('', [])
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(EmailMessageRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }
}
