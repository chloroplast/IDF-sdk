<?php
namespace Sdk\Message\Adapter\EmailMessage;

use PHPUnit\Framework\TestCase;

use Sdk\Message\Model\EmailMessage;

class EmailMessageMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new EmailMessageMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIMessageAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Adapter\EmailMessage\IEmailMessageAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Message\Model\EmailMessage',
            $this->stub->fetchObject(1)
        );
    }
}
