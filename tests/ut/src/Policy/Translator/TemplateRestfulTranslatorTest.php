<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\TemplateTranslatorUtilsTrait;

class TemplateRestfulTranslatorTest extends TestCase
{
    use TemplateTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new TemplateRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullTemplate',
            $result
        );
    }

    public function testArrayToObject()
    {
        $template = MockObjectGenerate::generateTemplate();
        $expression['data']['attributes']['template'] = $template->getContent();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\Template',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $template = array();
        $result = $this->stub->objectToArray($template);

        $this->assertEmpty($result);
    }
}
