<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\AmazonLTLPolicyTranslatorUtilsTrait;

use Sdk\Warehouse\Translator\WarehouseTranslator;

class AmazonLTLPolicyTranslatorTest extends TestCase
{
    use AmazonLTLPolicyTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonLTLPolicyTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetWarehouseTranslator()
    {
        $stub = new AmazonLTLPolicyTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Warehouse\Translator\WarehouseTranslator',
            $stub->getWarehouseTranslatorPublic()
        );
    }

    public function testGetNullObject()
    {
        $stub = new AmazonLTLPolicyTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullAmazonLTLPolicy',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $amazonLTLPolicy = array();
        $result = $this->stub->objectToArray($amazonLTLPolicy);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(AmazonLTLPolicyTranslatorMock::class)
                           ->setMethods([
                               'getWarehouseTranslator',
                               'statusFormatConversion'
                            ])->getMock();

        $amazonLTLPolicy = MockObjectGenerate::generateAmazonLTLPolicy(1);
        $warehouse = $amazonLTLPolicy->getSelfOperatedWarehouse();
        $warehouseArray = array('warehouseArray');
        $status = $amazonLTLPolicy->getStatus();
        $statusFormatConversion = array('statusFormatConversion');
        
        // 为 WarehouseTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(WarehouseTranslator::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $translator->objectToArray($warehouse, ['id', 'code'])->shouldBeCalled(1)->willReturn($warehouseArray);
        // 为 getWarehouseTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $stub->expects($this->exactly(1))->method(
            'getWarehouseTranslator'
        )->willReturn($translator->reveal());
        
        $stub->expects($this->exactly(1))->method(
            'statusFormatConversion'
        )->with($status)->willReturn($statusFormatConversion);

        $result = $stub->objectToArray($amazonLTLPolicy);

        $this->assertEquals($result['selfOperatedWarehouse'], $warehouseArray);
        $this->assertEquals($result['status'], $statusFormatConversion);

        $this->compareTranslatorEquals($result, $amazonLTLPolicy);
    }
}
