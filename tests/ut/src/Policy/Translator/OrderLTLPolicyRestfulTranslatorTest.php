<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\OrderLTLPolicyTranslatorUtilsTrait;

class OrderLTLPolicyRestfulTranslatorTest extends TestCase
{
    use OrderLTLPolicyTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderLTLPolicyRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullOrderLTLPolicy',
            $result
        );
    }

    public function testArrayToObject()
    {
        $orderLTLPolicy = MockObjectGenerate::generateOrderLTLPolicy(1);

        $expression['data']['id'] = $orderLTLPolicy->getId();
        $expression['data']['attributes']['name'] = $orderLTLPolicy->getName();
        $expression['data']['attributes']['description'] = $orderLTLPolicy->getDescription();
        $expression['data']['attributes']['content'] = $orderLTLPolicy->getContent();
        $expression['data']['attributes']['status'] = $orderLTLPolicy->getStatus();
        $expression['data']['attributes']['statusTime'] = $orderLTLPolicy->getStatusTime();
        $expression['data']['attributes']['createTime'] = $orderLTLPolicy->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $orderLTLPolicy->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\OrderLTLPolicy',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $orderLTLPolicy = array();
        $result = $this->stub->objectToArray($orderLTLPolicy);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $orderLTLPolicy = MockObjectGenerate::generateOrderLTLPolicy(1);

        $result = $this->stub->objectToArray($orderLTLPolicy);
        $this->compareRestfulTranslatorEquals($orderLTLPolicy, $result);
    }
}
