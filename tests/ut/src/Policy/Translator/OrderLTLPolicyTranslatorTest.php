<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\OrderLTLPolicyTranslatorUtilsTrait;

class OrderLTLPolicyTranslatorTest extends TestCase
{
    use OrderLTLPolicyTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderLTLPolicyTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new OrderLTLPolicyTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullOrderLTLPolicy',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $orderLTLPolicy = array();
        $result = $this->stub->objectToArray($orderLTLPolicy);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(OrderLTLPolicyTranslatorMock::class)
                           ->setMethods([
                               'statusFormatConversion'
                            ])->getMock();

        $orderLTLPolicy = MockObjectGenerate::generateOrderLTLPolicy(1);
        $status = $orderLTLPolicy->getStatus();
        $statusFormatConversion = array('statusFormatConversion');
        
        $stub->expects($this->exactly(1))->method(
            'statusFormatConversion'
        )->with($status)->willReturn($statusFormatConversion);

        $result = $stub->objectToArray($orderLTLPolicy);

        $this->assertEquals($result['status'], $statusFormatConversion);

        $this->compareTranslatorEquals($result, $orderLTLPolicy);
    }
}
