<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\OrderFTLPolicyTranslatorUtilsTrait;

class OrderFTLPolicyTranslatorTest extends TestCase
{
    use OrderFTLPolicyTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderFTLPolicyTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new OrderFTLPolicyTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullOrderFTLPolicy',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $orderFTLPolicy = array();
        $result = $this->stub->objectToArray($orderFTLPolicy);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(OrderFTLPolicyTranslatorMock::class)
                           ->setMethods([
                               'statusFormatConversion'
                            ])->getMock();

        $orderFTLPolicy = MockObjectGenerate::generateOrderFTLPolicy(1);
        $status = $orderFTLPolicy->getStatus();
        $statusFormatConversion = array('statusFormatConversion');
        
        $stub->expects($this->exactly(1))->method(
            'statusFormatConversion'
        )->with($status)->willReturn($statusFormatConversion);

        $result = $stub->objectToArray($orderFTLPolicy);

        $this->assertEquals($result['status'], $statusFormatConversion);

        $this->compareTranslatorEquals($result, $orderFTLPolicy);
    }
}
