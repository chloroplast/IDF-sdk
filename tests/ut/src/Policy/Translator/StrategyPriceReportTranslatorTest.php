<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\StrategyPriceReportTranslatorUtilsTrait;

class StrategyPriceReportTranslatorTest extends TestCase
{
    use StrategyPriceReportTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new StrategyPriceReportTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new StrategyPriceReportTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullStrategyPriceReport',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $strategyPriceReport = array();
        $result = $this->stub->objectToArray($strategyPriceReport);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $strategyPriceReport = MockObjectGenerate::generateStrategyPriceReport();
        $result = $this->stub->objectToArray($strategyPriceReport);

        $this->compareTranslatorEquals($result, $strategyPriceReport);
    }
}
