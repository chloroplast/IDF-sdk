<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\AmazonFTLPolicyTranslatorUtilsTrait;

class AmazonFTLPolicyTranslatorTest extends TestCase
{
    use AmazonFTLPolicyTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonFTLPolicyTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new AmazonFTLPolicyTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullAmazonFTLPolicy',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $amazonFTLPolicy = array();
        $result = $this->stub->objectToArray($amazonFTLPolicy);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(AmazonFTLPolicyTranslatorMock::class)
                           ->setMethods([
                               'statusFormatConversion'
                            ])->getMock();

        $amazonFTLPolicy = MockObjectGenerate::generateAmazonFTLPolicy(1);
        $status = $amazonFTLPolicy->getStatus();
        $statusFormatConversion = array('statusFormatConversion');
        
        $stub->expects($this->exactly(1))->method(
            'statusFormatConversion'
        )->with($status)->willReturn($statusFormatConversion);

        $result = $stub->objectToArray($amazonFTLPolicy);

        $this->assertEquals($result['status'], $statusFormatConversion);

        $this->compareTranslatorEquals($result, $amazonFTLPolicy);
    }
}
