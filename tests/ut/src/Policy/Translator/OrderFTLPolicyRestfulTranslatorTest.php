<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\OrderFTLPolicyTranslatorUtilsTrait;

class OrderFTLPolicyRestfulTranslatorTest extends TestCase
{
    use OrderFTLPolicyTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderFTLPolicyRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullOrderFTLPolicy',
            $result
        );
    }

    public function testArrayToObject()
    {
        $orderFTLPolicy = MockObjectGenerate::generateOrderFTLPolicy(1);

        $expression['data']['id'] = $orderFTLPolicy->getId();
        $expression['data']['attributes']['name'] = $orderFTLPolicy->getName();
        $expression['data']['attributes']['description'] = $orderFTLPolicy->getDescription();
        $expression['data']['attributes']['content'] = $orderFTLPolicy->getContent();
        $expression['data']['attributes']['status'] = $orderFTLPolicy->getStatus();
        $expression['data']['attributes']['statusTime'] = $orderFTLPolicy->getStatusTime();
        $expression['data']['attributes']['createTime'] = $orderFTLPolicy->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $orderFTLPolicy->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\OrderFTLPolicy',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $orderFTLPolicy = array();
        $result = $this->stub->objectToArray($orderFTLPolicy);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $orderFTLPolicy = MockObjectGenerate::generateOrderFTLPolicy(1);

        $result = $this->stub->objectToArray($orderFTLPolicy);
        $this->compareRestfulTranslatorEquals($orderFTLPolicy, $result);
    }
}
