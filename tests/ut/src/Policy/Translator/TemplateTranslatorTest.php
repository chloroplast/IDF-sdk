<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\TemplateTranslatorUtilsTrait;

class TemplateTranslatorTest extends TestCase
{
    use TemplateTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new TemplateTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new TemplateTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullTemplate',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $template = array();
        $result = $this->stub->objectToArray($template);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $template = MockObjectGenerate::generateTemplate();
        $result = $this->stub->objectToArray($template);

        $this->compareTranslatorEquals($result, $template);
    }
}
