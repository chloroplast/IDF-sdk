<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\AmazonFTLPolicyTranslatorUtilsTrait;

class AmazonFTLPolicyRestfulTranslatorTest extends TestCase
{
    use AmazonFTLPolicyTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonFTLPolicyRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullAmazonFTLPolicy',
            $result
        );
    }

    public function testArrayToObject()
    {
        $amazonFTLPolicy = MockObjectGenerate::generateAmazonFTLPolicy(1);

        $expression['data']['id'] = $amazonFTLPolicy->getId();
        $expression['data']['attributes']['name'] = $amazonFTLPolicy->getName();
        $expression['data']['attributes']['description'] = $amazonFTLPolicy->getDescription();
        $expression['data']['attributes']['content'] = $amazonFTLPolicy->getContent();
        $expression['data']['attributes']['status'] = $amazonFTLPolicy->getStatus();
        $expression['data']['attributes']['statusTime'] = $amazonFTLPolicy->getStatusTime();
        $expression['data']['attributes']['createTime'] = $amazonFTLPolicy->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $amazonFTLPolicy->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\AmazonFTLPolicy',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $amazonFTLPolicy = array();
        $result = $this->stub->objectToArray($amazonFTLPolicy);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $amazonFTLPolicy = MockObjectGenerate::generateAmazonFTLPolicy(1);

        $result = $this->stub->objectToArray($amazonFTLPolicy);
        $this->compareRestfulTranslatorEquals($amazonFTLPolicy, $result);
    }
}
