<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\StrategyPriceReportTranslatorUtilsTrait;

class StrategyPriceReportRestfulTranslatorTest extends TestCase
{
    use StrategyPriceReportTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new StrategyPriceReportRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullStrategyPriceReport',
            $result
        );
    }

    public function testArrayToObject()
    {
        $strategyPriceReport = MockObjectGenerate::generateStrategyPriceReport();
        $expression['data']['id'] = $strategyPriceReport->getId();
        $expression['data']['attributes']['price'] = $strategyPriceReport->getPrice();
        $expression['data']['attributes']['priceRemark'] = $strategyPriceReport->getPriceRemark();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\StrategyPriceReport',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $strategyPriceReport = array();
        $result = $this->stub->objectToArray($strategyPriceReport);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $strategyPriceReport = MockObjectGenerate::generateStrategyPriceReport(1);

        $result = $this->stub->objectToArray($strategyPriceReport);
        $this->compareRestfulTranslatorEquals($strategyPriceReport, $result);
    }
}
