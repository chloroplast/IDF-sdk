<?php
namespace Sdk\Policy\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Utils\MockObjectGenerate;
use Sdk\Policy\Utils\AmazonLTLPolicyTranslatorUtilsTrait;

use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;
use Sdk\Warehouse\Utils\MockObjectGenerate as WarehouseMockObjectGenerate;

class AmazonLTLPolicyRestfulTranslatorTest extends TestCase
{
    use AmazonLTLPolicyTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonLTLPolicyRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testGetWarehouseRestfulTranslator()
    {
        $stub = new AmazonLTLPolicyRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Warehouse\Translator\WarehouseRestfulTranslator',
            $stub->getWarehouseRestfulTranslatorPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullAmazonLTLPolicy',
            $result
        );
    }

    public function testArrayToObject()
    {
        $amazonLTLPolicy = MockObjectGenerate::generateAmazonLTLPolicy(1);

        $expression['data']['id'] = $amazonLTLPolicy->getId();
        $expression['data']['attributes']['name'] = $amazonLTLPolicy->getName();
        $expression['data']['attributes']['description'] = $amazonLTLPolicy->getDescription();
        $expression['data']['attributes']['content'] = $amazonLTLPolicy->getContent();
        $expression['data']['attributes']['status'] = $amazonLTLPolicy->getStatus();
        $expression['data']['attributes']['statusTime'] = $amazonLTLPolicy->getStatusTime();
        $expression['data']['attributes']['createTime'] = $amazonLTLPolicy->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $amazonLTLPolicy->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Policy\Model\AmazonLTLPolicy',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testArrayToObjectRelationships()
    {
        $stub = $this->getMockBuilder(AmazonLTLPolicyRestfulTranslatorMock::class)
                           ->setMethods([
                               'includedFormatConversion',
                               'relationshipFill',
                               'getWarehouseRestfulTranslator'
                            ])->getMock();

        $relationships = array(
            'selfOperatedWarehouse' => array('selfOperatedWarehouse')
        );
        $included = array('included');
        $expression['data']['relationships'] = $relationships;
        $expression['included'] = $included;

        $includedConversion = array('includedConversion');
        $selfOperatedWarehouseArray = array('selfOperatedWarehouseArray');
        $selfOperatedWarehouse = WarehouseMockObjectGenerate::generateWarehouse(1);

        $stub->expects($this->exactly(1))->method(
            'includedFormatConversion'
        )->with($included)->willReturn($includedConversion);

        $stub->expects($this->exactly(1))->method(
            'relationshipFill'
        )->with($relationships['selfOperatedWarehouse'], $includedConversion)->willReturn($selfOperatedWarehouseArray);

        // 为 WarehouseRestfulTranslator 类建立预言(prophecy)。
        $restfulTranslator = $this->prophesize(WarehouseRestfulTranslator::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $restfulTranslator->arrayToObject($selfOperatedWarehouseArray)->shouldBeCalled(1)->willReturn($selfOperatedWarehouse);
        // 为 getWarehouseRestfulTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $stub->expects($this->exactly(1))->method(
            'getWarehouseRestfulTranslator'
        )->willReturn($restfulTranslator->reveal());

        $result = $stub->arrayToObject($expression);
        $this->assertInstanceOf(
            'Sdk\Policy\Model\AmazonLTLPolicy',
            $result
        );

        $this->assertEquals($selfOperatedWarehouse, $result->getSelfOperatedWarehouse());
    }

    public function testObjectToArrayEmpty()
    {
        $amazonLTLPolicy = array();
        $result = $this->stub->objectToArray($amazonLTLPolicy);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $amazonLTLPolicy = MockObjectGenerate::generateAmazonLTLPolicy(1);

        $result = $this->stub->objectToArray($amazonLTLPolicy);
        $this->compareRestfulTranslatorEquals($amazonLTLPolicy, $result);
    }
}
