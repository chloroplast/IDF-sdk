<?php
namespace Sdk\Policy\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\StrategyPriceReport;
use Sdk\Policy\Adapter\AmazonLTLPolicy\IAmazonLTLPolicyAdapter;

class AmazonLTLPolicyRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(AmazonLTLPolicyRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIAmazonLTLPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\AmazonLTLPolicy\IAmazonLTLPolicyAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\AmazonLTLPolicy\AmazonLTLPolicyRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\AmazonLTLPolicy\AmazonLTLPolicyMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testGetTemplate()
    {
        $object = new Template();
        // 为 IAmazonLTLPolicyAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLPolicyAdapter::class);
        // 建立预期状况:getTemplate() 方法将会被调用一次。
        $adapter->getTemplate()->shouldBeCalled(1)->willReturn($object);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->getTemplate();

        $this->assertEquals($result, $object);
    }

    public function testCalculate()
    {
        $strategyPriceReport = new StrategyPriceReport(1);
        // 为 IAmazonLTLPolicyAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAmazonLTLPolicyAdapter::class);
        // 建立预期状况:calculate() 方法将会被调用一次。
        $adapter->calculate($strategyPriceReport)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->calculate($strategyPriceReport);

        $this->assertTrue($result);
    }
}
