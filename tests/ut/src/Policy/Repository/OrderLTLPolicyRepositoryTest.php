<?php
namespace Sdk\Policy\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Policy\Model\Template;
use Sdk\Policy\Adapter\OrderLTLPolicy\IOrderLTLPolicyAdapter;

class OrderLTLPolicyRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(OrderLTLPolicyRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIOrderLTLPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\OrderLTLPolicy\IOrderLTLPolicyAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\OrderLTLPolicy\OrderLTLPolicyRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\OrderLTLPolicy\OrderLTLPolicyMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testGetTemplate()
    {
        $object = new Template();
        // 为 IOrderLTLPolicyAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IOrderLTLPolicyAdapter::class);
        // 建立预期状况:getTemplate() 方法将会被调用一次。
        $adapter->getTemplate()->shouldBeCalled(1)->willReturn($object);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->getTemplate();

        $this->assertEquals($result, $object);
    }
}
