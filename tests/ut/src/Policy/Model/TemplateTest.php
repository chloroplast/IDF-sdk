<?php
namespace Sdk\Policy\Model;

use PHPUnit\Framework\TestCase;

class TemplateTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new Template();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    /**
     * Template 领域对象,测试构造函数
     */
    public function testTemplateConstructor()
    {
        $this->assertEmpty($this->stub->getContent());
    }

    //content 测试 -------------------------------------------------------- start
    /**
     * 设置 Template setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->stub->setContent(array('content'));
        $this->assertEquals(array('content'), $this->stub->getContent());
    }

    /**
     * 设置 Template setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->stub->setContent('content');
    }
    //content 测试 --------------------------------------------------------   end
}
