<?php
namespace Sdk\Policy\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullOrderFTLPolicyTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullOrderFTLPolicy::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsOrderFTLPolicy()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\OrderFTLPolicy',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullOrderFTLPolicyMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
