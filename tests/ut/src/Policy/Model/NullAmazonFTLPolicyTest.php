<?php
namespace Sdk\Policy\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullAmazonFTLPolicyTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullAmazonFTLPolicy::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsAmazonFTLPolicy()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\AmazonFTLPolicy',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullAmazonFTLPolicyMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
