<?php
namespace Sdk\Policy\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullTemplateTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullTemplate::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\Template',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullTemplateMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
