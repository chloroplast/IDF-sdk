<?php
namespace Sdk\Policy\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class OrderFTLPolicyTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderFTLPolicy();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }

    /**
     * OrderFTLPolicy 领域对象,测试构造函数
     */
    public function testOrderFTLPolicyConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getName());
        $this->assertEmpty($this->stub->getDescription());
        $this->assertEmpty($this->stub->getContent());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 OrderFTLPolicy setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 OrderFTLPolicy setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTLPolicy setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('name');
        $this->assertEquals('name', $this->stub->getName());
    }

    /**
     * 设置 OrderFTLPolicy setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array('name'));
    }
    //name 测试 --------------------------------------------------------   end

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTLPolicy setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->stub->setDescription('description');
        $this->assertEquals('description', $this->stub->getDescription());
    }

    /**
     * 设置 OrderFTLPolicy setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->stub->setDescription(array('description'));
    }
    //description 测试 --------------------------------------------------------   end

    //content 测试 -------------------------------------------------------- start
    /**
     * 设置 OrderFTLPolicy setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->stub->setContent(array('content'));
        $this->assertEquals(array('content'), $this->stub->getContent());
    }

    /**
     * 设置 OrderFTLPolicy setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->stub->setContent('content');
    }
    //content 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new OrderFTLPolicyMock();
        $this->assertInstanceOf(
            'Sdk\Policy\Repository\OrderFTLPolicyRepository',
            $stub->getRepositoryPublic()
        );
    }
}
