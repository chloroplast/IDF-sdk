<?php
namespace Sdk\Policy\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullStrategyPriceReportTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullStrategyPriceReport::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsStrategyPriceReport()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\StrategyPriceReport',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullStrategyPriceReportMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function initOperation($method)
    {
        $stub = $this->getMockBuilder(NullStrategyPriceReportMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
        $stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $stub->$method();
 
        $this->assertFalse($result);
    }

    public function testCalculate()
    {
        $this->initOperation('calculate');
    }
}
