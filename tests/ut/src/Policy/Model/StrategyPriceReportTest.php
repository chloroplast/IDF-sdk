<?php
namespace Sdk\Policy\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Warehouse\Model\Warehouse;
use Sdk\Policy\Repository\AmazonLTLPolicyRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class StrategyPriceReportTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(StrategyPriceReport::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    /**
     * StrategyPriceReport 领域对象,测试构造函数
     */
    public function testStrategyPriceReportConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertInstanceOf('Sdk\Warehouse\Model\Warehouse', $this->stub->getPickupWarehouse());
        $this->assertInstanceOf('Sdk\Warehouse\Model\Warehouse', $this->stub->getTargetWarehouse());
        $this->assertEmpty($this->stub->getPalletNumber());
        $this->assertEmpty($this->stub->getWeight());
        $this->assertEmpty($this->stub->getWidth());
        $this->assertEmpty($this->stub->getLength());
        $this->assertEmpty($this->stub->getHeight());
        $this->assertEmpty($this->stub->getIdfPickup());
        $this->assertEmpty($this->stub->getGrade());
        $this->assertEmpty($this->stub->getContent());
        $this->assertEmpty($this->stub->getPrice());
        $this->assertEmpty($this->stub->getPriceRemark());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 StrategyPriceReport setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //pickupWarehouse 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setPickupWarehouse() 正确的传参类型,期望传值正确
     */
    public function testSetPickupWarehouseCorrectType()
    {
        $pickupWarehouse = new Warehouse();
        $this->stub->setPickupWarehouse($pickupWarehouse);
        $this->assertEquals($pickupWarehouse, $this->stub->getPickupWarehouse());
    }

    /**
     * 设置 StrategyPriceReport setPickupWarehouse() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPickupWarehouseWrongType()
    {
        $this->stub->setPickupWarehouse(array('pickupWarehouse'));
    }
    //pickupWarehouse 测试 --------------------------------------------------------   end

    //targetWarehouse 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setTargetWarehouse() 正确的传参类型,期望传值正确
     */
    public function testSetTargetWarehouseCorrectType()
    {
        $targetWarehouse = new Warehouse();
        $this->stub->setTargetWarehouse($targetWarehouse);
        $this->assertEquals($targetWarehouse, $this->stub->getTargetWarehouse());
    }

    /**
     * 设置 StrategyPriceReport setTargetWarehouse() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetWarehouseWrongType()
    {
        $this->stub->setTargetWarehouse(array('targetWarehouse'));
    }
    //targetWarehouse 测试 --------------------------------------------------------   end

    //palletNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setPalletNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPalletNumberCorrectType()
    {
        $this->stub->setPalletNumber(1);
        $this->assertEquals(1, $this->stub->getPalletNumber());
    }

    /**
     * 设置 StrategyPriceReport setPalletNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPalletNumberWrongType()
    {
        $this->stub->setPalletNumber('palletNumber');
    }
    //palletNumber 测试 --------------------------------------------------------   end

    //weight 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setWeight() 正确的传参类型,期望传值正确
     */
    public function testSetWeightCorrectType()
    {
        $this->stub->setWeight(1);
        $this->assertEquals(1, $this->stub->getWeight());
    }

    /**
     * 设置 StrategyPriceReport setWeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightWrongType()
    {
        $this->stub->setWeight('weight');
    }
    //weight 测试 --------------------------------------------------------   end

    //width 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setWidth() 正确的传参类型,期望传值正确
     */
    public function testSetWidthCorrectType()
    {
        $this->stub->setWidth(1);
        $this->assertEquals(1, $this->stub->getWidth());
    }

    /**
     * 设置 StrategyPriceReport setWidth() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWidthWrongType()
    {
        $this->stub->setWidth('width');
    }
    //width 测试 --------------------------------------------------------   end

    //length 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setLength() 正确的传参类型,期望传值正确
     */
    public function testSetLengthCorrectType()
    {
        $this->stub->setLength(1);
        $this->assertEquals(1, $this->stub->getLength());
    }

    /**
     * 设置 StrategyPriceReport setLength() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetLengthWrongType()
    {
        $this->stub->setLength('length');
    }
    //length 测试 --------------------------------------------------------   end

    //height 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setHeight() 正确的传参类型,期望传值正确
     */
    public function testSetHeightCorrectType()
    {
        $this->stub->setHeight(1);
        $this->assertEquals(1, $this->stub->getHeight());
    }

    /**
     * 设置 StrategyPriceReport setHeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetHeightWrongType()
    {
        $this->stub->setHeight('height');
    }
    //height 测试 --------------------------------------------------------   end

    //idfPickup 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setIdfPickup() 正确的传参类型,期望传值正确
     */
    public function testSetIdfPickupCorrectType()
    {
        $this->stub->setIdfPickup(1);
        $this->assertEquals(1, $this->stub->getIdfPickup());
    }

    /**
     * 设置 StrategyPriceReport setIdfPickup() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdfPickupWrongType()
    {
        $this->stub->setIdfPickup('idfPickup');
    }
    //idfPickup 测试 --------------------------------------------------------   end

    //grade 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setGrade() 正确的传参类型,期望传值正确
     */
    public function testSetGradeCorrectType()
    {
        $this->stub->setGrade(1);
        $this->assertEquals(1, $this->stub->getGrade());
    }

    /**
     * 设置 StrategyPriceReport setGrade() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGradeWrongType()
    {
        $this->stub->setGrade('grade');
    }
    //grade 测试 --------------------------------------------------------   end

    //content 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->stub->setContent(array('content'));
        $this->assertEquals(array('content'), $this->stub->getContent());
    }

    /**
     * 设置 StrategyPriceReport setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->stub->setContent('content');
    }
    //content 测试 --------------------------------------------------------   end

    //price 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setPrice() 正确的传参类型,期望传值正确
     */
    public function testSetPriceCorrectType()
    {
        $this->stub->setPrice(1);
        $this->assertEquals(1, $this->stub->getPrice());
    }

    /**
     * 设置 StrategyPriceReport setPrice() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceWrongType()
    {
        $this->stub->setPrice('price');
    }
    //price 测试 --------------------------------------------------------   end

    //priceRemark 测试 -------------------------------------------------------- start
    /**
     * 设置 StrategyPriceReport setPriceRemark() 正确的传参类型,期望传值正确
     */
    public function testSetPriceRemarkCorrectType()
    {
        $this->stub->setPriceRemark(array('priceRemark'));
        $this->assertEquals(array('priceRemark'), $this->stub->getPriceRemark());
    }

    /**
     * 设置 StrategyPriceReport setPriceRemark() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPriceRemarkWrongType()
    {
        $this->stub->setPriceRemark('priceRemark');
    }
    //priceRemark 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new StrategyPriceReportMock();
        $this->assertInstanceOf(
            'Sdk\Policy\Repository\AmazonLTLPolicyRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testCalculate()
    {
        // 为 AmazonLTLPolicyRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLPolicyRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->calculate($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->calculate();

        $this->assertTrue($result);
    }
}
