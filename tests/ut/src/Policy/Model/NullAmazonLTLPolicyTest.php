<?php
namespace Sdk\Policy\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullAmazonLTLPolicyTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullAmazonLTLPolicy::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsAmazonLTLPolicy()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\AmazonLTLPolicy',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullAmazonLTLPolicyMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
