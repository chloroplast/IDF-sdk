<?php
namespace Sdk\Policy\Utils;

use Sdk\Policy\Model\Template;

trait TemplateTranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(Template $template, array $expression)
    {
        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['template'])) {
            $this->assertEquals($attributes['template'], $template->getContent());
        }
    }

    public function compareTranslatorEquals(array $expression, Template $template)
    {
        if (isset($expression['template'])) {
            $this->assertEquals($expression['template'], $template->getContent());
        }
    }
}
