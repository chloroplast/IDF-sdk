<?php
namespace Sdk\Policy\Utils;

use Sdk\Policy\Model\OrderFTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait OrderFTLPolicyTranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(OrderFTLPolicy $orderFTLPolicy, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $orderFTLPolicy->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $orderFTLPolicy->getName());
        }
        if (isset($attributes['description'])) {
            $this->assertEquals($attributes['description'], $orderFTLPolicy->getDescription());
        }
        if (isset($attributes['content'])) {
            $this->assertEquals($attributes['content'], $orderFTLPolicy->getContent());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $orderFTLPolicy->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $orderFTLPolicy->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $orderFTLPolicy->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $orderFTLPolicy->getUpdateTime());
        }
    }

    public function compareTranslatorEquals(array $expression, OrderFTLPolicy $orderFTLPolicy)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($orderFTLPolicy->getId()));
        }
        if (isset($expression['name'])) {
            $this->assertEquals($expression['name'], $orderFTLPolicy->getName());
        }
        if (isset($expression['description'])) {
            $this->assertEquals($expression['description'], $orderFTLPolicy->getDescription());
        }
        if (isset($expression['content'])) {
            $this->assertEquals($expression['content'], $orderFTLPolicy->getContent());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $orderFTLPolicy->getCreateTime());
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $orderFTLPolicy->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $orderFTLPolicy->getUpdateTime());
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $orderFTLPolicy->getUpdateTime())
            );
        }
    }
}
