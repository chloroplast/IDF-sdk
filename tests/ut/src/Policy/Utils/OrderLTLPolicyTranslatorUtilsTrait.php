<?php
namespace Sdk\Policy\Utils;

use Sdk\Policy\Model\OrderLTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait OrderLTLPolicyTranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(OrderLTLPolicy $orderLTLPolicy, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $orderLTLPolicy->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $orderLTLPolicy->getName());
        }
        if (isset($attributes['description'])) {
            $this->assertEquals($attributes['description'], $orderLTLPolicy->getDescription());
        }
        if (isset($attributes['content'])) {
            $this->assertEquals($attributes['content'], $orderLTLPolicy->getContent());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $orderLTLPolicy->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $orderLTLPolicy->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $orderLTLPolicy->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $orderLTLPolicy->getUpdateTime());
        }
    }

    public function compareTranslatorEquals(array $expression, OrderLTLPolicy $orderLTLPolicy)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($orderLTLPolicy->getId()));
        }
        if (isset($expression['name'])) {
            $this->assertEquals($expression['name'], $orderLTLPolicy->getName());
        }
        if (isset($expression['description'])) {
            $this->assertEquals($expression['description'], $orderLTLPolicy->getDescription());
        }
        if (isset($expression['content'])) {
            $this->assertEquals($expression['content'], $orderLTLPolicy->getContent());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $orderLTLPolicy->getCreateTime());
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $orderLTLPolicy->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $orderLTLPolicy->getUpdateTime());
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $orderLTLPolicy->getUpdateTime())
            );
        }
    }
}
