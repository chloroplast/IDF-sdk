<?php
namespace Sdk\Policy\Utils;

use Sdk\Policy\Model\AmazonLTLPolicy;
use Sdk\Policy\Model\AmazonFTLPolicy;
use Sdk\Policy\Model\OrderLTLPolicy;
use Sdk\Policy\Model\OrderFTLPolicy;
use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\StrategyPriceReport;

use Sdk\Warehouse\Utils\MockObjectGenerate as WarehouseMockObjectGenerate;

class MockObjectGenerate
{
    public static function generateAmazonLTLPolicy(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : AmazonLTLPolicy {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $amazonLTLPolicy = new AmazonLTLPolicy($id);
        $amazonLTLPolicy->setId($id);

        //name
        $name = isset($value['name']) ? $value['name'] : $faker->name();
        $amazonLTLPolicy->setName($name);
        
        //description
        $description = isset($value['description']) ? $value['description'] : $faker->title();
        $amazonLTLPolicy->setDescription($description);
        
        //content
        $content = isset($value['content']) ? $value['content'] : array($faker->name());
        $amazonLTLPolicy->setContent($content);

        //selfOperatedWarehouse
        $selfOperatedWarehouse = isset($value['selfOperatedWarehouse'])
        ? $value['selfOperatedWarehouse']
        : WarehouseMockObjectGenerate::generateWarehouse($faker->randomDigitNotNull());
        $amazonLTLPolicy->setSelfOperatedWarehouse($selfOperatedWarehouse);

        $amazonLTLPolicy->setStatus(0);
        $amazonLTLPolicy->setCreateTime($faker->unixTime());
        $amazonLTLPolicy->setUpdateTime($faker->unixTime());
        $amazonLTLPolicy->setStatusTime($faker->unixTime());

        return $amazonLTLPolicy;
    }

    public static function generateAmazonFTLPolicy(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : AmazonFTLPolicy {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $amazonFTLPolicy = new AmazonFTLPolicy($id);
        $amazonFTLPolicy->setId($id);

        //name
        $name = isset($value['name']) ? $value['name'] : $faker->name();
        $amazonFTLPolicy->setName($name);
        
        //description
        $description = isset($value['description']) ? $value['description'] : $faker->title();
        $amazonFTLPolicy->setDescription($description);
        
        //content
        $content = isset($value['content']) ? $value['content'] : array($faker->name());
        $amazonFTLPolicy->setContent($content);

        $amazonFTLPolicy->setStatus(0);
        $amazonFTLPolicy->setCreateTime($faker->unixTime());
        $amazonFTLPolicy->setUpdateTime($faker->unixTime());
        $amazonFTLPolicy->setStatusTime($faker->unixTime());

        return $amazonFTLPolicy;
    }

    public static function generateOrderLTLPolicy(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : OrderLTLPolicy {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $orderLTLPolicy = new OrderLTLPolicy($id);
        $orderLTLPolicy->setId($id);

        //name
        $name = isset($value['name']) ? $value['name'] : $faker->name();
        $orderLTLPolicy->setName($name);
        
        //description
        $description = isset($value['description']) ? $value['description'] : $faker->title();
        $orderLTLPolicy->setDescription($description);
        
        //content
        $content = isset($value['content']) ? $value['content'] : array($faker->name());
        $orderLTLPolicy->setContent($content);

        $orderLTLPolicy->setStatus(0);
        $orderLTLPolicy->setCreateTime($faker->unixTime());
        $orderLTLPolicy->setUpdateTime($faker->unixTime());
        $orderLTLPolicy->setStatusTime($faker->unixTime());

        return $orderLTLPolicy;
    }

    public static function generateOrderFTLPolicy(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : OrderFTLPolicy {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $orderFTLPolicy = new OrderFTLPolicy($id);
        $orderFTLPolicy->setId($id);

        //name
        $name = isset($value['name']) ? $value['name'] : $faker->name();
        $orderFTLPolicy->setName($name);
        
        //description
        $description = isset($value['description']) ? $value['description'] : $faker->title();
        $orderFTLPolicy->setDescription($description);
        
        //content
        $content = isset($value['content']) ? $value['content'] : array($faker->name());
        $orderFTLPolicy->setContent($content);

        $orderFTLPolicy->setStatus(0);
        $orderFTLPolicy->setCreateTime($faker->unixTime());
        $orderFTLPolicy->setUpdateTime($faker->unixTime());
        $orderFTLPolicy->setStatusTime($faker->unixTime());

        return $orderFTLPolicy;
    }

    public static function generateTemplate(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Template {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $template = new Template();
        
        //content
        $content = isset($value['content']) ? $value['content'] : array($faker->name());
        $template->setContent($content);

        return $template;
    }

    public static function generateStrategyPriceReport(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : StrategyPriceReport {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $strategyPriceReport = new StrategyPriceReport($id);
        $strategyPriceReport->setId($id);

        //pickupWarehouse
        $pickupWarehouse = isset($value['pickupWarehouse'])
        ? $value['pickupWarehouse']
        : WarehouseMockObjectGenerate::generateWarehouse($faker->randomDigitNotNull());
        $strategyPriceReport->setPickupWarehouse($pickupWarehouse);

        //targetWarehouse
        $targetWarehouse = isset($value['targetWarehouse'])
        ? $value['targetWarehouse']
        : WarehouseMockObjectGenerate::generateWarehouse($faker->randomDigitNotNull());
        $strategyPriceReport->setTargetWarehouse($targetWarehouse);
        
        //palletNumber
        $palletNumber = isset($value['palletNumber']) ? $value['palletNumber'] : $faker->randomDigitNotNull();
        $strategyPriceReport->setPalletNumber($palletNumber);

        //weight
        $weight = isset($value['weight']) ? $value['weight'] : $faker->randomDigitNotNull();
        $strategyPriceReport->setWeight($weight);
        
        //width
        $width = isset($value['width']) ? $value['width'] : $faker->randomDigitNotNull();
        $strategyPriceReport->setWidth($width);
        
        //length
        $length = isset($value['length']) ? $value['length'] : $faker->randomDigitNotNull();
        $strategyPriceReport->setLength($length);
        
        //height
        $height = isset($value['height']) ? $value['height'] : $faker->randomDigitNotNull();
        $strategyPriceReport->setHeight($height);
        
        //idfPickup
        $idfPickup = isset($value['idfPickup']) ? $value['idfPickup'] : $faker->randomDigitNotNull();
        $strategyPriceReport->setIdfPickup($idfPickup);
        
        //grade
        $grade = isset($value['grade']) ? $value['grade'] : $faker->randomDigitNotNull();
        $strategyPriceReport->setGrade($grade);

        //content
        $content = isset($value['content']) ? $value['content'] : array($faker->name());
        $strategyPriceReport->setContent($content);
        
        //price
        $price = isset($value['price']) ? $value['price'] : $faker->randomDigitNotNull();
        $strategyPriceReport->setPrice($price);

        //priceRemark
        $priceRemark = isset($value['priceRemark']) ? $value['priceRemark'] : array($faker->name());
        $strategyPriceReport->setPriceRemark($priceRemark);

        return $strategyPriceReport;
    }
}
