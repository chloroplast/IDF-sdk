<?php
namespace Sdk\Policy\Utils;

use Sdk\Policy\Model\AmazonLTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait AmazonLTLPolicyTranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(AmazonLTLPolicy $amazonLTLPolicy, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $amazonLTLPolicy->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $amazonLTLPolicy->getName());
        }
        if (isset($attributes['description'])) {
            $this->assertEquals($attributes['description'], $amazonLTLPolicy->getDescription());
        }
        if (isset($attributes['content'])) {
            $this->assertEquals($attributes['content'], $amazonLTLPolicy->getContent());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $amazonLTLPolicy->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $amazonLTLPolicy->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $amazonLTLPolicy->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $amazonLTLPolicy->getUpdateTime());
        }

        $relationships = isset($expression['data']['relationships']) ? $expression['data']['relationships'] : array();
        if (isset($relationships['selfOperatedWarehouse']['data'])) {
            $selfOperatedWarehouse = $relationships['selfOperatedWarehouse']['data'];
            $this->assertEquals($selfOperatedWarehouse['type'], 'warehouse');
            $this->assertEquals($selfOperatedWarehouse['id'], $amazonLTLPolicy->getSelfOperatedWarehouse()->getId());
        }
    }

    public function compareTranslatorEquals(array $expression, AmazonLTLPolicy $amazonLTLPolicy)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($amazonLTLPolicy->getId()));
        }
        if (isset($expression['name'])) {
            $this->assertEquals($expression['name'], $amazonLTLPolicy->getName());
        }
        if (isset($expression['description'])) {
            $this->assertEquals($expression['description'], $amazonLTLPolicy->getDescription());
        }
        if (isset($expression['content'])) {
            $this->assertEquals($expression['content'], $amazonLTLPolicy->getContent());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $amazonLTLPolicy->getCreateTime());
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $amazonLTLPolicy->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $amazonLTLPolicy->getUpdateTime());
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $amazonLTLPolicy->getUpdateTime())
            );
        }
    }
}
