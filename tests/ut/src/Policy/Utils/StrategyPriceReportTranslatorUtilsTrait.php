<?php
namespace Sdk\Policy\Utils;

use Sdk\Policy\Model\StrategyPriceReport;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait StrategyPriceReportTranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(StrategyPriceReport $strategyPriceReport, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $strategyPriceReport->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['price'])) {
            $this->assertEquals($attributes['price'], $strategyPriceReport->getPrice());
        }
        if (isset($attributes['priceRemark'])) {
            $this->assertEquals($attributes['priceRemark'], $strategyPriceReport->getPriceRemark());
        }

        if (isset($attributes['palletNumber'])) {
            $this->assertEquals($attributes['palletNumber'], $strategyPriceReport->getPalletNumber());
        }
        if (isset($attributes['weight'])) {
            $this->assertEquals($attributes['weight'], $strategyPriceReport->getWeight());
        }
        if (isset($attributes['width'])) {
            $this->assertEquals($attributes['width'], $strategyPriceReport->getWidth());
        }
        if (isset($attributes['length'])) {
            $this->assertEquals($attributes['length'], $strategyPriceReport->getLength());
        }
        if (isset($attributes['height'])) {
            $this->assertEquals($attributes['height'], $strategyPriceReport->getHeight());
        }
        if (isset($attributes['idfPickup'])) {
            $this->assertEquals($attributes['idfPickup'], $strategyPriceReport->getIdfPickup());
        }
        if (isset($attributes['grade'])) {
            $this->assertEquals($attributes['grade'], $strategyPriceReport->getGrade());
        }
        if (isset($attributes['content'])) {
            $this->assertEquals($attributes['content'], $strategyPriceReport->getContent());
        }
    }

    public function compareTranslatorEquals(array $expression, StrategyPriceReport $strategyPriceReport)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($strategyPriceReport->getId()));
        }
        if (isset($expression['price'])) {
            $this->assertEquals($expression['price'], $strategyPriceReport->getPrice());
        }
        if (isset($expression['priceRemark'])) {
            $this->assertEquals($expression['priceRemark'], $strategyPriceReport->getPriceRemark());
        }
    }
}
