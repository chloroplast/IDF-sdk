<?php
namespace Sdk\Policy\Utils;

use Sdk\Policy\Model\AmazonFTLPolicy;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait AmazonFTLPolicyTranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(AmazonFTLPolicy $amazonFTLPolicy, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $amazonFTLPolicy->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $amazonFTLPolicy->getName());
        }
        if (isset($attributes['description'])) {
            $this->assertEquals($attributes['description'], $amazonFTLPolicy->getDescription());
        }
        if (isset($attributes['content'])) {
            $this->assertEquals($attributes['content'], $amazonFTLPolicy->getContent());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $amazonFTLPolicy->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $amazonFTLPolicy->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $amazonFTLPolicy->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $amazonFTLPolicy->getUpdateTime());
        }
    }

    public function compareTranslatorEquals(array $expression, AmazonFTLPolicy $amazonFTLPolicy)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($amazonFTLPolicy->getId()));
        }
        if (isset($expression['name'])) {
            $this->assertEquals($expression['name'], $amazonFTLPolicy->getName());
        }
        if (isset($expression['description'])) {
            $this->assertEquals($expression['description'], $amazonFTLPolicy->getDescription());
        }
        if (isset($expression['content'])) {
            $this->assertEquals($expression['content'], $amazonFTLPolicy->getContent());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $amazonFTLPolicy->getCreateTime());
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $amazonFTLPolicy->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $amazonFTLPolicy->getUpdateTime());
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $amazonFTLPolicy->getUpdateTime())
            );
        }
    }
}
