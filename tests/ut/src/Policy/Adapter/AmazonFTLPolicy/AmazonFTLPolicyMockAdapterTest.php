<?php
namespace Sdk\Policy\Adapter\AmazonFTLPolicy;

use PHPUnit\Framework\TestCase;

class AmazonFTLPolicyMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonFTLPolicyMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIAmazonFTLPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\AmazonFTLPolicy\IAmazonFTLPolicyAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\AmazonFTLPolicy',
            $this->stub->fetchObject(1)
        );
    }

    public function testGetTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\Template',
            $this->stub->getTemplate()
        );
    }
}
