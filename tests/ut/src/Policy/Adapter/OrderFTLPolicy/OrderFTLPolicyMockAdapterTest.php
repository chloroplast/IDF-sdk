<?php
namespace Sdk\Policy\Adapter\OrderFTLPolicy;

use PHPUnit\Framework\TestCase;

class OrderFTLPolicyMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderFTLPolicyMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIOrderFTLPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\OrderFTLPolicy\IOrderFTLPolicyAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\OrderFTLPolicy',
            $this->stub->fetchObject(1)
        );
    }

    public function testGetTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\Template',
            $this->stub->getTemplate()
        );
    }
}
