<?php
namespace Sdk\Policy\Adapter\OrderFTLPolicy;

use PHPUnit\Framework\TestCase;
use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\NullTemplate;

use Prophecy\Argument;
use Sdk\Policy\Translator\TemplateRestfulTranslator;

class OrderFTLPolicyRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(OrderFTLPolicyRestfulAdapterMock::class)
                           ->setMethods([
                               'get',
                               'isSuccess',
                               'translateToTemplate',
                               'getNullTemplate',
                               'getTemplateRestfulTranslator',
                               'getContents'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIOrderFTLPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\OrderFTLPolicy\IOrderFTLPolicyAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullOrderFTLPolicy',
            $this->stub->getNullObjectPublic()
        );
    }

    public function testGetNullTemplate()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullTemplatePublic()
        );

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullTemplate',
            $this->stub->getNullTemplatePublic()
        );
    }

    public function testGetTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Translator\TemplateRestfulTranslator',
            $this->stub->getTemplateRestfulTranslatorPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'ORDERFTL_POLICY_LIST',
                OrderFTLPolicyRestfulAdapter::SCENARIOS['ORDERFTL_POLICY_LIST']
            ),
            array(
                'ORDERFTL_POLICY_FETCH_ONE',
                OrderFTLPolicyRestfulAdapter::SCENARIOS['ORDERFTL_POLICY_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(OrderFTLPolicyRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    public function testInsertTranslatorKeys()
    {
        $this->assertEquals(array(
            'name',
            'description',
            'content',
            'publisher'
        ), $this->stub->insertTranslatorKeysPublic());
    }

    public function testUpdateTranslatorKeys()
    {
        $this->assertEquals(array(
            'name',
            'description',
            'content',
            'publisher'
        ), $this->stub->updateTranslatorKeysPublic());
    }

    public function testEnableTranslatorKeys()
    {
        $this->assertEquals(array(
            'staff'
        ), $this->stub->enableTranslatorKeysPublic());
    }

    public function testDisableTranslatorKeys()
    {
        $this->assertEquals(array(
            'staff'
        ), $this->stub->disableTranslatorKeysPublic());
    }

    public function testDeletedTranslatorKeys()
    {
        $this->assertEquals(array(
            'staff'
        ), $this->stub->deletedTranslatorKeysPublic());
    }

    public function testTranslateToTemplate()
    {
        $expectedObject = 'object';
        $expectedContents = ['contents'];
        $expectedResult = 'result';
        $this->stub->expects($this->once())
                      ->method('getContents')
                      ->willReturn($expectedContents);

        $translator = $this->prophesize(TemplateRestfulTranslator::class);
        $translator->arrayToObject(Argument::exact($expectedContents))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($expectedResult);
        $this->stub->expects($this->once())
                      ->method('getTemplateRestfulTranslator')
                      ->willReturn($translator->reveal());

        $result = $this->stub->translateToTemplatePublic($expectedObject);
        $this->assertEquals($expectedResult, $result);
    }

    private function getTemplate(bool $result)
    {
        $id = 1;
        $resource = 'templates/price/orderFTL';
        
        $this->stub->expects($this->exactly(1))->method('get')->with($resource);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);
    }

    public function testGetTemplateTrue()
    {
        $id = 1;
        $object = new Template($id);
        $this->getTemplate(true);
        $this->stub->expects($this->exactly(1))->method('translateToTemplate')->willReturn($object);

        $result = $this->stub->getTemplate($id);

        $this->assertEquals($result, $object);
    }

    public function testGetTemplateFalse()
    {
        $id = 1;
        $nullObject = NullTemplate::getInstance();
        $this->getTemplate(false);
        $this->stub->expects($this->exactly(1))->method('getNullTemplate')->willReturn($nullObject);

        $result = $this->stub->getTemplate($id);

        $this->assertEquals($result, $nullObject);
    }
}
