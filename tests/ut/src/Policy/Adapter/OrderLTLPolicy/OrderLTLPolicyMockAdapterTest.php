<?php
namespace Sdk\Policy\Adapter\OrderLTLPolicy;

use PHPUnit\Framework\TestCase;

class OrderLTLPolicyMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new OrderLTLPolicyMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIOrderLTLPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\OrderLTLPolicy\IOrderLTLPolicyAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\OrderLTLPolicy',
            $this->stub->fetchObject(1)
        );
    }

    public function testGetTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\Template',
            $this->stub->getTemplate()
        );
    }
}
