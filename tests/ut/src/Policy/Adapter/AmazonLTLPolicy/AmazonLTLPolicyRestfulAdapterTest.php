<?php
namespace Sdk\Policy\Adapter\AmazonLTLPolicy;

use PHPUnit\Framework\TestCase;
use Sdk\Policy\Model\Template;
use Sdk\Policy\Model\NullTemplate;
use Sdk\Policy\Model\StrategyPriceReport;
use Sdk\Policy\Translator\StrategyPriceReportRestfulTranslator;

use Prophecy\Argument;
use Sdk\Policy\Translator\TemplateRestfulTranslator;

class AmazonLTLPolicyRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(AmazonLTLPolicyRestfulAdapterMock::class)
                           ->setMethods([
                               'get',
                               'isSuccess',
                               'translateToTemplate',
                               'getNullTemplate',
                               'post',
                               'getStrategyPriceReportRestfulTranslator',
                               'getResource',
                               'arrayToObject',
                               'getTemplateRestfulTranslator',
                               'getContents'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIAmazonLTLPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\AmazonLTLPolicy\IAmazonLTLPolicyAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullAmazonLTLPolicy',
            $this->stub->getNullObjectPublic()
        );
    }

    public function testGetNullTemplate()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullTemplatePublic()
        );

        $this->assertInstanceOf(
            'Sdk\Policy\Model\NullTemplate',
            $this->stub->getNullTemplatePublic()
        );
    }

    public function testGetTemplateRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Translator\TemplateRestfulTranslator',
            $this->stub->getTemplateRestfulTranslatorPublic()
        );
    }

    public function testGetStrategyPriceReportRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Translator\StrategyPriceReportRestfulTranslator',
            $this->stub->getStrategyPriceReportRestfulTranslatorPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'AMAZONLTL_POLICY_LIST',
                AmazonLTLPolicyRestfulAdapter::SCENARIOS['AMAZONLTL_POLICY_LIST']
            ),
            array(
                'AMAZONLTL_POLICY_FETCH_ONE',
                AmazonLTLPolicyRestfulAdapter::SCENARIOS['AMAZONLTL_POLICY_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(AmazonLTLPolicyRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    public function testInsertTranslatorKeys()
    {
        $this->assertEquals(array(
            'name',
            'description',
            'content',
            'publisher',
            'selfOperatedWarehouse'
        ), $this->stub->insertTranslatorKeysPublic());
    }

    public function testUpdateTranslatorKeys()
    {
        $this->assertEquals(array(
            'name',
            'description',
            'content',
            'publisher',
            'selfOperatedWarehouse'
        ), $this->stub->updateTranslatorKeysPublic());
    }

    public function testEnableTranslatorKeys()
    {
        $this->assertEquals(array(
            'staff'
        ), $this->stub->enableTranslatorKeysPublic());
    }

    public function testDisableTranslatorKeys()
    {
        $this->assertEquals(array(
            'staff'
        ), $this->stub->disableTranslatorKeysPublic());
    }

    public function testDeletedTranslatorKeys()
    {
        $this->assertEquals(array(
            'staff'
        ), $this->stub->deletedTranslatorKeysPublic());
    }

    public function testTranslateToTemplate()
    {
        $expectedObject = 'object';
        $expectedContents = ['contents'];
        $expectedResult = 'result';
        $this->stub->expects($this->once())
                      ->method('getContents')
                      ->willReturn($expectedContents);

        $translator = $this->prophesize(TemplateRestfulTranslator::class);
        $translator->arrayToObject(Argument::exact($expectedContents))
                   ->shouldBeCalledTimes(1)
                   ->willReturn($expectedResult);
        $this->stub->expects($this->once())
                      ->method('getTemplateRestfulTranslator')
                      ->willReturn($translator->reveal());

        $result = $this->stub->translateToTemplatePublic($expectedObject);
        $this->assertEquals($expectedResult, $result);
    }

    private function getTemplate(bool $result)
    {
        $id = 1;
        $resource = 'templates/price/amazonLTL';
        
        $this->stub->expects($this->exactly(1))->method('get')->with($resource);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);
    }

    public function testGetTemplateTrue()
    {
        $id = 1;
        $object = new Template($id);
        $this->getTemplate(true);
        $this->stub->expects($this->exactly(1))->method('translateToTemplate')->willReturn($object);

        $result = $this->stub->getTemplate($id);

        $this->assertEquals($result, $object);
    }

    public function testGetTemplateFalse()
    {
        $id = 1;
        $nullObject = NullTemplate::getInstance();
        $this->getTemplate(false);
        $this->stub->expects($this->exactly(1))->method('getNullTemplate')->willReturn($nullObject);

        $result = $this->stub->getTemplate($id);

        $this->assertEquals($result, $nullObject);
    }

    private function calculate(bool $result)
    {
        $resource = 'strategies/amazonLTL';
        $data = array();
        $keys = array(
            "pickupWarehouse",
            "targetWarehouse",
            "palletNumber",
            "weight",
            "width",
            "length",
            "height",
            "idfPickup",
            "grade",
            "content",
            "staff"

        );
        $strategyPriceReport = new StrategyPriceReport();

        // 为 StrategyPriceReportRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(StrategyPriceReportRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($strategyPriceReport, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getStrategyPriceReportRestfulTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getStrategyPriceReportRestfulTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('post')->with($resource.'/calculate', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        return $strategyPriceReport;
    }

    public function testCalculateFalse()
    {
        $strategyPriceReport = $this->calculate(false);

        $result = $this->stub->calculate($strategyPriceReport);

        $this->assertFalse($result);
    }
}
