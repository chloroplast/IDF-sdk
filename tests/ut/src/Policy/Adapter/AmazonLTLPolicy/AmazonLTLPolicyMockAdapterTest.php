<?php
namespace Sdk\Policy\Adapter\AmazonLTLPolicy;

use PHPUnit\Framework\TestCase;
use Sdk\Policy\Model\StrategyPriceReport;

class AmazonLTLPolicyMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AmazonLTLPolicyMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIAmazonLTLPolicyAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Adapter\AmazonLTLPolicy\IAmazonLTLPolicyAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\AmazonLTLPolicy',
            $this->stub->fetchObject(1)
        );
    }

    public function testGetTemplate()
    {
        $this->assertInstanceOf(
            'Sdk\Policy\Model\Template',
            $this->stub->getTemplate()
        );
    }

    public function testCalculate()
    {
        $strategyPriceReport = new StrategyPriceReport(1);
        $this->assertTrue($this->stub->calculate($strategyPriceReport));
    }
}
