<?php
namespace Sdk\Dictionary\Item\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ItemWidgetRuleTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ItemWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //name
    /**
     * @dataProvider additionProviderName
     */
    public function testName($parameter, $expected)
    {
        $result = $this->stub->name($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DICTIONARY_NAME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderName()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }
    
    //description
    /**
     * @dataProvider additionProviderDescription
     */
    public function testDescription($parameter, $expected)
    {
        $result = $this->stub->description($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(DICTIONARY_DESCRIPTION_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderDescription()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->realText(), true),
            array($faker->words(), false)
        );
    }
}
