<?php
namespace Sdk\Statistics\Record\Utils;

use Sdk\Statistics\Record\Model\Record;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait TranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(Record $record, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $record->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['category'])) {
            $this->assertEquals($attributes['category'], $record->getCategory());
        }
        if (isset($attributes['result'])) {
            $this->assertEquals($attributes['result'], $record->getResult());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $record->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $record->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $record->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $record->getUpdateTime());
        }
    }

    public function compareTranslatorEquals(array $expression, Record $record)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($record->getId()));
        }
        if (isset($expression['category'])) {
            $this->assertEquals($expression['category'], $record->getCategory());
        }
        if (isset($expression['result'])) {
            $this->assertEquals($expression['result'], $record->getResult());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $record->getCreateTime());
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $record->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $record->getUpdateTime());
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $record->getUpdateTime())
            );
        }
    }
}
