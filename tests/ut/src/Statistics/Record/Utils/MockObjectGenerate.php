<?php
namespace Sdk\Statistics\Record\Utils;

use Sdk\Statistics\Record\Model\Record;

class MockObjectGenerate
{
    public static function generateRecord(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Record {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $record = new Record($id);
        $record->setId($id);

        //category
        $category = isset($value['category']) ? $value['category'] : $faker->randomDigitNotNull();
        $record->setCategory($category);

        //result
        $result = isset($value['result']) ? $value['result'] : $faker->words();
        $record->setResult($result);

        $record->setStatus(0);
        $record->setCreateTime($faker->unixTime());
        $record->setUpdateTime($faker->unixTime());
        $record->setStatusTime($faker->unixTime());

        return $record;
    }
}
