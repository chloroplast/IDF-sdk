<?php
namespace Sdk\Statistics\Record\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Statistics\Record\Utils\MockObjectGenerate;
use Sdk\Statistics\Record\Utils\TranslatorUtilsTrait;

class RecordTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new RecordTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new RecordTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Statistics\Record\Model\NullRecord',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $item = array();
        $result = $this->stub->objectToArray($item);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $item = MockObjectGenerate::generateRecord(1);

        $result = $this->stub->objectToArray($item);

        $this->compareTranslatorEquals($result, $item);
    }
}
