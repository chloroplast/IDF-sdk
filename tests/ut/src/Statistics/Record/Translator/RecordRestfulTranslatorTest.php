<?php
namespace Sdk\Statistics\Record\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Statistics\Record\Utils\MockObjectGenerate;
use Sdk\Statistics\Record\Utils\TranslatorUtilsTrait;

class RecordRestfulTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new RecordRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Statistics\Record\Model\NullRecord',
            $result
        );
    }

    public function testArrayToObject()
    {
        $record = MockObjectGenerate::generateRecord(1);

        $expression['data']['id'] = $record->getId();
        $expression['data']['attributes']['category'] = $record->getCategory();
        $expression['data']['attributes']['result'] = $record->getResult();
        $expression['data']['attributes']['status'] = $record->getStatus();
        $expression['data']['attributes']['statusTime'] = $record->getStatusTime();
        $expression['data']['attributes']['createTime'] = $record->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $record->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Statistics\Record\Model\Record',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $record = array();
        $result = $this->stub->objectToArray($record);

        $this->assertEmpty($result);
    }
}
