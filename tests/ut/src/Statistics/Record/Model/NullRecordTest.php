<?php
namespace Sdk\Statistics\Record\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullRecordTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullRecord::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsRecord()
    {
        $this->assertInstanceOf(
            'Sdk\Statistics\Record\Model\Record',
            $this->stub
        );
    }
}
