<?php
namespace Sdk\Statistics\Record\Model;

use PHPUnit\Framework\TestCase;

use Sdk\Statistics\Category\Model\Category;

class RecordTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new Record();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * Record 领域对象,测试构造函数
     */
    public function testRecordConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getCategory());
        $this->assertEmpty($this->stub->getResult());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Record setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Record setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 Record setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $this->stub->setCategory(1);
        $this->assertEquals(1, $this->stub->getCategory());
    }

    /**
     * 设置 Record setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->stub->setCategory('category');
    }
    //category 测试 --------------------------------------------------------   end

    //result 测试 -------------------------------------------------------- start
    /**
     * 设置 Record setResult() 正确的传参类型,期望传值正确
     */
    public function testSetResultCorrectType()
    {
        $this->stub->setResult(array('result'));
        $this->assertEquals(array('result'), $this->stub->getResult());
    }

    /**
     * 设置 Record setResult() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetResultWrongType()
    {
        $this->stub->setResult('result');
    }
    //result 测试 --------------------------------------------------------   end
}
