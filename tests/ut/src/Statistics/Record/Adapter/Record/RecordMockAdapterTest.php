<?php
namespace Sdk\Statistics\Record\Adapter\Record;

use PHPUnit\Framework\TestCase;

class RecordMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new RecordMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRecordAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Statistics\Record\Adapter\Record\IRecordAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Statistics\Record\Model\Record',
            $this->stub->fetchObject(1)
        );
    }
}
