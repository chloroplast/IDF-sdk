<?php
namespace Sdk\Configuration\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Configuration\Utils\MockObjectGenerate;
use Sdk\Configuration\Utils\TranslatorUtilsTrait;
use Sdk\Configuration\Model\CustomList;

use Sdk\User\Staff\Translator\StaffTranslator;

class CustomListTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CustomListTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetStaffTranslator()
    {
        $stub = new CustomListTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffTranslator',
            $stub->getStaffTranslatorPublic()
        );
    }

    public function testGetNullObject()
    {
        $stub = new CustomListTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Configuration\Model\NullCustomList',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Configuration\Model\NullCustomList',
            $result
        );
    }

    public function testArrayToObject()
    {
        $customList = MockObjectGenerate::generateCustomList(1);
        
        $expression['id'] = marmot_encode($customList->getId());
        $expression['category'] = marmot_encode($customList->getCategory());
        $expression['identify'] = marmot_encode($customList->getIdentify());
        $expression['content'] = $customList->getContent();
        $expression['status']['id'] = marmot_encode($customList->getStatus());
        $expression['statusTime'] = $customList->getStatusTime();
        $expression['createTime'] = $customList->getCreateTime();
        $expression['updateTime'] = $customList->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Configuration\Model\CustomList',
            $result
        );

        $this->compareTranslatorEquals($expression, $result);
    }

    public function testObjectToArrayEmpty()
    {
        $customList = array();
        $result = $this->stub->objectToArray($customList);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(CustomListTranslatorMock::class)
                           ->setMethods([
                               'statusFormatConversion',
                               'getStaffTranslator'
                            ])->getMock();

        $customList = MockObjectGenerate::generateCustomList(1);
        $status = $customList->getStatus();
        $statusFormatConversion = array('statusFormatConversion');
        
        $stub->expects($this->exactly(1))->method(
            'statusFormatConversion'
        )->with($status)->willReturn($statusFormatConversion);

        $staffArray = $this->staffRelationObjectToArray($customList, $stub);

        $result = $stub->objectToArray($customList);

        $this->assertEquals($result['status'], $statusFormatConversion);
        $this->assertEquals($result['staff'], $staffArray);

        $this->compareTranslatorEquals($result, $customList);
    }

    private function staffRelationObjectToArray(CustomList $customList, $stub) : array
    {
        $staff = $customList->getStaff();
        $staffArray = array('staffArray');

        // 为 StaffTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(StaffTranslator::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $translator->objectToArray(
            $staff,
            ['id', 'name']
        )->shouldBeCalled(1)->willReturn($staffArray);
        // 为 getStaffTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $stub->expects($this->exactly(1))->method(
            'getStaffTranslator'
        )->willReturn($translator->reveal());

        return $staffArray;
    }
}
