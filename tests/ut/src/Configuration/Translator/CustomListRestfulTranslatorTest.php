<?php
namespace Sdk\Configuration\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Configuration\Utils\MockObjectGenerate;
use Sdk\Configuration\Utils\TranslatorUtilsTrait;

use Sdk\User\Staff\Utils\MockObjectGenerate as MockStaffGenerate;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

class CustomListRestfulTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CustomListRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testGetStaffRestfulTranslator()
    {
        $stub = new CustomListRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffRestfulTranslator',
            $stub->getStaffRestfulTranslatorPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Configuration\Model\NullCustomList',
            $result
        );
    }

    public function testArrayToObject()
    {
        $customList = MockObjectGenerate::generateCustomList(1);

        $expression['data']['id'] = $customList->getId();
        $expression['data']['attributes']['category'] = $customList->getCategory();
        $expression['data']['attributes']['identify'] = $customList->getIdentify();
        $expression['data']['attributes']['content'] = $customList->getContent();
        $expression['data']['attributes']['status'] = $customList->getStatus();
        $expression['data']['attributes']['statusTime'] = $customList->getStatusTime();
        $expression['data']['attributes']['createTime'] = $customList->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $customList->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Configuration\Model\CustomList',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testArrayToObjectRelationships()
    {
        $stub = $this->getMockBuilder(CustomListRestfulTranslatorMock::class)
                           ->setMethods([
                               'includedFormatConversion',
                               'relationshipFill',
                               'getStaffRestfulTranslator'
                            ])->getMock();

        $relationships = array(
            'staff' => array('staff')
        );
        $included = array('included');
        $expression['data']['relationships'] = $relationships;
        $expression['included'] = $included;

        $includedConversion = array('includedConversion');
        $staffArray = array('staffArray');
        $staff = MockStaffGenerate::generateStaff(1);
        $stub->expects($this->exactly(1))->method(
            'includedFormatConversion'
        )->with($included)->willReturn($includedConversion);

        $stub->expects($this->exactly(1))->method('relationshipFill')
            ->will($this->returnValueMap([[$relationships['staff'], $includedConversion, $staffArray]]));

        // 为 StaffRestfulTranslator 类建立预言(prophecy)。
        $staffRestfulTranslator = $this->prophesize(StaffRestfulTranslator::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $staffRestfulTranslator->arrayToObject($staffArray)->shouldBeCalled(1)->willReturn($staff);
        // 为 getStaffRestfulTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $stub->expects($this->exactly(1))->method(
            'getStaffRestfulTranslator'
        )->willReturn($staffRestfulTranslator->reveal());

        $result = $stub->arrayToObject($expression);
        $this->assertInstanceOf(
            'Sdk\Configuration\Model\CustomList',
            $result
        );

        $this->assertEquals($staff, $result->getStaff());
    }

    public function testObjectToArrayEmpty()
    {
        $customList = array();
        $result = $this->stub->objectToArray($customList);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $customList = MockObjectGenerate::generateCustomList(1);

        $result = $this->stub->objectToArray($customList);
        $this->compareRestfulTranslatorEquals($customList, $result);
    }
}
