<?php
namespace Sdk\Configuration\Utils;

use Sdk\Configuration\Model\CustomList;

/**
 * @todo
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait TranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(CustomList $customList, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $customList->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['category'])) {
            $this->assertEquals($attributes['category'], $customList->getCategory());
        }
        if (isset($attributes['identify'])) {
            $this->assertEquals($attributes['identify'], $customList->getIdentify());
        }
        if (isset($attributes['content'])) {
            $this->assertEquals($attributes['content'], $customList->getContent());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $customList->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $customList->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $customList->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $customList->getUpdateTime());
        }

        $relationships = isset($expression['data']['relationships']) ? $expression['data']['relationships'] : array();
        if (isset($relationships['staff']['data'])) {
            $staff = $relationships['staff']['data'];
            $this->assertEquals($staff['type'], 'staff');
            $this->assertEquals(intval($staff['id']), $customList->getStaff()->getId());
        }
    }

    public function compareTranslatorEquals(array $expression, CustomList $customList)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($customList->getId()));
        }
        if (isset($expression['category'])) {
            $this->assertEquals($expression['category'], marmot_encode($customList->getCategory()));
        }
        if (isset($expression['identify'])) {
            $this->assertEquals($expression['identify'], marmot_encode($customList->getIdentify()));
        }
        if (isset($expression['content'])) {
            $this->assertEquals($expression['content'], $customList->getContent());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $customList->getCreateTime());
        }
        if (isset($expression['createTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $customList->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $customList->getUpdateTime());
        }
        if (isset($expression['updateTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $customList->getUpdateTime())
            );
        }
    }
}
