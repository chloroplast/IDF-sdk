<?php
namespace Sdk\Configuration\Utils;

use Sdk\Configuration\Model\CustomList;

use Sdk\User\Staff\Utils\MockObjectGenerate as MockStaffGenerate;

class MockObjectGenerate
{
    public static function generateCustomList(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : CustomList {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $customList = new CustomList($id);
        $customList->setId($id);

        //category
        $category = isset($value['category']) ? $value['category'] : $faker->randomDigitNotNull();
        $customList->setCategory($category);
        
        //identify
        $identify = isset($value['identify']) ? $value['identify'] : $faker->randomDigitNotNull();
        $customList->setIdentify($identify);

        //content
        $content = isset($value['content']) ? $value['content'] : array($faker->name());
        $customList->setContent($content);

        //staff
        $staff = isset($value['staff']) ? $value['staff'] : MockStaffGenerate::generateStaff(0);
        $customList->setStaff($staff);
        
        $customList->setStatus(0);
        $customList->setCreateTime($faker->unixTime());
        $customList->setUpdateTime($faker->unixTime());
        $customList->setStatusTime($faker->unixTime());

        return $customList;
    }
}
