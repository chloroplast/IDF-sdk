<?php
namespace Sdk\Configuration\Adapter\CustomList;

use PHPUnit\Framework\TestCase;

use Sdk\Configuration\Model\CustomList;
use Sdk\Configuration\Model\NullCustomList;
use Sdk\Configuration\Translator\CustomListRestfulTranslator;

class CustomListRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(CustomListRestfulAdapterMock::class)
                           ->setMethods([
                               'getTranslator',
                               'patch',
                               'getResource',
                               'isSuccess',
                               'translateToObject'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsICustomListAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Configuration\Adapter\CustomList\ICustomListAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Configuration\Model\NullCustomList',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array(
                'CUSTOMLIST_LIST',
                CustomListRestfulAdapter::SCENARIOS['CUSTOMLIST_LIST']
            ),
            array(
                'CUSTOMLIST_FETCH_ONE',
                CustomListRestfulAdapter::SCENARIOS['CUSTOMLIST_FETCH_ONE']
            ),
            array(
                '',
                []
            )
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(CustomListRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    private function save(bool $result)
    {
        $resource = 'config/staff';
        $data = array();
        $keys = array(
            "category",
            "identify",
            "content",
            "staff"

        );
        $customList = new CustomList();

        // 为 CustomListRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(CustomListRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($customList, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource, $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);
        
        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($customList);
        }

        return $customList;
    }

    public function testSaveTrue()
    {
        $customList = $this->save(true);

        $result = $this->stub->save($customList);

        $this->assertTrue($result);
    }

    public function testSaveFalse()
    {
        $customList = $this->save(false);

        $result = $this->stub->save($customList);

        $this->assertFalse($result);
    }
}
