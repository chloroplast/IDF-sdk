<?php
namespace Sdk\Configuration\Adapter\CustomList;

use PHPUnit\Framework\TestCase;
use Sdk\Configuration\Model\CustomList;

class CustomListMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CustomListMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsICustomListAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Configuration\Adapter\CustomList\ICustomListAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Configuration\Model\CustomList',
            $this->stub->fetchObject(1)
        );
    }

    public function testSave()
    {
        $customList = new CustomList(1);
        $this->assertTrue($this->stub->save($customList));
    }
}
