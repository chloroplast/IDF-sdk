<?php
namespace Sdk\Configuration\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Configuration\Model\CustomList;
use Sdk\Configuration\Adapter\CustomList\ICustomListAdapter;

class CustomListRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(CustomListRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsICustomListAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Configuration\Adapter\CustomList\ICustomListAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Configuration\Adapter\CustomList\CustomListRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Configuration\Adapter\CustomList\CustomListMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testSave()
    {
        $customList = new CustomList(1);
        // 为 ICustomListAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(ICustomListAdapter::class);
        // 建立预期状况:save() 方法将会被调用一次。
        $adapter->save($customList)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->save($customList);

        $this->assertTrue($result);
    }
}
