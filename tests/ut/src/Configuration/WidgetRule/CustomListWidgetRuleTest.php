<?php
namespace Sdk\Configuration\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class CustomListWidgetRuleTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CustomListWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //identify
    /**
     * @dataProvider additionProviderIdentify
     */
    public function testIdentify($parameter, $expected)
    {
        $result = $this->stub->identify($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CUSTOMLIST_IDENTIFY_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderIdentify()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->title(), false),
            array($faker->randomNumber(), true),
            array($faker->shuffle([1,2,3,4]), false)
        );
    }

    //content
    /**
     * @dataProvider additionProviderContent
     */
    public function testContent($parameter, $expected)
    {
        $result = $this->stub->content($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CUSTOMLIST_CONTENT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderContent()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array($faker->title(), false),
            array($faker->randomNumber(), false),
            array($faker->shuffle([1,2,3,4]), true)
        );
    }
}
