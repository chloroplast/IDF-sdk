<?php
namespace Sdk\Configuration\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\User\Staff\Model\Staff;
use Sdk\Configuration\Repository\CustomListRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class CustomListTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(CustomList::class)
                           ->setMethods(['getRepository'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * CustomList 领域对象,测试构造函数
     */
    public function testCustomListConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEquals(CustomList::DEFAULT_CATEGORY, $this->stub->getCategory());
        $this->assertEmpty($this->stub->getIdentify());
        $this->assertEmpty($this->stub->getContent());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 CustomList setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 CustomList setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 CustomList setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $this->stub->setCategory(0);
        $this->assertEquals(0, $this->stub->getCategory());
    }

    /**
     * 设置 CustomList setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->stub->setCategory('category');
    }
    //category 测试 --------------------------------------------------------   end

    //identify 测试 -------------------------------------------------------- start
    /**
     * 设置 CustomList setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->stub->setIdentify(0);
        $this->assertEquals(0, $this->stub->getIdentify());
    }

    /**
     * 设置 CustomList setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->stub->setIdentify('identify');
    }
    //identify 测试 --------------------------------------------------------   end

    //content 测试 -------------------------------------------------------- start
    /**
     * 设置 CustomList setContent() 正确的传参类型,期望传值正确
     */
    public function testSetContentCorrectType()
    {
        $this->stub->setContent(array('content'));
        $this->assertEquals(array('content'), $this->stub->getContent());
    }

    /**
     * 设置 CustomList setContent() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetContentWrongType()
    {
        $this->stub->setContent('content');
    }
    //content 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 CustomList setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 CustomList setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 CustomList setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(0);
        $this->assertEquals(0, $this->stub->getStatus());
    }

    /**
     * 设置 CustomList setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new CustomListMock();
        $this->assertInstanceOf(
            'Sdk\Configuration\Repository\CustomListRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testSave()
    {
        // 为 CustomListRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(CustomListRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->save($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->save();

        $this->assertTrue($result);
    }
}
