<?php
namespace Sdk\Configuration\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullCustomListTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullCustomList::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsCustomList()
    {
        $this->assertInstanceOf(
            'Sdk\Configuration\Model\CustomList',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullCustomListMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function initOperation($method)
    {
        $stub = $this->getMockBuilder(NullCustomListMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
        $stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $stub->$method();
 
        $this->assertFalse($result);
    }

    public function testSave()
    {
        $this->initOperation('save');
    }
}
