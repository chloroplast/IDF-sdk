<?php
namespace Sdk\Configuration\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class CustomListPurviewTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CustomListPurview();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsPurview()
    {
        $this->assertInstanceOf(
            'Sdk\Role\Purview\Model\Purview',
            $this->stub
        );
    }
}
