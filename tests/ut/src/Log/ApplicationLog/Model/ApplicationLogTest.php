<?php
namespace Sdk\Log\ApplicationLog\Model;

use PHPUnit\Framework\TestCase;

class ApplicationLogTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ApplicationLog();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    /**
     * ApplicationLog 领域对象,测试构造函数
     */
    public function testApplicationLogConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getOperatorId());
        $this->assertEmpty($this->stub->getOperatorIdentify());
        $this->assertEmpty($this->stub->getOperatorCategory());
        $this->assertEmpty($this->stub->getTargetCategory());
        $this->assertEmpty($this->stub->getTargetAction());
        $this->assertEmpty($this->stub->getTargetId());
        $this->assertEmpty($this->stub->getTargetName());
        $this->assertEmpty($this->stub->getDescription());
        $this->assertEmpty($this->stub->getErrorId());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 ApplicationLog setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //operatorId 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setOperatorId() 正确的传参类型,期望传值正确
     */
    public function testSetOperatorIdCorrectType()
    {
        $this->stub->setOperatorId(1);
        $this->assertEquals(1, $this->stub->getOperatorId());
    }

    /**
     * 设置 ApplicationLog setOperatorId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperatorIdWrongType()
    {
        $this->stub->setOperatorId('operatorId');
    }
    //operatorId 测试 --------------------------------------------------------   end

    //operatorIdentify 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setOperatorIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetOperatorIdentifyCorrectType()
    {
        $this->stub->setOperatorIdentify('operatorIdentify');
        $this->assertEquals('operatorIdentify', $this->stub->getOperatorIdentify());
    }

    /**
     * 设置 ApplicationLog setOperatorIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperatorIdentifyWrongType()
    {
        $this->stub->setOperatorIdentify(array('operatorIdentify'));
    }
    //operatorIdentify 测试 --------------------------------------------------------   end

    //operatorCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setOperatorCategory() 正确的传参类型,期望传值正确
     */
    public function testSetOperatorCategoryCorrectType()
    {
        $this->stub->setOperatorCategory(1);
        $this->assertEquals(1, $this->stub->getOperatorCategory());
    }

    /**
     * 设置 ApplicationLog setOperatorCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOperatorCategoryWrongType()
    {
        $this->stub->setOperatorCategory('operatorCategory');
    }
    //operatorCategory 测试 --------------------------------------------------------   end

    //targetCategory 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setTargetCategory() 正确的传参类型,期望传值正确
     */
    public function testSetTargetCategoryCorrectType()
    {
        $this->stub->setTargetCategory(1);
        $this->assertEquals(1, $this->stub->getTargetCategory());
    }

    /**
     * 设置 ApplicationLog setTargetCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetCategoryWrongType()
    {
        $this->stub->setTargetCategory('targetCategory');
    }
    //targetCategory 测试 --------------------------------------------------------   end

    //targetAction 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setTargetAction() 正确的传参类型,期望传值正确
     */
    public function testSetTargetActionCorrectType()
    {
        $this->stub->setTargetAction(1);
        $this->assertEquals(1, $this->stub->getTargetAction());
    }

    /**
     * 设置 ApplicationLog setTargetAction() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetActionWrongType()
    {
        $this->stub->setTargetAction('targetAction');
    }
    //targetAction 测试 --------------------------------------------------------   end

    //targetId 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setTargetId() 正确的传参类型,期望传值正确
     */
    public function testSetTargetIdCorrectType()
    {
        $this->stub->setTargetId(1);
        $this->assertEquals(1, $this->stub->getTargetId());
    }

    /**
     * 设置 ApplicationLog setTargetId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetIdWrongType()
    {
        $this->stub->setTargetId('targetId');
    }
    //targetId 测试 --------------------------------------------------------   end

    //targetName 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setTargetName() 正确的传参类型,期望传值正确
     */
    public function testSetTargetNameCorrectType()
    {
        $this->stub->setTargetName('targetName');
        $this->assertEquals('targetName', $this->stub->getTargetName());
    }

    /**
     * 设置 ApplicationLog setTargetName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTargetNameWrongType()
    {
        $this->stub->setTargetName(array('targetName'));
    }
    //targetName 测试 --------------------------------------------------------   end

    //description 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setDescription() 正确的传参类型,期望传值正确
     */
    public function testSetDescriptionCorrectType()
    {
        $this->stub->setDescription('description');
        $this->assertEquals('description', $this->stub->getDescription());
    }

    /**
     * 设置 ApplicationLog setDescription() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetDescriptionWrongType()
    {
        $this->stub->setDescription(array('description'));
    }
    //description 测试 --------------------------------------------------------   end

    //errorId 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setErrorId() 正确的传参类型,期望传值正确
     */
    public function testSetErrorIdCorrectType()
    {
        $this->stub->setErrorId(1);
        $this->assertEquals(1, $this->stub->getErrorId());
    }

    /**
     * 设置 ApplicationLog setErrorId() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetErrorIdWrongType()
    {
        $this->stub->setErrorId('errorId');
    }
    //errorId 测试 --------------------------------------------------------   end

    //status 测试 -------------------------------------------------------- start
    /**
     * 设置 ApplicationLog setStatus() 正确的传参类型,期望传值正确
     */
    public function testSetStatusCorrectType()
    {
        $this->stub->setStatus(1);
        $this->assertEquals(1, $this->stub->getStatus());
    }

    /**
     * 设置 ApplicationLog setStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStatusWrongType()
    {
        $this->stub->setStatus('status');
    }
    //status 测试 --------------------------------------------------------   end
}
