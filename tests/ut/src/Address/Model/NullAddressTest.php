<?php
namespace Sdk\Address\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullAddressTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(NullAddressMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsAddress()
    {
        $this->assertInstanceOf(
            'Sdk\Address\Model\Address',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullAddressMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function testDefault()
    {
         // 为 resourceNotExist() 方法建立预期：该方法被调用一次且返回true。
        $this->stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $this->stub->default();

        $this->assertFalse($result);
    }
}
