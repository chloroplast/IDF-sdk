<?php
namespace Sdk\Address\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\User\Member\Model\Member;
use Sdk\Address\Repository\AddressRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class AddressTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(AddressMock::class)
                           ->setMethods(['getRepository', 'isEnableStatus'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * Address 领域对象,测试构造函数
     */
    public function testAddressConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getEnterpriseName());
        $this->assertEmpty($this->stub->getFirstAddress());
        $this->assertEmpty($this->stub->getSecondAddress());
        $this->assertEmpty($this->stub->getPhone());
        $this->assertEmpty($this->stub->getState());
        $this->assertEmpty($this->stub->getCity());
        $this->assertEmpty($this->stub->getPostalCode());
        $this->assertEmpty($this->stub->getIsDefault());
        $this->assertEmpty($this->stub->getCategory());
        $this->assertInstanceOf('Sdk\User\Member\Model\Member', $this->stub->getMember());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Address setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 Address setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //enterpriseName 测试 -------------------------------------------------------- start
    /**
     * 设置 Address setEnterpriseName() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseNameCorrectType()
    {
        $this->stub->setEnterpriseName('enterpriseName');
        $this->assertEquals('enterpriseName', $this->stub->getEnterpriseName());
    }

    /**
     * 设置 Address setEnterpriseName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseNameWrongType()
    {
        $this->stub->setEnterpriseName(array('enterpriseName'));
    }
    //enterpriseName 测试 --------------------------------------------------------   end

    //firstAddress 测试 -------------------------------------------------------- start
    /**
     * 设置 Address setFirstAddress() 正确的传参类型,期望传值正确
     */
    public function testSetFirstAddressCorrectType()
    {
        $this->stub->setFirstAddress('firstAddress');
        $this->assertEquals('firstAddress', $this->stub->getFirstAddress());
    }

    /**
     * 设置 Address setFirstAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFirstAddressWrongType()
    {
        $this->stub->setFirstAddress(array('firstAddress'));
    }
    //firstAddress 测试 --------------------------------------------------------   end

    //secondAddress 测试 -------------------------------------------------------- start
    /**
     * 设置 Address setSecondAddress() 正确的传参类型,期望传值正确
     */
    public function testSetSecondAddressCorrectType()
    {
        $this->stub->setSecondAddress('secondAddress');
        $this->assertEquals('secondAddress', $this->stub->getSecondAddress());
    }

    /**
     * 设置 Address setSecondAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSecondAddressWrongType()
    {
        $this->stub->setSecondAddress(array('secondAddress'));
    }
    //secondAddress 测试 --------------------------------------------------------   end

    //phone 测试 -------------------------------------------------------- start
    /**
     * 设置 Address setPhone() 正确的传参类型,期望传值正确
     */
    public function testSetPhoneCorrectType()
    {
        $this->stub->setPhone('phone');
        $this->assertEquals('phone', $this->stub->getPhone());
    }

    /**
     * 设置 Address setPhone() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPhoneWrongType()
    {
        $this->stub->setPhone(array('phone'));
    }
    //phone 测试 --------------------------------------------------------   end

    //state 测试 -------------------------------------------------------- start
    /**
     * 设置 Address setState() 正确的传参类型,期望传值正确
     */
    public function testSetStateCorrectType()
    {
        $this->stub->setState('state');
        $this->assertEquals('state', $this->stub->getState());
    }

    /**
     * 设置 Address setState() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStateWrongType()
    {
        $this->stub->setState(array('state'));
    }
    //state 测试 --------------------------------------------------------   end

    //city 测试 -------------------------------------------------------- start
    /**
     * 设置 Address setCity() 正确的传参类型,期望传值正确
     */
    public function testSetCityCorrectType()
    {
        $this->stub->setCity('city');
        $this->assertEquals('city', $this->stub->getCity());
    }

    /**
     * 设置 Address setCity() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCityWrongType()
    {
        $this->stub->setCity(array('city'));
    }
    //city 测试 --------------------------------------------------------   end

    //postalCode 测试 -------------------------------------------------------- start
    /**
     * 设置 Address setPostalCode() 正确的传参类型,期望传值正确
     */
    public function testSetPostalCodeCorrectType()
    {
        $this->stub->setPostalCode('postalCode');
        $this->assertEquals('postalCode', $this->stub->getPostalCode());
    }

    /**
     * 设置 Address setPostalCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPostalCodeWrongType()
    {
        $this->stub->setPostalCode(array('postalCode'));
    }
    //postalCode 测试 --------------------------------------------------------   end

    //isDefault 测试 -------------------------------------------------------- start
    /**
     * 设置 Address setIsDefault() 正确的传参类型,期望传值正确
     */
    public function testSetIsDefaultCorrectType()
    {
        $this->stub->setIsDefault(0);
        $this->assertEquals(0, $this->stub->getIsDefault());
    }

    /**
     * 设置 Address setIsDefault() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIsDefaultWrongType()
    {
        $this->stub->setIsDefault('isDefault');
    }
    //isDefault 测试 --------------------------------------------------------   end

    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 Address setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $this->stub->setCategory(0);
        $this->assertEquals(0, $this->stub->getCategory());
    }

    /**
     * 设置 Address setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->stub->setCategory('category');
    }
    //category 测试 --------------------------------------------------------   end
    
    //member 测试 -------------------------------------------------------- start
    /**
     * 设置 AmazonFTL setMember() 正确的传参类型,期望传值正确
     */
    public function testSetMemberCorrectType()
    {
        $member = new Member();
        $this->stub->setMember($member);
        $this->assertEquals($member, $this->stub->getMember());
    }

    /**
     * 设置 AmazonFTL setMember() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetMemberWrongType()
    {
        $this->stub->setMember(array('member'));
    }
    //member 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new AddressMock();
        $this->assertInstanceOf(
            'Sdk\Address\Repository\AddressRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testDefaultFalse()
    {
        $this->stub->expects($this->exactly(1))->method('isEnableStatus')->willReturn(false);

        $result = $this->stub->default();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testDefaultTrue()
    {
        $this->stub->expects($this->exactly(1))->method('isEnableStatus')->willReturn(true);

        // 为 AddressRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AddressRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->default($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->default();

        $this->assertTrue($result);
    }
}
