<?php
namespace Sdk\Address\Utils;

use Sdk\Address\Model\Address;

/**
 * @todo
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait TranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(Address $address, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $address->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['enterpriseName'])) {
            $this->assertEquals($attributes['enterpriseName'], $address->getEnterpriseName());
        }
        if (isset($attributes['firstAddress'])) {
            $this->assertEquals($attributes['firstAddress'], $address->getFirstAddress());
        }
        if (isset($attributes['secondAddress'])) {
            $this->assertEquals($attributes['secondAddress'], $address->getSecondAddress());
        }
        if (isset($attributes['phone'])) {
            $this->assertEquals($attributes['phone'], $address->getPhone());
        }
        if (isset($attributes['state'])) {
            $this->assertEquals($attributes['state'], $address->getState());
        }
        if (isset($attributes['city'])) {
            $this->assertEquals($attributes['city'], $address->getCity());
        }
        if (isset($attributes['postalCode'])) {
            $this->assertEquals($attributes['postalCode'], $address->getPostalCode());
        }
        if (isset($attributes['isDefault'])) {
            $this->assertEquals($attributes['isDefault'], $address->getIsDefault());
        }
        if (isset($attributes['category'])) {
            $this->assertEquals($attributes['category'], $address->getCategory());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $address->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $address->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $address->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $address->getUpdateTime());
        }
    }

    public function compareTranslatorEquals(array $expression, Address $address)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($address->getId()));
        }
        if (isset($expression['enterpriseName'])) {
            $this->assertEquals($expression['enterpriseName'], $address->getEnterpriseName());
        }
        if (isset($expression['firstAddress'])) {
            $this->assertEquals($expression['firstAddress'], $address->getFirstAddress());
        }
        if (isset($expression['secondAddress'])) {
            $this->assertEquals($expression['secondAddress'], $address->getSecondAddress());
        }
        if (isset($expression['phone'])) {
            $this->assertEquals($expression['phone'], $address->getPhone());
        }
        if (isset($expression['state'])) {
            $this->assertEquals($expression['state'], $address->getState());
        }
        if (isset($expression['city'])) {
            $this->assertEquals($expression['city'], $address->getCity());
        }
        if (isset($expression['postalCode'])) {
            $this->assertEquals($expression['postalCode'], $address->getPostalCode());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $address->getCreateTime());
        }
        if (isset($expression['createTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $address->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $address->getUpdateTime());
        }
        if (isset($expression['updateTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $address->getUpdateTime())
            );
        }
    }
}
