<?php
namespace Sdk\Address\Utils;

use Sdk\Address\Model\Address;

class MockObjectGenerate
{
    public static function generateAddress(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Address {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $address = new Address($id);
        $address->setId($id);

        //enterpriseName
        $enterpriseName = isset($value['enterpriseName']) ? $value['enterpriseName'] : $faker->name();
        $address->setEnterpriseName($enterpriseName);

        //firstAddress
        $firstAddress = isset($value['firstAddress']) ? $value['firstAddress'] : $faker->address();
        $address->setFirstAddress($firstAddress);
        
        //secondAddress
        $secondAddress = isset($value['secondAddress']) ? $value['secondAddress'] : $faker->address();
        $address->setSecondAddress($secondAddress);
        
        //phone
        $phone = isset($value['phone']) ? $value['phone'] : $faker->phoneNumber();
        $address->setPhone($phone);
        
        //state
        $state = isset($value['state']) ? $value['state'] : $faker->name();
        $address->setState($state);
        
        //city
        $city = isset($value['city']) ? $value['city'] : $faker->name();
        $address->setCity($city);
        
        //postalCode
        $postalCode = isset($value['postalCode']) ? $value['postalCode'] : $faker->postcode();
        $address->setPostalCode($postalCode);

        //isDefault
        $address->setIsDefault(Address::IS_DEFAULT['YES']);

        //category
        $address->setCategory(Address::CATEGORY['SHIPPING']);

        $address->setStatus(0);
        $address->setCreateTime($faker->unixTime());
        $address->setUpdateTime($faker->unixTime());
        $address->setStatusTime($faker->unixTime());

        return $address;
    }
}
