<?php
namespace Sdk\Address\Adapter\Address;

use PHPUnit\Framework\TestCase;

use Sdk\Address\Model\AddressMock;
use Sdk\Address\Translator\AddressRestfulTranslator;

class AddressRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(AddressRestfulAdapterMock::class)
                           ->setMethods([
                               'getTranslator',
                               'patch',
                               'getResource',
                               'isSuccess',
                               'translateToObject'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIAddressAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Address\Adapter\Address\IAddressAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Address\Model\NullAddress',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array('ADDRESS_LIST', AddressRestfulAdapter::SCENARIOS['ADDRESS_LIST']),
            array('ADDRESS_FETCH_ONE', AddressRestfulAdapter::SCENARIOS['ADDRESS_FETCH_ONE']),
            array('', [])
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(AddressRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    public function testInsertTranslatorKeys()
    {
        $this->assertEquals(array(
            'enterpriseName',
            'firstAddress',
            'secondAddress',
            'phone',
            'state',
            'city',
            'postalCode',
            'isDefault',
            'category',
            'member'
        ), $this->stub->insertTranslatorKeysPublic());
    }

    public function testUpdateTranslatorKeys()
    {
        $this->assertEquals(array(
            'enterpriseName',
            'firstAddress',
            'secondAddress',
            'phone',
            'state',
            'city',
            'postalCode',
            'isDefault',
            'category',
            'member'
        ), $this->stub->updateTranslatorKeysPublic());
    }

    public function testEnableTranslatorKeys()
    {
        $this->assertEquals(array(
            'member'
        ), $this->stub->enableTranslatorKeysPublic());
    }

    public function testDisableTranslatorKeys()
    {
        $this->assertEquals(array(
            'member'
        ), $this->stub->disableTranslatorKeysPublic());
    }

    public function testDeletedTranslatorKeys()
    {
        $this->assertEquals(array(
            'member'
        ), $this->stub->deletedTranslatorKeysPublic());
    }

    private function default(bool $result)
    {
        $id = 1;
        $resource = 'addresses';
        $data = array();
        $keys = array('member');
        $address = new AddressMock($id);

        // 为 AddressRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(AddressRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($address, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource.'/'.$id.'/default', $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($address);
        }

        return $address;
    }

    public function testDefaultTrue()
    {
        $address = $this->default(true);

        $result = $this->stub->default($address);

        $this->assertTrue($result);
    }

    public function testDefaultFalse()
    {
        $address = $this->default(false);

        $result = $this->stub->default($address);

        $this->assertFalse($result);
    }
}
