<?php
namespace Sdk\Address\Adapter\Address;

use PHPUnit\Framework\TestCase;

use Sdk\Address\Model\Address;

class AddressMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AddressMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIAddressAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Address\Adapter\Address\IAddressAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Address\Model\Address',
            $this->stub->fetchObject(1)
        );
    }

    public function testDefault()
    {
        $address = new Address(1);

        $this->assertTrue($this->stub->default($address));
    }
}
