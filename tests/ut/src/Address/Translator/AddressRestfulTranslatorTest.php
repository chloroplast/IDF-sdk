<?php
namespace Sdk\Address\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Address\Utils\MockObjectGenerate;
use Sdk\Address\Utils\TranslatorUtilsTrait;

class AddressRestfulTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AddressRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Address\Model\NullAddress',
            $result
        );
    }

    public function testArrayToObject()
    {
        $address = MockObjectGenerate::generateAddress(1);

        $expression['data']['id'] = $address->getId();
        $expression['data']['attributes']['enterpriseName'] = $address->getEnterpriseName();
        $expression['data']['attributes']['firstAddress'] = $address->getFirstAddress();
        $expression['data']['attributes']['secondAddress'] = $address->getSecondAddress();
        $expression['data']['attributes']['phone'] = $address->getPhone();
        $expression['data']['attributes']['state'] = $address->getState();
        $expression['data']['attributes']['city'] = $address->getCity();
        $expression['data']['attributes']['postalCode'] = $address->getPostalCode();
        $expression['data']['attributes']['isDefault'] = $address->getIsDefault();
        $expression['data']['attributes']['category'] = $address->getCategory();
        $expression['data']['attributes']['status'] = $address->getStatus();
        $expression['data']['attributes']['statusTime'] = $address->getStatusTime();
        $expression['data']['attributes']['createTime'] = $address->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $address->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Address\Model\Address',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $address = array();
        $result = $this->stub->objectToArray($address);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $address = MockObjectGenerate::generateAddress(1);

        $result = $this->stub->objectToArray($address);
        $this->compareRestfulTranslatorEquals($address, $result);
    }
}
