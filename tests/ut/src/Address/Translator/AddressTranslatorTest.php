<?php
namespace Sdk\Address\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Address\Utils\MockObjectGenerate;
use Sdk\Address\Utils\TranslatorUtilsTrait;
use Sdk\Address\Model\Address;

class AddressTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new AddressTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new AddressTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Address\Model\NullAddress',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Address\Model\NullAddress',
            $result
        );
    }

    public function testArrayToObject()
    {
        $address = MockObjectGenerate::generateAddress(1);
        
        $expression['id'] = marmot_encode($address->getId());
        $expression['enterpriseName'] = $address->getEnterpriseName();
        $expression['firstAddress'] = $address->getFirstAddress();
        $expression['secondAddress'] = $address->getSecondAddress();
        $expression['phone'] = $address->getPhone();
        $expression['state'] = $address->getState();
        $expression['city'] = $address->getCity();
        $expression['postalCode'] = $address->getPostalCode();
        $expression['isDefault'] = $address->getIsDefault();
        $expression['category'] = $address->getCategory();
        $expression['status']['id'] = marmot_encode($address->getStatus());
        $expression['statusTime'] = $address->getStatusTime();
        $expression['createTime'] = $address->getCreateTime();
        $expression['updateTime'] = $address->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Address\Model\Address',
            $result
        );

        $this->compareTranslatorEquals($expression, $result);
    }

    public function testObjectToArrayEmpty()
    {
        $address = array();
        $result = $this->stub->objectToArray($address);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(AddressTranslatorMock::class)
                           ->setMethods([
                               'statusFormatConversion',
                               'typeFormatConversion'
                            ])->getMock();

        $address = MockObjectGenerate::generateAddress(1);
        $isDefault = $address->getIsDefault();
        $category = $address->getCategory();
        $status = $address->getStatus();
        $isDefaultArray = array('isDefault');
        $categoryArray = array('category');
        $statusArray = array('status');

        $stub->expects($this->exactly(2))->method('statusFormatConversion')
            ->will($this->returnValueMap([
                [$isDefault, Address::IS_DEFAULT_TYPE, Address::IS_DEFAULT_CN, $isDefaultArray],
                [$status, Address::STATUS_TYPE, Address::STATUS_CN, $statusArray]
            ]));

        $stub->expects($this->exactly(1))->method('typeFormatConversion')
            ->will($this->returnValueMap([
                [$category, Address::CATEGORY_CN, $categoryArray]
            ]));

        $result = $stub->objectToArray($address);

        $this->assertEquals($result['isDefault'], $isDefaultArray);
        $this->assertEquals($result['category'], $categoryArray);
        $this->assertEquals($result['status'], $statusArray);

        $this->compareTranslatorEquals($result, $address);
    }
}
