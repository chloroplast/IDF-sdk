<?php
namespace Sdk\Address\Repository;

use PHPUnit\Framework\TestCase;

use Sdk\Address\Model\Address;
use Sdk\Address\Adapter\Address\IAddressAdapter;

class AddressRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(AddressRepository::class)
                           ->setMethods(['getAdapter'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIAddressAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Address\Adapter\Address\IAddressAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Address\Adapter\Address\AddressRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Address\Adapter\Address\AddressMockAdapter',
            $this->stub->getMockAdapter()
        );
    }

    public function testDefault()
    {
        $address = new Address(1);
        // 为 IAddressAdapter 类建立预言(prophecy)。
        $adapter = $this->prophesize(IAddressAdapter::class);
        // 建立预期状况:default() 方法将会被调用一次。
        $adapter->default($address)->shouldBeCalled(1)->willReturn(true);
        // 为 getAdapter() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAdapter')->willReturn($adapter->reveal());

        $result = $this->stub->default($address);

        $this->assertTrue($result);
    }
}
