<?php
namespace Sdk\Application\ParseTask\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullParseTaskTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullParseTask::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsParseTask()
    {
        $this->assertInstanceOf(
            'Sdk\Application\ParseTask\Model\ParseTask',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullParseTaskMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function initOperation($method)
    {
        $stub = $this->getMockBuilder(NullParseTaskMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
        $stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $stub->$method();
 
        $this->assertFalse($result);
    }

    public function testParse()
    {
        $this->initOperation('parse');
    }
}
