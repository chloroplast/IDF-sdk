<?php
namespace Sdk\Application\ParseTask\Model;

use PHPUnit\Framework\TestCase;

use Sdk\Order\Repository\AmazonLTLRepository;
use Sdk\Order\Repository\AmazonFTLRepository;
use Sdk\Order\Repository\OrderLTLRepository;
use Sdk\Order\Repository\OrderFTLRepository;

class ParseTaskTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(ParseTask::class)
                           ->setMethods([
                                'getAmazonLTLRepository',
                                'getAmazonFTLRepository',
                                'getOrderLTLRepository',
                                'getOrderFTLRepository'
                            ])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    /**
     * ParseTask 领域对象,测试构造函数
     */
    public function testParseTaskConstructor()
    {
        $this->assertEmpty($this->stub->getFileName());
        $this->assertEmpty($this->stub->getItems());
    }

    //fileName 测试 -------------------------------------------------------- start
    /**
     * 设置 ParseTask setFileName() 正确的传参类型,期望传值正确
     */
    public function testSetFileNameCorrectType()
    {
        $this->stub->setFileName('fileName');
        $this->assertEquals('fileName', $this->stub->getFileName());
    }

    /**
     * 设置 ParseTask setFileName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFileNameWrongType()
    {
        $this->stub->setFileName(array('fileName'));
    }
    //fileName 测试 --------------------------------------------------------   end

    //items 测试 -------------------------------------------------------- start
    /**
     * 设置 ParseTask setItems() 正确的传参类型,期望传值正确
     */
    public function testSetItemsCorrectType()
    {
        $this->stub->setItems(array('items'));
        $this->assertEquals(array('items'), $this->stub->getItems());
    }

    /**
     * 设置 ParseTask setItems() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetItemsWrongType()
    {
        $this->stub->setItems('items');
    }
    //items 测试 --------------------------------------------------------   end
    
    public function testGetAmazonLTLRepository()
    {
        $stub = new ParseTaskMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\AmazonLTLRepository',
            $stub->getAmazonLTLRepositoryPublic()
        );
    }

    public function testGetAmazonFTLRepository()
    {
        $stub = new ParseTaskMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\AmazonFTLRepository',
            $stub->getAmazonFTLRepositoryPublic()
        );
    }
    
    public function testGetOrderLTLRepository()
    {
        $stub = new ParseTaskMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\OrderLTLRepository',
            $stub->getOrderLTLRepositoryPublic()
        );
    }
    
    public function testGetOrderFTLRepository()
    {
        $stub = new ParseTaskMock();
        $this->assertInstanceOf(
            'Sdk\Order\Repository\OrderFTLRepository',
            $stub->getOrderFTLRepositoryPublic()
        );
    }

    public function testParseAmazonLTL()
    {
        // 为 AmazonLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->parse($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAmazonLTLRepository')->willReturn($repository->reveal());

        $result = $this->stub->parseAmazonLTL();

        $this->assertTrue($result);
    }

    public function testParseAmazonFTL()
    {
        // 为 AmazonFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(AmazonFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->parse($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getAmazonFTLRepository')->willReturn($repository->reveal());

        $result = $this->stub->parseAmazonFTL();

        $this->assertTrue($result);
    }

    public function testParseOrderLTL()
    {
        // 为 OrderLTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderLTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->parse($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getOrderLTLRepository')->willReturn($repository->reveal());

        $result = $this->stub->parseOrderLTL();

        $this->assertTrue($result);
    }

    public function testParseOrderFTL()
    {
        // 为 OrderFTLRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(OrderFTLRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->parse($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getOrderFTLRepository')->willReturn($repository->reveal());

        $result = $this->stub->parseOrderFTL();

        $this->assertTrue($result);
    }
}
