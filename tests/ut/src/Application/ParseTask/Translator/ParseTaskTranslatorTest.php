<?php
namespace Sdk\Application\ParseTask\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Application\ParseTask\Utils\MockObjectGenerate;
use Sdk\Application\ParseTask\Utils\ParseTaskTranslatorUtilsTrait;

class ParseTaskTranslatorTest extends TestCase
{
    use ParseTaskTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ParseTaskTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new ParseTaskTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Application\ParseTask\Model\NullParseTask',
            $stub->getNullObjectPublic()
        );
    }

    public function testObjectToArrayEmpty()
    {
        $parseTask = array();
        $result = $this->stub->objectToArray($parseTask);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $parseTask = MockObjectGenerate::generateParseTask();
        $result = $this->stub->objectToArray($parseTask);

        $this->compareTranslatorEquals($result, $parseTask);
    }
}
