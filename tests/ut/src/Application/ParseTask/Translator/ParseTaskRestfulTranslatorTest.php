<?php
namespace Sdk\Application\ParseTask\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Application\ParseTask\Utils\MockObjectGenerate;
use Sdk\Application\ParseTask\Utils\ParseTaskTranslatorUtilsTrait;

class ParseTaskRestfulTranslatorTest extends TestCase
{
    use ParseTaskTranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ParseTaskRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Application\ParseTask\Model\NullParseTask',
            $result
        );
    }

    public function testArrayToObject()
    {
        $parseTask = MockObjectGenerate::generateParseTask();
        $expression['data']['attributes']['name'] = $parseTask->getFileName();
        $expression['data']['attributes']['items'] = $parseTask->getItems();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Application\ParseTask\Model\ParseTask',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $parseTask = array();
        $result = $this->stub->objectToArray($parseTask);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $parseTask = MockObjectGenerate::generateParseTask(1);

        $result = $this->stub->objectToArray($parseTask);
        $this->compareRestfulTranslatorEquals($parseTask, $result);
    }
}
