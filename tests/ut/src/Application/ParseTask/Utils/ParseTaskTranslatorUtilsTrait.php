<?php
namespace Sdk\Application\ParseTask\Utils;

use Sdk\Application\ParseTask\Model\ParseTask;

trait ParseTaskTranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(ParseTask $parseTask, array $expression)
    {
        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $parseTask->getFileName());
        }
        if (isset($attributes['items'])) {
            $this->assertEquals($attributes['items'], $parseTask->getItems());
        }
    }

    public function compareTranslatorEquals(array $expression, ParseTask $parseTask)
    {
        if (isset($expression['fileName'])) {
            $this->assertEquals($expression['fileName'], $parseTask->getFileName());
        }
        if (isset($expression['items'])) {
            $this->assertEquals($expression['items'], $parseTask->getItems());
        }
    }
}
