<?php
namespace Sdk\Application\ParseTask\Utils;

use Sdk\Application\ParseTask\Model\ParseTask;

class MockObjectGenerate
{
    public static function generateParseTask(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : ParseTask {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $parseTask = new ParseTask();
        
        //fileName
        $fileName = isset($value['fileName']) ? $value['fileName'] : $faker->name();
        $parseTask->setFileName($fileName);
        
        //items
        $items = isset($value['items']) ? $value['items'] : array($faker->name());
        $parseTask->setItems($items);

        return $parseTask;
    }
}
