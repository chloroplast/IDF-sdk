<?php
namespace Sdk\Application\ExportDataTask\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class ExportDataTaskPurviewTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ExportDataTaskPurview();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsPurview()
    {
        $this->assertInstanceOf(
            'Sdk\Role\Purview\Model\Purview',
            $this->stub
        );
    }
}
