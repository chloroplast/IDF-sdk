<?php
namespace Sdk\Application\ExportDataTask\Model;

use PHPUnit\Framework\TestCase;

use Sdk\User\Staff\Model\Staff;

class ExportDataTaskTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ExportDataTask();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * ExportDataTask 领域对象,测试构造函数
     */
    public function testExportDataTaskConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getCategory());
        $this->assertEmpty($this->stub->getName());
        $this->assertEmpty($this->stub->getUpdatedNum());
        $this->assertEmpty($this->stub->getTotal());
        $this->assertEmpty($this->stub->getOffset());
        $this->assertEmpty($this->stub->getSize());
        $this->assertEmpty($this->stub->getFilter());
        $this->assertEmpty($this->stub->getSort());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertEmpty($this->stub->getCode());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 ExportDataTask setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //category 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setCategory() 正确的传参类型,期望传值正确
     */
    public function testSetCategoryCorrectType()
    {
        $this->stub->setCategory(1);
        $this->assertEquals(1, $this->stub->getCategory());
    }

    /**
     * 设置 ExportDataTask setCategory() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCategoryWrongType()
    {
        $this->stub->setCategory('category');
    }
    //category 测试 --------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('name');
        $this->assertEquals('name', $this->stub->getName());
    }

    /**
     * 设置 ExportDataTask setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array('name'));
    }
    //name 测试 --------------------------------------------------------   end

    //total 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setTotal() 正确的传参类型,期望传值正确
     */
    public function testSetTotalCorrectType()
    {
        $this->stub->setTotal(1);
        $this->assertEquals(1, $this->stub->getTotal());
    }

    /**
     * 设置 ExportDataTask setTotal() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetTotalWrongType()
    {
        $this->stub->setTotal('total');
    }
    //total 测试 --------------------------------------------------------   end

    //updatedNum 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setUpdatedNum() 正确的传参类型,期望传值正确
     */
    public function testSetUpdatedNumCorrectType()
    {
        $this->stub->setUpdatedNum(1);
        $this->assertEquals(1, $this->stub->getUpdatedNum());
    }

    /**
     * 设置 ExportDataTask setUpdatedNum() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUpdatedNumWrongType()
    {
        $this->stub->setUpdatedNum('updatedNum');
    }
    //updatedNum 测试 --------------------------------------------------------   end

    //filter 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setFilter() 正确的传参类型,期望传值正确
     */
    public function testSetFilterCorrectType()
    {
        $this->stub->setFilter(array('filter'));
        $this->assertEquals(array('filter'), $this->stub->getFilter());
    }

    /**
     * 设置 ExportDataTask setFilter() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetFilterWrongType()
    {
        $this->stub->setFilter('filter');
    }
    //filter 测试 --------------------------------------------------------   end

    //sort 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setSort() 正确的传参类型,期望传值正确
     */
    public function testSetSortCorrectType()
    {
        $this->stub->setSort('sort');
        $this->assertEquals('sort', $this->stub->getSort());
    }

    /**
     * 设置 ExportDataTask setSort() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSortWrongType()
    {
        $this->stub->setSort(array('sort'));
    }
    //sort 测试 --------------------------------------------------------   end

    //size 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setSize() 正确的传参类型,期望传值正确
     */
    public function testSetSizeCorrectType()
    {
        $this->stub->setSize(1);
        $this->assertEquals(1, $this->stub->getSize());
    }

    /**
     * 设置 ExportDataTask setSize() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetSizeWrongType()
    {
        $this->stub->setSize('size');
    }
    //size 测试 --------------------------------------------------------   end

    //offset 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setOffset() 正确的传参类型,期望传值正确
     */
    public function testSetOffsetCorrectType()
    {
        $this->stub->setOffset(1);
        $this->assertEquals(1, $this->stub->getOffset());
    }

    /**
     * 设置 ExportDataTask setOffset() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOffsetWrongType()
    {
        $this->stub->setOffset('offset');
    }
    //offset 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setstaff() 正确的传参类型,期望传值正确
     */
    public function testSetstaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 ExportDataTask setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    //code 测试 -------------------------------------------------------- start
    /**
     * 设置 ExportDataTask setCode() 正确的传参类型,期望传值正确
     */
    public function testSetCodeCorrectType()
    {
        $this->stub->setCode(1);
        $this->assertEquals(1, $this->stub->getCode());
    }

    /**
     * 设置 ExportDataTask setCode() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetCodeWrongType()
    {
        $this->stub->setCode('code');
    }
    //code 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new ExportDataTaskMock();
        $this->assertInstanceOf(
            'Sdk\Application\ExportDataTask\Repository\ExportDataTaskRepository',
            $stub->getRepositoryPublic()
        );
    }
}
