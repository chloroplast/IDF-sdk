<?php
namespace Sdk\Application\ExportDataTask\Adapter\ExportDataTask;

use PHPUnit\Framework\TestCase;

use Sdk\Application\ExportDataTask\Model\ExportDataTask;
use Sdk\Application\ExportDataTask\Model\ExportDataTaskMock;
use Sdk\Application\ExportDataTask\Translator\ExportDataTaskRestfulTranslator;

class ExportDataTaskRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(ExportDataTaskRestfulAdapterMock::class)
                           ->setMethods([
                               'insertTranslatorKeys',
                               'getTranslator',
                               'post',
                               'isSuccess',
                               'translateToObject'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIExportDataTaskAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Application\ExportDataTask\Adapter\ExportDataTask\IExportDataTaskAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Application\ExportDataTask\Model\NullExportDataTask',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array('EXPORT_DATA_TASK_LIST', ExportDataTaskRestfulAdapter::SCENARIOS['EXPORT_DATA_TASK_LIST']),
            array('EXPORT_DATA_TASK_FETCH_ONE', ExportDataTaskRestfulAdapter::SCENARIOS['EXPORT_DATA_TASK_FETCH_ONE']),
            array('', [])
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(ExportDataTaskRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    public function testInsertTranslatorKeys()
    {
        $this->assertEquals(array(
            'filter',
            'sort',
            'offset',
            'size',
            'staff'
        ), $this->stub->insertTranslatorKeysPublic());
    }

    public function testUpdateTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->updateTranslatorKeysPublic());
    }

    public function testEnableTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->enableTranslatorKeysPublic());
    }

    public function testDisableTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->disableTranslatorKeysPublic());
    }

    public function testDeletedTranslatorKeys()
    {
        $this->assertEquals(array(), $this->stub->deletedTranslatorKeysPublic());
    }

    private function insert(bool $result)
    {
        $id = 1;
        $resource = '/logs/application/exportTasks';
        $data = $keys = array();
        $exportDataTask = new ExportDataTaskMock($id);
        $exportDataTask->setCategory(ExportDataTask::CATEGORY['APPLICATION_LOG']);
        
        $this->stub->expects($this->exactly(1))->method('insertTranslatorKeys')->willReturn($keys);

        // 为 ExportDataTaskRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(ExportDataTaskRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($exportDataTask, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('post')->with($resource, $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($exportDataTask);
        }

        return $exportDataTask;
    }

    public function testInsertTrue()
    {
        $exportDataTask = $this->insert(true);

        $result = $this->stub->insert($exportDataTask);

        $this->assertTrue($result);
    }

    public function testInsertFalse()
    {
        $exportDataTask = $this->insert(false);

        $result = $this->stub->insert($exportDataTask);

        $this->assertFalse($result);
    }
}
