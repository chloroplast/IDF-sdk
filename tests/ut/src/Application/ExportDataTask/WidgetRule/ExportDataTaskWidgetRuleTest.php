<?php
namespace Sdk\Application\ExportDataTask\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\Application\ExportDataTask\Model\ExportDataTask;

class ExportDataTaskWidgetRuleTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new ExportDataTaskWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //category
    /**
     * @dataProvider additionProviderCategory
     */
    public function testCategory($parameter, $expected)
    {
        $result = $this->stub->category($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(EXPORT_TASK_CATEGORY_NOT_EXISTS, Core::getLastError()->getId());
    }

    public function additionProviderCategory()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->title(), false),
            array($faker->randomElement(ExportDataTask::CATEGORY), true)
        );
    }
}
