<?php
namespace Sdk\User\Model;

use PHPUnit\Framework\TestCase;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class UserTest extends TestCase
{
    private $userStub;

    protected function setUp(): void
    {
        $this->userStub = new UserMock();
    }

    protected function tearDown(): void
    {
        unset($this->userStub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->userStub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->userStub
        );
    }

    /**
     * User 领域对象,测试构造函数
     */
    public function testUserConstructor()
    {
        $this->assertEmpty($this->userStub->getId());
        $this->assertEmpty($this->userStub->getName());
        $this->assertEmpty($this->userStub->getPhone());
        $this->assertEmpty($this->userStub->getEmail());
        $this->assertEmpty($this->userStub->getOldPassword());
        $this->assertEmpty($this->userStub->getPassword());
        $this->assertEquals(User::STATUS['ENABLED'], $this->userStub->getStatus());
        $this->assertEmpty($this->userStub->getCreateTime());
        $this->assertEmpty($this->userStub->getUpdateTime());
        $this->assertEmpty($this->userStub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 User setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->userStub->setId(4);
        $this->assertEquals(4, $this->userStub->getId());
    }

    /**
     * 设置 User setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->userStub->setId('1');
        $this->assertTrue(is_int($this->userStub->getId()));
        $this->assertEquals(1, $this->userStub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 User setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->userStub->setName('name');
        $this->assertEquals('name', $this->userStub->getName());
    }

    /**
     * 设置 User setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->userStub->setName(array('name'));
    }
    //name 测试 --------------------------------------------------------   end

    //phone 测试 -------------------------------------------------------- start
    /**
     * 设置 User setPhone() 正确的传参类型,期望传值正确
     */
    public function testSetPhoneCorrectType()
    {
        $this->userStub->setPhone('phone');
        $this->assertEquals('phone', $this->userStub->getPhone());
    }

    /**
     * 设置 User setPhone() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPhoneWrongType()
    {
        $this->userStub->setPhone(array('phone'));
    }
    //phone 测试 --------------------------------------------------------   end

    //email 测试 -------------------------------------------------------- start
    /**
     * 设置 User setEmail() 正确的传参类型,期望传值正确
     */
    public function testSetEmailCorrectType()
    {
        $this->userStub->setEmail('email');
        $this->assertEquals('email', $this->userStub->getEmail());
    }

    /**
     * 设置 User setEmail() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEmailWrongType()
    {
        $this->userStub->setEmail(array('email'));
    }
    //email 测试 --------------------------------------------------------   end

    //password 测试 -------------------------------------------------------- start
    /**
     * 设置 User setPassword() 正确的传参类型,期望传值正确
     */
    public function testSetPasswordCorrectType()
    {
        $this->userStub->setPassword('password');
        $this->assertEquals('password', $this->userStub->getPassword());
    }

    /**
     * 设置 User setPassword() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPasswordWrongType()
    {
        $this->userStub->setPassword(array('password'));
    }
    //password 测试 --------------------------------------------------------   end
    
    //oldPassword 测试 -------------------------------------------------------- start
    /**
     * 设置 User setOldPassword() 正确的传参类型,期望传值正确
     */
    public function testSetOldPasswordCorrectType()
    {
        $this->userStub->setOldPassword('oldPassword');
        $this->assertEquals('oldPassword', $this->userStub->getOldPassword());
    }

    /**
     * 设置 User setOldPassword() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetOldPasswordWrongType()
    {
        $this->userStub->setOldPassword(array('oldPassword'));
    }
    //oldPassword 测试 --------------------------------------------------------   end
}
