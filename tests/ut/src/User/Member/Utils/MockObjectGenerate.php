<?php
namespace Sdk\User\Member\Utils;

use Sdk\User\Member\Model\Member;

class MockObjectGenerate
{
    public static function generateMember(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Member {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $member = new Member($id);
        $member->setId($id);

        //email
        self::generateEmail($member, $value, $faker);
        //cellphone
        self::generatePhone($member, $value, $faker);
        //userName
        self::generateUserName($member, $value, $faker);
        //enterpriseName
        self::generateEnterpriseName($member, $value, $faker);
        //enterpriseAddress
        self::generateEnterpriseAddress($member, $value, $faker);
        //gradeIndex
        self::generateGradeIndex($member, $value, $faker);
        //gradeName
        self::generateGradeName($member, $value, $faker);
        //password
        self::generatePassword($member, $value, $faker);
        //activeStatus
        self::generateActiveStatus($member, $value, $faker);
        //status
        self::generateStatus($member, $value, $faker);
        $member->setCreateTime($faker->unixTime());
        $member->setUpdateTime($faker->unixTime());
        $member->setStatusTime($faker->unixTime());

        return $member;
    }

    private static function generateEmail(Member $member, array $value, $faker) :void
    {
        //email
        $email = isset($value['email']) ? $value['email'] : $faker->email();
        $member->setEmail($email);
    }

    private static function generatePhone(Member $member, array $value, $faker) :void
    {
        //cellphone
        $cellphone = isset($value['cellphone']) ? $value['cellphone'] : $faker->phoneNumber();
        $member->setPhone($cellphone);
    }

    private static function generateUserName(Member $member, array $value, $faker) :void
    {
        //userName
        $userName = isset($value['userName']) ? $value['userName'] : $faker->name();
        $member->setUserName($userName);
    }

    private static function generateEnterpriseName(Member $member, array $value, $faker) :void
    {
        //enterpriseName
        $enterpriseName = isset($value['enterpriseName']) ? $value['enterpriseName'] : $faker->name();
        $member->setEnterpriseName($enterpriseName);
    }

    private static function generateGradeIndex(Member $member, array $value, $faker) :void
    {
        //gradeIndex
        $gradeIndex = isset($value['gradeIndex']) ? $value['gradeIndex'] : $faker->randomDigitNotNull();
        $member->setGradeIndex($gradeIndex);
    }

    private static function generateGradeName(Member $member, array $value, $faker) :void
    {
        //gradeName
        $gradeName = isset($value['gradeName']) ? $value['gradeName'] : $faker->name();
        $member->setGradeName($gradeName);
    }

    private static function generateEnterpriseAddress(Member $member, array $value, $faker) :void
    {
        //enterpriseAddress
        $enterpriseAddress = isset($value['enterpriseAddress']) ? $value['enterpriseAddress'] : $faker->address();
        $member->setEnterpriseAddress($enterpriseAddress);
    }

    private static function generatePassword(Member $member, array $value, $faker) :void
    {
        //password
        $password = isset($value['password']) ? $value['password'] : $faker->bothify('###??@A??');
        $member->setPassword($password);
    }

    private static function generateActiveStatus(Member $member, $value, $faker) : void
    {
        //activeStatus
        $activeStatus = isset($value['activeStatus'])
        ? $value['activeStatus']
        : $faker->randomElement(Member::ACTIVE_STATUS);
        $member->setActiveStatus($activeStatus);
    }

    private static function generateStatus(Member $member, $value, $faker) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(Member::STATUS);

        $member->setStatus($status);
    }
}
