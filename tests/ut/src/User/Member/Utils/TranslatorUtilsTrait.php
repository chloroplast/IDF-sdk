<?php
namespace Sdk\User\Member\Utils;

use Sdk\User\Member\Model\Member;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait TranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(Member $member, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $member->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['email'])) {
            $this->assertEquals($attributes['email'], $member->getEmail());
        }
        if (isset($attributes['phone'])) {
            $this->assertEquals($attributes['phone'], $member->getPhone());
        }
        if (isset($attributes['userName'])) {
            $this->assertEquals($attributes['userName'], $member->getUserName());
        }
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $member->getUserName());
        }
        if (isset($attributes['enterpriseName'])) {
            $this->assertEquals($attributes['enterpriseName'], $member->getEnterpriseName());
        }
        if (isset($attributes['enterpriseAddress'])) {
            $this->assertEquals($attributes['enterpriseAddress'], $member->getEnterpriseAddress());
        }
        if (isset($attributes['password'])) {
            $this->assertEquals($attributes['password'], $member->getPassword());
        }
        if (isset($attributes['oldPassword'])) {
            $this->assertEquals($attributes['oldPassword'], $member->getOldPassword());
        }
        if (isset($attributes['gradeIndex'])) {
            $this->assertEquals($attributes['gradeIndex'], $member->getGradeIndex());
        }
        if (isset($attributes['gradeName'])) {
            $this->assertEquals($attributes['gradeName'], $member->getGradeName());
        }
        if (isset($attributes['activeStatus'])) {
            $this->assertEquals($attributes['activeStatus'], $member->getActiveStatus());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $member->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $member->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $member->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $member->getUpdateTime());
        }
    }

    public function compareTranslatorEquals(array $expression, Member $member)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($member->getId()));
        }
        if (isset($expression['email'])) {
            $this->assertEquals($expression['email'], $member->getEmail());
        }
        if (isset($expression['phone'])) {
            $this->assertEquals($expression['phone'], $member->getPhone());
        }
        if (isset($expression['userName'])) {
            $this->assertEquals($expression['userName'], $member->getUserName());
        }
        if (isset($expression['enterpriseName'])) {
            $this->assertEquals($expression['enterpriseName'], $member->getEnterpriseName());
        }
        if (isset($expression['enterpriseAddress'])) {
            $this->assertEquals($expression['enterpriseAddress'], $member->getEnterpriseAddress());
        }
        if (isset($expression['gradeIndex'])) {
            $this->assertEquals($expression['gradeIndex'], $member->getGradeIndex());
        }
        if (isset($expression['gradeName'])) {
            $this->assertEquals($expression['gradeName'], $member->getGradeName());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $member->getCreateTime());
        }
        if (isset($expression['createTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $member->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $member->getUpdateTime());
        }
        if (isset($expression['updateTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $member->getUpdateTime())
            );
        }
    }
}
