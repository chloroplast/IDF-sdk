<?php
namespace Sdk\User\Member\Model;

use PHPUnit\Framework\TestCase;

use Marmot\Core;

use Sdk\User\Member\Repository\MemberRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MemberTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(MemberMock::class)
                           ->setMethods(['getRepository', 'getMemberJwtAuth', 'isActiveStatus'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsUser()
    {
        $this->assertInstanceOf(
            'Sdk\User\Model\User',
            $this->stub
        );
    }

    /**
     * Member 领域对象,测试构造函数
     */
    public function testMemberConstructor()
    {
        $this->assertEmpty($this->stub->getIdentify());
        $this->assertEmpty($this->stub->getUserName());
        $this->assertEmpty($this->stub->getEnterpriseName());
        $this->assertEmpty($this->stub->getEnterpriseAddress());
        $this->assertEmpty($this->stub->getGradeIndex());
        $this->assertEmpty($this->stub->getGradeName());
        $this->assertEmpty($this->stub->getActiveStatus());
        $this->assertEmpty($this->stub->getIdentification());
    }

    //identify 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setIdentify() 正确的传参类型,期望传值正确
     */
    public function testSetIdentifyCorrectType()
    {
        $this->stub->setIdentify('identify');
        $this->assertEquals('identify', $this->stub->getIdentify());
    }

    /**
     * 设置 Member setIdentify() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentifyWrongType()
    {
        $this->stub->setIdentify(array('identify'));
    }
    //identify 测试 --------------------------------------------------------   end

    //userName 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setUserName() 正确的传参类型,期望传值正确
     */
    public function testSetUserNameCorrectType()
    {
        $this->stub->setUserName('userName');
        $this->assertEquals('userName', $this->stub->getUserName());
    }

    /**
     * 设置 Member setUserName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUserNameWrongType()
    {
        $this->stub->setUserName(array('userName'));
    }
    //userName 测试 --------------------------------------------------------   end

    //enterpriseName 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setEnterpriseName() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseNameCorrectType()
    {
        $this->stub->setEnterpriseName('enterpriseName');
        $this->assertEquals('enterpriseName', $this->stub->getEnterpriseName());
    }

    /**
     * 设置 Member setEnterpriseName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseNameWrongType()
    {
        $this->stub->setEnterpriseName(array('enterpriseName'));
    }
    //enterpriseName 测试 --------------------------------------------------------   end

    //enterpriseAddress 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setEnterpriseAddress() 正确的传参类型,期望传值正确
     */
    public function testSetEnterpriseAddressCorrectType()
    {
        $this->stub->setEnterpriseAddress('enterpriseAddress');
        $this->assertEquals('enterpriseAddress', $this->stub->getEnterpriseAddress());
    }

    /**
     * 设置 Member setEnterpriseAddress() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetEnterpriseAddressWrongType()
    {
        $this->stub->setEnterpriseAddress(array('enterpriseAddress'));
    }
    //enterpriseAddress 测试 --------------------------------------------------------   end

    //gradeIndex 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setGradeIndex() 正确的传参类型,期望传值正确
     */
    public function testSetGradeIndexCorrectType()
    {
        $this->stub->setGradeIndex(1);
        $this->assertEquals(1, $this->stub->getGradeIndex());
    }

    /**
     * 设置 Member setGradeIndex() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGradeIndexWrongType()
    {
        $this->stub->setGradeIndex(array('gradeIndex'));
    }
    //gradeIndex 测试 --------------------------------------------------------   end

    //gradeName 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setGradeName() 正确的传参类型,期望传值正确
     */
    public function testSetGradeNameCorrectType()
    {
        $this->stub->setGradeName('gradeName');
        $this->assertEquals('gradeName', $this->stub->getGradeName());
    }

    /**
     * 设置 Member setGradeName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetGradeNameWrongType()
    {
        $this->stub->setGradeName(array('gradeName'));
    }
    //gradeName 测试 --------------------------------------------------------   end

    //activeStatus 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setActiveStatus() 正确的传参类型,期望传值正确
     */
    public function testSetActiveStatusCorrectType()
    {
        $this->stub->setActiveStatus(1);
        $this->assertEquals(1, $this->stub->getActiveStatus());
    }

    /**
     * 设置 Member setActiveStatus() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetActiveStatusWrongType()
    {
        $this->stub->setActiveStatus(array('activeStatus'));
    }
    //activeStatus 测试 --------------------------------------------------------   end

    //identification 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setIdentification() 正确的传参类型,期望传值正确
     */
    public function testSetIdentificationCorrectType()
    {
        $this->stub->setIdentification('identification');
        $this->assertEquals('identification', $this->stub->getIdentification());
    }

    /**
     * 设置 Member setIdentification() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIdentificationWrongType()
    {
        $this->stub->setIdentification(array('identification'));
    }
    //identification 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new MemberMock();
        $this->assertInstanceOf(
            'Sdk\User\Member\Repository\MemberRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testGetMemberCookieAuth()
    {
        $stub = new MemberMock();
        $this->assertInstanceOf(
            'Sdk\User\Member\Model\MemberCookieAuth',
            $stub->getMemberCookieAuthPublic()
        );
    }

    public function testGetMemberJwtAuth()
    {
        $stub = new MemberMock();
        $this->assertInstanceOf(
            'Sdk\User\Member\Model\MemberJwtAuth',
            $stub->getMemberJwtAuthPublic()
        );
    }

    public function testLogin()
    {
        // 为 MemberRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(MemberRepository::class);
        // 建立预期状况:login() 方法将会被调用一次。
        $repository->login($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        // 为 MemberJwtAuth 类建立预言(prophecy)。
        $memberJwtAuth = $this->prophesize(MemberJwtAuth::class);
        // 建立预期状况:generateJwtAndSaveMemberToCache() 方法将会被调用一次。
        $memberJwtAuth->generateJwtAndSaveMemberToCache($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method(
            'getMemberJwtAuth'
        )->willReturn($memberJwtAuth->reveal());

        $result = $this->stub->login();

        $this->assertTrue($result);
    }

    public function testLoginGateway()
    {
        // 为 MemberRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(MemberRepository::class);
        // 建立预期状况:login() 方法将会被调用一次。
        $repository->login($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        // 为 MemberJwtAuth 类建立预言(prophecy)。
        $memberJwtAuth = $this->prophesize(MemberJwtAuth::class);
        // 建立预期状况:generateJwtAndSaveMemberToCache() 方法将会被调用一次。
        $memberJwtAuth->generateJwtAndSaveMemberToCache($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method(
            'getMemberJwtAuth'
        )->willReturn($memberJwtAuth->reveal());

        $result = $this->stub->loginGateway();

        $this->assertTrue($result);
    }

    public function testLogout()
    {
        // 为 MemberJwtAuth 类建立预言(prophecy)。
        $memberJwtAuth = $this->prophesize(MemberJwtAuth::class);
        // 建立预期状况:clearJwtAndMemberToCache() 方法将会被调用一次。
        $memberJwtAuth->clearJwtAndMemberToCache($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method(
            'getMemberJwtAuth'
        )->willReturn($memberJwtAuth->reveal());

        $result = $this->stub->logout();

        $this->assertTrue($result);
    }

    public function testLogoutGateway()
    {
        // 为 MemberJwtAuth 类建立预言(prophecy)。
        $memberJwtAuth = $this->prophesize(MemberJwtAuth::class);
        // 建立预期状况:clearJwtAndMemberToCache() 方法将会被调用一次。
        $memberJwtAuth->clearJwtAndMemberToCache($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method(
            'getMemberJwtAuth'
        )->willReturn($memberJwtAuth->reveal());

        $result = $this->stub->logoutGateway();

        $this->assertTrue($result);
    }

    protected function initOperation($method)
    {
        // 为 MemberRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(MemberRepository::class);
        // 建立预期状况:$method() 方法将会被调用一次。
        $repository->$method($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->$method();

        $this->assertTrue($result);
    }

    public function testResetPassword()
    {
        $this->initOperation('resetPassword');
    }

    public function testUpdatePassword()
    {
        $this->initOperation('updatePassword');
    }

    public function testValidatePassword()
    {
        // 为 MemberRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(MemberRepository::class);
        // 建立预期状况:$login() 方法将会被调用一次。
        $repository->login($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->validatePassword();

        $this->assertTrue($result);
    }

    public function testActiveFalse()
    {
        $this->stub->expects($this->exactly(1))->method('isActiveStatus')->willReturn(true);

        $result = $this->stub->active();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_CAN_NOT_MODIFY, Core::getLastError()->getId());
    }

    public function testActiveTrue()
    {
        $this->stub->expects($this->exactly(1))->method('isActiveStatus')->willReturn(false);

        // 为 MemberRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(MemberRepository::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $repository->active($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->active();

        $this->assertTrue($result);
    }

    public function testIsActiveStatus()
    {
        $stub = new Member();

        $stub->setActiveStatus(Member::ACTIVE_STATUS['YES']);

        $result = $stub->isActiveStatus();

        $this->assertTrue($result);
    }
}
