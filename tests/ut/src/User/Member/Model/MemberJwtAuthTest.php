<?php
namespace Sdk\User\Member\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

use Sdk\User\Member\Repository\MemberRepository;
use Sdk\User\Member\Translator\MemberTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MemberJwtAuthTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(MemberJwtAuthMock::class)
                           ->setMethods([
                               'getTranslator',
                               'save',
                               'fetchMember',
                               'get',
                               'getRepository',
                               'fetchJwt',
                               'verifyJwt',
                               'del'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsDataCacheQuery()
    {
        $this->assertInstanceOf(
            'Marmot\Framework\Query\DataCacheQuery',
            $this->stub
        );
    }

    /**
     * Member 领域对象,测试构造函数
     */
    public function testMemberConstructor()
    {
        $this->assertEmpty($this->stub->getUid());
        $this->assertEquals(Core::$container->get('jwt.iss'), $this->stub->getIss());
        $this->assertEquals(Core::$container->get('jwt.sub'), $this->stub->getSub());
        $this->assertEquals(Core::$container->get('jwt.aud'), $this->stub->getAud());
        $this->assertEquals(
            Core::$container->get('time') + MemberJwtAuth::JWT_EXPIRATION_TIME,
            $this->stub->getExp()
        );
        $this->assertEquals(Core::$container->get('time'), $this->stub->getNbf());
        $this->assertEquals(Core::$container->get('time'), $this->stub->getIat());
        $this->assertEquals(Core::$container->get('jwt.key'), $this->stub->getKey());
    }

    public function testGetIss()
    {
        $this->assertIsString($this->stub->getIss());
    }

    public function testGetSub()
    {
        $this->assertIsString($this->stub->getSub());
    }

    public function testGetAud()
    {
        $this->assertIsString($this->stub->getAud());
    }

    public function testGetExp()
    {
        $this->assertIsInt($this->stub->getExp());
    }

    public function testGetNbf()
    {
        $this->assertIsInt($this->stub->getNbf());
    }

    public function testGetIat()
    {
        $this->assertIsInt($this->stub->getIat());
    }

    //jti 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setJti() 正确的传参类型,期望传值正确
     */
    public function testSetJtiCorrectType()
    {
        $this->stub->setJti('jti');
        $this->assertEquals('jti', $this->stub->getJti());
    }

    /**
     * 设置 Member setJti() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetJtiWrongType()
    {
        $this->stub->setJti(array('jti'));
    }
    //jti 测试 --------------------------------------------------------   end

    public function testGetKey()
    {
        $this->assertIsString($this->stub->getKey());
    }

    //uid 测试 -------------------------------------------------------- start
    /**
     * 设置 Member setUid() 正确的传参类型,期望传值正确
     */
    public function testSetUidCorrectType()
    {
        $this->stub->setUid(1);
        $this->assertEquals(1, $this->stub->getUid());
    }

    /**
     * 设置 Member setUid() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetUidWrongType()
    {
        $this->stub->setUid(array('memberUid'));
    }
    //uid 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new MemberJwtAuthMock();
        $this->assertInstanceOf(
            'Sdk\User\Member\Repository\MemberRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testGetTranslator()
    {
        $stub = new MemberJwtAuthMock();
        $this->assertInstanceOf(
            'Sdk\User\Member\Translator\MemberTranslator',
            $stub->getTranslatorPublic()
        );
    }

    public function testGenerateJwtAndSaveMemberToCache()
    {
        $stub = $this->getMockBuilder(MemberJwtAuth::class)
                           ->setMethods(['generateJwt', 'saveMemberToCache', 'initMember'])
                           ->getMock();

        $member = new Member(1);

        $stub->expects($this->exactly(1))->method('generateJwt')->with($member)->willReturn(true);
        $stub->expects($this->exactly(1))->method('saveMemberToCache')->with($member)->willReturn(true);
        $stub->expects($this->exactly(1))->method('initMember')->with($member)->willReturn(true);

        $result = $stub->generateJwtAndSaveMemberToCache($member);

        $this->assertTrue($result);
    }

    public function testGenerateJwt()
    {
        $member = new Member(1);
        $jwt = 'jwt';

        $this->stub->expects($this->exactly(1))->method('fetchJwt')->willReturn($jwt);
        $result = $this->stub->generateJwtPublic($member);

        $this->assertTrue($result);
        $this->assertEquals(Core::$container->get('jwt'), $jwt);
    }

    public function testSaveMemberToCache()
    {
        $member = new Member(1);
        $memberArray = array('memberArray');
        $cacheKey = MemberJwtAuth::CACHE_KEY.$member->getId();
        $time = MemberJwtAuth::EXPIRATION_TIME;

        // 为 MemberTranslator( 类建立预言(prophecy)。
        $translator = $this->prophesize(MemberTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($member)->shouldBeCalled(1)->willReturn($memberArray);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('save')->with($cacheKey, $memberArray, $time)->willReturn(true);

        $result = $this->stub->saveMemberToCachePublic($member);
        $this->assertTrue($result);
    }

    public function testInitMember()
    {
        $member = new Member(1);

        $result = $this->stub->initMemberPublic($member);

        $this->assertTrue($result);
        $this->assertEquals(Core::$container->get('member'), $member);
    }

    public function testVerifyJwtAndInitUserFalse()
    {
        $jwt = 'jwt';
        $this->stub->expects($this->exactly(1))->method('verifyJwt')->with($jwt)->willReturn(false);

        $result = $this->stub->verifyJwtAndInitUser($jwt);

        $this->assertFalse($result);
        $this->assertEquals(Core::$container->get('member'), NullMember::getInstance());
    }

    public function testVerifyJwtAndInitUserTrue()
    {
        $jwt = 'jwt';
        $jti = 'jti';
        $member = new Member(1);
        $this->stub->setJti($jti);
        $this->stub->expects($this->exactly(1))->method('verifyJwt')->with($jwt)->willReturn(true);
        $this->stub->expects($this->exactly(1))->method('fetchMember')->willReturn($member);

        $result = $this->stub->verifyJwtAndInitUser($jwt);

        $this->assertTrue($result);
        $this->assertEquals(Core::$container->get('member'), $member);
    }

    public function fetchMember($data)
    {
        $id = 1;
        $this->stub->setUid($id);
        $cacheKey = MemberJwtAuth::CACHE_KEY.$id;
        $member = new Member(1);

        $this->stub->expects($this->exactly(1))->method('get')->with($cacheKey)->willReturn($data);

        if (empty($data)) {
            // 为 MemberRepository 类建立预言(prophecy)。
            $repository = $this->prophesize(MemberRepository::class);

            // 建立预期状况:scenario() 方法将会被调用一次。
            $repository->scenario(
                MemberRepository::FETCH_ONE_MODEL_UN
            )->shouldBeCalledTimes(1)->willReturn($repository->reveal());
            // 建立预期状况:fetchOne() 方法将会被调用一次。
            $repository->fetchOne($id)->shouldBeCalled(1)->willReturn($member);
            // 为 getRepository() 方法建立预期：该方法被调用一次,返回揭示预言。
            $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());
        }

        if (!empty($data)) {
            // 为 MemberTranslator 类建立预言(prophecy)。
            $translator = $this->prophesize(MemberTranslator::class);
            // 建立预期状况:arrayToObject() 方法将会被调用一次。
            $translator->arrayToObject($data)->shouldBeCalled(1)->willReturn($member);
            // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
            $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());
        }

        $result = $this->stub->fetchMemberPublic();
        $this->assertEquals($result, $member);
    }

    public function testFetchMemberEmpty()
    {
        $data = array();

        $this->fetchMember($data);
    }

    public function testFetchMember()
    {
        $data = array('data');

        $this->fetchMember($data);
    }

    public function testClearJwtAndMemberToCache()
    {
        $member = new Member(1);
        $cacheKey = MemberJwtAuth::CACHE_KEY.$member->getId();

        $this->stub->expects($this->exactly(1))->method('del')->with($cacheKey)->willReturn(true);

        $result = $this->stub->clearJwtAndMemberToCache($member);
        $this->assertTrue($result);
        $this->assertEquals(Core::$container->get('jwt'), '');
    }
}
