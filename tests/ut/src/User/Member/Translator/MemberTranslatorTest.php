<?php
namespace Sdk\User\Member\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\User\Member\Model\Member;
use Sdk\User\Member\Utils\MockObjectGenerate;
use Sdk\User\Member\Utils\TranslatorUtilsTrait;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MemberTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new MemberTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new MemberTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\User\Member\Model\NullMember',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\User\Member\Model\NullMember',
            $result
        );
    }

    public function testArrayToObject()
    {
        $member = MockObjectGenerate::generateMember(1);

        $expression['id'] = marmot_encode($member->getId());
        $expression['email'] = $member->getEmail();
        $expression['phone'] = $member->getPhone();
        $expression['userName'] = $member->getUserName();
        $expression['enterpriseName'] = $member->getEnterpriseName();
        $expression['enterpriseAddress'] = $member->getEnterpriseAddress();
        $expression['activeStatus']['id'] = marmot_encode($member->getActiveStatus());
        $expression['status']['id'] = marmot_encode($member->getStatus());
        $expression['statusTime'] = $member->getStatusTime();
        $expression['createTime'] = $member->getCreateTime();
        $expression['updateTime'] = $member->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\User\Member\Model\Member',
            $result
        );

        $this->compareTranslatorEquals($expression, $result);
    }

    public function testObjectToArrayEmpty()
    {
        $member = array();
        $result = $this->stub->objectToArray($member);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(MemberTranslatorMock::class)
                           ->setMethods([
                               'statusFormatConversion'
                            ])->getMock();

        $member = MockObjectGenerate::generateMember(1);
        $activeStatus = $member->getActiveStatus();
        $status = $member->getStatus();
        $activeStatusArray = array('activeStatus');
        $statusArray = array('status');

        $stub->expects($this->exactly(2))->method('statusFormatConversion')
            ->will($this->returnValueMap([
                [$activeStatus, Member::ACTIVE_STATUS_TYPE, Member::ACTIVE_STATUS_CN, $activeStatusArray],
                [$status, Member::STATUS_TYPE, Member::STATUS_CN, $statusArray]
            ]));

        $result = $stub->objectToArray($member);

        $this->assertEquals($result['activeStatus'], $activeStatusArray);
        $this->assertEquals($result['status'], $statusArray);

        $this->compareTranslatorEquals($result, $member);
    }
}
