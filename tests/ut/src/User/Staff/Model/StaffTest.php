<?php
namespace Sdk\User\Staff\Model;

use PHPUnit\Framework\TestCase;

use Sdk\User\Staff\Repository\StaffRepository;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class StaffTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(StaffMock::class)
                           ->setMethods(['getRepository', 'getStaffJwtAuth'])
                           ->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsUser()
    {
        $this->assertInstanceOf(
            'Sdk\User\Model\User',
            $this->stub
        );
    }

    /**
     * Staff 领域对象,测试构造函数
     */
    public function testStaffConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getPurview());
        $this->assertEmpty($this->stub->getRoles());
    }

    //purview 测试 -------------------------------------------------------- start
    /**
     * 设置 Staff setPurview() 正确的传参类型,期望传值正确
     */
    public function testSetPurviewCorrectType()
    {
        $this->stub->setPurview(array('purviews'));
        $this->assertEquals(array('purviews'), $this->stub->getPurview());
    }

    /**
     * 设置 Staff setPurview() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPurviewWrongType()
    {
        $this->stub->setPurview('purview');
    }
    //purview 测试 --------------------------------------------------------   end

    //roles 测试 -------------------------------------------------------- start
    /**
     * 设置 Staff setRoles() 正确的传参类型,期望传值正确
     */
    public function testSetRolesCorrectType()
    {
        $this->stub->setRoles(array('roles'));
        $this->assertEquals(array('roles'), $this->stub->getRoles());
    }

    /**
     * 设置 Staff setRoles() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetRolesWrongType()
    {
        $this->stub->setRoles('roles');
    }
    //roles 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new StaffMock();
        $this->assertInstanceOf(
            'Sdk\User\Staff\Repository\StaffRepository',
            $stub->getRepositoryPublic()
        );
    }

    public function testGetStaffJwtAuth()
    {
        $stub = new StaffMock();
        $this->assertInstanceOf(
            'Sdk\User\Staff\Model\StaffJwtAuth',
            $stub->getStaffJwtAuthPublic()
        );
    }

    public function testLogin()
    {
        // 为 StaffRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(StaffRepository::class);
        // 建立预期状况:login() 方法将会被调用一次。
        $repository->login($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        // 为 StaffJwtAuth 类建立预言(prophecy)。
        $staffJwtAuth = $this->prophesize(StaffJwtAuth::class);
        // 建立预期状况:generateJwtAndSaveStaffToCache() 方法将会被调用一次。
        $staffJwtAuth->generateJwtAndSaveStaffToCache($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getStaffJwtAuth')->willReturn($staffJwtAuth->reveal());

        $result = $this->stub->login();

        $this->assertTrue($result);
    }

    public function testLogout()
    {
        // 为 StaffJwtAuth 类建立预言(prophecy)。
        $staffJwtAuth = $this->prophesize(StaffJwtAuth::class);
        // 建立预期状况:generateJwtAndSaveStaffToCache() 方法将会被调用一次。
        $staffJwtAuth->clearJwtAndStaffToCache($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getStaffJwtAuth')->willReturn($staffJwtAuth->reveal());

        $result = $this->stub->logout();

        $this->assertTrue($result);
    }

    public function testResetPassword()
    {
        // 为 StaffRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(StaffRepository::class);
        // 建立预期状况:resetPassword() 方法将会被调用一次。
        $repository->resetPassword($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->resetPassword();

        $this->assertTrue($result);
    }

    public function testUpdatePassword()
    {
        // 为 StaffRepository 类建立预言(prophecy)。
        $repository = $this->prophesize(StaffRepository::class);
        // 建立预期状况:updatePassword() 方法将会被调用一次。
        $repository->updatePassword($this->stub)->shouldBeCalled(1)->willReturn(true);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getRepository')->willReturn($repository->reveal());

        $result = $this->stub->updatePassword();

        $this->assertTrue($result);
    }
}
