<?php
namespace Sdk\User\Staff\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullStaffTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullStaff::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsStaff()
    {
        $this->assertInstanceOf(
            'Sdk\User\Staff\Model\Staff',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullStaffMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }

    public function initOperation($method)
    {
        $stub = $this->getMockBuilder(NullStaffMock::class)
                           ->setMethods(['resourceNotExist'])
                           ->getMock();
        $stub->expects($this->exactly(1))->method('resourceNotExist')->willReturn(false);

        $result = $stub->$method();
 
        $this->assertFalse($result);
    }

    public function testLogin()
    {
        $this->initOperation('login');
    }

    public function testLogout()
    {
        $this->initOperation('logout');
    }

    public function testResetPassword()
    {
        $this->initOperation('resetPassword');
    }

    public function testUpdatePassword()
    {
        $this->initOperation('updatePassword');
    }
}
