<?php
namespace Sdk\User\Staff\Utils;

use Sdk\User\Staff\Model\Staff;

use Sdk\Role\Utils\MockObjectGenerate as RoleMockObjectGenerate;

class MockObjectGenerate
{
    public static function generateStaff(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Staff {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $staff = new Staff();

        $staff->setId($id);

        //name
        self::generateName($staff, $value, $faker);
        //phone
        self::generatePhone($staff, $value, $faker);
        //email
        self::generateEmail($staff, $value, $faker);
        //password
        self::generatePassword($staff, $value, $faker);
        //purview
        self::generatePurview($staff, $value, $faker);
        //roles
        self::generateRoles($staff, $value, $faker);
        //status
        self::generateStatus($staff, $value, $faker);
        $staff->setCreateTime($faker->unixTime());
        $staff->setUpdateTime($faker->unixTime());
        $staff->setStatusTime($faker->unixTime());

        return $staff;
    }

    private static function generateName(Staff $staff, array $value, $faker) :void
    {
        //name
        $name = isset($value['name']) ? $value['name'] : $faker->name();
        $staff->setName($name);
    }

    private static function generatePhone(Staff $staff, array $value, $faker) :void
    {
        //phone
        $phone = isset($value['phone']) ? $value['phone'] : $faker->phoneNumber();
        $staff->setPhone($phone);
    }

    private static function generateEmail(Staff $staff, array $value, $faker) :void
    {
        //email
        $email = isset($value['email']) ? $value['email'] : $faker->email();
        $staff->setEmail($email);
    }

    private static function generatePassword(Staff $staff, array $value, $faker) :void
    {
        //password
        $password = isset($value['password']) ? $value['password'] : $faker->bothify('###??@A??');
        $staff->setPassword($password);
    }

    private static function generatePurview(Staff $staff, array $value, $faker) :void
    {
        //purview
        $purview = isset($value['purview']) ? $value['purview'] : array($faker->word());
        $staff->setPurview($purview);
    }

    private static function generateRoles(Staff $staff, array $value, $faker) :void
    {
        //roles
        $roles = isset($value['roles']) ?
                        $value['roles'] :
                        [RoleMockObjectGenerate::generateRole($faker->randomDigitNotNull())];

        $staff->setRoles($roles);
    }
    
    private static function generateStatus(Staff $staff, $value, $faker) : void
    {
        $status = isset($value['status']) ? $value['status'] : $faker->randomElement(Staff::STATUS);

        $staff->setStatus($status);
    }
}
