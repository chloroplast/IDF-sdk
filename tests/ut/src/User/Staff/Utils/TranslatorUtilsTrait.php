<?php
namespace Sdk\User\Staff\Utils;

use Marmot\Core;
use Sdk\User\Staff\Model\Staff;

/**
 * @todo
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait TranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(Staff $staff, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $staff->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $staff->getName());
        }
        if (isset($attributes['phone'])) {
            $this->assertEquals($attributes['phone'], $staff->getPhone());
        }
        if (isset($attributes['email'])) {
            $this->assertEquals($attributes['email'], $staff->getEmail());
        }
        if (isset($attributes['password'])) {
            $this->assertEquals($attributes['password'], $staff->getPassword());
        }
        if (isset($attributes['oldPassword'])) {
            $this->assertEquals($attributes['oldPassword'], $staff->getOldPassword());
        }
        if (isset($attributes['purview'])) {
            $this->assertEquals($attributes['purview'], $staff->getPurview());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $staff->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $staff->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $staff->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $staff->getUpdateTime());
        }

        $relationships = isset($expression['data']['relationships']) ? $expression['data']['relationships'] : array();
        if (isset($relationships['roles']['data'])) {
            $roles = $relationships['roles']['data'];
            foreach ($roles as $key => $role) {
                $this->assertEquals($role['type'], 'roles');
                $this->assertEquals($role['id'], $staff->getRoles()[$key]->getId());
            }
        }
    }

    public function compareTranslatorEquals(array $expression, Staff $staff)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($staff->getId()));
        }
        if (isset($expression['name'])) {
            $this->assertEquals($expression['name'], $staff->getName());
        }
        if (isset($expression['phone'])) {
            $this->assertEquals($expression['phone'], $staff->getPhone());
        }
        if (isset($expression['email'])) {
            $this->assertEquals($expression['email'], $staff->getEmail());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $staff->getCreateTime());
        }
        if (isset($expression['createTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $staff->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $staff->getUpdateTime());
        }
        if (isset($expression['updateTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $staff->getUpdateTime())
            );
        }
    }
}
