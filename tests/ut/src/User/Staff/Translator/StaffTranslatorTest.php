<?php
namespace Sdk\User\Staff\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\User\Staff\Model\Staff;
use Sdk\User\Staff\Utils\MockObjectGenerate;
use Sdk\User\Staff\Utils\TranslatorUtilsTrait;

use Sdk\Role\Translator\RoleTranslator;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class StaffTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new StaffTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetRoleTranslator()
    {
        $stub = new StaffTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\Role\Translator\RoleTranslator',
            $stub->getRoleTranslatorPublic()
        );
    }

    public function testGetNullObject()
    {
        $stub = new StaffTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\User\Staff\Model\NullStaff',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\User\Staff\Model\NullStaff',
            $result
        );
    }

    public function testArrayToObject()
    {
        $stub = $this->getMockBuilder(StaffTranslatorMock::class)
                           ->setMethods([
                               'purviewFormatConversionToObject',
                               'getRoleTranslator'
                            ])->getMock();

        $staff = MockObjectGenerate::generateStaff(1);
        $rolesArray = $this->arrayToObjectRoles($staff, $stub);

        $purview = $staff->getPurview();
        $purviewFormatConversion = array('purviewFormatConversion');
        $stub->expects($this->exactly(1))->method(
            'purviewFormatConversionToObject'
        )->with($purviewFormatConversion)->willReturn($purview);

        $expression['id'] = marmot_encode($staff->getId());
        $expression['email'] = $staff->getEmail();
        $expression['name'] = $staff->getName();
        $expression['phone'] = $staff->getPhone();
        $expression['purview'] = $purviewFormatConversion;
        $expression['roles'] = $rolesArray;
        $expression['status']['id'] = marmot_encode($staff->getStatus());
        $expression['statusTime'] = $staff->getStatusTime();
        $expression['createTime'] = $staff->getCreateTime();
        $expression['updateTime'] = $staff->getUpdateTime();

        $result = $stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\User\Staff\Model\Staff',
            $result
        );

        $this->assertEquals($result->getPurview(), $purview);
        $this->assertEquals($result->getRoles(), $staff->getRoles());
        $this->compareTranslatorEquals($expression, $result);
    }

    private function arrayToObjectRoles(Staff $staff, $stub)
    {
        $role = current($staff->getRoles());
        $roleArray = array('id' => $role->getId());

        // 为 RoleTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(RoleTranslator::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $translator->arrayToObject($roleArray)->shouldBeCalled(1)->willReturn($role);
        // 为 getRoleTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $stub->expects($this->exactly(1))->method(
            'getRoleTranslator'
        )->willReturn($translator->reveal());

        return [$roleArray];
    }

    public function testObjectToArrayEmpty()
    {
        $staff = array();
        $result = $this->stub->objectToArray($staff);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(StaffTranslatorMock::class)
                           ->setMethods([
                               'purviewFormatConversionToArray',
                               'getRoleTranslator',
                               'statusFormatConversion',
                            ])->getMock();

        $staff = MockObjectGenerate::generateStaff(1);
        list($rolesArray) = $this->relationObjectToArray($staff, $stub);

        $statusArray = $this->statusFormatConversion($staff, $stub);
        
        $result = $stub->objectToArray($staff);

        $this->assertEquals($result['status'], $statusArray);
        $this->assertEquals($result['roles'], $rolesArray);
        
        $this->compareTranslatorEquals($result, $staff);
    }

    private function statusFormatConversion(Staff $staff, $stub) : array
    {
        $statusArray = array('status');

        $stub->expects($this->exactly(1))->method(
            'statusFormatConversion'
        )->with($staff->getStatus())->willReturn($statusArray);

        return $statusArray;
    }

    private function relationObjectToArray(Staff $staff, $stub) : array
    {
        $rolesArray = $this->rolesRelationObjectToArray($staff, $stub);

        return [$rolesArray];
    }

    private function rolesRelationObjectToArray(Staff $staff, $stub) : array
    {
        $role = current($staff->getRoles());
        $roleArray = array('roleArray');

        // 为 RoleTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(RoleTranslator::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $translator->objectToArray($role, [])->shouldBeCalled(1)->willReturn($roleArray);
        // 为 getRoleTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $stub->expects($this->exactly(1))->method(
            'getRoleTranslator'
        )->willReturn($translator->reveal());

        return [$roleArray];
    }

    public function testPurviewFormatConversionToArray()
    {
        $stub = new StaffTranslatorMock();

        $purview = array(1=>2, 2=>4);

        $purviewResult = array(
            array(
                'id' => 1,
                'actions' => 2
            ),
            array(
                'id' => 2,
                'actions' => 4
            )
        );

        $result = $stub->purviewFormatConversionToArrayPublic($purview);
        $this->assertEquals($purviewResult, $result);
    }

    public function testPurviewFormatConversionToObject()
    {
        $stub = new StaffTranslatorMock();

        $purviewResult = array(1=>5, 2=>7);

        $purview = array(
            array(
                'id' => 1,
                'actions' => 5
            ),
            array(
                'id' => 2,
                'actions' => 7
            )
        );

        $result = $stub->purviewFormatConversionToObjectPublic($purview);
        $this->assertEquals($purviewResult, $result);
    }
}
