<?php
namespace Sdk\CarType\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullCarTypeTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullCarType::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsCarType()
    {
        $this->assertInstanceOf(
            'Sdk\CarType\Model\CarType',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullCarTypeMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
