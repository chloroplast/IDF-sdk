<?php
namespace Sdk\CarType\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;
use Sdk\User\Staff\Model\Staff;

/**
 * @todo
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class CarTypeTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CarType();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * CarType 领域对象,测试构造函数
     */
    public function testCarTypeConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getName());
        $this->assertEmpty($this->stub->getPalletNumber());
        $this->assertEmpty($this->stub->getWeight());
        $this->assertInstanceOf('Sdk\User\Staff\Model\Staff', $this->stub->getStaff());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 CarType setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(5);
        $this->assertEquals(5, $this->stub->getId());
    }

    /**
     * 设置 CarType setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //name 测试 -------------------------------------------------------- start
    /**
     * 设置 CarType setName() 正确的传参类型,期望传值正确
     */
    public function testSetNameCorrectType()
    {
        $this->stub->setName('name');
        $this->assertEquals('name', $this->stub->getName());
    }

    /**
     * 设置 CarType setName() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetNameWrongType()
    {
        $this->stub->setName(array('name'));
    }
    //name 测试 --------------------------------------------------------   end

    //palletNumber 测试 -------------------------------------------------------- start
    /**
     * 设置 CarType setPalletNumber() 正确的传参类型,期望传值正确
     */
    public function testSetPalletNumberCorrectType()
    {
        $this->stub->setPalletNumber(0);
        $this->assertEquals(0, $this->stub->getPalletNumber());
    }

    /**
     * 设置 CarType setPalletNumber() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetPalletNumberWrongType()
    {
        $this->stub->setPalletNumber('palletNumber');
    }
    //palletNumber 测试 --------------------------------------------------------   end

    //weight 测试 -------------------------------------------------------- start
    /**
     * 设置 CarType setWeight() 正确的传参类型,期望传值正确
     */
    public function testSetWeightCorrectType()
    {
        $this->stub->setWeight(0);
        $this->assertEquals(0, $this->stub->getWeight());
    }

    /**
     * 设置 CarType setWeight() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetWeightWrongType()
    {
        $this->stub->setWeight('weight');
    }
    //weight 测试 --------------------------------------------------------   end

    //staff 测试 -------------------------------------------------------- start
    /**
     * 设置 CarType setStaff() 正确的传参类型,期望传值正确
     */
    public function testSetStaffCorrectType()
    {
        $staff = new Staff();
        $this->stub->setStaff($staff);
        $this->assertEquals($staff, $this->stub->getStaff());
    }

    /**
     * 设置 CarType setStaff() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetStaffWrongType()
    {
        $this->stub->setStaff(array('staff'));
    }
    //staff 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new CarTypeMock();
        $this->assertInstanceOf(
            'Sdk\CarType\Repository\CarTypeRepository',
            $stub->getRepositoryPublic()
        );
    }
}
