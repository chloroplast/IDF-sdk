<?php
namespace Sdk\CarType\Repository;

use PHPUnit\Framework\TestCase;

class CarTypeRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CarTypeRepository();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsICarTypeAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\CarType\Adapter\CarType\ICarTypeAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\CarType\Adapter\CarType\CarTypeRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\CarType\Adapter\CarType\CarTypeMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
