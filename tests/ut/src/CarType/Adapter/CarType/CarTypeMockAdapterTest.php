<?php
namespace Sdk\CarType\Adapter\CarType;

use PHPUnit\Framework\TestCase;

class CarTypeMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CarTypeMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsICarTypeAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\CarType\Adapter\CarType\ICarTypeAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\CarType\Model\CarType',
            $this->stub->fetchObject(1)
        );
    }
}
