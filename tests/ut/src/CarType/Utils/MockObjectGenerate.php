<?php
namespace Sdk\CarType\Utils;

use Sdk\CarType\Model\CarType;

use Sdk\User\Staff\Utils\MockObjectGenerate as MockStaffGenerate;

class MockObjectGenerate
{
    public static function generateCarType(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : CarType {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $carType = new CarType($id);
        $carType->setId($id);

        //name
        $name = isset($value['name']) ? $value['name'] : $faker->name();
        $carType->setName($name);
        
        //palletNumber
        $palletNumber = isset($value['palletNumber']) ? $value['palletNumber'] : $faker->randomDigitNotNull();
        $carType->setPalletNumber($palletNumber);
        
        //weight
        $weight = isset($value['weight']) ? $value['weight'] : $faker->randomDigitNotNull();
        $carType->setWeight($weight);

        //staff
        $staff = isset($value['staff']) ? $value['staff'] : MockStaffGenerate::generateStaff(0);
        $carType->setStaff($staff);
        
        $carType->setStatus(0);
        $carType->setCreateTime($faker->unixTime());
        $carType->setUpdateTime($faker->unixTime());
        $carType->setStatusTime($faker->unixTime());

        return $carType;
    }
}
