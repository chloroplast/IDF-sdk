<?php
namespace Sdk\CarType\Utils;

use Sdk\CarType\Model\CarType;

/**
 * @todo
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait TranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(CarType $carType, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $carType->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['name'])) {
            $this->assertEquals($attributes['name'], $carType->getName());
        }
        if (isset($attributes['palletNumber'])) {
            $this->assertEquals($attributes['palletNumber'], $carType->getPalletNumber());
        }
        if (isset($attributes['weight'])) {
            $this->assertEquals($attributes['weight'], $carType->getWeight());
        }
        if (isset($attributes['status'])) {
            $this->assertEquals($attributes['status'], $carType->getStatus());
        }
        if (isset($attributes['statusTime'])) {
            $this->assertEquals($attributes['statusTime'], $carType->getStatusTime());
        }
        if (isset($attributes['createTime'])) {
            $this->assertEquals($attributes['createTime'], $carType->getCreateTime());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $carType->getUpdateTime());
        }

        $relationships = isset($expression['data']['relationships']) ? $expression['data']['relationships'] : array();
        if (isset($relationships['staff']['data'])) {
            $staff = $relationships['staff']['data'];
            $this->assertEquals($staff['type'], 'staff');
            $this->assertEquals(intval($staff['id']), $carType->getStaff()->getId());
        }
    }

    public function compareTranslatorEquals(array $expression, CarType $carType)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($carType->getId()));
        }
        if (isset($expression['name'])) {
            $this->assertEquals($expression['name'], $carType->getName());
        }
        if (isset($expression['palletNumber'])) {
            $this->assertEquals($expression['palletNumber'], $carType->getPalletNumber());
        }
        if (isset($expression['weight'])) {
            $this->assertEquals($expression['weight'], $carType->getWeight());
        }
        if (isset($expression['createTime'])) {
            $this->assertEquals($expression['createTime'], $carType->getCreateTime());
        }
        if (isset($expression['createTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['createTimeFormatConvert'],
                date('Y-m-d H:i', $carType->getCreateTime())
            );
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $carType->getUpdateTime());
        }
        if (isset($expression['updateTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $carType->getUpdateTime())
            );
        }
    }
}
