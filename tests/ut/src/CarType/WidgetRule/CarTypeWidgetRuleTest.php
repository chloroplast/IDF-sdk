<?php
namespace Sdk\CarType\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class CarTypeWidgetRuleTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CarTypeWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //name
    /**
     * @dataProvider additionProviderName
     */
    public function testName($parameter, $expected)
    {
        $result = $this->stub->name($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CARTYPE_NAME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderName()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array($faker->name(), true),
            array($faker->words(), false)
        );
    }

    //palletNumber
    /**
     * @dataProvider additionProviderPalletNumber
     */
    public function testPalletNumber($parameter, $expected)
    {
        $result = $this->stub->palletNumber($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CARTYPE_PALLET_NUMBER_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderPalletNumber()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->name(), false),
            array($faker->numberBetween(1, 99), true)
        );
    }

    //weight
    /**
     * @dataProvider additionProviderWeight
     */
    public function testWeight($parameter, $expected)
    {
        $result = $this->stub->weight($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(CARTYPE_WEIGHT_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderWeight()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->name(), false),
            array($faker->numberBetween(1, 999999), true)
        );
    }
}
