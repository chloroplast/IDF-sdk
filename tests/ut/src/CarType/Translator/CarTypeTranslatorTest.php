<?php
namespace Sdk\CarType\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\CarType\Utils\MockObjectGenerate;
use Sdk\CarType\Utils\TranslatorUtilsTrait;
use Sdk\CarType\Model\CarType;

use Sdk\User\Staff\Translator\StaffTranslator;

class CarTypeTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CarTypeTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetStaffTranslator()
    {
        $stub = new CarTypeTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffTranslator',
            $stub->getStaffTranslatorPublic()
        );
    }

    public function testGetNullObject()
    {
        $stub = new CarTypeTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\CarType\Model\NullCarType',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\CarType\Model\NullCarType',
            $result
        );
    }

    public function testArrayToObject()
    {
        $carType = MockObjectGenerate::generateCarType(1);
        
        $expression['id'] = marmot_encode($carType->getId());
        $expression['name'] = $carType->getName();
        $expression['palletNumber'] = $carType->getPalletNumber();
        $expression['weight'] = $carType->getWeight();
        $expression['status']['id'] = marmot_encode($carType->getStatus());
        $expression['statusTime'] = $carType->getStatusTime();
        $expression['createTime'] = $carType->getCreateTime();
        $expression['updateTime'] = $carType->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\CarType\Model\CarType',
            $result
        );

        $this->compareTranslatorEquals($expression, $result);
    }

    public function testObjectToArrayEmpty()
    {
        $carType = array();
        $result = $this->stub->objectToArray($carType);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $stub = $this->getMockBuilder(CarTypeTranslatorMock::class)
                           ->setMethods([
                               'statusFormatConversion',
                               'getStaffTranslator'
                            ])->getMock();

        $carType = MockObjectGenerate::generateCarType(1);
        $status = $carType->getStatus();
        $statusFormatConversion = array('statusFormatConversion');
        
        $stub->expects($this->exactly(1))->method(
            'statusFormatConversion'
        )->with($status)->willReturn($statusFormatConversion);

        $staffArray = $this->staffRelationObjectToArray($carType, $stub);

        $result = $stub->objectToArray($carType);

        $this->assertEquals($result['status'], $statusFormatConversion);
        $this->assertEquals($result['staff'], $staffArray);

        $this->compareTranslatorEquals($result, $carType);
    }

    private function staffRelationObjectToArray(CarType $carType, $stub) : array
    {
        $staff = $carType->getStaff();
        $staffArray = array('staffArray');

        // 为 StaffTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(StaffTranslator::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $translator->objectToArray(
            $staff,
            ['id', 'name']
        )->shouldBeCalled(1)->willReturn($staffArray);
        // 为 getStaffTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $stub->expects($this->exactly(1))->method(
            'getStaffTranslator'
        )->willReturn($translator->reveal());

        return $staffArray;
    }
}
