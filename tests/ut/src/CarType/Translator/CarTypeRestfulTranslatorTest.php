<?php
namespace Sdk\CarType\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\CarType\Utils\MockObjectGenerate;
use Sdk\CarType\Utils\TranslatorUtilsTrait;

use Sdk\User\Staff\Utils\MockObjectGenerate as MockStaffGenerate;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

class CarTypeRestfulTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new CarTypeRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testGetStaffRestfulTranslator()
    {
        $stub = new CarTypeRestfulTranslatorMock();
        $this->assertInstanceOf(
            'Sdk\User\Staff\Translator\StaffRestfulTranslator',
            $stub->getStaffRestfulTranslatorPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\CarType\Model\NullCarType',
            $result
        );
    }

    public function testArrayToObject()
    {
        $carType = MockObjectGenerate::generateCarType(1);

        $expression['data']['id'] = $carType->getId();
        $expression['data']['attributes']['name'] = $carType->getName();
        $expression['data']['attributes']['palletNumber'] = $carType->getPalletNumber();
        $expression['data']['attributes']['weight'] = $carType->getWeight();
        $expression['data']['attributes']['status'] = $carType->getStatus();
        $expression['data']['attributes']['statusTime'] = $carType->getStatusTime();
        $expression['data']['attributes']['createTime'] = $carType->getCreateTime();
        $expression['data']['attributes']['updateTime'] = $carType->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\CarType\Model\CarType',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testArrayToObjectRelationships()
    {
        $stub = $this->getMockBuilder(CarTypeRestfulTranslatorMock::class)
                           ->setMethods([
                               'includedFormatConversion',
                               'relationshipFill',
                               'getStaffRestfulTranslator'
                            ])->getMock();

        $relationships = array(
            'staff' => array('staff')
        );
        $included = array('included');
        $expression['data']['relationships'] = $relationships;
        $expression['included'] = $included;

        $includedConversion = array('includedConversion');
        $staffArray = array('staffArray');
        $staff = MockStaffGenerate::generateStaff(1);
        $stub->expects($this->exactly(1))->method(
            'includedFormatConversion'
        )->with($included)->willReturn($includedConversion);

        $stub->expects($this->exactly(1))->method('relationshipFill')
            ->will($this->returnValueMap([[$relationships['staff'], $includedConversion, $staffArray]]));

        // 为 StaffRestfulTranslator 类建立预言(prophecy)。
        $staffRestfulTranslator = $this->prophesize(StaffRestfulTranslator::class);
        // 建立预期状况:method() 方法将会被调用一次。
        $staffRestfulTranslator->arrayToObject($staffArray)->shouldBeCalled(1)->willReturn($staff);
        // 为 getStaffRestfulTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $stub->expects($this->exactly(1))->method(
            'getStaffRestfulTranslator'
        )->willReturn($staffRestfulTranslator->reveal());

        $result = $stub->arrayToObject($expression);
        $this->assertInstanceOf(
            'Sdk\CarType\Model\CarType',
            $result
        );

        $this->assertEquals($staff, $result->getStaff());
    }

    public function testObjectToArrayEmpty()
    {
        $carType = array();
        $result = $this->stub->objectToArray($carType);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $carType = MockObjectGenerate::generateCarType(1);

        $result = $this->stub->objectToArray($carType);
        $this->compareRestfulTranslatorEquals($carType, $result);
    }
}
