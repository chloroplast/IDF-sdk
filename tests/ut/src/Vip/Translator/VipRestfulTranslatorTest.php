<?php
namespace Sdk\Vip\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Vip\Utils\MockObjectGenerate;
use Sdk\Vip\Utils\TranslatorUtilsTrait;

class VipRestfulTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new VipRestfulTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIRestfulTranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\IRestfulTranslator',
            $this->stub
        );
    }

    public function testArrayToObjectEmpty()
    {
        $result = $this->stub->arrayToObject([]);

        $this->assertInstanceOf(
            'Sdk\Vip\Model\NullVip',
            $result
        );
    }

    public function testArrayToObject()
    {
        $vip = MockObjectGenerate::generateVip(1);

        $expression['data']['id'] = $vip->getId();
        $expression['data']['attributes']['indicators'] = $vip->getIndicators();
        $expression['data']['attributes']['updateTime'] = $vip->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Vip\Model\Vip',
            $result
        );

        $this->compareRestfulTranslatorEquals($result, $expression);
    }

    public function testObjectToArrayEmpty()
    {
        $vip = array();
        $result = $this->stub->objectToArray($vip);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $vip = MockObjectGenerate::generateVip(1);

        $result = $this->stub->objectToArray($vip);
        $this->compareRestfulTranslatorEquals($vip, $result);
    }
}
