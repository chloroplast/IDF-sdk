<?php
namespace Sdk\Vip\Translator;

use PHPUnit\Framework\TestCase;

use Sdk\Vip\Utils\MockObjectGenerate;
use Sdk\Vip\Utils\TranslatorUtilsTrait;

class VipTranslatorTest extends TestCase
{
    use TranslatorUtilsTrait;
    
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new VipTranslator();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsITranslator()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\ITranslator',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $stub = new VipTranslatorMock();

        $this->assertInstanceOf(
            'Sdk\Vip\Model\NullVip',
            $stub->getNullObjectPublic()
        );
    }

    public function testArrayToObjectEmpty()
    {
        $expression = array();
        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Vip\Model\NullVip',
            $result
        );
    }

    public function testArrayToObject()
    {
        $vip = MockObjectGenerate::generateVip(1);
        
        $expression['id'] = marmot_encode($vip->getId());
        $expression['indicators'] = $vip->getIndicators();
        $expression['updateTime'] = $vip->getUpdateTime();

        $result = $this->stub->arrayToObject($expression);

        $this->assertInstanceOf(
            'Sdk\Vip\Model\Vip',
            $result
        );

        $this->compareTranslatorEquals($expression, $result);
    }

    public function testObjectToArrayEmpty()
    {
        $vip = array();
        $result = $this->stub->objectToArray($vip);

        $this->assertEmpty($result);
    }

    public function testObjectToArray()
    {
        $vip = MockObjectGenerate::generateVip(1);
        $result = $this->stub->objectToArray($vip);

        $this->compareTranslatorEquals($result, $vip);
    }
}
