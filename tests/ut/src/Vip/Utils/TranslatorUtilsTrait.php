<?php
namespace Sdk\Vip\Utils;

use Sdk\Vip\Model\Vip;

/**
 * @todo
 * @SuppressWarnings(PHPMD.NPathComplexity)
 */
trait TranslatorUtilsTrait
{
    public function compareRestfulTranslatorEquals(Vip $vip, array $expression)
    {
        if (isset($expression['data']['id'])) {
            $this->assertEquals($expression['data']['id'], $vip->getId());
        }

        $attributes = isset($expression['data']['attributes']) ? $expression['data']['attributes'] : array();
        if (isset($attributes['indicators'])) {
            $this->assertEquals($attributes['indicators'], $vip->getIndicators());
        }
        if (isset($attributes['updateTime'])) {
            $this->assertEquals($attributes['updateTime'], $vip->getUpdateTime());
        }
    }

    public function compareTranslatorEquals(array $expression, Vip $vip)
    {
        if (isset($expression['id'])) {
            $this->assertEquals($expression['id'], marmot_encode($vip->getId()));
        }
        if (isset($expression['indicators'])) {
            $this->assertEquals($expression['indicators'], $vip->getIndicators());
        }
        if (isset($expression['updateTime'])) {
            $this->assertEquals($expression['updateTime'], $vip->getUpdateTime());
        }
        if (isset($expression['updateTimeFormatConvert'])) {
            $this->assertEquals(
                $expression['updateTimeFormatConvert'],
                date('Y-m-d H:i', $vip->getUpdateTime())
            );
        }
    }
}
