<?php
namespace Sdk\Vip\Utils;

use Sdk\Vip\Model\Vip;

class MockObjectGenerate
{
    public static function generateVip(
        int $id = 0,
        int $seed = 0,
        array $value = array()
    ) : Vip {
        $faker = \Faker\Factory::create('zh_CN');
        $faker->seed($seed);

        $vip = new Vip($id);
        $vip->setId($id);

        //indicators
        $indicators = isset($value['indicators'])
        ? $value['indicators']
        : array(
            array(
                'name' => 'name1',
                'consumerMinPrice' => 0,
                'consumerMaxPrice' => 100
            ),
            array(
                'name' => 'name2',
                'consumerMinPrice' => 101,
                'consumerMaxPrice' => 200
            ),
            array(
                'name' => 'name3',
                'consumerMinPrice' => 201,
                'consumerMaxPrice' => 300
            )
        );
        $vip->setIndicators($indicators);

        $vip->setStatus(0);
        $vip->setCreateTime($faker->unixTime());
        $vip->setUpdateTime($faker->unixTime());
        $vip->setStatusTime($faker->unixTime());

        return $vip;
    }
}
