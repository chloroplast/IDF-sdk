<?php
namespace Sdk\Vip\Adapter\Vip;

use PHPUnit\Framework\TestCase;

use Sdk\Vip\Model\VipMock;
use Sdk\Vip\Model\NullVipMock;
use Sdk\Vip\Translator\VipRestfulTranslator;

class VipRestfulAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = $this->getMockBuilder(VipRestfulAdapterMock::class)
                           ->setMethods([
                               'getTranslator',
                               'patch',
                               'getResource',
                               'isSuccess',
                               'translateToObject',
                               'get',
                               'getNullObject'
                            ])->getMock();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testExtendsCommonRestfulAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Adapter\CommonRestfulAdapter',
            $this->stub
        );
    }

    public function testImplementsIVipAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Vip\Adapter\Vip\IVipAdapter',
            $this->stub
        );
    }

    public function testGetNullObject()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub->getNullObjectPublic()
        );

        $this->assertInstanceOf(
            'Sdk\Vip\Model\NullVip',
            $this->stub->getNullObjectPublic()
        );
    }

    //scenario
    /**
     * @dataProvider additionProviderScenario
     */
    public function testScenario($scenario, $expect)
    {
        $this->stub->scenario($scenario);

        $this->assertEquals($expect, $this->stub->getScenario());
    }

    public function additionProviderScenario()
    {
        return array(
            array('VIP_LIST', VipRestfulAdapter::SCENARIOS['VIP_LIST']),
            array('VIP_FETCH_ONE', VipRestfulAdapter::SCENARIOS['VIP_FETCH_ONE']),
            array('', [])
        );
    }

    public function testGetAlonePossessMapErrors()
    {
        $this->assertEquals(VipRestfulAdapter::MAP_ERROR, $this->stub->getAlonePossessMapErrorsPublic());
    }

    private function fetchOne(bool $result)
    {
        $resource = 'resource';
        
        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('get')->with($resource);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);
    }

    public function testFetchOneTrue()
    {
        $id = 1;
        $object = new VipMock($id);
        $this->fetchOne(true);
        $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($object);

        $result = $this->stub->fetchOne($id);

        $this->assertEquals($result, $object);
    }

    public function testFetchOneFalse()
    {
        $id = 1;
        $nullObject = NullVipMock::getInstance();
        $this->fetchOne(false);
        $this->stub->expects($this->exactly(1))->method('getNullObject')->willReturn($nullObject);

        $result = $this->stub->fetchOne($id);

        $this->assertEquals($result, $nullObject);
    }

    private function update(bool $result)
    {
        $id = 1;
        $resource = 'config/vip';
        $data = array();
        $keys = array('indicators', 'staff');
        $operateAbleObject = new VipMock($id);

        // 为 VipRestfulTranslator 类建立预言(prophecy)。
        $translator = $this->prophesize(VipRestfulTranslator::class);
        // 建立预期状况:objectToArray() 方法将会被调用一次。
        $translator->objectToArray($operateAbleObject, $keys)->shouldBeCalled(1)->willReturn($data);
        // 为 getTranslator() 方法建立预期：该方法被调用一次,返回揭示预言。
        $this->stub->expects($this->exactly(1))->method('getTranslator')->willReturn($translator->reveal());

        $this->stub->expects($this->exactly(1))->method('getResource')->willReturn($resource);
        $this->stub->expects($this->exactly(1))->method('patch')->with($resource, $data);
        $this->stub->expects($this->exactly(1))->method('isSuccess')->willReturn($result);

        if ($result) {
            $this->stub->expects($this->exactly(1))->method('translateToObject')->willReturn($operateAbleObject);
        }

        return $operateAbleObject;
    }

    public function testUpdateTrue()
    {
        $operateAbleObject = $this->update(true);

        $result = $this->stub->updatePublic($operateAbleObject);

        $this->assertTrue($result);
    }

    public function testUpdateFalse()
    {
        $operateAbleObject = $this->update(false);

        $result = $this->stub->updatePublic($operateAbleObject);

        $this->assertFalse($result);
    }
}
