<?php
namespace Sdk\Vip\Adapter\Vip;

use PHPUnit\Framework\TestCase;

class VipMockAdapterTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new VipMockAdapter();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIVipAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Vip\Adapter\Vip\IVipAdapter',
            $this->stub
        );
    }

    public function testFetchObject()
    {
        $this->assertInstanceOf(
            'Sdk\Vip\Model\Vip',
            $this->stub->fetchObject(1)
        );
    }
}
