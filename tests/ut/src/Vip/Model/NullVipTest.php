<?php
namespace Sdk\Vip\Model;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class NullVipTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = NullVip::getInstance();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsINull()
    {
        $this->assertInstanceOf(
            'Marmot\Interfaces\INull',
            $this->stub
        );
    }

    public function testExtendsVip()
    {
        $this->assertInstanceOf(
            'Sdk\Vip\Model\Vip',
            $this->stub
        );
    }

    public function testResourceNotExist()
    {
        $stub = new NullVipMock();

        $result = $stub->resourceNotExistPublic();
        $this->assertFalse($result);
        $this->assertEquals(RESOURCE_NOT_EXIST, Core::getLastError()->getId());
    }
}
