<?php
namespace Sdk\Vip\Model;

use PHPUnit\Framework\TestCase;

class VipTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new Vip();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIObject()
    {
        $this->assertInstanceOf(
            'Marmot\Common\Model\IObject',
            $this->stub
        );
    }

    public function testImplementsIOperateAble()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Model\Interfaces\IOperateAble',
            $this->stub
        );
    }
    /**
     * Vip 领域对象,测试构造函数
     */
    public function testVipConstructor()
    {
        $this->assertEmpty($this->stub->getId());
        $this->assertEmpty($this->stub->getIndicators());
        $this->assertEmpty($this->stub->getStatus());
        $this->assertEmpty($this->stub->getCreateTime());
        $this->assertEmpty($this->stub->getUpdateTime());
        $this->assertEmpty($this->stub->getStatusTime());
    }

    //id 测试 ---------------------------------------------------------- start
    /**
     * 设置 Vip setId() 正确的传参类型,期望传值正确
     */
    public function testSetIdCorrectType()
    {
        $this->stub->setId(1);
        $this->assertEquals(1, $this->stub->getId());
    }

    /**
     * 设置 Vip setId() 错误的传参类型.但是传参是数值,期望返回类型正确,值正确.
     */
    public function testSetIdWrongTypeButNumeric()
    {
        $this->stub->setId('1');
        $this->assertTrue(is_int($this->stub->getId()));
        $this->assertEquals(1, $this->stub->getId());
    }
    //id 测试 ----------------------------------------------------------   end

    //indicators 测试 -------------------------------------------------------- start
    /**
     * 设置 Vip setIndicators() 正确的传参类型,期望传值正确
     */
    public function testSetIndicatorsCorrectType()
    {
        $this->stub->setIndicators(array('indicators'));
        $this->assertEquals(array('indicators'), $this->stub->getIndicators());
    }

    /**
     * 设置 Vip setIndicators() 错误的传参类型,期望期望抛出TypeError exception
     *
     * @expectedException TypeError
     */
    public function testSetIndicatorsWrongType()
    {
        $this->stub->setIndicators('indicators');
    }
    //indicators 测试 --------------------------------------------------------   end

    public function testGetRepository()
    {
        $stub = new VipMock();
        $this->assertInstanceOf(
            'Sdk\Vip\Repository\VipRepository',
            $stub->getRepositoryPublic()
        );
    }
}
