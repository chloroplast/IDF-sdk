<?php
namespace Sdk\Vip\WidgetRule;

use Marmot\Core;
use PHPUnit\Framework\TestCase;

class VipWidgetRuleTest extends TestCase
{

    private $stub;

    protected function setUp(): void
    {
        $this->stub = new VipWidgetRule();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    //indicators
    /**
     * @dataProvider additionProviderIndicators
     */
    public function testIndicators($parameter, $expected)
    {
        $result = $this->stub->indicators($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(VIP_INDICATORS_FORMAT_INCORRECT, Core::getLastError()->getId());
    }
    /**
     * @dataProvider additionProviderName
     */
    public function testName($parameter, $expected)
    {
        $result = $this->stub->indicators($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(VIP_NAME_FORMAT_INCORRECT, Core::getLastError()->getId());
    }
    /**
     * @dataProvider additionProviderConsumerMinPrice
     */
    public function testConsumerMinPrice($parameter, $expected)
    {
        $result = $this->stub->indicators($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(VIP_CONSUMER_MIN_PRICE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }
    /**
     * @dataProvider additionProviderConsumerMaxPrice
     */
    public function testConsumerMaxPrice($parameter, $expected)
    {
        $result = $this->stub->indicators($parameter);

        if ($expected) {
            $this->assertTrue($result);
            return ;
        }

        $this->assertFalse($result);
        $this->assertEquals(VIP_CONSUMER_MAX_PRICE_FORMAT_INCORRECT, Core::getLastError()->getId());
    }

    public function additionProviderIndicators()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array('', false),
            array($faker->randomNumber(), false),
            array(array(array('name'=>'name', 'consumerMinPrice'=>0, 'consumerMaxPrice'=>1)), true),
            array($faker->words(), false)
        );
    }

    public function additionProviderName()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(array(array('name'=>'namenamename', 'consumerMinPrice'=>0, 'consumerMaxPrice'=>1)), false),
            array(array(array('name'=>'name', 'consumerMinPrice'=>0, 'consumerMaxPrice'=>1)), true)
        );
    }

    public function additionProviderConsumerMinPrice()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(array(array('name'=>'name', 'consumerMinPrice'=>'con', 'consumerMaxPrice'=>1)), false),
            array(array(array('name'=>'name', 'consumerMinPrice'=>0, 'consumerMaxPrice'=>1)), true)
        );
    }

    public function additionProviderConsumerMaxPrice()
    {
        $faker = \Faker\Factory::create('zh_CN');

        return array(
            array(array(array('name'=>'name', 'consumerMinPrice'=>1, 'consumerMaxPrice'=>'con')), false),
            array(array(array('name'=>'name', 'consumerMinPrice'=>0, 'consumerMaxPrice'=>1)), true)
        );
    }
}
