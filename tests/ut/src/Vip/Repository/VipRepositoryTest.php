<?php
namespace Sdk\Vip\Repository;

use PHPUnit\Framework\TestCase;

class VipRepositoryTest extends TestCase
{
    private $stub;

    protected function setUp(): void
    {
        $this->stub = new VipRepository();
    }

    protected function tearDown(): void
    {
        unset($this->stub);
    }

    public function testImplementsIVipAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Vip\Adapter\Vip\IVipAdapter',
            $this->stub
        );
    }

    public function testExtendsCommonRepository()
    {
        $this->assertInstanceOf(
            'Sdk\Common\Repository\CommonRepository',
            $this->stub
        );
    }

    public function testGetActualAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Vip\Adapter\Vip\VipRestfulAdapter',
            $this->stub->getActualAdapter()
        );
    }

    public function testGetMockAdapter()
    {
        $this->assertInstanceOf(
            'Sdk\Vip\Adapter\Vip\VipMockAdapter',
            $this->stub->getMockAdapter()
        );
    }
}
