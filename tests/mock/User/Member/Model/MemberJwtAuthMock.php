<?php
namespace Sdk\User\Member\Model;

use Sdk\User\Member\Repository\MemberRepository;
use Sdk\User\Member\Translator\MemberTranslator;

class MemberJwtAuthMock extends MemberJwtAuth
{
    public function getRepositoryPublic() : MemberRepository
    {
        return parent::getRepository();
    }

    public function getTranslatorPublic() : MemberTranslator
    {
        return parent::getTranslator();
    }

    public function generateJwtPublic(Member $member) : bool
    {
        return parent::generateJwt($member);
    }

    public function fetchJwtPublic(array $payload) : string
    {
        return parent::fetchJwt($payload);
    }

    public function saveMemberToCachePublic(Member $member) : bool
    {
        return parent::saveMemberToCache($member);
    }

    public function initMemberPublic(Member $member) : bool
    {
        return parent::initMember($member);
    }

    public function verifyJwtPublic(string $jwt) : bool
    {
        return parent::verifyJwt($jwt);
    }

    public function fetchMemberPublic() : Member
    {
        return parent::fetchMember();
    }
}
