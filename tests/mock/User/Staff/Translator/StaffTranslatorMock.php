<?php
namespace Sdk\User\Staff\Translator;

use Marmot\Interfaces\INull;

use Sdk\Role\Translator\RoleTranslator;

class StaffTranslatorMock extends StaffTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }

    public function getRoleTranslatorPublic() : RoleTranslator
    {
        return parent::getRoleTranslator();
    }

    public function purviewFormatConversionToArrayPublic(array $purview) : array
    {
        return parent::purviewFormatConversionToArray($purview);
    }

    public function purviewFormatConversionToObjectPublic(array $purview) : array
    {
        return parent::purviewFormatConversionToObject($purview);
    }
}
