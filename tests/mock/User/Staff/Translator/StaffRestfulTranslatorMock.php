<?php
namespace Sdk\User\Staff\Translator;

use Sdk\Role\Translator\RoleRestfulTranslator;

class StaffRestfulTranslatorMock extends StaffRestfulTranslator
{
    public function getRoleRestfulTranslatorPublic() : RoleRestfulTranslator
    {
        return parent::getRoleRestfulTranslator();
    }
}
