<?php
namespace Sdk\Application\ExportDataTask\Adapter\ExportDataTask;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class ExportDataTaskRestfulAdapterMock extends ExportDataTaskRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
