<?php
namespace Sdk\Application\ExportDataTask\Model;

use Sdk\Application\ExportDataTask\Repository\ExportDataTaskRepository;

class ExportDataTaskMock extends ExportDataTask
{
    public function getRepositoryPublic() : ExportDataTaskRepository
    {
        return parent::getRepository();
    }
}
