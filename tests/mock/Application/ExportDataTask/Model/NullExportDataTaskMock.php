<?php
namespace Sdk\Application\ExportDataTask\Model;

class NullExportDataTaskMock extends NullExportDataTask
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
