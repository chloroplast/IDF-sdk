<?php
namespace Sdk\Application\ParseTask\Translator;

use Marmot\Interfaces\INull;

class ParseTaskTranslatorMock extends ParseTaskTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
