<?php
namespace Sdk\Application\ParseTask\Model;

use Sdk\Order\Repository\AmazonLTLRepository;
use Sdk\Order\Repository\AmazonFTLRepository;
use Sdk\Order\Repository\OrderLTLRepository;
use Sdk\Order\Repository\OrderFTLRepository;

class ParseTaskMock extends ParseTask
{
    public function getAmazonLTLRepositoryPublic() : AmazonLTLRepository
    {
        return parent::getAmazonLTLRepository();
    }

    public function getAmazonFTLRepositoryPublic() : AmazonFTLRepository
    {
        return parent::getAmazonFTLRepository();
    }

    public function getOrderLTLRepositoryPublic() : OrderLTLRepository
    {
        return parent::getOrderLTLRepository();
    }

    public function getOrderFTLRepositoryPublic() : OrderFTLRepository
    {
        return parent::getOrderFTLRepository();
    }
}
