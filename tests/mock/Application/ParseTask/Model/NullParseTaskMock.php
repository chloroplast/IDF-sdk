<?php
namespace Sdk\Application\ParseTask\Model;

class NullParseTaskMock extends NullParseTask
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
