<?php
namespace Sdk\Configuration\Model;

class NullCustomListMock extends NullCustomList
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
