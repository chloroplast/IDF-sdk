<?php
namespace Sdk\Configuration\Model;

use Sdk\Configuration\Repository\CustomListRepository;

class CustomListMock extends CustomList
{
    public function getRepositoryPublic() : CustomListRepository
    {
        return parent::getRepository();
    }
}
