<?php
namespace Sdk\Configuration\Translator;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;

class CustomListRestfulTranslatorMock extends CustomListRestfulTranslator
{
    public function getStaffRestfulTranslatorPublic() : StaffRestfulTranslator
    {
        return parent::getStaffRestfulTranslator();
    }
}
