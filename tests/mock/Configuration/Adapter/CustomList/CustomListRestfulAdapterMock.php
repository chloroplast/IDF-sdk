<?php
namespace Sdk\Configuration\Adapter\CustomList;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class CustomListRestfulAdapterMock extends CustomListRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
