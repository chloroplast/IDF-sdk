<?php
namespace Sdk\CarType\Adapter\CarType;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class CarTypeRestfulAdapterMock extends CarTypeRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
