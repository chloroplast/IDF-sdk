<?php
namespace Sdk\CarType\Model;

use Sdk\CarType\Repository\CarTypeRepository;

class CarTypeMock extends CarType
{
    public function getRepositoryPublic() : CarTypeRepository
    {
        return parent::getRepository();
    }
}
