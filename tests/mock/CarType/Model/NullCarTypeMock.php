<?php
namespace Sdk\CarType\Model;

class NullCarTypeMock extends NullCarType
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
