<?php
namespace Sdk\CarType\Translator;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;

class CarTypeRestfulTranslatorMock extends CarTypeRestfulTranslator
{
    public function getStaffRestfulTranslatorPublic() : StaffRestfulTranslator
    {
        return parent::getStaffRestfulTranslator();
    }
}
