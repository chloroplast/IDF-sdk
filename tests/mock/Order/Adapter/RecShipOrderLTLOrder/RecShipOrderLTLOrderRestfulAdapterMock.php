<?php
namespace Sdk\Order\Adapter\RecShipOrderLTLOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class RecShipOrderLTLOrderRestfulAdapterMock extends RecShipOrderLTLOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
