<?php
namespace Sdk\Order\Adapter\DisOrderLTLOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class DisOrderLTLOrderRestfulAdapterMock extends DisOrderLTLOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
