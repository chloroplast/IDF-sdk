<?php
namespace Sdk\Order\Adapter\MemberOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class MemberOrderRestfulAdapterMock extends MemberOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
