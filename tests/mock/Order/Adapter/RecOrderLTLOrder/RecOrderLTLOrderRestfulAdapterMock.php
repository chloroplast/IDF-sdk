<?php
namespace Sdk\Order\Adapter\RecOrderLTLOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class RecOrderLTLOrderRestfulAdapterMock extends RecOrderLTLOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
