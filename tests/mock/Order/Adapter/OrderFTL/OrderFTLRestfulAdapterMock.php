<?php
namespace Sdk\Order\Adapter\OrderFTL;

use Marmot\Interfaces\INull;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

use Sdk\Application\ParseTask\Translator\ParseTaskRestfulTranslator;
use Sdk\Order\Translator\OrderPriceReportRestfulTranslator;

class OrderFTLRestfulAdapterMock extends OrderFTLRestfulAdapter
{
    use CommonRestfulAdapterTrait;

    public function getNullParseTaskPublic() : INull
    {
        return parent::getNullParseTask();
    }

    public function getParseTaskRestfulTranslatorPublic() : ParseTaskRestfulTranslator
    {
        return parent::getParseTaskRestfulTranslator();
    }

    public function getOrderPriceReportRestfulTranslatorPublic() : OrderPriceReportRestfulTranslator
    {
        return parent::getOrderPriceReportRestfulTranslator();
    }
}
