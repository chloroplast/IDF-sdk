<?php
namespace Sdk\Order\Adapter\DispatchOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class DispatchOrderRestfulAdapterMock extends DispatchOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
