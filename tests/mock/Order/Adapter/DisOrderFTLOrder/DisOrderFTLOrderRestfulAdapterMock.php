<?php
namespace Sdk\Order\Adapter\DisOrderFTLOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class DisOrderFTLOrderRestfulAdapterMock extends DisOrderFTLOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
