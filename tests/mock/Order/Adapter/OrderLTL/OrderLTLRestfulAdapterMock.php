<?php
namespace Sdk\Order\Adapter\OrderLTL;

use Marmot\Interfaces\INull;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

use Sdk\Application\ParseTask\Translator\ParseTaskRestfulTranslator;
use Sdk\Order\Translator\OrderPriceReportRestfulTranslator;
use Sdk\Order\Translator\ItemLTLRestfulTranslator;

class OrderLTLRestfulAdapterMock extends OrderLTLRestfulAdapter
{
    use CommonRestfulAdapterTrait;

    public function getNullParseTaskPublic() : INull
    {
        return parent::getNullParseTask();
    }

    public function getParseTaskRestfulTranslatorPublic() : ParseTaskRestfulTranslator
    {
        return parent::getParseTaskRestfulTranslator();
    }

    public function getOrderPriceReportRestfulTranslatorPublic() : OrderPriceReportRestfulTranslator
    {
        return parent::getOrderPriceReportRestfulTranslator();
    }

    public function getItemLTLRestfulTranslatorPublic() : ItemLTLRestfulTranslator
    {
        return parent::getItemLTLRestfulTranslator();
    }
}
