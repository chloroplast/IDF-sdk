<?php
namespace Sdk\Order\Adapter\DisShipOrderLTLOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class DisShipOrderLTLOrderRestfulAdapterMock extends DisShipOrderLTLOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
