<?php
namespace Sdk\Order\Adapter\DisAmaFTLOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class DisAmaFTLOrderRestfulAdapterMock extends DisAmaFTLOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
