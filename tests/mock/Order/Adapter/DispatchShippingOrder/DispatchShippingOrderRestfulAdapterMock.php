<?php
namespace Sdk\Order\Adapter\DispatchShippingOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class DispatchShippingOrderRestfulAdapterMock extends DispatchShippingOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
