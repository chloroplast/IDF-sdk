<?php
namespace Sdk\Order\Adapter\ReceiveOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class ReceiveOrderRestfulAdapterMock extends ReceiveOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
