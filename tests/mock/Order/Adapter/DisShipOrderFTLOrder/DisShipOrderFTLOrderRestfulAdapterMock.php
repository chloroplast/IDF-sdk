<?php
namespace Sdk\Order\Adapter\DisShipOrderFTLOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class DisShipOrderFTLOrderRestfulAdapterMock extends DisShipOrderFTLOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
