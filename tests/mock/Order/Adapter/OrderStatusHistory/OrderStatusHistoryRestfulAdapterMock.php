<?php
namespace Sdk\Order\Adapter\OrderStatusHistory;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class OrderStatusHistoryRestfulAdapterMock extends OrderStatusHistoryRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
