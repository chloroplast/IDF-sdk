<?php
namespace Sdk\Order\Adapter\ReceiveShippingOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class ReceiveShippingOrderRestfulAdapterMock extends ReceiveShippingOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
