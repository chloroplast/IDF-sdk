<?php
namespace Sdk\Order\Adapter\DisShipAmaFTLOrder;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class DisShipAmaFTLOrderRestfulAdapterMock extends DisShipAmaFTLOrderRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
