<?php
namespace Sdk\Order\Model;

class NullRecShipOrderLTLOrderMock extends NullRecShipOrderLTLOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
