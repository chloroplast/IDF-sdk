<?php
namespace Sdk\Order\Model;

class NullAmazonLTLMock extends NullAmazonLTL
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
