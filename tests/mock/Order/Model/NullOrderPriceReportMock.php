<?php
namespace Sdk\Order\Model;

class NullOrderPriceReportMock extends NullOrderPriceReport
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
