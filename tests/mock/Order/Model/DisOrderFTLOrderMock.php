<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\DisOrderFTLOrderRepository;

class DisOrderFTLOrderMock extends DisOrderFTLOrder
{
    public function getRepositoryPublic() : DisOrderFTLOrderRepository
    {
        return parent::getRepository();
    }
}
