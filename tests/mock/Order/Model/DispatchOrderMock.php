<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\DispatchOrderRepository;

class DispatchOrderMock extends DispatchOrder
{
    public function getRepositoryPublic() : DispatchOrderRepository
    {
        return parent::getRepository();
    }
}
