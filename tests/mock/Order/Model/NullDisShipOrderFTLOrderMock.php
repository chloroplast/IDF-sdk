<?php
namespace Sdk\Order\Model;

class NullDisShipOrderFTLOrderMock extends NullDisShipOrderFTLOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
