<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\AmazonLTLRepository;

class AmazonLTLMock extends AmazonLTL
{
    public function getRepositoryPublic() : AmazonLTLRepository
    {
        return parent::getRepository();
    }
}
