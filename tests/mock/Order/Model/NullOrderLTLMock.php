<?php
namespace Sdk\Order\Model;

class NullOrderLTLMock extends NullOrderLTL
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
