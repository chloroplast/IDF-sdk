<?php
namespace Sdk\Order\Model;

class NullMemberOrderMock extends NullMemberOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
