<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\OrderLTLRepository;

class ItemLTLMock extends ItemLTL
{
    public function getRepositoryPublic() : OrderLTLRepository
    {
        return parent::getRepository();
    }
}
