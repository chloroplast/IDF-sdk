<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\DisAmaFTLOrderRepository;

class DisAmaFTLOrderMock extends DisAmaFTLOrder
{
    public function getRepositoryPublic() : DisAmaFTLOrderRepository
    {
        return parent::getRepository();
    }
}
