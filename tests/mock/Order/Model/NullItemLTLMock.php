<?php
namespace Sdk\Order\Model;

class NullItemLTLMock extends NullItemLTL
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
