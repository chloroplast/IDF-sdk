<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\OrderLTLRepository;

class OrderLTLMock extends OrderLTL
{
    public function getRepositoryPublic() : OrderLTLRepository
    {
        return parent::getRepository();
    }
}
