<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\OrderFTLRepository;

class OrderFTLMock extends OrderFTL
{
    public function getRepositoryPublic() : OrderFTLRepository
    {
        return parent::getRepository();
    }
}
