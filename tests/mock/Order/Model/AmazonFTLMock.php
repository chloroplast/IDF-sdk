<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\AmazonFTLRepository;

class AmazonFTLMock extends AmazonFTL
{
    public function getRepositoryPublic() : AmazonFTLRepository
    {
        return parent::getRepository();
    }
}
