<?php
namespace Sdk\Order\Model;

class NullDisShipAmaFTLOrderMock extends NullDisShipAmaFTLOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
