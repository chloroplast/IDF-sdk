<?php
namespace Sdk\Order\Model;

class NullDispatchShippingOrderMock extends NullDispatchShippingOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
