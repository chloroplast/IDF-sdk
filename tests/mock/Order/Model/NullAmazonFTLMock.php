<?php
namespace Sdk\Order\Model;

class NullAmazonFTLMock extends NullAmazonFTL
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
