<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\RecOrderLTLOrderRepository;

class RecOrderLTLOrderMock extends RecOrderLTLOrder
{
    public function getRepositoryPublic() : RecOrderLTLOrderRepository
    {
        return parent::getRepository();
    }
}
