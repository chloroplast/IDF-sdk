<?php
namespace Sdk\Order\Model;

class NullReceiveShippingOrderMock extends NullReceiveShippingOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
