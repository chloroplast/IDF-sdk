<?php
namespace Sdk\Order\Model;

class NullDispatchOrderMock extends NullDispatchOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
