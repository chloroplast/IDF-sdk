<?php
namespace Sdk\Order\Model;

class NullRecOrderLTLOrderMock extends NullRecOrderLTLOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
