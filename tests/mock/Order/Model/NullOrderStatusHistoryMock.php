<?php
namespace Sdk\Order\Model;

class NullOrderStatusHistoryMock extends NullOrderStatusHistory
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
