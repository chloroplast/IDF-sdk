<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\DisOrderLTLOrderRepository;

class DisOrderLTLOrderMock extends DisOrderLTLOrder
{
    public function getRepositoryPublic() : DisOrderLTLOrderRepository
    {
        return parent::getRepository();
    }
}
