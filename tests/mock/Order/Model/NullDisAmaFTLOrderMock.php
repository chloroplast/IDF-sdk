<?php
namespace Sdk\Order\Model;

class NullDisAmaFTLOrderMock extends NullDisAmaFTLOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
