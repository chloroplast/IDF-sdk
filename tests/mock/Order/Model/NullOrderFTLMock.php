<?php
namespace Sdk\Order\Model;

class NullOrderFTLMock extends NullOrderFTL
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
