<?php
namespace Sdk\Order\Model;

use Sdk\Order\Repository\ReceiveOrderRepository;

class ReceiveOrderMock extends ReceiveOrder
{
    public function getRepositoryPublic() : ReceiveOrderRepository
    {
        return parent::getRepository();
    }
}
