<?php
namespace Sdk\Order\Model;

class NullReceiveOrderMock extends NullReceiveOrder
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
