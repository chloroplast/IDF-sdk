<?php
namespace Sdk\Order\Translator;

use Sdk\User\Member\Translator\MemberRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;

class MemberOrderRestfulTranslatorMock extends MemberOrderRestfulTranslator
{
    public function getMemberRestfulTranslatorPublic() : MemberRestfulTranslator
    {
        return parent::getMemberRestfulTranslator();
    }

    public function getStaffRestfulTranslatorPublic() : StaffRestfulTranslator
    {
        return parent::getStaffRestfulTranslator();
    }
}
