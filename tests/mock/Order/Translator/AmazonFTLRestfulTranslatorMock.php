<?php
namespace Sdk\Order\Translator;

use Sdk\User\Member\Translator\MemberRestfulTranslator;
use Sdk\User\Staff\Translator\StaffRestfulTranslator;
use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;
use Sdk\Address\Translator\AddressRestfulTranslator;

class AmazonFTLRestfulTranslatorMock extends AmazonFTLRestfulTranslator
{
    public function getMemberRestfulTranslatorPublic() : MemberRestfulTranslator
    {
        return parent::getMemberRestfulTranslator();
    }

    public function getStaffRestfulTranslatorPublic() : StaffRestfulTranslator
    {
        return parent::getStaffRestfulTranslator();
    }

    public function getWarehouseRestfulTranslatorPublic() : WarehouseRestfulTranslator
    {
        return parent::getWarehouseRestfulTranslator();
    }

    public function getAddressRestfulTranslatorPublic() : AddressRestfulTranslator
    {
        return parent::getAddressRestfulTranslator();
    }

    public function getMemberOrderRestfulTranslatorPublic() : MemberOrderRestfulTranslator
    {
        return parent::getMemberOrderRestfulTranslator();
    }
}
