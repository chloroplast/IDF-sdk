<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;

class OrderPriceReportTranslatorMock extends OrderPriceReportTranslator
{

    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
