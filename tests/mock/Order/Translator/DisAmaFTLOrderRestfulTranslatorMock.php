<?php
namespace Sdk\Order\Translator;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;

class DisAmaFTLOrderRestfulTranslatorMock extends DisAmaFTLOrderRestfulTranslator
{
    public function getStaffRestfulTranslatorPublic() : StaffRestfulTranslator
    {
        return parent::getStaffRestfulTranslator();
    }
}
