<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;

use Sdk\CarType\Translator\CarTypeTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;

class ReceiveOrderTranslatorMock extends ReceiveOrderTranslator
{
    public function getCarTypeTranslatorPublic() : CarTypeTranslator
    {
        return parent::getCarTypeTranslator();
    }

    public function getStaffTranslatorPublic() : StaffTranslator
    {
        return parent::getStaffTranslator();
    }
    
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
