<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;

use Sdk\Warehouse\Translator\WarehouseTranslator;

class ReceiveShippingOrderTranslatorMock extends ReceiveShippingOrderTranslator
{
    public function getWarehouseTranslatorPublic() : WarehouseTranslator
    {
        return parent::getWarehouseTranslator();
    }

    public function getReceiveOrderTranslatorPublic() : ReceiveOrderTranslator
    {
        return parent::getReceiveOrderTranslator();
    }

    public function getAmazonLTLTranslatorPublic() : AmazonLTLTranslator
    {
        return parent::getAmazonLTLTranslator();
    }
    
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
