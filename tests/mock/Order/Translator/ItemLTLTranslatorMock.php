<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;

class ItemLTLTranslatorMock extends ItemLTLTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
