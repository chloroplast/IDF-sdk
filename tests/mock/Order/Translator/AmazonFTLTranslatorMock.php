<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;

use Sdk\User\Member\Translator\MemberTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;
use Sdk\Warehouse\Translator\WarehouseTranslator;
use Sdk\Address\Translator\AddressTranslator;

class AmazonFTLTranslatorMock extends AmazonFTLTranslator
{
    public function getMemberTranslatorPublic() : MemberTranslator
    {
        return parent::getMemberTranslator();
    }

    public function getStaffTranslatorPublic() : StaffTranslator
    {
        return parent::getStaffTranslator();
    }

    public function getWarehouseTranslatorPublic() : WarehouseTranslator
    {
        return parent::getWarehouseTranslator();
    }

    public function getAddressTranslatorPublic() : AddressTranslator
    {
        return parent::getAddressTranslator();
    }

    public function getMemberOrderTranslatorPublic() : MemberOrderTranslator
    {
        return parent::getMemberOrderTranslator();
    }
    
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
