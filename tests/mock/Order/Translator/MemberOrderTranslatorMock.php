<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;

use Sdk\User\Member\Translator\MemberTranslator;
use Sdk\User\Staff\Translator\StaffTranslator;

class MemberOrderTranslatorMock extends MemberOrderTranslator
{
    public function getMemberTranslatorPublic() : MemberTranslator
    {
        return parent::getMemberTranslator();
    }

    public function getStaffTranslatorPublic() : StaffTranslator
    {
        return parent::getStaffTranslator();
    }
    
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
