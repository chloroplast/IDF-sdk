<?php
namespace Sdk\Order\Translator;

use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;

class ReceiveShippingOrderRestfulTranslatorMock extends ReceiveShippingOrderRestfulTranslator
{
    public function getWarehouseRestfulTranslatorPublic() : WarehouseRestfulTranslator
    {
        return parent::getWarehouseRestfulTranslator();
    }

    public function getAmazonLTLRestfulTranslatorPublic() : AmazonLTLRestfulTranslator
    {
        return parent::getAmazonLTLRestfulTranslator();
    }
    
    public function getReceiveOrderRestfulTranslatorPublic() : ReceiveOrderRestfulTranslator
    {
        return parent::getReceiveOrderRestfulTranslator();
    }
}
