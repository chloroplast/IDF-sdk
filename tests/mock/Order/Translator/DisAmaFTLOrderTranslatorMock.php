<?php
namespace Sdk\Order\Translator;

use Marmot\Interfaces\INull;

use Sdk\User\Staff\Translator\StaffTranslator;

class DisAmaFTLOrderTranslatorMock extends DisAmaFTLOrderTranslator
{
    public function getStaffTranslatorPublic() : StaffTranslator
    {
        return parent::getStaffTranslator();
    }
    
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
