<?php
namespace Sdk\Order\Translator;

use Sdk\User\Staff\Translator\StaffRestfulTranslator;
use Sdk\CarType\Translator\CarTypeRestfulTranslator;

class ReceiveOrderRestfulTranslatorMock extends ReceiveOrderRestfulTranslator
{
    public function getStaffRestfulTranslatorPublic() : StaffRestfulTranslator
    {
        return parent::getStaffRestfulTranslator();
    }

    public function getCarTypeRestfulTranslatorPublic() : CarTypeRestfulTranslator
    {
        return parent::getCarTypeRestfulTranslator();
    }

    public function getAmazonLTLRestfulTranslatorPublic() : AmazonLTLRestfulTranslator
    {
        return parent::getAmazonLTLRestfulTranslator();
    }
}
