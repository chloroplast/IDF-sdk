<?php
namespace Sdk\Warehouse\Translator;

use Marmot\Interfaces\INull;

class WarehouseTranslatorMock extends WarehouseTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
