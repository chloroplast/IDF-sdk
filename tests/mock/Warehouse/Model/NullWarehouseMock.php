<?php
namespace Sdk\Warehouse\Model;

class NullWarehouseMock extends NullWarehouse
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
