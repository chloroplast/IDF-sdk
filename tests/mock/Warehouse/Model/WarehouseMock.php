<?php
namespace Sdk\Warehouse\Model;

use Sdk\Warehouse\Repository\WarehouseRepository;

class WarehouseMock extends Warehouse
{
    public function getRepositoryPublic() : WarehouseRepository
    {
        return parent::getRepository();
    }
}
