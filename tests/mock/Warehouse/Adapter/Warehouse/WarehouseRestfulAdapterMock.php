<?php
namespace Sdk\Warehouse\Adapter\Warehouse;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class WarehouseRestfulAdapterMock extends WarehouseRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
