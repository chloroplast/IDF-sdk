<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;

class OrderLTLPolicyTranslatorMock extends OrderLTLPolicyTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
