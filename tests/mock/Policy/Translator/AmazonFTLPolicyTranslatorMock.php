<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;

class AmazonFTLPolicyTranslatorMock extends AmazonFTLPolicyTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
