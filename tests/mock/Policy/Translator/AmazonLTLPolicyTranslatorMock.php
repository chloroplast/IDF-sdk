<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;

use Sdk\Warehouse\Translator\WarehouseTranslator;

class AmazonLTLPolicyTranslatorMock extends AmazonLTLPolicyTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }

    public function getWarehouseTranslatorPublic() : WarehouseTranslator
    {
        return parent::getWarehouseTranslator();
    }
}
