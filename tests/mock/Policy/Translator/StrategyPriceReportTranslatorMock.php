<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;

class StrategyPriceReportTranslatorMock extends StrategyPriceReportTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
