<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;

class TemplateTranslatorMock extends TemplateTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
