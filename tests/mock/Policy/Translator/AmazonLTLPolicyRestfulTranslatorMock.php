<?php
namespace Sdk\Policy\Translator;

use Sdk\Warehouse\Translator\WarehouseRestfulTranslator;

class AmazonLTLPolicyRestfulTranslatorMock extends AmazonLTLPolicyRestfulTranslator
{
    public function getWarehouseRestfulTranslatorPublic() : WarehouseRestfulTranslator
    {
        return parent::getWarehouseRestfulTranslator();
    }
}
