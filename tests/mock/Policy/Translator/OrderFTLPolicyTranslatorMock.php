<?php
namespace Sdk\Policy\Translator;

use Marmot\Interfaces\INull;

class OrderFTLPolicyTranslatorMock extends OrderFTLPolicyTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
