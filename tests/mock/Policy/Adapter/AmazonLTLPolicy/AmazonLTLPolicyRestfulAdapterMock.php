<?php
namespace Sdk\Policy\Adapter\AmazonLTLPolicy;

use Marmot\Interfaces\INull;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;
use Sdk\Policy\Translator\TemplateRestfulTranslator;
use Sdk\Policy\Translator\StrategyPriceReportRestfulTranslator;

class AmazonLTLPolicyRestfulAdapterMock extends AmazonLTLPolicyRestfulAdapter
{
    use CommonRestfulAdapterTrait;

    public function getNullTemplatePublic() : INull
    {
        return parent::getNullTemplate();
    }

    public function getTemplateRestfulTranslatorPublic() : TemplateRestfulTranslator
    {
        return parent::getTemplateRestfulTranslator();
    }

    public function getStrategyPriceReportRestfulTranslatorPublic() : StrategyPriceReportRestfulTranslator
    {
        return parent::getStrategyPriceReportRestfulTranslator();
    }

    public function translateToTemplatePublic()
    {
        return parent::translateToTemplate();
    }
}
