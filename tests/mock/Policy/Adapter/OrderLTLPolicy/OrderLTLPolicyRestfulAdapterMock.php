<?php
namespace Sdk\Policy\Adapter\OrderLTLPolicy;

use Marmot\Interfaces\INull;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;
use Sdk\Policy\Translator\TemplateRestfulTranslator;

class OrderLTLPolicyRestfulAdapterMock extends OrderLTLPolicyRestfulAdapter
{
    use CommonRestfulAdapterTrait;

    public function getNullTemplatePublic() : INull
    {
        return parent::getNullTemplate();
    }

    public function getTemplateRestfulTranslatorPublic() : TemplateRestfulTranslator
    {
        return parent::getTemplateRestfulTranslator();
    }

    public function translateToTemplatePublic()
    {
        return parent::translateToTemplate();
    }
}
