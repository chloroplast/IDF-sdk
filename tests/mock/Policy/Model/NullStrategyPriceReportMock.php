<?php
namespace Sdk\Policy\Model;

class NullStrategyPriceReportMock extends NullStrategyPriceReport
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
