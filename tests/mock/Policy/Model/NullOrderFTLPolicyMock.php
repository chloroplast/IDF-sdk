<?php
namespace Sdk\Policy\Model;

class NullOrderFTLPolicyMock extends NullOrderFTLPolicy
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
