<?php
namespace Sdk\Policy\Model;

use Sdk\Policy\Repository\AmazonFTLPolicyRepository;

class AmazonFTLPolicyMock extends AmazonFTLPolicy
{
    public function getRepositoryPublic() : AmazonFTLPolicyRepository
    {
        return parent::getRepository();
    }
}
