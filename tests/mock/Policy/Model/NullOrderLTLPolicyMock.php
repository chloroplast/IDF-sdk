<?php
namespace Sdk\Policy\Model;

class NullOrderLTLPolicyMock extends NullOrderLTLPolicy
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
