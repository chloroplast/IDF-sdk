<?php
namespace Sdk\Policy\Model;

class NullAmazonLTLPolicyMock extends NullAmazonLTLPolicy
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
