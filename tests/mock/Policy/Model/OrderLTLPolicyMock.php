<?php
namespace Sdk\Policy\Model;

use Sdk\Policy\Repository\OrderLTLPolicyRepository;

class OrderLTLPolicyMock extends OrderLTLPolicy
{
    public function getRepositoryPublic() : OrderLTLPolicyRepository
    {
        return parent::getRepository();
    }
}
