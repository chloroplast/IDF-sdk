<?php
namespace Sdk\Policy\Model;

use Sdk\Policy\Repository\OrderFTLPolicyRepository;

class OrderFTLPolicyMock extends OrderFTLPolicy
{
    public function getRepositoryPublic() : OrderFTLPolicyRepository
    {
        return parent::getRepository();
    }
}
