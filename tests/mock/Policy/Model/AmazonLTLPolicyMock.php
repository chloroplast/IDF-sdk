<?php
namespace Sdk\Policy\Model;

use Sdk\Policy\Repository\AmazonLTLPolicyRepository;

class AmazonLTLPolicyMock extends AmazonLTLPolicy
{
    public function getRepositoryPublic() : AmazonLTLPolicyRepository
    {
        return parent::getRepository();
    }
}
