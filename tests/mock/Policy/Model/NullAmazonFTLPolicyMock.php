<?php
namespace Sdk\Policy\Model;

class NullAmazonFTLPolicyMock extends NullAmazonFTLPolicy
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
