<?php
namespace Sdk\Policy\Model;

use Sdk\Policy\Repository\AmazonLTLPolicyRepository;

class StrategyPriceReportMock extends StrategyPriceReport
{
    public function getRepositoryPublic() : AmazonLTLPolicyRepository
    {
        return parent::getRepository();
    }
}
