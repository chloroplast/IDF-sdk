<?php
namespace Sdk\Policy\Model;

class NullTemplateMock extends NullTemplate
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
