<?php
namespace Sdk\Statistics\Record\Translator;

use Marmot\Interfaces\INull;

class RecordTranslatorMock extends RecordTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
