<?php
namespace Sdk\Statistics\Record\Adapter\Record;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class RecordRestfulAdapterMock extends RecordRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
