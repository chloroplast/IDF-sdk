<?php
namespace Sdk\Message\Adapter\Message;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class MessageRestfulAdapterMock extends MessageRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
