<?php
namespace Sdk\Message\Adapter\ImMessage;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class ImMessageRestfulAdapterMock extends ImMessageRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
