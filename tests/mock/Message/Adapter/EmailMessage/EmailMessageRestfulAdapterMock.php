<?php
namespace Sdk\Message\Adapter\EmailMessage;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class EmailMessageRestfulAdapterMock extends EmailMessageRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
