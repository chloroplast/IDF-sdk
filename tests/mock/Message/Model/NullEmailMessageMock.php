<?php
namespace Sdk\Message\Model;

class NullEmailMessageMock extends NullEmailMessage
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
