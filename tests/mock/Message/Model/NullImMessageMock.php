<?php
namespace Sdk\Message\Model;

class NullImMessageMock extends NullImMessage
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
