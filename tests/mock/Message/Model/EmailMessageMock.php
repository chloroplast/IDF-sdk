<?php
namespace Sdk\Message\Model;

use Sdk\Message\Repository\EmailMessageRepository;

class EmailMessageMock extends EmailMessage
{
    public function getRepositoryPublic() : EmailMessageRepository
    {
        return parent::getRepository();
    }
}
