<?php
namespace Sdk\Message\Model;

class NullMessageMock extends NullMessage
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
