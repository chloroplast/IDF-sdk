<?php
namespace Sdk\Message\Model;

use Sdk\Message\Repository\ImMessageRepository;

class ImMessageMock extends ImMessage
{
    public function getRepositoryPublic() : ImMessageRepository
    {
        return parent::getRepository();
    }
}
