<?php
namespace Sdk\Message\Model;

use Sdk\Message\Repository\MessageRepository;

class MessageMock extends Message
{
    public function getRepositoryPublic() : MessageRepository
    {
        return parent::getRepository();
    }
}
