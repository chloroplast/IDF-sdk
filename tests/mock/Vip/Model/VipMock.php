<?php
namespace Sdk\Vip\Model;

use Sdk\Vip\Repository\VipRepository;

class VipMock extends Vip
{
    public function getRepositoryPublic() : VipRepository
    {
        return parent::getRepository();
    }
}
