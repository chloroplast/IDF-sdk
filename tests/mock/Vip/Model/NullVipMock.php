<?php
namespace Sdk\Vip\Model;

class NullVipMock extends NullVip
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
