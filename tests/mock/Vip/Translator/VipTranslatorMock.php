<?php
namespace Sdk\Vip\Translator;

use Marmot\Interfaces\INull;

class VipTranslatorMock extends VipTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
