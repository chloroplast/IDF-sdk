<?php
namespace Sdk\Vip\Adapter\Vip;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;
use Sdk\Common\Model\Interfaces\IOperateAble;

class VipRestfulAdapterMock extends VipRestfulAdapter
{
    use CommonRestfulAdapterTrait;
    
    public function updatePublic(IOperateAble $operateAbleObject)
    {
        return $this->update($operateAbleObject);
    }
}
