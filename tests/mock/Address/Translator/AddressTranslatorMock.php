<?php
namespace Sdk\Address\Translator;

use Marmot\Interfaces\INull;

class AddressTranslatorMock extends AddressTranslator
{
    public function getNullObjectPublic() : INull
    {
        return parent::getNullObject();
    }
}
