<?php
namespace Sdk\Address\Model;

use Sdk\Address\Repository\AddressRepository;

class AddressMock extends Address
{
    public function getRepositoryPublic() : AddressRepository
    {
        return parent::getRepository();
    }
}
