<?php
namespace Sdk\Address\Model;

class NullAddressMock extends NullAddress
{
    public function resourceNotExistPublic() : bool
    {
        return parent::resourceNotExist();
    }
}
