<?php
namespace Sdk\Address\Adapter\Address;

use Sdk\Common\Adapter\CommonRestfulAdapterTrait;

class AddressRestfulAdapterMock extends AddressRestfulAdapter
{
    use CommonRestfulAdapterTrait;
}
