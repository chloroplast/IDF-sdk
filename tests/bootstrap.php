<?php

include './vendor/autoload.php';
require './Core.php';

use Marmot\Core;
use Sdk\User\Staff\Repository\StaffRepository;
use Sdk\User\Member\Repository\MemberRepository;

$testCore = Marmot\Core::getInstance();
$testCore->initTest();

// 单元测试：初始化后台登录用户信息
$staffRepository = new StaffRepository();
$staff = $staffRepository->scenario(StaffRepository::FETCH_ONE_MODEL_UN)->fetchOne(1);
Core::$container->set('staff', $staff);

// 单元测试：初始化前台登录用户信息
$memberRepository = new MemberRepository();
$member = $memberRepository->scenario(MemberRepository::FETCH_ONE_MODEL_UN)->fetchOne(1);
Core::$container->set('member', $member);
